# This file is part of DRASI - a data acquisition data pump.
#
# Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301  USA

all: lwroc_dir lwrocmon_dir \
	nulldaq_dir testdaq_dir f_user_daq_dir trigbussim_dir \
	f_user_example_dir \
	proglinks

build_rules_dir:
	ACC_MAKEFLAGS="$(MAKEFLAGS)" $(MAKE) -C build_rules

hwmap_dir: build_rules_dir
	$(MAKE) -C hwmap

lwroc_dir: hwmap_dir
	$(MAKE) -C lwroc

lwrocmon_dir: lwroc_dir
	$(MAKE) -C lwrocmon

nulldaq_dir: lwroc_dir
	$(MAKE) -C nulldaq

testdaq_dir: lwroc_dir
	$(MAKE) -C testdaq

f_user_daq_dir: lwroc_dir
	$(MAKE) -C f_user_daq

f_user_example_dir: f_user_daq_dir
	$(MAKE) -C f_user_example -f makefile.drasi

trigbussim_dir: lwroc_dir
	$(MAKE) -C trigbussim

###
# These symlinks are not removed when cleaning, since they work
# for multiple platforms

PROGS=lwrocmon lwroclog lwrocctrl lwrocdt lwrocmerge \
	f_user_example f_user_regress f_user_speed f_user_sync \
	f_user_cmvlc_mdpp \
	filter_example filter_trbts2lmdwr  filter_lmd2ebye \
	testdaq nulldaq \
	trigbussim port_help test_track_timestamp

.PHONY: proglinks
proglinks: $(addprefix bin/,$(PROGS))

# | is a order-only prerequisite  (extra test below if make anyhow runs it)
bin/%: | hwmap_dir
	@# mkdir -p bin/ # in hwmap/Makefile, since MKDIR_P not available here
	@(test -e $@ && \
	  echo "   LN    $@ (order-only prereq. not understood by make)") || \
	  echo "   LN    $@"
	@test -e $@ || ln -s ../scripts/drasiexecbin.sh $@

###

make stress:
	scripts/runsim.sh --session=STRESS --no-attach
	scripts/simstress.sh --session=STRESS --num-stress=5
	scripts/runsim.sh --session=STRESS --kill

###

chkoptdoc:
	find . | egrep "\.(h|c)" | grep -v "/gen" | \
	  xargs -n 1 cat | scripts/chkcmdlineoptsdoc.pl

###

# Explicitly do not depend on lwroc_dir.

# Just run it with 'make -k showconfig'.

showconfig:
	$(MAKE) -C lwroc showconfig

showconfig_all:
	$(MAKE) -C lwroc showconfig_all

###

clean:
	$(MAKE) -C build_rules clean
	$(MAKE) -C hwmap      clean
	$(MAKE) -C lwroc      clean
	$(MAKE) -C lwrocmon   clean
	$(MAKE) -C nulldaq    clean
	$(MAKE) -C testdaq    clean
	$(MAKE) -C f_user_daq clean
	$(MAKE) -C trigbussim clean
	$(MAKE) -C f_user_example clean -f makefile.drasi
	rm -f bin/emptydaq  # to be removed

clean-all:
	$(MAKE) -C build_rules clean-all
	$(MAKE) -C hwmap      clean-all
	$(MAKE) -C lwroc      clean-all
	$(MAKE) -C lwrocmon   clean-all
	$(MAKE) -C nulldaq    clean-all
	$(MAKE) -C testdaq    clean-all
	$(MAKE) -C f_user_daq clean-all
	$(MAKE) -C trigbussim clean-all
	$(MAKE) -C f_user_example clean-all -f makefile.drasi
	rm -f $(addprefix bin/,$(PROGS))

#!/bin/sh

# This file is part of DRASI - a data acquisition data pump.
#
# Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301  USA

# The purpose of this script is to provide necessary compile and link
# flags to _user_ programs.  I.e. not when compiling drasi itself.

# Platform dependent flags which the drasi build laboriously extract
# during build are cached, such that a user program just can get them.

# TODO: cleanup is needed!

# Assume default compiler if none given
if [ -z "$CC" ]; then
    CC=cc
fi

CC_MACHINE=`$CC -dumpmachine`
CC_VERSION=`$CC -dumpversion`
ARCH_SUFFIX=${CC_MACHINE}_${CC_VERSION}

usage()
{
    echo "Usage: drasi-config [options]"
    echo ""
    echo "  --mbscompat    add flags for MBS compatibility, give before flag options"
    echo "  --cflags       echos C compiler flags needed"
    echo "  --cflags-int   echos C compiler flags needed for internal use"
    echo "  --ldflags      echos linker flags needed"
    echo "  --libs         echos library flags needed"
    echo "  --warnflags    echos C compiler warning flags used to build libraries"
    echo "  --stamp        echos name of file to check if headers/library was rebuilt"
    echo "  --mkdir-p      echos command to perform 'mkdir -p'"
    echo "  --f-user-daq   echos name of object file for f_user_daq main function"
    echo "  --f-user-header  include path to f_user_daq.h, give before --cflags"
    echo "  --merge        use merger instead of readout, give before --libs"
    echo "  --filter       use user filter, give before --libs"
    echo "  --no-hwmap-error  do not include _hwmap_error_internal, give before --libs"
    echo "  --arch-prefix  echos the architecture string (used for directory names)"
    echo "  --has-cmvlc    echos 1 if cmvlc is used for hardware access"
    echo "  --help         prints this message"
    exit $1
}

if [ $# -eq 0 ]; then
    usage 1 1>&2
fi

SCRIPTDIR="$( cd "$( dirname "$0" )" && pwd )"

# echo directory $SCRIPTDIR

LWROC_DIR=$SCRIPTDIR/../lwroc

LIB_DIR=lib_${ARCH_SUFFIX}
BUILD_DIR=bld_${ARCH_SUFFIX}

if [ -e ${LWROC_DIR}/${LIB_DIR}/lwroc_libs.config ]
then
    . ${LWROC_DIR}/${LIB_DIR}/lwroc_libs.config
fi

CFLAGS="$CFLAGS -I ${LWROC_DIR}/gen_${ARCH_SUFFIX}"
# TODO: Adding the entire lwroc/ directory as include path is too much.
# Need to separate out parts really needed by a readout compile.
CFLAGS="$CFLAGS -I ${LWROC_DIR}"
LDFLAGS="$LDFLAGS"
# The platform support libs ($LIBS from config) must be last
LIBSDIR="-L${LWROC_DIR}/${LIB_DIR}"
LIBS_MAIN="-llwroc_main"
LIBS_READOUT="-llwroc_readout"
LIBS_NOFILTER="-llwroc_nofilter"
LIBS_MERGE="-llwroc_merge"
LIBS="-llwroc -llwroc_netutil -llwroc_parseutil \
    -L${LWROC_DIR}/../hwmap/${LIB_DIR} -lhwmap \
    $LIBS"
LIBS_HWMAP_ERROR="-llwroc_hwmap_error"
USE_HWMAP_ERROR=1

LWROC_LIBS_STAMP="${LWROC_DIR}/${LIB_DIR}/lwroc_libs.stamp \
    ${LWROC_DIR}/../hwmap/${LIB_DIR}/hwmap_libs.stamp"

while [ $# -gt 0 ]; do
    case $1 in
	--mbscompat)
	    MBSCOMPAT=1
	    ;;
	--arch-prefix)
	    OUT="$OUT ${ARCH_SUFFIX}"
	    ;;
	--cflags-int)
	    OUT="$OUT ${CFLAGS_INTERNAL}"
	    ;;
	--cflags)
	    OUT="$OUT ${CFLAGS}"
	    if [ -n "${MBSCOMPAT}" ]
	    then
		OUT="$OUT -I ${LWROC_DIR}/../mbscompat"
	    fi
	    if [ -n "${F_USER_HEADER}" ]
	    then
		OUT="$OUT -I ${LWROC_DIR}/../f_user_daq"
		OUT="$OUT ${CFLAGS_CMVLC}"
	    fi
	    ;;
	--ldflags)
	    OUT="$OUT ${LDFLAGS}"
	    ;;
	--libs)
	    OUT="$OUT ${LIBSDIR}"
	    if [ -n "${MBSCOMPAT}" ]
	    then
		OUT="$OUT -llwroc_mbscompat"
	    fi
	    OUT="$OUT ${LIBS_MAIN}"
	    if [ -n "$USE_MERGE" ]
	    then
		OUT="$OUT ${LIBS_MERGE}"
	    else
		OUT="$OUT ${LIBS_READOUT}"
	    fi
	    if [ -n "$USE_FILTER" ]
	    then
		OUT="$OUT" # nothing, user supplies filter
	    else
		OUT="$OUT ${LIBS_NOFILTER}"
	    fi
	    OUT="$OUT ${LIBS}"
	    if [ -n "$USE_HWMAP_ERROR" ]
	    then
		OUT="$OUT ${LIBS_HWMAP_ERROR}"
	    fi
	    ;;
	--warnflags)
	    OUT="$OUT ${WARN_FLAGS}"
	    ;;
	--stamp)
	    OUT="$OUT ${LWROC_LIBS_STAMP}"
	    ;;
	--mkdir-p)
	    OUT="$OUT ${MKDIR_P}"
	    ;;
	--f-user-header)
	    F_USER_HEADER=1
	    ;;
	--no-hwmap-error)
	    unset USE_HWMAP_ERROR
	    ;;
	--has-cmvlc)
	    if [ -n "${CFLAGS_CMVLC}" ]
	    then
		OUT="$OUT 1"
	    fi
	    ;;
	--f-user-daq)
	    OUT="$OUT ${LWROC_DIR}/../f_user_daq/${BUILD_DIR}/f_user_daq.o"
	    ;;
	--merge)
	    USE_MERGE=1
	    ;;
	--filter)
	    USE_FILTER=1
	    ;;
	--help|-h)
	    usage 0
	    ;;
	*)
	    echo "Unknown argument: $1" 1>&2
	    usage 1 1>&2
	    ;;
    esac
    shift
done

echo $OUT

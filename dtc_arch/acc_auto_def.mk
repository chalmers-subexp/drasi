# This file is part of DRASI - a data acquisition data pump.
#
# Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301  USA

######################################################################

MAKE_ACC_AUTO_DEF=$(DTC_ARCH_DIR)/make_acc_auto_def_$(HOST_ARCH_SUFFIX)

ifneq ($(strip $(ACC_DEF_PRINT)),)
PRINT_OPTION=--print-option
endif

# This rule is essentially never used, since
# force_make_acc_auto_def.sh will make sure the executable exists.
$(MAKE_ACC_AUTO_DEF): $(DTC_ARCH_DIR)/make_acc_auto_def.c
	@test -e $(MAKE_ACC_AUTO_DEF) && \
	  echo "$(MAKE_ACC_AUTO_DEF) needs rebuild," \
	       "but force_make_acc_auto_def.sh did not realize." && \
	  echo "Try: make clean" || \
	  echo "$(MAKE_ACC_AUTO_DEF) should have been made already!" \
	       "(by force_make_acc_auto_def.sh)"
	false

${GENDIR_ARCH}/acc_auto_def/%.h: $(DTC_ARCH_DIR)/acc_def/%.h $(MAKE_ACC_AUTO_DEF)
	@echo " ACCDEF  $@"
	@$(MKDIR_P) $(dir $@)
	$(QUIET)ACC_MAKEFLAGS="$(MAKEFLAGS)" $(MAKE_ACC_AUTO_DEF) \
	  $(PRINT_OPTION) \
	  --input $< --compiler $(CC) \
	  --compileargs $(CFLAGS) $(OPTFLAGS) > $@.tmp || \
	  (echo FAIL && echo "See: $@.tmp" 1>&2 && \
	   (test $(GITLAB_CI) && cat $@.tmp) && exit 1)
	@mv $@.tmp $@

######################################################################

# Some headers include other headers.  List those dependencies
# explicitly:

${GENDIR_ARCH}/acc_auto_def/socket_types.h: ${GENDIR_ARCH}/acc_auto_def/socket_include.h

${GENDIR_ARCH}/acc_auto_def/myusleep.h: ${GENDIR_ARCH}/acc_auto_def/time_include.h

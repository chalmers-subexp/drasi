/* This file is part of UCESB - a tool for data unpacking and processing.
 *
 * Copyright (C) 2016  GSI Helmholtzzentrum fuer Schwerionenforschung GmbH
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __BYTEORDER_INCLUDE_H__
#define __BYTEORDER_INCLUDE_H__

#ifndef ACC_DEF_RUN
# include "acc_auto_def/byteorder_include.h"
#endif

#if ACC_DEF__BYTEORDER_INCLUDE_byteorder_h
# include "byteorder.h"
#endif
#if ACC_DEF__BYTEORDER_INCLUDE_bsd_bsd_byteorder_h
# include "bsd/bsd_byteorder.h"
#endif
#if ACC_DEF__BYTEORDER_INCLUDE_endian_h
# include "endian.h"
#endif
#if ACC_DEF__BYTEORDER_INCLUDE_sys_endian_h
# include "sys/endian.h"
#endif
#if ACC_DEF__BYTEORDER_INCLUDE_bsd_ip_h
# include <sys/types.h>
# include <bsd/tcp.h>
#endif
#if ACC_DEF__BYTEORDER_INCLUDE_machine_endian_h
# ifndef _DARWIN_C_SOURCE
#  define _DARWIN_C_SOURCE
# endif
# include "machine/endian.h"
#endif

/* So far (probably) unused variants - kept for inspiration... */
#if ACC_DEF__BYTEORDER_INCLUDE_machine_byte_order_h
# include "machine/byte_order.h"
#endif
#if ACC_DEF__BYTEORDER_INCLUDE_sys_types_h_stdint_h
/* OpenBSD ?? */
#include <sys/types.h>
#include <stdint.h>
#endif
/* End (probably) unused. */

#ifndef BYTE_ORDER
# ifdef __BYTE_ORDER
#  define BYTE_ORDER     __BYTE_ORDER
#  define LITTLE_ENDIAN  __LITTLE_ENDIAN
#  define BIG_ENDIAN     __BIG_ENDIAN
# endif
#endif

#ifndef BYTE_ORDER
# ifdef __AUTO_DEF__BYTEORDER_INCLUDE_HH__
/* If BYTE_ORDER is not defined when we have the auto-generated
 * include file, then there is a problem.  Cannot check without
 * auto-generated include file, as it would warn during dependency
 * search.
 */
#  error BYTE_ORDER not defined
# endif
#endif

#ifdef ACC_DEF_RUN
void acc_test_func(void)
{
}
#endif

#endif/*__BYTEORDER_INCLUDE_H__*/

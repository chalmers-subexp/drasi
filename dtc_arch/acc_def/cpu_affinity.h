/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __CPU_AFFINITY_H__
#define __CPU_AFFINITY_H__

#ifndef ACC_DEF_RUN
# include "acc_auto_def/cpu_affinity.h"
#endif

#if ACC_DEF__CPU_AFFINITY_pthread_affinity
# define CPU_AFFINITY_PTHREAD_AFFINITY 1
# define CPU_AFFINITY_PTHREAD_AFFINITY_VAR 1
#endif
#if ACC_DEF__CPU_AFFINITY_pthread_affinity_fixed
# define CPU_AFFINITY_PTHREAD_AFFINITY 1
# define CPU_AFFINITY_PTHREAD_AFFINITY_COUNT 1
#endif
#if ACC_DEF__CPU_AFFINITY_pthread_affinity_fixed_nocount
# define CPU_AFFINITY_PTHREAD_AFFINITY 1
#endif
#if ACC_DEF__CPU_AFFINITY_notavail
/* # define CPU_AFFINITY 0 */
#endif

#ifdef ACC_DEF_RUN
# include <pthread.h>

void acc_test_func(void)
{
  int num_cpus = -1;

#if CPU_AFFINITY_PTHREAD_AFFINITY
  cpu_set_t mask;

#if CPU_AFFINITY_PTHREAD_AFFINITY_VAR
  num_cpus = CPU_COUNT_S(sizeof (mask), &mask);
#endif

#if CPU_AFFINITY_PTHREAD_AFFINITY_COUNT
  num_cpus = CPU_COUNT(&mask);
#endif

  pthread_getaffinity_np(pthread_self(), sizeof (mask), &mask);
  pthread_setaffinity_np(pthread_self(), sizeof (mask), &mask);
#endif
  (void) num_cpus;
}
#endif

#endif/*__CPU_AFFINITY_H__*/

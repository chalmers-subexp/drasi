/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __FALL_THROUGH_H__
#define __FALL_THROUGH_H__

#ifndef ACC_DEF_RUN
# include "acc_auto_def/fall_through.h"
#endif

#if ACC_DEF__FALL_THROUGH_fallthrough
# define FALL_THROUGH [[fallthrough]]
#endif
#if ACC_DEF__FALL_THROUGH_attribute_fallthrough
# define FALL_THROUGH __attribute__ ((fallthrough))
#endif
/* This should be last. */
#if ACC_DEF__FALL_THROUGH_none
# define FALL_THROUGH {}
#endif

#ifdef ACC_DEF_RUN
void foo(int x) { (void) x; }

void acc_test_func(int cond)
{
  switch (cond)
    {
    case 1:
      foo(1);
      FALL_THROUGH;
    case 2:
      foo(2);
      break;
    }
}
#endif

#endif/*__FALL_THROUGH_H__*/

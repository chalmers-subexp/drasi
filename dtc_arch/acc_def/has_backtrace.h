/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __HAS_BACKTRACE_H__
#define __HAS_BACKTRACE_H__

#ifndef ACC_DEF_RUN
# include "acc_auto_def/has_backtrace.h"
#endif

#if ACC_DEF__HAS_BACKTRACE_execinfo_h
# define HAS_BACKTRACE 1
# include <execinfo.h>
#endif
#if ACC_DEF__HAS_BACKTRACE_notavail
# define HAS_BACKTRACE 0
#endif

#ifdef ACC_DEF_RUN
void acc_test_func(void)
{
#if HAS_BACKTRACE
  int naddrs;
  void *addrs[5];
  naddrs = backtrace(addrs, 5);
  (void) naddrs;
#endif
}
#endif

#endif/*__HAS_BACKTRACE_H__*/

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __HAS_CURSES_H__
#define __HAS_CURSES_H__

/* Also see ../acc_rules/has_ncurses.mk */

#ifndef ACC_DEF_RUN
# include "acc_auto_def/has_curses.h"
#endif

#if ACC_DEF__HAS_CURSES_curses_h_term_h
# define HAS_CURSES 1
# include <curses.h>
# include <term.h>
#endif
#if ACC_DEF__HAS_CURSES_curses_h_term_h_FIXUP_wattrset
# define HAS_CURSES 1
# include <curses.h>
# include <term.h>
# define CURSES_ATTRSET_ATTR_CAST (attr_t)
#endif
#if ACC_DEF__HAS_CURSES_notavail
# define HAS_CURSES 0
#endif

#ifndef CURSES_ATTRSET_ATTR_CAST
# define CURSES_ATTRSET_ATTR_CAST
#endif

#include <string.h>

#ifdef ACC_DEF_RUN
void acc_test_func(void)
{
#if HAS_CURSES
# ifndef HAS_NCURSES_LIBS
#  error "Not trying curses compile when libraries are missing."
# endif
  int errret, ret;
  int attr = A_NORMAL;
  ret = setupterm(NULL, 1, &errret);
  tparm(tigetstr(strdup("setaf")),1);
  (void) ret;
  attrset(CURSES_ATTRSET_ATTR_CAST attr);
#else
#endif
}
#endif

#endif/*__HAS_CURSES_H__*/

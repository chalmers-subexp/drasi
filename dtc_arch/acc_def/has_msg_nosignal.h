/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2019  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __HAS_MSG_NOSIGNAL_H__
#define __HAS_MSG_NOSIGNAL_H__

#ifndef ACC_DEF_RUN
# include "acc_auto_def/has_msg_nosignal.h"
#endif

/* Define should be here, if available. */
/* Must be outside if-statements, to force compile order. */
#include "acc_auto_def/socket_include.h"
#include "socket_include.h" /* Needed for dependencies? */

#if ACC_DEF__HAS_MSG_NOSIGNAL_socket_include_h
# define HAS_MSG_NOSIGNAL 1
#endif
#if ACC_DEF__HAS_MSG_NOSIGNAL_notavail
# define HAS_MSG_NOSIGNAL 0
#endif

#ifdef ACC_DEF_RUN
void acc_test_func(void)
{
#if HAS_MSG_NOSIGNAL
  int flags = MSG_NOSIGNAL;
  (void) flags;
#endif
}
#endif

/* Define the constant, but as a 0 flag.
 * Such that using code need not do the switch.
 * And we'll actually fail if it does redefine the constant.
 */
#if !HAS_MSG_NOSIGNAL
#define MSG_NOSIGNAL 0
#endif

#endif/*__HAS_MSG_NOSIGNAL_H__*/

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __HAS_STRERROR_R_H__
#define __HAS_STRERROR_R_H__

#ifndef ACC_DEF_RUN
# include "acc_auto_def/has_strerror_r.h"
#endif

#if ACC_DEF__HAS_STRERROR_R_string_h_ret_int
# define HAS_STRERROR_R 1
# define STRERROR_R_RET_CHAR_PTR 0
# include <string.h>
#endif
#if ACC_DEF__HAS_STRERROR_R_string_h_ret_char_ptr
# define HAS_STRERROR_R 1
# define STRERROR_R_RET_CHAR_PTR 1
# include <string.h>
#endif
#if ACC_DEF__HAS_STRERROR_R_notavail
# define HAS_STRERROR_R 0
#endif

#ifdef ACC_DEF_RUN
void acc_test_func(void)
{
#if HAS_STRERROR_R
  int errnum = 0;
  char buf[256];
# if STRERROR_R_RET_CHAR_PTR
  char *ret =
# else
  int ret =
# endif
  strerror_r(errnum, buf, sizeof (buf));
  (void) ret;
#endif
}
#endif

#endif/*__HAS_STRERROR_R_H__*/

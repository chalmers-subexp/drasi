/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2019  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __HAS_VALLOC_H__
#define __HAS_VALLOC_H__

#ifndef ACC_DEF_RUN
# include "acc_auto_def/has_valloc.h"
#endif

#if ACC_DEF__HAS_VALLOC_stdlib_h
# define HAS_VALLOC 1
# include <stdlib.h>
#endif
#if ACC_DEF__HAS_VALLOC_notavail
# define HAS_VALLOC 0
#endif

#ifdef ACC_DEF_RUN
void acc_test_func(void)
{
#if HAS_VALLOC
  void *ptr = valloc(1);
  free(ptr);
#endif
}
#endif

#endif/*__HAS_VALLOC_H__*/

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __HAS_VSNPRINTF_H__
#define __HAS_VSNPRINTF_H__

#ifndef ACC_DEF_RUN
# include "acc_auto_def/has_vsnprintf.h"
#endif

#if ACC_DEF__HAS_VSNPRINTF_stdio_h_stdarg_h
# define HAS_VSNPRINTF 1
# include <stdio.h>
# include <stdarg.h>
#endif
#if ACC_DEF__HAS_VSNPRINTF_notavail
# define HAS_VSNPRINTF 0
#endif

#ifdef ACC_DEF_RUN
void acc_test_func(
#if HAS_VSNPRINTF
		   va_list ap
#endif
		   )
{
#if HAS_VSNPRINTF
  char buf[256];
  vsnprintf(buf, sizeof (buf), "fmt", ap);
#endif
}
#endif

#endif/*__HAS_VSNPRINTF_H__*/

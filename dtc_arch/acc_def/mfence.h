/* This file is part of UCESB - a tool for data unpacking and processing.
 *
 * Copyright (C) 2016  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __MYMFENCE_H__
#define __MYMFENCE_H__

#ifndef ACC_DEF_RUN
# include "acc_auto_def/mfence.h"
#endif

/* These constructs are necessary for correct operation when the code in
 * question operates using several threads (in SMP mode).
 */

/* See the linux kernel, include/asm-<arch>/system.h, search for
 * mb/wmb/rmb (we are only accessing memory, no devices)
 */

#if ACC_DEF__MFENCE_ARCH__x86_64__mfence
# if defined (__x86_64__)
#  include <cpuid.h>
/* Careful here: cpuid bit 26 must be set (sse2, or we have no mfence). */
/* #  ifdef HAS_XMM2 */
#  define MFENCE_CHECK(fatal_report_macro) do {				\
    unsigned int eax, ebx, ecx, edx = 0; /* Silence compiler. */	\
    if (__get_cpuid_max(0, NULL) < 1) {					\
      fatal_report_macro("CPUID not available, cannot check MFENCE.");	\
    }									\
    if (__get_cpuid(1, &eax, &ebx, &ecx, &edx) < 1) {			\
      fatal_report_macro("CPUID failed, cannot check MFENCE.");		\
    }									\
    if (!(edx & (1 << 26))) {						\
      fatal_report_macro("CPUID(1) reports 0 in bit 26 of edx, "	\
			 "MFENCE not available.");			\
    }									\
  } while (0)
#  define MFENCE __asm volatile ("    mfence  \n\t" : : : "memory")
#  define SFENCE __asm volatile ("    sfence  \n\t" : : : "memory")
#  define LFENCE __asm volatile ("    lfence  \n\t" : : : "memory")
# endif
#endif

#if ACC_DEF__MFENCE_ARCH__i386__lock_addl
# if defined (__i386__)
#  define MFENCE __asm volatile ("    lock; addl $0,0(%%esp)  \n\t" : : : \
				 "memory")
# elif defined (__x86_64__)
/* Use as fallback when CPUID wrappers are not available. */
#  define MFENCE __asm volatile ("    lock; addl $0,0(%%rsp)  \n\t" : : : \
				 "memory")
# endif
#endif

#if ACC_DEF__MFENCE_ARCH__powerpc__eieio
# ifdef __powerpc__
#  define MFENCE __asm volatile ("    eieio  \n\t" : : : "memory")
# endif
#endif

#if ACC_DEF__MFENCE_ARCH__arm__dmb
# if defined(__arm__) || defined(__arm64__)
#  define MFENCE __asm volatile ("    dmb  \n\t" : : : "memory")
# endif
#endif

#if ACC_DEF__MFENCE_ARCH__aarch64__dmb
# if defined(__AARCH64EL__) || defined(__AARCH64EB__)
#  define MFENCE __asm volatile ("    dmb sy\n\t" : : : "memory")
# endif
#endif

#if ACC_DEF__MFENCE_ARCH__arm__kuser_dmb_t /* If dmb above is not available. */
# include <stdint.h>
# if defined(__arm__) || defined(__arm64__)
/* See http://www.kernel.org/doc/Documentation/arm/kernel_user_helpers.txt */
#  define __kuser_helper_version (*(int32_t *)0xffff0ffc)
#  define MFENCE_CHECK(fatal_report_macro) do {				\
    if (!(__kuser_helper_version >= 3)) {				\
      fatal_report_macro("kuser_memory_barrier not available, "		\
			 "kernel too old.");				\
    }									\
  } while (0)
typedef void (__kuser_dmb_t)(void);
#  define MFENCE do {					      \
    (*(__kuser_dmb_t *)0xffff0fa0)();			      \
  } while (0)
# endif
#endif

#if ACC_DEF__MFENCE_ARCH__sparc__xxx
# ifdef __sparc__
#  define MFENCE __asm volatile ("" : : : "memory")
# endif
#endif

#if ACC_DEF__MFENCE_ARCH__alpha__mb
# ifdef __alpha__
#  define MFENCE __asm volatile ("    mb    \n\t" : : : "memory")
#  define SFENCE __asm volatile ("    wmb   \n\t" : : : "memory")
# endif
#endif

#ifndef SFENCE
# define SFENCE MFENCE
#endif

#ifndef LFENCE
# define LFENCE MFENCE
#endif

#ifndef MFENCE_CHECK
# define MFENCE_CHECK(fatal_report_macro) do { } while (0)
#endif

#ifdef ACC_DEF_RUN
#include <stdlib.h>
#include <stdio.h>
#define fatal_macro(str) do {			\
    fprintf(stderr, "%s", str);			\
    exit(0);					\
  } while (0)
void acc_test_func(void)
{
  int a, b, c;

  MFENCE_CHECK(fatal_macro);
  *(&a) = 1;
  MFENCE;
  *(&b) = 2;
  SFENCE;
  *(&c) = 3;
  LFENCE;
  *(&c) = *(&a);
}
#endif

#endif/*__MFENCE_H__*/

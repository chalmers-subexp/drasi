/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __MYINTTYPES_H__
#define __MYINTTYPES_H__

#ifndef ACC_DEF_RUN
# include "acc_auto_def/myinttypes.h"
#endif

#if ACC_DEF__MYINTTYPES_inttypes_h
# include <inttypes.h>
#endif
/* This is a local fall-back solution, so should be last. */
#if ACC_DEF__MYINTTYPES_myinttypes_long_long_64
/* Assume 32 bit ints.  Which is not unreasonable for a system with
 * such an old compiler that inttypes does not take care.  We just
 * define what we need.
 */
#endif
#if ACC_DEF__MYINTTYPES_myinttypes_long_64
/* Assume 64 bit ints.  Less likely for a system with an old compiler.
 * We just define what we need.
 */
#endif

#if ACC_DEF__MYINTTYPES_myinttypes_long_long_64 || \
  ACC_DEF__MYINTTYPES_myinttypes_long_64

#ifndef PRIdPTR /* incomplete inttypes.h */
#define PRIdPTR "d"
#define PRIiPTR "i"
#define PRIoPTR "o"
#define PRIuPTR "u"
#define PRIxPTR "x"
#define PRIXPTR "X"
#endif

#ifndef PRId8
#define PRId8   "d"
#define PRIi8   "i"
#define PRIo8   "o"
#define PRIu8   "u"
#define PRIx8   "x"
#define PRIX8   "X"
#endif

#ifndef PRId16
#define PRId16  "d"
#define PRIi16  "i"
#define PRIo16  "o"
#define PRIu16  "u"
#define PRIx16  "x"
#define PRIX16  "X"
#endif

#ifndef PRId32
#define PRId32  "d"
#define PRIi32  "i"
#define PRIo32  "o"
#define PRIu32  "u"
#define PRIx32  "x"
#define PRIX32  "X"
#endif

#endif

#if ACC_DEF__MYINTTYPES_myinttypes_long_long_64

#ifndef PRId64
#define PRId64  "lld"
#define PRIi64  "lli"
#define PRIo64  "llo"
#define PRIu64  "llu"
#define PRIx64  "llx"
#define PRIX64  "llX"
#endif

#endif

#if ACC_DEF__MYINTTYPES_myinttypes_long_64

#ifndef PRId64
#define PRId64  "ld"
#define PRIi64  "li"
#define PRIo64  "lo"
#define PRIu64  "lu"
#define PRIx64  "lx"
#define PRIX64  "lX"
#endif

#endif

#ifdef ACC_DEF_RUN
#include <stdio.h>

# ifdef ACC_DEF__MYINTTYPES_myinttypes_long_64
# include <limits.h>
/* We check that the limits.h definition of LONG max matches expectation. */
#  if LONG_MAX != 2147483647L
#   error "LONG_MAX is not 2147483647L"
#  endif
# endif

# ifdef ACC_DEF__MYINTTYPES_myinttypes_long_long_64
# include <limits.h>
#  if LONG_MAX != 9223372036854775807LL
#   error "LLONG_MAX is not 9223372036854775807LL"
#  endif
# endif

void acc_test_func(void)
{
  /* We should really try with uint32_t and friends.  To make sure
   * the compiler is happy about the conversions...
   *
   * But since that also needs auto-id...  (which needs further
   * tricks to ensure the order between...)
   */

  /* We just ensure that the constants are defined. */

  printf ("%s", PRIdPTR);
  printf ("%s", PRIi8);
  printf ("%s", PRId16);
  printf ("%s", PRIu32);
  printf ("%s", PRIx64);
}
#endif

#endif/*__MYINTTYPES_H__*/

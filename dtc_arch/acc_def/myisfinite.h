/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __MYISFINITE_H__
#define __MYISFINITE_H__

#ifndef ACC_DEF_RUN
# include "acc_auto_def/myisfinite.h"
#endif

#if ACC_DEF__MYISFINITE_math_h
# include <math.h>
#endif
#if ACC_DEF__MYISFINITE_finite_ieeefp_h
# include <ieeefp.h>
# define isfinite finite
#endif
/* To be last - deals with broken clang: faulting valid double arguments. */
#if ACC_DEF__MYISFINITE_math_h_double_conversion
# include <math.h>
# pragma clang diagnostic push
# pragma clang diagnostic ignored "-Wconversion"
static int myisfinite(double x)
{
  return isfinite(x);
}
# pragma clang diagnostic pop
# undef isfinite
# define isfinite(x) myisfinite(x)
#endif

#ifdef ACC_DEF_RUN
void acc_test_func(void)
{
  float valf = 0.;
  double vald = 0.;
  int retf;
  int retd;
  retf = isfinite(valf);
  retd = isfinite(vald);
  (void) retf;
  (void) retd;
}
#endif

#endif/*__MYISFINITE_H__*/

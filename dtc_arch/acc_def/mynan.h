/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __MYNAN_H__
#define __MYNAN_H__

#ifndef ACC_DEF_RUN
# include "acc_auto_def/mynan.h"
#endif

#if ACC_DEF__MYNAN_math_h
# include <math.h>
#endif
#if ACC_DEF__MYNAN_by_sqrtf
# include <math.h>
# define NAN (fabsf(sqrtf(-1.f)))
#endif
#if ACC_DEF__MYNAN_by_sqrt
# include <math.h>
# define NAN ((float) fabs(sqrt(-1.)))
#endif

#ifdef ACC_DEF_RUN
void acc_test_func(void)
{
  float valf = NAN;
  double vald = NAN;
  (void) valf;
  (void) vald;
}
#endif

#endif/*__MYNAN_H__*/

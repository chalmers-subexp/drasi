/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __MYPRIZD_H__
#define __MYPRIZD_H__

#ifndef ACC_DEF_RUN
# include "acc_auto_def/myprizd.h"
#endif

/* Not all compilers have printf knowing %zd (for size_t).
 * or %td (for ptrdiff_t).  So simply try a few...
 */

/* This would be the correct way, so do that first. */
#if ACC_DEF__MYPRIZD_normal
#define MYPRIzd "zd"
#define MYPRIzx "zx"
#define MYPRItd "td"
#endif
/* Some systems have problems with ssize_t, while size_t works.
 * Even though d is signed...  To not change all zd into zu, we add
 * an internal moniker s for the ssize_t cases.
 */
#if ACC_DEF__MYPRIZD_normal_signed
#define MYPRIzd "zd"
#define MYPRIszd "d"
#define MYPRIzx "zx"
#define MYPRItd "td"
#endif
/* And then variations on attempts. */
#if ACC_DEF__MYPRIZD_int_int
#define MYPRIzd "d"
#define MYPRIzx "x"
#define MYPRItd "d"
#endif
#if ACC_DEF__MYPRIZD_int_long
#define MYPRIzd "d"
#define MYPRIzx "x"
#define MYPRItd "ld"
#endif
#if ACC_DEF__MYPRIZD_long_long
#define MYPRIzd "ld"
#define MYPRIzx "lx"
#define MYPRItd "ld"
#endif

#ifndef MYPRIszd
# define MYPRIszd MYPRIzd
#endif

#ifdef ACC_DEF_RUN
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void acc_test_func(int *a, int *b)
{
  printf ("zd: %"MYPRIzd"\n" , sizeof (a));
  printf ("zd: %"MYPRIzd"\n" , (size_t) sizeof (a));
  printf ("zd: %"MYPRIszd"\n" , /* returns ssize_t */ read(0, NULL, 0));
  printf ("td: %"MYPRItd"\n" , b - a);
}
#endif

#endif/*__MYPRIZD_H__*/

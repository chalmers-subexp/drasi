/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __MYSTDINT_H__
#define __MYSTDINT_H__

#ifndef ACC_DEF_RUN
# include "acc_auto_def/mystdint.h"
#endif

#if ACC_DEF__MYSTDINT_stdint_h
# include <stdint.h>
#endif
/* This is a local fall-back solution, so should be last. */
#if ACC_DEF__MYSTDINT_mystdint
/* The below assumes 32 bit ints.  Which is not unreasonable for a
 * system with such an old compiler.  We just define what we need.
 */

#include <sys/types.h>

typedef u_int8_t  uint8_t;
typedef u_int16_t uint16_t;
typedef u_int32_t uint32_t;
typedef u_int64_t uint64_t;
#endif

#ifndef UINT64_C
/* When not defined, assume such an old system is 32 bits. */
# define UINT64_C(v) v##ULL
#endif

#ifdef ACC_DEF_RUN
uint64_t acc_test_func(void)
{
  uint8_t  t8  = 0;
  uint16_t t16 = 0;
  uint32_t t32 = 0;
  uint64_t t64; /* Some compiler could not handle the init here. */

  t64 = 0;

  return ((uint32_t) (t8 + t16)) + t32 + t64;
}
#endif

#endif/*__MYSTDINT_H__*/

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __MYSTRNDUP_H__
#define __MYSTRNDUP_H__

#ifndef ACC_DEF_RUN
# include "acc_auto_def/mystrndup.h"
#endif

#if ACC_DEF__MYSTRNDUP_string_h
# include <string.h>
#endif
/* This is a local fall-back solution, so should be last. */
#if ACC_DEF__MYSTRNDUP_mystrndup
# include <stdlib.h>
static char *mystrndup(const char *src,size_t length);
# define strndup mystrndup
#define IMPLEMENT_MYSTRNDUP 1
#endif

#ifdef IMPLEMENT_MYSTRNDUP
# include <string.h>
static char *mystrndup(const char *src,size_t length)
{
  /* We waste memory in case the string actually is shorter... */
  char *dest = (char *) malloc(length+1);
  if (!dest)
    return NULL;
  strncpy(dest,src,length);
  dest[length]='\0'; /* since strncpy would not handle this */
  return dest;
}
#endif

#ifdef ACC_DEF_RUN
void acc_test_func(void)
{
  char *str;
  str = strndup("", 0);
  (void) str;
}
#endif

#endif/*__MYSTRNDUP_H__*/

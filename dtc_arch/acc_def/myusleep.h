/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __MYUSLEEP_H__
#define __MYUSLEEP_H__

#ifndef ACC_DEF_RUN
# include "acc_auto_def/myusleep.h"
#endif

#if ACC_DEF__MYUSLEEP_unistd_h
# include <unistd.h>
#endif
/* This is a local fall-back solution, so should be last. */
#if ACC_DEF__MYUSLEEP_myusleep
# include <stdlib.h>
static int usleep(int usec);
# define usleep myusleep
# define IMPLEMENT_USLEEP 1
#endif

#ifdef IMPLEMENT_USLEEP
# ifndef ACC_DEF_RUN
#  include "time_include.h"
# endif
static int myusleep(int us)
{
# ifndef ACC_DEF_RUN
  struct timespec rqtp;

  rqtp.tv_sec  = us / 1000000;
  rqtp.tv_nsec = 1000 * (us % 1000000);

  return nanosleep(&rqtp,NULL);
# else
  /* We have to implement a simpler version, since including
   * time_include.h makes a major mess.
   */
  (void) us;
# endif
}
#endif

#ifdef ACC_DEF_RUN
void acc_test_func(void)
{
  usleep(0);
}
#endif

#endif/*__MYUSLEEP_H__*/

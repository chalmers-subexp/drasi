/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __RESOURCE_INCLUDE_H__
#define __RESOURCE_INCLUDE_H__

#ifndef ACC_DEF_RUN
# include "acc_auto_def/resource_include.h"
#endif

#if ACC_DEF__RESOURCE_INCLUDE_resource_h
# include <resource.h>
#endif
#if ACC_DEF__RESOURCE_INCLUDE_sys_resource_h
# include <sys/resource.h>
#endif

#ifdef ACC_DEF_RUN_RESOURCE_INCLUDE
void acc_test_func(void)
{
}
#endif

#endif/*__RESOURCE_INCLUDE_H__*/

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __SOCKET_INCLUDE_H__
#define __SOCKET_INCLUDE_H__

#ifndef ACC_DEF_RUN
# include "acc_auto_def/socket_include.h"
#endif

#if ACC_DEF__SOCKET_INCLUDE_socket_h
# include <socket.h>
#endif
#if ACC_DEF__SOCKET_INCLUDE_sys_socket_h
# include <sys/socket.h>
#endif

#ifdef ACC_DEF_RUN_SOCKET_INCLUDE
void acc_test_func(void)
{
}
#endif

#endif/*__SOCKET_INCLUDE_H__*/

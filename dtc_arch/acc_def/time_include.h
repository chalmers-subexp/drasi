/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __TIME_INCLUDE_H__
#define __TIME_INCLUDE_H__

#ifndef ACC_DEF_RUN
# include "acc_auto_def/time_include.h"
#endif

#if ACC_DEF__TIME_INCLUDE_time_h
# include <time.h>
#endif
#if ACC_DEF__TIME_INCLUDE_sys_time_h
# include <sys/time.h>
#endif
#if ACC_DEF__TIME_INCLUDE_time_h_sys_time_h
# include <time.h>
# include <sys/time.h>
#endif
#if ACC_DEF__TIME_INCLUDE_time_h_FIXUP_timercmp
# include <time.h>
# define DO_TIMERCMP_FIXUP
#endif
#if ACC_DEF__TIME_INCLUDE_time_h_FIXUP_timercmp_DEF_timeraddsub
# include <time.h>
# define DO_TIMERCMP_FIXUP
# define DO_TIMERADDSUB_DEF
#endif
#if ACC_DEF__TIME_INCLUDE_sys_time_h_FIXUP_timercmp
# include <sys/time.h>
# define DO_TIMERCMP_FIXUP
#endif

#ifdef DO_TIMERADDSUB_DEF
/* Declare when found missing. */
#define timeradd(a, b, r)  do {				\
    (r)->tv_sec  = (a)->tv_sec + (b)->tv_sec;		\
    (r)->tv_usec = (a)->tv_usec + (b)->tv_usec;		\
    if ((r)->tv_usec >= 1000000) {			\
      (r)->tv_sec++;					\
      (r)->tv_usec -= 1000000;				\
    }							\
  } while (0)
#define timersub(a, b, r)  do {			\
    (r)->tv_sec  = (a)->tv_sec - (b)->tv_sec;	\
    (r)->tv_usec = (a)->tv_usec - (b)->tv_usec;	\
    if ((r)->tv_usec < 0) {			\
      (r)->tv_sec--;				\
      (r)->tv_usec += 1000000;			\
    }						\
  } while (0)
#endif

#ifdef DO_TIMERCMP_FIXUP
/* Not enough parenthesis in timercmp...  Causes compile warnings. */
# undef timercmp
# define timercmp(a, b, cmp_op)			\
  (((a)->tv_sec  ==     (b)->tv_sec ) ?		\
   ((a)->tv_usec cmp_op (b)->tv_usec) :		\
   ((a)->tv_sec  cmp_op (b)->tv_sec ))
#endif

#ifdef ACC_DEF_RUN
# include <stddef.h>
void acc_test_func(void)
{
  time_t t;
  struct timeval tv, tv2, tv3;
  struct timespec tp;

  t = time(NULL);
  gettimeofday(&tv, NULL);
  clock_settime(CLOCK_REALTIME, &tp);

  gettimeofday(&tv2, NULL);
  (void) timercmp(&tv, &tv2, <);

  timeradd(&tv, &tv2, &tv3);
  timersub(&tv, &tv2, &tv3);

  timerclear(&tv);
  (void) timerisset(&tv);

  (void) t;
}
#endif

#endif/*__TIME_INCLUDE_H__*/

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __TYPEDEF_FUNC_ATTRIB_H__
#define __TYPEDEF_FUNC_ATTRIB_H__

#ifndef ACC_DEF_RUN
# include "acc_auto_def/typedef_func_attrib.h"
#endif

#if ACC_DEF__TYPEDEF_FUNC_ATTRIB_1
# define DO_TYPEDEF_FUNC_ATTRIB 1
#endif
/* This is less strict, so should be last. */
#if ACC_DEF__TYPEDEF_FUNC_ATTRIB_0
# define DO_TYPEDEF_FUNC_ATTRIB 0
#endif

#ifdef ACC_DEF_RUN
typedef void (printf_ptr)(const char *fmt,...)
# if DO_TYPEDEF_FUNC_ATTRIB
  __attribute__ ((__format__ (__printf__, 1, 2)));
# endif
  ;
#endif

#endif/*__TYPEDEF_FUNC_ATTRIB_H__*/

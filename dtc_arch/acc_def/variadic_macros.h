/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __VARIADIC_MACROS_H__
#define __VARIADIC_MACROS_H__

#ifndef ACC_DEF_RUN
# include "acc_auto_def/variadic_macros.h"
#endif

#if ACC_DEF__VARIADIC_MACROS_c11
# define VARIADIC_MACROS_c11
#endif
/* This should be last. */
#if ACC_DEF__VARIADIC_MACROS_old
# define VARIADIC_MACROS_OLD
#endif

#ifdef ACC_DEF_RUN

#include <stdio.h>

#ifdef VARIADIC_MACROS_OLD
#define VARIADIC_TEST(x,__VA_ARGS__...) \
  do { printf (x, __VA_ARGS__); } while (0)
#else
#define VARIADIC_TEST(x,...) \
  do { printf (x, __VA_ARGS__); } while (0)
#endif

void acc_test_func(void)
{
  VARIADIC_TEST("fmt-string %d %d", 1, 2);
}
#endif

#endif/*__VARIADIC_MACROS_H__*/

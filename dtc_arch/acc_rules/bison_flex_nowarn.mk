# This file is part of DRASI - a data acquisition data pump.
#
# Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301  USA

# There are at least two known problems with this rule:
# The rule builder does not propagate $(YACC) or $(LEX),
# so it tries with the wrong commands.  And even if it tries the
# right commands, it does the comparisons wrongly!

$(error This file is not working!)

ifdef ACC_DEF_RUN
include $(DTC_ARCH_DIR)/arch_suffix_inc.mk
endif

######################################################################

# If bison or flex is too old (or known troublesome), switch compiler
# warnings off.

ifdef ACC_DEF__BISON_FLEX_NOWARN_0
BISON_FLEX_NOWARN = 0
endif

ifdef ACC_DEF__BISON_FLEX_NOWARN_1
BISON_FLEX_NOWARN = 1
endif

######################################################################

ifdef ACC_DEF_RUN_BISON_FLEX_NOWARN
FLEX_VERSION  := $(shell $(LEX)  -V | head -n 1 | sed -e "s/[^0-9]*//")
BISON_VERSION := $(shell $(YACC) -V | head -n 1 | sed -e "s/[^0-9]*//")

# TODO: this does NOT work.  Comparing would have to be for exact numbers.
# E.g. 2.5.39 compares as less than 2.5.5, which we do not want...

FLEX_BAD  := $(shell test "$(FLEX_VERSION)" \< "2.5.5" && echo bad) # 2.5.4 bad
BISON_BAD := $(shell test "$(BISON_VERSION)" \< "1.20" && echo bad) # 1.19 bad

#$(warning $(FLEX_VERSION))
#$(warning $(BISON_VERSION))
#$(warning $(FLEX_BAD))
#$(warning $(BISON_BAD))

all:

endif

######################################################################

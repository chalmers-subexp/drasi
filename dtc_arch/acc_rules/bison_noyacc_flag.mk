# This file is part of DRASI - a data acquisition data pump.
#
# Copyright (C) 2022  Haakan T. Johansson  <f96hajo@chalmers.se>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301  USA

# There are at least two known problems with this rule:
# The rule builder does not propagate $(YACC) or $(LEX),
# so it tries with the wrong commands.  And even if it tries the
# right commands, it does the comparisons wrongly!

ifdef ACC_DEF_RUN
include $(DTC_ARCH_DIR)/arch_suffix_inc.mk

-include $(GENDIR_ARCH)/acc_auto_def/mkdir_p.mk
-include $(DTC_ARCH_DIR)/acc_rules/mkdir_p.mk
endif

######################################################################

# If bison takes the -Wno-yacc warning, then use it.
# Since we *do* want to use bison extensions, that should then *not* cause
# failures.
ifdef ACC_DEF__BISON_NOYACC_FLAG_Wnoyacc
BISON_NOYACC_FLAG = -Wno-yacc
endif

# Without the command-line option.  For other yacc implementations.
# Then causes bison to give warnings, so not preferred.
ifdef ACC_DEF__BISON_NOYACC_FLAG_none
BISON_NOYACC_FLAG =
endif

# This rule such that this test can build even if no YACC is present.
# Useful for projects that does not need it.  (Otherwise: a dead end.)
ifdef ACC_DEF__BISON_NOYACC_FLAG_missing
BISON_NOYACC_ATALL = || true
# There is no bison at all...
ACC_DEF_VAR__BISON_NOYACC_FLAG_missing_PENALTY = 1
endif

######################################################################

ifdef ACC_DEF_RUN_BISON_NOYACC_FLAG
all: $(BUILD_TEST_DIR)/test_$(RULE_FILE)

$(BUILD_TEST_DIR)/test_$(RULE_FILE):
	$(MKDIR_P) $(dir $@)
	cat $(DTC_ARCH_DIR)/acc_rules/test_bison_noyacc_flag.y | \
	  grep -v "parse.error" > $@.tmp.y
	$(YACC) $(BISON_NOYACC_FLAG) \
	  -o $@.c $@.tmp.y > /dev/null \
	  $(BISON_NOYACC_ATALL)
	touch $@.c # Handle case of no $(YACC) at all.
	$(CC) -o $@ -c $@.c
endif

######################################################################

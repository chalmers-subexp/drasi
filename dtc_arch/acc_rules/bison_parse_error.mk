# This file is part of DRASI - a data acquisition data pump.
#
# Copyright (C) 2022  Haakan T. Johansson  <f96hajo@chalmers.se>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301  USA

# The purpose of this rule is to find out if the current $(YACC)
# supports a statement:
#
# %define parse.error verbose
#
# or if a statement
#
# %error-verbose
#
# is accepted.  If neither, then none is used.
#
# Furthermore, in  case the  newer (first) option  is accepted,  it is
# wanted to not copy the .y file, but to make a symlink, such that any
# compile errors  point to  the actual source  file.  That  is helpful
# during development.  Thus  the roundabout way of  handling that case
# in the fixup script.

ifdef ACC_DEF_RUN
include $(DTC_ARCH_DIR)/arch_suffix_inc.mk

-include $(GENDIR_ARCH)/acc_auto_def/mkdir_p.mk
-include $(DTC_ARCH_DIR)/acc_rules/mkdir_p.mk
-include $(GENDIR_ARCH)/acc_auto_def/bison_noyacc_flag.mk
-include $(DTC_ARCH_DIR)/acc_rules/bison_noyacc_flag.mk
endif

######################################################################

# The modern version, '%define parse.error verbose'.
ifdef ACC_DEF__BISON_PARSE_ERROR_define
BISON_PARSE_ERROR = --keep
endif

# When '%define parse.error verbose' is not supported, try '%error-verbose'.
ifdef ACC_DEF__BISON_PARSE_ERROR_old_error_verbose
BISON_PARSE_ERROR = --error-verbose
endif

# When neither of the above are supported, remove.
ifdef ACC_DEF__BISON_PARSE_ERROR_noparseerror
BISON_PARSE_ERROR = --remove
endif

# This rule such that this test can build even if no YACC is present.
# Useful for projects that does not need it.  (Otherwise: a dead end.)
ifdef ACC_DEF__BISON_PARSE_ERROR_missing
BISON_NOYACC_ATALL = || true
BISON_PARSE_ERROR = --keep   # Keep fixup script happy.
# There is no bison at all...
ACC_DEF_VAR__BISON_PARSE_ERROR_missing_PENALTY = 1
endif

######################################################################

ifdef ACC_DEF_RUN_BISON_PARSE_ERROR
all: $(BUILD_TEST_DIR)/test_$(RULE_FILE)

$(BUILD_TEST_DIR)/test_$(RULE_FILE):
	$(MKDIR_P) $(dir $@)
	$(DTC_ARCH_DIR)/acc_rules/bison_parse_error_fixup.sh \
	  $(BISON_PARSE_ERROR) \
	  $(DTC_ARCH_DIR)/acc_rules/test_bison_noyacc_flag.y $@.tmp.y
	$(YACC) $(BISON_NOYACC_FLAG) \
	  -o $@.c $@.tmp.y > /dev/null \
	  $(BISON_NOYACC_ATALL)
	touch $@.c # Handle case of no $(YACC) at all.
	$(CC) -o $@ -c $@.c
endif

######################################################################

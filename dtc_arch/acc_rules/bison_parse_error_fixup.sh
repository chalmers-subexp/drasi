#!/bin/sh

set -e

OPT=$1
INPUT=$2
OUTPUT=$3

# echo "FIXUP: script: $0"      1>&2
# echo "FIXUP: opt:    $OPT"    1>&2
# echo "FIXUP: input:  $INPUT"  1>&2
# echo "FIXUP: output: $OUTPUT" 1>&2

case $OPT in
    --keep)
	# First remove the output.  In case it is a symlink and the
	# symlinking fails, the copy may destroy the input.
	rm -f $OUTPUT
	# Try to make a relative symlink.  If ln does not know that,
	# fall back to make a copy of the file.
	ln -f -r -s $INPUT $OUTPUT || \
	    cp $INPUT $OUTPUT
	;;

    --error-verbose)
	cat $INPUT | sed -e "s/.*parse.error.*/%error-verbose/" > $OUTPUT
	;;

    --remove)
	cat $INPUT | grep -v "parse.error" > $OUTPUT
	;;

    *)
	echo "Unhandled option '$OPT' for $0." 1>&2
	exit 1;
	;;
esac

# echo "--- ls $OUTPUT"
# ls $OUTPUT
# echo "--- cat $OUTPUT"
# cat $OUTPUT
# echo "---"

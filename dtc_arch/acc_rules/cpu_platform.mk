# This file is part of DRASI - a data acquisition data pump.
#
# Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301  USA

ifdef ACC_DEF_RUN
include $(DTC_ARCH_DIR)/arch_suffix_inc.mk

-include $(GENDIR_ARCH)/acc_auto_def/mkdir_p.mk
-include $(DTC_ARCH_DIR)/acc_rules/mkdir_p.mk
endif

######################################################################

ifdef ACC_DEF__CPU_PLATFORM_Lynx
LEX      := flex
DTC_CFLAGS   += -DLynx -mthreads
DTC_LDFLAGS  += -mthreads
DTC_LIBS     += -lm -lnetinet
PLATFORM_LIBS += -llynx -lnetinet
endif

ifdef ACC_DEF__CPU_PLATFORM_Linux
DTC_CFLAGS   += -D_GNU_SOURCE
DTC_LIBS     += -lpthread -lrt
DTC_LIBS     += -lm
endif

ifdef ACC_DEF__CPU_PLATFORM_Mac
DTC_CFLAGS   += -D_GNU_SOURCE
DTC_LIBS     += -lpthread
DTC_LIBS     += -lm
endif

ifdef ACC_DEF_RUN_CPU_PLATFORM
CFLAGS  += $(DTC_CFLAGS)
LDFLAGS += $(DTC_LDFLAGS)
LIBS    += $(DTC_LIBS)

all: $(BUILD_TEST_DIR)/test_$(RULE_FILE)

$(BUILD_TEST_DIR)/test_$(RULE_FILE).o: $(DTC_ARCH_DIR)/acc_rules/test_$(RULE_FILE).c
	$(MKDIR_P) $(dir $@)
	$(CC) $(CFLAGS) -o $@ -c $<

$(BUILD_TEST_DIR)/test_$(RULE_FILE): $(BUILD_TEST_DIR)/test_$(RULE_FILE).o
	$(MKDIR_P) $(dir $@)
	$(CC) -o $@ $< $(LIBS)
endif

######################################################################

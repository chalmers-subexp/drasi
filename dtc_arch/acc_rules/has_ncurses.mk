# This file is part of DRASI - a data acquisition data pump.
#
# Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301  USA

ifdef ACC_DEF_RUN
include $(DTC_ARCH_DIR)/arch_suffix_inc.mk

-include $(GENDIR_ARCH)/acc_auto_def/mkdir_p.mk
-include $(DTC_ARCH_DIR)/acc_rules/mkdir_p.mk
endif

######################################################################

# Also see ../acc_def/has_curses.h

ifdef ACC_DEF__HAS_NCURSES_ncursesw6_config
NCURSES_CONFIG = ncursesw6-config
endif

ifdef ACC_DEF__HAS_NCURSES_ncursesw6_config_ltermcap
NCURSES_CONFIG = ncursesw6-config
NCURSES_EXTRA_LIB = -ltermcap
endif

ifdef ACC_DEF__HAS_NCURSES_ncurses6_config
NCURSES_CONFIG = ncurses6-config
endif

ifdef ACC_DEF__HAS_NCURSES_ncurses6_config_ltermcap
NCURSES_CONFIG = ncurses6-config
NCURSES_EXTRA_LIB = -ltermcap
endif

ifdef ACC_DEF__HAS_NCURSES_ncursesw5_config
NCURSES_CONFIG = ncursesw5-config
endif

ifdef ACC_DEF__HAS_NCURSES_ncursesw5_config_ltermcap
NCURSES_CONFIG = ncursesw5-config
NCURSES_EXTRA_LIB = -ltermcap
endif

ifdef ACC_DEF__HAS_NCURSES_ncurses5_config
NCURSES_CONFIG = ncurses5-config
endif

ifdef ACC_DEF__HAS_NCURSES_ncurses5_config_ltermcap
NCURSES_CONFIG = ncurses5-config
NCURSES_EXTRA_LIB = -ltermcap
endif

ifdef ACC_DEF__HAS_NCURSES_ncurses
DTC_CFLAGS    += -DHAS_NCURSES_LIBS
DTC_LIBS      += -lncurses
endif

ifdef ACC_DEF__HAS_NCURSES_none
ACC_DEF_VAR__HAS_NCURSES_none_PENALTY = 1
endif

######################################################################

ifdef NCURSES_CONFIG
TEST_NCURSES_CONFIG = $(NCURSES_CONFIG) --cflags --libs > /dev/null
DTC_CFLAGS    += -DHAS_NCURSES_LIBS `$(NCURSES_CONFIG) --cflags`
DTC_LIBS      += `$(NCURSES_CONFIG) --libs`
endif

ifdef NCURSES_EXTRA_LIB
DTC_LIBS      += $(NCURSES_EXTRA_LIB)
endif

######################################################################

ifdef ACC_DEF_RUN
CFLAGS  += $(DTC_CFLAGS)
LDFLAGS += $(DTC_LDFLAGS)
LIBS    += $(DTC_LIBS)

all: $(BUILD_TEST_DIR)/test_$(RULE_FILE)

$(BUILD_TEST_DIR)/test_$(RULE_FILE).o: $(DTC_ARCH_DIR)/acc_rules/test_$(RULE_FILE).c
	$(MKDIR_P) $(dir $@)
	$(TEST_NCURSES_CONFIG)
	$(CC) $(CFLAGS) -o $@ -c $<

$(BUILD_TEST_DIR)/test_$(RULE_FILE): $(BUILD_TEST_DIR)/test_$(RULE_FILE).o
	$(MKDIR_P) $(dir $@)
	$(CC) -o $@ $< $(LIBS)
endif

######################################################################

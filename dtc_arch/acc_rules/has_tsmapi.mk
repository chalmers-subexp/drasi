# This file is part of DRASI - a data acquisition data pump.
#
# Copyright (C) 2019  Hans T. Toernqvist   <h.toernqvist@gsi.de>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301  USA

ifdef ACC_DEF_RUN
include $(DTC_ARCH_DIR)/arch_suffix_inc.mk

-include $(GENDIR_ARCH)/acc_auto_def/mkdir_p.mk
-include $(DTC_ARCH_DIR)/acc_rules/mkdir_p.mk
endif

######################################################################

# Also see ../acc_def/has_tsmapi.h

ifdef ACC_DEF__HAS_TSMAPI_tsmapi
DTC_CFLAGS  += -I$(TSM_INC_PATH) -I$(LTSM_LIB_PATH)
DTC_LDFLAGS += -L$(TSM_LIB_PATH) -L$(LTSM_LIB_PATH)
DTC_LIBS    += -ltsmapi -lApiTSM -lz
endif

ifdef ACC_DEF__HAS_TSMAPI_tsmapi64
DTC_CFLAGS  += -I$(TSM_INC_PATH) -I$(LTSM_LIB_PATH)
DTC_LDFLAGS += -L$(TSM_LIB_PATH) -L$(LTSM_LIB_PATH)
DTC_LIBS    += -ltsmapi -lApiTSM64 -lz
endif

ifdef ACC_DEF__HAS_TSMAPI_none
ACC_DEF_VAR__HAS_TSMAPI_none_PENALTY = 1
endif

ifdef ACC_DEF_RUN
CFLAGS  += $(DTC_CFLAGS)
LDFLAGS += $(DTC_LDFLAGS)
LIBS    += $(DTC_LIBS)

# $(info $(CFLAGS))
# $(info $(LDFLAGS))
# $(info $(LIBS))

all: $(BUILD_TEST_DIR)/test_$(RULE_FILE)

$(BUILD_TEST_DIR)/test_$(RULE_FILE).o: $(DTC_ARCH_DIR)/acc_rules/test_$(RULE_FILE).c
	$(MKDIR_P) $(dir $@)
	$(CC) $(CFLAGS) -o $@ -c $<

$(BUILD_TEST_DIR)/test_$(RULE_FILE): $(BUILD_TEST_DIR)/test_$(RULE_FILE).o
	$(MKDIR_P) $(dir $@)
	$(CC) -o $@ $< $(LDFLAGS) $(LIBS)
endif

######################################################################

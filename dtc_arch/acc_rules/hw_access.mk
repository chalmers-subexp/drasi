# This file is part of DRASI - a data acquisition data pump.
#
# Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301  USA

ifdef ACC_DEF_RUN
include $(DTC_ARCH_DIR)/arch_suffix_inc.mk

-include $(GENDIR_ARCH)/acc_auto_def/mkdir_p.mk
-include $(DTC_ARCH_DIR)/acc_rules/mkdir_p.mk

# May be good to have any platform libs when trying hw access!
-include $(GENDIR_ARCH)/acc_auto_def/cpu_platform.mk
-include $(DTC_ARCH_DIR)/acc_rules/cpu_platform.mk
endif

ACC_DEF__HW_ACCESS_showconfig=1  # Show with 'make showconfig'.

######################################################################

ifdef ACC_DEF__HW_ACCESS_ces_find_controller
DTC_CFLAGS    += -DHAS_FINDCONTROLLER=1
DTC_LIBS      += -L/lib/ces -lvme -lbma -luio
endif

ifdef ACC_DEF__HW_ACCESS_Linux_XPC_3_3_10
DTC_CFLAGS    += -DHAS_XPC_3_3_10=1
DTC_CFLAGS    += -I /usr/include/ces/cesXpcLib -I /usr/include/ces/cesOsApi
DTC_LIBS      += -lcesXpcLib
endif

ifdef ACC_DEF__HW_ACCESS_Linux_XPC_3_2_3
DTC_CFLAGS    += -DHAS_XPC_3_2_3=1
endif

ifdef ACC_DEF__HW_ACCESS_Linux_UNIVERSE
DTC_CFLAGS    += -DHAS_UNIVERSE=1
VMESTUFFDIR = $(HOME)/vme_stuff
DTC_CFLAGS    += -I $(VMESTUFFDIR)/include
DTC_LIBS      += -L $(VMESTUFFDIR)/lib -lvme -Wl,-rpath $(VMESTUFFDIR)/lib
endif

ifdef ACC_DEF__HW_ACCESS_Linux_Rimfaxe_AVB # AXI VME bridge
DTC_CFLAGS    += -DHAS_RIMFAXE_AVB=1
DTC_LIBS      += -l avb
endif

ifdef ACC_DEF__HW_ACCESS_Linux_IFC_ALTHEA # ioxos IFC with Althea bridge
ALTHEA_DIR    := /mbs/driv/ifc/althea/ALTHEA7910
DTC_CFLAGS    += -DHAS_ALTHEA=1
DTC_CFLAGS    += -I$(ALTHEA_DIR)/include
DTC_LIBS      += -L$(ALTHEA_DIR)/lib -l alt
endif

ifdef ACC_DEF__HW_ACCESS_CAENVME
DTC_CFLAGS    += -DHAS_CAENVME=1 -DHWMAP_RW_FUNC=1
DTC_CFLAGS    += -I$(CAENLIB_DIR)/include
DTC_LIBS      += -L$(CAENLIB_DIR)/lib/x64 -l CAENVME -Wl,-rpath=$(CAENLIB_DIR)/lib/x64
endif

ifdef ACC_DEF__HW_ACCESS_MVLCC
ifeq (,$(MVLCC_CONFIG))
MVLCC_CONFIG = $(shell which mvlcc-config.sh)
endif
DTC_CFLAGS    += -DHAS_MVLCC=1 -DHWMAP_RW_FUNC=1
DTC_CFLAGS    += $(shell $(MVLCC_CONFIG) --cflags)
DTC_LIBS      += $(shell $(MVLCC_CONFIG) --ldflags --libs)

DTC_CCLD      = $(CXX)
endif

ifdef ACC_DEF__HW_ACCESS_CMVLC
ifeq (,$(CMVLC_CONFIG))
CMVLC_CONFIG = $(shell which cmvlc-config.sh)
endif
CMVLC_CFLAGS  += -DHAS_CMVLC=1 -DHWMAP_RW_FUNC=1
CMVLC_CFLAGS  += $(shell $(CMVLC_CONFIG) --cflags)

DTC_CFLAGS    += $(CMVLC_CFLAGS)
DTC_LIBS      += $(shell $(CMVLC_CONFIG) --ldflags --libs)
endif

# After introduction of AVB on Rimfaxe, this is not really needed.
# But keeping, since implementation is rather generic.
ifdef ACC_DEF__HW_ACCESS_Linux_Zynq_mmap
DTC_CFLAGS    += -DHAS_ZYNQ_MMAP=1
ACC_DEF_VAR__HW_ACCESS_Linux_Zynq_mmap_PENALTY = 1
endif

ifdef ACC_DEF__HW_ACCESS_none
# Penalty such that this option (which cannot map) becomes fallback.
ACC_DEF_VAR__HW_ACCESS_none_PENALTY = 2
endif

ifdef ACC_DEF_RUN
CFLAGS  += $(DTC_CFLAGS)
LDFLAGS += $(DTC_LDFLAGS)
LIBS    += $(DTC_LIBS)

CCLD     = $(DTC_CCLD)
# If no-one set/requested the linker, use $(CC).
ifeq (,$(CCLD))
CCLD     = $(CC)
endif

all: $(BUILD_TEST_DIR)/test_$(RULE_FILE)

ifdef ACC_DEF__HW_ACCESS_Linux_Zynq_mmap
test_zynq:
	cat /proc/cpuinfo | grep Hardware | grep Zynq > /dev/null

$(BUILD_TEST_DIR)/test_$(RULE_FILE).o: test_zynq
endif

$(BUILD_TEST_DIR)/test_$(RULE_FILE).o: $(DTC_ARCH_DIR)/acc_rules/test_$(RULE_FILE).c
	$(MKDIR_P) $(dir $@)
	$(CC) $(CFLAGS) -o $@ -c $<

$(BUILD_TEST_DIR)/test_$(RULE_FILE): $(BUILD_TEST_DIR)/test_$(RULE_FILE).o
	$(MKDIR_P) $(dir $@)
	$(CCLD) -o $@ $< $(LIBS)
endif

######################################################################

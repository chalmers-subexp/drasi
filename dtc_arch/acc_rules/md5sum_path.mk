# This file is part of DRASI - a data acquisition data pump.
#
# Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301  USA

ifdef ACC_DEF_RUN
include $(DTC_ARCH_DIR)/arch_suffix_inc.mk
endif

######################################################################

ifdef ACC_DEF__MD5SUM_PATH_INC_bywhich
MD5SUM := $(shell which md5sum)
endif

ifdef ACC_DEF__MD5SUM_PATH_INC_bywhich_md5
MD5SUM := $(shell which md5)
endif

ifdef ACC_DEF__MD5SUM_PATH_INC_md5sum
MD5SUM := md5sum
endif

ifdef ACC_DEF__MD5SUM_PATH_INC_missing
# When md5sum is used as a build rule, the generated files are generally
# architecture independent.  So a missing md5sum can be circumvented
# by first compiling on another platform
MD5SUM := echo md5sum_is_missing
# Penalty such that this option becomes fallback.
ACC_DEF_VAR__MD5SUM_PATH_INC_missing_PENALTY = 1
endif

ifdef ACC_DEF_RUN
all: $(BUILD_TEST_DIR)/test_$(RULE_FILE)

$(BUILD_TEST_DIR)/test_$(RULE_FILE):
	$(MD5SUM) /dev/null > /dev/null
endif

######################################################################

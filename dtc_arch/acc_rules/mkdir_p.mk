# This file is part of DRASI - a data acquisition data pump.
#
# Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301  USA

ifdef ACC_DEF_RUN
include $(DTC_ARCH_DIR)/arch_suffix_inc.mk
endif

######################################################################

ifdef ACC_DEF__MKDIR_P_mkdir_p
MKDIR_P = mkdir -p
endif

ifdef ACC_DEF__MKDIR_P_mkdir_fp
MKDIR_P = mkdir -fp
endif

######################################################################

ifdef ACC_DEF_RUN_MKDIR_P
all: $(BUILD_TEST_DIR)/test_$(RULE_FILE)

$(BUILD_TEST_DIR)/test_$(RULE_FILE):
	$(MKDIR_P) $(dir $@)
	$(MKDIR_P) $(dir $@) # make it twice to handle that case correctly
	touch $@
endif

######################################################################

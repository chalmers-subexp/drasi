/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

/* This test program is to be kept very simple.
 *
 * It's purpose is to figure out which compiler and linker options
 * are required on the target platform, by testing different recipes.
 *
 * We would like if it can work on all platforms, without having to do
 * the other acc_def rules first.  Since those depend on the outcome
 * of the decision for compiler and linker flags.
 */

/* We do not really test the ncurses implementation.
 * Just that the linker can find a library.
 *
 */

#if HAS_NCURSES_LIBS
#include <curses.h>
#include <term.h>
#include <string.h>
#endif

int main(int argc, char *argv[])
{
#if HAS_NCURSES_LIBS
  int errret, ret;
  ret = setupterm(NULL, 1, &errret);
  tparm(tigetstr(strdup("setaf")),1);
  (void) ret;
#endif
  (void) argc;
  (void) argv;

  return 0;
}

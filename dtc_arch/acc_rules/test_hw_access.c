/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

/* This test program is to be kept very simple.
 *
 * It's purpose is to figure out which compiler and linker options
 * are required on the target platform, by testing different recipes.
 *
 * We would like if it can work on all platforms, without having to do
 * the other acc_def rules first.  Since those depend on the outcome
 * of the decision for compiler and linker flags.
 */

#include <stdlib.h>

#if HAS_FINDCONTROLLER
# include <sys/types.h>
# include <smem.h>
# include <ces/vmelib.h>

/* FIXME: find right include file. */
unsigned long find_controller(unsigned long,
			      unsigned long,
			      unsigned long,
			      int,int,
			      struct pdparam_master *);
#endif

#if HAS_UNIVERSE
# include <vme/vme.h>
# include <vme/vme_api.h>
#endif

#if HAS_RIMFAXE_AVB
# include <rimfaxe/avb.h>
#endif

#if HAS_XPC_3_2_3
# include <ces/xpc.h>
# include <ces/xpc_vme.h>
# include <ces/xpc_dma.h>
#endif

#if HAS_XPC_3_3_10
# include <ces/CesXpcBridge.h>
# include <ces/CesXpcBridge_Vme.h>
#endif

#if HAS_ALTHEA
# include <stdint.h>
# include <altmasioctl.h>
# include <altulib.h>
#endif

#if HAS_CAENVME
# include <CAENVMElib.h>
#endif

#if HAS_MVLCC
# include <mvlcc_wrap.h>
#endif

#if HAS_CMVLC
# include <cmvlc.h>
#endif

int main(int argc, char *argv[])
{
#if HAS_FINDCONTROLLER
  {
    find_controller(0,0,0,0,0,NULL);
  }
#endif

#if HAS_UNIVERSE
  {
    vme_bus_handle_t bus_handle;

    vme_init(&bus_handle);
  }
#endif

#if HAS_RIMFAXE_AVB
  {
    struct avb *avb;
    int dummy = avb_init(&avb);
    (void) dummy;
  }
#endif

#if HAS_ALTHEA
  {
    int alt = alt_init();
    struct alt_ioctl_vme_mas_conf mas;
    (void) alt;
    (void) mas;
  }
#endif

#if HAS_XPC_3_2_3
  {
    /* check that variable is defined (used in ioctl call) */
    int dummy = XPC_DMA_CHAN_MODIFY;
    (void) dummy;
  }
#endif

#if HAS_XPC_3_3_10
  {
    int dummy = XPC_VME_A32_STD_USER;
    (void) dummy;
  }
#endif

#if HAS_CAENVME
  {
    CVBoardTypes board_type = cvV1718;
    short conet_node = 0;
    short link_number = 0;
    int32_t handle;
    CVErrorCodes ret =
      CAENVME_Init(board_type, conet_node, link_number, &handle);
    (void) ret;
  }
#endif

#if HAS_MVLCC
  {
    mvlcc_t mvlcc = mvlcc_make_mvlc_eth("");
    int ec = mvlcc_connect(mvlcc);
    (void) ec;
  }
#endif

#if HAS_CMVLC
  {
    struct cmvlc_client *cmvlc = cmvlc_connect("", 0, NULL, NULL);
    (void) cmvlc;
  }
#endif

#if HAS_FINDCONTROLLER
#elif HAS_UNIVERSE
#elif HAS_XPC_3_2_3
#elif HAS_XPC_3_3_10
#else
  /* No way to map, but has penalty in the hw_access.mk. */
#endif

  (void) argc;
  (void) argv;

  return 0;
}

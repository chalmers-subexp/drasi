/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

/* This test program is to be kept very simple.
 *
 * Its purpose is to figure out which compiler and linker options
 * are required on the target platform, by testing different recipes.
 *
 * We would like if it can work on all platforms, without having to do
 * the other acc_def rules first.  Since those depend on the outcome
 * of the decision for compiler and linker flags.
 */

short func_arg_short(short x)
{
  return x;
}

int main(int argc, char *argv[])
{
  (void) argc;
  (void) argv;

  /* gcc before 4.3 with -Wconversion gives warnings about htons (or
   * any other function called with a < int argument even if it is
   * correct according to the prototype.  Use short in testing instead
   * of uint16_t as the latter requires stdint.h.
   */
  {
    short a = 1, b;

    b = func_arg_short(a);

    (void) b;
  }

  return 0;
}

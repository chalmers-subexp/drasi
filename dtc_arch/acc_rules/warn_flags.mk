# This file is part of DRASI - a data acquisition data pump.
#
# Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301  USA

ifdef ACC_DEF_RUN
include $(DTC_ARCH_DIR)/arch_suffix_inc.mk

-include $(GENDIR_ARCH)/acc_auto_def/mkdir_p.mk
-include $(DTC_ARCH_DIR)/acc_rules/mkdir_p.mk
endif

######################################################################

WARN_FLAGS :=

ifdef ACC_DEF__WARN_FLAGS_INC_most
ACC_DEF_USE_WARN_FLAGS_INC_most = 1
ACC_DEF_USE_WARN_FLAGS_INC_base = 1
endif

ifdef ACC_DEF__WARN_FLAGS_INC_base
ACC_DEF_USE_WARN_FLAGS_INC_base = 1
# Penalty for not doing -Wconversion
ACC_DEF_VAR__WARN_FLAGS_INC_base_PENALTY = 1
endif

ifdef ACC_DEF__WARN_FLAGS_INC_few
ACC_DEF_USE_WARN_FLAGS_INC_few = 1
endif

ifdef ACC_DEF__WARN_FLAGS_INC_most_no_float_opt
ACC_DEF_USE_WARN_FLAGS_INC_most = 1
ACC_DEF_USE_WARN_FLAGS_INC_base = 1
# Penalty for not doing full -Wconversion
ACC_DEF_VAR__WARN_FLAGS_INC_most_no_float_opt_PENALTY = 2
# FreeBSD 9 cannot handle -Wno-error=float-conversion
ACC_DEF_USE_WARN_FLAGS_no_float_opt = 1
endif

ifdef ACC_DEF_USE_WARN_FLAGS_INC_most
WARN_FLAGS += -Wconversion
endif

ifdef ACC_DEF_USE_WARN_FLAGS_INC_base
WARN_FLAGS += -std=gnu99 -Wall -W -Wshadow -Wwrite-strings -Werror -Wno-unused-function -Wno-unused-label -Wstrict-prototypes -Wdeclaration-after-statement -Wcast-qual
endif

ifdef ACC_DEF_USE_WARN_FLAGS_INC_few
WARN_FLAGS += -ansi -Wall -W -Wshadow -Wwrite-strings # -Werror # -Wconversion # -Wno-unused-function -Wno-unused-label # -Wcast-qual
endif


# Ability to disable troublesome warning from command line.
# (It is triggered by normal use of FD_SET et al on some platforms.)
ifdef NO_WERROR_SIGN_CONVERSION
WARN_FLAGS += -Wno-error=sign-conversion
endif

ifdef NO_WERROR_CONVERSION
WARN_FLAGS += -Wno-error=conversion
endif

ifdef NO_WERROR_FLOAT_CONVERSION
ifndef ACC_DEF_USE_WARN_FLAGS_no_float_opt
WARN_FLAGS += -Wno-error=float-conversion
endif
endif

ifdef ACC_DEF_RUN
CFLAGS  += $(DTC_CFLAGS)
LDFLAGS += $(DTC_LDFLAGS)
LIBS    += $(DTC_LIBS)

CFLAGS += $(WARN_FLAGS)

all: $(BUILD_TEST_DIR)/test_$(RULE_FILE)

$(BUILD_TEST_DIR)/test_$(RULE_FILE).o: $(DTC_ARCH_DIR)/acc_rules/test_$(RULE_FILE).c
	$(MKDIR_P) $(dir $@)
	$(CC) $(CFLAGS) -o $@ -c $<

$(BUILD_TEST_DIR)/test_$(RULE_FILE): $(BUILD_TEST_DIR)/test_$(RULE_FILE).o
	$(MKDIR_P) $(dir $@)
	$(CC) -o $@ $< $(LIBS)
endif

######################################################################

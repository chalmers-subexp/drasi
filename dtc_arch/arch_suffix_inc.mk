# This file is part of DRASI - a data acquisition data pump.
#
# Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301  USA

# Suggestion from Hans:
# (he also has an appended _$(BUILD_MODE), which may be set to 'debug')

CC_MACHINE  := $(shell $(CC) -dumpmachine)
CC_VERSION  := $(shell $(CC) -dumpversion)
ARCH_SUFFIX := $(CC_MACHINE)_$(CC_VERSION)

# If cross compiling, native executable should be qualified by the
# relevant compiler.

ifneq (,$(HOSTCC))
HOSTCC_MACHINE  := $(shell $(HOSTCC) -dumpmachine)
HOSTCC_VERSION  := $(shell $(HOSTCC) -dumpversion)
HOST_ARCH_SUFFIX := $(HOSTCC_MACHINE)_$(HOSTCC_VERSION)
else
HOST_ARCH_SUFFIX := $(ARCH_SUFFIX)
endif

# Old style:
#
# ARCH_SUFFIX := $(shell uname -s)_$(shell uname -m)
#
# ifneq ($(GSI_CPU_PLATFORM),)
# ARCH_SUFFIX := $(ARCH_SUFFIX)_$(GSI_CPU_PLATFORM)
# endif

GENDIR = gen
GENDIR_ARCH = ${GENDIR}_${ARCH_SUFFIX}

# This file is part of DRASI - a data acquisition data pump.
#
# Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301  USA

include $(DTC_ARCH_DIR)/arch_suffix_inc.mk

######################################################################

# Order is important:

# mkdir_p_inc       is used by all(most) other rules
# cpu_platform_inc  is used by  hw_access_inc

BUILD_RULES = mkdir_p warn_flags cpu_platform hw_access \
	perl_path md5sum_path has_ncurses has_tsmapi \
	bison_noyacc_flag bison_parse_error
#	bison_flex_nowarn

ifndef ACC_DEF_RUN
# Before including the file, we must ensure it exist!
# We cannot use -include, as other files that are needed in the build
# (e.g. auto-dependencies) depend on the flags that we are about
# to determine.  So they must be correct on the first run!

# Construct it inside the shell script.  Force execution of the shell
# script by assigning a dummy variable.

ifneq (clean,$(MAKECMDGOALS))
ifneq (clean-all,$(MAKECMDGOALS))
ifndef FORCE_DTC_ARCH_DIR
ACC_AUTO_DEF_CPU_PLATFORM_INC_FORCE := \
	$(foreach rule_file, $(BUILD_RULES), \
		  $(shell $(DTC_ARCH_DIR)/force_make_acc_auto_def.sh \
			  $(MAKE) \
			  $(DTC_ARCH_DIR) \
			  $(HOST_ARCH_SUFFIX) \
			  $(GENDIR_ARCH)/build_test \
			  $(rule_file) \
			  $(GENDIR_ARCH)/acc_auto_def/$(rule_file).mk \
			  $(DTC_ARCH_TEMPLATE_DIR)))
ifneq (,$(findstring FAIL,$(ACC_AUTO_DEF_CPU_PLATFORM_INC_FORCE)))
$(error Build rule failed - cannot continue!)
endif
include $(foreach rule_file, $(BUILD_RULES), \
		  $(GENDIR_ARCH)/acc_auto_def/$(rule_file).mk)
endif
endif
endif
endif

######################################################################

include $(foreach rule_file, $(BUILD_RULES), \
		  $(DTC_ARCH_DIR)/acc_rules/$(rule_file).mk)

CFLAGS  += $(DTC_CFLAGS)
LDFLAGS += $(DTC_LDFLAGS)
LIBS    += $(DTC_LIBS)

CCLD     = $(DTC_CCLD)
# If no-one set/requested the linker, use $(CC).
ifeq (,$(CCLD))
CCLD     = $(CC)
endif

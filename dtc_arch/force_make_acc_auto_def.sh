#!/bin/sh

# This file is part of DRASI - a data acquisition data pump.
#
# Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301  USA

MAKE=$1
DTC_ARCH_DIR=$2
HOST_ARCH_SUFFIX=$3
BUILD_TEST_DIR_BASE=$4
RULE_FILE=$5
ACC_AUTO_DEF_RULE_INC=$6
RULE_TEMPLATE_DIR=$7

RULE_INC_MK=${DTC_ARCH_DIR}/acc_rules/${RULE_FILE}.mk
TEST_RULE_INC_C=${DTC_ARCH_DIR}/acc_rules/test_${RULE_FILE}.c

MAKE_ACC_AUTO_DEF=${DTC_ARCH_DIR}/make_acc_auto_def_${HOST_ARCH_SUFFIX}
MAKE_ACC_AUTO_DEF_SRC=${DTC_ARCH_DIR}/make_acc_auto_def.c

# This overcomplicated way of saying
# [ ! -e .. -o .. -nt .. -o ( ! .. -nt .. -a ! .. -ot .. ) ]
# is probably not needed (it can be combined).  But since it works...

# The reason for doing ! -nt -a ! -ot is that some shells only have second
# precision in -nt, and then misses if they have almost the same time.
# But make may notice the difference, and then executes the
# (catch non-rempiled) rule in acc_auto_def.mk...

RECOMP=
if [ ! -e ${MAKE_ACC_AUTO_DEF} ]                          ; then RECOMP=1 ; fi
if [ ${MAKE_ACC_AUTO_DEF_SRC} -nt ${MAKE_ACC_AUTO_DEF} ]  ; then RECOMP=1 ; fi
if [ ! ${MAKE_ACC_AUTO_DEF_SRC} -nt ${MAKE_ACC_AUTO_DEF} -a \
     ! ${MAKE_ACC_AUTO_DEF_SRC} -ot ${MAKE_ACC_AUTO_DEF} ] ; then RECOMP=2 ; fi
# echo "Recompile? $RECOMP" 1>&2
if [ -n "$RECOMP" ]
then
    CC_LOCAL=${HOSTCC}
    CC_LOCAL_NAME=" HOSTCC "
    if [ ! ${CC_LOCAL} ] ; then CC_LOCAL=${CC} ;
				CC_LOCAL_NAME="   CC   " ; fi
    if [ ! ${CC_LOCAL} ] ; then CC_LOCAL=cc ; fi
    echo "${CC_LOCAL_NAME} ${MAKE_ACC_AUTO_DEF}" 1>&2
    ${CC_LOCAL} ${MAKE_ACC_AUTO_DEF_SRC} -o ${MAKE_ACC_AUTO_DEF} \
		-DWITH_SETENV ||
    (echo " ... trying without WITH_SETENV" 1>&2 && \
    ${CC_LOCAL} ${MAKE_ACC_AUTO_DEF_SRC} -o ${MAKE_ACC_AUTO_DEF}) ||
	(echo FAIL && \
	 echo "Failed compiling ${MAKE_ACC_AUTO_DEF} with ${CC_LOCAL}" 1>&2 && \
	 exit 1)
fi

set -e

PRINT_OPTION=
if [ -n "$ACC_DEF_PRINT" ]
then
    PRINT_OPTION=--print-option
fi

if [ ! -e ${ACC_AUTO_DEF_RULE_INC} -o \
    ${RULE_INC_MK} -nt ${ACC_AUTO_DEF_RULE_INC} -o \
    ${TEST_RULE_INC_C} -nt ${ACC_AUTO_DEF_RULE_INC} -o \
    ${MAKE_ACC_AUTO_DEF} -nt ${ACC_AUTO_DEF_RULE_INC} ]
then
    ACC_AUTO_DEF_DIR=`dirname ${ACC_AUTO_DEF_RULE_INC}`

    TEMPLATE_RULE_INC=${RULE_TEMPLATE_DIR}/${ACC_AUTO_DEF_RULE_INC}
    # Do we have a pre-built rule, that is fresh enough?
    if [ -e ${TEMPLATE_RULE_INC} ]
    then
	if [ ${RULE_INC_MK} -nt ${TEMPLATE_RULE_INC} -o \
	    ${TEST_RULE_INC_C} -nt ${TEMPLATE_RULE_INC} -o \
	    ${MAKE_ACC_AUTO_DEF} -nt ${TEMPLATE_RULE_INC} ]
	then
	    : # null command - failed to reverse condition...
	else
	    echo "COPYRULE ${ACC_AUTO_DEF_RULE_INC}" 1>&2

	    # Same command as below, duplicate to have COPYRULE first
	    # We try all commands available to create the directory
	    mkdir -p ${ACC_AUTO_DEF_DIR} 2> /dev/null || \
		mkdir -fp ${ACC_AUTO_DEF_DIR} 2> /dev/null

	    cp ${TEMPLATE_RULE_INC} \
	       ${ACC_AUTO_DEF_RULE_INC} || \
		(echo FAIL && \
		 echo "Failed to copy ${TEMPLATE_RULE_INC}" 1>&2 && exit 1)
	    exit 0
	fi
    fi

    # Tell what we are doing
    echo " ACCRULE ${ACC_AUTO_DEF_RULE_INC}" 1>&2

    # We try all commands available to create the directory
    mkdir -p ${ACC_AUTO_DEF_DIR} 2> /dev/null || \
	mkdir -fp ${ACC_AUTO_DEF_DIR} 2> /dev/null

    # Kill the result file, such that a new failure to find
    # a working option stops the build
    rm -f ${ACC_AUTO_DEF_RULE_INC}

    # And generate the file
    ${MAKE_ACC_AUTO_DEF} \
	${PRINT_OPTION} \
	--input ${RULE_INC_MK} \
	--build-test-dir-base ${BUILD_TEST_DIR_BASE} \
	--ismake --compiler ${MAKE} \
	--compileargs DTC_ARCH_DIR=${DTC_ARCH_DIR} RULE_FILE=${RULE_FILE} \
	> ${ACC_AUTO_DEF_RULE_INC}.tmp &&
    mv ${ACC_AUTO_DEF_RULE_INC}.tmp \
	${ACC_AUTO_DEF_RULE_INC} || \
	(echo FAIL 1>&2 && echo "See: ${ACC_AUTO_DEF_RULE_INC}.tmp" 1>&2 && \
	     (test ${GITLAB_CI} && \
		     cat ${ACC_AUTO_DEF_RULE_INC}.tmp 1>&2) && \
	     exit 1)
fi

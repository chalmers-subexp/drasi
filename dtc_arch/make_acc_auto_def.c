/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#define _POSIX_C_SOURCE  200809L  /* For -ansi */

/* #define _BSD_SOURCE   1 */     /* Gives deprecation warning, thus: */
#define _DEFAULT_SOURCE  1        /* For strdup. */
#if __GNUC__<= 4                  /* But on fairly seasoned systems... */
#define _BSD_SOURCE               /* ... for strdup. */
#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>

/* Rules for choosing an option:
 *
 * 1. Use the first working option (i.e. that at all compiles).
 * 2. If a later option also works and has less penalty,
 *    use that instead.
 * 2. If a later option also works and gives less text output,
 *    and has the same penalty, use that instead.
 */

/* Variables ('basename' is the name of the input file before a dot):
 *
 * ACC_DEF__basename_xxx                - An option 'xxx'.
 * ACC_DEF_VAR__basename_xxx_PENALTY n  - A penalty specification for 'xxx'.
 * ACC_DEF__basename_showconfig         - Option is important - mark it for
 *                                        easy find by scripts.
 */

/* This was originally a perl script.
 * But running a compiler inside a perl job was too much for some
 * memory challenged machines...
 * And then a shell script.
 * That also created problems...
 * (Except for that, C is not an improvement!  Tripled in size :-( )
 */

/* The program is compiled before we know what special include file
 * options etc are used on the platform.  Thus, we use very simple
 * constructions that work in all (even remotely sane) cases.
 *
 * Since even that is not always enough to compile cleanly, we will in
 * default mode ignore all warnings from the compilation of this
 * program.  As long is it compiles and makes an executable, it is a
 * success!
 */

/* For stricter checking during development compilation:

gcc -std=gnu99 -Wall -W -Wshadow -Wwrite-strings -Werror \
  -Wno-unused-function -Wno-unused-label -Wconversion \
  dtc_arch/make_acc_auto_def.c -o dtc_arch/make_acc_auto_def

 */

struct option_t
{
  const char       *name;
  int               penalty;
  struct option_t  *next;

  /* Local variables for parallel execution. */

  char            **call_argv;
  const char       *comp_opt; /* Remember the argument strdup'd argument. */
  pid_t             cpid;
  int               parent_stdouterr;
};

#define CMT_BEGIN(fmt_str)		 \
  do {					 \
    fputs (begin_comment_line, stdout);	 \
    printf fmt_str;			 \
    fputs ("\n", stdout);		 \
  } while (0)

#define CMT__TEXT(fmt_str)		\
  do {					\
    fputs (cont_comment_line, stdout);	\
    printf fmt_str;			\
    fputs ("\n", stdout);		\
  } while (0)

#define CMT__NONL(fmt_str)		\
  do {					\
    fputs (cont_comment_line, stdout);	\
    printf fmt_str;			\
  } while (0)

#define CMT_BLANK			\
  do {					\
    fputs (cont_comment_line, stdout);	\
    fputs ("\n", stdout);		\
  } while (0)

#define BLANK_LINE			\
  do {					\
    fputs ("\n", stdout);		\
  } while (0)

#define MEM_ALLOC_NULL_STDERR_EXIT(ptr)				\
  do {								\
    if ((ptr) == NULL)						\
      {								\
	fprintf (stderr, "%s:%d: Memory allocation failure.\n",	\
		 __FILE__, __LINE__);				\
	exit(EXIT_FAILURE);					\
      }								\
  } while (0)

char *strdup_chk(const char *s)
{
  char *ret = strdup(s);
  MEM_ALLOC_NULL_STDERR_EXIT(ret);
  return ret;
}

int mangle_comment(char *line, const char *mark)
{
  /* We mangle by inserting a space after the first character of the mark. */

  char *p;

  p = strstr(line, mark);

  if (p == NULL)
    return 0;

  memmove(p+2, p+1, strlen(p+1)+1);
  p[1] = ' ';

  return 1;
}

char *kill_argument(char *opts, const char *killarg)
{
  char *findarg;
  char *findarg_end;
  char *newopts;

  findarg = strstr(opts, killarg);

  if (!findarg)
    return opts;

  findarg_end = findarg + strlen(killarg);

  while (*findarg_end &&
	 *findarg_end != ' ' &&
	 *findarg_end != '\t' &&
	 *findarg_end != '\n')
    findarg_end++;

  /* strndup is not always available.  Use strdup. */
  newopts = strdup_chk(opts);
  strcpy(newopts + (findarg - opts),
	 findarg_end);

  return newopts;
}

int main(int argc, char *argv[])
{
  /* Command line inputs. */

  char *inputfile = NULL;
  char *compiler = NULL;
  char **compileargs = NULL;
  const char *build_test_dir_base = NULL;
  int   ismake = 0;
  int   print_option = 0;

  /* Derived. */

  char *basename = NULL;
  size_t num_compileargs = 0;

  const char *begin_comment_line = "/*";
  const char *cont_comment_line  = " *";

  /* Options found in file. */

  struct option_t *options = NULL;
  size_t max_opt_len = 0;

  /* Chosen option. */

  const char *working = NULL;
  int         working_penalty = 0;
  size_t      working_output_length = 0;
  int         show_config_important = 0;  /* For user info / showconfig. */

  /* For parallel running. */

  int max_jobs = 1;

  /* Parse command line. */

  {
    int iarg;

    for (iarg = 1; iarg < argc; iarg++)
      {
#define MATCH_ARG(name) (strcmp(argv[iarg],name) == 0)
#define MATCH_ARG2(name) (strcmp(argv[iarg],name) == 0 && iarg+1 < argc)

	if (MATCH_ARG("--ismake"))
	  {
	    ismake = 1;
	  }
	else if (MATCH_ARG2("--input"))
	  {
	    inputfile = argv[++iarg];
	  }
	else if (MATCH_ARG2("--compiler"))
	  {
	    compiler = argv[++iarg];
	  }
	else if (MATCH_ARG2("--build-test-dir-base"))
	  {
	    build_test_dir_base = argv[++iarg];
	  }
	else if (MATCH_ARG("--compileargs"))
	  {
	    compileargs = &argv[iarg + 1];
	    /* All following arguments are compiler flags. */
	    num_compileargs = (size_t) (argc - (iarg + 1));
	    break;
	  }
	else if (MATCH_ARG("--print-option"))
	  {
	    print_option = 1;
	  }
	else
	  {
	    fprintf (stderr, "Invalid option (or missing argument): %s",
		     argv[iarg]);
	    exit(EXIT_FAILURE);
	  }
      }
  }

  /* Check command line options. */

  if (!inputfile)
    {
      fprintf (stderr, "No input file specified (--input FILE).\n");
      exit(EXIT_FAILURE);
    }

  if (!compiler)
    {
      fprintf (stderr, "No compiler specified (--compiler PROG).\n");
      exit(EXIT_FAILURE);
    }

  if (!compileargs)
    {
      fprintf (stderr, "No compiler arguments (--compileargs [ARG ...]).\n");
      exit(EXIT_FAILURE);
    }

  if (ismake)
    {
      if (!build_test_dir_base)
	{
	  fprintf (stderr, "Missing --build-test-dir-base for make.\n");
	  exit(EXIT_FAILURE);
	}
    }
  else
    build_test_dir_base = "";

  /* Figure out base name for option strings. */

  {
    char *begin_base;
    char *end_base;
    char *p;

    begin_base = strrchr(inputfile, '/');
    if (!begin_base)
      begin_base = inputfile;
    else
      begin_base++;

    /* strndup is not always available.  Use strdup. */
    basename = strdup_chk(begin_base);

    end_base = strchr(basename, '.');
    if (end_base)
      *end_base = 0;

    for (p = basename; *p; p++)
      if (*p >= 'a' && *p <= 'z')
	*p = (char) (*p + 'A' - 'a');
  }

  /* Comment style is different for Makefile fragments. */

  if (ismake)
    {
      begin_comment_line = "##";
      cont_comment_line  = "##";
    }

  /* Information. */

  CMT_BEGIN(("*************************************************************"));
  CMT_BLANK;
  CMT__TEXT((" This file is autogenerated by %s",argv[0]));
  CMT_BLANK;
  CMT__TEXT((" Editing is useless."));
  CMT_BLANK;
  CMT__TEXT(("*************************************************************"));
  CMT_BLANK;
  CMT__TEXT((" Input file:    %s", inputfile));
  CMT__TEXT((" Basename:      %s", basename));
  CMT__TEXT((" Compiler:      %s", compiler));
  CMT__NONL((" Compiler args:"));
  {
    char **p;
    for (p = compileargs; *p; p++)
      printf (" %s", *p);
    printf ("\n");
  }
  CMT_BLANK;
  if (ismake)
    {
      char *makeflags = getenv("MAKEFLAGS");

      if (makeflags)
	{
	  char *newenv;

	  /* If MAKEFLAGS contains a string of the form
	   * '--jobserver...', or '-j...' we remove those, to not
	   * have the complaints about jobserver not available.
	   * (And to not run in parallel in these submakes.)
	   */

	  newenv = makeflags;
	  newenv = kill_argument(newenv, "--jobserver");
	  newenv = kill_argument(newenv, "-j");

	  if (newenv != makeflags)
	    {
	      CMT__TEXT((" MAKEFLAGS:     %s", makeflags));
	      CMT__TEXT((" MAKEFLAGS:     %s", newenv));
	      CMT_BLANK;
	      /* When setenv is not available, we just do not this.
	       * Is not critical anyhow.
	       *
	       * Just try compilation twice.  Feature macros did not
	       * avoid the inclusion with actually missing setenv.
	       */
#if WITH_SETENV
	      setenv("MAKEFLAGS", newenv, 1);
#endif
	    }
	}
    }
  {
    char *acc_makeflags = getenv("ACC_MAKEFLAGS");

    if (acc_makeflags &&
	strstr(acc_makeflags, "-j"))
      {
	if (ismake)
	  {
	    /* Assume that we can run a lot of jobs. */
	    max_jobs = 10;
	  }
	else
	  {
	    /* Compile option jobs would already run in parallel.
	     * Just add a little bit of overlap in execution.
	     */
	    max_jobs = 3;
	  }
      }
  }
  CMT__TEXT(("************************************************************/"));

  if (!ismake)
    {
      BLANK_LINE;
      printf ("#ifndef __AUTO_DEF__%s_HH__\n", basename);
      printf ("#define __AUTO_DEF__%s_HH__\n", basename);
    }

  BLANK_LINE;
  CMT_BEGIN(("*************************************************************"));

  /* Find available options. */

  {
    FILE *fid;

    fid = fopen(inputfile, "r");

    if (!fid)
      {
	perror("fopen");
	fprintf (stderr, "Failed to open %s for reading.\n", inputfile);
	exit(EXIT_FAILURE);
      }

    /* Find all strings on the form 'ACC_DEF__${BASENAME}_([_a-zA-Z0-9]*)'
     * and remember the trailing part.  Became rather hairy in C.
     */

    while (!feof(fid))
      {
	char line[1024];
	char *p;
	char *acc_def_begin;
	char *option;
	char *opt_end;

	if (fgets(line, sizeof (line), fid) == NULL)
	  break;

	p = line;

	for ( ; ; )
	  {
	    acc_def_begin = strstr(p, "ACC_DEF__");

	    if (!acc_def_begin)
	      break;

	    p = acc_def_begin + strlen("ACC_DEF__");

	    if (strncmp(p, basename, strlen(basename)) != 0 ||
		*(p + strlen(basename)) != '_')
	      {
		fprintf (stderr, "Suspicious ACC_DEF__ with "
			 "unknown basename '%s'.\n", p);
		exit(EXIT_FAILURE);
		/* When only handled as warning, it was just
		   continue;
		*/
	      }

	    p += strlen(basename);
	    p++;

	    /* p is now at the option name beginning, strdup from
	     * here.
	     */

	    option = strdup_chk(p);
	    opt_end = option;

	    while ((*opt_end >= 'a' && *opt_end <= 'z') ||
		   (*opt_end >= 'A' && *opt_end <= 'Z') ||
		   (*opt_end >= '0' && *opt_end <= '9') ||
		   *opt_end == '_')
	      opt_end++;

	    *opt_end = 0; /* end of option name */
	    p += strlen(option);

	    if (strstr(option, "PENALTY"))
	      {
		fprintf (stderr, "Suspicious option '%s' with "
			 "PENALTY in name.\n",
			 option);
		exit(EXIT_FAILURE);
	      }

	    if (strstr(option, "showconfig"))
	      {
		show_config_important = 1;
		goto option_seen;
	      }

	    /* Make sure we have not seen the option before.
	     * And find the last pointer.
	     */
	    {
	      struct option_t **lopt = &options;

	      while (*lopt)
		{
		  struct option_t *opt = *lopt;

		  if (strcmp(option, opt->name) == 0)
		    {
		      free(option);
		      goto option_seen;
		    }

		  lopt = &opt->next;
		}

	      /* Option is new - add it! */

	      {
		struct option_t *opt =
		  (struct option_t *) malloc (sizeof (struct option_t));

		MEM_ALLOC_NULL_STDERR_EXIT(opt);

		*lopt = opt;
		opt->name    = option;
		opt->penalty = 0;
		opt->next    = NULL;

		if (strlen(option) > max_opt_len)
		  max_opt_len = strlen(option);
	      }

	      CMT__TEXT((" Option: %s", option));

	    option_seen:
	      ;
	    }
	  }
      }

    rewind(fid);

    /* Find all strings on the form
     * 'ACC_DEF_VAR__${BASENAME}_([_a-zA-Z0-9]*)_PENALTY number'
     */

    while (!feof(fid))
      {
	char line[1024];
	char *p;
	char *acc_def_begin;
	char *option;
	char *opt_end;

	if (fgets(line, sizeof (line), fid) == NULL)
	  break;

	p = line;

	for ( ; ; )
	  {
	    acc_def_begin = strstr(p, "ACC_DEF_VAR_");

	    if (!acc_def_begin)
	      break;

	    p = acc_def_begin + strlen("ACC_DEF_VAR_");

	    /* We match the second '_' here, to catch ACC_DEF_VAR_
	     * without the __.
	     */
	    if (*p != '_' ||
		strncmp(p + 1, basename, strlen(basename)) != 0 ||
		*(p + 1 + strlen(basename)) != '_')
	      {
		fprintf (stderr, "Suspicious ACC_DEF_VAR__ with "
			 "unknown basename '%s'.\n", p);
		exit(EXIT_FAILURE);
		/* When only handled as warning, it was just
		   continue;
		*/
	      }

	    p++;
	    p += strlen(basename);
	    p++;

	    /* p is now at the option name beginning, strdup from
	     * here.
	     */

	    option = p;
	    opt_end = option;

	    while ((*opt_end >= 'a' && *opt_end <= 'z') ||
		   (*opt_end >= 'A' && *opt_end <= 'Z') ||
		   (*opt_end >= '0' && *opt_end <= '9') ||
		   *opt_end == '_')
	      opt_end++;

	    p = opt_end; /* continue next search from here */

	    if ((size_t) (opt_end - option) > strlen("_PENALTY") &&
		strncmp(opt_end - strlen("_PENALTY"), "_PENALTY",
			strlen("_PENALTY")) == 0)
	      {
		int penalty;
		struct option_t *opt;
		size_t optlen =
		  (size_t) (opt_end - option) - strlen("_PENALTY");
		int found = 0;

		/* Found a penalty. */

		while (*opt_end == ' ' || *opt_end == '\t' ||
		       *opt_end == '=')
		  opt_end++;

		penalty = atoi(opt_end);

		for (opt = options; opt; opt = opt->next)
		  if (strncmp (opt->name, option, optlen) == 0)
		    {
		      if (opt->penalty)
			{
			  fprintf (stderr, "Penalty already given for "
				   "option '%s'.\n", option);
			  exit(EXIT_FAILURE);
			}
		      opt->penalty = penalty;
		      found = 1;
		    }

		if (!found)
		  {
		    fprintf (stderr, "Penalty for unknown option '%s'.\n",
			     option);
		    exit(EXIT_FAILURE);
		  }

		CMT__TEXT((" Penalty: %d: %.*s",
			   penalty, (int) optlen, option));
	      }
	    else
	      {
		fprintf (stderr, "Unknown option var ACC_DEF_VAR__ '%s'.\n",
			 option);
		exit(EXIT_FAILURE);
	      }
	  }
      }

    fclose(fid);
  }

  CMT__TEXT(("/"));

  /* All options found - now test them! */

  {
    struct option_t *opt_start;
    struct option_t *opt_report;
    struct option_t *opt;
    char *acc_def_run_basename;
    char *comp_opt;
    char *build_test_dir;
    char *build_test_dir_opt;
    int jobs_active = 0;

    acc_def_run_basename =
      (char *) malloc ((100 + strlen(basename)) * sizeof (char));
    comp_opt =
      (char *) malloc ((100 + strlen(basename) + max_opt_len) * sizeof (char));
    build_test_dir =
      (char *) malloc ((100 + strlen(basename) + max_opt_len +
			strlen(build_test_dir_base)) * sizeof (char));
    build_test_dir_opt =
      (char *) malloc ((100 + strlen(basename) + max_opt_len +
			strlen(build_test_dir_base)) * sizeof (char));

    MEM_ALLOC_NULL_STDERR_EXIT(acc_def_run_basename);
    MEM_ALLOC_NULL_STDERR_EXIT(comp_opt);
    MEM_ALLOC_NULL_STDERR_EXIT(build_test_dir);
    MEM_ALLOC_NULL_STDERR_EXIT(build_test_dir_opt);

    /* For each of the options, try to compile the input file. */

    opt_start = options;
    opt_report = options;

    for ( ; ; )
      {
	/* A new job is started if there are more jobs to be started.
	 * Since a loop will always report a job if needed, we do not
	 * need to check if there are enough job-slots.
	 */
    if (opt_start != NULL)
      {
	int pipefd_stdin[2];
	int pipefd_stdouterr[2];

	int call_argc = 0;
	char **p;

	opt = opt_start;
	opt_start = opt_start->next;

	opt->call_argv = (char **)
	  malloc ((20 + num_compileargs) * sizeof (char *));

	MEM_ALLOC_NULL_STDERR_EXIT(opt->call_argv);

	/* Play games with strdup, since otherwise we have const warnings. */
#define ADD_ARG(arg)  opt->call_argv[call_argc++] = strdup_chk(arg);

	if (ismake)
	  {
	    sprintf (build_test_dir, "%s_%s_%s",
		     build_test_dir_base, basename, opt->name);

	    /* We shall also kill the build directory. */

	    sprintf (build_test_dir_opt, "rm -rf %s", build_test_dir);
	    {
	      int ret = system(build_test_dir_opt);

	      if (WIFSIGNALED(ret) &&
		  (WTERMSIG(ret) == SIGINT ||
		   WTERMSIG(ret) == SIGQUIT))
		{
		  fprintf (stderr, "Abort during directory clean (%s).",
			   build_test_dir_opt);
		  exit(EXIT_FAILURE);
		}
	    }

	    ADD_ARG(compiler);
	    ADD_ARG("-s");
	    ADD_ARG("-f");
	    ADD_ARG(inputfile);
	    ADD_ARG("ACC_DEF_RUN=1");
	    sprintf (acc_def_run_basename, "ACC_DEF_RUN_%s=1", basename);
	    ADD_ARG(acc_def_run_basename);
	    sprintf (comp_opt, "ACC_DEF__%s_%s=1", basename, opt->name);
	    opt->comp_opt = ADD_ARG(comp_opt);
	    sprintf (build_test_dir_opt, "BUILD_TEST_DIR=%s", build_test_dir);
	    ADD_ARG(build_test_dir_opt);
	  }
	else
	  {
	    ADD_ARG(compiler);
	    ADD_ARG("-x");
	    ADD_ARG("c");
	    ADD_ARG("-c");
	    ADD_ARG(inputfile);
	    ADD_ARG("-o");
	    ADD_ARG("/dev/null");
	    ADD_ARG("-DACC_DEF_RUN");
	    sprintf (acc_def_run_basename, "-DACC_DEF_RUN_%s", basename);
	    ADD_ARG(acc_def_run_basename);
	    sprintf (comp_opt, "-DACC_DEF__%s_%s", basename, opt->name);
	    opt->comp_opt = ADD_ARG(comp_opt);
	  }

	for (p = compileargs; *p; p++)
	  ADD_ARG(*p);

	opt->call_argv[call_argc]   = NULL;

	/* We have now set up the argument list.
	 * Create some pipes for in and output.
	 */

	if (pipe(pipefd_stdin) != 0 ||
	    pipe(pipefd_stdouterr) != 0)
	  {
	    fprintf (stderr, "While starting: %s: ", opt->comp_opt);
	    perror("pipe");
	    exit(EXIT_FAILURE);
	  }
	close (pipefd_stdin[1]); /* We provide a broken stdin */

	/* Then fork off the child process, and hope for the best. */

	opt->cpid = fork();
	if (opt->cpid == -1)
	  {
	    fprintf (stderr, "While starting: %s: ", opt->comp_opt);
	    perror("fork");
	    exit(EXIT_FAILURE);
	  }

	if (opt->cpid == 0) /* Child process. */
	  {
	    close(pipefd_stdouterr[0]);

	    dup2(pipefd_stdin[0], STDIN_FILENO);
	    dup2(pipefd_stdouterr[1], STDOUT_FILENO);
	    dup2(pipefd_stdouterr[1], STDERR_FILENO);

	    close(pipefd_stdin[0]);
	    close(pipefd_stdouterr[1]);

	    execvp(compiler, opt->call_argv);

	    /* If we got here, it failed! */

	    fprintf (stderr, "While starting: %s: ", opt->comp_opt);
	    perror("execvp");
	    exit(EXIT_FAILURE);

	    /* And that was it! */
	  }
	else /* Parent process. */
	  {
	    close (pipefd_stdin[0]);
	    close (pipefd_stdouterr[1]);
	    opt->parent_stdouterr = pipefd_stdouterr[0];
	  }
	CMT_BEGIN((" Started %s%s", opt->comp_opt, ismake ? "" : " */"));
	jobs_active++;
      }

    /* We report a job if either there are the maximum number of jobs
     * outstanding, or if there are no more jobs to start.
     */
    if (jobs_active >= max_jobs ||
	opt_start == NULL)
      {
	char **p;

	opt = opt_report;
	opt_report = opt_report->next;

	/* The reporting part of the job.  Done in start order.
	 * Could be after more jobs have been started.
	 */
	  {
	    FILE *fid;
	    size_t output_length = 0;
	    int status;
	    int exit_status;
	    pid_t w;

	    /* Tell what we run. */

	    CMT_BEGIN((" "));
	    CMT__TEXT((" %s",opt->comp_opt));
	    CMT_BLANK;
	    CMT__NONL((" Command:"));
	    for (p = opt->call_argv; *p; p++)
	      printf (" %s", *p);
	    printf ("\n");
	    BLANK_LINE;

	    /* Now, read (and echo) while there is input to read. */

	    fid = fdopen(opt->parent_stdouterr, "r");

	    if (!fid)
	      {
		fprintf (stderr, "While running: %s: ", opt->comp_opt);
		perror("fdopen");
		exit(EXIT_FAILURE);
	      }

	    while (!feof(fid))
	      {
		char line[1024*2]; /* *2 for mangling below */

		if (fgets(line, sizeof (line)/2, fid) == NULL)
		  break;

		output_length += strlen(line);

		if (ismake)
		  printf ("# %s", line);
		else
		  {
		    /* We must mangle any multi-line comment begin and
		     * end markers that appear, since they otherwise
		     * confuse the compile.
		     */

		    while (mangle_comment(line, "/*"))
		      ;
		    while (mangle_comment(line, "*/"))
		      ;

		    printf ("%s", line);
		  }
	      }

	    fclose (fid);

	    /* And figure out the return code. */

	    w = waitpid(opt->cpid, &status, 0);

	    if (w == -1)
	      {
		fprintf (stderr, "While running: %s: ", opt->comp_opt);
		perror("waitpid");
		exit(EXIT_FAILURE);
	      }

	    if (WIFEXITED(status))
	      {
		exit_status = WEXITSTATUS(status);
	      }
	    else
	      {
		if (WIFSIGNALED(status) &&
		    (WTERMSIG(status) == SIGINT ||
		     WTERMSIG(status) == SIGQUIT))
		  {
		    fprintf (stderr, "While running: %s: ", opt->comp_opt);
		    fprintf (stderr, "Abort during (of) child process.");
		    exit(EXIT_FAILURE);
		  }
		/* Child process did not exit nicely, bad one! */
		exit_status = EXIT_FAILURE;
	      }

	    /* Report result. */

	    BLANK_LINE;
	    CMT__TEXT((" --> exit %d (penalty %d) (len %d)",
		       exit_status, opt->penalty, (int) output_length));
	    CMT__TEXT(("/"));

	    /* Was it an useful option, and better than seen before? */

	    if (exit_status == EXIT_SUCCESS)
	      {
		int use_this = 0;

		if (!working)
		  use_this = 1;
		else if (opt->penalty < working_penalty)
		  use_this = 1;
		else if (opt->penalty == working_penalty &&
			 output_length < working_output_length)
		  use_this = 1;

		if (use_this)
		  {
		    working = opt->name;
		    working_penalty = opt->penalty;
		    working_output_length = output_length;
		  }
	      }
	  }

	for (p = opt->call_argv; *p; p++)
	  free(*p); /* Was strdup'd. */
	free(opt->call_argv);

	jobs_active--;

	/* If we just reported the last job, then we are done. */
	if (opt_report == NULL)
	  break;
      }
    /* End of the infinite loop starting and reporting all jobs. */
      }

    free(acc_def_run_basename);
    free(comp_opt);
    free(build_test_dir);
    free(build_test_dir_opt);
  }

  /* Final report. */

  BLANK_LINE;
  CMT_BEGIN(("*************************************************************"));
  BLANK_LINE;

  {
    char *inputfile_after_slash;

    inputfile_after_slash = strrchr(inputfile, '/');

    if (inputfile_after_slash)
      inputfile_after_slash++;
    else
      inputfile_after_slash = inputfile;

    CMT__TEXT((" ACC_DEF_SHOW_CONFIG: %-25s %-25s %s%s",
	       inputfile_after_slash,
	       working ? working : "NO WORKING OPTION FOUND",
	       working_output_length ? "[non-clean]" : "",
	       show_config_important ? "# Important" : ""));
  }

  BLANK_LINE;
  CMT__TEXT(("************************************************************/"));
  BLANK_LINE;

  if (working)
    {
      if (ismake)
	printf("ACC_DEF__%s_%s=1\n", basename, working);
      else
	printf("#define ACC_DEF__%s_%s 1\n", basename, working);
    }
  else
    {
      CMT_BEGIN(("** Did NOT find any working option. ***/"));
      BLANK_LINE;

      if (ismake)
	printf("$(error No working build option!)\n");
      else
	printf("#error No working compile option!\n");
  }

  BLANK_LINE;
  CMT_BEGIN(("************************************************************/"));

  if (!ismake)
    {
      BLANK_LINE;
      printf ("#endif/*__AUTO_DEF__%s_HH__*/\n", basename);
    }

  /* Exit with error code if no working option was found. */

  if (!working)
    {
      fprintf (stderr, "No working option found.\n");
      exit(EXIT_FAILURE);
    }

  /* Print a warning if the working option gives output. */

  if (working_output_length)
    {
      fprintf (stderr, "Warning: working option found, "
	       "but produces output!\n");
    }

  if (print_option)
    {
      fprintf (stderr,"         -> ACC_DEF__%s_%s\n",
	       basename, working);
    }

  exit(EXIT_SUCCESS);
}

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Bastian Loeher  <b.loeher@gsi.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

/*****************************************************************************/

#include "lwroc_message.h"
#include "lwroc_net_proto.h"
#include "lwroc_mon_block.h"
#include "lwroc_pipe_buffer.h"
#include "lwroc_readout.h"
#include "lwroc_data_pipe.h"
#include "lwroc_parse_util.h"
#include "lwroc_triva.h"
#include "lwroc_triva_readout.h"

#include "f_user_daq.h"
#include "../mbscompat/sbs_def.h"

#include "lmd/lwroc_lmd_event.h"
#include "lmd/lwroc_lmd_titris_stamp.h"
#include "lmd/lwroc_lmd_white_rabbit_stamp.h"
#include "lmd/lwroc_lmd_ev_sev.h"
#include "lmd/lwroc_lmd_util.h"

#include <stdarg.h>
#include <stdio.h>
#include "../dtc_arch/acc_def/myinttypes.h"

#include "../dtc_arch/acc_def/time_include.h"
#include <string.h>
#include "../dtc_arch/acc_def/myprizd.h"

#include <unistd.h>

extern struct lwroc_readout_functions _lwroc_readout_functions;
struct lwroc_mbs_compat_functions _f_user;
struct lwroc_cmdline_functions  _lwroc_fud_cmdline_fcns;

int f_user_get_virt_ptr(long *, long []);
int f_user_init(unsigned char, long *, long *, long *);
int f_user_readout(unsigned char, unsigned char, register long *,
		   register long *, long *, void *, long *, long *);
int f_user_end(long *, long [], int);

/*****************************************************************************/

void fud_pre_parse_setup(void);
void fud_pre_parse_daq_config(void);
void fud_setup_daq_config(void);
void fud_cmdline_usage(void);
int  fud_parse_cmdline_arg(const char *arg);
void fud_init(void);
void fud_start_stop_loop(int start);
void fud_read_event(uint64_t eventno, uint16_t trig);
void fud_format_event(uint64_t eventno, uint16_t trig,
		      const void *data_in, size_t data_in_len);
void fud_uninit(int);

int fud_prepare_cmvlc(unsigned char readout,
		      struct cmvlc_stackcmdbuf *stack);

void lwroc_readout_pre_parse_functions(void)
{
  _lwroc_readout_functions.init = fud_init;
  _lwroc_readout_functions.start_stop_loop = fud_start_stop_loop;
  _lwroc_readout_functions.read_event = fud_read_event;
  _lwroc_readout_functions.uninit = fud_uninit;
  _lwroc_readout_functions.cmdline_fcns.usage = fud_cmdline_usage;
  _lwroc_readout_functions.cmdline_fcns.parse_arg = fud_parse_cmdline_arg;
  _lwroc_readout_functions.fmt = &_lwroc_lmd_format_functions;

  _f_user.get_virt_ptr = f_user_get_virt_ptr;
  _f_user.early_init = NULL;
  _f_user.init = f_user_init;
  _f_user.readout = f_user_readout;
  _f_user.format_event = NULL;
  _f_user.end = NULL;

  _f_user.prepare_cmvlc = NULL;

  _lwroc_fud_cmdline_fcns.usage = NULL;
  _lwroc_fud_cmdline_fcns.parse_arg = NULL;

  fud_pre_parse_daq_config();

  fud_pre_parse_setup();
}

void lwroc_readout_setup_functions(void)
{
  fud_setup_daq_config();
}

/*****************************************************************************/

/* Order of initialisation:
 *
 * C main()
 * |
 * +-C lwroc_main_pre_parse_setup()
 * | | ... memset _lwroc_readout_functions,0
 * | +-C lwroc_readout_pre_parse_functions()
 * |   +-C fud_pre_parse_daq_config()
 * |   +-C fud_pre_parse_setup()
 * |     +-C f_user_pre_parse_setup()     [weak function]
 * |
 * | [Parsing command line in main()]
 * | +-C lwroc_main_parse_cmdline_arg()
 * | | +-C _lwroc_readout_functions.parse_cmdline_arg() =
 * | |     fud_parse_cmdline_arg()
 * | +-C lwroc_main_filter_parse_cmdline_arg()
 * |
 * | [Usage for command line in main()]
 * | +-C lwroc_main_usage()
 * |   +-C lwroc_main_cmdline_usage()
 * |   | +-C _lwroc_readout_functions.cmdline_usage() =
 * |   |     fud_cmdline_usage()
 * |   +-C lwroc_main_filter_cmdline_usage()
 * |
 * +-C lwroc_main_parse_setup()
 * | +-C lwroc_triva_parse_setup()
 * |
 * | [READOUT_PIPE is allocated]
 * |
 * +-C lwroc_main_setup()
 * | +-C lwroc_triva_setup()
 * | | +-C lwroc_triva_map();
 * | | +-- [TRIVA/MI 'conquered': HALT issued.]
 * | |
 * | +-C lwroc_readout_setup_functions()
 * |   +-C fud_setup_daq_config()
 * |
 * | [Threads are created]
 * |
 * +-C lwroc_main_loop()
 *   +-C _lwroc_readout_functions.init() =
 *   | | fud_init()
 *   | +-C _f_user.get_virt_ptr()
 *   | +-C _f_user.early_init()
 *   |
 *   +-C lwroc_triva_readout()                   [If TRIVA configured.]
 *   | +-C _lwroc_readout_functions.triva_event_loop()        [If set.]
 *   | +-C lwroc_triva_event_loop()                             [Else.]
 *   |   +-C _lwroc_readout_functions.start_stop_loop(1) =
 *   |   | | fud_start_stop_loop(1)
 *   |   | +-C _f_user.init()
 *   |   +-C _lwroc_readout_functions.read_event() =
 *   |     | fud_read_event()
 *   |     +-C _f_user.readout()
 *   |
 *   +-C _lwroc_readout_functions.untriggered_loop()   [No TRIVA conf.]
 *   |
 *   +-C _lwroc_readout_functions.uninit() =
 *     | fud_uninit()
 *     +-C _f_user.end()
 *
 * Note: _f_user.get_virt_ptr() is intentionally long before the
 * readout loop.  This can set _f_user.early_init() to be called.
 * User should not have to wait for a multi-crate system to come up to
 * see potential error messages from their readout initialisation,
 * (due to triva state machine delaying start trigger until all
 * systems are available.)
 */

/*****************************************************************************/

#define MAX_N_CRATES 16
#define MAX_N_TRIG 16

struct crate_config
{
  char enabled;
  struct lwroc_lmd_subevent_info se_info;
};

struct daq_config
{
  /* We only give one max length, for all triggers.  The optimisation
   * to do it per trigger is small, but the pain (with more user
   * options) high.
   *
   * max_event_length is initialized from data pipe,
   * i.e. comes from elsewhere.
   */
  uint32_t max_event_length;
  uint32_t max_sticky_length_before;
  uint32_t max_sticky_length_after;
  struct crate_config crates[MAX_N_CRATES];
};

struct daq_config _lwroc_daq_cfg;

unsigned char _f_user_used_crates[MAX_N_CRATES];
int _f_user_num_used_crates = 0;

/*****************************************************************************/

void fud_pre_parse_daq_config(void)
{
  memset(&_lwroc_daq_cfg, 0, sizeof(struct daq_config));

  /* General setup */
  _lwroc_daq_cfg.max_sticky_length_before = 0x100;
  _lwroc_daq_cfg.max_sticky_length_after = 0x1000;
  /* User has to specify even the first crate on the command-line. */
}

/*****************************************************************************/

void __attribute__((weak)) f_user_pre_parse_setup(void)
{
  /* Do nothing. */
  /* LWROC_INFO("f_user at_startup setup weak (no user function)"); */
}

void fud_pre_parse_setup(void)
{
  f_user_pre_parse_setup();
}

/*****************************************************************************/

uint32_t fud_get_max_event_length(void)
{
  return _lwroc_daq_cfg.max_event_length;
}

/*****************************************************************************/

void fud_subev_usage(void)
{
  printf ("\n");
  printf ("f_user LMD subev options:\n");
  printf ("\n");
  printf ("  subcrate=N               Subevent subcrate number [0-15] (or crate).\n");
  printf ("  type=N                   Subevent type [0-65535] (or t).\n");
  printf ("  subtype=N                Subevent subtype [0-65535] (or st).\n");
  printf ("  procid=N                 Subevent procid [0-65535] (or proc).\n");
  printf ("  control=N                Subevent control [0-255] (or ctrl).\n");
  printf ("\n");
}

void fud_parse_subev(const char *cfg)
{
  const char *request;
  lwroc_parse_split_list parse_info;

  struct crate_config *crate;

  uint64_t procid   = (uint64_t) -1;
  uint64_t type     = (uint64_t) -1;
  uint64_t subtype  = (uint64_t) -1;
  uint64_t control  = (uint64_t) -1;
  uint64_t subcrate = (uint64_t) -1;

  /* This picks up the endianness: */
  union {
    uint32_t u32;
    uint8_t u8[4];
  } u;
  u.u32 = 1;
#define RUNTIME_IS_LITTLE_ENDIAN (u.u8[0])

  lwroc_parse_split_list_setup(&parse_info, cfg, ',');

  while ((request = lwroc_parse_split_list_next(&parse_info)) != NULL)
    {
      const char *post;

      if (LWROC_MATCH_C_ARG("help"))
	{
	  fud_subev_usage();
	  exit(0);
	}

      /* For command line doc checking, fakes for below:
       * LWROC_MATCH_C_PREFIX("procid=")
       * LWROC_MATCH_C_PREFIX("type=")
       * LWROC_MATCH_C_PREFIX("subtype=")
       * LWROC_MATCH_C_PREFIX("control=")
       * LWROC_MATCH_C_PREFIX("subcrate=")
       */
#define GET_SUBEV_COMP(type, item, itemshort, min, max) do {		\
	if (LWROC_MATCH_C_PREFIX(#item "=", post) ||			\
	    LWROC_MATCH_C_PREFIX(#itemshort "=", post))			\
	  {								\
	    uint64_t val =						\
	      lwroc_parse_hex_limit(post,				\
				    "Subev. option '" #item "'",	\
				    (min), (max));			\
	    /* If we reach here, value was good. */			\
	    item = (type) val;						\
	    goto matched_item;						\
	  }								\
      } while (0)

      GET_SUBEV_COMP(uint16_t, procid,   proc,  0, 0xffff);
      GET_SUBEV_COMP(uint16_t, type,     t,     0, 0xffff);
      GET_SUBEV_COMP(uint16_t, subtype,  st,    0, 0xffff);
      GET_SUBEV_COMP(uint8_t,  control,  ctrl,  0, 0xff);
      /* 15 is max value in MBS config file, stick to that. */
      GET_SUBEV_COMP(uint8_t,  subcrate, crate, 0, MAX_N_CRATES-1);

      LWROC_BADCFG_FMT("Unrecognised triva subev option: %s",
		       request);

    matched_item:
      ;
    }

  if (subcrate == (uint64_t) -1)
    LWROC_BADCFG_FMT("Subev '%s' missing subcrate #.", cfg);

  crate = &_lwroc_daq_cfg.crates[subcrate];

  if (crate->enabled)
    LWROC_BADCFG_FMT("Subev with crate #%" PRIi64 " specified twice.",
		     subcrate);

#define SUBEV_WARN_DEFAULT(item,def) do {			\
    if (item == (uint64_t) -1)					\
      {								\
	LWROC_WARNING_FMT("Subev '%s' missing " #item ", "	\
			  "using default (%d).",		\
			  cfg, def);				\
	item = (def);						\
      }								\
  } while (0)

  /* There is no MBS default, has to be given in config.  Use ugly values. */
  SUBEV_WARN_DEFAULT(type,    1234);
  SUBEV_WARN_DEFAULT(subtype, 4567);
  SUBEV_WARN_DEFAULT(control, 0);

  if (procid == (uint64_t) -1)
    {
      const char *gsi_cpu_platform;

      /* If we can determine the platform from GSI_CPU_PLATFORM, use that. */
      gsi_cpu_platform = getenv("GSI_CPU_PLATFORM");

      if (gsi_cpu_platform != NULL)
	{
	  if      (strcmp(gsi_cpu_platform, "CVC") == 0)  procid = 1;
	  else if (strcmp(gsi_cpu_platform, "E7") == 0)   procid = 3;
	  else if (strcmp(gsi_cpu_platform, "RIO2") == 0) procid = 8;
	  else if (strcmp(gsi_cpu_platform, "RIO3") == 0) procid = 10;
	  else if (strcmp(gsi_cpu_platform, "RIO4") == 0) procid = 12;
	  else if (strcmp(gsi_cpu_platform, "X86") == 0)  procid = 13;
	  else if (strcmp(gsi_cpu_platform, "IPV") == 0)  procid = 14;
	}

      if (procid == (uint64_t) -1)
	{
	  /* If still unresolved, determine an 'effective' procid from
	   * the machine endianness.
	   *
	   * Using 12 = RIO4, BigEndian, 13 = X86, LittleEndian.
	   */
	  procid = (uint16_t)(12 + RUNTIME_IS_LITTLE_ENDIAN);

	  /* Warn the user that we had to resort to dirty methods. */

	  LWROC_WARNING_FMT("Subev '%s' missing procid, "
			    "using endian-dependent approximation "
			    "(%" PRIu64 ").", cfg, procid);
	}
    }

  /* Check that given procid has correct endianness, unless 0, which we
   * do not warn about.  Such that we can give a platform independent
   * value for procid during tests without warnings.
   */
  if (procid != 0)
    {
      int procid_little_endian;

      /* TODO: It is not clear at all if this is the 'definition' of
       * endianness of 10/1 event subevents in lmd files.  But until
       * further notice, assume so.
       */

      if (procid == 1 || procid == 3)
	procid_little_endian = 0;
      else
	procid_little_endian = procid & 1;

      if (procid_little_endian != RUNTIME_IS_LITTLE_ENDIAN)
	LWROC_WARNING_FMT("Subevn '%s' procid %" PRIu64 " suggests %s endian, "
			  "but machine is %s endian.",
			  cfg, procid,
			  procid_little_endian ? "little" : "big",
			  RUNTIME_IS_LITTLE_ENDIAN  ? "little" : "big");
    }

  crate->enabled = 1;
  crate->se_info.type     = (uint16_t) type;
  crate->se_info.subtype  = (uint16_t) subtype;
  crate->se_info.procid   = (uint16_t) procid;
  crate->se_info.control  = (uint8_t) control;
  crate->se_info.subcrate = (uint8_t) subcrate;
}

/*****************************************************************************/

void fud_cmdline_usage(void)
{
  printf ("  --max-sticky-size=N      Maximum sticky (after) event size (def 0x1000).\n");
  printf ("  --max-sticky-before-size=N  Maximum sticky (before) event size (def 0x100).\n");
  printf ("  --subev=[opts]           LMD subevent/crate readout specs.  [or help]\n");
  printf ("\n");

  if (_lwroc_fud_cmdline_fcns.usage)
    _lwroc_fud_cmdline_fcns.usage();
}

/*****************************************************************************/

int fud_parse_cmdline_arg(const char *request)
{
  const char *post;

  /*NOHELPCALL*/ /* We are a option subset, --help handled elsewhere. */

  if (LWROC_MATCH_C_PREFIX("--max-sticky-size=",post)) {
    _lwroc_daq_cfg.max_sticky_length_after =
      (uint32_t) lwroc_parse_hex(post,"maximum sticky after event size");
    return 1;
  }
  else if (LWROC_MATCH_C_PREFIX("--max-sticky-before-size=",post)) {
    _lwroc_daq_cfg.max_sticky_length_before =
      (uint32_t) lwroc_parse_hex(post,"maximum sticky before event size");
    return 1;
  }
  else if (LWROC_MATCH_C_PREFIX("--subev=",post)) {
    fud_parse_subev(post);
    return 1;
  }
  else if (_lwroc_fud_cmdline_fcns.parse_arg) {
    return _lwroc_fud_cmdline_fcns.parse_arg(request);
  } else {
    return 0;
  }
}

/*****************************************************************************/

lmd_stream_handle *_f_user_lmd_stream = NULL;

void fud_setup_daq_config(void)
{
  _f_user_lmd_stream = lwroc_get_lmd_stream("READOUT_PIPE");

  _lwroc_daq_cfg.max_event_length =
    lwroc_lmd_stream_get_max_ev_len(_f_user_lmd_stream);

  if (_lwroc_daq_cfg.max_sticky_length_after > _lwroc_daq_cfg.max_event_length)
    LWROC_BADCFG_FMT("Max sticky after (%" PRIu32 ") longer than "
		     "maximum event size (%" PRIu32 ").",
		     _lwroc_daq_cfg.max_sticky_length_after,
		     _lwroc_daq_cfg.max_event_length);
  if (_lwroc_daq_cfg.max_sticky_length_before > _lwroc_daq_cfg.max_event_length)
    LWROC_BADCFG_FMT("Max sticky before (%" PRIu32 ") longer than "
		     "maximum event size (%" PRIu32 ").",
		     _lwroc_daq_cfg.max_sticky_length_before,
		     _lwroc_daq_cfg.max_event_length);
}

/*****************************************************************************/

long _f_user_read_status = 0;

void fud_call_init(f_user_init_func init)
{
  long *hardware = NULL;
  long *camac = NULL;
  int i;

  for (i = 0; i < _f_user_num_used_crates; ++i)
    {
      unsigned char crate_number = _f_user_used_crates[i];
      long *ptr_read_status;

      ptr_read_status = &_f_user_read_status;

      if (lwroc_data_pipe_is_phys_pexor())
	camac = (long *) lwroc_get_data_pipe_phys_offset();

      init(crate_number,
	   hardware,
	   camac,
	   ptr_read_status);
    }
}

void fud_init(void)
{
  long *hardware = NULL;
  long *camac = NULL;
  int i;

  /* Find out which crates are used. */

  _f_user_num_used_crates = 0;
  for (i = 0; i < MAX_N_CRATES; ++i)
    {
      if (_lwroc_daq_cfg.crates[i].enabled)
	{
	  _f_user_used_crates[_f_user_num_used_crates] = (unsigned char) i;
	  _f_user_num_used_crates++;
	}
    }

  if (_f_user_num_used_crates == 0)
    LWROC_WARNING("No sub-crates (subev) specified for f_user.  "
		  "f_user_readout() will not be called.");

  _f_user.get_virt_ptr(hardware, camac);

  if (_f_user.early_init)
    fud_call_init(_f_user.early_init);
}

void fud_start_stop_loop(int start)
{
  if (start)
    if (_f_user.init)
      fud_call_init(_f_user.init);
}

void fud_uninit(int start_no_stop)
{
  long *hardware = NULL;
  long *camac = NULL;

  /* If start but no stop, then trig 15 was NOT handled. */
  int trig15done = !start_no_stop;

  if (_f_user.end)
    _f_user.end(hardware, camac,
		trig15done);
}

/*****************************************************************************/

#define FUD_READ_EVENT  fud_read_event

#include "f_user_daq_read_event.c"

#undef FUD_READ_EVENT

#define FUD_READ_EVENT  fud_format_event
#define FUD_READ_EVENT_FORMAT  1

#include "f_user_daq_read_event.c"

#undef FUD_READ_EVENT
#undef FUD_READ_EVENT_FORMAT

/*****************************************************************************/

#if HAS_CMVLC
int fud_prepare_cmvlc(unsigned char readout_no,
		      struct cmvlc_stackcmdbuf *stack)
{
  return _f_user.prepare_cmvlc(readout_no, stack);
}

void fud_setup_cmvlc_readout(unsigned char readout_for_trig[16],
			     f_user_prepare_cmvlc_func prepare_cmvlc,
			     f_user_format_event_func format_event)
{
  memcpy(_lwroc_readout_functions.cmvlc_readout_for_trig, readout_for_trig,
	 sizeof (_lwroc_readout_functions.cmvlc_readout_for_trig));

  _lwroc_readout_functions.prepare_cmvlc = fud_prepare_cmvlc;
  _lwroc_readout_functions.format_event = fud_format_event;

  _f_user.prepare_cmvlc = prepare_cmvlc;
  _f_user.format_event = format_event;
}

void fud_setup_cmvlc_readout_release_dt(unsigned char readout_no,
					struct cmvlc_stackcmdbuf *stack)
{
  lwroc_triva_prepare_cmvlc_release_dt(stack, readout_no);
}

#endif/*HAS_CMVLC*/

/*****************************************************************************/

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Bastian Loeher  <b.loeher@gsi.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __F_USER_DAQ_H__
#define __F_USER_DAQ_H__

#include "lwroc_cmdline_fcns.h"

struct cmvlc_stackcmdbuf;

/* Interface for MBS-compliant code */
typedef int (*f_user_get_virt_ptr_func)(long *, long []);
typedef int (*f_user_init_func)(unsigned char, long *, long *, long *);
typedef int (*f_user_readout_func)(unsigned char, unsigned char,
				   register long *, register long *, long *,
				   void *, long *, long *);
typedef int (*f_user_format_event_func)(unsigned char, unsigned char,
					const long *, long, long *,
					long *, void *, long *);
typedef int (*f_user_end_func)(long *, long [], int);

typedef int (*f_user_prepare_cmvlc_func)(unsigned char readout_no,
					 struct cmvlc_stackcmdbuf *stack);

struct lwroc_mbs_compat_functions
{
  f_user_get_virt_ptr_func  get_virt_ptr;
  f_user_init_func          early_init;    /* This is not compliant. */
  f_user_init_func          init;
  f_user_readout_func       readout;
  f_user_format_event_func  format_event;  /* This is not compliant. */
  f_user_end_func           end;           /* This is not compliant. */

  f_user_prepare_cmvlc_func prepare_cmvlc; /* This is not compliant. */
};

uint32_t fud_get_max_event_length(void);

void fud_setup_cmvlc_readout_release_dt(unsigned char readout_no,
					struct cmvlc_stackcmdbuf *stack);

void fud_setup_cmvlc_readout(unsigned char readout_for_trig[16],
			     f_user_prepare_cmvlc_func prepare_cmvlc,
			     f_user_format_event_func format_event);

int f_user_prepare_cmvlc(unsigned char readout_no,
			 struct cmvlc_stackcmdbuf *stack);

int f_user_format_event(unsigned char trig, unsigned char crate_number,
			const long *input, long input_len, long *input_used,
			long *buf,
			void *subevent, long *bytes_read);

extern struct lwroc_cmdline_functions  _lwroc_fud_cmdline_fcns;

#endif/*__F_USER_DAQ_H__*/

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Bastian Loeher  <b.loeher@gsi.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

/* The function in this file is in the main event loop.
 *
 * To avoid too many static if-statements, this file is included several
 * times to create multiple functions.
 */

/*****************************************************************************/

void FUD_READ_EVENT(uint64_t eventno, uint16_t trig
#if FUD_READ_EVENT_FORMAT
		    ,const void *input, size_t input_len
#endif
		    )
{
#if !FUD_READ_EVENT_FORMAT
  long *hardware = NULL;
  long *camac = NULL;
#endif
  int i;

  lmd_event_10_1_host *event;
  lmd_subevent_10_1_host *subevent;

  size_t max_event_size         = _lwroc_daq_cfg.max_event_length;
  size_t max_sticky_before_size = _lwroc_daq_cfg.max_sticky_length_before;
  size_t max_sticky_after_size  = _lwroc_daq_cfg.max_sticky_length_after;

#if !FUD_READ_EVENT_FORMAT
  if (lwroc_data_pipe_is_phys_pexor())
    camac = (long *) lwroc_get_data_pipe_phys_offset();
#endif

  lwroc_reserve_event_buffer(_f_user_lmd_stream, (uint32_t) eventno,
			     max_event_size,
			     max_sticky_before_size,
			     max_sticky_after_size);

  lwroc_new_event(_f_user_lmd_stream, &event, trig);

  for (i = 0; i < _f_user_num_used_crates; ++i)
    {
      unsigned char crate_number = _f_user_used_crates[i];
      struct lwroc_lmd_subevent_info *sub_info;
      char *data_buf;
      uint32_t *end;
      long bytes_read = 0;
      long *ptr_bytes_read;
#if !FUD_READ_EVENT_FORMAT
      long *ptr_read_status;
#endif
#if FUD_READ_EVENT_FORMAT
      long input_used = 0;
      long *ptr_input_used;
#endif

      sub_info = &_lwroc_daq_cfg.crates[crate_number].se_info;

      data_buf = lwroc_new_subevent(_f_user_lmd_stream,
				    LWROC_LMD_SEV_NORMAL, &subevent, sub_info);

      bytes_read = 0;
      ptr_bytes_read = &bytes_read;
#if !FUD_READ_EVENT_FORMAT
      ptr_read_status = &_f_user_read_status;
#endif

#if FUD_READ_EVENT_FORMAT
      ptr_input_used = &input_used;

      _f_user.format_event((unsigned char) trig,
			   (unsigned char) crate_number,
			   (const long *) input, (long) input_len,
			   ptr_input_used,
			   (long *) data_buf,
			   (void *) subevent,
			   ptr_bytes_read);

      if ((size_t) input_used > input_len)
	{
	  LWROC_FATAL_FMT("_f_user.format_event(trig=%d,crate=%d) "
			  "used %lu bytes >= "
			  "available %" MYPRIzd ".",
			  trig, crate_number, input_used, input_len);
	}

      input = (const uint32_t *)((const char *)input + input_used);
      input_len -= (size_t) input_used;
#else
      _f_user.readout((unsigned char) trig,
		      (unsigned char) crate_number,
		      hardware,
		      camac,
		      (long *) data_buf,
		      (void *) subevent,
		      ptr_bytes_read,
		      ptr_read_status);
#endif

      /* Complain, if we didn't read a multiple of 4 bytes
       * and round up to the next multiple of 4.
       */
      if (bytes_read % 4 != 0)
	{
	  LWROC_ERROR_FMT("_f_user.readout(trig=%d,crate=%d) "
			  "returned %lu bytes, "
			  "which is not a multiple of 4.  "
			  "Rounding up.",
			  trig, crate_number, bytes_read);
	  bytes_read = (bytes_read + 3) & ~(long) 0x3;
	}
      /* Subevent overflow checked in lwroc_finalise_subevent. */

      end = (uint32_t *)((char *)data_buf + bytes_read);

      lwroc_finalise_subevent(_f_user_lmd_stream, LWROC_LMD_SEV_NORMAL, end);
    }

#if FUD_READ_EVENT_FORMAT
  if (input_len)
    {
      LWROC_ERROR_FMT("_f_user.format_event(trig=%d) "
		      "left %" MYPRIzd " bytes unused.",
		      trig, input_len);
    }
#endif

  lwroc_finalise_event_buffer(_f_user_lmd_stream);

  /* Let's strictly verify that user called what they claim they did. */
  if (_f_user_read_status != _lwroc_readout_released_dt)
    {
      /* For trigger 15 we will have set _lwroc_readout_released_dt,
       * without actually releasing deadtime.
       */
      if (trig != 15)
	LWROC_FATAL_FMT("_f_user.readout(trig=%d) "
			"status = 0x%lx not agreeing with actual "
			"call to deadtime release = 0x%lx.",
			trig,
			_f_user_read_status,
			_lwroc_readout_released_dt);
    }

  /* We should NOT return a pointer to the buffer here, as it may
   * already have been consumed.
   */
}

/*****************************************************************************/

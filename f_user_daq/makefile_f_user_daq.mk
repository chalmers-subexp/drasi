# -*- makefile -*-

# This file is part of DRASI - a data acquisition data pump.
#
# Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301  USA

QUIET ?= @

#######################################################################

DTC_ARCH_DIR=../dtc_arch
DTC_ARCH_TEMPLATE_DIR=../build_rules

CFLAGS += -I . -I $(DTC_ARCH_DIR)
LIBS   +=

include $(DTC_ARCH_DIR)/build_rules_inc.mk

INC = $(F_USER_DAQ_DIR)/../mbscompat

PREFIX = $(F_USER_DAQ_DIR)/../daq/

# HEADERS = $(PREFIX)/error.hh

OBJS    = $(F_USER_DAQ_DIR)/f_user_daq.o

CFLAGS += -g -O3

GENDIR = gen
GENDIR_ARCH = ${GENDIR}_${ARCH_SUFFIX}

CFLAGS += -I ${GENDIR_ARCH}

CFLAGS += $(WARN_FLAGS)

BUILD_DIR = bld_$(ARCH_SUFFIX)
LIB_DIR = lib_$(ARCH_SUFFIX)
BIN_DIR = bin_$(ARCH_SUFFIX)

###

LWROC_DIR=$(F_USER_DAQ_DIR)/../lwroc

CFLAGS += -I $(LWROC_DIR)

LWROC_LIBS=-L$(LWROC_DIR)/$(LIB_DIR) \
	-llwroc_main -llwroc_readout -llwroc -llwroc_netutil

LWROC_LIBS_STAMP = $(LWROC_DIR)/$(LIB_DIR)/lwroc_libs.stamp

LIBS += $(PLATFORM_LIBS)

###

OBJS_ARCH = $(OBJS:%.o=$(BUILD_DIR)/%.o)

all: $(OBJS_ARCH) # $(BUILD_DIR)/f_user_daq.o

###

#######################################################################

AUTO_DEPS = $(OBJS_ARCH:%.o=%.d)

FIXUP_DEPS = sed -e 's,\($(*F)\)\.o[ :]*,$(BUILD_DIR)/$*.o $@ : ,g' \
	-e 's,[^/]acc_auto_def/, gen_$(ARCH_SUFFIX)/acc_auto_def/,g'

ifneq (clean,$(MAKECMDGOALS))
ifneq (clean-all,$(MAKECMDGOALS))
-include $(AUTO_DEPS) # dependency files (.d)
endif
endif

#######################################################################

include $(DTC_ARCH_DIR)/acc_auto_def.mk

#######################################################################

$(BUILD_DIR)/%.o: %.c
	@echo "   CC    $@"
	@$(MKDIR_P) $(dir $@)
	$(QUIET)$(CC) $(CFLAGS) $(OPTFLAGS) -o $@ -c $<

$(BUILD_DIR)/%.d: %.c
	@echo "  DEPS   $@"
	@$(MKDIR_P) $(dir $@)
	$(QUIET)$(CC) $(CFLAGS) -MM -MG $< | $(FIXUP_DEPS) > $@

#######################################################################

clean:
	rm -rf $(BUILD_DIR)
	rm -rf $(BIN_DIR)
	rm -rf ${GENDIR_ARCH}
	rm -f *_$(ARCH_SUFFIX).o *_$(ARCH_SUFFIX).d
	rm -f f_user_daq_$(ARCH_SUFFIX)

clean-all:
	rm -rf build_* # old, to be removed
	rm -rf bld_*
	rm -rf bin_*
	rm -rf gen*

#######################################################################

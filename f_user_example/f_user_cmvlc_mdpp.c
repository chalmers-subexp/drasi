/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2025  Haakan T. Johansson  <f96hajo@chalmers.se>
 * Copyright (C) 2025  Anna Kawecka  <anna.kawecka@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "f_user_daq.h"

#include "../lwroc_parse_util.h"
#include "../lwroc_message.h"
#include "../lwroc_triva_readout.h"

#include "cmvlc_stackcmd.h"

struct cmvlc_client *lwroc_triva_access_get_cmvlc(void);

extern struct lwroc_mbs_compat_functions _f_user;

#define MAX_NUM_MDPP  20

uint32_t _mdpp_vme_base[MAX_NUM_MDPP];
int      _num_mdpp = 0;
int      _early_dt_release = 0;
uint64_t _extra_wait_ns = 0;

void local_cmdline_usage(void)
{
  printf ("  --mdpp=ADDR              MDPP address.\n");
  printf ("  --early-dt-release       Release DT after reading event-counters.\n");
  printf ("  --extra-wait=Nns         Additional delay (wait) in MVLC readout sequence.\n");
  printf ("\n");
}

int local_parse_cmdline_arg(const char *request)
{
  const char *post;

  /*NOHELPCALL*/ /* We are a option subset, --help handled elsewhere. */

  if (LWROC_MATCH_C_PREFIX("--mdpp=",post)) {
    if (_num_mdpp >= MAX_NUM_MDPP)
      LWROC_BADCFG_FMT("Too many MDPPs (max %d).", MAX_NUM_MDPP);

    _mdpp_vme_base[_num_mdpp] =
      (uint32_t) lwroc_parse_hex(post, "MDPP address");

    printf ("MDPP #%d: '%s' => 0x%08x\n",
	    _num_mdpp, request, _mdpp_vme_base[_num_mdpp]);

    _num_mdpp++;

    return 1;
  }
  else if (LWROC_MATCH_C_ARG("--early-dt-release")) {
    _early_dt_release = 1;
    return 1;
  }
  else if (LWROC_MATCH_C_PREFIX("--extra-wait=",post)) {
    _extra_wait_ns = lwroc_parse_time_ns(post, "extra wait");
    return 1;
  }

  return 0;
}

/* Overrides default (attribute weak) do-nothing function. */
void f_user_pre_parse_setup(void)
{
  _lwroc_fud_cmdline_fcns.usage = local_cmdline_usage;
  _lwroc_fud_cmdline_fcns.parse_arg = local_parse_cmdline_arg;
}

int f_user_early_init(unsigned char crate_number, long *hardware, long *camac,
		      long *status);

int f_user_get_virt_ptr(long *hardware, long camac[])
{
  (void)hardware;
  (void)camac;

  /* Init function that is called before event loop. */
  _f_user.early_init = f_user_early_init;

  {
    /* Tell which sequencer stacks shall be executed for each trigger.
     * The same sequencer stack can be used for several triggers.
     * 0 means an unexpected trigger (will (todo) generate an error).
     *
     * Trigger 14 and 15 are used implicitly.
     *
     * It is a good idea to avoid stack #1-7, since they correspond to
     * physical interrupts, and thus may be set by 'active' modules.
     * In particular #1.
     */

    unsigned char readout_for_trig[16] =
    /*   0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15 */
      {  0,  8,  0,  9,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 };

    fud_setup_cmvlc_readout(readout_for_trig,
			    f_user_prepare_cmvlc,
			    f_user_format_event);
  }

  return 0;
}

#define VME_READ_A32_D16(addr, pdest) do {			\
    int ret;                                                    \
    ret = cmvlc_single_vme_read(cmvlc, (addr), (pdest),		\
				vme_user_A32, vme_D16);		\
    if (ret) {                                                  \
      fprintf(stderr, "Could not read  @ 0x%08x.\n",		\
	      (addr));						\
      exit (1);                                                 \
    }                                                           \
  } while (0)

#define VME_WRITE_A32_D16(addr, value) do {                     \
    int ret;                                                    \
    ret = cmvlc_single_vme_write(cmvlc, (addr), (value),        \
				 vme_user_A32, vme_D16);        \
    if (ret) {                                                  \
      fprintf(stderr, "Could not write 0x%04x @ 0x%08x.\n",     \
	      (value), (addr));                                 \
      exit (1);                                                 \
    }                                                           \
  } while (0)

/* For MVLC, do module init early, before readout is set up.
 * To disable IRQs, in case earlier configuration had them enabled.
 */

int _master_starts;

int f_user_early_init(unsigned char crate_number, long *hardware, long *camac,
		      long *status)
{
  int i;

  (void)crate_number;
  (void)hardware;
  (void)camac;
  (void)status;

  struct cmvlc_client *cmvlc = NULL;
  cmvlc = lwroc_triva_access_get_cmvlc();

  for (i = 0; i < _num_mdpp; i++)
    {
      uint32_t vme_base = _mdpp_vme_base[i];
      uint32_t hw_id, fw_ver;

      VME_READ_A32_D16(vme_base + 0x6008, &hw_id);
      VME_READ_A32_D16(vme_base + 0x600e, &fw_ver);

      printf("MDPP #%d: hw_id: 0x%04x  fw_ver: 0x%04x\n",
	     i, hw_id, fw_ver);

      VME_WRITE_A32_D16(vme_base + 0x6008, 1); /* Module reset. */
      sleep(1);
      VME_WRITE_A32_D16(vme_base + 0x6036, 1); /* Single ev., no buffering. */
      VME_WRITE_A32_D16(vme_base + 0x6058, 1); /* Trigger source: T0. */
      VME_WRITE_A32_D16(vme_base + 0x6090, 3); /* Reset event counters. */
    }

  /* For event counter check. */
  _master_starts = 0;

  printf("\nf_user_early_init()\n\n");

  return 0;
}

int f_user_init(unsigned char crate_number, long *hardware, long *camac,
		long *status)
{
  (void)crate_number;
  (void)hardware;
  (void)camac;
  (void)status;

  printf("\nf_user_init()\n\n");

  return 0;
}

int f_user_readout(unsigned char trig, unsigned char crate_number,
		   register long *hardware, register long *camac, long *buf,
		   void *subevent, long *bytes_read, long *status)
{
  (void)trig;
  (void)crate_number;
  (void)hardware;
  (void)camac;
  (void)buf;
  (void)subevent;
  (void)bytes_read;
  (void)status;

  /* This function is not used for MVLC sequencer readout! */

  return 0;
}

/* Prepare the readout-specific part of the MVLC sequencer for each
 * readout stack given to fud_setup_cmvlc_readout().
 *
 * f_user_format_event() will then be called with a pointer to the
 * corresponding data.
 */

int f_user_prepare_cmvlc(unsigned char readout_no,
			 struct cmvlc_stackcmdbuf *stack)
{
  int i;

  (void)readout_no;
  (void)stack;

  printf ("Prepare MVLC sequencer readout #%d.\n", readout_no);

  /* Read all event counters before reading the data. */
  for (i = 0; i < _num_mdpp; i++)
    {
      uint32_t vme_base = _mdpp_vme_base[i];

      /* FW 2021 has a bug where event counters are latched _after_ read
       * of low word.
       */
      cmvlc_stackcmd_vme_rw(stack, vme_base + 0x6092, 0, vme_rw_read,
			    vme_user_A32, vme_D16);

      /* Read MDPP event counter (low word). */
      cmvlc_stackcmd_vme_rw(stack, vme_base + 0x6092, 0, vme_rw_read,
			    vme_user_A32, vme_D16);
    }

  if (_early_dt_release &&
      readout_no != 15)
    fud_setup_cmvlc_readout_release_dt(readout_no, stack);

  /* Data readout. */
  for (i = 0; i < _num_mdpp; i++)
    {
      uint32_t vme_base = _mdpp_vme_base[i];

      /* MDPP readout. */
      cmvlc_stackcmd_vme_block(stack, vme_base, vme_user_MBLT_A32, 0xffff);
      cmvlc_stackcmd_vme_rw(stack, vme_base + 0x6034, 1, vme_rw_write,
			    vme_user_A32, vme_D16);
    }

  if (_extra_wait_ns)
    {
      /* Emulate more readout by an actual sleep. */
      cmvlc_stackcmd_wait_ns(stack, _extra_wait_ns);
    }

  if (!_early_dt_release &&
      readout_no != 15)
    fud_setup_cmvlc_readout_release_dt(readout_no, stack);

  return 0;
}

int f_user_format_event(unsigned char trig, unsigned char crate_number,
			const long *input, long input_len, long *input_used,
			long *buf,
			void *subevent, long *bytes_read)
{
  const uint32_t *input_u32 = (const uint32_t *) input;
  int i;

  (void)trig;
  (void)crate_number;
  (void)subevent;

#if 0
  {
    static int count = 0;

    count = count + 1;

    if (count % 4096 == 0)
      {
	long i;

	printf ("%d %zd: ", trig, input_len);

	for (i = 0; i < input_len; i += sizeof (uint32_t))
	  {
	    printf (" %08x",
		    *((uint32_t *) (((char *) input) + i)));
	  }
	printf ("\n");
      }
    fflush(stdout);
  }
  /*
14 16: baff1e0e 0000a70e 00000000 f5200000
3 16:  baff1e09 00000000 00000000 f5200000
1 32:  baff1e08 00000000 00000001 f5200004 4021b803 10400020 1041001f c0000000
1 32:  baff1e08 00000001 00000002 f5200004 4021b803 10400020 1041001f c0000001
1 32:  baff1e08 00000002 00000003 f5200004 4021b803 10400020 1041001f c0000002
1 32:  baff1e08 00000003 00000004 f5200004 4021b803 10400020 1041001f c0000003
  */
#endif

  if (trig == 1)
    _master_starts++;

  for (i = 0; i < _num_mdpp; i++)
    {
      /* First word of each module is the bug-related dummy readout. */
      if (input_u32[2*i+1] != (_master_starts & 0xffff))
	fprintf (stderr,
		 "MDPP #%d: "
		 "event counter mismatch 0x%04x != expected 0x%04x.\n",
		 i, input_u32[2*i+1], _master_starts & 0xffff);
    }

  memcpy(buf, input, input_len);
  *bytes_read = input_len;
  *input_used = input_len;

  return 0;
}

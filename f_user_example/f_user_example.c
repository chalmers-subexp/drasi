/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

int f_user_get_virt_ptr(long *hardware, long camac[])
{
  (void)hardware;
  (void)camac;

  return 0;
}

int f_user_init(unsigned char crate_number, long *hardware, long *camac,
		long *status)
{
  (void)crate_number;
  (void)hardware;
  (void)camac;
  (void)status;

  return 0;
}

int f_user_readout(unsigned char trig, unsigned char crate_number,
		   register long *hardware, register long *camac, long *buf,
		   void *subevent, long *bytes_read, long *status)
{
  (void)trig;
  (void)crate_number;
  (void)hardware;
  (void)camac;
  (void)buf;
  (void)subevent;
  (void)bytes_read;
  (void)status;

  return 0;
}

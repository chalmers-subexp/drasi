/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

/* *** This is *not* intended as example code. ***
 *
 * The purpose of this file is to try inclusion of the various
 * mbscompat headers, and test that expected routines / constants
 * are declared.
 */

#include "s_veshe.h"
#include "typedefs.h"
#include "err_mask_def.h"
#include "errnum_def.h"
#include "f_ut_error.h"
#include "error_mac.h"
#include "f_ut_printm.h"
#include "f_user_trig_clear.h"
#include "sbs_def.h"
/*#include "smem.h"*/

/* lwroc f_user extended interface: */
#include "f_user_daq.h"

#include "../lwroc_parse_util.h"

#define EMULATE_DT_USLEEP 0
#if EMULATE_DT_USLEEP
#include <unistd.h>
#endif

int f_user_get_virt_ptr(long *hardware, long camac[])
{
  (void)hardware;
  (void)camac;

  return 0;
}

int f_user_init(unsigned char crate_number, long *hardware, long *camac,
		long *status)
{
  (void)crate_number;
  (void)hardware;
  (void)camac;
  (void)status;

  return 0;
}

int _test_print_interval = 0;
int _test_print_counter = 0;

int f_user_readout(unsigned char trig, unsigned char crate_number,
		   register long *hardware, register long *camac, long *buf,
		   s_veshe *subevent, long *bytes_read, long *status)
{
  (void)trig;
  (void)crate_number;
  (void)hardware;
  (void)camac;
  (void)buf;
  (void)subevent;
  (void)bytes_read;
  (void)status;

  _test_print_counter++;
  if (_test_print_counter > _test_print_interval)
    {
      const char *c_modnam = "f_user_readout"; /* For F_ERROR macro. */

      (void) c_modnam;

      _test_print_counter = 0;
      _test_print_interval++;

      /* Test printm() */

      printm("Test of printm().");
      printm("Test of printm() with argument: trig=%d.", trig);

#undef printm

      printm("Test of printm().");
      printm("Test of printm() with argument: trig=%d.", trig);

      printm("Test of printm(), interval = %d.", _test_print_interval);

      /* Test f_ut_error() */

      f_ut_error("f_user_readout", ERR__MSG_INFO, NULL,
		 "f_ut_error INFO, TERM", MASK__PRTTERM);
      f_ut_error("f_user_readout", ERR__MSG_INFO, NULL,
		 "f_ut_error INFO, LOG", MASK__PRTSLOG);
      f_ut_error("f_user_readout", ERR__MSG_INFO, NULL,
		 "f_ut_error INFO, TT(TERM+LOG)", MASK__PRTT);
      f_ut_error("f_user_readout", ERR__MSG_ERROR, NULL,
		 "f_ut_error ERROR, TERM", MASK__PRTTERM);
      f_ut_error("f_user_readout", ERR__MSG_ERROR, NULL,
		 "f_ut_error ERROR, LOG", MASK__PRTSLOG);
      f_ut_error("f_user_readout", ERR__MSG_ERROR, NULL,
		 "f_ut_error ERROR, TT(TERM+LOG)", MASK__PRTT);

      /* Test F_ERROR() */

      F_ERROR(ERR__MSG_INFO, NULL, "F_ERROR INFO, TERM", MASK__PRTTERM);

      /* Test fud_get_max_event_length(). */
      /* (lwroc f_user extended interface) */

      printm("%d",fud_get_max_event_length());
    }

  /* Test *status */

#define EMULATE_DT_USLEEP 0
#if EMULATE_DT_USLEEP
  usleep(100);
#endif

  *status = 0; /* DT not released. */
  if (!(trig & 1))
    {
      f_user_trig_clear(trig);
      *status = TRIG__CLEARED;

#if EMULATE_DT_USLEEP
      usleep(50);
#endif
    }

  /* *** */

  return 0;
}

void local_cmdline_usage(void)
{
  printf ("  --test-command           Dummy test command.\n");
  printf ("\n");
}

int local_parse_cmdline_arg(const char *request)
{
  /*NOHELPCALL*/ /* We are a option subset, --help handled elsewhere. */

  if (LWROC_MATCH_C_ARG("--test-command")) {
    printf ("Test command argument parsed.\n");
    return 1;
  }

  return 0;
}

/* Overrides default (attribute weak) do-nothing function. */
void f_user_pre_parse_setup(void)
{
  _lwroc_fud_cmdline_fcns.usage = local_cmdline_usage;
  _lwroc_fud_cmdline_fcns.parse_arg = local_parse_cmdline_arg;
}

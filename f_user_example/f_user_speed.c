/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

/* *** This is *not* intended as example code. ***
 *
 * The purpose of this file is some minimal handling speed testing.
 */

#include <float.h>
#include "../dtc_arch/acc_def/time_include.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "f_user_trig_clear.h"
#include "f_ut_printm.h"
#include "sbs_def.h"

/* lwroc internals are usually not used in f_user interfaces.  Used
 * here to get at the buffer fill level. ../ is relative to mbscompat/
 */
#include "../lwroc_pipe_buffer.h"
#include "../lwroc_data_pipe.h"
#include "../f_user_daq/f_user_daq.h"

lwroc_pipe_buffer_control *_pipe_ctrl = NULL;
size_t _pipe_size = 0;

extern struct lwroc_mbs_compat_functions _f_user;

int f_user_early_init(unsigned char crate_number, long *hardware, long *camac,
		      long *status);
int f_user_end(long *hardware, long camac[], int trig15done);

void f_user_pre_parse_setup(void)
{
  /* printm("f_user_pre_parse_setup()."); */
}

int f_user_get_virt_ptr(long *hardware, long camac[])
{
  (void)hardware;
  (void)camac;

  printm("f_user_get_virt_ptr().");

  /* Test setting of an init function that gets called before the
   * event loop.
   */
  _f_user.early_init = f_user_early_init;

  /* Test setting of an endgame function that gets called after
   * the event loop.
   */
  _f_user.end = f_user_end;

  return 0;
}

int f_user_early_init(unsigned char crate_number, long *hardware, long *camac,
		      long *status)
{
  (void)crate_number;
  (void)hardware;
  (void)camac;
  (void)status;

  printm("f_user_early_init().");

  return 0;
}

int f_user_init(unsigned char crate_number, long *hardware, long *camac,
		long *status)
{
  (void)crate_number;
  (void)hardware;
  (void)camac;
  (void)status;

  printm("f_user_init().");

  {
    const char *name = "READOUT_PIPE";
    lwroc_data_pipe_handle *base_handle = lwroc_get_data_pipe(name);

    if (!base_handle)
      LWROC_FATAL_FMT("No handle for data pipe '%s'.", name);

    _pipe_ctrl = lwroc_data_pipe_get_pipe_ctrl(base_handle);
    _pipe_size = lwroc_pipe_buffer_size(_pipe_ctrl);
  }

  return 0;
}

int f_user_end(long *hardware, long camac[], int trig15done)
{
  (void)hardware;
  (void)camac;
  (void)trig15done;

  printm("f_user_end(trig15done=%d).", trig15done);

  return 0;
}

#define ELAPSED(a,b) ((double) ((b).tv_sec - (a).tv_sec) + \
		      0.000001 * ((b).tv_usec - (a).tv_usec))

double t_gettimeofday = 0.0;

#define EVENT_HEADERS_SIZE 28 /* 1 event + 1 subevent header */

int extra_per_event = 0/*245780*//*0*//*9584*//*244*/;
int measure = 0;
int events = 0;
int tot_events = 0;
int check_interval     = 1;
int check_interval_d10 = 1;
struct timeval t_start_interval;

double sum_el  = 0;
double sum_el2 = 0;

double num_fillfrac = 0;
double sum_fillfrac  = 0;
double sum_fillfrac2 = 0;

int f_user_readout(unsigned char trig, unsigned char crate_number,
		   register long *hardware, register long *camac, long *buf,
		   void *subevent, long *bytes_read, long *status)
{
  (void)trig;
  (void)crate_number;
  (void)hardware;
  (void)camac;
  (void)buf;
  (void)subevent;
  (void)bytes_read;
  (void)status;

  /* Release DT early, to hopefully avoid dummy VME accesses. */
  f_user_trig_clear(trig);
  *status = TRIG__CLEARED;

  switch (trig)
    {
    case 14:
      {
	double min_tloop = DBL_MAX;
	struct timeval t_start;
	struct timeval t_end;
	int niter = 1000;
	int i, j;

	/* Start trigger, figure out the time it takes to make a
	 * gettimeofday() call.
	 */

	for (i = 0; i < 10; i++)
	  {
	    double tloop;

	    gettimeofday(&t_start, NULL);
	    for (j = 0; j < niter; j++)
	      gettimeofday(&t_end, NULL);

	    tloop = ELAPSED(t_start, t_end);

	    if (tloop < min_tloop)
	      min_tloop = tloop;
	  }

	t_gettimeofday = min_tloop / niter;

	printm("Time per gettimeofday: %.2f us.\n",
	       t_gettimeofday * 1.e6);

	gettimeofday(&t_start_interval, NULL);
      }
      break;
    case 15:
      break;
    default:
      {
	*bytes_read = extra_per_event;
	events++;

	if (events < check_interval_d10)
	  break;

	tot_events += events;
	events = 0;

	{
	  size_t pipe_fill = lwroc_pipe_buffer_fill(_pipe_ctrl);
	  double frac = (double) pipe_fill / (double) _pipe_size;

	  num_fillfrac++;
	  sum_fillfrac += frac;
	  sum_fillfrac2 += frac * frac;
	}

	if (tot_events < check_interval)
	  break;

	{
	  struct timeval t_now;
	  double elapsed;

	  gettimeofday(&t_now, NULL);
	  elapsed = ELAPSED(t_start_interval, t_now);

	  sum_el  += elapsed;
	  sum_el2 += elapsed*elapsed;

	  switch (measure)
	    {
	    case 0:
	      /* How many events are needed to go through a 100 MB
	       * buffer?
	       */
	      check_interval =
		100000000 / (extra_per_event + EVENT_HEADERS_SIZE);
	      check_interval_d10 = check_interval / 10 + 1;
	      measure = 1;
	      printf("# Burn-in: %d events.\r", check_interval);
	      fflush(stdout);
	      break;
	    case 1:
	      /* We just did the burn-in number of events, to make
	       * conditions stable.
	       */
	      printf("# Burned in... \r");
	      fflush(stdout);
	      measure = 2;
	      break;
	    case 2:
	      if (elapsed < 5)
		{
		  check_interval *= 2;
		  check_interval_d10 = check_interval / 10 + 1;
		  printf("# Scale up... \r");
		  fflush(stdout);
		}
	      else
		{
		  check_interval *= 2;
		  check_interval_d10 = check_interval / 10 + 1;
		  measure = 3;
		  printf("# Scaled up... \r");
		  fflush(stdout);
		}
	      sum_el = 0;
	      sum_el2 = 0;
	      num_fillfrac = 0;
	      sum_fillfrac = 0;
	      sum_fillfrac2 = 0;
	      break;
	    case 3:
	    case 4:
	    case 5:
	      printf("# Measured %d/%d... \r",measure-3+1,4);
	      fflush(stdout);
	      measure++;
	      break;
	    case 6:
	      /* This was the actual measurement. */
	      printm("ES %5d PE %6.3f(%5.3f)us FF %6.4f(%6.4f) Ev%d\n",
		     extra_per_event + EVENT_HEADERS_SIZE,
		     (sum_el / 4) * 1.e6 / tot_events,
		     sqrt((sum_el2-sum_el*sum_el/4)/(4.-1)) *
		     1.e6 / tot_events,
		     sum_fillfrac / num_fillfrac,
		     sqrt((sum_fillfrac2-
			   sum_fillfrac*sum_fillfrac/num_fillfrac)/
			  (num_fillfrac-1)),
		     tot_events);

	      /* Next measurement. */
	      extra_per_event += extra_per_event / 2 + 4;
	      extra_per_event &= ~3;
	      if (extra_per_event > 900000)
		extra_per_event = 0;

	      /* Restart the measurement cycle. */
	      measure = 0;
	      check_interval = 1;
	      check_interval_d10 = check_interval / 10 + 1;
	      break;
	    }
	  tot_events = 0;
	  gettimeofday(&t_start_interval, NULL);
	}
	break;
      }
    }

  return 0;
}

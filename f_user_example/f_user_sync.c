/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include <stdlib.h>
#include <stdio.h>

#include "f_ut_printm.h"

#include "lwroc_triva_kind.h"
#include "lwroc_triva_state.h"

#include "lmd/lwroc_lmd_white_rabbit_stamp.h"

#include "include/trlo_functions.h"
#include "include/trlo_access.h"
#include "../../common/trcom_util.h"

/* The purpose of this readout is to demonstrate the use of the sync
 * check counters and sync check triggers.
 */

volatile trlo_opaque *_trlo = NULL;
void                 *_trlo_unmap = NULL;

int f_user_get_virt_ptr(long *hardware, long camac[])
{
  uint32_t address;

  (void)hardware;
  (void)camac;

  if (_lwroc_triva_config._kind == LWROC_TRIVA_KIND_TRIMI)
    address = _lwroc_triva_config._vmeaddr / 0x01000000;
  else
    address = 9;

  _trlo = trlo_setup_map_hardware(address, &_trlo_unmap);

  /* Read the version again here, was useful for debugging that
   * TRLO_READ resolves to the correct reading method.
   */

  printm("TRLO II version: %08x", TRLO_READ(_trlo, fixed.version_md5sum));

  return 0;
}

int f_user_init(unsigned char crate_number, long *hardware, long *camac,
		long *status)
{
  (void)crate_number;
  (void)hardware;
  (void)camac;
  (void)status;

  return 0;
}

int f_user_readout(unsigned char trig, unsigned char crate_number,
		   register long *hardware, register long *camac, long *buf,
		   void *subevent, long *bytes_read, long *status)
{
  uint32_t sync_check;
  uint32_t ts_low = 0xffffffff, ts_high = 0xffffffff;
  uint32_t ts_ok = 0;
  long *buf_start;

  (void)trig;
  (void)crate_number;
  (void)hardware;
  (void)camac;
  (void)buf;
  (void)subevent;
  (void)bytes_read;
  (void)status;

  buf_start = buf;

  {
    uint32_t ts_status;
    uint32_t checksum;

    ts_status = TRLO_READ(_trlo, out.serial_timestamp_status);

    if ((ts_status & 0x03ff) == 2)
      {
	ts_low  = TRLO_READ(_trlo, serial_tstamp[0]);
	ts_high = TRLO_READ(_trlo, serial_tstamp[0]);
	ts_ok = 1;

	/* printm("a %08x  %08x:%08x", ts_status, ts_high, ts_low); */

	checksum = ts_high ^ ts_low;
	checksum = checksum ^ (checksum >> 16);
	checksum = checksum ^ (checksum >> 8);

	if ((checksum & 0xff) != (ts_status >> 24))
	  printm("TS-CHECKSUM: error (%02x != %02x, %08x:%08x)",
		 (checksum & 0xff), (ts_status >> 24),
		 ts_high, ts_low);
      }
    else
      {
	/* int i; */

	printm("TS-STATUS: %08x (not 2 entries) (trig=%d)", ts_status, trig);
	/*
	  for (i = 0; i < (ts_status & 0x03ff); i++)
	    printm("Entry %d: %08x\n", i, TRLO_READ(_trlo, serial_tstamp[0]));
	*/
	printm("clear TS buffer");
	TRLO_WRITE(_trlo, pulse.pulse, TRLO_PULSE_SERIAL_TSTAMP_BUF_CLEAR);
	SERIALIZE_IO;
	/*
	  printm("status: %08x", TRLO_READ(_trlo, out.serial_timestamp_status));
	*/
      }

    if (ts_status & (0x8000 | 0x20000 | 0x80000 | 0x00f00000))
      {
	printm("TS-STATUS: %08x (bad bits) (trig=%d)", ts_status, trig);
	printm("clear TS error");
	TRLO_WRITE(_trlo, pulse.pulse, TRLO_PULSE_SERIAL_TSTAMP_FAIL_CLEAR);
	SERIALIZE_IO;
      }
  }

  sync_check = TRLO_READ(_trlo, out.trig_sync_check);

  {
    uint32_t *wr_stamp = (uint32_t *) buf;

    /* First data of subevent: WR timestamp. */

    wr_stamp[0] = ((crate_number & 0xff) << 8) |
      (ts_ok ? 0 : WHITE_RABBIT_STAMP_EBID_ERROR);
    wr_stamp[1] = WHITE_RABBIT_STAMP_LL16_ID | ((ts_low  >>  0) & 0xffff);
    wr_stamp[2] = WHITE_RABBIT_STAMP_LH16_ID | ((ts_low  >> 16) & 0xffff);
    wr_stamp[3] = WHITE_RABBIT_STAMP_HL16_ID | ((ts_high >>  0) & 0xffff);
    wr_stamp[4] = WHITE_RABBIT_STAMP_HH16_ID | ((ts_high >> 16) & 0xffff);

    buf += 5;
  }
  {
    /* Directly after WR timestamp: sync check info. */

    uint32_t *sync_stamp = (uint32_t *) buf;
    uint32_t sync_mark;

    if (crate_number == 1)
      sync_mark = SYNC_CHECK_REF   | ((sync_check >> 16) & 0x3f);
    else
      sync_mark = SYNC_CHECK_RECV  | (sync_check & 0xffff);

    sync_stamp[0] = SYNC_CHECK_MAGIC | sync_mark;

    buf += 1;
  }

  /*
  printf ("%08x : %2d (%2d:%d) %5d\n",
	  sync_check,
	  ((sync_check >> 16) & 0x3f),
	  ((sync_check >> 16) & 0x3f) >> 2,
	  ((sync_check >> 16) & 0x3f) & 3,
	  sync_check & 0xffff);
  */

  *bytes_read = ((char *) buf) - ((char *) buf_start);

  return 0;
}

/*
Master/sender:

export TRLOLIB_DIR=/XXX/trloii/trloctrl/fw_XXXXXXXX_trlo
setenv TRLOLIB_DIR /XXX/trloii/trloctrl/fw_XXXXXXXX_trlo

${TRLOLIB_DIR}/bin_`bin/drasi-config.sh --arch-prefix`/trlo_ctrl --addr=3 \
  --config=f_user_example/f_user_sync.trlo \
  --clear-setup \
  common sender \
  --print-config

${TRLOLIB_DIR}/../../bin/trimictrl --addr=3 "encoded_in=TRLO_ENCODED_TRIG"

bin/f_user_sync --buf=size=10M --trimi=master,@3 \
  --log-no-start-wait --subev=type=10,subtype=1,subcrate=1,control=1 \
  --server=trans --server=drasi,dest=192.168.1.11 --label=SEND

Slave:

${TRLOLIB_DIR}/bin_`bin/drasi-config.sh --arch-prefix`/trlo_ctrl --addr=2 \
  --config=f_user_example/f_user_sync.trlo \
  --clear-setup \
  common receiver \
  --print-config

${TRLOLIB_DIR}/../../bin/trimictrl --addr=2 "encoded_in=TRLO_ENCODED_TRIG"

bin/f_user_sync --buf=size=10M --trimi=master,@2 \
  --log-no-start-wait --subev=type=10,subtype=1,subcrate=2,control=1 \
  --server=trans --server=drasi,dest=192.168.2.1 --label=RECV

Time sorter:

bin/lwrocmerge \
  --buf=size=100M --drasi=192.168.2.80 --drasi=192.168.1.42 \
  --merge-mode=wr --log-no-start-wait --max-ev-size=100000 \
  --merge-ts-analyse-ref=1 --merge-ts-analyse-sync-trig=3 \
  --server=trans --label=TS

*/

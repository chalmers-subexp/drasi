/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2020  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_filter.h"
#include "lwroc_message.h"
#include "lwroc_mon_block.h"
#include "lwroc_filter_loop.h"
#include "lmd/lwroc_lmd_util.h"

#include <string.h>

void filter_example_loop(lwroc_pipe_buffer_consumer *pipe_buf,
			 const lwroc_thread_block *thread_block,
			 lwroc_data_pipe_handle *data_handle,
			 volatile int *terminate);

void lwroc_user_filter_pre_setup_functions(void)
{
  _lwroc_user_filter_functions->loop = filter_example_loop;
  _lwroc_user_filter_functions->name = "filter_example";
}

/*****************************************************************************/

void filter_example_loop(lwroc_pipe_buffer_consumer *pipe_buf,
			 const lwroc_thread_block *thread_block,
			 lwroc_data_pipe_handle *data_handle,
			 volatile int *terminate)
{
  const lmd_event_10_1_host *header;
  size_t size, write_size;
  lwroc_iterate_event iter_data;
  int ret;
  char *dest;

  for ( ; ; )
    {
      /****************************************************************/
      /* Get next event from the source.                              */

      ret = lwroc_lmd_pipe_get_event(pipe_buf, thread_block,
				     &header, &size, &iter_data);

      if (ret == LWROC_LMD_PIPE_GET_EVENT_FAILURE)
	{
	  LWROC_BUG("Filter failed to get event.");
	}
      if (ret == LWROC_LMD_PIPE_GET_EVENT_WOULD_BLOCK)
	{
	  struct timeval timeout;
	  if (*terminate)
	    break; /* No more data, and asked to quit. */
	  /* Try again in 0.1 s. */
	  timeout.tv_sec = 0;
	  timeout.tv_usec = 100000;
	  lwroc_thread_block_get_token_timeout(thread_block, &timeout);
	  goto update_monitor;
	}

      /* End of source fetching.                                      */
      /****************************************************************/

      /****************************************************************/
      /* Sticky events shall generally just be passed along.          */

      if (ret == LWROC_LMD_PIPE_GET_EVENT_STICKY ||
	  ret == LWROC_LMD_PIPE_GET_EVENT_INTERNAL)
	goto emit_event;

      /****************************************************************/
      /* The event data can now be used:
       *
       * To just pass the event along: goto emit_event;
       *
       * To ignore (drop) it: goto event_used;
       *
       * To write a new event:
       * - call lwroc_request_event_space() to allocate destination space,
       * - fill the destination, and
       * - call lwroc_used_event_space().
       *
       * The source buffer may NOT be modified under any circumstance!
       * (It may also be used for other data destinations.)
       *
       * Flushing should be used sparingly - only for special events.
       * (The buffer readers also have time-based flushing.)
       */
      /*****************************************************************/

      /* printf ("Event: %zd\n", size); */
      /* goto event_used; */

    emit_event:
      /* From here on we just emit the (original) event. */

      write_size = size;

      /****************************************************************/
      /* Get space in the destination buffer.                         */
      dest = lwroc_request_event_space(data_handle,
				       (uint32_t) write_size);

      /* Copy the event data. */
      memcpy(dest, header, write_size);

      /* Data has been written to the destination.
       * Will not be modified any further.
       */
      lwroc_used_event_space(data_handle,
			     (uint32_t) write_size,
			     0 /* no flush */);

      /* End of destination handling.                                 */
      /****************************************************************/

    /* event_used: */
      /****************************************************************/
      /* Source data has been used, will not use it any more!         */

      lwroc_pipe_buffer_did_read(pipe_buf, size);

      /* End of source handling.                                      */
      /****************************************************************/

    update_monitor:
      LWROC_FILTER_MON_CHECK(LWROC_FILTER_USER, 0 /* flush */);
    }
}

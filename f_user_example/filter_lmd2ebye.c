/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2024  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

/* *** This is *not* intended as example code. ***
 *
 * The purpose of this file is to test the EBYE event header generation.
 * The LMD payload data is discarded!
 */

#include <math.h>
#include <time.h>
#include <unistd.h>

#include "lwroc_filter.h"
#include "lwroc_message.h"
#include "lwroc_mon_block.h"
#include "lwroc_filter_loop.h"
#include "lwroc_parse_util.h"
#include "lmd/lwroc_lmd_util.h"
#include "ebye/lwroc_ebye_util.h"
#include "../dtc_arch/acc_def/myinttypes.h"
#include "../dtc_arch/acc_def/myprizd.h"

#include "lwroc_heimtime.c"

#include "lwroc_accum_sort.c"

#include <errno.h>
#include <string.h>
#include <assert.h>

/*****************************************************************************/

/* The data flows through the accumulate-sort functionality per channel.
 *
 * This allows data from different channels to be slightly out-of order,
 * as well as data from different modules to be much out-of-order.
 *
 * We however expect that data that came from two different events
 * with another in-between to be ordered.  I.e., knowing the last
 * timestamp seen two events ago, hits until that timestamp can be
 * emitted.
 */

lwroc_accum_many   *_lwroc_accum_many = NULL;
lwroc_accum_buffer *_lwroc_accum_dams = NULL;
lwroc_accum_buffer *_lwroc_accum_ht_t = NULL;

/*****************************************************************************/

typedef struct ht_t_sample_t {
  uint64_t ht;
  uint64_t t;
} ht_t_sample;

static struct {
  pthread_t	thread;
} g_warp;

/* Queue of ht-t sample pairs from packet reader to sorting thread. */
lwroc_pipe_buffer_control  *_ht_t_pairs_queue = NULL;

/* Max number of entries in queue. */
#define LWROC_HT_T_PAIRS_BUF_ENTRIES  0x10000 /* At least some 300 s. */

#define LENGTH(x) (sizeof (x) / sizeof *(x))

void *
warp_run(void *dummy)
{
  struct sockaddr_in addr;
  int sock;
  lwroc_thread_block *thread_notify;

  thread_notify = lwroc_thread_block_init();

  sock = socket(AF_INET, SOCK_DGRAM, 0);
  if (sock < 0)
    LWROC_FATAL_FMT("socket: %s", strerror(errno));
  memset(&addr, 0, sizeof addr);
  addr.sin_family = AF_INET;
  addr.sin_addr.s_addr = INADDR_ANY;
  addr.sin_port = htons(10000);
  if (bind(sock, (void *)&addr, sizeof addr) < 0)
    LWROC_FATAL_FMT("bind: %s", strerror(errno));
  /* Insert dummy mapping points. */
  /*
  {
    ht_t_sample sample;
    for (sample.ht = 0;
	 sample.ht <  0x1000000000000000;
	 sample.ht += 0x10000000000000)
    {
      ht_t_sample *entry;

      sample.t = sample.ht * 16;

      entry = (ht_t_sample *)
	lwroc_pipe_buffer_alloc_write(_ht_t_pairs_queue,
				      sizeof (ht_t_sample),
				      NULL,
				      thread_notify,
				      1);

      memcpy(entry, &sample, sizeof (sample));



      lwroc_pipe_buffer_did_write(_ht_t_pairs_queue,
				  sizeof (ht_t_sample), 0);
    }
  }
  */
  for (;;) {
    ht_t_sample sample;
    ssize_t r;

    r = recv(sock, &sample, sizeof sample, 0);
    if (r < 0)
      {
	printf("recv: %s", strerror(errno));
	continue;
      }
    /* sample.ht <<= 19; */
    /* sample.ht *= 1000; */
    /*
    printf("Sample ht=0x%016" PRIx64 " caen=0x%016" PRIx64 "\n",
	   sample.ht,
	   sample.t);
    */
    {
      ht_t_sample *entry;

      entry = (ht_t_sample *)
	lwroc_pipe_buffer_alloc_write(_ht_t_pairs_queue,
				      sizeof (ht_t_sample),
				      NULL,
				      thread_notify,
				      1);

      memcpy(entry, &sample, sizeof (sample));

      if (sample.ht == 0 || sample.t == 0)
	printf ("Got pair with zero ht or t time from UDP!\n");

      lwroc_pipe_buffer_did_write(_ht_t_pairs_queue,
				  sizeof (ht_t_sample), 0);
    }
  }
  return NULL;
}

/*****************************************************************************/

void filter_lmd2ebye_cmdline_usage(void);
int  filter_lmd2ebye_parse_cmdline_arg(const char *arg);

void filter_lmd2ebye_setup(void);
void filter_lmd2ebye_loop(lwroc_pipe_buffer_consumer *pipe_buf,
			  const lwroc_thread_block *thread_block,
			  lwroc_data_pipe_handle *data_handle,
			  volatile int *terminate);
void filter_lmd2ebye_max_ev_len(uint32_t *max_len,
				uint32_t *add_len);

void lwroc_user_filter_pre_setup_functions(void)
{
  _lwroc_user_filter_functions->setup = filter_lmd2ebye_setup;
  _lwroc_user_filter_functions->loop = filter_lmd2ebye_loop;
  _lwroc_user_filter_functions->name = "filter_lmd2ebye";
  _lwroc_user_filter_functions->cmdline_fcns.usage =
    filter_lmd2ebye_cmdline_usage;
  _lwroc_user_filter_functions->cmdline_fcns.parse_arg =
    filter_lmd2ebye_parse_cmdline_arg;
  /* Specify output format, since we do not generate the same output
   * as input:
   */
  _lwroc_user_filter_functions->fmt  = &_lwroc_ebye_format_functions;
  _lwroc_user_filter_functions->max_ev_len =
    filter_lmd2ebye_max_ev_len;
}

/*****************************************************************************/

int _filter_lmd2ebye_ht2t        = 0;
int _filter_lmd2ebye_ht2t_debug  = 0;
int _filter_lmd2ebye_zero_qshort = 0;

void filter_lmd2ebye_cmdline_usage(void)
{
  printf ("  --ht-to-t                Transform Heimtime to ISS time.\n");
  printf ("                           Requires sideline mapping from CAEN DAQ data.\n");
  printf ("  --ht-to-t-debug          Print transformation debug info.\n");
  printf ("  --zero-qshort            Also emit qshort for fixed 0 items.\n");
  printf ("\n");
}

int filter_lmd2ebye_parse_cmdline_arg(const char *request)
{
  /* const char *post; */

  /*NOHELPCALL*/ /* We are a option subset, --help handled elsewhere. */

  if (LWROC_MATCH_C_ARG("--ht-to-t")) {
    _filter_lmd2ebye_ht2t = 1;
  }
  else if (LWROC_MATCH_C_ARG("--ht-to-t-debug")) {
    _filter_lmd2ebye_ht2t_debug = 1;
  }
  else if (LWROC_MATCH_C_ARG("--zero-qshort")) {
    _filter_lmd2ebye_zero_qshort = 1;
  }
  else
    return 0;

  return 1;
}

/*****************************************************************************/

#include "gen/lwroc_message_source_serializer.h"
#include "gen/lwroc_monitor_dams_source_serializer.h"
#include "gen/lwroc_monitor_dams_block_serializer.h"
#include "gen/lwroc_monitor_dam_source_serializer.h"
#include "gen/lwroc_monitor_dam_block_serializer.h"

void filter_lmd2ebye_monitor_fetch(void *ptr);

typedef struct lwroc_monitor_dams_dam_block_t
{
  lwroc_monitor_dams_block  _dams;
  lwroc_monitor_dam_block   _dam[1 /* actually more */];
} lwroc_monitor_dams_dam_block;

lwroc_monitor_dams_dam_block *_lwroc_mon_dams_dam;
lwroc_mon_block              *_lwroc_mon_dams_dam_handle = NULL;
uint32_t                      _lwroc_mon_dam_num = 0;

char *lwroc_mon_dams_dam_serialise(char *wire, void *block)
{
  lwroc_monitor_dams_dam_block *dams_dam_block =
    (lwroc_monitor_dams_dam_block *) block;
  uint32_t i;

  wire = lwroc_monitor_dams_block_serialize(wire, &dams_dam_block->_dams);

  for (i = 0; i < _lwroc_mon_dam_num; i++)
    wire =
      lwroc_monitor_dam_block_serialize(wire, &dams_dam_block->_dam[i]);

  return wire;
}

#define FILTER_TYPE_DAM   1
#define FILTER_TYPE_HT_T  2

void filter_lmd2ebye_accum_report_ins_backw(uint32_t type, uint32_t id,
					    lwroc_accum_item_base *item,
					    uint64_t last_ts)
{
  LWROC_WARNING_FMT("Insert timestamp backwards for ID %d:%d "
		    "(%016" PRIx64 " - %016" PRIx64 " = %" PRId64 ").",
		    type, id,
		    item->_ts, last_ts,
		    (int64_t) (item->_ts - last_ts));
}

void filter_lmd2ebye_accum_report_pick_backw(uint32_t type, uint32_t id,
					     lwroc_accum_item_base *item,
					     uint64_t last_ts)
{
  LWROC_WARNING_FMT("Picked timestamp backwards for ID %d:%d "
		    "(%016" PRIx64 " - %016" PRIx64 " = %" PRId64 ").",
		    type, id,
		    item->_ts, last_ts,
		    (int64_t) (item->_ts - last_ts));
}

void filter_lmd2ebye_accum_report_resorted(uint32_t type, uint32_t id,
					   size_t n)
{
  LWROC_WARNING_FMT("Resorted for ID %d:%d, %" MYPRIzd " items.",
		    type, id, n);
}

void filter_lmd2ebye_emit_item(uint32_t type, uint32_t id,
			       lwroc_accum_item_base *item,
			       void *arg);

void filter_lmd2ebye_process_ht_t(uint32_t type, uint32_t id,
				  lwroc_accum_item_base *item,
				  void *arg);

void filter_lmd2ebye_setup(void)
{
  int ret;

  /* Monitor information. */

  /* TODO: for now, we have statically allocated one for each 16 possible.
   * Needs to change if we have many more...
   */
  _lwroc_mon_dam_num = 64;

  {
    lwroc_message_source dams_src_source;
    lwroc_message_source_sersize sz_dams_src_source;
    lwroc_monitor_dams_source dams_src;
    char *wire;
    size_t sz_mon_dams;
    uint32_t mon_idx = 0;

    dams_src._dam_blocks = _lwroc_mon_dam_num;

    lwroc_mon_source_set(&dams_src_source);

    /* The size ends where the dams writer blocks end. */
    sz_mon_dams = offsetof(lwroc_monitor_dams_dam_block,
			   _dam[_lwroc_mon_dam_num]);

    _lwroc_mon_dams_dam = malloc (sz_mon_dams);

    if (!_lwroc_mon_dams_dam)
      LWROC_FATAL("Failure allocating memory for DAMs monitor.");

    memset (_lwroc_mon_dams_dam, 0, sz_mon_dams);
    for ( ; mon_idx < _lwroc_mon_dam_num; )
      {
	lwroc_monitor_dam_block *mon =
	  &_lwroc_mon_dams_dam->_dam[mon_idx++];
	/*
	mon->_src_index_mask = 0;
	mon->_ts_ref_off_sig_k = (uint64_t) -2;
	*/
	mon->_id = (uint32_t) -1;
      }

    _lwroc_mon_dams_dam_handle =
      lwroc_reg_mon_block(0, &_lwroc_mon_dams_dam->_dams, sz_mon_dams,
			  lwroc_monitor_dams_block_serialized_size() +
			  _lwroc_mon_dam_num *
			  lwroc_monitor_dam_block_serialized_size(),
			  lwroc_mon_dams_dam_serialise,
			  lwroc_message_source_serialized_size(&dams_src_source,
							       &sz_dams_src_source) +
			  lwroc_monitor_dams_source_serialized_size(),
			  filter_lmd2ebye_monitor_fetch,
			  NULL);

    wire = _lwroc_mon_dams_dam_handle->_ser_source;

    wire = lwroc_message_source_serialize(wire, &dams_src_source,
					  &sz_dams_src_source);
    wire = lwroc_monitor_dams_source_serialize(wire, &dams_src);

    assert (wire == (_lwroc_mon_dams_dam_handle->_ser_source +
		     _lwroc_mon_dams_dam_handle->_ser_source_size));
  }

  lwroc_pipe_buffer_init(&_ht_t_pairs_queue,
			 1, NULL,
			 LWROC_HT_T_PAIRS_BUF_ENTRIES * sizeof (ht_t_sample),
			 10);

   _lwroc_accum_many =
    lwroc_accum_many_init(2);

  if (!_lwroc_accum_many)
    LWROC_FATAL("Failure allocating memory for accumulation envelope.");

  _lwroc_accum_dams =
    lwroc_accum_init(_lwroc_accum_many, 0,
		     FILTER_TYPE_DAM,
		     _lwroc_mon_dam_num * 64,
		     filter_lmd2ebye_accum_report_ins_backw,
		     filter_lmd2ebye_accum_report_pick_backw,
		     filter_lmd2ebye_accum_report_resorted,
		     filter_lmd2ebye_emit_item);

  if (!_lwroc_accum_dams)
    LWROC_FATAL("Failure allocating memory for DAMs accumulation.");

  /* We can allocate even if we do not do the mapping.
   * Just do not wait for data from this source.
   */
  _lwroc_accum_ht_t =
    lwroc_accum_init(_lwroc_accum_many, 1,
		     FILTER_TYPE_HT_T,
		     1 /* Just one timescale mapping ht->t. */,
		     filter_lmd2ebye_accum_report_ins_backw,
		     filter_lmd2ebye_accum_report_pick_backw,
		     filter_lmd2ebye_accum_report_resorted,
		     filter_lmd2ebye_process_ht_t);

  ret = lwroc_accum_many_post_init(_lwroc_accum_many);

  if (!ret)
    LWROC_FATAL("Failure allocating memory for accumulation envelope "
		"post-init.");

  if (_filter_lmd2ebye_ht2t)
    {
      int rc;

      rc = pthread_create(&g_warp.thread, NULL, warp_run, NULL);
      if (0 != rc)
	LWROC_FATAL_FMT("pthread_create: %s", strerror(rc));
    }
}

/*****************************************************************************/

void filter_lmd2ebye_max_ev_len(uint32_t *max_len,
				   uint32_t *add_len)
{
  /* Maximum generated event size: */

  *max_len = 8 * sizeof(uint32_t);
  (void) add_len;
}

/*****************************************************************************/

typedef struct mod_statistics_t
{
  uint64_t _ht_hits;
  uint64_t _hits;

  uint64_t _hits_bad;
  uint64_t _hits_missed;

  uint64_t _hits_filt;
  uint64_t _hits_drop;

} mod_statistics;

/*****************************************************************************/

typedef struct ht_track_info_t
{
  uint64_t                _ts1_cur;
  uint64_t                _discarded_hits;

} ht_track_info;

/*****************************************************************************/

lwroc_heimtime_state _ht_state[64];
ht_track_info        _ht_info[64];
mod_statistics       _stats[64];

uint64_t             _pairs_count = 0;
uint64_t             _pairs_cur = 0;

void filter_lmd2ebye_monitor_fetch(void *ptr)
{
  lwroc_monitor_dams_dam_block *dams_dam_mon =
    (lwroc_monitor_dams_dam_block *) ptr;
  uint32_t i;

  for (i = 0; i < _lwroc_mon_dam_num; i++)
    {
      lwroc_monitor_dam_block *mon =
	&(dams_dam_mon->_dam[i]);

      if (_stats[i]._hits ||
	  _stats[i]._ht_hits)
	mon->_id = i;
      /*
      mon->_time._sec  = (uint64_t) now.tv_sec;
      mon->_time._nsec = (uint32_t) now.tv_usec * 1000;
      */
      mon->_trig_sig    = _stats[i]._ht_hits;
      mon->_hits        = _stats[i]._hits;
      mon->_hits_bad    = _stats[i]._hits_bad;
      mon->_hits_missed = _stats[i]._hits_missed;
      mon->_hits_filt   = _stats[i]._hits_filt;
      mon->_hits_drop   = _stats[i]._hits_drop;
    }
}

/*****************************************************************************/

#define ACCUM_ITEM_HT_T_FLAGS_SET      0x01  /* Value is actually set. */
#define ACCUM_ITEM_HT_T_FLAGS_DISCARD  0x02  /* Discard hits. */

typedef struct accum_item_ht_t_t
{
  lwroc_accum_item_base _base;

  ht_t_sample           _pair[2];
  int                   _flags;

} accum_item_ht_t;

typedef struct lmd2ebye_process_info_t
{
  lwroc_data_pipe_handle *_data_handle;
  accum_item_ht_t         _ht_t;
  uint64_t                _discarded_hits;

} lmd2ebye_process_info;

void filter_lmd2ebye_process_ht_t(uint32_t type, uint32_t id,
				  lwroc_accum_item_base *item,
				  void *arg)
{
  /* This function is called when a new ht->t interpolation range is
   * entered.
   *
   * It starts at the first pair, and ends at the second.  It
   * therefore is sorted as the first time, and we can then sort until
   * the second time.
   */

  accum_item_ht_t *item_ht_t = (accum_item_ht_t *) item;

  lmd2ebye_process_info *info = (lmd2ebye_process_info *) arg;

  (void) type;

  info->_ht_t = *item_ht_t;

  if (0)
    printf ("pair: "
	    "%016" PRIx64 " %016" PRIx64 " (%016" PRId64 ") -> "
	    "%016" PRIx64 " %016" PRIx64 " (%016" PRId64 ") \n",
	    info->_ht_t._pair[0].ht, info->_ht_t._pair[1].ht,
	    info->_ht_t._pair[1].ht- info->_ht_t._pair[0].ht,
	    info->_ht_t._pair[0].t , info->_ht_t._pair[1].t,
	    info->_ht_t._pair[1].t - info->_ht_t._pair[0].t);

  _pairs_count++;
}

typedef struct accum_item_dam_t
{
  lwroc_accum_item_base _base;

  uint32_t              _t;
  uint16_t              _adc;
  uint16_t              _ov_pu;

} accum_item_dam;

void filter_lmd2ebye_emit_item(uint32_t type, uint32_t id,
			       lwroc_accum_item_base *item,
			       void *arg)
{
  accum_item_dam *item_dam = (accum_item_dam *) item;
  lmd2ebye_process_info *info = (lmd2ebye_process_info *) arg;
  lwroc_data_pipe_handle *data_handle = info->_data_handle;
  char *dest;
  uint32_t *u32;
  uint32_t mod;
  uint32_t ch;
  uint64_t ref_t;
  uint32_t adc;
  uint32_t ov, pu;
  uint32_t t_fine = 0;

  (void) type;

  mod = (id >> 6) & 0x3f;
  ch  = (id     ) & 0x3f;

  ref_t = item_dam->_base._ts;
  adc   = item_dam->_adc;
  ov    = (item_dam->_ov_pu     ) & 1;
  pu    = (item_dam->_ov_pu >> 1) & 1;

  if (0)
    printf ("%3d %3d %d %d %5d  %20" PRIu64 "\n",
	    mod, ch, ov, pu, adc, ref_t);

  /* Provided the sorting has done its job, and data is in order, then
   * the hit will have a heimtime that is at most as new as the
   * second value in the mapping pair.  It may be earlier than the
   * first in case the data was older than the first mapping received.
   * In that case we extrapolate for those earlier timestamps.
   *
   * There will always be a mapping, since sorting does not proceed until
   * a heimtime mapping has been seen.
   */

  if (_filter_lmd2ebye_ht2t)
    {
      if (!(info->_ht_t._flags & ACCUM_ITEM_HT_T_FLAGS_SET))
	{
	  /* Cannot perform mapping without pair.  Drop item. */
	  if (!info->_discarded_hits)
	    LWROC_ERROR("Discarding hits - no ht->t mapping.");
	  info->_discarded_hits++;
	  _stats[mod]._hits_drop++;
	  return;
	}

      {
	uint64_t ht = ref_t;
	ht_t_sample *s = info->_ht_t._pair;
	uint64_t t;
	double t_offset;

      if ((info->_ht_t._flags & ACCUM_ITEM_HT_T_FLAGS_DISCARD) ||
	  (ref_t < s[0].ht) ||
	  (ref_t > s[1].ht))
	{
	  /* Discarding (dropping) item. */
	  if (!info->_discarded_hits)
	    LWROC_ERROR("Discarding hits - unusable ht->t mapping.");
	  info->_discarded_hits++;
	  _stats[mod]._hits_drop++;
	  return;
	}

      /*
	printf("ref_t=%08x ht=%08x\n", ref_t, ht);
	puts("");
      */

	t_offset =
	  (double) (int64_t) (ref_t - s[0].ht) *
	  (double) (s[1].t - s[0].t) /
	  (double) (s[1].ht - s[0].ht);
	t = (uint64_t) ((int64_t) s[0].t + (int64_t) t_offset);

	t_fine = (uint32_t) (t & 0x3ff);
	t      = t >> 10;

	if (0)
	  printf("[0x%016" PRIx64 " <= 0x%016"   PRIx64 " < 0x%016" PRIx64 "], "
		 "[0x%016" PRIx64 " <= < 0x%016" PRIx64 "] -> 0x%016" PRIx64 ""
		 "\n",
		 s[0].ht,
		 ht,
		 s[1].ht,
		 s[0].t,
		 s[1].t,
		 t);

	ref_t = t;
      }
    }

  /****************************************************************/
  /* Get space in the destination buffer.                         */

  /* Worst case, 4 data items, plus 2 timestamps. */
  dest = lwroc_request_event_space(data_handle,
				   6 * 2 * sizeof (uint32_t));

  u32 = (uint32_t *) dest;

  /* Generate as an info 5 and 4 timestamp items,
   * followed by and CAEN type value.
   */

#if 0
  u32[0] =
    0x80000000 |                 /* 31..29  Info type marker     */
    (mod << 24) |                /* 29..24  channel high: module */
    (5 << 20) |                  /* 23..20  info type            */
    ((ref_t >> 48) & 0xffff);    /* 15.. 0 (timestamp 63..48)    */
  u32[1] = (0x0fffffff & ref_t); /* timestamp (same as below!)   */
  u32 += 2;
#endif

  u32[0] =
    0x80000000 |                 /* 31..29  Info type marker     */
    (mod << 24) |                /* 29..24  channel high: module */
    (4 << 20) |                  /* 23..20  info type            */
    ((ref_t >> 28) & 0xfffff);   /* 19.. 0 (timestamp 47..28)    */
  u32[1] = (0x0fffffff & ref_t); /* timestamp (same as below!)   */
  u32 += 2;

  u32[0] =
    0xc0000000 |                 /* 31..29  CAEN type marker     */
    (mod << 24) |                /* 28..24  channel high: module */
    (0 << 22) |                  /* 23..22  channel mid: data id */
    (ch << 16) |                 /* 21..16  channel low: channel */
    adc;                         /* 15.. 0  ADC value (Qlong)    */
  u32[1] = (0x0fffffff & ref_t); /* timestamp                    */
  u32 += 2;

  if (_filter_lmd2ebye_zero_qshort)
    {
      u32[0] =
	0xc0000000 |                 /* 31..29  CAEN type marker     */
	(mod << 24) |                /* 28..24  channel high: module */
	(1 << 22) |                  /* 23..22  channel mid: data id */
	(ch << 16) |                 /* 21..16  channel low: channel */
	0;                           /* 15.. 0  ADC value (Qshort)   */
      u32[1] = (0x0fffffff & ref_t); /* timestamp                    */
      u32 += 2;
    }

  u32[0] =
    0xc0000000 |                 /* 31..29  CAEN type marker     */
    (mod << 24) |                /* 28..24  channel high: module */
    (3 << 22) |                  /* 23..22  channel mid: data id */
    (ch << 16) |                 /* 21..16  channel low: channel */
    t_fine;                      /* 15.. 0  ADC value (fine time)*/
  u32[1] = (0x0fffffff & ref_t); /* timestamp                    */
  u32 += 2;

  (void) ov;
  (void) pu;

  /* Data has been written to the destination.
   * Will not be modified any further.
   */
  lwroc_used_event_space(data_handle,
			 ((char *) u32) - ((char *) dest),
			 0 /* no flush */);

  /* End of destination handling.                                 */
  /****************************************************************/

  if (info->_discarded_hits)
    {
      LWROC_WARNING_FMT("%" PRIu64" hits were discarded, "
			"due to no ht->t mapping.",
			info->_discarded_hits);
      LWROC_INFO("Hit propagated.");
      info->_discarded_hits = 0;
    }

  _stats[mod]._hits_filt++;
}

/*****************************************************************************/

void filter_accum_flush(lmd2ebye_process_info *info)
{
  LWROC_INFO("Flushing accumulation-sort buffer!  "
	     "Discard items outside ht-t pairs!");

  /* Insert a catch-all remaining ht-t pair that discards all hits
   * remaining after the last pair that has been inserted.
   */
  if (_filter_lmd2ebye_ht2t)
    {
      accum_item_ht_t ht_t_insert;

      ht_t_insert._pair[0].ht = 0;
      ht_t_insert._pair[1].ht = (uint64_t) -1;
      ht_t_insert._pair[0].t  = 0;
      ht_t_insert._pair[1].t  = (uint64_t) -1;
      ht_t_insert._flags = ACCUM_ITEM_HT_T_FLAGS_DISCARD;

      lwroc_accum_insert_item(_lwroc_accum_ht_t,
			      0,
			      &ht_t_insert._base,
			      sizeof (ht_t_insert));
    }

  lwroc_accum_sort_until(_lwroc_accum_many, (uint64_t) -1,
			 info);

  /* Drop any items until the first mapping pair.
   * There is no mapping pair until the first that comes.
   */
  info->_ht_t._flags = 0;
}

/*****************************************************************************/

void filter_lmd2ebye_loop(lwroc_pipe_buffer_consumer *pipe_buf,
			  const lwroc_thread_block *thread_block,
			  lwroc_data_pipe_handle *data_handle,
			  volatile int *terminate)
{
  const lmd_event_10_1_host *header;
  size_t size;
  lwroc_iterate_event iter_data;
  int ret;
  int update_seq = 0;

  lwroc_pipe_buffer_consumer *ht_t_pairs_queue_consumer = NULL;

  lmd2ebye_process_info process_info;

  accum_item_ht_t ht_t_insert;

  /* uint32_t raw_t_prev[64][64]; */
  uint64_t ref_t_prev[64][64];

  uint64_t max_ref_t[4] = { 0, 0, 0, 0 };

  memset (&_ht_state, 0, sizeof (_ht_state));
  memset (&_ht_info,  0, sizeof (_ht_info));
  memset (&_stats,    0, sizeof (_stats));

  memset (ref_t_prev, 0, sizeof (ref_t_prev));

  ht_t_pairs_queue_consumer =
    lwroc_pipe_buffer_get_consumer(_ht_t_pairs_queue, 0);

  memset(&ht_t_insert, 0, sizeof (ht_t_insert));

  memset(&process_info, 0, sizeof (process_info));
  process_info._data_handle = data_handle;

  for ( ; ; )
    {
      const lmd_subevent_10_1_host *subev_header;
      lwroc_iterate_event sub_data;
      const uint32_t *lmd_subev_data = NULL;

      /****************************************************************/
      /* See if we have any ht-t items in the queue.                  */

      if (_filter_lmd2ebye_ht2t)
      {
	char *ptr;
	size_t avail;

	avail = lwroc_pipe_buffer_avail_read(ht_t_pairs_queue_consumer,
					     thread_block,
					     &ptr, NULL, 0, 0);

	if (avail)
	  {
	    /* When we get a new mapping point, we can insert it as
	     * the second part of the previous point.
	     */

	    ht_t_sample *sample = (ht_t_sample *) ptr;
	    double d_t_d_ht_prev;
	    double d_t_d_ht_this;
	    double slope_diff;
	    double d_t_this;

	    d_t_d_ht_prev =
	      ((double) (ht_t_insert._pair[1].t  - ht_t_insert._pair[0].t)) /
	      ((double) (ht_t_insert._pair[1].ht - ht_t_insert._pair[0].ht));
	    d_t_d_ht_this =
	      ((double) (sample->t  - ht_t_insert._pair[1].t)) /
	      ((double) (sample->ht - ht_t_insert._pair[1].ht));
	    slope_diff =
	      /* */  (d_t_d_ht_this - d_t_d_ht_prev) /
	      (0.5 * (d_t_d_ht_this + d_t_d_ht_prev));
	    d_t_this = (double) (sample->t  - ht_t_insert._pair[1].t);

	    if (_filter_lmd2ebye_ht2t_debug ||
		d_t_this * 4. / 1024. > 100000000.)
	      {
		printf("Sample ht=0x%016" PRIx64 ", "
		       "iss t=0x%016" PRIx64 "\n",
		       sample->ht,
		       sample->t);
		printf("Sample d_ht=0x%016" PRId64 ", "
		       "iss d_t=0x%016" PRId64 "\n",
		       (int64_t) (sample->ht - ht_t_insert._pair[1].ht),
		       (int64_t) (sample->t  - ht_t_insert._pair[1].t));
	      }

	    ht_t_insert._pair[0] = ht_t_insert._pair[1];
	    ht_t_insert._pair[1] = *sample;
	    ht_t_insert._flags = ACCUM_ITEM_HT_T_FLAGS_SET;

	    /* Sort as ht of first part of pair.  Such that it can be
	     * used from that time, until the second part of the pair.
	     */
	    ht_t_insert._base._ts = ht_t_insert._pair[0].ht;

	    if (sample->ht <= ht_t_insert._pair[0].ht)
	      {
		LWROC_ERROR_FMT("ht->t mapping ht time goes backwards, "
				"0x%016" PRIx64 " < 0x%016" PRIx64 ".",
				sample->ht, ht_t_insert._pair[1].ht);

		/* Do not insert this entry, as it goes backwards. */

		/* Flush all inserted items, as they would otherwise
		 * linger in the queue forever.  And also cause
		 * warnings about things being inserted out-of-order.
		 *
		 * This of course can cause data loss since we might
		 * not have gotten the actual good items before this
		 * timescale reset yet, and they would then be lost as
		 * the appropriate pairs are already gone.
		 */

		filter_accum_flush(&process_info);

		/* But update was done as usual, so that next-next
		 * entry can be used.  (If that moves forward again,
		 * matching the next.)
		 */
	      }
	    else if (fabs(slope_diff) > 100.e-6)
	      {
		LWROC_ERROR_FMT("ht->t mapping pairs have different slopes, "
				"%.2f ppm, "
				"prev:%.6f vs. this:%.6f.",
				slope_diff * 1.e6,
				d_t_d_ht_prev, d_t_d_ht_this);
		ht_t_insert._flags = ACCUM_ITEM_HT_T_FLAGS_DISCARD;
	      }
	    else if (ht_t_insert._pair[0].ht == 0)
	      {
		/* Do not insert first incomplete item. */

	      }
	    else if (d_t_this * 4. / 1024. > 100000000.)
	      {
		LWROC_ERROR_FMT("ht->t mapping pair with "
				"too long dt = %.3f s.",
				d_t_this * 4. / 1024. * 1.e-9);
		ht_t_insert._flags = ACCUM_ITEM_HT_T_FLAGS_DISCARD;
	      }
	    else
	      {
		if (d_t_this * 4. / 1024. > 10000000.)
		  {
		    LWROC_WARNING_FMT("ht->t mapping pair lost, "
				      "dt = %.3f s.",
				      d_t_this * 4. / 1024. * 1.e-9);
		  }

		lwroc_accum_insert_item(_lwroc_accum_ht_t,
					0,
					&ht_t_insert._base,
					sizeof (ht_t_insert));
	      }

	    /* We have consumed the item from the input queue. */

	    lwroc_pipe_buffer_did_read(ht_t_pairs_queue_consumer,
				       sizeof (*sample));
	  }
      }

      /****************************************************************/

      /****************************************************************/
      /* Get next event from the source.                              */

      ret = lwroc_lmd_pipe_get_event(pipe_buf, thread_block,
				     &header, &size, &iter_data);

      if (ret == LWROC_LMD_PIPE_GET_EVENT_FAILURE)
	{
	  LWROC_BUG("Filter failed to get event.");
	}
      if (ret == LWROC_LMD_PIPE_GET_EVENT_WOULD_BLOCK)
	{
	  struct timeval timeout;
	  if (*terminate)
	    break; /* No more data, and asked to quit. */
	  /* Try again in 0.1 s. */
	  timeout.tv_sec = 0;
	  timeout.tv_usec = 100000;
	  lwroc_thread_block_get_token_timeout(thread_block, &timeout);
	  goto update_monitor;
	}

      /* End of source fetching.                                      */
      /****************************************************************/

      /****************************************************************/
      /* Sticky events shall generally just be passed along.          */

      if (ret == LWROC_LMD_PIPE_GET_EVENT_STICKY ||
	  ret == LWROC_LMD_PIPE_GET_EVENT_INTERNAL)
	{
	  LWROC_FATAL("Sticky event not handled.");
	}

      /****************************************************************/
      /* The event data can now be used:
       *
       * To just pass the event along: goto emit_event;
       *
       * To ignore (drop) it: goto event_used;
       *
       * To write a new event:
       * - call lwroc_request_event_space() to allocate destination space,
       * - fill the destination, and
       * - call lwroc_used_event_space().
       *
       * The source buffer may NOT be modified under any circumstance!
       * (It may also be used for other data destinations.)
       *
       * Flushing should be used sparingly - only for special events.
       * (The buffer readers also have time-based flushing.)
       */
      /*****************************************************************/

      /* Make sure there is an LMD subevent. */
      if (!lwroc_lmd_get_subevent(&iter_data,
				  &subev_header, &sub_data))
	{
	  LWROC_FATAL("No subevent found.");
	}

      if (iter_data._size)
	{
	  /* Several subevents could deliver data out-of-order?
	   * Need to take care if accepted.
	   */
	  LWROC_FATAL_FMT("More than one subevent.  Not expected / handled.  "
			  "(%" MYPRIzd " bytes left.)",
			  iter_data._size);
	}

      /* The subevent data. */
      lmd_subev_data = (const uint32_t *) sub_data._p;

      /*
      printf ("Event: %" MYPRIzd "  Subevent: %" MYPRIzd " "
	      "Left: %" MYPRIzd "\n",
	      size, sub_data._size, iter_data._size);
      */
      /* goto event_used; */

      if (sub_data._size < sizeof (uint32_t))
	{
	  LWROC_FATAL_FMT("Subevent smaller (%" MYPRIzd ") than expected "
			  "(but ignored) header (%" MYPRIzd ").",
			  sub_data._size, sizeof (uint32_t));
	}

      if (*lmd_subev_data != 0)
	{
	  LWROC_FATAL_FMT("Unexpected non-zero header (0x%08" PRIx32 ") "
			  "in payload.", *lmd_subev_data);
	}
      lmd_subev_data += 1;
      sub_data._size -= 1 * sizeof (uint32_t);

      if (sub_data._size % (2 * sizeof (uint32_t)) != 0)
	{
	  LWROC_FATAL_FMT("Subevent payload (%" MYPRIzd ") not a multiple "
			  "of 2 32-bit words(%" MYPRIzd ").",
			  sub_data._size, 2 * sizeof (uint32_t));
	}

      max_ref_t[0] = 0;

      for ( ; sub_data._size;
	    sub_data._size -= 2 * sizeof (uint32_t),
	    lmd_subev_data += 2)
	{
	  const uint32_t *p32 = lmd_subev_data;
	  uint32_t mod, ch, adc, t, pu, ov;
	  accum_item_dam item_dam;

	  uint64_t ref_t;
	  int64_t ref_t_diff;

	  /*
	  printf ("x %08x %08x (%" MYPRIzd ")  ",
		  lmd_subev_data[0], lmd_subev_data[1],
		  sub_data._size);
	  */
	  if ((p32[0] & 0xc0000000) != 0x40000000)
	    LWROC_FATAL_FMT("Header marker missing in first word.  "
			    "(%08x %08x)", p32[0], p32[1]);
	  if ((p32[1] & 0xc0000000) != 0xc0000000)
	    LWROC_FATAL_FMT("Footer marker missing in second word.  "
			    "(%08x %08x)", p32[0], p32[1]);

	  adc = (p32[0]      ) & 0xffff;
	  ov  = (p32[0] >> 16) &    0x1;
	  pu  = (p32[0] >> 17) &    0x1;
	  ch  = (p32[0] >> 18) &   0x3f;
	  mod = (p32[0] >> 24) &   0x3f;

	  t   = (p32[1]      ) & 0x3fffffff;
	  /*
	  printf ("mod=%3d ch=%3d ov=%d pu=%d adc=%5d t=%10d\n",
		  mod, ch, ov, pu, adc, t);
	  */

	  if (0)
	    printf ("\n%02d/%02d %d %d %5d "
		    "t:%8x\n",
		    mod, ch, ov, pu, adc,
		    t);

	  if (ch == 32)
	    {
	      /* Handle as timestamp. */
	      if (lwroc_process_heimtime(&_ht_state[mod], t, 0x3fffffff))
		{
		  /* The tracking updated its idea of the reference time.
		   *
		   * If it went backwards, we need to do some cleanup.
		   */

		  if (_ht_state[mod]._ts1 < _ht_info[mod]._ts1_cur)
		    {
		      LWROC_ERROR_FMT("Heimtime tracking went backwards, "
				      "0x%016" PRIx64 " < 0x%016" PRIx64 ".",
				      _ht_state[mod]._ts1,
				      _ht_info[mod]._ts1_cur);

		      /* See comments at ht->t mapping having similar
		       * issue.
		       */
		      filter_accum_flush(&process_info);
		    }

		  _ht_info[mod]._ts1_cur = _ht_state[mod]._ts1;
		}
	      _stats[mod]._ht_hits++;
	    }
	  else
	    _stats[mod]._hits++;

	  if ((ch & 0x20) &&
	      adc)
	    {
	      /* printf ("Data loss reported: %d\n", adc); */
	      _stats[mod]._hits_missed += adc;

	      lwroc_message_rate_ctrl(LWROC_MESSAGE_RATE_CTRL_LIMIT);
	      LWROC_ERROR_FMT("Data loss reported: %d\n", adc);
	    }

	  /*
	  if (t < raw_t_prev[mod][ch])
	    printf ("Raw time backwards: %08x < %08x\n",
		    t, raw_t_prev[mod][ch]);
	  */
	  if (!_ht_state[mod]._good)
	    {
	      if (!_ht_info[mod]._discarded_hits)
		LWROC_ERROR_FMT("Discarding hits - no Heimtime tracking "
				"for module %d.", mod);
	      _ht_info[mod]._discarded_hits++;
	      _stats[mod]._hits_bad++;
	    }
	  else
	    {
	      ref_t = lwroc_interp_heimtime(&_ht_state[mod], t, 0x3fffffff);

	      if (ref_t > max_ref_t[0])
		max_ref_t[0] = ref_t;

	      ref_t_diff = (int64_t) (ref_t - ref_t_prev[mod][ch]);

	      if (_ht_state[mod]._good && 0)
		printf ("%02d/%02d %d %d %5d "
			"t:%8x  ht:%016" PRIx64 "  %16" PRIx64 "\n",
			mod, ch, ov, pu, adc,
			t, ref_t, ref_t_diff);

	      ref_t_prev[mod][ch] = ref_t;
	      /* raw_t_prev[mod][ch] = t; */

	      item_dam._base._ts = ref_t;
	      item_dam._t        = t;
	      item_dam._adc      = adc;
	      item_dam._ov_pu    = (ov << 0) | (pu << 1);

	      /********************************************************/

	      lwroc_accum_insert_item(_lwroc_accum_dams,
				      (mod << 6) | ch,
				      &item_dam._base, sizeof (item_dam));

	      if (_ht_info[mod]._discarded_hits)
		{
		  LWROC_WARNING_FMT("%" PRIu64" hits discarded for module %d, "
				    "due to no decoded heimtime.",
				    _ht_info[mod]._discarded_hits, mod);
		  LWROC_INFO_FMT("Hit converted to heimtime for module %d.",
				 mod);
		  _ht_info[mod]._discarded_hits = 0;
		}
	    }

	  /************************************************************/
	}

    /* event_used: */
      /****************************************************************/
      /* Source data has been used, will not use it any more!         */

      lwroc_pipe_buffer_did_read(pipe_buf, size);

      /* End of source handling.                                      */
      /****************************************************************/

      /****************************************************************/
      /* Sort the data.                                               */

      {
	uint64_t ht_until;

	/* Maximum time allowed by data itself. */
	ht_until = max_ref_t[3];

	/* Or limited by how far we have gotten ht->t mapping pairs. */
	if (_filter_lmd2ebye_ht2t &&
	    ht_t_insert._base._ts < ht_until)
	  ht_until = ht_t_insert._base._ts;

	/* printf ("Sort until %016" PRIx64 "\n", ht_until); */

	/* Only sort up until the oldest stored Heimtime. */
	lwroc_accum_sort_until(_lwroc_accum_many, ht_until,
			       &process_info);
      }

      max_ref_t[3] = max_ref_t[2];
      max_ref_t[2] = max_ref_t[1];
      max_ref_t[1] = max_ref_t[0];

      /****************************************************************/

      if (_lwroc_mon_update_seq > update_seq + 5 ||
	  _lwroc_mon_update_seq < update_seq)
	{
	  int mod;

	  printf ("%d: pairs %10" PRIu64 "\n",
		  _lwroc_mon_update_seq,
		  _pairs_count);
	  update_seq = _lwroc_mon_update_seq;

	  for (mod = 0; mod < 64; mod++)
	    {
	      if (_stats[mod]._hits ||
		  _stats[mod]._ht_hits)
		{
		  printf ("%2d: %10" PRIu64 " %4" PRIu64 " (%2d) "
			  "%10" PRIu64 " t:0x%016" PRIx64 "\n",
			  mod,
			  _stats[mod]._hits, _stats[mod]._ht_hits,
			  _ht_state[mod]._matches,
			  _stats[mod]._hits_missed,
			  _ht_state[mod]._ts1);
		}

	    }
	}

    update_monitor:
      LWROC_FILTER_MON_CHECK(LWROC_FILTER_USER, 0 /* flush */);

      LWROC_MON_CHECK_COPY_BLOCK(_lwroc_mon_dams_dam_handle,
				 &(_lwroc_mon_dams_dam->_dams), 0);
    }

  LWROC_INFO("Sort (discard) all remaining hits.\n");
  /* Sort and emit whatever is remaining.
   * Anything beyond the last valid pair will be discarded.
   */
  filter_accum_flush(&process_info);
}

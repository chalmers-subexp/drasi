/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2022  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

/* *** This is *not* intended as example code. ***
 *
 * The purpose of this file is to convert some ad hoc (?) TRB
 * timestamps (from HLD subevent in HLD event packed in LMD subevent
 * inside LMD event), to the de-facto WR timestamp in LMD subevent, by
 * including the timestamp data directly after the first LMD subevent
 * header.  The remaining data is copied as-is.
 */

#include "lwroc_filter.h"
#include "lwroc_message.h"
#include "lwroc_mon_block.h"
#include "lwroc_filter_loop.h"
#include "lwroc_parse_util.h"
#include "lmd/lwroc_lmd_util.h"
#include "lmd/lwroc_lmd_white_rabbit_stamp.h"
#include "hld/lwroc_hld_util.h"
#include "../dtc_arch/acc_def/myinttypes.h"
#include "../dtc_arch/acc_def/myprizd.h"
#include "../dtc_arch/acc_def/byteswap_include.h"

#include <string.h>

void filter_trbts2lmdwr_loop(lwroc_pipe_buffer_consumer *pipe_buf,
			     const lwroc_thread_block *thread_block,
			     lwroc_data_pipe_handle *data_handle,
			     volatile int *terminate);

void filter_trbts2lmdwr_cmdline_usage(void);
int filter_trbts2lmdwr_parse_cmdline_arg(const char *arg);
void filter_trbts2lmdwr_max_ev_len(uint32_t *max_len,
				   uint32_t *add_len);

void lwroc_user_filter_pre_setup_functions(void)
{
  _lwroc_user_filter_functions->loop = filter_trbts2lmdwr_loop;
  _lwroc_user_filter_functions->name = "filter_trbts2lmdwr";
  _lwroc_user_filter_functions->cmdline_fcns.usage =
    filter_trbts2lmdwr_cmdline_usage;
  _lwroc_user_filter_functions->cmdline_fcns.parse_arg =
    filter_trbts2lmdwr_parse_cmdline_arg;
  _lwroc_user_filter_functions->max_ev_len =
    filter_trbts2lmdwr_max_ev_len;
}

/*****************************************************************************/

int _filter_trbts2lmdwr_wr_id = -1;
uint64_t _filter_trbts2lmdwr_wr_add = 0;
int _filter_trbts2lmdwr_sync_check_channel = -1;
int _filter_trbts2lmdwr_sync_check_trigger = 1;

void filter_trbts2lmdwr_cmdline_usage(void)
{
  printf ("  --wr-id=ID               Write timestamps with WR ID.\n");
  printf ("  --wr-add=n               Added to WR timestamps.\n");
  printf ("  --sync-check-ch=n        Channel with sync check value.\n");
  printf ("  --sync-check-trig=n      Trigger with sync check value (default 1).\n");
  printf ("\n");
}

int filter_trbts2lmdwr_parse_cmdline_arg(const char *request)
{
  const char *post;

  /*NOHELPCALL*/ /* We are a option subset, --help handled elsewhere. */

  if (LWROC_MATCH_C_PREFIX("--wr-id=",post)) {
    _filter_trbts2lmdwr_wr_id =
      (int) lwroc_parse_hex_u32(post, "WR ID");
  }
  else if (LWROC_MATCH_C_PREFIX("--wr-add=",post)) {
    _filter_trbts2lmdwr_wr_add =
      lwroc_parse_hex(post, "WR timestamp add");
  }
  else if (LWROC_MATCH_C_PREFIX("--sync-check-ch=",post)) {
    _filter_trbts2lmdwr_sync_check_channel =
      (int) lwroc_parse_hex_u32(post, "sync check channel");
  }
  else if (LWROC_MATCH_C_PREFIX("--sync-check-trig=",post)) {
    _filter_trbts2lmdwr_sync_check_trigger =
      (int) lwroc_parse_hex_u32(post, "trigger with sync check");
  }
  else
    return 0;

  return 1;
}

void filter_trbts2lmdwr_max_ev_len(uint32_t *max_len,
				   uint32_t *add_len)
{
  /* We inject at most: */

  (void) max_len;
  *add_len = (5 + 1) * sizeof (uint32_t);
}

/*****************************************************************************/

void filter_trbts2lmdwr_loop(lwroc_pipe_buffer_consumer *pipe_buf,
			     const lwroc_thread_block *thread_block,
			     lwroc_data_pipe_handle *data_handle,
			     volatile int *terminate)
{
  const lmd_event_10_1_host *header;
  size_t size;
  lwroc_iterate_event iter_data;
  int ret;
  char *dest;

  for ( ; ; )
    {
      size_t header_size;
      size_t inject_size;
      size_t trail_size;

      size_t write_size;

      const lmd_subevent_10_1_host *subev_header;
      lwroc_iterate_event sub_data;
      const uint32_t *lmd_subev_data = NULL;

      const hld_event *hld_ev_header;
      lwroc_iterate_event hld_ev_data;
      int hld_ev_swap;

      const hld_subevent *hld_subev_header;
      lwroc_iterate_event hld_subev_data;
      int hld_subev_swap;

      const uint32_t *p_hld_subev_data;

      uint32_t trig_typ = 1;

      uint32_t wr_error = WHITE_RABBIT_STAMP_EBID_ERROR;

      uint64_t ts = 0;
      uint32_t sync_check = 0;

      /****************************************************************/
      /* Get next event from the source.                              */

      ret = lwroc_lmd_pipe_get_event(pipe_buf, thread_block,
				     &header, &size, &iter_data);

      if (ret == LWROC_LMD_PIPE_GET_EVENT_FAILURE)
	{
	  LWROC_BUG("Filter failed to get event.");
	}
      if (ret == LWROC_LMD_PIPE_GET_EVENT_WOULD_BLOCK)
	{
	  struct timeval timeout;
	  if (*terminate)
	    break; /* No more data, and asked to quit. */
	  /* Try again in 0.1 s. */
	  timeout.tv_sec = 0;
	  timeout.tv_usec = 100000;
	  lwroc_thread_block_get_token_timeout(thread_block, &timeout);
	  goto update_monitor;
	}

      /* End of source fetching.                                      */
      /****************************************************************/

      /* By default (if checks fail), just emit the event as is. */
      header_size = size;
      inject_size = 0;
      trail_size = 0;

      /****************************************************************/
      /* Internal events shall generally just be passed along.          */

      if (ret == LWROC_LMD_PIPE_GET_EVENT_STICKY ||
	  ret == LWROC_LMD_PIPE_GET_EVENT_INTERNAL)
	goto emit_event;

      /****************************************************************/
      /* We will investigate the event. */

      /* Strategy:
       *
       * a) Check:
       *    - (The event header has already been checked.)
       *    - Input shall have an LMD subevent.
       *    - Payload of subevent shall be one entire HLD event.
       *    - HLD event shall contain HLD subevent.
       *
       *    If an events fails this, just output it.
       *
       *    If there was enough info to find a subevent, emit
       *    a WR error mark.
       *
       * b) Extract:
       *    - Get timestamp from HDL subevent.
       *
       * c) Generate:
       *    - Copy LMD event header, modify size.
       *    - Insert new WR TS LMD subevent.
       *    - Copy remaining payload.
       *      I.e. original timestamp is kept.
       */

      /*
      printf ("------------------------------------------\n");
      printf ("event size: %zd\n", size);
      printf ("header.l_dlen : %d\n", header->_header.l_dlen);
      fflush(stdout);

      printf ("remain: %zd\n",
	      iter_data._size);
      fflush(stdout);
      */

      /* Make sure there is an LMD subevent. */
      if (!lwroc_lmd_get_subevent(&iter_data,
				  &subev_header, &sub_data))
	{
	  LWROC_ERROR("No LMD subevent found.");
	  goto emit_event;
	}

      /* This is what will be written as trailing part. */
      lmd_subev_data = (const uint32_t *) sub_data._p;

      /* If we have reached this point, we can at least insert the WR
       * timestamp and sync check.  In case of failure, an WR error mark
       * will be stored (set at the beginning).
       */
      header_size = (sizeof (lmd_subevent_10_1_host) +
		     sizeof (lmd_event_10_1_host));
      inject_size = (5 + 1) * sizeof (uint32_t);
      trail_size = size - header_size;

      /*
      printf ("remain: %zd  sub: %zd\n",
	      iter_data._size, sub_data._size);
      fflush(stdout);
      */

      /* Check the HLD event header.
       * It is inside the LMD subevent, so use that as source iterator.
       */
      if (!lwroc_hld_get_event(&sub_data,
			       &hld_ev_header, &hld_ev_data,
			       &hld_ev_swap))
	{
	  LWROC_ERROR("No HLD event found (in LMD subevent).");
	  goto emit_event;
	}

      /*
      printf ("remain: %zd  sub: %zd\n",
	      sub_data._size, hld_ev_data._size);
      fflush(stdout);
      */

      /* Check the HLD subevent header.
       * Inside the HLD subevent, so use that as source iterator.
       */
      if (!lwroc_hld_get_subevent(&hld_ev_data,
				  &hld_subev_header, &hld_subev_data,
				  &hld_subev_swap))
	{
	  LWROC_ERROR("No HLD subevent found (in HLD event).");
	  goto emit_event;
	}

      /*
      printf ("remain: %zd  sub: %zd\n",
	      hld_ev_data._size, hld_subev_data._size);
      fflush(stdout);
      */

      p_hld_subev_data = (const uint32_t *) hld_subev_data._p;

      /* TODO: Check that it is an HLD event + subevent. */

      if (hld_subev_data._size < 13 * sizeof (uint32_t))
	{
	  LWROC_ERROR_FMT("HLD subevent has too small size for "
			  "expected timestamp location "
			  "(%" MYPRIzd " available < %" MYPRIzd ").",
			  hld_subev_data._size, 13 * sizeof (uint32_t));
	  goto emit_event;
	}

      /****************************************************************/
      /* Trigger type extraction. */

      trig_typ = hld_ev_header->_id & 0xf;

      /****************************************************************/
      /* Timestamp extraction. */

/*
*/

      {
	uint32_t ts_lo = *(p_hld_subev_data+11);
	uint32_t ts_hi = *(p_hld_subev_data+12);

	if (hld_subev_swap)
	{
	  ts_lo = bswap_32(ts_lo);
	  ts_hi = bswap_32(ts_hi);
	}
	/*
	   printf ("%08x:%08x\n", ts_hi, ts_lo);
	   fflush(stdout);
	 */
	ts = ((uint64_t) ts_hi << 32) | ts_lo;

	if (ts == 0)
	  LWROC_WARNING("Timestamp in HLD data is 0.");

	ts += _filter_trbts2lmdwr_wr_add;
      }

      /****************************************************************/
      /* If we got here, we have a timestamp.
       * Store timestamp, not error mark.
       */
      wr_error = 0;

      /****************************************************************/
      /* Sync check extraction. */

      if (_filter_trbts2lmdwr_sync_check_channel != -1)
      {
/*
 00000300 03e1fd00 04e19958 05e1ca2f 06e116ed f1a20042 00000110 00030001
 00002001 00124208 007a040a 0010081b 1af3ee14 00000000 74000000 11000200
 00c00000 dc084212 01c00000 00c01500 2e020420 554c055c c3961100 617f1800
 4bab0000 afb71600 72820200 ef343e30 51d81200 f83a5599 2fcaed16 00000000
 1c070700 00950020 5575b66d 9cfe0380 8c4e5480 cd464980 8c4e0b81 ce861581
 0000dc01 55550100 01000000 aaaaaaaa 74000000 11000200 01c00000 dc084212
 01a00700 00910020 5f7db66d 41081880 5e7db66d 232fdd85 3517d885 0000dc01
 02a00400 00910020 5f7db66d 41a80d80 0000dc01 03a00400 00910020 5f7db66d
 42181280 0000dc01 04a00400 00910020 5f7db66d 42181a80 0000dc01 55550100
 01000000 aaaaaaaa
*/
	int i;
	int max_i = hld_subev_data._size / sizeof (uint32_t) - 16;
	const uint32_t *p32 = p_hld_subev_data+16;
	uint32_t lead = 0x10000;
	uint32_t trail = 0x10000;
	int edges = 0;
	uint32_t const c = 2048;

	for (i = 0; (0x10000 == lead || 0x10000 == trail) && i < max_i; ++i)
	{
	  uint32_t u32 = *p32++;
	  unsigned marker3;
	  if (hld_subev_swap)
	    u32 = bswap_32(u32);
	  marker3 = u32 & 0x80000000;
	  if (marker3) {
	    unsigned ch;
	    /* swapped:  l_l: 0x0000000f, l_h: 0x00000010, h: 0x0000c000 */
	    /*           l_l: 0x0f000000, l_h: 0x10000000, h: 0x00c00000 */
	    /* swapped: edge: 0x00080000, coarse_l: ff000000, _h: 0x00070000 */
	    /*          edge: 0x00000800, coarse: 0x000007ff */
	    ch = (u32 >> 22) & 0x7f;
	    if (_filter_trbts2lmdwr_sync_check_channel == ch) {
	      unsigned edge, coarse;
	      edge   = (u32 & 0x800) >> 11;
	      coarse =  u32 & 0x7ff;
	      if (edge)
		lead = coarse;
	      else
		trail = coarse;
	      edges |= (1 << edge);
	    }
	  }
	}

	/*
	   printf ("0 %08x:%08x\n", lead, trail);
	   fflush(stdout);
	 */
	/*
	lead = (lead >> 24) | (((lead >> 16) & 7) << 8);
	trail = (trail >> 24) | (((trail >> 16) & 7) << 8);
	 */
	/*
	   printf ("1 %08x:%08x\n", lead, trail);
	   fflush(stdout);
	 */
	sync_check = (trail - lead + c) % c;
	/*
	   printf("%08x -> %u\n", sync_check, sync_check * 5);
	 */

	if (edges != 0x03 &&
	    trig_typ == _filter_trbts2lmdwr_sync_check_trigger)
	  LWROC_WARNING_FMT("Sync check value not found (edge mask: 0x%x).",
			    edges);
      }

      /* printf ("Event: %zd\n", size); */
      /* goto event_used; */

    emit_event:
      /* From here on we just emit the (original) event. */

      /****************************************************************/
      /* Get space in the destination buffer.                         */

      write_size = header_size + inject_size + trail_size;

      dest = lwroc_request_event_space(data_handle,
				       (uint32_t) write_size);

      /* Copy the header data. */
      if (header_size)
	{
	  lmd_event_10_1_host *dest_header;
	  lmd_subevent_10_1_host *dest_subev_header;

	  memcpy(dest, header, header_size);

	  /* Fixup the header length to the new size. */
	  dest_header = (lmd_event_10_1_host *) dest;

	  dest_header->_header.l_dlen += inject_size / 2;

	  dest_subev_header = (lmd_subevent_10_1_host *) (dest_header+1);

	  dest_subev_header->_header.l_dlen += inject_size / 2;

	  dest += header_size;

	  /* Corrects the trigger type in the lmd header. */
	  dest_header->_info.i_trigger = trig_typ;
	}

      /* Inject the new data. */
      if (inject_size)
	{
	  uint32_t *wr_stamp = (uint32_t *) dest;

	  uint32_t wr_id = _filter_trbts2lmdwr_wr_id;

	  wr_stamp[0] = wr_error |
	    ((wr_id << WHITE_RABBIT_STAMP_EBID_BRANCH_ID_SHIFT) &
	     WHITE_RABBIT_STAMP_EBID_BRANCH_ID_MASK);
	  wr_stamp[1] = WHITE_RABBIT_STAMP_LL16_ID | ((ts >>  0) & 0xffff);
	  wr_stamp[2] = WHITE_RABBIT_STAMP_LH16_ID | ((ts >> 16) & 0xffff);
	  wr_stamp[3] = WHITE_RABBIT_STAMP_HL16_ID | ((ts >> 32) & 0xffff);
	  wr_stamp[4] = WHITE_RABBIT_STAMP_HH16_ID | ((ts >> 48) & 0xffff);

	  wr_stamp[5] = SYNC_CHECK_MAGIC |
	    (trig_typ == _filter_trbts2lmdwr_sync_check_trigger ?
	     SYNC_CHECK_RECV : SYNC_CHECK_LOCAL) |
	    (sync_check & 0xffff);

	  dest += inject_size;
	}

      /* Copy the remaining event data. */
      if (trail_size)
	{
	  memcpy(dest, lmd_subev_data, trail_size);
	}

      /* Data has been written to the destination.
       * Will not be modified any further.
       */
      lwroc_used_event_space(data_handle,
			     (uint32_t) write_size,
			     0 /* no flush */);

      /* End of destination handling.                                 */
      /****************************************************************/

    /* event_used: */
      /****************************************************************/
      /* Source data has been used, will not use it any more!         */

      lwroc_pipe_buffer_did_read(pipe_buf, size);

      /* End of source handling.                                      */
      /****************************************************************/

    update_monitor:
      LWROC_FILTER_MON_CHECK(LWROC_FILTER_USER, 0 /* flush */);
    }
}

/* This file is part of TRLO II - a flexible FPGA trigger logic.
 *
 * Copyright (C) 2022  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __HWMAP_ACCESS_H__
#define __HWMAP_ACCESS_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "../dtc_arch/acc_def/mystdint.h"

/* This structure is only used as a typed pointer.
 *
 * With direct memory access using a macro, it is just the actual
 * hardware pointer.  (Therefore we also mark it volatile, just to
 * remember that, even though the macros cast to volatile.)
 *
 * With indirect access, it contains a pointer to a structure holding
 * access information.
 */
struct hwmap_opaque_t;
typedef struct hwmap_opaque_t hwmap_opaque;

#define HWMAP_ptr32(hw, off)				\
  ((volatile uint32_t *) (((volatile uint8_t *) (hw)) + (off)))

/* Access with macros. */
#if !HWMAP_RW_FUNC
# define HWMAP_R32_D32(hw, off)			\
  *(HWMAP_ptr32((hw), (off)))

# define HWMAP_W32_D32(hw, off, value)		\
  (*(HWMAP_ptr32((hw), (off))) = (value))
#endif

/* Access with functions. */
#if HWMAP_RW_FUNC
uint32_t HWMAP_R32_D32(volatile hwmap_opaque *opaque, uint32_t off);
void     HWMAP_W32_D32(volatile hwmap_opaque *opaque, uint32_t off,
		       uint32_t value);
#endif

#ifdef __cplusplus
}
#endif

#endif/*__HWMAP_ACCESS_H__*/

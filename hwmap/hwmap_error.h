/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __HWMAP_ERROR_H__
#define __HWMAP_ERROR_H__

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include "../dtc_arch/acc_def/typedef_func_attrib.h"

#ifdef __GNUC__
# if __GNUC__ < 3
#  define HWMAP_OLD_VARIADIC 1 /* 2.95 do not do iso99 variadic macros */
# endif
#endif

#define HWMAP_ERRLVL_FATAL  1
#define HWMAP_ERRLVL_LOG    0

typedef void (*hwmap_error_internal_func)(int fatal,const char *file,int line,
					  const char *fmt,...)
#if DO_TYPEDEF_FUNC_ATTRIB
  __attribute__ ((__format__ (__printf__, 4, 5)))
#endif
  ;

/* We are quite simplistic, if it is not fatal, it is log. */
/* This function pointer has to be defined by whoever uses us! */
extern hwmap_error_internal_func _hwmap_error_internal;

#ifdef HWMAP_OLD_VARIADIC

/* FATAL */

#define HWMAP_FATAL(x)							\
  do { _hwmap_error_internal(HWMAP_ERRLVL_FATAL,__FILE__,__LINE__,x); } \
  while (0)

#define HWMAP_FATAL_FMT(x,__VA_ARGS__...)				\
  do { _hwmap_error_internal(HWMAP_ERRLVL_FATAL,__FILE__,__LINE__,x,	\
			     __VA_ARGS__); } \
  while (0)

/* LOG */

#define HWMAP_LOG(x)							\
  do { _hwmap_error_internal(HWMAP_ERRLVL_LOG,__FILE__,__LINE__,x); }	\
  while (0)

#define HWMAP_LOG_FMT(x,__VA_ARGS__...)					\
  do { _hwmap_error_internal(HWMAP_ERRLVL_LOG,__FILE__,__LINE__,x,	\
			     __VA_ARGS__); }				\
  while (0)

#else

/* FATAL */

#define HWMAP_FATAL(x)							\
  do { _hwmap_error_internal(HWMAP_ERRLVL_FATAL,__FILE__,__LINE__,x); } \
  while (0)

#define HWMAP_FATAL_FMT(x,...)						\
  do { _hwmap_error_internal(HWMAP_ERRLVL_FATAL,__FILE__,__LINE__,x,	\
			     __VA_ARGS__); } while (0)

/* LOG */

#define HWMAP_LOG(x)							\
  do { _hwmap_error_internal(HWMAP_ERRLVL_LOG,__FILE__,__LINE__,x); }	\
  while (0)

#define HWMAP_LOG_FMT(x,...)						\
  do { _hwmap_error_internal(HWMAP_ERRLVL_LOG,__FILE__,__LINE__,x,	\
			     __VA_ARGS__); }				\
  while (0)

#endif

#endif/*__HWMAP_ERROR_INTERNAL_H__*/

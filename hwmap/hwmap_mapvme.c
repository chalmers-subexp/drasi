/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "hwmap_error.h"
#include "hwmap_mapvme.h"
#include "hwmap_access.h"

#include "../dtc_arch/acc_def/myinttypes.h"

#include <string.h>
#include <assert.h>

#if HAS_FINDCONTROLLER
#include <smem.h>
#include <ces/vmelib.h>

/* FIXME: find right include file. */

unsigned long find_controller(unsigned long,
			      unsigned long,
			      unsigned long,
			      int,int,
			      struct pdparam_master *);
void return_controller(unsigned long,unsigned long);
#endif

#if HAS_FINDCONTROLLER
struct hwmap_unmap_info
{
  unsigned long vme_virt_addr;
  unsigned long vme_len;
};
#endif

#if HAS_UNIVERSE
#include <time.h>
#include <vme/vme.h>
#include <vme/vme_api.h>

struct hwmap_unmap_info
{
  vme_bus_handle_t bus_handle;
  vme_master_handle_t window_handle_A32;
};

vme_bus_handle_t _bus_handle;
#endif

/* XPC is the bus to communicate with VME on a RIO4 running Linux. */
#if HAS_XPC_3_2_3
#include <ces/xpc.h>
#include "hwmap_vme_linux.h"
#endif

#if HAS_XPC_3_3_10
#include <ces/CesXpcBridge.h>
#include <ces/CesXpcBridge_Vme.h>
struct hwmap_unmap_info
{
  struct CesXpcBridge *bridge;
  void *ptr;
  size_t size;
};
#endif

#if HAS_ZYNQ_MMAP
#include <unistd.h>
#include <sys/mman.h>
#include <sys/fcntl.h>
struct hwmap_unmap_info
{
  int fd;
  void *ptr;
  size_t size;
};
#endif


/* Rimfaxe uses the AXI VME bridge (AVB) */
#if HAS_RIMFAXE_AVB
#include <rimfaxe/avb.h>

static struct avb *_avb = NULL;
struct hwmap_unmap_info
{
  volatile void *p;
};
#endif

/* ioxos IFC uses the Althea library */
#if HAS_ALTHEA
# include <unistd.h>
# include <sys/mman.h>
# include <sys/fcntl.h>

# pragma GCC diagnostic push
# pragma GCC diagnostic ignored "-Wstrict-prototypes"
# include <altmasioctl.h>
# include <altulib.h>
# pragma GCC diagnostic pop
int triva_fd;
const char *hwmap_triva_devpath = "/dev/trigvme";
struct hwmap_unmap_info
{
  struct alt_ioctl_map_win *alt_win;
  volatile void *p;
};
#endif

#if HAS_CAENVME
# include <CAENVMElib.h>
# define WITH_CAENVME(x) x;
#else
# define WITH_CAENVME(x)
#endif
#if HAS_MVLCC
# include <mvlcc_wrap.h>
# define WITH_MVLCC(x) x;
#else
# define WITH_MVLCC(x)
#endif
#if HAS_CMVLC
# include <cmvlc.h>
# define WITH_CMVLC(x) x;
#else
# define WITH_CMVLC(x)
#endif

#if HAS_CAENVME || HAS_MVLCC || HAS_CMVLC
struct hwmap_opaque_handle
{
  union
  {
    WITH_CAENVME(int32_t handle);
    WITH_MVLCC  (mvlcc_t  mvlcc);
    WITH_CMVLC  (struct cmvlc_client *cmvlc);
    uint8_t *ptr;    /* For dummy region in memory. */
  };
  uint32_t vme_addr; /* 0 when using dummy pointer. */
};
struct hwmap_unmap_info
{
  struct hwmap_opaque_handle *opaque;
};
#endif

#if HAS_CAENVME || HAS_MVLCC || HAS_CMVLC
/* The same controller cannot/should not be mapped multiple times from
 * the same process.  Remember handle for reuse.
 */
int _hwmap_single_handle_count = 0;
WITH_CAENVME(int32_t _hwmap_caenvme_handle = -1);
WITH_MVLCC  (mvlcc_t  _hwmap_mvlcc_handle = NULL);
WITH_CMVLC  (struct cmvlc_client *_hwmap_cmvlc_handle = NULL);
#endif

volatile hwmap_opaque *hwmap_map_vme(const char *bridge,
				     uint32_t vme_addr, uint32_t vme_len,
				     const char *purpose,
				     void **unmapinfo)
{
#if !HAS_FINDCONTROLLER && !HAS_UNIVERSE && \
  !HAS_XPC_3_2_3 && !HAS_XPC_3_3_10 && \
  !HAS_ZYNQ_MMAP && !HAS_RIMFAXE_AVB && !HAS_ALTHEA && \
  !HAS_CAENVME && !HAS_MVLCC && !HAS_CMVLC
#else
  volatile void *ptr = NULL;
# if HAS_FINDCONTROLLER
  struct pdparam_master param;
  unsigned long vme_am;
  unsigned long vme_virt_addr;
# endif
# if HAS_UNIVERSE
  vme_bus_handle_t bus_handle;
  vme_master_handle_t window_handle_A32;
# endif
# if HAS_ALTHEA
  uint64_t offset = 0;
  size_t len = 0;
  struct alt_ioctl_map_win *alt_win;
  int alt_fd;
  int rc;
# endif
# if HAS_XPC_3_2_3
  xpc_master_map_t vme_map;
  uint32_t vme_virt_addr = 0;
  uint32_t access_type = DMA_NO_BLT; /* Don't setup BLT. */
# endif
# if !HAS_XPC_3_2_3 /* Unmapping not yet available. */
  struct hwmap_unmap_info *unmap_info;
# endif
# if HAS_CAENVME
# endif
# if HAS_MVLCC
# endif
# if HAS_CMVLC
# endif
#endif

  if (_hwmap_error_internal == NULL)
    return NULL;

#if !HAS_FINDCONTROLLER && !HAS_UNIVERSE && \
  !HAS_XPC_3_2_3 && !HAS_XPC_3_3_10 && \
  !HAS_RIMFAXE_AVB && !HAS_ZYNQ_MMAP && !HAS_ALTHEA && \
  !HAS_CAENVME && !HAS_MVLCC && !HAS_CMVLC
  *unmapinfo = NULL;
  HWMAP_FATAL_FMT("Cannot map %s hardware.  "
		  "No address mapping support.",
		  purpose);
  (void) bridge;    /* unused */
  (void) vme_addr;  /* unused */
  (void) vme_len;   /* unused */
  return NULL;
#else

#if !HAS_CAENVME || !HAS_MVLCC || !HAS_CMVLC
  (void) bridge;    /* unused */
#endif

  /* Make transition to new requirement easy to debug. */
  if (unmapinfo == NULL)
    HWMAP_FATAL("unmapinfo != NULL is now mandatory in hwmap_map_vme.");

  *unmapinfo = NULL;

# if !HAS_XPC_3_2_3 /* Unmapping not yet available. */
  unmap_info = (struct hwmap_unmap_info *)
    malloc(sizeof(struct hwmap_unmap_info));
  if (!unmap_info)
    HWMAP_FATAL("Memory allocation failure (unmapinfo).");
  memset(unmap_info, 0, sizeof(struct hwmap_unmap_info));
  *unmapinfo = unmap_info;
# endif

# if HAS_FINDCONTROLLER
  memset(&param, 0, sizeof (param));
  param.iack   = 1;
  param.rdpref = 0;
  param.wrpost = 0;
  param.swap   = SINGLE_AUTO_SWAP;

  vme_am  = 0x09;

  vme_virt_addr = find_controller(vme_addr,vme_len,vme_am,0,0,&param);

  if (vme_virt_addr == (unsigned long) -1)
    {
      HWMAP_FATAL_FMT("Failed to get virtual address for "
		      "%s @ VME 0x%08" PRIx32 " (len 0x%08" PRIx32 ").",
		      purpose, vme_addr, vme_len);
    }

  unmap_info->vme_virt_addr = vme_virt_addr;
  unmap_info->vme_len = vme_len;

  ptr = (volatile void *) vme_virt_addr;
# endif

# if HAS_UNIVERSE
  if (vme_init(&bus_handle))
    HWMAP_FATAL("Error initializing the VMEbus.");

  if (vme_master_window_create(bus_handle, &window_handle_A32,
			       (uint64_t) vme_addr, VME_A32UD, vme_len,
			       VME_CTL_MAX_DW_32, NULL) != 0)
    HWMAP_FATAL_FMT("Error creating master A32 window "
		    "for %s @ VME 0x%08" PRIx32 ".", purpose, vme_addr);

  ptr = (volatile void *)
    vme_master_window_map(bus_handle, window_handle_A32, 0);

  if (ptr == NULL)
    HWMAP_FATAL_FMT("Error mapping master A32 window "
		    "for %s @ VME 0x%08" PRIx32 ".", purpose, vme_addr);

  unmap_info->bus_handle = bus_handle;
  unmap_info->window_handle_A32 = window_handle_A32;

  _bus_handle = bus_handle;
# endif

# if HAS_XPC_3_2_3
  /* Request mapping */
  if (map_vme_linux(&vme_map, &vme_virt_addr, vme_addr,
		    vme_len, access_type) != 0)
    HWMAP_FATAL_FMT("Error mapping master A32 window on XPC bus for "
		    " %s @ VME 0x%08" PRIx32 ".", purpose, vme_addr);
  ptr = (volatile void*) vme_virt_addr;
# endif

# if HAS_XPC_3_3_10
  unmap_info->bridge = CesXpcBridge_GetByName("VME Bridge");
  if (NULL == unmap_info->bridge) {
    HWMAP_FATAL_FMT("CesXpcBridge_GetByName(VME Bridge): Failed for %s.",
	purpose);
  }
  ptr = CesXpcBridge_MasterMapVirt64(unmap_info->bridge, vme_addr, vme_len,
      XPC_VME_A32_STD_USER);
  if (NULL == ptr) {
    HWMAP_FATAL_FMT("CesXpcBridge_MasterMapVirt64(0x%08" PRIx32 ","
	"0x%08" PRIx32 ") for %s failed.", vme_addr, vme_len, purpose);
  }
  unmap_info->ptr = (void *) ptr;
  unmap_info->size = vme_len;
# endif

# if HAS_ZYNQ_MMAP
#  define FILE_DEV_MEM "/dev/mem"
  unmap_info->fd = open(FILE_DEV_MEM, O_RDWR | O_SYNC);
  if (unmap_info->fd == -1) {
    HWMAP_FATAL_FMT("Failed to open '%s' for %s.", FILE_DEV_MEM, purpose);
  }
  ptr = mmap(NULL, vme_len, PROT_READ | PROT_WRITE, MAP_SHARED,
	     unmap_info->fd, (off_t) vme_addr);
  if (ptr == MAP_FAILED) {
    HWMAP_FATAL_FMT("Failed to mmap (0x%08" PRIx32 ","
		    "0x%08" PRIx32 ") for %s.",
		    vme_addr, vme_len, purpose);
  }
  unmap_info->ptr = (void *) ptr;
  unmap_info->size = vme_len;
# endif

# if HAS_RIMFAXE_AVB
  if (_avb == NULL) {
    if (avb_init(&_avb)) {
      HWMAP_FATAL("Failed to init avb");
    }
  }

  /* printf("VME len 0x%08x\n", vme_len); */
  /* vme_len = (1 << 19); */
  unmap_info->p = ptr = avb_map(_avb, vme_addr, vme_len, AM_A32_DATA, AVB_MASK_NONE);
  if (!ptr) {
    HWMAP_FATAL_FMT("avb_map(0x%08" PRIx32 ","
		    "0x%08" PRIx32 ") for %s failed. avb error = %s",
		    vme_addr, vme_len, purpose, avb_get_err_msg(_avb));
  }
# endif

# if HAS_ALTHEA
  alt_fd = alt_init();
  if (alt_fd <= 0)
    HWMAP_FATAL("alt_init() failed.");

  alt_win = (struct alt_ioctl_map_win *)malloc(
    sizeof(struct alt_ioctl_map_win));
  if (!alt_win)
    HWMAP_FATAL("Memory allocation failure (alt_win).");
  memset(alt_win, 0, sizeof(struct alt_ioctl_map_win));

  alt_win->req.rem_addr   = vme_addr;
  alt_win->req.loc_addr   = (unsigned) MAP_LOC_ADDR_AUTO;
  alt_win->req.size       = vme_len;
  alt_win->req.mode.sg_id = MAP_ID_MAS_PCIE_PMEM;
  alt_win->req.mode.space = MAP_SPACE_VME;
  alt_win->req.mode.am    = 0x9;
  alt_win->req.mode.swap  = 0;
  alt_win->req.mode.flags = MAP_FLAG_FREE;

  rc = alt_map_alloc(alt_win);
  if (rc < 0)
    HWMAP_FATAL_FMT("alt_map_alloc() failed, rc = %d", rc);

  /* transform to user space */
  offset = vme_addr - alt_win->req.rem_addr;
  offset += alt_win->req.loc_addr;
  len = alt_win->req.size;

  /* this is userspace r/w memory */
  triva_fd = open(hwmap_triva_devpath, O_RDWR);
  if (triva_fd < 0)
    HWMAP_FATAL_FMT("Failed to open '%s' for %s", hwmap_triva_devpath, purpose);

  ptr = mmap(NULL, len, PROT_READ | PROT_WRITE, MAP_SHARED, triva_fd,
	     (off_t)offset);
  if (ptr == MAP_FAILED)
    HWMAP_FATAL_FMT("Failed to mmap (0x%08" PRIx64 ","
		    "0x%08lu) for %s.",
		    offset, len, purpose);

  unmap_info->alt_win = alt_win;
  unmap_info->p = (void *) ptr;
#endif

#if HAS_CAENVME || HAS_MVLCC || HAS_CMVLC
  {
    struct hwmap_opaque_handle *opaque_handle;

#if HAS_CAENVME
    CVBoardTypes board_type = cvV2718;
    short conet_node = 0;
    short link_number = 0;
    int32_t handle;
    CVErrorCodes ret;
#endif
#if HAS_MVLCC
    mvlcc_t mvlcc;
    int ec;
#endif
#if HAS_CMVLC
    struct cmvlc_client *cmvlc;
    const char *error_ptr;
#endif

#if HAS_MVLCC || HAS_CMVLC
    if (!bridge)
      HWMAP_FATAL("VME bridge hostname not specified.");
#endif

    opaque_handle = (struct hwmap_opaque_handle *)
      malloc(sizeof(struct hwmap_opaque_handle));
    if (!opaque_handle)
      HWMAP_FATAL("Memory allocation failure (opaque_handle).");
    memset(opaque_handle, 0, sizeof(struct hwmap_opaque_handle));

    if (_hwmap_single_handle_count <= 0)
      {
#if HAS_CAENVME
#if CAENVME_VERSION_NUMBER < 30400
	ret = CAENVME_Init(board_type, link_number, conet_node, &handle);
#else
	ret = CAENVME_Init2(board_type, &link_number, conet_node, &handle);
#endif

	if (ret != cvSuccess)
	  HWMAP_FATAL_FMT("Failed to map '%s' CAEN VME bridge "
			  "(0x%08" PRIx32 ") for %s.",
			  CAENVME_DecodeError(ret),
			  vme_addr, purpose);

	_hwmap_caenvme_handle = handle;
#endif
#if HAS_MVLCC
	mvlcc = mvlcc_make_mvlc_eth(bridge);
	ec = mvlcc_connect(mvlcc);

	if (ec)
	  HWMAP_FATAL_FMT("Failed to map '%d' MVLC bridge %s "
			  "(0x%08" PRIx32 ") for %s.",
			  ec, bridge,
			  vme_addr, purpose);

	_hwmap_mvlcc_handle = mvlcc;
#endif
#if HAS_CMVLC
	cmvlc = cmvlc_connect(bridge, CMVLC_CONNECT_FLAGS_DATA /* flags */,
			      &error_ptr, NULL);

	if (!cmvlc)
	  HWMAP_FATAL_FMT("Failed to connect to (c)MVLC bridge %s "
			  "(0x%08" PRIx32 ") (error: %s) for %s.\n",
			  bridge,
			  vme_addr, error_ptr, purpose);

	_hwmap_cmvlc_handle = cmvlc;
#endif
      }
    else
      {
	/* We have already mapped a controller, use the same handle.
	 * Note: this will not work if there are multiple
	 * controllers.
	 */
	WITH_CAENVME(handle = _hwmap_caenvme_handle);
	WITH_MVLCC  (mvlcc = _hwmap_mvlcc_handle);
	WITH_CMVLC  (cmvlc = _hwmap_cmvlc_handle);
    }

    _hwmap_single_handle_count++;

    assert(vme_addr != 0); /* Else logic of addr == 0 means virtual
			    * pointer does not work.
			    */

    WITH_CAENVME(opaque_handle->handle = handle);
    WITH_MVLCC  (opaque_handle->mvlcc   = mvlcc);
    WITH_CMVLC  (opaque_handle->cmvlc   = cmvlc);
    opaque_handle->vme_addr = vme_addr;
    (void) vme_len;

    unmap_info->opaque = opaque_handle;

    ptr = (void *) opaque_handle;
  }
#endif

  HWMAP_LOG_FMT("Virtual address for "
		"%s @ VME 0x%08" PRIx32 " is 0x%08llx.",
		purpose, vme_addr,(long long) (size_t) ptr);

  return (hwmap_opaque volatile *) ptr;
#endif
}

volatile hwmap_opaque *hwmap_dummy_region(void *ptr_dummy)
{
  volatile hwmap_opaque *opaque = NULL;

#if !HWMAP_RW_FUNC
  opaque = (hwmap_opaque *) ptr_dummy;
#endif

#if HWMAP_RW_FUNC
#if HAS_CAENVME || HAS_MVLCC || HAS_CMVLC
  {
    struct hwmap_opaque_handle *opaque_handle;

    opaque_handle = (struct hwmap_opaque_handle *)
      malloc(sizeof(struct hwmap_opaque_handle));
    if (!opaque_handle)
      HWMAP_FATAL("Memory allocation failure (opaque_handle).");
    memset(opaque_handle, 0, sizeof(struct hwmap_opaque_handle));

    opaque_handle->ptr = ptr_dummy;
    opaque_handle->vme_addr = 0;

    opaque = (hwmap_opaque *) opaque_handle;
  }
#endif
#endif

  return opaque;
}

void hwmap_unmap_vme(void *unmapinfo)
{
  struct hwmap_unmap_info *unmap_info =
    (struct hwmap_unmap_info *) unmapinfo;

  if (!unmap_info)
    return;

#if HAS_FINDCONTROLLER
  return_controller(unmap_info->vme_virt_addr,unmap_info->vme_len);
#endif
#if HAS_UNIVERSE
  vme_master_window_unmap(unmap_info->bus_handle,
			  unmap_info->window_handle_A32);
  vme_master_window_release(unmap_info->bus_handle,
			    unmap_info->window_handle_A32);
  vme_term(unmap_info->bus_handle);
#endif
#if HAS_XPC_3_3_10
  CesXpcBridge_MasterUnMapVirt(unmap_info->bridge, unmap_info->ptr,
      unmap_info->size);
  CesXpcBridge_Put(unmap_info->bridge);
#endif
#if HAS_ZYNQ_MMAP
  munmap(unmap_info->ptr, unmap_info->size);
  close(unmap_info->fd);
#endif
#if HAS_RIMFAXE_AVB
  avb_unmap(_avb, unmap_info->p);
#endif
#if HAS_ALTHEA
  munmap((void *)unmap_info->p, unmap_info->alt_win->req.size);
  alt_map_free(unmap_info->alt_win);
#endif
#if HAS_CAENVME || HAS_MVLCC || HAS_CMVLC
  if (unmap_info->opaque->vme_addr)
    {
      _hwmap_single_handle_count--;
#if HAS_CAENVME
      assert(unmap_info->opaque->handle == _hwmap_caenvme_handle);
      if (_hwmap_single_handle_count == 0)
	{
	  CAENVME_End(_hwmap_caenvme_handle);
	  _hwmap_caenvme_handle = -1;
	}
#endif
#if HAS_MVLCC
      assert(unmap_info->opaque->mvlcc == _hwmap_mvlcc_handle);
      if (_hwmap_single_handle_count == 0)
	{
	  mvlcc_disconnect(_hwmap_mvlcc_handle);
	  mvlcc_free_mvlc(_hwmap_mvlcc_handle);
	  _hwmap_mvlcc_handle = NULL;
	}
#endif
#if HAS_CMVLC
      assert(unmap_info->opaque->cmvlc == _hwmap_cmvlc_handle);
      if (_hwmap_single_handle_count == 0)
	{
	  cmvlc_close(_hwmap_cmvlc_handle);
	  _hwmap_cmvlc_handle = NULL;
	}
#endif
    }
  free(unmap_info->opaque);
#endif
  free(unmap_info);
}

#if HWMAP_RW_FUNC
#if HAS_CAENVME || HAS_MVLCC || HAS_CMVLC
uint32_t HWMAP_R32_D32(volatile hwmap_opaque *opaque, uint32_t off)
{
  struct hwmap_opaque_handle volatile *opaque_handle =
    (struct hwmap_opaque_handle volatile *) opaque;
  uint32_t data;
  WITH_CAENVME(CVErrorCodes ret)
  WITH_MVLCC  (int ec)
  WITH_CMVLC  (int ret)

  /* fprintf (stderr, "READ: %d\n", off); */

  if (opaque_handle->vme_addr == 0)
    {
      data = *(HWMAP_ptr32(opaque_handle->ptr, (off)));
      /*
      fprintf (stderr, "READ: %p + 0x%x = %p => %08x\n",
	       opaque_handle->ptr, off,
	       HWMAP_ptr32(opaque_handle->ptr, (off)),
	       data);
      */
      return data;
    }

  /* fprintf (stderr, "READ: %d\n", off); */

#if HAS_CAENVME
  ret = CAENVME_ReadCycle(opaque_handle->handle,
			  opaque_handle->vme_addr + off,
			  &data,
			  cvA32_U_DATA,
			  cvD32);

  if (ret != cvSuccess)
    HWMAP_FATAL_FMT("Error '%s' reading CAEN VME bridge "
		    "(0x%08" PRIx32 " + 0x%08" PRIx32 ").",
		    CAENVME_DecodeError(ret),
		    opaque_handle->vme_addr, off);
#endif
#if HAS_MVLCC
  ec = mvlcc_single_vme_read(opaque_handle->mvlcc,
			     opaque_handle->vme_addr + off,
			     &data,
			     32 /* addrmode */,
			     32 /* data width */);

  if (ec)
    HWMAP_FATAL_FMT("Error '%d' reading MVLC bridge "
		    "(0x%08" PRIx32 " + 0x%08" PRIx32 ").",
		    ec,
		    opaque_handle->vme_addr, off);
#endif
#if HAS_CMVLC
  ret = cmvlc_single_vme_read(opaque_handle->cmvlc,
			      opaque_handle->vme_addr + off,
			      &data,
			      vme_user_A32,
			      vme_D32);

  if (ret)
    HWMAP_FATAL_FMT("Error '%d' reading MVLC bridge "
		    "(0x%08" PRIx32 " + 0x%08" PRIx32 ").",
		    ret,
		    opaque_handle->vme_addr, off);
#endif
  return data;
}

void HWMAP_W32_D32(volatile hwmap_opaque *opaque, uint32_t off,
		   uint32_t value)
{
  struct hwmap_opaque_handle volatile *opaque_handle =
    (struct hwmap_opaque_handle volatile *) opaque;
  WITH_CAENVME(CVErrorCodes ret)
  WITH_MVLCC  (int ec)
  WITH_CMVLC  (int ret)

  if (opaque_handle->vme_addr == 0)
    {
      *(HWMAP_ptr32(opaque_handle->ptr, (off))) = (value);
      /*
      fprintf (stderr, "WRITE: %p + 0x%x = %p => %08x\n",
	       opaque_handle->ptr, off,
	       HWMAP_ptr32(opaque_handle->ptr, (off)),
	       value);
      */
      return;
    }

#if HAS_CAENVME
  ret = CAENVME_WriteCycle(opaque_handle->handle,
			   opaque_handle->vme_addr + off,
			   &value,
			   cvA32_U_DATA,
			   cvD32);

  if (ret != cvSuccess)
    HWMAP_FATAL_FMT("Error '%s' writing CAEN VME bridge "
		    "(0x%08" PRIx32 " + 0x%08" PRIx32 ").",
		    CAENVME_DecodeError(ret),
		    opaque_handle->vme_addr, off);
#endif
#if HAS_MVLCC
  ec = mvlcc_single_vme_write(opaque_handle->mvlcc,
			     opaque_handle->vme_addr + off,
			     value,
			     32 /* addrmode */,
			     32 /* data width */);

  if (ec)
    HWMAP_FATAL_FMT("Error '%d' writing MVLC bridge "
		    "(0x%08" PRIx32 " + 0x%08" PRIx32 ").",
		    ec,
		    opaque_handle->vme_addr, off);
#endif
#if HAS_CMVLC
  ret = cmvlc_single_vme_write(opaque_handle->cmvlc,
			       opaque_handle->vme_addr + off,
			       value,
			       vme_user_A32,
			       vme_D32);

  if (ret)
    HWMAP_FATAL_FMT("Error '%d' writing MVLC bridge "
		    "(0x%08" PRIx32 " + 0x%08" PRIx32 ").",
		    ret,
		    opaque_handle->vme_addr, off);
#endif
}
#endif
#endif

#if HAS_CMVLC
struct cmvlc_client *hwmap_get_cmvlc(volatile hwmap_opaque *opaque)
{
  struct hwmap_opaque_handle volatile *opaque_handle =
    (struct hwmap_opaque_handle volatile *) opaque;

  return opaque_handle->cmvlc;
}
#endif

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __HWMAP_MAPVME_H__
#define __HWMAP_MAPVME_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "hwmap_access.h"

#include "../dtc_arch/acc_def/mystdint.h"

volatile hwmap_opaque *hwmap_map_vme(const char *bridge,
				     uint32_t vmeaddr, uint32_t vmelen,
				     const char *purpose,
				     void **unmapinfo);

volatile hwmap_opaque *hwmap_dummy_region(void *ptr_dummy);

void hwmap_unmap_vme(void *unmapinfo);

#if HAS_CMVLC
struct cmvlc_client *hwmap_get_cmvlc(volatile hwmap_opaque *opaque);
#endif

#ifdef __cplusplus
}
#endif

#endif/*__HWMAP_MAPVME_H__*/

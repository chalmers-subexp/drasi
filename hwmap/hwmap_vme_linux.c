/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Bastian Loeher  <b.loeher@gsi.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

/* TODO: clear up printf occurrences in this file. */

/* Map the access to memory on Linux using XPC bus
 * vme_base_addr : Address on the VME bus to map
 * vme_size : Size of memory to map (module size)
 * access_type : Access type
 *   0 : NO DMA (single cycle D8, D16, D32)
 *   1 : BLT
 *   2 : MBLT
 *   3 : 2eSST
 */

#include "hwmap_vme_linux.h"

#ifdef HAS_XPC_3_2_3

int map_vme_linux(xpc_master_map_t* return_map,
		  uint32_t* return_addr, uint32_t vme_base_addr,
		  uint32_t vme_size, uint32_t access_type)
 {
  int ret = 0;
  uint32_t flags = 0;
  uint32_t options = 0;
  /* uint32_t shared_mem = 0; */
  int fd_xpc = 0, fd_dma = 0;
  uint32_t channel = 0;
  xpc_master_map_t map;
  void *mapped_address = NULL;

  *return_addr = 0;

  /* Set options and flags *
   * Note : Burst size 32, 64, 128, 256, 512, 1024 available
   * This setting might have a large impact on performance
   * SST frequencies : 160, 267, 320
   * FIFO option : XPC_DMA_VME_ADDR_CONST
   * Double buffer : XPC_DMA_DOUBLE_BUFFER
  */

  switch (access_type) {
  case DMA_NO_BLT:
    flags = XPC_VME_A32_STD_USER;
    break;
  case DMA_BLT:
    flags = XPC_VME_A32_BLT_USER;
    options = XPC_DMA_READ | XPC_DMA_USER_BUFFER;
    break;
  case DMA_MBLT:
    flags = XPC_VME_A32_MBLT_USER;
    options = XPC_DMA_READ | XPC_DMA_USER_BUFFER | XPC_DMA_VME_BURST_512;
    break;
  case DMA_2ESST:
    flags = XPC_VME_ATYPE_A32 | XPC_VME_DTYPE_2eSST |
	    XPC_VME_PTYPE_USER | XPC_VME_FREQ_SST320;
    options = XPC_DMA_READ | XPC_DMA_USER_BUFFER | XPC_DMA_VME_BURST_512;
    break;
  default:
    return UNKNOWN_ACCESS_TYPE;
  }

  /* Open XPC bus driver */
  fd_xpc = open ("/dev/xpc_vme", O_RDWR);
  if (fd_xpc < 0) {
    printf("%s: Could not open /dev/xpc_vme\n",__FUNCTION__);
    return XPC_OPEN_FAILED;
  }

  /* Fill MAP struct */
  map.bus_address = vme_base_addr & 0xffffffff;
  map.size = vme_size;
  map.access_type = flags;

  /* Map VME memory to XPC bus as master */
  ret = ioctl (fd_xpc, XPC_MASTER_MAP, &map);
  if (ret < 0) {
    printf("%s: ioctl XPC_MASTER_MAP failed\n",__FUNCTION__);
    perror("XPC_MASTER_MAP");
    return XPC_MAPPING_FAILED;
  }

  /* Map XPC bus memory to CPU space */
  mapped_address =
    (void *) mmap(NULL, (unsigned int) map.size,
		  PROT_READ | PROT_WRITE,
		  MAP_SHARED, fd_xpc, (int) map.xpc_address);
  if (mapped_address == (void*) -1) {
    printf("%s: Memory mapping of XPC space failed!\n",__FUNCTION__);
    perror("mmap()");
    return MMAP_FAILED;
  }
  close(fd_xpc);

  if (access_type != DMA_NO_BLT) {
    /* Open DMA bus driver */
    fd_dma = open ("/dev/xpc_dma", O_RDWR);
    if (fd_dma < 0) {
      printf("%s: Could not open /dev/xpc_dma\n",__FUNCTION__);
      return DMA_OPEN_FAILED;
    }

    /* Configure DMA */
    ret = ioctl (fd_dma, XPC_DMA_CHAN_MODIFY, &channel);
    if (ret < 0) {
      printf("%s: ioctl XPC_DMA_CHAN_MODIFY failed\n",__FUNCTION__);
      perror("XPC_DMA_CHAN_MODIFY");
      return DMA_SETUP_FAILED;
    }
    printf("%s: using DMA channel %d\n",__FUNCTION__,(int) channel);
    close(fd_dma);
  }

  /* Return values */
  *return_addr = (uint32_t) mapped_address;
  *return_map = map;

  return ret;
}

#endif

/* Request shared memory block */
/* This is only to show how this is done */
/* How to get some DRAM space to play with */
/* shared_mem = (uint32_t) smem_get ("DRAM", vme_size, SM_READ | SM_WRITE); */

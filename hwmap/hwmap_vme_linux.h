/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Bastian Loeher  <b.loeher@gsi.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __MAP_VME_LINUX_H
#define __MAP_VME_LINUX_H

#ifdef HAS_XPC_3_2_3

/* #include <stdlib.h> */
#include <stdio.h>
#include <stdint.h>
#include <fcntl.h>
#include <unistd.h>
#include <ces/xpc.h>
#include <ces/xpc_vme.h>
#include <ces/xpc_dma.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

#define DMA_NO_BLT 0
#define DMA_BLT 1
#define DMA_MBLT 2
#define DMA_2ESST 3
#define DMA_OPEN_FAILED 1
#define DMA_SETUP_FAILED 2
#define XPC_OPEN_FAILED 4
#define XPC_MAPPING_FAILED 8
#define UNKNOWN_ACCESS_TYPE 16
#define MMAP_FAILED 32
int map_vme_linux(xpc_master_map_t* return_map,
		  uint32_t* return_addr, uint32_t vme_base_addr,
		  uint32_t vme_size, uint32_t access_type);

#endif

#endif

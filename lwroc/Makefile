# This file is part of DRASI - a data acquisition data pump.
#
# Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301  USA

QUIET ?= @

#######################################################################

DTC_ARCH_DIR=../dtc_arch
DTC_ARCH_TEMPLATE_DIR=../build_rules

CFLAGS += -I . -I $(DTC_ARCH_DIR) -I ../hwmap
LIBS   +=

include $(DTC_ARCH_DIR)/build_rules_inc.mk

CFLAGS += -g -O3

GENDIR = gen
GENDIR_ARCH = ${GENDIR}_${ARCH_SUFFIX}

CFLAGS += -I ${GENDIR_ARCH}

CFLAGS += $(WARN_FLAGS)

OBJS_MAIN = lwroc_main.o lwroc_filter.o

OBJS_READOUT = lwroc_readout.o lwroc_triva_readout.o \
	lwroc_triva_control.o lwroc_triva_access.o

OBJS_NOFILTER = lwroc_filter_none.o

OBJS_MERGE = lwroc_merge.o lwroc_merge_in.o lwroc_merge_in_unit.o \
	lwroc_merge_sort.o lwroc_merge_analyse.o

OBJS_NETUTIL = lwroc_net_io_util.o lwroc_hostname_util.o \
	lwroc_net_badrequest.o

OBJS_PARSEUTIL = lwroc_parse_util.o

OBJS_MBSCOMPAT = lwroc_mbscompat.o

OBJS_HWMAP_ERROR = lwroc_hwmap_error.o

OBJS = lwroc_pipe_buffer.o \
	lwroc_thread_block.o \
	lwroc_thread_util.o \
	lwroc_item_buffer.o \
	lwroc_message_internal.o \
	lwroc_message_wait.o \
	lwroc_two_copy.o \
	lwroc_mon_block.o \
	lwroc_control.o \
	lwroc_serializers.o lwroc_serializer_util.o lwroc_format_prefix.o \
	lwroc_select_util.o \
	lwroc_data_pipe.o \
	lwroc_data_pipe_alloc.o \
	gdf/lwroc_gdf_util.o \
	lmd/lwroc_lmd_util.o lmd/lwroc_lmd_ev_sev.o \
	lmd/lwroc_lmd_sticky_store.o \
	ebye/lwroc_ebye_util.o \
	xfer/lwroc_xfer_util.o \
	hld/lwroc_hld_util.o \
	lwroc_filter_loop.o \
	lwroc_filter_ts.o \
	lwroc_file_writer.o \
	lwroc_crc32.o \
	lwroc_file_functions.o \
	lwroc_fd_writer.o \
	lwroc_tsm_writer.o \
	lwroc_timeout_util.o \
	lwroc_slow_async_loop.o \
	lwroc_net_io.o \
	lwroc_net_client_base.o lwroc_net_portmap.o lwroc_udp_awaken_hints.o \
	lwroc_net_incoming.o lwroc_net_message.o \
	lwroc_net_mon.o lwroc_net_control.o lwroc_net_in_rev_link.o \
	lwroc_cpu_affinity.o \
	lwroc_delayed_eb.o \
	lwroc_net_trans.o \
	lwroc_net_trans_loop.o \
	lwroc_net_outgoing.o lwroc_net_reverse_link.o \
	lwroc_net_trigbus.o \
	lwroc_triva.o lwroc_triva_sim.o \
	lwroc_map_pexor.o \
	lwroc_triva_state.o \
	lwroc_net_conn_monitor.o \
	lwroc_net_ntp.o \
	lwroc_leap_seconds.o \
	lwroc_calc_mean_sigma.o \
	lwroc_accum_sort.o \
# Intentionally empty

OBJS_TEST_TRACK_TIMESTAMP = test_track_timestamp.o

OBJS_TEST_PRINT_PREFIX = test_format_prefix.o

OBJS_TEST_CRC32 = test_crc32.o

BUILD_DIR = bld_$(ARCH_SUFFIX)
LIB_DIR = lib_$(ARCH_SUFFIX)
BIN_DIR = bin_$(ARCH_SUFFIX)

OBJS_MAIN_ARCH = $(OBJS_MAIN:%.o=$(BUILD_DIR)/%.o)

OBJS_READOUT_ARCH = $(OBJS_READOUT:%.o=$(BUILD_DIR)/%.o)

OBJS_NOFILTER_ARCH = $(OBJS_NOFILTER:%.o=$(BUILD_DIR)/%.o)

OBJS_MERGE_ARCH = $(OBJS_MERGE:%.o=$(BUILD_DIR)/%.o)

OBJS_NETUTIL_ARCH = $(OBJS_NETUTIL:%.o=$(BUILD_DIR)/%.o)

OBJS_PARSEUTIL_ARCH = $(OBJS_PARSEUTIL:%.o=$(BUILD_DIR)/%.o)

OBJS_MBSCOMPAT_ARCH = $(OBJS_MBSCOMPAT:%.o=$(BUILD_DIR)/%.o)

OBJS_HWMAP_ERROR_ARCH = $(OBJS_HWMAP_ERROR:%.o=$(BUILD_DIR)/%.o)

OBJS_ARCH = $(OBJS:%.o=$(BUILD_DIR)/%.o)

OBJS_TEST_TRACK_TIMESTAMP_ARCH = \
	$(OBJS_TEST_TRACK_TIMESTAMP:%.o=$(BUILD_DIR)/%.o)

OBJS_TEST_PRINT_PREFIX_ARCH = \
	$(OBJS_TEST_PRINT_PREFIX:%.o=$(BUILD_DIR)/%.o)

OBJS_TEST_CRC32_ARCH = \
	$(OBJS_TEST_CRC32:%.o=$(BUILD_DIR)/%.o)

ALL_LIBS = $(LIB_DIR)/liblwroc.a \
	$(LIB_DIR)/liblwroc_main.a \
	$(LIB_DIR)/liblwroc_readout.a \
	$(LIB_DIR)/liblwroc_nofilter.a \
	$(LIB_DIR)/liblwroc_netutil.a \
	$(LIB_DIR)/liblwroc_parseutil.a \
	$(LIB_DIR)/liblwroc_mbscompat.a \
	$(LIB_DIR)/liblwroc_hwmap_error.a \
	$(LIB_DIR)/liblwroc_merge.a

all: $(ALL_LIBS) $(LIB_DIR)/lwroc_libs.stamp \
	$(BIN_DIR)/test_track_timestamp $(BIN_DIR)/test_format_prefix \
	$(BIN_DIR)/test_crc32

#######################################################################

AUTO_DEPS = $(OBJS_ARCH:%.o=%.d) \
	$(OBJS_MAIN_ARCH:%.o=%.d) \
	$(OBJS_READOUT_ARCH:%.o=%.d) \
	$(OBJS_NOFILTER_ARCH:%.o=%.d) \
	$(OBJS_MERGE_ARCH:%.o=%.d) \
	$(OBJS_NETUTIL_ARCH:%.o=%.d) \
	$(OBJS_PARSEUTIL_ARCH:%.o=%.d) \
	$(OBJS_MBSCOMPAT_ARCH:%.o=%.d) \
	$(OBJS_HWMAP_ERROR_ARCH:%.o=%.d) \
	$(OBJS_TEST_TRACK_TIMESTAMP_ARCH:%.o=%.d) \
	$(OBJS_TEST_PRINT_PREFIX_ARCH:%.o=%.d) \
	$(OBJS_TEST_CRC32_ARCH:%.o=%.d)

FIXUP_DEPS = sed -e 's,\($(*F)\)\.o[ :]*,$(BUILD_DIR)/$*.o $@ : ,g' \
	-e 's,[^/]acc_auto_def/, gen_$(ARCH_SUFFIX)/acc_auto_def/,g'

ifneq (clean,$(MAKECMDGOALS))
ifneq (clean-all,$(MAKECMDGOALS))
-include $(AUTO_DEPS) # dependency files (.d)
endif
endif

#######################################################################

# Force create the git-describe file by making it from a shell script
GIT_DESCRIBE_FORCE=$(shell ./force_git_describe.sh $(GENDIR_ARCH) $(GENDIR))
ifneq (,$(findstring FAIL,$(GIT_DESCRIBE_FORCE)))
$(error Git describe rule failed - cannot continue!)
endif

#######################################################################

include $(DTC_ARCH_DIR)/acc_auto_def.mk

#######################################################################

MAKE_PROTO_SERIALIZATION = ./lwroc_make_proto_serialization.pl

PROTO_SOURCES = lwroc_net_proto.h

${GENDIR}/%_serializer.h: $(MAKE_PROTO_SERIALIZATION) $(PROTO_SOURCES)
	@echo "  MKSER  $@"
	@$(MKDIR_P) $(dir $@)
	@$(PERL) $(MAKE_PROTO_SERIALIZATION) \
	  --header --struct=$* "--md5sum=$(MD5SUM)" --md5tmpfile=$@.md5tmp < \
	  $(PROTO_SOURCES) > $@.tmp
	@mv $@.tmp $@

${GENDIR}/%_serializer.c: ${GENDIR}/%_serializer.h $(MAKE_PROTO_SERIALIZATION) \
  $(PROTO_SOURCES)
	@echo "  MKSER  $@"
	@$(MKDIR_P) $(dir $@)
	@$(PERL) $(MAKE_PROTO_SERIALIZATION) \
	  --struct=$* "--md5sum=$(MD5SUM)" --md5tmpfile=$@.md5tmp < \
	  $(PROTO_SOURCES) > $@.tmp
	@mv $@.tmp $@

#######################################################################

MAKE_MESSAGE_MACROS = ./lwroc_make_message_macros.pl

${GENDIR}/lwroc_message_macros.h: $(MAKE_MESSAGE_MACROS)
	@echo "MKMACROS $@"
	@$(MKDIR_P) $(dir $@)
	@$(PERL) $(MAKE_MESSAGE_MACROS) > $@.tmp
	@mv $@.tmp $@

#######################################################################

$(BUILD_DIR)/%.o: %.c
	@echo "   CC    $@"
	@$(MKDIR_P) $(dir $@)
	$(QUIET)$(CC) $(CFLAGS) $(OPTFLAGS) -o $@ -c $<

$(BUILD_DIR)/%.d: %.c
	@echo "  DEPS   $@"
	@$(MKDIR_P) $(dir $@)
	$(QUIET)$(CC) $(CFLAGS) -MM -MG $< | $(FIXUP_DEPS) > $@

#######################################################################

$(LIB_DIR)/liblwroc.a: $(OBJS_ARCH)
	@echo "   AR    $@"
	@$(MKDIR_P) $(dir $@)
	$(QUIET)${AR} rcs $@ $(OBJS_ARCH)

$(LIB_DIR)/liblwroc_main.a: $(OBJS_MAIN_ARCH)
	@echo "   AR    $@"
	@$(MKDIR_P) $(dir $@)
	$(QUIET)${AR} rcs $@ $(OBJS_MAIN_ARCH)

$(LIB_DIR)/liblwroc_readout.a: $(OBJS_READOUT_ARCH)
	@echo "   AR    $@"
	@$(MKDIR_P) $(dir $@)
	$(QUIET)${AR} rcs $@ $(OBJS_READOUT_ARCH)

$(LIB_DIR)/liblwroc_nofilter.a: $(OBJS_NOFILTER_ARCH)
	@echo "   AR    $@"
	@$(MKDIR_P) $(dir $@)
	$(QUIET)${AR} rcs $@ $(OBJS_NOFILTER_ARCH)

$(LIB_DIR)/liblwroc_merge.a: $(OBJS_MERGE_ARCH)
	@echo "   AR    $@"
	@$(MKDIR_P) $(dir $@)
	$(QUIET)${AR} rcs $@ $(OBJS_MERGE_ARCH)

$(LIB_DIR)/liblwroc_netutil.a: $(OBJS_NETUTIL_ARCH)
	@echo "   AR    $@"
	@$(MKDIR_P) $(dir $@)
	$(QUIET)${AR} rcs $@ $(OBJS_NETUTIL_ARCH)

$(LIB_DIR)/liblwroc_parseutil.a: $(OBJS_PARSEUTIL_ARCH)
	@echo "   AR    $@"
	@$(MKDIR_P) $(dir $@)
	$(QUIET)${AR} rcs $@ $(OBJS_PARSEUTIL_ARCH)

$(LIB_DIR)/liblwroc_mbscompat.a: $(OBJS_MBSCOMPAT_ARCH)
	@echo "   AR    $@"
	@$(MKDIR_P) $(dir $@)
	$(QUIET)${AR} rcs $@ $(OBJS_MBSCOMPAT_ARCH)

$(LIB_DIR)/liblwroc_hwmap_error.a: $(OBJS_HWMAP_ERROR_ARCH)
	@echo "   AR    $@"
	@$(MKDIR_P) $(dir $@)
	$(QUIET)${AR} rcs $@ $(OBJS_HWMAP_ERROR_ARCH)

#######################################################################

DTC_CFLAGS_HWMAP_RW_FUNC = $(filter -DHWMAP_RW_FUNC, $(DTC_CFLAGS))

$(LIB_DIR)/lwroc_libs.config: Makefile
	@echo " CONFIG  $@"
	@$(MKDIR_P) $(dir $@)
	$(QUIET)rm -f $@
	$(QUIET)echo "export CFLAGS=\"$(NOCFLAGS) $(DTC_CFLAGS_HWMAP_RW_FUNC)\"" >> $@ # pthreads??
	$(QUIET)echo "export CFLAGS_INTERNAL=\"$(DTC_CFLAGS)\"" >> $@
	$(QUIET)echo "export CFLAGS_CMVLC=\"$(CMVLC_CFLAGS)\"" >> $@
	$(QUIET)echo "export LDFLAGS=\"$(LDFLAGS)\"" >> $@
	$(QUIET)echo "export LIBS=\"$(LIBS)\"" >> $@
	$(QUIET)echo "export WARN_FLAGS=\"$(WARN_FLAGS)\"" >> $@
	$(QUIET)echo "export MKDIR_P=\"$(MKDIR_P)\"" >> $@

MBSCOMPAT_HEADERS=$(shell ls ../mbscompat/*.h)

$(LIB_DIR)/lwroc_libs.stamp: $(ALL_LIBS) $(LIB_DIR)/lwroc_libs.config \
  $(MBSCOMPAT_HEADERS) $(BIN_DIR)/test_crc32.test
	@echo "  TOUCH  $@"
	@$(MKDIR_P) $(dir $@)
	$(QUIET)touch $@

#######################################################################

$(BIN_DIR)/test_track_timestamp: $(BUILD_DIR)/test_track_timestamp.o
	@echo "  LINK   $@"
	@$(MKDIR_P) $(dir $@)
	$(QUIET)$(CC) -o $@ $< $(LDFLAGS) $(LIBS)

#######################################################################

$(BIN_DIR)/test_format_prefix: $(BUILD_DIR)/test_format_prefix.o
	@echo "  LINK   $@"
	@$(MKDIR_P) $(dir $@)
	$(QUIET)$(CC) -o $@ $< $(LDFLAGS) $(LIBS)

#######################################################################

$(BIN_DIR)/test_crc32: $(BUILD_DIR)/test_crc32.o
	@echo "  LINK   $@"
	@$(MKDIR_P) $(dir $@)
	$(QUIET)$(CC) -o $@ $< $(LDFLAGS) $(LIBS)

$(BIN_DIR)/test_crc32.test: $(BIN_DIR)/test_crc32
	@echo "  CHECK  $@"
	$(QUIET) if [ -n "$(HOSTCC)" -a "$(HOSTCC)" != "$(CC)" ] \
	  ; then \
	    echo ; \
	    echo "HOSTCC set (cross compiling), not running '$<'" ; \
	    echo \
	  ; else \
	    $< \
	  ; fi
	$(QUIET)touch $@

#######################################################################

showconfig_all:
	$(QUIET) echo "  ------------------------  -----------------------"
	$(QUIET) ls $(GENDIR_ARCH)/acc_auto_def/*.mk | \
	  xargs grep ACC_DEF_SHOW_CONFIG | \
	  sed -e "s/.*ACC_DEF_SHOW_CONFIG: /  /" -e "s/#.*//"
	$(QUIET) echo "  ------------------------  -----------------------"
	$(QUIET) ls $(GENDIR_ARCH)/acc_auto_def/*.h | \
	  xargs grep ACC_DEF_SHOW_CONFIG | \
	  sed -e "s/.*ACC_DEF_SHOW_CONFIG: /  /" -e "s/#.*//"
	$(QUIET) echo "  ------------------------  -----------------------"

showconfig:
	$(QUIET) echo "  ------------------------  -----------------------"
	$(QUIET) ls $(GENDIR_ARCH)/acc_auto_def/*.mk \
		    $(GENDIR_ARCH)/acc_auto_def/*.h | \
	  xargs grep ACC_DEF_SHOW_CONFIG | grep Important | \
	  sed -e "s/.*ACC_DEF_SHOW_CONFIG: /  /" -e "s/#.*//"
	$(QUIET) echo "  ------------------------  -----------------------"

#######################################################################

clean:
	rm -rf $(BUILD_DIR)
	rm -rf $(LIB_DIR)
	rm -rf $(BIN_DIR)
	rm -rf ${GENDIR_ARCH}
	rm -f *_$(ARCH_SUFFIX).o *_$(ARCH_SUFFIX).d
	rm -f lmd/*_$(ARCH_SUFFIX).o lmd/*_$(ARCH_SUFFIX).d
	rm -f ebye/*_$(ARCH_SUFFIX).o ebye/*_$(ARCH_SUFFIX).d
	rm -f lwroc_*$(ARCH_SUFFIX).a

clean-all:
	rm -rf build_* # old, to be removed
	rm -rf bld_*
	rm -rf lib_*
	rm -rf bin_*
	rm -rf gen*
	rm -f *.o *.d
	rm -f lmd/*.o lmd/*.d
	rm -f ebye/*.o ebye/*.d
	rm -f lwroc_*.a

#######################################################################

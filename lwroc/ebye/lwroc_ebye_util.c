/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Bastian Loeher  <b.loeher@gsi.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "ebye/lwroc_ebye_util.h"
#include "gdf/lwroc_gdf_util.h"
#include "lwroc_message.h"
#include "lwroc_net_trans.h"
#include "lwroc_parse_util.h"

#include <assert.h>
#include <string.h>

#include "../dtc_arch/acc_def/myinttypes.h"
#include "../dtc_arch/acc_def/myprizd.h"

/* Collecting data for an ebye output buffer from the circular pipe.
 *
 * In case the collect function has only found data to partially fill
 * the buffer, it may be called again after some time to see if
 * further data is available.
 *
 * In case we have a leftover of a fragmented event, we will not have
 * released it at all, but should not restart the search at its
 * beginning.
 *
 * If the search begins with waste space, that is released
 * immediately.  Waste space inside the data is ignored, and becomes
 * released upon releasing the data before it.
 *
 * How many chunks of data can we at most return?  Waste space can
 * occur at the end, and the beginning (for static data).  Thus we may
 * have one skip region.  Leading to two chunks.
 *
 * If any internal control commands are to go via the data flow
 * (e.g. file open requests [not implemented like that yet]), then
 * they have to be formatted as some special kind of proper event that
 * end up in the actual output.  Thus need no skipping.
 *
 * One may also think of going to a situation with a double pipe
 * buffer, where the first just has control records, and the second
 * pure data.  The control records need not be per event, they could
 * span many event (to reduce the amount needed).  Many-event spanning
 * however introduces latency, as they cannot be written immediately.
 * Unless we use some flag tricks...
 */

#define LBC (&(lbc->_gdf))

lwroc_gdf_buffer_chunks *lwroc_ebye_buffer_chunks_alloc(int flags)
{
  lwroc_ebye_buffer_chunks *lbc =
    (lwroc_ebye_buffer_chunks *) malloc (sizeof (lwroc_ebye_buffer_chunks));

  if (!lbc)
    LWROC_FATAL("Memory allocation failure (ebye buffer chunks).");

  memset (lbc, 0, sizeof (*lbc));

  LBC->flags = flags;

  return LBC;
}

void lwroc_ebye_buffer_chunks_init(lwroc_gdf_buffer_chunks *lbc_gdf,
				   uint32_t buffer_size,
				   int skip_internal_events)
{
  lwroc_gdf_buffer_chunks_init(lbc_gdf,
			       buffer_size, skip_internal_events);
}

void lwroc_ebye_buffer_chunks_get_fixed_raw(lwroc_gdf_buffer_chunks *lbc_gdf,
					    void **ptr, size_t *size)
{
  lwroc_ebye_buffer_chunks *lbc = (lwroc_ebye_buffer_chunks *) lbc_gdf;

  *ptr  =         lbc->fixed.raw;
  *size = sizeof (lbc->fixed.raw);
}

int lwroc_ebye_buffer_chunks_collect(/*lwroc_ebye_buffer_chunks *lbc,*/
				     lwroc_gdf_buffer_chunks *lbc_gdf,
				     lwroc_pipe_buffer_consumer *pipe_buf,
				     const lwroc_thread_block *thread_block,
				     void *v_sticky_store,
				     void *v_sticky_store_drvk,
				     int partial_buffer, int for_skipping)
{
  lwroc_ebye_buffer_chunks *lbc = (lwroc_ebye_buffer_chunks *) lbc_gdf;
  int buffer_full = 0;
  int flush_buffer = 0;
  size_t avail = 0;
  char *ptr;

  (void) v_sticky_store;
  (void) v_sticky_store_drvk;

 check_buffer_avail:
  for ( ; ; )
    {
      /* If there is no data, next time to be woken up is when there
       * is additionally what is missing to get a complete buffer.
       */
      /* We always reserve space enough to allow the record header (24
       * bytes) be replaced by an xfer header (32 bytes).
       */
      size_t hysteresis =
	LBC->buffer_size - sizeof (ebye_xfer_header) -
	LBC->chunks[LWROC_GDF_BUF_CHUNK_DATA_1].size -
	LBC->chunks[LWROC_GDF_BUF_CHUNK_DATA_2].size;
      size_t waste_sz;

      avail = lwroc_pipe_buffer_avail_read(pipe_buf,
					   thread_block,
					   &ptr, &waste_sz, LBC->collect_used,
					   hysteresis);

      if (!avail)
	{
	  if (waste_sz)
	    {
	      /* If we are at offset 0, then we may immediately release
	       * this part of the buffer.  Otherwise, we just skip it.
	       */

	      if (LBC->collect_used == 0)
		lwroc_pipe_buffer_waste_read(pipe_buf, waste_sz);
	      else
		{
		  LBC->collect_used += waste_sz;
		  if (LBC->collect_chunk != LWROC_GDF_BUF_CHUNK_DATA_1)
		    LWROC_BUG("Found two waste headers for one buffer.");
		  LBC->collect_chunk++;
		}
	      continue; /* Try again. */
	    }
	  break;
	}
      break;
    }

  if (avail)
    {
      /* For the moment, let's be plain stupid...
       * We write whatever amount of data is available, i.e.
       * are not waiting until we get a full buffer.
       */

      uint32_t use = 0;

      /* Data is available here, and is no waste header, so we can
       * set pointer.
       */
      if (LBC->chunks[LBC->collect_chunk].ptr == NULL)
	LBC->chunks[LBC->collect_chunk].ptr = ptr;

      for ( ; ; )
	{
	  uint32_t *p0;
	  size_t s;
	  uint32_t ts_lo;
	  uint32_t w0, w1, w0_type, w1_ts;
	  int had_non_info = 0;

	  if (avail < 2 * sizeof (uint32_t))
	    LWROC_BUG_FMT("Incomplete two-word entry in data buffer "
			  "(%" MYPRIzd " bytes available).",
			  avail);

	  p0 = (uint32_t *) (ptr + use);

	  (void) p0;

	  w0 = p0[0];
	  w1 = p0[1];

	  w0_type = (w0 >> 30) & 0x03;
	  w1_ts   = (w1      ) & 0x0fffffff;

	  ts_lo = w1_ts;

	  if (w0_type == 0x2) /* info */
	    ;
	  else
	    had_non_info = 1;

	  s = 2 * sizeof (uint32_t);

	  /* Also include two-words as long as the timestamp (bits
	   * 27..0) are the same.  Or until we hit the end of data
	   * (guaranteed entry break-point).
	   */
	  while (avail >= use + s + 2 * sizeof (uint32_t))
	    {
	      uint32_t ts_lo_new;
	      /* Another two-word is available.  Does it have the same
	       * low timestamp?
	       */
	      p0 += 2;

	      w0 = p0[0];
	      w1 = p0[1];

	      w0_type = (w0 >> 30) & 0x03;
	      w1_ts   = (w1      ) & 0x0fffffff;

	      if (w0_type == 0x2) /* info */
		{
		  /* TODO: this would break for trace data, since that
		   * has raw data words.
		   */
		  if (had_non_info)
		    break; /* Info after non-info.  Break hit. */
		}
	      else
		had_non_info = 1;

	      ts_lo_new = w1_ts;

	      if (ts_lo_new != ts_lo)
		{
		  /* Not same timestamp, so take the data until (not
		   * including) this point.
		   */
		  break;
		}
	      if (s >= LBC->buffer_size / 2)
		{
		  /* This has become way larger than ever expected for
		   * sensible data.  Break it such that we do not crash.
		   */
		  LWROC_ERROR_FMT("Accumulated way too many two-word items "
				  "(%" MYPRIzd " bytes > "
				  "half buffer size = %d).  "
				  "Breaking hit.",
				  s, LBC->buffer_size / 2);
		  break;
		}
	      /* Include these two words as well. */
	      s += 2 * sizeof (uint32_t);
	    }

	  /* TODO: due to the check just above, this can only happen
	   * if we would be handling trace data, that has more than
	   * just the fixed size two words.
	   */
	  if (use + s > avail)
	    LWROC_BUG_FMT("Incomplete entry in data buffer "
			  "(%" MYPRIzd " bytes needed, "
			  "%" MYPRIzd " available).",
			  use + s, avail);

	  if (LBC->chunks[LWROC_GDF_BUF_CHUNK_DATA_1].size +
	      LBC->chunks[LWROC_GDF_BUF_CHUNK_DATA_2].size +
	      s + sizeof (ebye_xfer_header) > LBC->buffer_size)
	    {
	      if (LBC->chunks[LWROC_GDF_BUF_CHUNK_DATA_1].size +
		  LBC->chunks[LWROC_GDF_BUF_CHUNK_DATA_2].size == 0)
		{
		  /* This in principle we should guard against at an earlier
		   * location, by failing the input.  But better report
		   * a failure than running an infinite loop here.
		   */
		  LWROC_FATAL_FMT("Event longer (%" MYPRIzd " bytes) than "
				  "output ebye buffer (%" PRIu32 " bytes).",
				  s + sizeof (ebye_xfer_header),
				  LBC->buffer_size);
		}
	      buffer_full = 1;
	      break;
	    }

	  LBC->collect_used += s;

	  LBC->collect_events++;
	  LBC->chunks[LBC->collect_chunk].size += s;

	  use += (uint32_t) s;
	  if (use >= avail)
	    break;
	}

      if (LBC->collect_used == 0)
	LWROC_BUG("collect_used == 0, despite avail != 0");

      assert(LBC->collect_events > 0);

      /* We may at this point have read just to the end of available
       * linear space.  In that case, we should retry getting data
       * once more.  If we reached the end with a waste buffer, that
       * was already handled, so only case is when we have already
       * accepted all data into the buffer.
       */

      if (use == avail)
	goto check_buffer_avail;
    }

  if (!LBC->collect_used)
    return 0;

  if (for_skipping)
    {
      LBC->fully_used = LBC->collect_used;
      return 1;
    }

  if (!flush_buffer && !partial_buffer && !buffer_full)
    return 0;

  /* Ok, so we have found ourselves some data to write. */

  lwroc_ebye_buffer_chunks_prepare_write(lbc);

  return 1;
}

uint32_t lwroc_ebye_buffer_chunks_write_size(lwroc_ebye_buffer_chunks *lbc,
					     size_t total_use)
{
  uint32_t write_size;

  if (LBC->var_buffer_size == LWROC_GDF_BUF_VARBUFSIZE_TRIM)
    write_size = (uint32_t) total_use;
  else if (LBC->var_buffer_size == LWROC_GDF_BUF_VARBUFSIZE_TRIM_1024)
    write_size = (uint32_t) ((total_use + 1023) & (size_t) ~1023);
  else
    write_size = LBC->buffer_size;

   return write_size;
}

void lwroc_ebye_buffer_chunks_prepare_write(lwroc_ebye_buffer_chunks *lbc)
{
  uint32_t write_size;
  size_t data_use =
    LBC->chunks[LWROC_GDF_BUF_CHUNK_DATA_1].size +
    LBC->chunks[LWROC_GDF_BUF_CHUNK_DATA_2].size;
  size_t total_use;

  if (LBC->flags & LWROC_GDF_FLAGS_FILE)
    {
      total_use = data_use + sizeof (ebye_record_header);

      write_size = lwroc_ebye_buffer_chunks_write_size(lbc, total_use);

      /* Fill out the buffer header. */
      lwroc_ebye_buffer_chunks_record(lbc,
				      &lbc->fixed.record,
				      (uint32_t) data_use);

      /* lwroc_ebye_record_dump(&lbc->fixed.record); */

      LBC->chunks[LWROC_GDF_BUF_CHUNK_BUFHE].ptr  =
	&lbc->fixed.record;
      LBC->chunks[LWROC_GDF_BUF_CHUNK_BUFHE].size =
	sizeof (lbc->fixed.record);
    }
  else
    {
      total_use = data_use + sizeof (ebye_xfer_header);

      write_size = lwroc_ebye_buffer_chunks_write_size(lbc, total_use);

      /* Fill out the buffer header. */
      lwroc_ebye_buffer_chunks_xfer(lbc,
				    &lbc->fixed.xfer,
				    write_size,
				    (uint32_t) data_use);

      /* lwroc_ebye_xfer_dump(&lbc->fixed.xfer); */

      LBC->chunks[LWROC_GDF_BUF_CHUNK_BUFHE].ptr  =
	&lbc->fixed.xfer;
      LBC->chunks[LWROC_GDF_BUF_CHUNK_BUFHE].size =
	sizeof (lbc->fixed.xfer);
    }
  LBC->chunks[LWROC_GDF_BUF_CHUNK_FRAG_EV_HEADER].ptr  = NULL;
  LBC->chunks[LWROC_GDF_BUF_CHUNK_FRAG_EV_HEADER].size = 0;
  /* LWROC_GDF_BUF_CHUNK_DATA_1 and LWROC_GDF_BUF_CHUNK_DATA_2
   * already filled.
   */
  LBC->chunks[LWROC_GDF_BUF_CHUNK_ZERO_PAD].ptr  = _lwroc_zero_block;
  LBC->chunks[LWROC_GDF_BUF_CHUNK_ZERO_PAD].size =
    write_size - total_use;

  /*
  LWROC_INFO_FMT("data_use: %zd total_use: %zd write_size: %d pad: %zd\n",
		 data_use, total_use, write_size,
		 LBC->chunks[LWROC_GDF_BUF_CHUNK_ZERO_PAD].size);
  */

  LBC->fully_used = LBC->collect_used;
  LBC->done_events = LBC->collect_events;

  /*There is data to write! */

  LBC->chunk = LWROC_GDF_BUF_CHUNK_BUFHE;
}

void lwroc_ebye_buffer_chunks_record(lwroc_ebye_buffer_chunks *lbc,
				     ebye_record_header *record,
				     uint32_t used)
{
  memset (record, 0, sizeof (*record));

  memcpy (&record->_id, EBYE_RECORD_HEADER_ID, sizeof (record->_id));

  record->_sequence    = LBC->buffer_no++;
  record->_tape        = 1;
  record->_stream      = 0;
  record->_endian_tape = 1; /* This header is in native endianess. */
  record->_endian_data = 1; /* Data is written in native endianess. */
  record->_data_length = used;
}

void lwroc_ebye_xfer_ntoh(ebye_xfer_header *xfer)
{
  xfer->_flags        = ntohs(xfer->_flags);
  xfer->_stream       = ntohs(xfer->_stream);
  /* xfer->_endian */
  xfer->_id           = ntohs(xfer->_id);
  xfer->_sequence     = ntohl(xfer->_sequence);
  xfer->_block_length = ntohl(xfer->_block_length);
  xfer->_data_length  = ntohl(xfer->_data_length);
  xfer->_offset       = ntohl(xfer->_offset);
  xfer->_id1          = ntohl(xfer->_id1);
  xfer->_id2          = ntohl(xfer->_id2);
}

void lwroc_ebye_xfer_hton(ebye_xfer_header *xfer)
{
  xfer->_flags        = htons(xfer->_flags);
  xfer->_stream       = htons(xfer->_stream);
  /* xfer->_endian */
  xfer->_id           = htons(xfer->_id);
  xfer->_sequence     = htonl(xfer->_sequence);
  xfer->_block_length = htonl(xfer->_block_length);
  xfer->_data_length  = htonl(xfer->_data_length);
  xfer->_offset       = htonl(xfer->_offset);
  xfer->_id1          = htonl(xfer->_id1);
  xfer->_id2          = htonl(xfer->_id2);
}

void lwroc_ebye_buffer_chunks_xfer(lwroc_ebye_buffer_chunks *lbc,
				   ebye_xfer_header *xfer,
				   uint32_t buffer_size, uint32_t used)
{
  memset (xfer, 0, sizeof (*xfer));

  xfer->_flags        = 2; /* No acq. */
  xfer->_stream       = 1;
  xfer->_endian       = 1; /* Data is written in native endianess. */
  xfer->_id           = 0;
  xfer->_sequence     = (LBC->buffer_no++);
  xfer->_block_length = buffer_size;
  xfer->_data_length  = used;
  xfer->_offset       = 0;
  xfer->_id1          = EBYE_XFER_HEADER_ID1;
  xfer->_id2          = EBYE_XFER_HEADER_ID2;

  /* lwroc_ebye_xfer_dump(xfer); */

  lwroc_ebye_xfer_hton(xfer);
}

void lwroc_ebye_record_dump(ebye_record_header *record)
{
  char print_id[9];

  lwroc_string_to_print(print_id, record->_id, 8);

  printf ("%s seq=%10d tape=%d stream=%d "
	  "endian=%04x/%04x length=%5d=0x%04x\n",
	  print_id,
	  record->_sequence,
	  record->_tape,
	  record->_stream,
	  record->_endian_tape,
	  record->_endian_data,
	  record->_data_length,
	  record->_data_length);
}

void lwroc_ebye_xfer_dump(ebye_xfer_header *xfer)
{
  printf ("xfer flg=%d str=%d "
	  "endian=%04x id=%d seq=%10d length=%5d:%5d off=%d id=%08x:%08x\n",
	  xfer->_flags,
	  xfer->_stream,
	  xfer->_endian,
	  xfer->_id,
	  xfer->_sequence,
	  xfer->_block_length,
	  xfer->_data_length,
	  xfer->_offset,
	  xfer->_id1,
	  xfer->_id2);
}

void lwroc_ebye_buffer_chunks_file_header(lwroc_gdf_buffer_chunks *lbc_gdf)
{
  lwroc_ebye_buffer_chunks *lbc = (lwroc_ebye_buffer_chunks *) lbc_gdf;
  (void) lbc;

  /* EBYE files has no file header, only record headers. */
}

void lwroc_ebye_buffer_chunks_empty_xfer(lwroc_gdf_buffer_chunks *lbc_gdf,
					 int resize_startup, uint32_t port_hint)
{
  lwroc_ebye_buffer_chunks *lbc = (lwroc_ebye_buffer_chunks *) lbc_gdf;

  ebye_xfer_header *xfer = &lbc->fixed.xfer;

  uint32_t write_size;

  /* Non-first resize blocks shall have the actual size. */
  write_size = LBC->buffer_size;

  if (LBC->var_buffer_size == LWROC_GDF_BUF_VARBUFSIZE_TRIM)
    write_size = sizeof (*xfer);

  memset (xfer, 0, sizeof (*xfer));

  xfer->_flags        = 2; /* No acq. */
  xfer->_stream       = 0;
  xfer->_endian       = 1;
  xfer->_id           = 0;
  xfer->_sequence     = (uint32_t) -1;
  xfer->_block_length = write_size;
  xfer->_data_length  = (uint32_t) -1;
  xfer->_id1          = EBYE_XFER_HEADER_ID1;
  xfer->_id2          = EBYE_XFER_HEADER_ID2;

  if (LBC->var_buffer_size != LWROC_GDF_BUF_VARBUFSIZE_TRIM &&
      resize_startup)
    {
      write_size = 0x400; /* 1024 */

      xfer->_offset       = port_hint;
    }

  /* lwroc_ebye_xfer_dump(xfer); */

  lwroc_ebye_xfer_hton(xfer);

  lwroc_gdf_buffer_chunks_send(LBC,
			       &lbc->fixed.xfer,
			       sizeof (ebye_xfer_header),
			       write_size - sizeof (ebye_xfer_header));

  /* printf ("empty: %" PRId32 "\n", write_size); */
}

void lwroc_ebye_buffer_chunks_empty(lwroc_gdf_buffer_chunks *lbc_gdf)
{
  lwroc_ebye_buffer_chunks_empty_xfer(lbc_gdf, 0, 0);
}

/* */

lwroc_gdf_format_functions _lwroc_ebye_format_functions =
{
  LWROC_DATA_TRANSPORT_FORMAT_EBYE, "EBYE",
  lwroc_ebye_buffer_chunks_alloc,
  lwroc_ebye_buffer_chunks_init,
  lwroc_ebye_buffer_chunks_get_fixed_raw,
  lwroc_ebye_buffer_chunks_collect,
  lwroc_ebye_buffer_chunks_file_header,
  lwroc_ebye_buffer_chunks_empty,
  LMD_EBYE_DEFAULT_SIZE,
  LMD_EBYE_DEFAULT_SIZE,
  1 /* runno-digits */, 1 /* fileno-digits */, 0 /* first-fileno-in-run */
};

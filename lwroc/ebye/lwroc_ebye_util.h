/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_EBYE_UTIL_H__
#define __LWROC_EBYE_UTIL_H__

#include "gdf/lwroc_gdf_util.h"

#include "ebye/lwroc_ebye_event.h"

extern lwroc_gdf_format_functions _lwroc_ebye_format_functions;

typedef struct lwroc_ebye_buffer_chunks_t
{
  struct lwroc_gdf_buffer_chunks_t _gdf;

  union
  {
    ebye_record_header  record;
    ebye_xfer_header    xfer;
    uint32_t raw[8];
  } fixed;
} lwroc_ebye_buffer_chunks;

int lwroc_ebye_buffer_chunks_collect(/*lwroc_ebye_buffer_chunks *lbc,*/
				     lwroc_gdf_buffer_chunks *lbc_gdf,
				     lwroc_pipe_buffer_consumer *pipe_buf,
				     const lwroc_thread_block *thread_block,
				     void *sticky_store,
				     void *sticky_store_drvk,
				     int partial_buffer, int for_skipping);

void lwroc_ebye_buffer_chunks_prepare_write(lwroc_ebye_buffer_chunks *lbc);

void lwroc_ebye_buffer_chunks_record(lwroc_ebye_buffer_chunks *lbc,
				     ebye_record_header *record,
				     uint32_t used);

void lwroc_ebye_buffer_chunks_xfer(lwroc_ebye_buffer_chunks *lbc,
				   ebye_xfer_header *record,
				   uint32_t buffer_size, uint32_t used);

void lwroc_ebye_record_dump(ebye_record_header *record);

void lwroc_ebye_xfer_dump(ebye_xfer_header *record);

void lwroc_ebye_xfer_hton(ebye_xfer_header *xfer);
void lwroc_ebye_xfer_ntoh(ebye_xfer_header *xfer);

void lwroc_ebye_buffer_chunks_empty_xfer(lwroc_gdf_buffer_chunks *lbc_gdf,
					 int resize, uint32_t port_hint);

/* Used also by xfer mode: */

lwroc_gdf_buffer_chunks *lwroc_ebye_buffer_chunks_alloc(int flags);

void lwroc_ebye_buffer_chunks_init(lwroc_gdf_buffer_chunks *lbc_gdf,
				   uint32_t buffer_size,
				   int skip_internal_events);

void lwroc_ebye_buffer_chunks_get_fixed_raw(lwroc_gdf_buffer_chunks *lbc_gdf,
					    void **ptr, size_t *size);

void lwroc_ebye_buffer_chunks_file_header(lwroc_gdf_buffer_chunks *lbc_gdf);
void lwroc_ebye_buffer_chunks_empty(lwroc_gdf_buffer_chunks *lbc_gdf);

/*****************************************************************************/

#endif/*__LWROC_EBYE_UTIL_H__*/

/* This file is part of UCESB - a tool for data unpacking and processing.
 *
 * Copyright (C) 2016  GSI Helmholtzzentrum fuer Schwerionenforschung GmbH
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_EBYE_RECORD_HEADER_HH__
#define __LWROC_EBYE_RECORD_HEADER_HH__

#ifndef EXT_USE_NO_DTC_ARCH
#include "../dtc_arch/acc_def/mystdint.h"
#else
#include <stdint.h>
#endif

/********************************************************************/

struct ebye_record_header_t
{
  uint8_t  _id[8];       /* EBYEDATA */
  uint32_t _sequence;
  uint16_t _tape;        /* 1    */
  uint16_t _stream;      /* 1..4 */
  uint16_t _endian_tape; /* native 1 by tape writer */
  uint16_t _endian_data; /* native 1 by data writer */
  uint32_t _data_length; /* in bytes, header not included */
};

typedef struct ebye_record_header_t ebye_record_header;

/********************************************************************/

#define EBYE_RECORD_HEADER_ID "EBYEDATA"

/********************************************************************/

/* Default file buffer size, 64 kiB. */
#define LMD_EBYE_DEFAULT_SIZE  0x10000

/********************************************************************/

#endif/*__LWROC_EBYE_RECORD_HEADER_HH__*/

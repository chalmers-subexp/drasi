/* This file is part of UCESB - a tool for data unpacking and processing.
 *
 * Copyright (C) 2024  GSI Helmholtzzentrum fuer Schwerionenforschung GmbH
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_EBYE_XFER_HEADER_HH__
#define __LWROC_EBYE_XFER_HEADER_HH__

#ifndef EXT_USE_NO_DTC_ARCH
#include "../dtc_arch/acc_def/mystdint.h"
#else
#include <stdint.h>
#endif

/********************************************************************/

struct ebye_xfer_header_t
{
  uint16_t _flags;
  uint16_t _stream;
  uint16_t _endian;
  uint16_t _id;
  uint32_t _sequence;
  uint32_t _block_length;
  uint32_t _data_length;
  uint32_t _offset;
  uint32_t _id1;
  uint32_t _id2;
};

typedef struct ebye_xfer_header_t ebye_xfer_header;

/********************************************************************/

#define EBYE_XFER_HEADER_ID1  0x19062002
#define EBYE_XFER_HEADER_ID2  0x09592400

/********************************************************************/

#endif/*__LWROC_EBYE_XFER_HEADER_HH__*/

#!/usr/bin/perl

use strict;
use warnings;

my %states = ( "HOLD" => 1,
	       "CLOSED" => 1,
	       "NEW" => 1,
	       "CLOSE" => 1,
	       "OPEN" => 1,
	       "FILE_HEADER_PREPARE" => 1,
	       "FILE_HEADER_WRITE" => 1,
	       "STICKY_COMPACT" => 1,
	       "STICKY_PREPARE" => 1,
	       "STICKY_WRITE" => 1,
	       "BUFFER_COLLECT" => 1,
	       "BUFFER_WRITE" => 1 );

my %default_next = ( "HOLD" => "-",
		     "CLOSED" => "-",
		     "NEW" => "CLOSE",
		     "CLOSE" => "OPEN",
		     "OPEN" => "FILE_HEADER_PREPARE",
		     "FILE_HEADER_PREPARE" => "FILE_HEADER_WRITE",
		     "FILE_HEADER_WRITE" => "STICKY_COMPACT",
		     "STICKY_COMPACT" => "STICKY_PREPARE",
		     "STICKY_PREPARE" => "STICKY_WRITE",
		     "STICKY_WRITE" => "STICKY_PREPARE",
		     "BUFFER_COLLECT" => "BUFFER_WRITE",
		     "BUFFER_WRITE" => "BUFFER_COLLECT" );

my %on_request_next = ( "HOLD:N" => "OPEN",
			"HOLD:O" => "OPEN",
			"HOLD:C" => "CLOSED",
			"CLOSED:N" => "OPEN",
			"CLOSED:O" => "OPEN",
			"CLOSED:H" => "HOLD",
			"CLOSE:H" => "HOLD",
			"CLOSE:U" => "HOLD",
			"CLOSE:C" => "CLOSED",
			"CLOSE:N" => "OPEN",
			"FILE_HEADER_WRITE:H" => "CLOSE",
			"FILE_HEADER_WRITE:U" => "CLOSE",
			"FILE_HEADER_WRITE:C" => "CLOSE",
			"STICKY_WRITE:H" => "CLOSE",
			"STICKY_WRITE:U" => "CLOSE",
			"STICKY_WRITE:C" => "CLOSE",
			"BUFFER_COLLECT:H" => "CLOSE",
			"BUFFER_COLLECT:C" => "CLOSE",
			"BUFFER_COLLECT:N" => "BUFFER_WRITE",
			"BUFFER_WRITE:H" => "CLOSE",
			"BUFFER_WRITE:U" => "CLOSE",
			"BUFFER_WRITE:C" => "CLOSE",
			"STICKY_WRITE:N" => "NEW",
			"BUFFER_WRITE:N" => "NEW" );

my %fulfill_request = ( "H" => "HOLD",
			"U" => "HOLD",
			"C" => "CLOSED",
			"N" => "OPEN",
			"O" => "OPEN" );

# Check that wherever we set a new request, it is possible to reach
# the state which kills the request.

sub follow_state($$$);

foreach my $request (sort keys %fulfill_request)
{
    print "Check $request.\n";

  check_request_state:
    foreach my $state (sort keys %states)
    {
	# Special: OPEN request can only be issued on HOLD or CLOSED.
	# UNJAM can only be issued on _WRITE states

	if ($request eq "O" &&
	    !($state eq "HOLD" ||
	      $state eq "CLOSED")) {
	    next check_request_state;
	}
	if ($request eq "U" &&
	    !($state eq "FILE_HEADER_WRITE" ||
	      $state eq "STICKY_WRITE" ||
	      $state eq "BUFFER_WRITE")) {
	    next check_request_state;
	}

	print " Check $state: ";

	follow_state($state, $request, 0);

	print "\n";
    }
}

sub follow_state($$$)
{
    my $state = shift;
    my $request = shift;
    my $depth = shift;

    if (!defined($states{$state}))
    {
	die "Unknown state '$state'.\n";
    }

    if ($fulfill_request{$request} eq $state)
    {
	return;
    }

    if ($depth > 20)
    {
	print "\n"; flush stdout;
	die "Too deep!  Did not reach target for request '$request'!\n";
    }

    # Do we have a special next state with this request?

    my $request_next = $on_request_next{"$state:$request"};

    if ($request_next)
    {
	print " -[$request]-> $request_next";

	follow_state($request_next, $request, $depth+1);
	return;
    }

    my $default_next = $default_next{"$state"};

    if (!defined($default_next))
    {
	print "\n"; flush stdout;
	die "Continuation for '$state' not known.\n";
    }

    if ($default_next eq "-")
    {
	print "\n"; flush stdout;
	die "State '$state' has no continuation, stuck!\n";
    }

    if ($default_next eq "x")
    {
	print "\n"; flush stdout;
	die "State '$state' has no valid continuation without request!\n";
    }

    print " -> $default_next";

    follow_state($default_next, $request, $depth+1);
}

#!/bin/sh

# This file is part of DRASI - a data acquisition data pump.
#
# Copyright (C) 2018  Haakan T. Johansson  <f96hajo@chalmers.se>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301  USA

GENDIR_ARCH=$1
GENDIR=$2

OUTFILE=$1/git-describe-include.h
TMPFILE=$1/git-describe-include.h.tmp

FILEGENERIC=$2/git-describe-include-generic.h

set -e

GITDESCRIBE=\
`git describe --all --always --dirty --long --match donotmatchthis \
2>/dev/null || echo "no-git-describe"`

mkdir -p $GENDIR 2> /dev/null || \
    mkdir -fp $GENDIR 2> /dev/null
mkdir -p $GENDIR_ARCH 2> /dev/null || \
    mkdir -fp $GENDIR_ARCH 2> /dev/null

cat << EOF > $TMPFILE
/* File is automatically generated, do not edit. */

#define GIT_DESCRIBE_STRING  "$GITDESCRIBE"
EOF

# If we succeeded, we want to use our version.
# If we did not succeed, we want to use the generic version, if it exist.

# Rationale for generic file: special systems may not have git, but if
# directories are first built on the system where git was used, then
# that will provide the data.

SRCFILE=$TMPFILE

if ! grep "no-git-describe" $TMPFILE
then
    # We succeeded.  Replace generic file.
    cat << EOF > $FILEGENERIC
/* File is automatically generated, do not edit. */

#define GIT_DESCRIBE_STRING  "$GITDESCRIBE-NO-NATIVE-GIT"
EOF
elif [ -e $FILEGENERIC ]
then
    # Not successful, use generic file as source.
    SRCFILE=$FILEGENERIC
else
    echo
    #echo FAIL
    #echo "Please first compile on a system that can run git describe." 1>&2
    #exit 1
fi

# Copy the file only if contents changed.  Otherwise, we always rebuild.

if ! cmp -s "$SRCFILE" "$OUTFILE" >/dev/null 2>/dev/null
then
    # echo Differ 1>&2
    cp $SRCFILE $OUTFILE || \
        (echo FAIL && \
	 echo "Failed to copy to $OUTFILE" 1>&2 && exit 1)

fi

# echo Forced 1>&2
# ls -ltr --full-time $OUTFILE 1>&2

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Bastian Loeher  <b.loeher@gsi.de>
 * Copyright (C) 2023  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "gdf/lwroc_gdf_util.h"
#include "lwroc_message.h"
#include "lwroc_net_trans.h"
#include "lwroc_crc32.h"

#include "gen/lwroc_data_transport_setup_serializer.h"

#include <assert.h>
#include <string.h>

#include "../dtc_arch/acc_def/myinttypes.h"
#include "../dtc_arch/acc_def/myprizd.h"

void lwroc_gdf_buffer_chunks_init(lwroc_gdf_buffer_chunks *lbc,
				  uint32_t buffer_size,
				  int skip_internal_events)
{
  lbc->buffer_no = 1;
  lbc->buffer_size = buffer_size;

  lbc->skip_internal_events = skip_internal_events;
  lbc->var_buffer_size = 0;

  lbc->chunk = LWROC_GDF_BUF_CHUNK_NUM; /* currently not writing */

  lbc->has_sticky = 0;
  lbc->collect_used = 0;
  lbc->collect_events = 0;
  lbc->collect_chunk = LWROC_GDF_BUF_CHUNK_DATA_1;
  lbc->chunks[LWROC_GDF_BUF_CHUNK_DATA_1].ptr = NULL;
  lbc->chunks[LWROC_GDF_BUF_CHUNK_DATA_1].size = 0;
  lbc->chunks[LWROC_GDF_BUF_CHUNK_DATA_2].ptr = NULL;
  lbc->chunks[LWROC_GDF_BUF_CHUNK_DATA_2].size = 0;
}


int lwroc_gdf_buffer_chunks_pending(lwroc_gdf_buffer_chunks *lbc)
{
  return lbc->chunk != LWROC_GDF_BUF_CHUNK_NUM;
}

ssize_t lwroc_gdf_buffer_chunks_write(lwroc_gdf_buffer_chunks *lbc,
				      lwroc_write_func write_func,
				      void *write_arg, uint32_t *pcrc)
{
  /* We should only get called when there is something to write! */

  /* No need to skip here, the first chunk is never zero size; we
   * always write a bufhe.
   */

  lwroc_gdf_buffer_chunk *chunk = &lbc->chunks[lbc->chunk];

  size_t towrite = chunk->size;
  ssize_t n;

  if (lbc->chunk == LWROC_GDF_BUF_CHUNK_ZERO_PAD &&
      towrite > sizeof (_lwroc_zero_block))
    towrite = sizeof (_lwroc_zero_block);

  n = write_func (write_arg, chunk->ptr, towrite);

  if (n == -1)
    return -1; /* caller deals with errno */

  /* Update the crc with the data actually written. */
  if (pcrc)
    lwroc_crc32_calc(chunk->ptr, (size_t) n, pcrc);

  if (lbc->chunk != LWROC_GDF_BUF_CHUNK_ZERO_PAD)
    chunk->ptr += n;
  chunk->size -= (size_t) n;

  while (!chunk->size)
    {
      lbc->chunk++;

      if (lbc->chunk == LWROC_GDF_BUF_CHUNK_NUM)
	break;

      /* Skip past any empty chunks... */

      chunk = &lbc->chunks[lbc->chunk];
    }

  return n;
}

size_t lwroc_gdf_buffer_chunks_wr_buf(lwroc_gdf_buffer_chunks *lbc,
				      char *buf, size_t bufsize)
{
  size_t total = 0;

  while (lbc->chunk != LWROC_GDF_BUF_CHUNK_NUM)
    {
      lwroc_gdf_buffer_chunk *chunk = &lbc->chunks[lbc->chunk];
      size_t towrite = chunk->size;

      if (total + towrite > bufsize)
	LWROC_BUG_FMT("Attempt to write chunk outside buffer "
		      "(%" MYPRIzd " > %" MYPRIzd ").",
		      total + towrite, bufsize);

      if (lbc->chunk == LWROC_GDF_BUF_CHUNK_ZERO_PAD)
	memset(buf + total, 0, towrite);
      else
	memcpy(buf + total, chunk->ptr, towrite);

      total += towrite;

      lbc->chunk++;
    }
  return total;
}

void lwroc_gdf_buffer_chunks_release(lwroc_gdf_buffer_chunks *lbc,
				     lwroc_pipe_buffer_consumer *pipe_buf,
				     uint32_t *done_events)
{
  if (done_events)
    *done_events = lbc->done_events;

  /* We are done writing these buffer chunks.
   */

  /* Release the data as far as is possible. */
  /*
  printf ("Release, data-ptr: %p -- %p\n",
	  lbc->chunks[LWROC_GDF_BUF_CHUNK_DATA_1].ptr,
	  lbc->chunks[LWROC_GDF_BUF_CHUNK_DATA_1].ptr + lbc->fully_used);
  */
  /* If we have an output fragmented event, move the write chunk
   * forward.  And create the fragmentation event header.
   */

  if (lbc->fully_used)
    lwroc_pipe_buffer_did_read(pipe_buf, lbc->fully_used);

  /* Such that we can be called again without harm. */
  lbc->fully_used = 0;
  lbc->done_events = 0;

  lbc->has_sticky = 0;
  lbc->collect_used = 0;
  lbc->collect_events = 0;
  lbc->collect_chunk = LWROC_GDF_BUF_CHUNK_DATA_1;
  lbc->chunks[LWROC_GDF_BUF_CHUNK_DATA_1].ptr = NULL;
  lbc->chunks[LWROC_GDF_BUF_CHUNK_DATA_1].size = 0;
  lbc->chunks[LWROC_GDF_BUF_CHUNK_DATA_2].ptr = NULL;
  lbc->chunks[LWROC_GDF_BUF_CHUNK_DATA_2].size = 0;
}

void lwroc_gdf_buffer_chunks_send(lwroc_gdf_buffer_chunks *lbc,
				  void *ptr, size_t size,
				  size_t pad)
{
  /* Put it in the first chunk, so it gets written.
   */

  memset (lbc->chunks, 0, sizeof (lbc->chunks));

  lbc->chunks[LWROC_GDF_BUF_CHUNK_BUFHE].ptr  = ptr;
  lbc->chunks[LWROC_GDF_BUF_CHUNK_BUFHE].size = size;

  if (pad)
    {
      lbc->chunks[LWROC_GDF_BUF_CHUNK_ZERO_PAD].ptr  = _lwroc_zero_block;
      lbc->chunks[LWROC_GDF_BUF_CHUNK_ZERO_PAD].size = pad;
    }

  lbc->chunk = LWROC_GDF_BUF_CHUNK_BUFHE;

  /* Nothing to release! */
  lbc->done_events = 0;
  lbc->fully_used = 0;
}

void lwroc_gdf_buffer_chunks_data_trans_setup(lwroc_gdf_buffer_chunks *lbc_gdf,
					      lwroc_gdf_format_functions *fmt,
					      uint32_t max_ev_len,
					      uint32_t max_event_interval)
{
  lwroc_data_transport_setup setup_info;
  void   *fixed_raw;
  size_t  size_fixed_raw;

  /* Report no promised interval (0) as infinite. */
  if (!max_event_interval)
    max_event_interval = (uint32_t) -1;

  setup_info._max_buf_size = lbc_gdf->buffer_size;
  setup_info._max_ev_len = max_ev_len;
  setup_info._max_event_interval = max_event_interval;
  setup_info._data_format = fmt->_buffer_format;

  fmt->chunks_get_fixed_raw(lbc_gdf, &fixed_raw, &size_fixed_raw);

  /*
  printf ("size: %zd %zd\n",
	  size_fixed_raw, lwroc_data_transport_setup_serialized_size());
  fflush(stdout);
  */
  assert(size_fixed_raw >=
	 lwroc_data_transport_setup_serialized_size());
  lwroc_data_transport_setup_serialize((char *) fixed_raw, &setup_info);

  lwroc_gdf_buffer_chunks_send(lbc_gdf,
			       fixed_raw,
			       lwroc_data_transport_setup_serialized_size(),
			       0);
}

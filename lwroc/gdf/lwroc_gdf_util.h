/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2023  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_GDF_UTIL_H__
#define __LWROC_GDF_UTIL_H__

#include "lwroc_pipe_buffer.h"
#include "lwroc_data_pipe.h"
#include "lwroc_thread_block.h"
#include "lwroc_net_proto.h"
#include "lwroc_net_legacy.h"
#include "lwroc_file_functions.h"
#include "lwroc_iterate_event.h"

/*****************************************************************************/

#define LWROC_GDF_BUF_CHUNK_BUFHE           0
#define LWROC_GDF_BUF_CHUNK_FRAG_EV_HEADER  1
#define LWROC_GDF_BUF_CHUNK_DATA_1          2
#define LWROC_GDF_BUF_CHUNK_DATA_2          3
#define LWROC_GDF_BUF_CHUNK_ZERO_PAD        4
#define LWROC_GDF_BUF_CHUNK_NUM             5

#define LWROC_GDF_BUF_VARBUFSIZE_NO         0
#define LWROC_GDF_BUF_VARBUFSIZE_TRIM       1
#define LWROC_GDF_BUF_VARBUFSIZE_TRIM_1024  2

#define LWROC_GDF_FLAGS_FILE                0x01
#define LWROC_GDF_FLAGS_NET                 0x02

typedef struct lwroc_gdf_buffer_chunk_t
{
  void   *ptr;
  size_t  size;
} lwroc_gdf_buffer_chunk;

typedef struct lwroc_gdf_buffer_chunks_t
{
  uint32_t     buffer_no;
  uint32_t     buffer_size;

  /* Used amount of data and found events while collecting. */
  int          has_sticky;
  size_t       collect_used;
  uint32_t     collect_events;
  int          collect_chunk;

  /* Completely consumed event data once written. */
  size_t       fully_used;
  /* For accounting: */
  uint32_t     done_events;

  /* Configuration/behaviour. */
  int          skip_internal_events;
  int          var_buffer_size;

  int          flags;

  /* Chunks for keeping track during writing. */

  lwroc_gdf_buffer_chunk chunks[LWROC_GDF_BUF_CHUNK_NUM];
  int          chunk;
} lwroc_gdf_buffer_chunks;

void lwroc_gdf_buffer_chunks_init(lwroc_gdf_buffer_chunks *lbc,
				  uint32_t buffer_size,
				  int skip_internal_events);

int lwroc_gdf_buffer_chunks_pending(lwroc_gdf_buffer_chunks *lbc);

ssize_t lwroc_gdf_buffer_chunks_write(lwroc_gdf_buffer_chunks *lbc,
				      lwroc_write_func write_func,
				      void *write_arg, uint32_t *pcrc);

size_t lwroc_gdf_buffer_chunks_wr_buf(lwroc_gdf_buffer_chunks *lbc,
				      char *buf, size_t bufsize);

/* */

void lwroc_gdf_buffer_chunks_release(lwroc_gdf_buffer_chunks *lbc,
				     lwroc_pipe_buffer_consumer *pipe_buf,
				     uint32_t *done_events);

void lwroc_gdf_buffer_chunks_send(lwroc_gdf_buffer_chunks *lbc,
				  void *ptr, size_t size,
				  size_t pad);

/* */

void lwroc_gdf_buffer_chunks_data_trans_setup(lwroc_gdf_buffer_chunks *lbc_gdf,
					      lwroc_gdf_format_functions *fmt,
					      uint32_t max_ev_len,
					      uint32_t max_event_interval);

/*****************************************************************************/

typedef lwroc_gdf_buffer_chunks *
/* */        (*lwroc_gdf_chunks_alloc )(int flags);
typedef void (*lwroc_gdf_chunks_init  )(lwroc_gdf_buffer_chunks *lbc_gdf,
					uint32_t buffer_size,
					int skip_internal_events);
typedef void (*lwroc_gdf_chunks_get_fixed_raw)(lwroc_gdf_buffer_chunks *lbc_gdf,
					       void **ptr, size_t *size);
typedef int (*lwroc_gdf_chunks_collect)(lwroc_gdf_buffer_chunks *lbc_gdf,
					lwroc_pipe_buffer_consumer *pipe_buf,
					const lwroc_thread_block *thread_block,
					void *sticky_store,
					void *sticky_store_drvk,
					int partial_buffer, int for_skipping);
typedef void (*lwroc_gdf_chunks_empty )(lwroc_gdf_buffer_chunks *lbc_gdf);
typedef void (*lwroc_gdf_chunks_file_header)(lwroc_gdf_buffer_chunks *lbc_gdf);

/* typedef in lwroc_data_pipe.h */
struct lwroc_gdf_format_functions_t
{
  uint32_t                      _buffer_format;
  const char                   *_name;
  lwroc_gdf_chunks_alloc        chunks_alloc;
  lwroc_gdf_chunks_init         chunks_init;
  lwroc_gdf_chunks_get_fixed_raw chunks_get_fixed_raw;
  lwroc_gdf_chunks_collect      chunks_collect;
  lwroc_gdf_chunks_file_header  chunks_file_header;
  lwroc_gdf_chunks_empty        chunks_empty;
  uint32_t                      _default_net_buffer_size;
  uint32_t                      _default_file_buffer_size;

  int                           _default_runno_digits;
  int                           _default_fileno_digits;
  uint32_t                      _default_first_fileno;
};

/*****************************************************************************/

#endif/*__LWROC_GDF_UTIL_H__*/

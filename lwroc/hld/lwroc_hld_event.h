/* This file is part of UCESB - a tool for data unpacking and processing.
 *
 * Copyright (C) 2016  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_HLD_EVENT_H__
#define __LWROC_HLD_EVENT_H__

#include "../dtc_arch/acc_def/mystdint.h"

/* For the decoding member:
 *
 * High byte = 00.
 * Second (high) byte: shift of subevent alignment
 *  (0 = 8 bits, 1 = 16 bits, 2 = 32 bits; align = 1 << shift
 * Low word: decoding type.  Note: low byte != 0.
 */

typedef struct hld_event_t
{
  uint32_t _size;     /* Size of event in bytes, including header.  */
  uint32_t _decoding; /* Subevent alignment and data decoding type. */
  uint32_t _id;       /* Type of event.                             */
  uint32_t _seq_no;   /* Event counter (use with file_nr).          */
  uint32_t _date;     /* Date 0x00yymmdd yy in years since 1900.    */
  uint32_t _time;     /* Time 0x00hhmmss.                           */
  uint32_t _file_no;  /* File counter.                              */
  uint32_t _pad;
} hld_event;

typedef struct hld_subevent_t
{
  uint32_t _size;     /* Size of event in bytes, including header.  */
  uint32_t _decoding; /* Subevent word size and data decoding type. */
  uint32_t _id;       /* Type of subevent.                          */
  uint32_t _trig_no;  /* Trigger number.                            */
} hld_subevent;

#endif/*__LWROC_HLD_EVENT_H__*/

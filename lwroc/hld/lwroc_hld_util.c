/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2022  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "hld/lwroc_hld_util.h"
#include "lwroc_message.h"

#include "../dtc_arch/acc_def/myinttypes.h"
#include "../dtc_arch/acc_def/myprizd.h"
#include "../dtc_arch/acc_def/byteswap_include.h"

int lwroc_hld_get_event(lwroc_iterate_event *iter_data,
			const hld_event **ev_header,
			lwroc_iterate_event *event_data,
			int *header_swap)
{
  const hld_event *ev_head;
  uint32_t length;

  int native, isswap;

  if (iter_data->_size < sizeof (hld_event))
    {
      LWROC_ERROR_FMT("Incomplete HLD header in remaining event payload "
		      "(%" MYPRIzd " bytes available < %" MYPRIzd ").",
		      iter_data->_size, sizeof (hld_event));
      return 0;
    }

  ev_head = (const hld_event *) iter_data->_p;

  /* Figure out the swapping.
   * The decoding member shall be either 0 for 0xff000000 (native)
   * or for 0x000000ff.  Not both.
   */

  native = !(ev_head->_decoding & 0xff000000);
  isswap = !(ev_head->_decoding & 0x000000ff);

  if (native + isswap != 1)
    {
      LWROC_ERROR_FMT("HLD header swap marker ambiguous (%" PRIu32 ").",
		      ev_head->_decoding);
      return 0;
    }

  length = ev_head->_size;

  if (isswap)
    length = bswap_32(length);

  if (length < sizeof (hld_event))
    {
      LWROC_ERROR_FMT("HLD header size marker broken, smaller than header "
		      "(%" PRIu32 " < %" MYPRIzd ").",
		      length, sizeof (hld_event));
      return 0;
    }

  if (length > iter_data->_size)
    {
      LWROC_ERROR_FMT("HLD event size larger than remaining event payload "
		      "(%" MYPRIzd " available < %" PRIu32 ").",
		      iter_data->_size, length);
      return 0;
    }

  *ev_header   = ev_head;
  *header_swap = isswap;

  event_data->_p    = (const char*) (ev_head + 1);
  event_data->_size = length - sizeof (hld_event);

  iter_data->_p    += length;
  iter_data->_size -= length;

  return 1;
}


int lwroc_hld_get_subevent(lwroc_iterate_event *iter_data,
			   const hld_subevent **subev_header,
			   lwroc_iterate_event *subev_data,
			   int *subev_swap)
{
  const hld_subevent *subev_head;
  uint32_t length;

  int native, isswap;

  if (iter_data->_size < sizeof (hld_subevent))
    {
      LWROC_ERROR_FMT("Incomplete HLD subheader in remaining event payload "
		      "(%" MYPRIzd " bytes available < %" MYPRIzd ").",
		      iter_data->_size, sizeof (hld_subevent));
      return 0;
    }

  subev_head = (const hld_subevent *) iter_data->_p;

  /* Figure out the swapping.
   * The decoding member shall be either 0 for 0xff000000 (native)
   * or for 0x000000ff.  Not both.
   */

  native = !(subev_head->_decoding & 0xff000000);
  isswap = !(subev_head->_decoding & 0x000000ff);

  if (native + isswap != 1)
    {
      LWROC_ERROR_FMT("HLD subevent header swap marker ambiguous "
		      "(%" PRIu32 ").",
		      subev_head->_decoding);
      return 0;
    }

  length = subev_head->_size;

  if (isswap)
    length = bswap_32(length);

  if (length < sizeof (hld_subevent))
    {
      LWROC_ERROR_FMT("HLD subevent header size marker broken, "
		      "smaller than header "
		      "(%" PRIu32 " < %" MYPRIzd ").",
		      length, sizeof (hld_subevent));
      return 0;
    }

  if (length > iter_data->_size)
    {
      LWROC_ERROR_FMT("HLD subevent size larger than remaining event payload "
		      "(%" MYPRIzd " available < %" PRIu32 ").",
		      iter_data->_size, length);
      return 0;
    }

  *subev_header = subev_head;
  *subev_swap   = isswap;

  subev_data->_p    = (const char*) (subev_head + 1);
  subev_data->_size = length - sizeof (hld_subevent);

  iter_data->_p    += length;
  iter_data->_size -= length;

  return 1;
}


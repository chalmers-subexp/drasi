/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2022  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_HLD_UTIL_H__
#define __LWROC_HLD_UTIL_H__

#include "lwroc_iterate_event.h"
#include "hld/lwroc_hld_event.h"

/*****************************************************************************/

int lwroc_hld_get_event(lwroc_iterate_event *iter_data,
			const hld_event **ev_header,
			lwroc_iterate_event *event_data,
			int *header_swap);

int lwroc_hld_get_subevent(lwroc_iterate_event *iter_data,
			   const hld_subevent **subev_header,
			   lwroc_iterate_event *subev_data,
			   int *subev_swap);

/*****************************************************************************/

#endif/*__LWROC_HLD_UTIL_H__*/

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Bastian Loeher  <b.loeher@gsi.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lmd/lwroc_lmd_ev_sev.h"
#include "lwroc_data_pipe.h"
#include "lwroc_message.h"
#include <assert.h>
#include <string.h>
#include "../dtc_arch/acc_def/myinttypes.h"
#include "../dtc_arch/acc_def/myprizd.h"

typedef struct lmd_stream_fill_part_t
{
  char *_start;
  char *_cur;
  char *_end;

} lmd_stream_fill_part;

struct lmd_stream_handle_t
{
  /* The underlying data pipe. */
  lwroc_data_pipe_handle *_data_pipe;

  /* Ongoing event. */
  char *_cur_buf;
  uint32_t _cur_eventno;

  int _has_kind_mask;
  int _ongoing_sev;

  uint32_t _canary;

  lmd_stream_fill_part _part[LWROC_LMD_SEV_NUM_KINDS];
};

lmd_stream_handle *
lwroc_get_lmd_stream(const char *name)
{
  lwroc_data_pipe_handle *base_handle;
  lmd_stream_handle *handle;

  base_handle = lwroc_get_data_pipe(name);

  if (!base_handle)
    LWROC_FATAL_FMT("No handle for data pipe '%s'.", name);

  handle = (lmd_stream_handle *) lwroc_data_pipe_get_extra(base_handle);

  if (handle)
    return handle;

  handle = (lmd_stream_handle *) malloc (sizeof (lmd_stream_handle));

  if (handle == NULL)
    LWROC_FATAL("Memory allocation failure (lmd stream info).");

  handle->_data_pipe = base_handle;

  handle->_cur_buf = NULL;
  handle->_ongoing_sev = 0;
  handle->_canary = 777;

  lwroc_data_pipe_set_extra(base_handle, handle);

  return handle;
}

uint32_t lwroc_lmd_stream_get_max_ev_len(lmd_stream_handle *handle)
{
  return lwroc_data_pipe_get_max_ev_len(handle->_data_pipe);
}

#define ALIGN_LENGTH(x, l) do {			\
    x = ((x) + ((l) - 1)) & ~((l) - 1);		\
  } while (0)

void
lwroc_reserve_event_buffer(lmd_stream_handle *handle,
			   uint32_t eventno,
			   size_t max_size,
			   size_t max_sticky_before_size,
			   size_t max_sticky_after_size)
{
  size_t total;
  char *buf;

  if (handle->_cur_buf != NULL)
    LWROC_BUG("Attempt to reserve new event buffer, "
	      "before finalising previous.");

  ALIGN_LENGTH(max_size, sizeof (uint32_t));
  ALIGN_LENGTH(max_sticky_before_size, sizeof (uint32_t));
  ALIGN_LENGTH(max_sticky_after_size, sizeof  (uint32_t));

  total = 4 * sizeof(lmd_event_10_1_host) + /* headers */
    3 * sizeof (uint32_t) + /* canaries */
    max_size + max_sticky_after_size + 2 * max_sticky_before_size;

  handle->_cur_buf = lwroc_request_event_space(handle->_data_pipe,
					       (uint32_t) total);
  buf = handle->_cur_buf;

#define ASSIGN_EVENT_LOCATION(kind, size) do {				\
    handle->_part[kind]._start = buf;					\
    handle->_part[kind]._cur =						\
      handle->_part[kind]._start + sizeof(lmd_event_10_1_host);		\
    handle->_part[kind]._end = handle->_part[kind]._cur + size;		\
    buf = handle->_part[kind]._end + sizeof (uint32_t); /* canary */	\
  } while (0)

  /* The events are prepared in locations such that we at most have
   * to do one move when finalising.  Note that there is enough
   * space after the normal and after sticky events to move them
   * if the before sticky event is used.  Arrangement:
   *
   * Offset                  Length   Event
   * ---------------------   ------   ------
   * 0                       Normal   Normal
   * Normal                  After    After
   * Normal+After+Before     Before   Before
   * Normal+After+2*Before   -        [end]
   */

  ASSIGN_EVENT_LOCATION(LWROC_LMD_SEV_NORMAL, max_size);
  ASSIGN_EVENT_LOCATION(LWROC_LMD_SEV_STICKY_AFTER, max_sticky_after_size);
  buf += sizeof(lmd_event_10_1_host) + max_sticky_before_size;
  ASSIGN_EVENT_LOCATION(LWROC_LMD_SEV_STICKY_BEFORE, max_sticky_before_size);

  assert(buf == handle->_cur_buf + total);

  handle->_canary =
    handle->_canary * 1103515245 + 12345;

  *((uint32_t *) handle->_part[LWROC_LMD_SEV_NORMAL]._end) =
    handle->_canary;
  *((uint32_t *) handle->_part[LWROC_LMD_SEV_STICKY_AFTER]._end) =
    handle->_canary;
  *((uint32_t *) handle->_part[LWROC_LMD_SEV_STICKY_BEFORE]._end) =
    handle->_canary;

  handle->_has_kind_mask = 0;
  handle->_ongoing_sev = 0;
  handle->_cur_eventno = eventno;
}

void
lwroc_new_event(lmd_stream_handle *handle,
		lmd_event_10_1_host **a_ev, int trig)
{
  lmd_event_10_1_host *ev;

  if (handle->_cur_buf == NULL)
    LWROC_BUG("Attempt to start new event, without reserving space.");
  if (handle->_has_kind_mask & (1 << LWROC_LMD_SEV_NORMAL))
    LWROC_BUG("Attempt to start new event twice.");

  ev = (lmd_event_10_1_host *) handle->_part[LWROC_LMD_SEV_NORMAL]._start;

  ev->_header.i_type    = LMD_EVENT_10_1_TYPE;
  ev->_header.i_subtype = LMD_EVENT_10_1_SUBTYPE;
  ev->_info.i_dummy   = 0;
  ev->_info.i_trigger = (uint16_t) trig;
  ev->_info.l_count   = handle->_cur_eventno;

  if (a_ev != NULL)
    *a_ev = ev;

  handle->_has_kind_mask |= (1 << LWROC_LMD_SEV_NORMAL);
}

void
lwroc_finalise_event_buffer(lmd_stream_handle *handle)
{
  size_t used_size[LWROC_LMD_SEV_NUM_KINDS];
  char *end[LWROC_LMD_SEV_NUM_KINDS];
  size_t total_used;
  int kind;
  int no_hysteresis = 0;

  /* The size will be checked lwroc_used_event_space below.
   * So will canary value after the event.
   */

  if (handle->_cur_buf == NULL)
    LWROC_BUG("Attempt to finalise event with none started.");
  if (handle->_ongoing_sev)
    LWROC_BUG("Attempt to finalise event with ongoing subevent.");

#define CHECK_CANARY(kind, name) do {		    \
    if (*((uint32_t *) handle->_part[kind]._end) != handle->_canary)	\
      LWROC_BUG_FMT("%s buffer overflow!  "				\
		    "Unexpected canary (0x%" PRIx32 " != 0x%" PRIx32 ").", \
		    #name, *((uint32_t *) handle->_part[kind]._end),	\
		    handle->_canary);					\
  } while (0)

  CHECK_CANARY(LWROC_LMD_SEV_NORMAL,        "Event");
  CHECK_CANARY(LWROC_LMD_SEV_STICKY_AFTER,  "Sticky after");
  CHECK_CANARY(LWROC_LMD_SEV_STICKY_BEFORE, "Sticky before");

  /* First make sure that the event headers to be used are filled out. */

  /* printf ("%08x\n", handle->_has_kind_mask); */

  for (kind = 0; kind < LWROC_LMD_SEV_NUM_KINDS; kind++)
    {
      lmd_event_10_1_host *ev;
      lmd_stream_fill_part *part;
      size_t avail;

      if (!(handle->_has_kind_mask & (1 << kind)))
	{
	  used_size[kind] = 0;
	  continue;
	}

      part = &handle->_part[kind];

      used_size[kind] = (size_t) (part->_cur - part->_start);
      avail =           (size_t) (part->_end - part->_start);

      if (used_size[kind] > avail)
	LWROC_FATAL_FMT("Buffer/event overflow - finalising too large event "
			"of kind (%d) "
			"(%" MYPRIzd" bytes > %" MYPRIzd").",
			kind,
			used_size[kind], avail);

      ev = (lmd_event_10_1_host *) part->_start;

      ev->_header.l_dlen = (uint32_t)
	DLEN_FROM_EVENT_DATA_LENGTH(used_size[kind] -
				    sizeof (lmd_event_header_host));

      if (kind == LWROC_LMD_SEV_NORMAL)
	{
	  /* Check that the event header (type/subtype) has not been changed.
	   */
	  if (ev->_header.i_type    != LMD_EVENT_10_1_TYPE ||
	      ev->_header.i_subtype != LMD_EVENT_10_1_SUBTYPE)
	    LWROC_FATAL_FMT("Event header corruption, "
			    "finalising with changed type/subtype "
			    "(%d=%04x/%d/%04x) != (%d,%d)",
			    ev->_header.i_type, ev->_header.i_type,
			    ev->_header.i_subtype, ev->_header.i_subtype,
			    LMD_EVENT_10_1_TYPE,
			    LMD_EVENT_10_1_SUBTYPE);

	  if (ev->_info.i_trigger >= 12)
	    no_hysteresis = 1;
	  continue;
	}

      /* For the sticky events, we have to fill the rest of the headers too. */

      ev->_header.i_type    = LMD_EVENT_STICKY_TYPE;
      ev->_header.i_subtype = LMD_EVENT_STICKY_SUBTYPE;
      ev->_info.i_dummy   = 0;
      ev->_info.i_trigger = (kind == LWROC_LMD_SEV_STICKY_AFTER ?
			     LMD_EVENT_STICKY_TRIG_AFTER :
			     LMD_EVENT_STICKY_TRIG_BEFORE);
      ev->_info.l_count   = handle->_cur_eventno;

      /* TODO: ??? Should we?  They will be blocked in buffer anyhow? */
      /* no_hysteresis = 1;*/ /* Sticky events are propagated immediately. */
    }

  /* Then we may have to move the events around.
   * The normal event only moves if there is a sticky event to be
   * inserted before it.  The sticky event after likely moves to an
   * earlier position (if the normal event is small), but may also
   * have to move to a later position.  Therefore:
   *
   * - First move the sticky event after.
   * - Then move the normal event.
   * - Finally copy the sticky event before, if needed.
   */

  end[LWROC_LMD_SEV_STICKY_BEFORE] =
    handle->_cur_buf + used_size[LWROC_LMD_SEV_STICKY_BEFORE];
  end[LWROC_LMD_SEV_NORMAL] =
    end[LWROC_LMD_SEV_STICKY_BEFORE] + used_size[LWROC_LMD_SEV_NORMAL];
  end[LWROC_LMD_SEV_STICKY_AFTER] =
    end[LWROC_LMD_SEV_NORMAL] + used_size[LWROC_LMD_SEV_STICKY_AFTER];

  /* Move the 'after' sticky event. */
  if (used_size[LWROC_LMD_SEV_STICKY_AFTER])
    memmove(end[LWROC_LMD_SEV_NORMAL],
	    handle->_part[LWROC_LMD_SEV_STICKY_AFTER]._start,
	    used_size[LWROC_LMD_SEV_STICKY_AFTER]);
  /* The 'after' event is out of the way, the 'normal' event can be
   * moved to make way for the 'before' event.
   */
  if (used_size[LWROC_LMD_SEV_STICKY_BEFORE])
    {
      memmove(end[LWROC_LMD_SEV_STICKY_BEFORE],
	      handle->_part[LWROC_LMD_SEV_NORMAL]._start,
	      used_size[LWROC_LMD_SEV_NORMAL]);
      /* Copy the 'before' event. */
      /* This is never overlapping with its destination. */
      memcpy(handle->_cur_buf,
	     handle->_part[LWROC_LMD_SEV_STICKY_BEFORE]._start,
	     used_size[LWROC_LMD_SEV_STICKY_BEFORE]);
    }

  /* And commit them. */

  total_used = (size_t) (end[LWROC_LMD_SEV_STICKY_AFTER] - handle->_cur_buf);

  assert (total_used ==
	  used_size[LWROC_LMD_SEV_STICKY_BEFORE] +
	  used_size[LWROC_LMD_SEV_NORMAL] +
	  used_size[LWROC_LMD_SEV_STICKY_AFTER]);

  lwroc_used_event_space(handle->_data_pipe, (uint32_t) total_used,
			 no_hysteresis);

  /* We are done. */

  handle->_cur_buf = NULL;
}

char *
lwroc_new_subevent(lmd_stream_handle *handle,
		   int kind,
		   lmd_subevent_10_1_host **a_sev,
		   struct lwroc_lmd_subevent_info *info/*,
		   size_t max_size*/)
{
  lmd_stream_fill_part *part;
  lmd_subevent_10_1_host *sev;
  char *ptr;

  if (kind < 0 || kind >= LWROC_LMD_SEV_NUM_KINDS)
    LWROC_BUG_FMT("Attempt to start subevent of unknown kind (%d).", kind);

  if (handle->_cur_buf == NULL)
      LWROC_BUG("Attempt to start subevent without event space reservation.  "
		"Use lwroc_reserve_event_buffer first.");

  if (kind == LWROC_LMD_SEV_NORMAL &&
      !(handle->_has_kind_mask & (1 << kind)))
    LWROC_BUG("Attempt to start subevent without event.");

  if (handle->_ongoing_sev & (1 << kind))
    LWROC_BUG("Attempt to start new subevent without finalising previous.");

  part = &handle->_part[kind];

  /* printf ("kind: %d  %zd\n", kind, part->_end - part->_cur); */

  if (part->_end - part->_cur <
      (ssize_t) (sizeof (lmd_subevent_10_1_host)/* + max_size*/))
    LWROC_BUG("Attempt to start new subevent in buffer without enough space.");

  sev = (lmd_subevent_10_1_host *) part->_cur;
  sev->_header.l_dlen    = 0; /* overwritten below in finalise */
  sev->_header.i_type    = info->type;
  sev->_header.i_subtype = info->subtype;
  sev->i_procid          = info->procid;
  sev->h_subcrate        = info->subcrate;
  sev->h_control         = info->control;

  handle->_has_kind_mask |= (1 << kind);
  handle->_ongoing_sev |= (1 << kind);

  if (a_sev != NULL)
    *a_sev = sev;

  ptr = (char *)(sev + 1);

  return ptr;
}

/* Not a technical limit; used to catch user code bugs. */
#define LWROC_INSANE_SUBEVENT_SIZE  0x10000000  /* 256 MiB */
/* Events are checked vs available space, so likely fails earlier. */

void
lwroc_finalise_subevent(lmd_stream_handle *handle,
			int kind,
			void *end)
{
  lmd_stream_fill_part *part;
  lmd_subevent_10_1_host *sev;
  size_t used_size, avail;

  if (kind < 0 || kind >= LWROC_LMD_SEV_NUM_KINDS)
    LWROC_BUG_FMT("Attempt to finalise subevent of unknown kind (%d).", kind);

  if (!(handle->_ongoing_sev & (1 << kind)))
    LWROC_BUG_FMT("Attempt to finalise subevent of kind (%d) "
		  "with none started.",
		  kind);

  part = &handle->_part[kind];

  used_size = (size_t) ((char *) end - part->_cur);
  avail     = (size_t) (part->_end   - part->_cur);

  ALIGN_LENGTH(used_size, sizeof (uint32_t));

  sev = (lmd_subevent_10_1_host *) part->_cur;

  if (used_size > avail)
    LWROC_FATAL_FMT("Buffer/event overflow - finalising too large subevent "
		    "of kind (%d) "
		    "(%" MYPRIzd" bytes > %" MYPRIzd") "
		    "(type=%d,subtype=%d,procid=%d,crate=%d,ctrl=%d).",
		    kind, used_size, avail,
		    sev->_header.i_type,
		    sev->_header.i_subtype,
		    sev->i_procid,
		    sev->h_subcrate,
		    sev->h_control);
  if (used_size > LWROC_INSANE_SUBEVENT_SIZE)
    LWROC_WARNING_FMT("Finalising insanely large subevent of kind (%d) "
		      "(%" MYPRIzd" bytes).",
		      kind, used_size);

  if (sev->_header.l_dlen == 0)
    {
      /* Only set the subevent length if not set by user routine.
       * For compatibility with MBS call to f_user_readout.
       */
      sev->_header.l_dlen = (uint32_t)
	SUBEVENT_DLEN_FROM_DATA_LENGTH(used_size -
				       sizeof(lmd_subevent_10_1_host));
    }
  part->_cur += used_size;

  handle->_ongoing_sev &= ~(1 << kind);
}

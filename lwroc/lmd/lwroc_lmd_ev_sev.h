/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Bastian Loeher  <b.loeher@gsi.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lmd/lwroc_lmd_event.h"

/* This handle is opaque, as the contents shall not be (ab)used. */
typedef struct lmd_stream_handle_t lmd_stream_handle;

/* This function should not be called often, as it does a
 * (potentially) expensive lookup. */
lmd_stream_handle *lwroc_get_lmd_stream(const char *name);

uint32_t lwroc_lmd_stream_get_max_ev_len(lmd_stream_handle *handle);

#define LWROC_LMD_SEV_NORMAL          0
#define LWROC_LMD_SEV_STICKY_BEFORE   1
#define LWROC_LMD_SEV_STICKY_AFTER    2
#define LWROC_LMD_SEV_NUM_KINDS       3

/* For each event produced, the sequence is:
 *
 * - lwroc_reserve_event_buffer()
 *
 *   Reserves space for the event, and any sticky subevents.  Note
 *   that sticky subevents before the event will lead to copying of
 *   the data at finalisation.
 *
 * - lwroc_new_event()
 *
 *   To set the trigger number etc.  If this function is not called,
 *   there will be no event.  (But sticky ones, iff sticky subevents
 *   are produced.)
 *
 * - lwroc_new_subevent(kind=LWROC_LMD_SEV_NORMAL)
 * - lwroc_finalise_subevent(kind=LWROC_LMD_SEV_NORMAL)
 *
 *   The above are called in pairs for each subevent to produce.
 *
 * - lwroc_finalise_event_buffer()
 *
 *   Calculates the final sizes, and also moves any sticky event to
 *   its proper location.  This also produces the sticky event
 *   headers.
 *
 * Sticky subevent injection:
 *
 * Anytime between the call to lwroc_reserve_event_buffer() and
 * lwroc_finalise_event_buffer(), call lwroc_new_subevent()/
 * lwroc_finalise_subevent() with the wanted kind.  Subevents of
 * different kinds may be active at the same time, but must be
 * finalised within each kind before a new is started.
 *
 * After each item is finalised, it is checked that too much data was
 * not written (i.e. that the end pointer is within bounds).  It is
 * intentional that the end pointers are not given to the user.  The
 * user really should reserve enough space in the call to
 * lwroc_reserve_event_buffer().  With margin... - you do not really
 * want to cut an event?
 */

void
lwroc_reserve_event_buffer(lmd_stream_handle *stream,
			   uint32_t eventno,
			   size_t max_size,
			   size_t max_sticky_before_size,
			   size_t max_sticky_after_size);

void lwroc_finalise_event_buffer(lmd_stream_handle *stream);

void lwroc_new_event(lmd_stream_handle *stream,
		     lmd_event_10_1_host **_ev, int trig);

char *lwroc_new_subevent(lmd_stream_handle *stream,
			 int kind,
			 lmd_subevent_10_1_host **_sev,
			 struct lwroc_lmd_subevent_info *info/*,
			 size_t max_size*/);

void lwroc_finalise_subevent(lmd_stream_handle *stream,
			     int kind,
			     void *end);

/* This file is part of UCESB - a tool for data unpacking and processing.
 *
 * Copyright (C) 2016  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LMD_EVENT_H__
#define __LMD_EVENT_H__

#include "lmd_struct/lmd_file_header.h"
#include "lmd_struct/lmd_buf_header.h"

#include "lmd_struct/lmd_event_10_1.h"
#include "lmd_struct/lmd_subevent_10_1.h"

#include <stdlib.h>

/* Convert from nutty specification to full buffer size in bytes
 * description.
 */

/* Calculate buffer size from dlen header member */
#define BUFFER_SIZE_FROM_DLEN(dlen)          ((dlen) * 2 + sizeof(s_bufhe_host))
/* Calculate number of bytes used of data block in buffer from iused */
#define BUFFER_USED_FROM_IUSED(iused)        ((iused) * 2)
/* Calculate data length of event from dlen header member */
#define EVENT_DATA_LENGTH_FROM_DLEN(dlen)    ((dlen) * 2)
/* Calculate data length of event from dlen header member */
#define SUBEVENT_DATA_LENGTH_FROM_DLEN(dlen) (((dlen) - 2) * 2)

/* Calculate dlen header member from buffer size */
#define DLEN_FROM_BUFFER_SIZE(size)          ((size - sizeof(s_bufhe_host)) / 2)
/* Calculate iused from number of bytes used of data block in buffer */
#define IUSED_FROM_BUFFER_USED(used)         ((used) / 2)
/* Calculate dlen header member from data length of event */
#define DLEN_FROM_EVENT_DATA_LENGTH(length)  ((length) / 2)
/* Calculate data length of event from dlen header member */
#define SUBEVENT_DLEN_FROM_DATA_LENGTH(length) (((length) / 2) + 2)

/* /////////////////////////////////////////////////////// */

/* This is for internal use in LWROC only.  And should be phased out
 * with the introduction of an controlling pipe of encapsulation
 * information.
 */

#define LWROC_LMD_IDENT_TYPE         0x4944u
#define LWROC_LMD_IDENT_SUBTYPE      0x4e54u

#endif/*__LMD_EVENT_H__*/

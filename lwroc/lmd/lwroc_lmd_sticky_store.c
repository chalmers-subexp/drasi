/* This file is part of UCESB - a tool for data unpacking and processing.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_lmd_sticky_store.h"
#include "lwroc_lmd_event.h"
#include "lwroc_lmd_util.h"
#include "lwroc_message.h"

#include <string.h>
#include <assert.h>

#include "../dtc_arch/acc_def/myprizd.h"

/* In order to deliver a full set of sticky subevents on request, we
 * need to keep track of them.  Since however the usual delivery is
 * incremental, as they occur, this additional book-keeping is just
 * done to satisfy very spurious requests.  It should therefore be of
 * low cost (and preferably take the hit when the request to re-emit
 * comes, and not upon arrival).
 *
 * We store the sticky events as they arrive in a plain data array.
 * New sticky events are added at the end.  In order to keep track of
 * the contents of the array, we keep an additional array with just
 * the meta-data, i.e. identifiers, lengths and offsets of the events
 * and subevents.  Thus it is easy to in that metadata remove (by
 * marking, not moving) entries as they are superseded.  Further, to
 * easily find entries in the array, we also keep a hash-table of all
 * identifiers, with offsets to the last (i.e. active) entry.
 *
 * Thus insertation is cheap, the main cost is a single copy of the
 * data into the big array.  The meta-data update is constant time.
 *
 * When we need to request the contents, the cost is also linear in
 * size, as it only has to fully traverse the meta-data array and copy
 * out any active items.
 *
 * To avoid this store to eat infinite amounts of memory, we keep
 * track of the amount of discarded data.  When a reallocation is
 * necessary, it is first checked if there is more than half empty
 * (superseded) data.  If that is the case, we compact the array
 * instead of allocating more memory.  This will of course take time
 * proportional to the array size, but as it at most happens when we
 * reach the end of the array, its occurrence goes inversely
 * proportional to the array size, and thus overall scales with the
 * copy cost.
 *
 * ---
 *
 * Handling event input / sorting: we are not needed to reproduce
 * events, as that happens in the output.  But we are needed to ensure
 * that multiple sticky events are not injected by multiple sources.
 * Or that sticky events that are supplied from multiple sources are
 * bit-wise identical.
 *
 * If we are merging data streams from multiple sources that actually
 * have the same origin (each get a fraction of the normal events),
 * they will however all contain sticky events (duplicates).  To
 * survive the injection of sticky events from each of them, when they
 * necessarily may disagree, the enforcement on agreement can only be
 * done at points when a normal event passes.  At that point, they
 * must agree.
 *
 * There is a tricky point: when one source is not delivering events
 * over some stretch of time, and other do, then the sticky state of
 * the dormant source would seem to be out of sync.  We could require
 * that such stickies however shall be changed/revoked before such a
 * source emits further real events.
 *
 * When being part of such an input chain, it then also does not make
 * sense to pass sticky events on as soon as they appear.  Rather, they
 * should be queued up here, and ejected when they are required.  I.e.
 * before a source is allowed to pass an event on.
 *
 * ---
 *
 * To handle all this, each sticky subevent is associated with a
 * source number.  When multiple sources provide the same sticky
 * event, they point to the same source.  (Should we use a bitmask?
 * Yes!  :-) ) When multiple, we just keep a prev-pointer in each
 * subevent to the next sticky of the same kind that is active (for
 * other sources).
 *
 *
 */

/* urgent TODO: error handling - in UCESB error throws an exception.
 * We must here return error codes, as otherwise we may segfault
 * etc...
 */

#define DEBUG_LMD_STICKY_STORE        1
#define DEBUG_LMD_STICKY_STORE_PRINT  0

#if DEBUG_LMD_STICKY_STORE
void lwroc_lmd_sticky_store_verify_meta(lwroc_lmd_sticky_store *store);
#endif

lwroc_lmd_sticky_store *lwroc_lmd_sticky_store_init(void)
{
  lwroc_lmd_sticky_store *store;

  store = (lwroc_lmd_sticky_store *) malloc (sizeof (lwroc_lmd_sticky_store));

  if (store == NULL)
    LWROC_FATAL("Memory allocation failure (lmd sticky store info).");

  store->_data = NULL;
  store->_data_end = store->_data_alloc = 0; /* bytes */
  store->_data_revoked = 0;

  store->_meta = NULL;
  store->_meta_end = store->_meta_alloc = 0; /* bytes */
  store->_meta_revoked = 0;

  store->_hash = NULL;
  store->_hash_used = store->_hash_size = 0; /* items */

  return store;
}

void lwroc_lmd_sticky_store_deinit(lwroc_lmd_sticky_store *store)
{
  free(store->_data);
  free(store->_meta);
  free(store->_hash);

  free(store);
}

void lwroc_lmd_sticky_store_clear(lwroc_lmd_sticky_store *store)
{
  store->_data_end = 0;
  store->_data_revoked = 0;

  store->_meta_end = 0;
  store->_meta_revoked = 0;

  /* This will simply clear the hash. */
  lwroc_lmd_sticky_store_populate_hash(store);
}

uint32_t sticky_subevent_hash(const lmd_subevent_10_1_host *subevent_header)
{
  const uint32_t *p = (const uint32_t *) subevent_header;

  uint32_t d1 = p[1];
  uint32_t d2 = p[2];

  uint32_t hash = d1;

  hash ^= (hash << 6);
  hash ^= (hash << 21);
  hash ^= (hash << 7);

  hash ^= d2;

  hash ^= (hash << 6);
  hash ^= (hash << 21);
  hash ^= (hash << 7);

  return hash;
}

int sticky_subevent_same_id(const lmd_subevent_10_1_host *header_a,
			    const lmd_subevent_10_1_host *header_b)
{
  const uint32_t *p_a = (const uint32_t *) header_a;
  const uint32_t *p_b = (const uint32_t *) header_b;

  if (p_a[1] == p_b[1] &&
      p_a[2] == p_b[2])
    return 1;
  return 0;
}

void lwroc_lmd_sticky_store_insert(lwroc_lmd_sticky_store *store,
				   char *ptr, size_t len,
				   /*
				     const lmd_event_out *event,
				   */
				   int discard_revoke,
				   int as_active)
{
  size_t ev_data_len;
  size_t ev_len;
  size_t ev_data_off;
  char *raw_ev;
  lmd_event_10_1_host *ev_header;
  size_t ev_size;
  size_t ev_raw_data_len;
  uint32_t num_sev;
  size_t sev_data_off;
  size_t meta_add;
  size_t hash_add;
  int recalc_hash;
  size_t dest_ev_offset;
  lwroc_lmd_sticky_meta_event *ev;

#if DEBUG_LMD_STICKY_STORE
  lwroc_lmd_sticky_store_verify_meta(store);
#endif

#if DEBUG_LMD_STICKY_STORE
# if DEBUG_LMD_STICKY_STORE_PRINT
  fprintf (stderr, "insert %d\n", ((uint32_t *) ptr)[3]);
# endif
#endif

  /* How much additional does this event contribute (worst case,
   * i.e. without considering any stuff that it revokes)
   *
   * The event that we get to handle comes via lmd_event_out, which
   * means it is broken down into chunks, and subevents headers etc
   * not are clearly marked any long.  (It may even be malformed, if
   * the user has copied badly...)
   *
   * The easiest approach is that we copy the entire data (which we
   * anyhow would do) into the data buffer, and then from there sort
   * out the subevents again.
   */

#if UCESB
  ev_data_len = event->get_length();
  ev_len = ev_data_len + sizeof(lmd_event_header_host);
#else
  ev_len = len;
  ev_data_len = ev_len - sizeof(lmd_event_header_host);
#endif

  if (store->_data_end + ev_len > store->_data_alloc)
    {
      size_t sz;

      /* Is compactification due? */

      if (store->_data_revoked > store->_data_end / 2)
	lwroc_lmd_sticky_store_compact_data(store);

      /* Problem may still not be solved... */

      sz = store->_data_alloc;

      if (sz < 1024)
	sz = 1024;

      while (store->_data_end + ev_len > sz)
	sz *= 2;

      if (sz > store->_data_alloc)
	{
	  store->_data = (char *) realloc (store->_data, sz);
	  if (!store->_data)
	    LWROC_ERROR("Memory allocation failure.");

	  store->_data_alloc = sz;
	}
    }

  /* Now that we have the needed data array secured, copy the event. */

  ev_data_off = store->_data_end;
  store->_data_end += ev_len;
  raw_ev = store->_data + ev_data_off;

#if UCESB
  event->write(raw_ev);
#else
  memcpy(raw_ev, ptr, len);
#endif

#if DEBUG_LMD_STICKY_STORE
# if DEBUG_LMD_STICKY_STORE_PRINT
  for (size_t i = 0; i < ev_len; i += sizeof (uint32_t))
    fprintf (stderr, "%3zx: %08x\n", i, *((uint32_t *) (raw_ev+i)));
# endif
#endif

  /* And figure out how many subevents it contains */

  /* Some minor checks of the event header.  That the size is correct,
   * and that it has the right type/subtype.
   */

  ev_header = (lmd_event_10_1_host *) raw_ev;

  ev_size =
    EVENT_DATA_LENGTH_FROM_DLEN((size_t) ev_header->_header.l_dlen);

  if (ev_size != ev_data_len)
    LWROC_ERROR_FMT("Trying to store sticky event with wrong data size "
	  "(in header data %" MYPRIzd " != total data %" MYPRIzd ").",
	  ev_size, ev_len);

  if (ev_size + sizeof(lmd_event_header_host) < sizeof (lmd_event_10_1_host))
    LWROC_ERROR_FMT("Trying to store sticky event with small size "
	  "(%" MYPRIzd " < 1 header).",
	  ev_size + sizeof(lmd_event_header_host));

  if (!(ev_header->_header.i_type    == LMD_EVENT_STICKY_TYPE &&
	ev_header->_header.i_subtype == LMD_EVENT_STICKY_SUBTYPE))
    LWROC_ERROR_FMT("Trying to store sticky event with wrong type "
	  "(%d=0x%04x/%d=0x%04x)",
	  (uint16_t) ev_header->_header.i_type,
	  (uint16_t) ev_header->_header.i_type,
	  (uint16_t) ev_header->_header.i_subtype,
	  (uint16_t) ev_header->_header.i_subtype);

  ev_raw_data_len = sizeof (lmd_event_10_1_host);

  /* Now count the subevents. */

  num_sev = 0;

  for (sev_data_off = ev_raw_data_len; sev_data_off != ev_len; )
    {
      lmd_subevent_10_1_host *sev_header;
      size_t sev_payload_size;
      size_t sev_next_off;

      if (sev_data_off + sizeof (lmd_subevent_10_1_host) > ev_len)
	LWROC_ERROR_FMT("Trying to store sticky event with "
	      "subevent header overflowing event (%" MYPRIzd " bytes).",
	      sev_data_off + sizeof (lmd_subevent_10_1_host) -
	      ev_len);

      sev_header = (lmd_subevent_10_1_host *) (raw_ev + sev_data_off);

      if (sev_header->_header.l_dlen == LMD_SUBEVENT_STICKY_DLEN_REVOKE)
	sev_payload_size = 0;
      else
	sev_payload_size =
	  SUBEVENT_DATA_LENGTH_FROM_DLEN((size_t) sev_header->_header.l_dlen);
      sev_next_off =
	sev_data_off + (sizeof (lmd_subevent_10_1_host) +
			sev_payload_size);

      if (sev_next_off > ev_len)
	{
	LWROC_ERROR_FMT("Trying to store sticky event with "
	      "subevent payload overflowing event (%" MYPRIzd " bytes).",
	      sev_next_off - ev_len);
	}

      if (sev_header->_header.i_type    == LMD_SUBEVENT_STICKY_TSTAMP_TYPE &&
	  sev_header->_header.i_subtype == LMD_SUBEVENT_STICKY_TSTAMP_SUBTYPE)
	{
	  if (num_sev)
	    LWROC_ERROR_FMT("Trying to store sticky event with "
		  "tstamp subevent (%d/%d) as non-first subevent.",
		  sev_header->_header.i_type,
		  sev_header->_header.i_subtype);
	  else
	    {
	      /* We (locally) account this together with the event
	       * header, such that this subevent is kept whatever happens
	       * to the other subevents.
	       */
	      ev_raw_data_len +=
		(sizeof (lmd_subevent_10_1_host) + sev_payload_size);
	    }
	}
      else
	num_sev++;

      sev_data_off = sev_next_off;
    }

  /* Reallocations? */

  meta_add =
    sizeof (lwroc_lmd_sticky_meta_event) +
    num_sev * sizeof (lwroc_lmd_sticky_meta_subevent);
  hash_add = num_sev;

  recalc_hash = 0;

  if (store->_meta_end + meta_add > store->_meta_alloc)
    {
      size_t sz;

      /* Is compactification due? */

      if (store->_meta_revoked > store->_meta_end / 2)
	{
	  lwroc_lmd_sticky_store_compact_meta(store);
	  /* Since the offsets change, we just recalculate the hash table. */
	  recalc_hash = 1;
	}

      /* Problem may still not be solved... */

      sz = store->_meta_alloc;

      if (sz < 256)
	sz = 256;

      while (store->_meta_end + meta_add > sz)
	sz *= 2;

      if (sz > store->_meta_alloc || sz < store->_meta_alloc / 2)
	{
	  store->_meta = (char *) realloc (store->_meta, sz);
	  if (!store->_meta)
	    LWROC_ERROR("Memory allocation failure.");

	  store->_meta_alloc = sz;
	}
    }

  if (store->_hash_used + hash_add > store->_hash_size / 2)
    {
      size_t sz = store->_hash_size;

      if (sz < 16)
	sz = 16;

      while (store->_hash_used + hash_add > sz / 2)
	sz *= 2;

      if (sz > store->_hash_size ||
	  sz < store->_hash_size / 4) /* If needs have decreased a lot... */
	{
	  store->_hash = (lwroc_lmd_sticky_hash_subevent *)
	    realloc (store->_hash, sz * sizeof (store->_hash[0]));
	  if (!store->_hash)
	    LWROC_ERROR("Memory allocation failure.");

	  store->_hash_size = sz;

	  /* We simply repopulate the hash table */
	  recalc_hash = 1;
	}
    }

  if (recalc_hash)
    lwroc_lmd_sticky_store_populate_hash(store);

  /* Ok, so now we also have space to store the meta-data about this event...*/

  dest_ev_offset = store->_meta_end;

  ev =
    (lwroc_lmd_sticky_meta_event *) (store->_meta + store->_meta_end);
  store->_meta_end += sizeof (lwroc_lmd_sticky_meta_event);

  ev->_data_offset = ev_data_off;
  ev->_data_length = ev_raw_data_len;
  ev->_num_sub  = num_sev;
  ev->_live_sub = num_sev;

  for (sev_data_off = ev_raw_data_len; sev_data_off != ev_len; )
    {
      lmd_subevent_10_1_host *sev_header =
	(lmd_subevent_10_1_host *) (raw_ev + sev_data_off);
      size_t sev_payload_size;
      size_t sev_next_off;
      size_t dest_sev_offset;
      lwroc_lmd_sticky_meta_subevent *sev;
      uint32_t hash;
      size_t entry;

      if (sev_header->_header.l_dlen == LMD_SUBEVENT_STICKY_DLEN_REVOKE)
	sev_payload_size = 0;
      else
	sev_payload_size =
	  SUBEVENT_DATA_LENGTH_FROM_DLEN((size_t) sev_header->_header.l_dlen);
      sev_next_off =
	sev_data_off + (sizeof (lmd_subevent_10_1_host) +
			sev_payload_size);

      /* Insert the subevent metadata */

      dest_sev_offset = store->_meta_end;

      sev = (lwroc_lmd_sticky_meta_subevent *) (store->_meta +
						store->_meta_end);

      sev->_ev_offset = dest_ev_offset;
      sev->_data_offset = (ssize_t) (ev_data_off + sev_data_off);
      sev->_data_length = (sizeof (lmd_subevent_10_1_host) +
			   sev_payload_size);
      sev->_flags = 0;

      if (as_active)
	sev->_flags |= LWROC_LMD_STICKY_FLAG_ACTIVE;

      /* And into the hash table.  Perhaps it is known before?  Note: We
       * cannot remove revoke events.  Since our purpose is to be used
       * for replays, also the revoke events are important!
       */

      /* For file replay, we do not want the revoke events however. */

      hash = sticky_subevent_hash(sev_header);

      entry = hash & (store->_hash_size - 1);

      for ( ; store->_hash[entry]._sub_offset;
	    entry = (entry + 1) & (store->_hash_size - 1))
	{
	  lwroc_lmd_sticky_meta_subevent *check_sev;
	  char *check_raw;
	  lmd_subevent_10_1_host *check_sev_header;

	  if (store->_hash[entry]._sub_offset == -1)
	    continue; /* discarded */

	  /* Investigate if this is the same id as ours */

	  check_sev =
	    (lwroc_lmd_sticky_meta_subevent *)
	    (store->_meta + store->_hash[entry]._sub_offset);

	  if (check_sev->_data_offset == -1)
	    continue; /* discarded */

	  check_raw = store->_data + check_sev->_data_offset;

	  check_sev_header = (lmd_subevent_10_1_host *) check_raw;

	  if (sticky_subevent_same_id(sev_header,
				      check_sev_header))
	    {
	      lwroc_lmd_sticky_meta_event *check_ev;

	      /* Remove the subevent from the event it belongs to.
	       * Actual removal happens later.  Just decrease the
	       * number of live subevents.
	       */

	      check_sev->_data_offset = -1;

	      check_ev =
		(lwroc_lmd_sticky_meta_event *)
		(store->_meta + check_sev->_ev_offset);

	      /* When data (or meta) gets compacted, this subevent
	       * will not be retained
	       */
	      store->_data_revoked += check_sev->_data_length;
	      store->_meta_revoked += sizeof (lwroc_lmd_sticky_meta_subevent);
	      check_ev->_live_sub--;
	      if (check_ev->_live_sub == 0)
		{
		  /* Upon compacting, this event will not be retained. */
		  store->_data_revoked += check_ev->_data_length;
		  store->_meta_revoked += sizeof (lwroc_lmd_sticky_meta_event);
		}
	      /* May be moved into earlier discarded slot, so kill this one. */
	      store->_hash[entry]._sub_offset = -1;
	      goto hash_found;
	    }
	}
      /* Note that we do not reduce for discarded subevents, such that
       * if there is a lot of rotation, we still get rehashing done.
       */
      store->_hash_used++;
    hash_found:
      if (!discard_revoke ||
	  sev_header->_header.l_dlen != LMD_SUBEVENT_STICKY_DLEN_REVOKE)
	{
	  /* We actually inserted this subevent */
	  store->_meta_end += sizeof (lwroc_lmd_sticky_meta_subevent);
	  /* Since we may have discarded slots, do the search again */
	  entry = hash & (store->_hash_size - 1);
	  while (store->_hash[entry]._sub_offset &&
		 store->_hash[entry]._sub_offset != -1)
	    {
	      entry = (entry + 1) & (store->_hash_size - 1);
	    }
	  /* We have found either an empty slot, or we will overwrite
	   * the previous.
	   */
	  store->_hash[entry]._sub_offset = (ssize_t) dest_sev_offset;
#if DEBUG_LMD_STICKY_STORE
# if DEBUG_LMD_STICKY_STORE_PRINT
	  fprintf(stderr, "Inserted sev (ctrl:%d) "
		  "at hash %" MYPRIzd ".\n",
		  sev_header->h_control,
		  entry);
# endif
#endif
	}
      else
	{
#if DEBUG_LMD_STICKY_STORE
# if DEBUG_LMD_STICKY_STORE_PRINT || 0
	  if (!discard_revoke)
	    fprintf(stderr, "NOT inserting sev (ctrl:%d).\n",
		    sev_header->h_control);
# endif
#endif
	  ev->_num_sub--;
	  ev->_live_sub--;
	}
      sev_data_off = sev_next_off;
    }

  if (ev->_num_sub == 0)
    {
      /* We did not insert any subevents at all, completely forget
       * this one.
       */
      store->_meta_end = dest_ev_offset;
      store->_data_revoked += ev->_data_length;
    }

#if DEBUG_LMD_STICKY_STORE
  lwroc_lmd_sticky_store_verify_meta(store);
#endif
}

void lwroc_lmd_sticky_store_compact_data(lwroc_lmd_sticky_store *store)
{
  size_t dest_offset = 0;
  size_t iter_ev_offset;

#if DEBUG_LMD_STICKY_STORE
  lwroc_lmd_sticky_store_verify_meta(store);
# if DEBUG_LMD_STICKY_STORE_PRINT
  fprintf(stderr,"Compact data!\n");
# endif
#endif
  /* Go through the meta-data, and move all event data up ahead. */

  for (iter_ev_offset = 0; iter_ev_offset < store->_meta_end; )
    {
      lwroc_lmd_sticky_meta_event *ev;
      lwroc_lmd_sticky_meta_subevent *sev;
      uint32_t i;

      ev = (lwroc_lmd_sticky_meta_event *) (store->_meta + iter_ev_offset);
      iter_ev_offset +=
	sizeof (lwroc_lmd_sticky_meta_event) +
	ev->_num_sub * sizeof (lwroc_lmd_sticky_meta_subevent);

      if (!ev->_live_sub)
	{
	  /* There are no live subevents left in this event.
	   * Forget about its data.
	   */
	  continue;
	}

      {
	char *src_ptr = store->_data + ev->_data_offset;
	char *dest_ptr = store->_data + (ev->_data_offset = dest_offset);
	dest_offset += ev->_data_length;

	/* Move (if needed) the event to its new position */
	assert (dest_ptr <= src_ptr);
	memmove(dest_ptr, src_ptr, ev->_data_length);
      }

      /* Get pointer to the subevents */
      sev = (lwroc_lmd_sticky_meta_subevent *) (ev + 1);

      for (i = 0; i < ev->_num_sub; i++, sev++)
	{
	  char *src_ptr;
	  char *dest_ptr;

	  if (sev->_data_offset == -1)
	    continue;

	  src_ptr = store->_data + sev->_data_offset;
	  dest_ptr = store->_data +
	    (sev->_data_offset = (ssize_t) dest_offset);
	  dest_offset += sev->_data_length;

	  /* Move (if needed) to its new position */
	  assert (dest_ptr <= src_ptr);
	  memmove(dest_ptr, src_ptr, sev->_data_length);
	}
    }
  assert(iter_ev_offset == store->_meta_end);
  /* New end of meta-data: */
  store->_data_end = dest_offset;
  store->_data_revoked = 0;

#if DEBUG_LMD_STICKY_STORE
  lwroc_lmd_sticky_store_verify_meta(store);
#endif
}

void lwroc_lmd_sticky_store_compact_meta(lwroc_lmd_sticky_store *store)
{
  size_t dest_offset = 0;
  size_t iter_ev_offset;

#if DEBUG_LMD_STICKY_STORE
  lwroc_lmd_sticky_store_verify_meta(store);
# if DEBUG_LMD_STICKY_STORE_PRINT
  fprintf(stderr,"Compact meta!\n");
# endif
#endif
  /* Go through the meta-data, and squeeze out all subevents that have
   * vanished.
   */

  for (iter_ev_offset = 0; iter_ev_offset < store->_meta_end; )
    {
      lwroc_lmd_sticky_meta_event *ev;
      lwroc_lmd_sticky_meta_subevent *sev;
      size_t dest_ev_offset;
      lwroc_lmd_sticky_meta_event *dev;
      uint32_t i;

      ev = (lwroc_lmd_sticky_meta_event *) (store->_meta + iter_ev_offset);
      iter_ev_offset +=
	sizeof (lwroc_lmd_sticky_meta_event) +
	ev->_num_sub * sizeof (lwroc_lmd_sticky_meta_subevent);

      if (!ev->_live_sub)
	{
	  /* There are no live subevents left in this event.
	   * Completely forget about it.
	   */
	  continue;
	}

      /* Get pointer to the subevents (source) */
      sev = (lwroc_lmd_sticky_meta_subevent *) (ev + 1);

      dest_ev_offset = dest_offset;
      dev = (lwroc_lmd_sticky_meta_event *) (store->_meta + dest_offset);
      dest_offset += sizeof (lwroc_lmd_sticky_meta_event);

      /* Move (if needed) the event to its new position */
      assert (dev <= ev);
      memmove(dev, ev, sizeof (lwroc_lmd_sticky_meta_event));
      ev = dev;

      for (i = 0; i < ev->_num_sub; i++, sev++)
	{
	  lwroc_lmd_sticky_meta_subevent *dsev;

	  if (sev->_data_offset == -1)
	    continue;

	  /* Subevent is still live.  Calculate its new position. */
	  dsev =
	    (lwroc_lmd_sticky_meta_subevent *) (store->_meta + dest_offset);
	  dest_offset += sizeof (lwroc_lmd_sticky_meta_subevent);

	  /* Move (if needed) to its new position */
	  assert (dsev <= sev);
	  memmove(dsev, sev, sizeof (lwroc_lmd_sticky_meta_subevent));
	  /* Update the pointer to its event. */
	  dsev->_ev_offset = dest_ev_offset;
	}
      assert(sev == (void*) store->_meta + iter_ev_offset);
      ev->_num_sub = ev->_live_sub;
    }
  assert(iter_ev_offset == store->_meta_end);
  /* New end of meta-data: */
  store->_meta_end = dest_offset;
  store->_meta_revoked = 0;
#if DEBUG_LMD_STICKY_STORE
  /* We cannot verify data here, since hash table has stale pointers! */
  /* lwroc_lmd_sticky_store_verify_meta(store); */
#endif
}

void lwroc_lmd_sticky_store_populate_hash(lwroc_lmd_sticky_store *store)
{
  size_t i;
  size_t iter_ev_offset;

#if DEBUG_LMD_STICKY_STORE
# if DEBUG_LMD_STICKY_STORE_PRINT
  fprintf(stderr,"Populating hash!\n");
# endif
#endif

  /* Initialize all items empty */

  store->_hash_used = 0;
  for (i = 0; i < store->_hash_size; i++)
    store->_hash[i]._sub_offset = 0;

  /* Go through the meta-data, and reconstruct the hash table */

  for (iter_ev_offset = 0; iter_ev_offset < store->_meta_end; )
    {
      lwroc_lmd_sticky_meta_event *ev;
      lwroc_lmd_sticky_meta_subevent *sev;
      uint32_t j;

      ev = (lwroc_lmd_sticky_meta_event *) (store->_meta + iter_ev_offset);
      iter_ev_offset +=
	sizeof (lwroc_lmd_sticky_meta_event) +
	ev->_num_sub * sizeof (lwroc_lmd_sticky_meta_subevent);

      if (!ev->_live_sub)
	continue;

      /* Get pointer to the subevents */
      sev = (lwroc_lmd_sticky_meta_subevent *) (ev + 1);

      for (j = 0; j < ev->_num_sub; j++, sev++)
	{
	  if (sev->_data_offset == -1)
	    continue;

	  /* Subevent is live, insert into hash table
	   * insert_hash((char *) sev - _meta);
	   */
	  lwroc_lmd_sticky_store_insert_hash(store, sev);
	}
      assert(sev == (void*) store->_meta + iter_ev_offset);
    }
  assert(iter_ev_offset == store->_meta_end);
#if DEBUG_LMD_STICKY_STORE
  lwroc_lmd_sticky_store_verify_meta(store);
#endif
}

void lwroc_lmd_sticky_store_insert_hash(lwroc_lmd_sticky_store *store,
					lwroc_lmd_sticky_meta_subevent *sev)
{
#if DEBUG_LMD_STICKY_STORE
# if DEBUG_LMD_STICKY_STORE_PRINT
  fprintf(stderr,"Insert hash!\n");
# endif
#endif

  /* can never be 0, as an event always is before the subevent */
  ssize_t offset_sev = (char *) sev - store->_meta;

  char *sev_raw = store->_data + sev->_data_offset;

  lmd_subevent_10_1_host *subevent_header =
    (lmd_subevent_10_1_host *) sev_raw;

  uint32_t hash = sticky_subevent_hash(subevent_header);

  size_t entry = hash & (store->_hash_size - 1);

  while (store->_hash[entry]._sub_offset &&
	 store->_hash[entry]._sub_offset != -1) /* can insert at disc'd slot*/
    {
      entry = (entry + 1) & (store->_hash_size - 1);
    }
  store->_hash[entry]._sub_offset = offset_sev;
  store->_hash_used++;
}

#if DEBUG_LMD_STICKY_STORE
void lwroc_lmd_sticky_store_verify_meta(lwroc_lmd_sticky_store *store)
{
  /* This function is used to check the integrity of the meta-data
   * structures
   */

  size_t iter_ev_offset;

# if DEBUG_LMD_STICKY_STORE_PRINT
  fprintf (stderr,"--- DATA: [%05zx/%05zx] r:%05zx ---\n",
	   store->_data_end, store->_data_alloc, store->_data_revoked);
  fprintf (stderr,"--- META: [%05zx/%05zx] r:%05zx ---\n",
	   store->_meta_end, store->_meta_alloc, store->_meta_revoked);
# endif
  for (iter_ev_offset = 0; iter_ev_offset < store->_meta_end; )
    {
      size_t this_ev_offset;
      lwroc_lmd_sticky_meta_event *ev;
      uint32_t live_sub;
      lwroc_lmd_sticky_meta_subevent *sev;
      uint32_t j;

      assert (iter_ev_offset + sizeof (lwroc_lmd_sticky_meta_event) <=
	      store->_meta_end);

      this_ev_offset = iter_ev_offset;
      ev = (lwroc_lmd_sticky_meta_event *) (store->_meta + iter_ev_offset);
# if DEBUG_LMD_STICKY_STORE_PRINT
      fprintf (stderr,
	       "@%04zx  EV   d@=%05zx:%02zx  live:%d/%d\n",
	       iter_ev_offset,
	       ev->_data_offset, ev->_data_length,
	       ev->_live_sub, ev->_num_sub);
# endif
      iter_ev_offset +=
	sizeof (lwroc_lmd_sticky_meta_event) +
	ev->_num_sub * sizeof (lwroc_lmd_sticky_meta_subevent);
      assert(iter_ev_offset <= store->_meta_end);

      assert(!ev->_live_sub ||
	     ev->_data_offset + ev->_data_length <= store->_data_end);

      live_sub = 0;

      /* Get pointer to the subevents */
      sev = (lwroc_lmd_sticky_meta_subevent *) (ev + 1);

      for (j = 0; j < ev->_num_sub; j++, sev++)
	{
	  assert((char *) (sev+1) <= store->_meta + store->_meta_end);

# if DEBUG_LMD_STICKY_STORE_PRINT
	  fprintf (stderr,
		   " @%04zx  SEV d@=%05zx:%02zx  (EV@%04zx)  (fl:%x)",
		   ((char *) sev) - store->_meta,
		   sev->_data_offset, sev->_data_length,
		   sev->_ev_offset,
		   sev->_flags);
# endif

	  if (sev->_data_offset == -1)
	    {
# if DEBUG_LMD_STICKY_STORE_PRINT
	      fprintf (stderr, "\n");
# endif
	      continue;
	    }

	  live_sub++;

	  assert(sev->_ev_offset == this_ev_offset);
	  assert(sev->_data_offset > 0);
	  assert(sev->_data_offset + (ssize_t) sev->_data_length <=
		 (ssize_t) store->_data_end);

# if DEBUG_LMD_STICKY_STORE_PRINT
	  {
	    lmd_subevent_10_1_host *sev_header =
	      (lmd_subevent_10_1_host *) (store->_data + sev->_data_offset);
	    fprintf (stderr, " ctrl:%d\n",
		     sev_header->h_control);
	  }
# endif
	}
      assert(live_sub == ev->_live_sub);
    }
  assert(iter_ev_offset == store->_meta_end);

# if DEBUG_LMD_STICKY_STORE_PRINT
  {
  size_t i;
  fprintf (stderr,"--- HASH: [%zx/%zx] ---\n",
	   store->_hash_used,store->_hash_size);
  for (i = 0; i < store->_hash_size; i++)
    {
      if (store->_hash[i]._sub_offset &&
	  store->_hash[i]._sub_offset != -1)
	{
	  fprintf (stderr,
		   " [%04zx] SEV@%04zx\n",
		   i,
		   store->_hash[i]._sub_offset);
	}
    }
  fprintf (stderr,"-------------\n");
  }
# endif
}
#endif

size_t lwroc_lmd_sticky_store_write_events(lwroc_lmd_sticky_store *store,
					   char *dest, size_t left
					   /*
					     lmd_output_buffered *dest
					   */)
{
  size_t written = 0;
  uint32_t i;
  size_t iter_ev_offset;

#if DEBUG_LMD_STICKY_STORE
  lwroc_lmd_sticky_store_verify_meta(store);
# if DEBUG_LMD_STICKY_STORE_PRINT
  fprintf(stderr,"Write events!\n");
# endif
#endif

  /* Go through the meta-data, and prepare all non-empty events
   * for writing, and send the write call.
   */

  /* TODO: when writing for a file, we should not eject sticky
   * 'revoke' events.  The file is new, and does not need any
   * revoking.
   */

#if UCESB
  lmd_event_out event;
#endif

  for (iter_ev_offset = 0; iter_ev_offset < store->_meta_end; )
    {
      lwroc_lmd_sticky_meta_event *ev;
      size_t write_length;
      const char *src_ptr;
      lwroc_lmd_sticky_meta_subevent *sev;

      ev = (lwroc_lmd_sticky_meta_event *) (store->_meta + iter_ev_offset);
      iter_ev_offset +=
	sizeof (lwroc_lmd_sticky_meta_event) +
	ev->_num_sub * sizeof (lwroc_lmd_sticky_meta_subevent);

      if (!ev->_live_sub)
	{
	  /* There are no live subevents left in this event. */
	  continue;
	}

      /* First find out the size that needs to be written. */

      src_ptr = store->_data + ev->_data_offset;
      write_length = ev->_data_length;

      sev = (lwroc_lmd_sticky_meta_subevent *) (ev + 1);

      for (i = 0; i < ev->_num_sub; i++, sev++)
	{
	  if (sev->_data_offset == -1)
	    continue;

	  write_length += sev->_data_length;
	}

      if (write_length > left)
	return written;

      /* We have space enough for this item. */

      left -= write_length;
      written += write_length;

#if UCESB
      event.clear();
#endif

      src_ptr = store->_data + ev->_data_offset;

#if UCESB
      const lmd_event_10_1_host *header_ptr =
	(lmd_event_10_1_host *) src_ptr;

      /* The first part of the header */
      event._header = header_ptr->_header;
      /* The second part of the header */
      event._info = header_ptr->_info;
      /* Which is handled as a chunk */
      event.add_chunk(&event._info, sizeof (event._info), false/*native*/);

      assert(ev->_data_length >= sizeof (lmd_event_10_1_host));
      const char *more_ptr = src_ptr + sizeof (lmd_event_10_1_host);
      size_t more_length = ev->_data_length - sizeof (lmd_event_10_1_host);
      /* Any additional data */
      if (more_length)
	event.add_chunk(more_ptr, more_length, false/*native*/);
#else
      memcpy (dest, src_ptr, ev->_data_length);
      dest += ev->_data_length;
#endif

      /*
      printf ("%" MYPRIzd " = %" MYPRIzd " - %" MYPRIzd "\n",
	      more_length,ev->_data_length,sizeof (lmd_event_10_1_host));
      */
      /* Get pointer to the subevents */
      /*lwroc_lmd_sticky_meta_subevent **/sev =
	(lwroc_lmd_sticky_meta_subevent *) (ev + 1);

      for (i = 0; i < ev->_num_sub; i++, sev++)
	{
	  const char *sev_src_ptr;

	  if (sev->_data_offset == -1)
	    continue;
	  sev_src_ptr = store->_data + sev->_data_offset;

#if UCESB
	  event.add_chunk(sev_src_ptr, sev->_data_length, false/*native*/);
#else
	  memcpy (dest, sev_src_ptr, sev->_data_length);
	  dest += sev->_data_length;
	  /* We will not write this one again. */
	  store->_data_revoked += sev->_data_length;
	  store->_meta_revoked += sizeof (lwroc_lmd_sticky_meta_subevent);
	  /* TODO: remove from the hash. */
#endif
	}

#if UCESB
      /* event.dump_debug(); */
      dest->write_event(&event, true);
#else
      /* What we have written, we will not have to write again.  So
       * forget it.
       */
      ev->_num_sub = 0;
      store->_data_revoked += ev->_data_length;
      store->_meta_revoked += sizeof (lwroc_lmd_sticky_meta_event);
#endif
    }
  assert(iter_ev_offset == store->_meta_end);

  return written;
}

int lwroc_lmd_sticky_store_has_chunks(lwroc_lmd_sticky_store *store)
{
  /* This function does a quick test.  To verify if there really
   * is any data, first compact the meta-data!
   */
  return store->_meta_end != 0;
}

int lwroc_lmd_sticky_store_write_chunks(lwroc_lmd_sticky_store *store,
					lwroc_lmd_buffer_chunks *lbc,
					size_t *piter_ev_offset,
					int discard_written)
{
#if DEBUG_LMD_STICKY_STORE
  lwroc_lmd_sticky_store_verify_meta(store);
# if DEBUG_LMD_STICKY_STORE_PRINT
  fprintf(stderr,"Write events!\n");
# endif
#endif

#define LBC (&(lbc->_gdf))

  /* Go through the meta-data, and prepare all non-empty events
   * for writing, and send the write call.
   */

  /* TODO: when writing for a file, we should not eject sticky
   * 'revoke' events.  The file is new, and does not need any
   * revoking.
   */

  if (*piter_ev_offset == store->_meta_end)
    return 0; /* No (more) sticky data to write. */

  for ( ; *piter_ev_offset < store->_meta_end; )
    {
      size_t next_ev_offset;
      size_t s;
      lwroc_lmd_sticky_meta_event *ev;
      lwroc_lmd_sticky_meta_subevent *sev;
      uint32_t i;
      lmd_event_10_1_host *ev_header;

      ev = (lwroc_lmd_sticky_meta_event *) (store->_meta + *piter_ev_offset);
      next_ev_offset = *piter_ev_offset +
	sizeof (lwroc_lmd_sticky_meta_event) +
	ev->_num_sub * sizeof (lwroc_lmd_sticky_meta_subevent);

      if (!ev->_live_sub)
	goto ev_handled;

      /* First find out the size that needs to be written. */

      s = ev->_data_length;

      sev = (lwroc_lmd_sticky_meta_subevent *) (ev + 1);

      for (i = 0; i < ev->_num_sub; i++, sev++)
	if (sev->_data_offset != -1)
	  {
	    if (sev->_data_offset != (ssize_t) ev->_data_offset + (ssize_t) s)
	      {
		/* This should not happen,
		 * as we are to write continuous data.
		 */
		LWROC_BUG("Non-continuous sticky store data "
			  "while preparing chunk.");
	      }
	    s += sev->_data_length;
	  }

      if (LBC->chunks[LWROC_GDF_BUF_CHUNK_DATA_1].size +
	  s + sizeof (s_bufhe_host) > LBC->buffer_size)
	{
	  if (LBC->chunks[LWROC_GDF_BUF_CHUNK_DATA_1].size == 0)
	    {
	      /* See similar in lwroc_lmd_buffer_chunks_collect(). */
	      LWROC_FATAL("Sticky event longer than output lmd buffer.");
	    }
	  goto buffer_full;
	}

      /* We have space enough for this item. */

      if (LBC->chunks[LWROC_GDF_BUF_CHUNK_DATA_1].ptr == NULL)
	LBC->chunks[LWROC_GDF_BUF_CHUNK_DATA_1].ptr =
	  store->_data + ev->_data_offset;

      /* It is somewhat dirty, but we are the only ones writing from
       * this buffer: We have to update the event header with the
       * correct length, in case some subevents were discarded from it
       * since it was inserted into the sticky store.
       */
      ev_header = (lmd_event_10_1_host *) (store->_data + ev->_data_offset);

      ev_header->_header.l_dlen = (uint32_t)
	DLEN_FROM_EVENT_DATA_LENGTH(s - sizeof (lmd_event_header_host));

      LBC->chunks[LWROC_GDF_BUF_CHUNK_DATA_1].size += s;
      LBC->collect_events++;

      if (discard_written)
	{
	  ev->_live_sub = 0;
	  store->_data_revoked += ev->_data_length;
	  store->_meta_revoked += sizeof (lwroc_lmd_sticky_meta_event);
	  sev = (lwroc_lmd_sticky_meta_subevent *) (ev + 1);
	  for (i = 0; i < ev->_num_sub; i++, sev++)
	    if (sev->_data_offset != -1)
	      {
		sev->_data_offset = -1;
		store->_data_revoked += sev->_data_length;
		store->_meta_revoked += sizeof (lwroc_lmd_sticky_meta_subevent);
	      }
	}

    ev_handled:
      *piter_ev_offset = next_ev_offset;
    }
  assert(*piter_ev_offset == store->_meta_end);

 buffer_full:
  LBC->has_sticky = 1;
  lwroc_lmd_buffer_chunks_prepare_write(lbc);

  return 1;
}

void lwroc_lmd_sticky_store_copy(lwroc_lmd_sticky_store *dest,
				 lwroc_lmd_sticky_store *src)
{
  dest->_data_alloc = src->_data_alloc;
  dest->_meta_alloc = src->_meta_alloc;
  dest->_hash_size = src->_hash_size;

  dest->_data = (char *) realloc (dest->_data, dest->_data_alloc);
  dest->_meta = (char *) realloc (dest->_meta, dest->_meta_alloc);
  dest->_hash = (lwroc_lmd_sticky_hash_subevent *)
    realloc (dest->_hash, dest->_hash_size * sizeof (dest->_hash[0]));

  if ((dest->_data_alloc && !dest->_data) ||
      (dest->_meta_alloc && !dest->_meta) ||
      (dest->_hash_size  && !dest->_hash))
    LWROC_ERROR("Memory allocation failure.");

  memcpy (dest->_data, src->_data, src->_data_end);
  memcpy (dest->_meta, src->_meta, src->_meta_end);
  memcpy (dest->_hash, src->_hash, src->_hash_used);

  dest->_data_end  = src->_data_end;
  dest->_meta_end  = src->_meta_end;
  dest->_hash_used = src->_hash_used;
}

size_t lwroc_lmd_sticky_store_size_revoke(lwroc_lmd_sticky_store *store)
{
  size_t iter_ev_offset;
  size_t revoke_size = 0;

  /* We are called after compactification.  All subevents are live. */

  /* Any subevent marked active means that we have events to revoke. */

  for (iter_ev_offset = 0; iter_ev_offset < store->_meta_end; )
    {
      lwroc_lmd_sticky_meta_event *ev;
      lwroc_lmd_sticky_meta_subevent *sev;
      int ev_had_active = 0;
      uint32_t i;

      ev = (lwroc_lmd_sticky_meta_event *) (store->_meta + iter_ev_offset);
      iter_ev_offset +=
	sizeof (lwroc_lmd_sticky_meta_event) +
	ev->_num_sub * sizeof (lwroc_lmd_sticky_meta_subevent);

      sev = (lwroc_lmd_sticky_meta_subevent *) (ev + 1);

      /* printf ("size_revoke: \n"); */

      for (i = 0; i < ev->_num_sub; i++, sev++)
	{
	  assert (sev->_data_offset != -1);

	  /* printf ("size_revoke: sub %d fl:%d\n", i, sev->_flags); */

	  if (sev->_flags & LWROC_LMD_STICKY_FLAG_ACTIVE)
	    {
	      if (!ev_had_active)
		{
		  ev_had_active = 1;
		  /* Assume revoke event header has same length as event
		   * header.  Including possible time stamp.
		   */
		  revoke_size += ev->_data_length;
		}
	      /* Revoking takes a subevent header. */
	      revoke_size += sizeof (lmd_subevent_10_1_host);
	    }
	}
    }
  assert(iter_ev_offset == store->_meta_end);

  return revoke_size;
}

size_t lwroc_lmd_sticky_store_write_revoke(lwroc_lmd_sticky_store *store,
					   char *dest)
{
  char *start = dest;
  size_t iter_ev_offset;

  /* We are called after compactification.  All subevents are live. */

  for (iter_ev_offset = 0; iter_ev_offset < store->_meta_end; )
    {
      lwroc_lmd_sticky_meta_event *ev;
      lwroc_lmd_sticky_meta_subevent *sev;
      int ev_had_active = 0;
      uint32_t i;
      lmd_event_10_1_host *dest_header_ptr = NULL;

      ev = (lwroc_lmd_sticky_meta_event *) (store->_meta + iter_ev_offset);
      iter_ev_offset +=
	sizeof (lwroc_lmd_sticky_meta_event) +
	ev->_num_sub * sizeof (lwroc_lmd_sticky_meta_subevent);

      sev = (lwroc_lmd_sticky_meta_subevent *) (ev + 1);

      for (i = 0; i < ev->_num_sub; i++, sev++)
	{
	  assert (sev->_data_offset != -1);

	  if (sev->_flags & LWROC_LMD_STICKY_FLAG_ACTIVE)
	    {
	      const char *sev_src_ptr;
	      lmd_subevent_10_1_host *dest_sev_header;

	      sev_src_ptr = store->_data + sev->_data_offset;

	      if (!ev_had_active)
		{
		  const char *src_ptr;

		  ev_had_active = 1;

		  /* Assume revoke event header has same length as event
		   * header.  Including possible time stamp.
		   *
		   * TODO: modify timestamp??
		   */

		  src_ptr = store->_data + ev->_data_offset;

		  dest_header_ptr = (lmd_event_10_1_host *) dest;

		  /* Copy the event header. */

		  memcpy (dest_header_ptr, src_ptr, ev->_data_length);
		  dest += ev->_data_length;

		  /* dest_header_ptr->... = ... */
		}

	      dest_sev_header = (lmd_subevent_10_1_host *) dest;

	      /* Revoking takes a subevent header. */
	      memcpy (dest, sev_src_ptr, sizeof (lmd_subevent_10_1_host));
	      dest += sizeof (lmd_subevent_10_1_host);
	      /* And mark it revoke. */
	      dest_sev_header->_header.l_dlen =
		LMD_SUBEVENT_STICKY_DLEN_REVOKE;

	      /* Subevent has been revoked. */
	      sev->_flags &= ~LWROC_LMD_STICKY_FLAG_ACTIVE;
	    }
	}

      /* Update the header length. */
      if (dest_header_ptr)
	dest_header_ptr->_header.l_dlen = (uint32_t)
	  DLEN_FROM_EVENT_DATA_LENGTH((size_t) (dest -
						(char *) dest_header_ptr) -
				      sizeof (lmd_event_header_host));
    }
  assert(iter_ev_offset == store->_meta_end);

  return (size_t) (dest - start);
}

void lwroc_lmd_sticky_store_apply(lwroc_lmd_sticky_store *store,
				  lwroc_lmd_sticky_store *src,
				  int as_active)
{
  size_t iter_ev_offset;

#if DEBUG_LMD_STICKY_STORE
  lwroc_lmd_sticky_store_verify_meta(store);
# if DEBUG_LMD_STICKY_STORE_PRINT
  fprintf(stderr,"Apply from other store!\n");
# endif
#endif

  /* Go through the events, and insert them into the store. */

  for (iter_ev_offset = 0; iter_ev_offset < src->_meta_end; )
    {
      size_t s;
      lwroc_lmd_sticky_meta_event *ev;
      lwroc_lmd_sticky_meta_subevent *sev;
      uint32_t i;

      ev = (lwroc_lmd_sticky_meta_event *) (src->_meta + iter_ev_offset);
      iter_ev_offset +=
	sizeof (lwroc_lmd_sticky_meta_event) +
	ev->_num_sub * sizeof (lwroc_lmd_sticky_meta_subevent);

      if (!ev->_live_sub)
	continue;

      /* First find out the size. */

      s = ev->_data_length;

      sev = (lwroc_lmd_sticky_meta_subevent *) (ev + 1);

      for (i = 0; i < ev->_num_sub; i++, sev++)
	if (sev->_data_offset != -1)
	  {
	    if (sev->_data_offset != (ssize_t) ev->_data_offset + (ssize_t) s)
	      {
		/* This should not happen,
		 * as we are to write continuous data.
		 */
		LWROC_BUG("Non-continuous sticky store data "
			  "while applying event.");
	      }
	    s += sev->_data_length;
	  }

      lwroc_lmd_sticky_store_insert(store,
				    src->_data + ev->_data_offset, s,
				    0, as_active);
    }
  assert(iter_ev_offset == src->_meta_end);
}

void lwroc_lmd_sticky_store_remove_revokes(lwroc_lmd_sticky_store *store)
{
  size_t iter_ev_offset;

  for (iter_ev_offset = 0; iter_ev_offset < store->_meta_end; )
    {
      lwroc_lmd_sticky_meta_event *ev;
      lwroc_lmd_sticky_meta_subevent *sev;
      uint32_t i;

      ev = (lwroc_lmd_sticky_meta_event *) (store->_meta + iter_ev_offset);
      iter_ev_offset +=
	sizeof (lwroc_lmd_sticky_meta_event) +
	ev->_num_sub * sizeof (lwroc_lmd_sticky_meta_subevent);

      sev = (lwroc_lmd_sticky_meta_subevent *) (ev + 1);

      for (i = 0; i < ev->_num_sub; i++, sev++)
	{
	   lmd_subevent_10_1_host *sev_header =
	     (lmd_subevent_10_1_host *) (store->_data + sev->_data_offset);

	   if (sev_header->_header.l_dlen == LMD_SUBEVENT_STICKY_DLEN_REVOKE)
	     {
	       sev->_data_offset = -1;

	       store->_data_revoked += sev->_data_length;
	       store->_meta_revoked += sizeof (lwroc_lmd_sticky_meta_subevent);
	       ev->_live_sub--;
	       if (ev->_live_sub == 0)
		 {
		   /* Upon compacting, this event will not be retained. */
		   store->_data_revoked += ev->_data_length;
		   store->_meta_revoked += sizeof (lwroc_lmd_sticky_meta_event);
		 }
	       /* We do not need to find and discard the hash entry,
		* users of it will notice that we have been discarded.
		*/
	     }
	}
    }
}

size_t lwroc_lmd_sticky_store_write_inactive(lwroc_lmd_sticky_store *store,
					     char *dest, size_t left,
					     size_t *need_size,
					     size_t *piter_ev_offset)
{
  size_t written = 0;
  *need_size = 0;

#if DEBUG_LMD_STICKY_STORE
  lwroc_lmd_sticky_store_verify_meta(store);
# if DEBUG_LMD_STICKY_STORE_PRINT
  fprintf(stderr,"Write inactive events!\n");
# endif
#endif

  /* Go through the meta-data, and prepare all non-empty events
   * for writing, and send the write call.
   */

  for ( ; *piter_ev_offset < store->_meta_end; )
    {
      size_t next_ev_offset;
      size_t s;
      lwroc_lmd_sticky_meta_event *ev;
      lwroc_lmd_sticky_meta_subevent *sev;
      uint32_t i;
      int ev_has_active = 0;
      const char *src_ptr;

      ev = (lwroc_lmd_sticky_meta_event *) (store->_meta + *piter_ev_offset);
      next_ev_offset = *piter_ev_offset +
	sizeof (lwroc_lmd_sticky_meta_event) +
	ev->_num_sub * sizeof (lwroc_lmd_sticky_meta_subevent);

      if (!ev->_live_sub)
	goto ev_handled;

      /* First find out the size that needs to be written. */

      s = ev->_data_length;

      sev = (lwroc_lmd_sticky_meta_subevent *) (ev + 1);

      for (i = 0; i < ev->_num_sub; i++, sev++)
	{
	  if (sev->_data_offset == -1)
	    continue;
	  if (sev->_flags & LWROC_LMD_STICKY_FLAG_ACTIVE)
	    continue;

	  s += sev->_data_length;
	  ev_has_active = 1;
	}

      if (!ev_has_active)
	goto ev_handled;

      if (s > left)
	{
	  printf ("s > left: dest %p left %" MYPRIzd " s %" MYPRIzd "\n",
		  dest, left, s);
	  if (dest != NULL && written == 0)
	    {
	      LWROC_FATAL("Sticky event longer than output buffer.");
	    }
	  *need_size = s;
	  return written;
	}

      left -= s;
      written += s;

      src_ptr = store->_data + ev->_data_offset;

      memcpy (dest, src_ptr, ev->_data_length);
      /* Update the header length. */
      ((lmd_event_10_1_host *) dest)->_header.l_dlen = (uint32_t)
	DLEN_FROM_EVENT_DATA_LENGTH(s - sizeof (lmd_event_header_host));

      dest += ev->_data_length;

      /*lwroc_lmd_sticky_meta_subevent **/sev =
	(lwroc_lmd_sticky_meta_subevent *) (ev + 1);

      for (i = 0; i < ev->_num_sub; i++, sev++)
	{
	  const char *sev_src_ptr;

	  if (sev->_data_offset == -1)
	    continue;
	  if (sev->_flags & LWROC_LMD_STICKY_FLAG_ACTIVE)
	    continue;

	  sev_src_ptr = store->_data + sev->_data_offset;

	  memcpy (dest, sev_src_ptr, sev->_data_length);
	  dest += sev->_data_length;

	  sev->_flags |= LWROC_LMD_STICKY_FLAG_ACTIVE;
	}

    ev_handled:
      *piter_ev_offset = next_ev_offset;
    }
  assert(*piter_ev_offset == store->_meta_end);

  return written;
}

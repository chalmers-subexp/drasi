/* This file is part of UCESB - a tool for data unpacking and processing.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_LMD_STICKY_STORE_HH__
#define __LWROC_LMD_STICKY_STORE_HH__

#include <stdlib.h>
#include <stdint.h>
#include <sys/types.h>

typedef struct lwroc_lmd_sticky_meta_event_t
{
  size_t   _data_offset;
  size_t   _data_length; /* may include timestamp subevent */
  uint32_t _num_sub;     /* number of subevent metadata entries */
  uint32_t _live_sub;    /* decreased as subevents are revoked */
} lwroc_lmd_sticky_meta_event;

#define LWROC_LMD_STICKY_FLAG_ACTIVE  0x0001

typedef struct lwroc_lmd_sticky_meta_subevent_t
{
  size_t   _ev_offset;
  ssize_t  _data_offset; /* -1 for data not live any longer */
  size_t   _data_length;
  int      _flags;
} lwroc_lmd_sticky_meta_subevent;

typedef struct lwroc_lmd_sticky_hash_subevent_t
{
  ssize_t  _sub_offset;  /* -1 for revoked */
} lwroc_lmd_sticky_hash_subevent;

/*class lmd_event_out;*/
/*class lmd_output_buffered;*/

typedef struct lwroc_lmd_sticky_store_t
{
  /* *********** */
  char   *_data;
  size_t  _data_end;
  size_t  _data_alloc;

  size_t  _data_revoked;

  /* *********** */
  char   *_meta;
  size_t  _meta_end;
  size_t  _meta_alloc;

  size_t  _meta_revoked;

  /* *********** */
  lwroc_lmd_sticky_hash_subevent *_hash;
  size_t  _hash_used;
  size_t  _hash_size;

} lwroc_lmd_sticky_store;

lwroc_lmd_sticky_store *lwroc_lmd_sticky_store_init(void);
void lwroc_lmd_sticky_store_deinit(lwroc_lmd_sticky_store *store);

void lwroc_lmd_sticky_store_clear(lwroc_lmd_sticky_store *store);

void lwroc_lmd_sticky_store_insert(lwroc_lmd_sticky_store *store,
				   char *ptr, size_t len,
				   int discard_revoke,
				   int as_active);

size_t lwroc_lmd_sticky_store_write_events(lwroc_lmd_sticky_store *store,
					   char *dest, size_t left);

struct lwroc_lmd_buffer_chunks_t;

typedef struct lwroc_lmd_buffer_chunks_t lwroc_lmd_buffer_chunks;

/* Note: may return true if even if the chunks in reality are
 * empty/discarded. */
int lwroc_lmd_sticky_store_has_chunks(lwroc_lmd_sticky_store *store);

int lwroc_lmd_sticky_store_write_chunks(lwroc_lmd_sticky_store *store,
					lwroc_lmd_buffer_chunks *lbc,
					size_t *iter_ev_offset,
					int discard_written);

void lwroc_lmd_sticky_store_compact_data(lwroc_lmd_sticky_store *store);
void lwroc_lmd_sticky_store_compact_meta(lwroc_lmd_sticky_store *store);
void lwroc_lmd_sticky_store_populate_hash(lwroc_lmd_sticky_store *store);

void lwroc_lmd_sticky_store_insert_hash(lwroc_lmd_sticky_store *store,
					lwroc_lmd_sticky_meta_subevent *sev);

void lwroc_lmd_sticky_store_verify_meta(lwroc_lmd_sticky_store *store);

void lwroc_lmd_sticky_store_copy(lwroc_lmd_sticky_store *dest,
				 lwroc_lmd_sticky_store *src);

size_t lwroc_lmd_sticky_store_size_revoke(lwroc_lmd_sticky_store *store);
size_t lwroc_lmd_sticky_store_write_revoke(lwroc_lmd_sticky_store *store,
					   char *dest);

size_t lwroc_lmd_sticky_store_write_inactive(lwroc_lmd_sticky_store *store,
					     char *dest, size_t left,
					     size_t *need_size,
					     size_t *piter_ev_offset);

void lwroc_lmd_sticky_store_apply(lwroc_lmd_sticky_store *store,
				  lwroc_lmd_sticky_store *src,
				  int as_active);
void lwroc_lmd_sticky_store_remove_revokes(lwroc_lmd_sticky_store *store);

#endif/*__LWROC_LMD_STICKY_STORE_HH__*/

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Bastian Loeher  <b.loeher@gsi.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lmd/lwroc_lmd_util.h"
#include "gdf/lwroc_gdf_util.h"
#include "lwroc_message.h"
#include "lwroc_net_trans.h"

#include <assert.h>
#include <string.h>

#include "../dtc_arch/acc_def/myinttypes.h"
#include "../dtc_arch/acc_def/myprizd.h"

/* Collecting data for an lmd output buffer from the circular pipe.
 *
 * In case the collect function has only found data to partially fill
 * the buffer, it may be called again after some time to see if
 * further data is available.
 *
 * In case we have a leftover of a fragmented event, we will not have
 * released it at all, but should not restart the search at its
 * beginning.
 *
 * If the search begins with waste space, that is released
 * immediately.  Waste space inside the data is ignored, and becomes
 * released upon releasing the data before it.
 *
 * How many chunks of data can we at most return?  Waste space can
 * occur at the end, and the beginning (for static data).  Thus we may
 * have one skip region.  Leading to two chunks.
 *
 * If any internal control commands are to go via the data flow
 * (e.g. file open requests [not implemented like that yet]), then
 * they have to be formatted as some special kind of proper event that
 * end up in the actual output.  Thus need no skipping.
 *
 * One may also think of going to a situation with a double pipe
 * buffer, where the first just has control records, and the second
 * pure data.  The control records need not be per event, they could
 * span many event (to reduce the amount needed).  Many-event spanning
 * however introduces latency, as they cannot be written immediately.
 * Unless we use some flag tricks...
 */

#define LBC (&(lbc->_gdf))

lwroc_gdf_buffer_chunks *lwroc_lmd_buffer_chunks_alloc(int flags)
{
  lwroc_lmd_buffer_chunks *lbc =
    (lwroc_lmd_buffer_chunks *) malloc (sizeof (lwroc_lmd_buffer_chunks));

  if (!lbc)
    LWROC_FATAL("Memory allocation failure (lmd buffer chunks).");

  memset (lbc, 0, sizeof (*lbc));

  LBC->flags = flags;

  return LBC;
}

void lwroc_lmd_buffer_chunks_init(lwroc_gdf_buffer_chunks *lbc_gdf,
				  uint32_t buffer_size,
				  int skip_internal_events)
{
  lwroc_gdf_buffer_chunks_init(lbc_gdf,
			       buffer_size, skip_internal_events);
}

void lwroc_lmd_buffer_chunks_get_fixed_raw(lwroc_gdf_buffer_chunks *lbc_gdf,
					   void **ptr, size_t *size)
{
  lwroc_lmd_buffer_chunks *lbc = (lwroc_lmd_buffer_chunks *) lbc_gdf;

  *ptr  =         lbc->fixed.raw;
  *size = sizeof (lbc->fixed.raw);
}

int lwroc_lmd_buffer_chunks_collect(/*lwroc_lmd_buffer_chunks *lbc,*/
				    lwroc_gdf_buffer_chunks *lbc_gdf,
				    lwroc_pipe_buffer_consumer *pipe_buf,
				    const lwroc_thread_block *thread_block,
				    void *v_sticky_store,
				    void *v_sticky_store_drvk,
				    int partial_buffer, int for_skipping)
{
  lwroc_lmd_buffer_chunks *lbc = (lwroc_lmd_buffer_chunks *) lbc_gdf;
  int buffer_full = 0;
  int flush_buffer = 0;
  size_t avail = 0;
  char *ptr;

  lwroc_lmd_sticky_store *sticky_store =
    (lwroc_lmd_sticky_store *) v_sticky_store;
  lwroc_lmd_sticky_store *sticky_store_drvk =
    (lwroc_lmd_sticky_store *) v_sticky_store_drvk;

 check_buffer_avail:
  for ( ; ; )
    {
      /* If there is no data, next time to be woken up is when there
       * is additionally what is missing to get a complete buffer.
       */
      size_t hysteresis =
	LBC->buffer_size - sizeof (s_bufhe_host) -
	LBC->chunks[LWROC_GDF_BUF_CHUNK_DATA_1].size -
	LBC->chunks[LWROC_GDF_BUF_CHUNK_DATA_2].size;
      size_t waste_sz;

      avail = lwroc_pipe_buffer_avail_read(pipe_buf,
					   thread_block,
					   &ptr, &waste_sz, LBC->collect_used,
					   hysteresis);

      if (!avail)
	{
	  if (waste_sz)
	    {
	      /* If we are at offset 0, then we may immediately release
	       * this part of the buffer.  Otherwise, we just skip it.
	       */

	      if (LBC->collect_used == 0)
		lwroc_pipe_buffer_waste_read(pipe_buf, waste_sz);
	      else
		{
		  LBC->collect_used += waste_sz;
		  if (LBC->collect_chunk != LWROC_GDF_BUF_CHUNK_DATA_1)
		    LWROC_BUG("Found two waste headers for one buffer.");
		  LBC->collect_chunk++;
		}
	      continue; /* Try again. */
	    }
	  break;
	}
      break;
    }

  if (avail)
    {
      /* For the moment, let's be plain stupid...
       * We write whatever amount of data is available, i.e.
       * are not waiting until we get a full buffer.
       */

      uint32_t use = 0;

      /* Data is available here, and is no waste header, so we can
       * set pointer.
       */
      if (LBC->chunks[LBC->collect_chunk].ptr == NULL)
	LBC->chunks[LBC->collect_chunk].ptr = ptr;

      for ( ; ; )
	{
	  lmd_event_header_host *header;
	  size_t s;
	  size_t min_exp_size = sizeof (lmd_event_header_host);
	  int is_sticky = 0;
	  int discard_event = 0;

	  if (avail < sizeof (lmd_event_header_host))
	    LWROC_BUG_FMT("Incomplete event header in data buffer "
			  "(%" MYPRIzd " bytes available).",
			  avail);

	  header = (lmd_event_header_host *) (ptr + use);

	  if (header->i_type    == LMD_EVENT_10_1_TYPE &&
	      header->i_subtype == LMD_EVENT_10_1_SUBTYPE)
	    {
	      /* TODO: flush buffer on triggers 12,13,14,15 */
	      min_exp_size = sizeof(lmd_event_10_1_host);
	    }
	  else if (header->i_type    == LMD_EVENT_STICKY_TYPE &&
		   header->i_subtype == LMD_EVENT_STICKY_SUBTYPE)
	    {
	      LBC->has_sticky = 1;
	      is_sticky = 1;
	      min_exp_size = sizeof(lmd_event_10_1_host);
	    }
	  else if (header->i_type    == LWROC_LMD_IDENT_TYPE &&
		   header->i_subtype == LWROC_LMD_IDENT_SUBTYPE)
	    {
	      /*
	      printf ("ident skip ? %d used: %" MYPRIzd "\n",
		      LBC->skip_internal_events, LBC->collect_used);
	      */
	      if (LBC->skip_internal_events)
		discard_event = 1;
	      flush_buffer = 1;
	      min_exp_size = sizeof(lmd_event_header_host) + 3 * sizeof(uint32_t);
	    }
	  else
	    {
	      lwroc_lmd_dump_event((lmd_event_10_1_host *) header,
				   44 /* FIXME */);
	      LWROC_BUG_FMT("Found unknown LMD event (%d=0x%04x/%d=0x%04x).",
			    header->i_type, header->i_type,
			    header->i_subtype, header->i_subtype);
	    }

	  s = sizeof (lmd_event_header_host) +
	    (size_t) EVENT_DATA_LENGTH_FROM_DLEN(header->l_dlen);

	  if (use + s > avail)
	    LWROC_BUG_FMT("Incomplete event in data buffer "
			  "(%" MYPRIzd " bytes needed, "
			  "%" MYPRIzd " available).",
			  use + s, avail);

	  if (s < min_exp_size)
	    LWROC_BUG_FMT("Too small event for event type (%d/%d) "
			  "(>=%" MYPRIzd " bytes expected, "
			  "%" MYPRIzd " bytes from event).",
			  header->i_type, header->i_subtype,
			  min_exp_size, s);

	  if (discard_event &&
	      LBC->collect_used == 0)
	    {
	      /* If we have not collected any data yet, then just
	       * ignore this event.  It is easier to just get rid of
	       * it instead of meddling with the accounting (hard to
	       * detect a second one like this).  Events to discard
	       * are rare, so slightly expensive handling is ok.
	       */
	      lwroc_pipe_buffer_did_read(pipe_buf, s);
	      LBC->chunks[LBC->collect_chunk].ptr = NULL;
	      goto check_buffer_avail;
	    }

	  if (LBC->chunks[LWROC_GDF_BUF_CHUNK_DATA_1].size +
	      LBC->chunks[LWROC_GDF_BUF_CHUNK_DATA_2].size +
	      s + sizeof (s_bufhe_host) > LBC->buffer_size)
	    {
	      if (LBC->chunks[LWROC_GDF_BUF_CHUNK_DATA_1].size +
		  LBC->chunks[LWROC_GDF_BUF_CHUNK_DATA_2].size == 0)
		{
		  /* This in principle we should guard against at an earlier
		   * location, by failing the input.  But better report
		   * a failure than running an infinite loop here.
		   */
		  LWROC_FATAL_FMT("Event longer (%" MYPRIzd " bytes) than"
				  "output lmd buffer (%" PRIu32 " bytes).",
				  s + sizeof (s_bufhe_host),
				  LBC->buffer_size);
		}
	      buffer_full = 1;
	      break;
	    }

	  if (is_sticky)
	    {
	      if (sticky_store)
		lwroc_lmd_sticky_store_insert(sticky_store, ptr + use, s,
					      0 /* no discard revoke */,
					      0 /* not using active flag */);
	      if (sticky_store_drvk) /* drvk = discard_revoke */
		lwroc_lmd_sticky_store_insert(sticky_store_drvk, ptr + use, s,
					      1 /* discard revoke */,
					      0 /* not using active flag */);
	    }

	  LBC->collect_used += s;

	  if (discard_event)
	    {
	      /* We do not add the event to the chunk to output.  We
	       * have already collected some data to output, so abort
	       * the collection and flush the buffer.
	       */
	      flush_buffer = 1;
	      break;
	    }

	  LBC->collect_events++;
	  LBC->chunks[LBC->collect_chunk].size += s;

	  use += (uint32_t) s;

	  if (use >= avail)
	    break;
	}

      if (LBC->collect_used == 0)
	LWROC_BUG("collect_used == 0, despite avail != 0");

      assert(LBC->collect_events > 0);

      /* We may at this point have read just to the end of available
       * linear space.  In that case, we should retry getting data
       * once more.  If we reached the end with a waste buffer, that
       * was already handled, so only case is when we have already
       * accepted all data into the buffer.
       */

      if (use == avail)
	goto check_buffer_avail;
    }

  if (!LBC->collect_used)
    return 0;

  if (for_skipping)
    {
      LBC->fully_used = LBC->collect_used;
      return 1;
    }

  if (!flush_buffer && !partial_buffer && !buffer_full)
    return 0;

  /* Ok, so we have found ourselves some data to write. */

  lwroc_lmd_buffer_chunks_prepare_write(lbc);

  return 1;
}

uint32_t lwroc_lmd_buffer_chunks_write_size(lwroc_lmd_buffer_chunks *lbc,
					    size_t total_use)
{
  uint32_t write_size;

  if (LBC->var_buffer_size == LWROC_GDF_BUF_VARBUFSIZE_TRIM)
    write_size = (uint32_t) total_use;
  else if (LBC->var_buffer_size == LWROC_GDF_BUF_VARBUFSIZE_TRIM_1024)
    write_size = (uint32_t) ((total_use + 1023) & (size_t) ~1023);
  else
    write_size = LBC->buffer_size;

   return write_size;
}

void lwroc_lmd_buffer_chunks_prepare_write(lwroc_lmd_buffer_chunks *lbc)
{
  uint32_t write_size;
  size_t data_use =
    LBC->chunks[LWROC_GDF_BUF_CHUNK_DATA_1].size +
    LBC->chunks[LWROC_GDF_BUF_CHUNK_DATA_2].size;
  size_t total_use = data_use + sizeof (s_bufhe_host);

  write_size = lwroc_lmd_buffer_chunks_write_size(lbc, total_use);

  /* Fill out the buffer header. */
  lwroc_lmd_buffer_chunks_bufhe(lbc,
				&lbc->fixed.bufhe,
				write_size, (uint32_t) data_use,
				LBC->collect_events, 0, 0);

  LBC->chunks[LWROC_GDF_BUF_CHUNK_BUFHE].ptr  =
    &lbc->fixed.bufhe;
  LBC->chunks[LWROC_GDF_BUF_CHUNK_BUFHE].size =
    sizeof (lbc->fixed.bufhe);
  LBC->chunks[LWROC_GDF_BUF_CHUNK_FRAG_EV_HEADER].ptr  = NULL;
  LBC->chunks[LWROC_GDF_BUF_CHUNK_FRAG_EV_HEADER].size = 0;
  /* LWROC_GDF_BUF_CHUNK_DATA_1 and LWROC_GDF_BUF_CHUNK_DATA_2
   * already filled.
   */
  LBC->chunks[LWROC_GDF_BUF_CHUNK_ZERO_PAD].ptr  = _lwroc_zero_block;
  LBC->chunks[LWROC_GDF_BUF_CHUNK_ZERO_PAD].size =
    write_size - total_use;

  LBC->fully_used = LBC->collect_used;
  LBC->done_events = LBC->collect_events;

  /*There is data to write! */

  LBC->chunk = LWROC_GDF_BUF_CHUNK_BUFHE;
}

void lwroc_lmd_buffer_chunks_trans_info(lwroc_gdf_buffer_chunks *lbc_gdf,
					int info_hint,
					uint32_t info_hint2)
{
  lwroc_lmd_buffer_chunks *lbc = (lwroc_lmd_buffer_chunks *) lbc_gdf;

  /* Info to be sent to transport client before any data. */

  lbc->fixed.trans_info.testbit = 0x00000001;
  lbc->fixed.trans_info.bufsize = LBC->buffer_size;
  lbc->fixed.trans_info.bufs_per_stream = 1;
  lbc->fixed.trans_info.streams = 1;

  if (info_hint != 0)
    {
      /* Put nasty data in the struct, to hint the client that it will
       * not get any data.
       */
      lbc->fixed.trans_info.bufsize = (uint32_t) info_hint;
      lbc->fixed.trans_info.bufs_per_stream = 0;
      lbc->fixed.trans_info.streams = info_hint2;
    }

  lwroc_gdf_buffer_chunks_send(LBC,
			       &lbc->fixed.trans_info,
			       sizeof (lbc->fixed.trans_info), 0);
}

void lwroc_lmd_buffer_chunks_bufhe(lwroc_lmd_buffer_chunks *lbc,
				   s_bufhe_host *bufhe,
				   uint32_t buffer_size, uint32_t used,
				   uint32_t events,
				   uint8_t h_end, uint8_t h_begin)
{
  struct timeval tv;

  memset (bufhe, 0, sizeof (*bufhe));

  bufhe->l_dlen =
    (uint32_t) DLEN_FROM_BUFFER_SIZE(buffer_size);
  if (!LBC->has_sticky)
    {
      bufhe->i_type    = LMD_BUF_HEADER_10_1_TYPE;
      bufhe->i_subtype = LMD_BUF_HEADER_10_1_SUBTYPE;
    }
  else
    {
      bufhe->i_type    = LMD_BUF_HEADER_HAS_STICKY_TYPE;
      bufhe->i_subtype = LMD_BUF_HEADER_HAS_STICKY_SUBTYPE;
    }

  bufhe->l_free[2] =
    IUSED_FROM_BUFFER_USED(used);

  if (bufhe->l_dlen <= LMD_BUF_HEADER_MAX_IUSED_DLEN)
    bufhe->i_used  =
      (uint16_t) bufhe->l_free[2];

  bufhe->l_buf     = LBC->buffer_no++;
  bufhe->l_evt     = events;
  bufhe->h_end     = h_end;
  bufhe->h_begin   = h_begin;

  bufhe->l_free[0] = 0x00000001;

  gettimeofday(&tv,NULL);

  bufhe->l_time[0] = (uint32_t) tv.tv_sec;
  bufhe->l_time[1] = (uint32_t) tv.tv_usec / 1000;
}

void lwroc_lmd_buffer_chunks_file_header(lwroc_gdf_buffer_chunks *lbc_gdf)
{
  lwroc_lmd_buffer_chunks *lbc = (lwroc_lmd_buffer_chunks *) lbc_gdf;
  uint32_t write_size = LBC->buffer_size;
  uint32_t header_size = sizeof (s_filhe_host);
  s_bufhe_host *bufhe            = &lbc->fixed.filhe._buf_header;
  s_filhe_extra_host *file_extra = &lbc->fixed.filhe._file_extra;

  memset (file_extra, 0, sizeof (*file_extra));

  /* There is an awkward handling of the file header is the buffer
   * size is above 32k:
   *
   * The l_dlen member gives the buffer size of the following buffers,
   * but the file header actually written is only the size
   * corresponding to the used size of it.
   *
   * This means that all following buffers are misaligned in the file.
   */

  lwroc_lmd_buffer_chunks_bufhe(lbc,
				&lbc->fixed.filhe._buf_header,
				write_size, 0 /* does not count as used */,
				0, 0, 0);

  bufhe->i_type    = LMD_FILE_HEADER_2000_1_TYPE;
  bufhe->i_subtype = LMD_FILE_HEADER_2000_1_SUBTYPE;

  if (write_size > BUFFER_SIZE_FROM_DLEN(LMD_BUF_HEADER_MAX_IUSED_DLEN))
    {
      uint32_t lines =
	file_extra->filhe_lines < 30 ? file_extra->filhe_lines : 30;
      size_t file_header_used =
	sizeof(*file_extra) - sizeof(file_extra->s_strings) +
	(lines * sizeof(file_extra->s_strings[0]));
      uint32_t used;

      used = (uint32_t) file_header_used;

      bufhe->l_free[2] =
	IUSED_FROM_BUFFER_USED(used);
      bufhe->i_used = (uint16_t) bufhe->l_free[2];

      write_size = header_size = used + (uint32_t) sizeof (s_bufhe_host);
    }

  lwroc_gdf_buffer_chunks_send(LBC,
			       &lbc->fixed.filhe,
			       header_size,
			       write_size - header_size);
}

void lwroc_lmd_buffer_chunks_empty(lwroc_gdf_buffer_chunks *lbc_gdf)
{
  lwroc_lmd_buffer_chunks *lbc = (lwroc_lmd_buffer_chunks *) lbc_gdf;

  s_bufhe_host *bufhe = &lbc->fixed.filhe._buf_header;

  uint32_t write_size;
  size_t total_use = sizeof (s_bufhe_host);

  write_size = lwroc_lmd_buffer_chunks_write_size(lbc, total_use);

  memset (bufhe, 0, sizeof (*bufhe));

  bufhe->l_dlen =
    (uint32_t) DLEN_FROM_BUFFER_SIZE(write_size);

  bufhe->i_type    = LMD_BUF_HEADER_10_1_TYPE;
  bufhe->i_subtype = LMD_BUF_HEADER_10_1_SUBTYPE;

  bufhe->l_free[0] = 0x00000001;

  lwroc_gdf_buffer_chunks_send(LBC,
			       &lbc->fixed.filhe._buf_header,
			       sizeof (s_bufhe_host),
			       write_size - sizeof (s_bufhe_host));

  /* printf ("empty: %" PRId32 "\n", write_size); */
}

/* */

void lwroc_lmd_dump_event(lmd_event_10_1_host* ev, uint32_t size)
{
  unsigned int i;
  printf(" ----------- DUMP -----------\n");
  for (i = 0; i < size; i += 4) {
    if ((i % 32) == 0)
      printf("%06x:", i);

    printf(" %08x", *((uint32_t*) (((char *) ev) + i)));

    if ((i % 32) == 28)
      printf("\n");
  }
  printf("\n");
  printf(" --------- END DUMP ---------\n");
}


int lwroc_lmd_pipe_get_event(lwroc_pipe_buffer_consumer *pipe_buf,
			     const lwroc_thread_block *thread_block,
			     const lmd_event_10_1_host **header,
			     size_t *total_size,
			     lwroc_iterate_event *iter_data)
{
  size_t avail = 0;
  char *ptr;
  int kind = LWROC_LMD_PIPE_GET_EVENT_FAILURE;

  for ( ; ; )
    {
      /* TODO: fixme: */
      size_t hysteresis = sizeof (s_bufhe_host);

      avail = lwroc_pipe_buffer_avail_read(pipe_buf,
					   thread_block,
					   &ptr, NULL, 0,
					   hysteresis);

      if (!avail)
	return LWROC_LMD_PIPE_GET_EVENT_WOULD_BLOCK;

      break;
    }

  {
    lmd_event_10_1_host *head;
    size_t s;

    /* In principle, one should first see if enough data is available
     * for lmd_event_header_host.  But since we only handle 10,1 type
     * events, there is no need to treat that separately.
     */

    if (avail < sizeof (lmd_event_10_1_host))
      LWROC_BUG_FMT("Incomplete event header in data buffer "
		    "(%" MYPRIzd " bytes available < %" MYPRIzd ").",
		    avail, sizeof (lmd_event_10_1_host));

    head = (lmd_event_10_1_host *) ptr;

    if (head->_header.i_type    == LMD_EVENT_10_1_TYPE &&
	head->_header.i_subtype == LMD_EVENT_10_1_SUBTYPE)
      {
	kind = LWROC_LMD_PIPE_GET_EVENT_SUCCESS;
      }
    else if (head->_header.i_type    == LMD_EVENT_STICKY_TYPE &&
	     head->_header.i_subtype == LMD_EVENT_STICKY_SUBTYPE)
      {
	kind = LWROC_LMD_PIPE_GET_EVENT_STICKY;
      }
    else if (head->_header.i_type    == LWROC_LMD_IDENT_TYPE &&
	     head->_header.i_subtype == LWROC_LMD_IDENT_SUBTYPE)
      {
	/*
	  printf ("ident skip ? %d used: %" MYPRIzd "\n",
	  LBC->skip_internal_events, LBC->collect_used);
	*/
	kind = LWROC_LMD_PIPE_GET_EVENT_INTERNAL;
      }
    else
      {
	lwroc_lmd_dump_event(head,
			     44 /* FIXME */);
	LWROC_BUG_FMT("Found unknown LMD event (%d=0x%04x/%d=0x%04x).",
		      head->_header.i_type, head->_header.i_type,
		      head->_header.i_subtype, head->_header.i_subtype);
      }

    s = sizeof (lmd_event_header_host) +
      (size_t) EVENT_DATA_LENGTH_FROM_DLEN(head->_header.l_dlen);

    if (s > avail)
      LWROC_BUG_FMT("Incomplete event in data buffer "
		    "(%" MYPRIzd " bytes needed, "
		    "%" MYPRIzd " available).",
		    s, avail);

    *header     = head;
    *total_size = s;

    iter_data->_p    = (const char *) (head+1);
    iter_data->_size = s - sizeof (*head);
  }

  return kind;
}

int lwroc_lmd_get_subevent(lwroc_iterate_event *iter_data,
			   const lmd_subevent_10_1_host **subev_header,
			   lwroc_iterate_event *sub_data)
{
  const lmd_subevent_10_1_host *subev_head;
  uint32_t subev_data_length;
  uint32_t subev_size;

  if (iter_data->_size < sizeof (lmd_subevent_10_1_host))
    {
      LWROC_ERROR_FMT("Incomplete subevent header in remaining event payload "
		      "(%" MYPRIzd " bytes available < %" MYPRIzd ").",
		      iter_data->_size, sizeof (lmd_subevent_10_1_host));
      return 0;
    }

  subev_head = (const lmd_subevent_10_1_host *) iter_data->_p;

  subev_data_length =
    SUBEVENT_DATA_LENGTH_FROM_DLEN(subev_head->_header.l_dlen);

  subev_size =
    (uint32_t) sizeof (lmd_subevent_10_1_host) + subev_data_length;

  if (subev_size > iter_data->_size)
    {
      LWROC_ERROR_FMT("Subevent size larger than remaining event payload "
		      "(%" MYPRIzd " available < %" PRIu32 ").",
		      iter_data->_size, subev_size);
      return 0;
    }

  *subev_header = subev_head;

  sub_data->_p    = (const char*) (subev_head + 1);
  sub_data->_size = subev_data_length;

  iter_data->_p    += subev_size;
  iter_data->_size -= subev_size;

  return 1;
}

lwroc_gdf_format_functions _lwroc_lmd_format_functions =
{
  LWROC_DATA_TRANSPORT_FORMAT_LMD, "LMD",
  lwroc_lmd_buffer_chunks_alloc,
  lwroc_lmd_buffer_chunks_init,
  lwroc_lmd_buffer_chunks_get_fixed_raw,
  lwroc_lmd_buffer_chunks_collect,
  lwroc_lmd_buffer_chunks_file_header,
  lwroc_lmd_buffer_chunks_empty,
  LMD_BUF_DEFAULT_SIZE,
  LMD_BUF_DEFAULT_SIZE,
  4 /* runno-digits */, 4 /* fileno-digits */, 1 /* first-fileno-in-run */
};

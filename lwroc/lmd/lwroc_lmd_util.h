/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_LMD_UTIL_H__
#define __LWROC_LMD_UTIL_H__

#include "gdf/lwroc_gdf_util.h"

#include "lmd/lwroc_lmd_event.h"
#include "lmd/lwroc_lmd_sticky_store.h"

extern lwroc_gdf_format_functions _lwroc_lmd_format_functions;

/* typedef in lwroc_lmd_sticky_store.h */
struct lwroc_lmd_buffer_chunks_t
{
  struct lwroc_gdf_buffer_chunks_t _gdf;

  union
  {
    s_bufhe_host bufhe;
    s_filhe_host filhe;
    ltcp_stream_trans_open_info trans_info;
    uint32_t raw[8];
  } fixed;

};

int lwroc_lmd_buffer_chunks_collect(/*lwroc_lmd_buffer_chunks *lbc,*/
				    lwroc_gdf_buffer_chunks *lbc_gdf,
				    lwroc_pipe_buffer_consumer *pipe_buf,
				    const lwroc_thread_block *thread_block,
				    void *sticky_store,
				    void *sticky_store_drvk,
				    int partial_buffer, int for_skipping);

void lwroc_lmd_buffer_chunks_prepare_write(lwroc_lmd_buffer_chunks *lbc);

void lwroc_lmd_buffer_chunks_trans_info(/*lwroc_lmd_buffer_chunks *lbc,*/
					lwroc_gdf_buffer_chunks *lbc_gdf,
					int info_hint,
					uint32_t info_hint2);

void lwroc_lmd_buffer_chunks_bufhe(lwroc_lmd_buffer_chunks *lbc,
				   s_bufhe_host *bufhe,
				   uint32_t buffer_size, uint32_t used,
				   uint32_t events,
				   uint8_t h_end, uint8_t h_begin);

void lwroc_lmd_buffer_chunks_empty(lwroc_gdf_buffer_chunks *lbc_gdf);

/* Dump the event data in hex format.
 * Size needs to be given separately, in order to avoid
 * having to trust a potentially broken LMD header
 * */
void lwroc_lmd_dump_event(lmd_event_10_1_host *ev, uint32_t size);

/*****************************************************************************/

#define LWROC_LMD_PIPE_GET_EVENT_FAILURE       0
#define LWROC_LMD_PIPE_GET_EVENT_SUCCESS       1
#define LWROC_LMD_PIPE_GET_EVENT_STICKY        2
#define LWROC_LMD_PIPE_GET_EVENT_INTERNAL      3
#define LWROC_LMD_PIPE_GET_EVENT_WOULD_BLOCK   4

int lwroc_lmd_pipe_get_event(lwroc_pipe_buffer_consumer *pipe_buf,
			     const lwroc_thread_block *thread_block,
			     const lmd_event_10_1_host **header,
			     size_t *total_size,
			     lwroc_iterate_event *iter_data);

int lwroc_lmd_get_subevent(lwroc_iterate_event *iter_data,
			   const lmd_subevent_10_1_host **subev_header,
			   lwroc_iterate_event *sub_data);

/*****************************************************************************/

#endif/*__LWROC_LMD_UTIL_H__*/

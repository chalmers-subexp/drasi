/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2024  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include <stdio.h>
#include <string.h>

#ifndef EXT_USE_NO_DTC_ARCH
#include "../dtc_arch/acc_def/myinttypes.h"
#else
#include <inttypes.h>
#endif

#include "lwroc_accum_sort.h"

#include "lwroc_array_heap.h"

/* This buffer holds a series of items for one channel. */
struct lwroc_accum_onebuf_t
{
  uint64_t _last_ts;

  size_t   _item_sz;
  size_t   _off_first;
  size_t   _off_last;
  size_t   _n_alloc;

  int      _needs_sort;  /* Out-of-order item has been inserted. */
  size_t   _off_sort;    /* Offset of previously ordered items. */

  void    *_buf;
};

#define AB_ITEMS(ab,a,b) ((ab)->_off_##b - (ab)->_off_##a)
#define AB_PTR0(ab)      ((lwroc_accum_item_base *) \
			  ((ab)->_buf))
#define AB_PTRi(ab,i)    ((lwroc_accum_item_base *) \
			  ((ab)->_buf + (i) * (ab)->_item_sz))
#define AB_PTR(ab,a)     AB_PTRi(ab, (ab)->_off_##a)
#define AB_SIZE(ab,a,b)  (AB_ITEMS(ab,a,b) * (ab)->_item_sz)
#define AB_PTR_INC(p)    ((lwroc_accum_item_base *) \
			  (((char *) p) + (ab)->_item_sz))
#define AB_PTR_DEC(p)    ((lwroc_accum_item_base *) \
			  (((char *) p) - (ab)->_item_sz))

typedef struct lwroc_accum_heap_item_t
{
  uint32_t _i;
  uint32_t _id;
} lwroc_accum_heap_item;

/* This buffer holds several series of items for many channels. */
struct lwroc_accum_buffer_t
{
  uint32_t             _num_ids;
  lwroc_accum_onebuf  *_bufs;

  uint32_t             _type;  /* For info for callbacks. */

  uint32_t             _insert_count;

  lwroc_accum_report_ins_backwards_func  _report_ins_backwards;
  lwroc_accum_report_pick_backwards_func _report_pick_backwards;
  lwroc_accum_report_resorted_func       _report_resorted;
  lwroc_accum_process_item_func          _process_item_func;
};

#define ABUF_AB(abuf,id)  ((abuf)->_bufs + (id))

struct lwroc_accum_many_t
{
  uint32_t             _num_bufs;
  lwroc_accum_buffer **_abufs;

  lwroc_accum_heap_item *_heap;

  uint64_t             _prev_pick_ts;
};

void lwroc_accum_dump_buf(lwroc_accum_onebuf *ab);

lwroc_accum_many *
lwroc_accum_many_init(uint32_t num_bufs)
{
  lwroc_accum_many *mbuf;
  uint32_t i;

  mbuf = (lwroc_accum_many *) malloc(sizeof (lwroc_accum_many));

  if (!mbuf)
    return NULL;

  mbuf->_num_bufs = num_bufs;
  mbuf->_heap = NULL;
  mbuf->_prev_pick_ts = 0;

  mbuf->_abufs    =
    (lwroc_accum_buffer **) malloc(num_bufs * sizeof (mbuf->_abufs[0]));

  if (!mbuf->_abufs)
    {
      free(mbuf->_abufs);
      free(mbuf->_heap);
      free(mbuf);
      return NULL;
    }

  for (i = 0; i < num_bufs; i++)
    mbuf->_abufs[i] = NULL;

  return mbuf;
}

int lwroc_accum_many_post_init(lwroc_accum_many *mbuf)
{
  uint32_t total_ids = 0;
  uint32_t i;

  /* Report to user if they did not initialise all buffers. */
  for (i = 0; i < mbuf->_num_bufs; i++)
    if (!mbuf->_abufs[i])
      return 0;

  /* We must know how many channels are in each kind before we can
   * allocate the heap.
   */

  for (i = 0; i < mbuf->_num_bufs; i++)
    total_ids += mbuf->_abufs[i]->_num_ids;

  mbuf->_heap    =
    (lwroc_accum_heap_item *) malloc(total_ids * sizeof (mbuf->_heap[0]));

  if (!mbuf->_heap)
    return 0;

  return 1;
}

lwroc_accum_buffer *
lwroc_accum_init(lwroc_accum_many *mbuf, uint32_t i,
		 uint32_t type, uint32_t num_ids,
		 lwroc_accum_report_ins_backwards_func  report_ins_backw,
		 lwroc_accum_report_pick_backwards_func report_pick_backw,
		 lwroc_accum_report_resorted_func       report_resorted,
		 lwroc_accum_process_item_func          process_item_func)
{
  lwroc_accum_buffer *abuf;
  uint32_t id;

  /* Prevent user from initialising twice. */
  if (mbuf->_abufs[i])
    return NULL;

  /* We want users to care. */
  if (!report_ins_backw ||
      !report_pick_backw ||
      !report_resorted ||
      !process_item_func)
    return NULL;

  abuf = (lwroc_accum_buffer *) malloc(sizeof (lwroc_accum_buffer));

  if (!abuf)
    return NULL;

  abuf->_report_ins_backwards  = report_ins_backw;
  abuf->_report_pick_backwards = report_pick_backw;
  abuf->_report_resorted       = report_resorted;
  abuf->_process_item_func     = process_item_func;

  abuf->_type    = type;
  abuf->_num_ids = num_ids;
  abuf->_bufs    =
    (lwroc_accum_onebuf *) malloc(num_ids * sizeof (abuf->_bufs[0]));

  abuf->_insert_count = 1;

  if (!abuf->_bufs)
    {
      free(abuf->_bufs);
      free(abuf);
      return NULL;
    }

  for (id = 0; id < num_ids; id++)
    {
      lwroc_accum_onebuf *ab = ABUF_AB(abuf,id);

      memset (ab, 0, sizeof (lwroc_accum_onebuf));
    }

  mbuf->_abufs[i] = abuf;

  return abuf;
}

void lwroc_accum_realloc(lwroc_accum_onebuf *ab, size_t more)
{
  /* Need to reallocate. */
  /* If less than 3/4 used, then first move to beginning. */

  if (AB_ITEMS(ab, first, last) < (ab->_n_alloc * 3) / 4)
    {
      memmove(AB_PTR0(ab),
	      AB_PTR(ab,first),
	      AB_SIZE(ab, first, last));

      /*
	fprintf (stderr,"Move %zd (of %zd).\n",
		 ab->_off_last - ab->_off_first, ab->_n_alloc);
      */

      ab->_off_sort -= ab->_off_first;
      ab->_off_last -= ab->_off_first;
      ab->_off_first = 0;
    }

  if (ab->_off_last + more > ab->_n_alloc)
    {
      if (!ab->_n_alloc)
	ab->_n_alloc = 1024;

      while (ab->_off_last + more > ab->_n_alloc)
	ab->_n_alloc *= 2;

      ab->_buf = realloc(ab->_buf, ab->_n_alloc * ab->_item_sz);

      /* fprintf (stderr, "Realloc -> %zd.\n", ab->_n_alloc); */
    }
}

void lwroc_accum_insert_item(lwroc_accum_buffer *abuf,
			     uint32_t id,
			     lwroc_accum_item_base *item,
			     size_t sz)
{
  lwroc_accum_onebuf *ab;
  uint64_t ts;
  lwroc_accum_item_base *insert_at;

  /* We got an item for id @id with timestamp @ts. */

  if (id >= abuf->_num_ids)
    {
      fprintf (stderr,
	       "Accumulate ID (%d) outside range (max %d).\n",
	       id, abuf->_num_ids);
      exit(1);
    }

  ab = ABUF_AB(abuf,id);

  ts = item->_ts;

  if (ts < ab->_last_ts)
    {
      abuf->_report_ins_backwards(abuf->_type, id, item, ab->_last_ts);

      if (!ab->_needs_sort)
	ab->_off_sort = ab->_off_last;
      ab->_needs_sort = 1;
    }

  if (sz != ab->_item_sz)
    {
      if (ab->_item_sz)
	{
	  fprintf (stderr, "Item size confusion.\n");
	  exit(1);
	}

      ab->_item_sz = sz;
    }

  if (ab->_off_last >= ab->_n_alloc)
    lwroc_accum_realloc(ab, 1);

  insert_at =
    (lwroc_accum_item_base *) AB_PTR(ab, last);

  memcpy(insert_at, item, ab->_item_sz);

  insert_at->_insert_count = abuf->_insert_count++;

  ab->_off_last++;

  /* fprintf (stderr, "accum: %d %016" PRIx64 "\n", id, ts - ab->_last_ts); */

  ab->_last_ts = ts;
}

int lwroc_accum_compare_item_base(const void *p1, const void *p2)
{
  const lwroc_accum_item_base *item_a = (const lwroc_accum_item_base *) p1;
  const lwroc_accum_item_base *item_b = (const lwroc_accum_item_base *) p2;
  int32_t insert_count_diff;

  if (item_a->_ts < item_b->_ts)
    return -1;
  if (item_a->_ts > item_b->_ts)
    return 1;

  /* The insert count may wrap, so compare it as a signed integer. */

  insert_count_diff =
    (int32_t) (item_a->_insert_count - item_b->_insert_count);

  if (insert_count_diff < 0)
    return -1;
  if (insert_count_diff > 0)
    return 1;

  return 0;
}

uint64_t lwroc_accum_get_idx_ts(lwroc_accum_onebuf *ab, size_t i)
{
  lwroc_accum_item_base *item = (lwroc_accum_item_base *)
    AB_PTRi(ab, i);
  uint64_t item_ts = item->_ts;

  return item_ts;
}

int lwroc_accum_compare_less_idx_ab_ts(lwroc_accum_heap_item a,
				       lwroc_accum_heap_item b,
				       lwroc_accum_buffer **abufs)
{
  lwroc_accum_buffer *abuf_a = abufs[a._i];
  lwroc_accum_onebuf *ab_a = ABUF_AB(abuf_a, a._id);
  lwroc_accum_item_base *item_a = (lwroc_accum_item_base *)
    AB_PTR(ab_a, first);

  lwroc_accum_buffer *abuf_b = abufs[b._i];
  lwroc_accum_onebuf *ab_b = ABUF_AB(abuf_b, b._id);
  lwroc_accum_item_base *item_b = (lwroc_accum_item_base *)
    AB_PTR(ab_b, first);

  return lwroc_accum_compare_item_base(item_a, item_b) < 0;
}

#define LWROC_ACCUM_SORT_DUMP(x)  do {		\
    fprintf (stderr, "Sort %s : ", #x);		\
    lwroc_accum_dump_buf(ab);				\
  } while (0)

#undef LWROC_ACCUM_SORT_DUMP
#define LWROC_ACCUM_SORT_DUMP(x)  do { } while (0)

void lwroc_accum_merge_reverse(lwroc_accum_onebuf *ab,
			       lwroc_accum_item_base *l1_start,
			       lwroc_accum_item_base *l1_cur,
			       lwroc_accum_item_base *l1_end,
			       lwroc_accum_item_base *l2_start,
			       lwroc_accum_item_base *l2_cur)
{
  /*
  fprintf(stderr,
	  "Merge-sort: [%zd,%zd[;%zd[ [%zd,%zd[\n",
	  (((char *) l1_start) - (char *) ab->_buf) / ab->_item_sz,
	  (((char *) l1_cur)   - (char *) ab->_buf) / ab->_item_sz,
	  (((char *) l1_end)   - (char *) ab->_buf) / ab->_item_sz,
	  (((char *) l2_start) - (char *) ab->_buf) / ab->_item_sz,
	  (((char *) l2_cur)   - (char *) ab->_buf) / ab->_item_sz);
  */
  /* Move end pointers to point at actual items, not beyond. */
  l1_end = AB_PTR_DEC(l1_end);
  l1_cur = AB_PTR_DEC(l1_cur);
  l2_cur = AB_PTR_DEC(l2_cur);

  /* Also the pointers we compare to. */
  l1_start = AB_PTR_DEC(l1_start);
  l2_start = AB_PTR_DEC(l2_start);

  while (l1_end > l1_cur && l1_cur > l1_start)
    {
      /* When l1_end catches up with l1_cur, only items in l1 remain,
       * and need not be copied.  That should be the same as l2_cur
       * catching up with l2_start.
       */

      /*printf ("merge? %04" PRIx64 " %04" PRIx64 "\n",
	l1_cur->_ts, l2_cur->_ts);*/

      if (lwroc_accum_compare_item_base(l1_cur, l2_cur) > 0)
	{
	  /* Pick l1_cur. */

	  /*printf ("pick l1_cur\n");*/

	  memcpy(l1_end, l1_cur, ab->_item_sz);

	  l1_cur = AB_PTR_DEC(l1_cur);
	}
      else
	{
	  /* We are sorting in reverse, pick l2, is largest. */

	  /*printf ("pick l2_cur\n");*/

	  memcpy(l1_end, l2_cur, ab->_item_sz);

	  l2_cur = AB_PTR_DEC(l2_cur);
	}

      l1_end = AB_PTR_DEC(l1_end);
    }

  while (l2_cur > l2_start)
    {
      /*printf ("more l2_cur\n");*/

      memcpy(l1_end, l2_cur, ab->_item_sz);

      l2_cur = AB_PTR_DEC(l2_cur);
      l1_end = AB_PTR_DEC(l1_end);
    }
}

void lwroc_accum_check_order(lwroc_accum_onebuf *ab,
			     lwroc_accum_item_base *start,
			     lwroc_accum_item_base *end)
{
  lwroc_accum_item_base *i, *ii;

  /* Only for checking. */

  i = start;
  ii = AB_PTR_INC(i);

  for ( ; ii < end; ii = AB_PTR_INC(ii))
    {
      if (lwroc_accum_compare_item_base(i, ii) > 0)
	{
	  fprintf (stderr, "Bad order!\n");
	  exit(1);
	}
    }
}

/* When items were added out-of-order, a resort action is needed.
 * That is deferred until the array actually is used, to do it as one
 * operation, and not do insertation sort for each item.
 *
 * The basic assumption is that out-of-order items occur seldom.
 * Only due to channels having data around the same time, and getting
 * read out in not strict time order (between channels).
 *
 * State before:
 *
 * Items L0: [ ab->_off_first , ab->_off_sort [ are sorted (n0 items).
 * Items L1: [ ab->_off_sort  , ab->_off_last [ are not sorted (n1 items).
 *
 * State after:
 *
 * Items [ ab->_off_first , ab->_off_last [ are sorted (n0 + n1 items).
 *
 * Sorting is done in three stages:
 *
 * 0) Allocate spare memory for n1 items after ab->_off_last, list L2.
 *
 * 1) Go through items in L1, and move all items which are not
 *    strictly increasing to list L2.  (This will be at most n1-1
 *    items, as the first by definition is in increasing order with
 *    itself).
 *
 *    Move any items in L1 to the front, to make it compact.
 *
 * 2) Sort the (presumably short) array L2 using quicksort.
 *
 * 3) Do a merge-sort between the short L1 and L2 into L1, from behind.
 *
 *    From behind, such that the items already in L1 are not
 *    overwritten.
 *
 * 4) Find the item overlap between L0 and L1.  I.e. the last item of
 *    L1 which is smaller than the end item of L0.  Call this subset
 *    L1''.  Move all items in L1'' to L2''.
 *
 * 5) Do a merge-sort between L0 and L2'' into L0+L1''.
 *
 * Complexity:
 *
 *     1: O(n1)               (most items copied once).
 *     2: O(n1' * log(n1')).
 *     3: O(n1)               (most items copied once).
 *     4: O(n1'')
 *     5: O(n1'')
 *
 * Total: 2*O(n1) + 2*O(n1'') + O(n1' * log(n1')).
 *
 * Which, with the assumption of n1'' << n1 and n1' << n1, beats a
 * quicksort over all n1: O(n1 * log(n1))
 */

void lwroc_accum_sort_end(lwroc_accum_onebuf *ab)
{
  /* Safer (but slower):
   */

#if 0
  qsort(AB_PTR(ab, first),
	AB_ITEMS(ab, first, last), ab->_item_sz,
	lwroc_accum_compare_item_base);
#else
  /* ******************************************************************/

  size_t n_l2;

  lwroc_accum_item_base *start_l0;
  lwroc_accum_item_base *start_l1, *item_l1;
  lwroc_accum_item_base *start_l2, *item_l2;
  lwroc_accum_item_base *item_i;
  /*
  fprintf(stderr,
	  "------------------------------\n"
	  "Sort: %zd %zd (%zd %zd %zd)\n",
	  AB_ITEMS(ab, first, sort),
	  AB_ITEMS(ab, sort, last),
	  ab->_off_first, ab->_off_sort, ab->_off_last);
  */
  /* Step 0: allocate spare space. */

  lwroc_accum_realloc(ab, AB_ITEMS(ab, sort, last));
  /*
  fprintf(stderr,
	  "------------------------------\n"
	  "Sort: %zd %zd (%zd %zd %zd)\n",
	  AB_ITEMS(ab, first, sort),
	  AB_ITEMS(ab, sort, last),
	  ab->_off_first, ab->_off_sort, ab->_off_last);
  */
  LWROC_ACCUM_SORT_DUMP(0);

  /* Step 1: move away all new out-of-order items. */

  start_l1 = item_l1 = item_i = AB_PTR(ab, sort);
  start_l2 = item_l2 =          AB_PTR(ab, last);
  /*
  fprintf(stderr,
	  "URRK: [%zd,%zd[\n",
	  (((char *) start_l1) - (char *) ab->_buf) / ab->_item_sz,
	  (((char *) start_l2) - (char *) ab->_buf) / ab->_item_sz);
  */
  n_l2 = 0;

  item_i = AB_PTR_INC(item_i);

  for ( ; item_i < start_l2; item_i = AB_PTR_INC(item_i))
    {
      /* item_l1 is the last kept item in L1, i.e. in-order.
       * item_i is the item under consideration.
       */

      if (lwroc_accum_compare_item_base(item_i, item_l1) < 0)
	{
	  /* Item is out-of-order, copy to L2. */

	  /*printf ("move -> l2: %04" PRIx64 "\n", item_i->_ts);*/

	  memcpy(item_l2, item_i, ab->_item_sz);

	  item_l2 = AB_PTR_INC(item_l2);
	  n_l2++;
	}
      else
	{
	  /* Keep item in L1. */

	  /*printf ("keep in l1: %04" PRIx64 "\n", item_i->_ts);*/

	  item_l1 = AB_PTR_INC(item_l1);

	  /* Only copy if moving. */
	  if (item_i != item_l1)
	    memcpy(item_l1, item_i, ab->_item_sz);
	}
    }

  /* This pointer has been pointing to the actual last element. */
  item_l1 = AB_PTR_INC(item_l1);

  /*fprintf(stderr, "Out-of-order: %zd\n", n_l2);*/

  /* Step 2: sort the out-of-order items. */

  qsort(AB_PTR(ab, last),
	n_l2, ab->_item_sz,
	lwroc_accum_compare_item_base);

  /* Step 3: reverse merge sort. */
  /*
  fprintf(stderr,
	  "URRK: [%zd,%zd[\n",
	  (((char *) start_l1) - (char *) ab->_buf) / ab->_item_sz,
	  (((char *) start_l2) - (char *) ab->_buf) / ab->_item_sz);
  */
  lwroc_accum_merge_reverse(ab,
		      start_l1, item_l1,
		      start_l2,
		      start_l2, item_l2);

  LWROC_ACCUM_SORT_DUMP(3);

  lwroc_accum_check_order(ab, AB_PTR(ab, sort), AB_PTR(ab, last));

  /* Step 4: find first item in L1 which is beyond L0. */

  start_l0 = AB_PTR(ab, first);

  if (start_l1 > start_l0)
    {
      /* Point to last item in L0. */
      item_i = start_l1;
      item_i = AB_PTR_DEC(item_i);

      item_l1 = start_l1;
      item_l2 = start_l2;

      while (item_l1 < start_l2)
	{
	  /*printf ("chk %04" PRIx64 " vs %04" PRIx64 "\n",
	    item_l1->_ts, item_i->_ts);*/

	  if (lwroc_accum_compare_item_base(item_l1, item_i) >= 0)
	    break;

	  /*printf ("move -> l2: %04" PRIx64 "\n", item_l1->_ts);*/

	  memcpy(item_l2, item_l1, ab->_item_sz);

	  item_l1 = AB_PTR_INC(item_l1);
	  item_l2 = AB_PTR_INC(item_l2);
	}

      /* Step 5: reverse merge sort. */

      lwroc_accum_merge_reverse(ab,
			  start_l0, start_l1,
			  item_l1,
			  start_l2, item_l2);

    }

  LWROC_ACCUM_SORT_DUMP(5);

  /* Done. */
  /* ******************************************************************/
#endif

  ab->_needs_sort = 0;

  /* Check? */

#if 0
  lwroc_accum_check_order(ab, AB_PTR(ab, first), AB_PTR(ab, last));
#endif
}

void lwroc_accum_sort_until(lwroc_accum_many *mbuf,
			    uint64_t until_ts,
			    void *arg)
{
  uint32_t  i, id;
  size_t    nheap = 0;
  size_t    num_pick = 0;

  lwroc_accum_heap_item *heap = mbuf->_heap;

  /*
  fprintf (stderr, "Sort until %016" PRIx64 "\n", until_ts);
  */

  /* Find any buffers that need to be resorted. */

  for (i = 0; i < mbuf->_num_bufs; i++)
    {
      lwroc_accum_buffer *abuf = mbuf->_abufs[i];

      for (id = 0; id < abuf->_num_ids; id++)
	{
	  lwroc_accum_onebuf *ab = ABUF_AB(abuf,id);

	  if (ab->_needs_sort)
	    {
	      lwroc_accum_item_base *item;

	      lwroc_accum_sort_end(ab);

	      abuf->_report_resorted(abuf->_type, id,
				     AB_ITEMS(ab, first, last));

	      /* Make sure the last item timestamp is updated to be the
	       * really last, and not just last added, so checking is
	       * accurate.  Otherwise resort may be forgotten next round
	       * of additions.
	       */

	      item = (lwroc_accum_item_base *)
		AB_PTRi(ab, (ab->_off_last - 1));

	      ab->_last_ts = item->_ts;
	    }
	}
    }

  /* */

  /* First loop over all items to set up heap. */
  /* TODO: small improvement would be to first produce all items for
   * the heap, and then do a heapify.  But this is just a small part
   * of extracting all items.
   */

  for (i = 0; i < mbuf->_num_bufs; i++)
    {
      lwroc_accum_buffer *abuf = mbuf->_abufs[i];

      for (id = 0; id < abuf->_num_ids; id++)
	{
	  lwroc_accum_onebuf *ab = ABUF_AB(abuf,id);
	  lwroc_accum_heap_item heap_item;

	  if (ab->_off_first >= ab->_off_last)
	    continue;

	  heap_item._i  = i;
	  heap_item._id = id;

	  LWROC_HEAP_INSERT(lwroc_accum_heap_item, heap, nheap,
			    lwroc_accum_compare_less_idx_ab_ts, heap_item,
			    mbuf->_abufs);
	}
    }

  num_pick = 0;

  for ( ; ; )
    {
      /* Next item to pick is given by heap top. */

#define CHECK_HEAP_SELECTION  0

#if CHECK_HEAP_SELECTION
      uint64_t min_ts = (uint64_t) -1;
      int      min_i = -1;
      int      min_id = -1;
      uint32_t i, id;

      /* Brute force approach: loop through all items to find first item. */

      for (i = 0; i < mbuf->_num_bufs; i++)
	{
	  lwroc_accum_buffer *abuf = mbuf->_abufs[i];

	  for (id = 0; id < abuf->_num_ids; id++)
	    {
	      lwroc_accum_onebuf *ab = ABUF_AB(abuf,id);

	      if (!ab->_n_alloc)
		continue;
	      if (ab->_off_first >= ab->_off_last)
		continue;

	      lwroc_accum_item_base *item = (lwroc_accum_item_base *)
		AB_PTR(ab, first);

	      /*
	      fprintf (stderr,
		       "idx: %4d ts: %016" PRIx64 " %c\n",
		       i, item->_ts,
		       (item->_ts < min_ts) ? '*' : ' ');
	      */

	      if (item->_ts < min_ts)
		{
		  min_ts = item->_ts;
		  min_i  = i;
		  min_id = id;
		}
	    }
	}
#endif/*CHECK_HEAP_SELECTION*/

      /* (dump heap)
      for (int i = 0; i < nheap; i++)
	{
	  int idx = heap[i];
	  lwroc_accum_onebuf *ab_heap = _lwroc_accum_buffer[idx];
	  lwroc_accum_item_base *item_heap = (lwroc_accum_item_base *)
	    (ab_heap->_buf + ab_heap->_off_first * ab_heap->_item_sz);
	  uint64_t heap_ts = item_heap->_ts;

	  fprintf (stderr,
		   "i: %d idx: %4d ts: %016" PRIx64 "\n",
		   i, idx, heap_ts);
	}
      */

#if CHECK_HEAP_SELECTION
      (void) min_i;
      if ((min_id == -1) != (nheap == 0))
	{
	  fprintf (stderr, "Heap end != min_id end.\n");
	  exit(1);
	}
#endif/*CHECK_HEAP_SELECTION*/

      if (nheap == 0)
	break;

      {
      lwroc_accum_heap_item min_heap_id = heap[0];
      lwroc_accum_buffer *abuf = mbuf->_abufs[min_heap_id._i];
      lwroc_accum_onebuf *ab_heap = ABUF_AB(abuf, min_heap_id._id);
      lwroc_accum_item_base *item_heap = (lwroc_accum_item_base *)
	AB_PTR(ab_heap, first);
      uint64_t min_heap_ts = item_heap->_ts;

#if CHECK_HEAP_SELECTION
      if (min_heap_ts != min_ts)
	{
	  fprintf (stderr,
		   "Heap ts %016" PRIx64 " != min_ts %016" PRIx64 ".\n",
		   min_heap_ts,min_ts);
	  exit(1);
	}
#endif/*CHECK_HEAP_SELECTION*/

      if (min_heap_ts >= until_ts)
	break;

      /* We pick this item! */

      {
	lwroc_accum_onebuf *ab = ABUF_AB(abuf, min_heap_id._id);

	lwroc_accum_item_base *item = (lwroc_accum_item_base *)
	  AB_PTR(ab, first);

	/*

	fprintf (stderr, "Pick %2d/%4d @ %016" PRIx64 " (diff: %" PRId64 ")\n",
		 min_heap_id._i, min_heap_id._id,
		 item->_ts,
		 item->_ts - mbuf->_prev_pick_ts);
	*/

	if (item->_ts < mbuf->_prev_pick_ts)
	  {
	    abuf->_report_pick_backwards(abuf->_type, min_heap_id._id,
					 item, mbuf->_prev_pick_ts);
	  }

	abuf->_process_item_func(abuf->_type, min_heap_id._id,
				 item, arg);

	mbuf->_prev_pick_ts = item->_ts;

	ab->_off_first++;

	if (ab->_off_first >= ab->_off_last)
	  {
	    /* This index has no further items.
	     * Replace by last item form heap (and push that down).
	     */
	    heap[0] = heap[--nheap];
	  }

	LWROC_HEAP_MOVE_DOWN(lwroc_accum_heap_item, heap, nheap,
			     lwroc_accum_compare_less_idx_ab_ts, 0,
			     mbuf->_abufs);
      }
      }

      num_pick++;
    }

  /*
  fprintf (stderr, "Picked: %zd\n", num_pick);
  */
  (void) num_pick;
}


void lwroc_accum_dump_buf(lwroc_accum_onebuf *ab)
{
  size_t j;

  for (j = ab->_off_first; j < ab->_off_last; j++)
    {
      lwroc_accum_item_base *item = (lwroc_accum_item_base *)
	AB_PTRi(ab, j);

      fprintf (stderr,
	       "  %04" PRIx64 " [%5d]",
	       item->_ts, item->_insert_count);
    }

  fprintf (stderr, "\n");
}

void lwroc_accum_dump(lwroc_accum_buffer *abuf)
{
  uint32_t id;

  for (id = 0; id < abuf->_num_ids; id++)
    {
      lwroc_accum_onebuf *ab = ABUF_AB(abuf, id);

      if (ab->_off_first == ab->_off_last)
	continue;

      fprintf (stderr, "ID: %d:", id);

      lwroc_accum_dump_buf(ab);
    }
}

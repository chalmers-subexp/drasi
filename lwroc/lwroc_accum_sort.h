/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2024  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_ACCUM_SORT_H__
#define __LWROC_ACCUM_SORT_H__

#ifndef EXT_USE_NO_DTC_ARCH
#include "../dtc_arch/acc_def/mystdint.h"
#else
#include <stdint.h>
#endif
#include <stdlib.h>

typedef struct lwroc_accum_item_base_t
{
  uint64_t _ts;
  /* In case of equal timestamps, make sure items are reported in the
   * same order they were inserted.  Not needed for functionality, but
   * simplifies debugging, as results are reproducible.
   *
   * By only using 32 bits, we rely on not having a span of more then
   * 2^31 items live.
   */
  uint32_t _insert_count;
  /* Extra user variable.  Typically used to store a sub-id when an
   * accumulation buffer is used as a second stage where the various
   * items are already delivered in order.
   */
  uint32_t _aux;

} lwroc_accum_item_base;

/* The way to use the above struct to store actual data is to have a
 * first member of the struct of type lwroc_accum_item_base, like
 * (where _adc is the actual data):

typedef struct lwroc_accum_item_something_t
{
  lwroc_accum_item_base _base;
  uint32_t              _adc;
} lwroc_accum_item_something;

* End example.
*/

/* Forward declaration of opaque structure handle.  One channel. */
typedef struct lwroc_accum_onebuf_t lwroc_accum_onebuf;

/* Forward declaration.  Several channels. */
typedef struct lwroc_accum_buffer_t lwroc_accum_buffer;

/* Forward declaration.  Several kinds of channels. */
typedef struct lwroc_accum_many_t lwroc_accum_many;

/* Callback to report when item was inserted with lower timestamp. */
typedef void
(*lwroc_accum_report_ins_backwards_func)(uint32_t type, uint32_t id,
					 lwroc_accum_item_base *item,
					 uint64_t last_ts);

/* Callback to report when item was picked with lower timestamp. */
typedef void
(*lwroc_accum_report_pick_backwards_func)(uint32_t type, uint32_t id,
					  lwroc_accum_item_base *item,
					  uint64_t last_ts);

/* Callback to report when a resort was required after a series of
 * inserts with some backward.
 */
typedef void
(*lwroc_accum_report_resorted_func)(uint32_t type, uint32_t id, size_t n);

/* Callback function to process picked item after sorting. */
typedef void
(*lwroc_accum_process_item_func)(uint32_t type, uint32_t id,
				 lwroc_accum_item_base *item,
				 void *arg);

/* Initialise accumulation buffer holder.  */
lwroc_accum_many *
lwroc_accum_many_init(uint32_t num_bufs);

/* Do post-set-items init. */
int lwroc_accum_many_post_init(lwroc_accum_many *mbuf);

/* Initialise accumulation buffer.  */
lwroc_accum_buffer *
lwroc_accum_init(lwroc_accum_many *mbuf, uint32_t i,
		 uint32_t type, uint32_t num_ids,
		 lwroc_accum_report_ins_backwards_func  report_ins_backw,
		 lwroc_accum_report_pick_backwards_func report_pick_backw,
		 lwroc_accum_report_resorted_func       report_resorted,
		 lwroc_accum_process_item_func          process_item_func);

/* Insert one item into accumulation buffer. */
void lwroc_accum_insert_item(lwroc_accum_buffer *abuf,
			     uint32_t id,
			     lwroc_accum_item_base *item,
			     size_t sz);

/* Pick items from the accumulation buffer until given timestamp. */
void lwroc_accum_sort_until(lwroc_accum_many *mbuf,
			    uint64_t ts,
			    void *arg);

/* Dump content of accumulation buffer. */
void lwroc_accum_dump(lwroc_accum_buffer *abuf);

#endif/*__LWROC_ACCUM_SORT_H__*/

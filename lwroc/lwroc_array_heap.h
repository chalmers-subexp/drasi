/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_ARRAY_HEAP_H__
#define __LWROC_ARRAY_HEAP_H__

/* The heap is stored in an @array, and has @entries items, each of
 * type @item_t.  They are ordered by the @compare_less function.
 */

#define LWROC_HEAP_MOVE_DOWN(item_t, array, entries, compare_less, 	\
			     move_index, extra_ptr) do {		\
    size_t __move_to = (move_index);					\
    item_t __tmp_item;							\
    __tmp_item = array[__move_to];					\
    for ( ; ; ) {							\
      size_t __parent = __move_to;					\
      size_t __child1 = __parent * 2 + 1;				\
      size_t __child2 = __child1 + 1;					\
      if (__child1 >= (entries))					\
	break; /* Parent has no children. */				\
      if (compare_less(array[__child1], __tmp_item, extra_ptr))		\
	__move_to = __child1;						\
      if (__child2 < (entries) &&					\
	  compare_less(array[__child2], __tmp_item, extra_ptr) &&	\
	  compare_less(array[__child2], array[__child1], extra_ptr))	\
	__move_to = __child2;						\
      if (__move_to == __parent)					\
	break; /* Parent is smaller than children. */			\
      array[__parent] = array[__move_to];				\
    }									\
    /* if (__move_to != (move_index)) */				\
    /* Faster not to check when dealing with small items. */		\
    array[__move_to] = __tmp_item;					\
  } while (0)

#define LWROC_HEAP_INSERT(item_t, array, entries, compare_less, 	\
			  insert_item, extra_ptr) do {			\
    size_t __insert_at = (entries)++;					\
    while (__insert_at > 0) {						\
      size_t __parent_at = (__insert_at - 1) / 2;			\
      if (compare_less(array[__parent_at],insert_item, extra_ptr))	\
	break; /* Parent is already smaller. */				\
      array[__insert_at] = array[__parent_at];				\
      __insert_at = __parent_at;					\
    }									\
    array[__insert_at] = insert_item;					\
  } while (0)

#endif/*__LWROC_ARRAY_HEAP_H__*/

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2019  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_calc_mean_sigma.h"

#include "../dtc_arch/acc_def/mynan.h"
#include <stdio.h>

#ifndef __USE_BSD
#define __USE_BSD /* To get M_PI. */
#endif
#include <math.h> /* For sqrt(). */
#include <float.h>

int
lwroc_compare_int32_t(const void *a, const void *b)
{
  if (*((const int32_t *) a) < *((const int32_t *) b))
    return -1;
  return *((const int32_t *) a) > *((const int32_t *) b);
}

int
lwroc_calc_mean_sigma(int32_t *array, size_t n,
		      int32_t quantisation,
		      lwroc_mean_sigma *mean_sigma)
{
  size_t min_k, max_k;

  double prel_mean  = NAN;
  double prel_sigma = NAN;

  double sum;
  double sum_x;
  double sum_x2;

  int frac16;

  double mean;
  double var;

  mean_sigma->mean = NAN;
  mean_sigma->var  = NAN;

  /* Our job is to find the mean and variance of the values in the
   * @array of length @n.  We assume a Gaussian behaviour.  The main
   * issue we have is to avoid any random noise (which severely affect
   * both the mean and the variance.
   */

  /* First sort the array.
   */

  qsort (array,n,sizeof(array[0]),lwroc_compare_int32_t);

  /*
    for (int i = 0; i < n; i++)
      printf ("%6.1f ",array[i]);
    printf ("\n");
  */

  /* The trick to get rid of the noise is to remove any values which are
   * too far away.  Far, since close but wrong values are not that
   * detrimental to the determined values.  One problem is of course
   * to define far.
   */

  /* Then, assuming the data to have enough entries (i.e. @n is large
   * enough), and not too much noise to be present, we can estimate the
   * wanted variables by utilising the fact that the array we now have
   * is the 'error function' whose derivative at the peak (in the middle
   * of the distribution) will be 1/\sigma\sqrt{2\pi} * \exp(0), i.e.
   * if we just calculate the slope, we already have a pretty good
   * estimate.
   *
   * Let's use about 1/4 of the distribution, i.e. data at 5/8 and 7/8
   * to evaluate the approximations.
   *
   * An additional problem: since we deal with data which has integer
   * source we may end up with no difference at all, or really too small
   * to be significant, if the peak is really narrow.  For that case,
   * we'll revert to first attempting a larger interval, and then if
   * also that fails, assume that all data is ok, as long as the
   * difference to the next value in the array is small.  (Hmm, let's
   * not use neighbours, but rather larger intervals.)
   */

  if (n < 16)
    {
      /* printf ("CMS_TOO_LITTLE_DATA\n"); */
      return LWROC_CMS_TOO_LITTLE_DATA;
    }

  sum = 0;
  sum_x = 0;
  sum_x2 = 0;

  for (frac16 = 6; frac16 <= 15; frac16++)
    {
      double min_diff = DBL_MAX;

      size_t frac = (n * (size_t) frac16) / 16;
      size_t start;

      /* Find the steepest part, i.e. changes value the least as a
       * fixed number of items are traversed.
       */

      for (start = 0; start < n-frac; start += n/16)
	{
	  double first = array[start];
	  double last  = array[start+frac];

	  double diff = last - first;

	  if (diff < min_diff)
	    {
	      prel_mean = 0.5 * (first + last);
	      prel_sigma =
		diff / ((double) frac / (double) n) / sqrt(2 * M_PI);

	      min_diff = diff;
	    }
	}
      /*
      printf ("min_diff(%2d): %6.1f prel %6.1f +/- %6.1f\n",
	      frac16,min_diff,prel_mean,prel_sigma);
      */
      if (min_diff >= quantisation)
	goto found_prel;
    }

  {
    /* The distribution is very tight.
     *
     * We'll accept all values which are not further away
     * from a value 1/8th close to the centrum than the
     * quantization step.
     */

    size_t nstep = n/8;
    size_t i;

    for (min_k = n/2; min_k > 0; min_k--)
      if (array[min_k-1 + nstep] - array[min_k-1] > quantisation)
	break;

    for (max_k = n/2; max_k < n-1; max_k++)
      if (array[max_k+1] - array[max_k+1 - nstep] > quantisation)
	break;

    /* Then loop over the values that are accepted for calculation
     * and collect statistics.
     */

    sum = (double) (max_k - min_k);

    for (i = min_k; i < max_k; i++)
      {
	sum_x  += array[i];
	sum_x2 += array[i] * array[i];
      }

    /* printf ("tight: %d .. %d ",min_k,max_k); */

    goto calced_statistics;
  }

 found_prel:
  {
    double min_accept = prel_mean - prel_sigma * 4;
    double max_accept = prel_mean + prel_sigma * 4;
    /*
    printf ("prel %6.1f +/- %6.1f -> ( %6.1f .. %6.1f )",
	    prel_mean,prel_sigma,min_accept,max_accept);
    */
    /* First loop until we find an acceptable value. */

    for (min_k = 0; min_k < n; min_k++)
      if (array[min_k] >= min_accept)
	break;

    for (max_k = min_k; max_k < n; max_k++)
      {
	if (array[max_k] > max_accept)
	  break;

	sum_x  += array[max_k];
	sum_x2 += array[max_k] * (double) array[max_k];
      }
  }

  sum = (double) (max_k - min_k);

 calced_statistics:

  /* We are now done with the statistics calculations.
   */

  if (sum < 0.5 * (double) n)
    {
      /* printf ("CMS_TOO_MUCH_NOISE\n"); */
      return LWROC_CMS_TOO_MUCH_NOISE;
    }

  mean = sum_x / sum;
  var  = (sum_x2 - sum_x*sum_x / sum)/(sum-1);

  mean_sigma->mean = mean;
  mean_sigma->var  = var;

  /*
  printf ("--[%.0f]-> %6.1f +/- %6.1f\n", sum, mean, sqrt(var));
  */
  return LWROC_CMS_OK;
}

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2019  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_CALC_MEAN_SIGMA_H__
#define __LWROC_CALC_MEAN_SIGMA_H__

#include "../dtc_arch/acc_def/mystdint.h"
#include <stdlib.h>

#define LWROC_CMS_OK              0
#define LWROC_CMS_TOO_LITTLE_DATA 1
#define LWROC_CMS_TOO_MUCH_NOISE  2

typedef struct lwroc_mean_sigma_t
{
  double mean;
  double var;
} lwroc_mean_sigma;

int
lwroc_calc_mean_sigma(int32_t *array, size_t n,
		      int32_t quantisation,
		      lwroc_mean_sigma *mean_sigma);

#endif/*__LWROC_CALC_MEAN_SIGMA_H__*/

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2022  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_CMDLINE_FCNS_H__
#define __LWROC_CMDLINE_FCNS_H__

typedef void  (*lwroc_cmdline_usage)(void);
typedef int   (*lwroc_parse_cmdline_arg)(const char *arg);

struct lwroc_cmdline_functions
{
  lwroc_cmdline_usage             usage;
  lwroc_parse_cmdline_arg         parse_arg;
};

#endif/*__LWROC_CMDLINE_FCNS_H__*/

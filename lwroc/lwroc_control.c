/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_control.h"
#include "lwroc_message.h"

#include <stdlib.h>
#include <string.h>

#include "../dtc_arch/acc_def/myinttypes.h"
#include "../dtc_arch/acc_def/myprizd.h"

PD_LL_SENTINEL(_lwroc_controls);

lwroc_control_buffer *lwroc_alloc_control_buffer(uint32_t op, uint32_t size)
{
  size_t buffer_alloc = size +
    2 * 2 * sizeof (uint32_t) + /* for canaries (must also be aligned!) */
    2 * sizeof (uint64_t);      /* for alignment */
  size_t sz_lwroc_control_buffer =
    LWROC_ALIGN_SZ(sizeof (lwroc_control_buffer),
		   sizeof (uint64_t));
  size_t alloc = sz_lwroc_control_buffer + buffer_alloc;

  lwroc_control_buffer *ctrl;

  ctrl = (lwroc_control_buffer *) malloc (alloc);

  if (!ctrl)
    LWROC_FATAL("Failed to allocate control message buffer.");

  memset (ctrl, 0, sizeof (lwroc_control_buffer));

  ctrl->_op = op;

  /* Not including space for canaries or alignment. */
  ctrl->_buffer_size = size;
  ctrl->_data_buffer = ((char *) ctrl) + sz_lwroc_control_buffer;
  /*
  printf ("alloc ctrl: %p %p %zd %d\n",
	  ctrl, ctrl->_data_buffer, ctrl->_buffer_size, size);
  */
  /* Insert into list of controls. */

  PD_LL_ADD_BEFORE(&_lwroc_controls, &ctrl->_controls);

  return ctrl;
}

lwroc_control_buffer *lwroc_get_control_buffer(uint32_t op)
{
  pd_ll_item *iter;

  PD_LL_FOREACH(_lwroc_controls, iter)
    {
      lwroc_control_buffer *ctrl =
	PD_LL_ITEM(iter, lwroc_control_buffer, _controls);

      if (ctrl->_op == op)
	return ctrl;
    }
  return NULL;
}

void lwroc_control_done(lwroc_control_buffer *ctrl,
			const lwroc_thread_block *thread_notify_ready)
{
  if (ctrl->_response._response_data_size >
      ctrl->_request._response_data_maxsize)
    LWROC_BUG_FMT("Control performer has written "
		  "more data (%" PRIu32 ") "
		  "than allowed (%" PRIu32 ") - "
		  "buffer overflow.",
		  ctrl->_response._response_data_size,
		  ctrl->_request._response_data_maxsize);
  if (ctrl->_response_err_msg &&
      strlen(ctrl->_response_err_msg) >
      sizeof (ctrl->_response._err_msg) - 1)
    LWROC_BUG_FMT("Control performer has provided "
		  "too long error message "
		  "(%" MYPRIzd " > %" MYPRIzd ").",
		  strlen(ctrl->_response_err_msg),
		  sizeof (ctrl->_response._err_msg) - 1);
  if ((ctrl->_response._status == LWROC_CONTROL_STATUS_SUCCESS) !=
      (ctrl->_response_err_msg == NULL))
    {
      if (ctrl->_response._status == LWROC_CONTROL_STATUS_SUCCESS)
	LWROC_BUG_FMT("Control performer message response success "
		      "while having error string (%s).",
		      ctrl->_response_err_msg);
      else
	LWROC_BUG_FMT("Control performer message response non-success (%08x) "
		      "while having no error string.",
		      ctrl->_response._status);
    }

  /* Set us up as being ready to handle the next request. */
  ctrl->_thread_notify_ready = thread_notify_ready;

  /* Make sure we write the response data before updating the state. */
  SFENCE;

  ctrl->_state = LWROC_CONTROL_STATE_MESSAGE_HANDLED;

  LWROC_THREAD_BLOCK_NOTIFY(&ctrl->_thread_notify_handled);
}



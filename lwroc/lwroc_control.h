/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_CONTROL_H__
#define __LWROC_CONTROL_H__

#include "lwroc_thread_block.h"
#include "lwroc_net_proto.h"
#include "pd_linked_list.h"

#define LWROC_CONTROL_STATE_INIT             0x81
#define LWROC_CONTROL_STATE_WAIT_MESSAGE     0x02
#define LWROC_CONTROL_STATE_FILLING          0x03
#define LWROC_CONTROL_STATE_MESSAGE_READY    0x84
#define LWROC_CONTROL_STATE_MESSAGE_WORKING  0x85 /* Used by performer (user)*/
#define LWROC_CONTROL_STATE_MESSAGE_SEND     0x86 /* Used by performer (user)*/
#define LWROC_CONTROL_STATE_MESSAGE_HANDLED  0x07

#define LWROC_CONTROL_STATE_USER_ACTION_MARK 0x80 /* To avoid switch. */

/* limit against insanity (and 32-bit overflow) */
#define LWROC_CONTROL_CONTROL_LIMIT_DATA_SIZE  0x01000000

typedef struct lwroc_control_buffer_t
{
  pd_ll_item _controls;

  /* What control type do we handle? */
  uint32_t _op;

  /* Current status. */
  int _state;

  /* Thread to notify when message is ready. */
  const lwroc_thread_block *_thread_notify_ready;
  const lwroc_thread_block *_thread_notify_handled;

  /* The actual request. */
  lwroc_control_request _request;

  /* The response. */
  lwroc_control_response _response;

  /* The response error message. */
  const char *_response_err_msg;

  /* Free-format data. */
  void *_request_data;
  void *_response_data;
  uint32_t *_canaries[2];

  /* Total size of the free-format buffer. */
  size_t _buffer_size;

  /* Value of the canaries before and after the response data buffer. */
  uint32_t _canary;

  /* And some space for free-format data... */
  void *_data_buffer;
} lwroc_control_buffer;

lwroc_control_buffer *lwroc_alloc_control_buffer(uint32_t op, uint32_t size);

lwroc_control_buffer *lwroc_get_control_buffer(uint32_t op);

void lwroc_control_done(lwroc_control_buffer *ctrl,
			const lwroc_thread_block *thread_notify_ready);

#define LWROC_SET_CONTROL_STATUS(ctrl,					\
				 status, origin, err_msg) do {		\
    (ctrl)->_response._status = LWROC_CONTROL_STATUS_##status;		\
    (ctrl)->_response._origin = LWROC_CONTROL_STAT_ORIG_##origin;	\
    /* We will not modify the string. */				\
    (ctrl)->_response_err_msg = (err_msg);				\
  } while (0)

#endif/*__LWROC_CONTROL_H__*/

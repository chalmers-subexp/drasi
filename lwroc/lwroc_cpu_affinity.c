/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_cpu_affinity.h"
#include "lwroc_message.h"

#include <pthread.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <assert.h>

#include "../dtc_arch/acc_def/myprizd.h"

lwroc_cpu_set _lwroc_cpu_set = { 0, NULL };

void lwroc_cpu_set_alloc(lwroc_cpu_set *set)
{
  set->_setsize = _lwroc_cpu_set._setsize;
  set->_mask = malloc (set->_setsize);

  if (!set->_mask)
    LWROC_FATAL("Memory allocation failure (cpu_set_t).");
}

void lwroc_cpu_set_free(lwroc_cpu_set *set)
{
  free (set->_mask);
  set->_mask = NULL;
  set->_setsize = 0;
}

void lwroc_cpu_affinity_init(void)
{
#if CPU_AFFINITY_PTHREAD_AFFINITY
# if CPU_AFFINITY_PTHREAD_AFFINITY_VAR
  /* We have to (can) guess the (minimum) size of the sched_getaffinity
   * bitmask size.  Start at the glibc standard size.
   */

  int num_cpus = (int) (sizeof(cpu_set_t) * 8);
  num_cpus=1;
  for ( ; ; )
    {
      int ret;

      _lwroc_cpu_set._setsize = CPU_ALLOC_SIZE((size_t) num_cpus);

      lwroc_cpu_set_alloc(&_lwroc_cpu_set);

      ret =
	pthread_getaffinity_np(pthread_self(),
			       _lwroc_cpu_set._setsize,
			       (cpu_set_t *) _lwroc_cpu_set._mask);

      /*printf ("TRY: %d %zd %d %d\n",
	num_cpus,_lwroc_cpu_set._setsize,ret,errno); */

      if (ret == 0)
	break;

      /* For pthread_getaffinity_np the error code is in ret. */
      if (ret != EINVAL)
	{
	  errno = ret;
	  LWROC_PERROR("sched_getaffinity");
	  LWROC_FATAL("Failed to get number of CPUs.");
	}

      lwroc_cpu_set_free(&_lwroc_cpu_set);
      /* Try again, larger. */
      num_cpus *= 2;
      if (num_cpus > 1024 * 1024)
	LWROC_FATAL_FMT("Covardly refusing to query pthread_getaffinity_np "
			"with %d CPUs in set.", num_cpus);
    }
# else
  _lwroc_cpu_set._setsize = sizeof (cpu_set_t);
  lwroc_cpu_set_alloc(&_lwroc_cpu_set);
  {
    int ret =
      pthread_getaffinity_np(pthread_self(),
			     _lwroc_cpu_set._setsize,
			     (cpu_set_t *) _lwroc_cpu_set._mask);

    if (ret != 0)
      {
	errno = ret;
	LWROC_PERROR("pthread_getaffinity_np");
	LWROC_FATAL("Failed to get number of CPUs.");
      }
  }
# endif
#endif
}

#if CPU_AFFINITY_PTHREAD_AFFINITY
# if CPU_AFFINITY_PTHREAD_AFFINITY_VAR
#  define LWROC_CPU_COUNT(set)      CPU_COUNT_S(set->_setsize,		\
						(cpu_set_t *) set->_mask)
#  define LWROC_CPU_ZERO(set)       CPU_ZERO_S(set->_setsize,		\
					       (cpu_set_t *) set->_mask)
#  define LWROC_CPU_ISSET(cpu, set) CPU_ISSET_S(cpu, set->_setsize,	\
						(cpu_set_t *) set->_mask)
#  define LWROC_CPU_SET(cpu, set)   CPU_SET_S(cpu, set->_setsize,	\
					      (cpu_set_t *) set->_mask)
#  define LWROC_CPU_CLR(cpu, set)   CPU_CLR_S(cpu, set->_setsize,	\
					      (cpu_set_t *) set->_mask)
# else
#  define LWROC_CPU_COUNT(set)      CPU_COUNT(set->_mask)
#  define LWROC_CPU_ZERO(set)       CPU_ZERO(set->_mask)
#  define LWROC_CPU_ISSET(cpu, set) CPU_ISSET(cpu, (cpu_set_t *) set->_mask)
#  define LWROC_CPU_SET(cpu, set)   CPU_SET(cpu, (cpu_set_t *) set->_mask)
#  define LWROC_CPU_CLR(cpu, set)   CPU_CLR(cpu, (cpu_set_t *) set->_mask)
# endif
#endif

int lwroc_cpu_set_count(lwroc_cpu_set *set)
{
  int active_cpus = -1;

#if CPU_AFFINITY_PTHREAD_AFFINITY
# if CPU_AFFINITY_PTHREAD_AFFINITY_VAR || \
  CPU_AFFINITY_PTHREAD_AFFINITY_COUNT
  active_cpus = LWROC_CPU_COUNT(set);
# else
  size_t i;

  for (i = 0; i < set->_setsize * 8; i++)
    if (LWROC_CPU_ISSET(i, set))
      active_cpus++;
# endif
#else
  (void) set;
#endif

  return active_cpus;
}

int lwroc_num_cpus(void)
{
  return lwroc_cpu_set_count(&_lwroc_cpu_set);
}

void lwroc_cpu_set_copy(lwroc_cpu_set *dest, lwroc_cpu_set *src)
{
  assert(dest->_setsize == src->_setsize);
  memcpy(dest->_mask, src->_mask, dest->_setsize);
}

void lwroc_cpu_set_clear(lwroc_cpu_set *set, size_t i)
{
#if CPU_AFFINITY_PTHREAD_AFFINITY
  LWROC_CPU_CLR(i, set);
#else
  (void) set;
  (void) i;
#endif
}

void lwroc_cpu_set_print(lwroc_cpu_set *set)
{
#if CPU_AFFINITY_PTHREAD_AFFINITY
  size_t i;

  printf ("%zd ",
	  set->_setsize);
  for (i = 0; i < set->_setsize * 8; i++)
    printf ("%d", LWROC_CPU_ISSET(i, set));
#else
  (void) set;
#endif
}

void lwroc_cpu_set_peel_last(lwroc_cpu_set *last, lwroc_cpu_set *remain,
			     size_t *last_index)
{
  assert(last->_setsize == remain->_setsize);

#if CPU_AFFINITY_PTHREAD_AFFINITY
#if LWROC_DEBUG_THREAD_AFFINITY
  lwroc_cpu_set_print(remain);
  printf (" : last=%" MYPRIzd "\n", *last_index);
#endif

  /* Move past any empty slots. */
  while ((ssize_t) *last_index >= 0 &&
	 !LWROC_CPU_ISSET(*last_index, remain))
  (*last_index)--;

#if LWROC_DEBUG_THREAD_AFFINITY
  printf ("assign: %" MYPRIzd  "\n", *last_index);
#endif

  /* There shall be an core for us! */
  assert(*last_index != (size_t) -1);
  LWROC_CPU_ZERO(last);
  LWROC_CPU_SET(*last_index, last);

  LWROC_CPU_CLR(*last_index, remain);

  (*last_index)--;

  /* Move past any empty slots. */
  while ((ssize_t) *last_index >= 0 &&
	 !LWROC_CPU_ISSET(*last_index, remain))
    (*last_index)--;
#else
  (void) last;
  (void) remain;
  (void) last_index;
#endif
}

void lwroc_cpu_set_apply_this_thread(lwroc_cpu_set *set)
{
#if CPU_AFFINITY_PTHREAD_AFFINITY
  int ret;

  ret =
    pthread_setaffinity_np(pthread_self(),
			   set->_setsize,
			   (cpu_set_t *) set->_mask);

#if LWROC_DEBUG_THREAD_AFFINITY
  printf ("set affinity ");
  lwroc_cpu_set_print(set);
  printf (" -> %d\n", ret);
#endif

  /* For pthread_setaffinity_np the error code is in ret. */
  if (ret != 0)
    {
      errno = ret;
      LWROC_PERROR("pthread_setaffinity_np");
      LWROC_FATAL("Failed to set CPU for thread.");
    }
#else
  (void) set;
#endif
}

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_CPU_AFFINITY_H__
#define __LWROC_CPU_AFFINITY_H__

#include "../dtc_arch/acc_def/cpu_affinity.h"

#include <stddef.h>
#include <stdlib.h>

#define LWROC_DEBUG_THREAD_AFFINITY 0

typedef struct lwroc_cpu_set_t
{
  size_t  _setsize;
  void   *_mask;
} lwroc_cpu_set;

extern lwroc_cpu_set _lwroc_cpu_set;

void lwroc_cpu_affinity_init(void);

int lwroc_num_cpus(void);

void lwroc_cpu_set_alloc(lwroc_cpu_set *set);
void lwroc_cpu_set_free(lwroc_cpu_set *set);

int lwroc_cpu_set_count(lwroc_cpu_set *set);
void lwroc_cpu_set_clear(lwroc_cpu_set *set, size_t i);

void lwroc_cpu_set_copy(lwroc_cpu_set *dest, lwroc_cpu_set *src);
void lwroc_cpu_set_peel_last(lwroc_cpu_set *last, lwroc_cpu_set *remain,
			     size_t *last_index);
void lwroc_cpu_set_apply_this_thread(lwroc_cpu_set *set);

#endif/*__LWROC_CPU_AFFINITY_H__*/

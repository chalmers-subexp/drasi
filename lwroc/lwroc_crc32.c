/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2021  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_crc32.h"
#include <stddef.h>
#include "../dtc_arch/acc_def/byteorder_include.h"
#include "../dtc_arch/acc_def/byteswap_include.h"

/* Working with inverted CRC value; the initial value is 0xffffffff,
 * and after completion, all bits are inverted.  (Avoids xor'ing the
 * highest bit each cycle below.)
 *
 * One bit of data is included into the CRC calculation as:
 *
 * crc = (crc >> 1) ^ ((crc & 1) ^ bit ? poly : 0)
 *
 * - The CRC register is shifted down one bit.
 *
 * - If the least significant (shifted-out) bit of the CRC register
 *   xor'd with the input bit is one, the polynomial bits are inserted
 *   into the new register.
 */

/* D. V. Sarwate, Communications of the ACM, Vol. 31, No. 8, August 1988.
 * Computation of cyclic redundancy checks via table look-up.
 */

/* In the Sarwate implementation, the update is performed for one byte
 * at a time, using a precalculated table.
 *
 * The CRC register is downshifted all 8 bits at once.
 *
 * The table holds the combined updates due to the polynominal.
 *
 * Those updates depend on the 8 input bits, and the lowest 8 bits of
 * the CRC register, which can be combined directly by an xor
 * operation, whose result is used to index the table.
 *
 * The feedback this causes into the low 8 CRC bits is thus taken into
 * account when preparing the table.
 */

#define LWROC_CRC32_MAX_CHUNK  16

uint32_t _lwroc_crc32_table[LWROC_CRC32_MAX_CHUNK * 256];

void lwroc_crc32_table_init(void)
{
  uint32_t poly = 0xedb88320;
  uint32_t d;

  /* Prepare CRC table. */

  /* A table for each possible input of 8 bits (i.e. one byte). */

  for (d = 0; d < 256; d++)
    {
      uint32_t crc;
      int i;

      /* Prepare the table for 8 bits input data 'd'. */

      /* Calculation is performed using the single-bit update method. */
#if 0
      crc = 0;
      for (i = 0; i < 8; i++)
	{
	  uint32_t bit = (d >> i) & 1;
	  crc = (crc >> 1) ^ ((crc & 1) ^ bit ? poly : 0);
	}
#endif
      /* The above update loop is simplified in the following way:
       *
       * - Instead of getting each input bit each cycle, the bits can
       *   be 'preloaded' into the CRC register.  They will thus
       *   appear at the lowest bit in each of the 8 cycles they are
       *   used.  Any xor contribution to them is still accounted for.
       */
      crc = d;
      for (i = 0; i < 8; i++)
	{
	  crc = (crc >> 1) ^ ((crc & 1) ? poly : 0);
	}
      /* crc ^= 0xff000000; If handling had not been inverted. */

      _lwroc_crc32_table[d] = crc;
    }

  for (d = 0; d < 256; d++)
    {
      uint32_t crc;
      uint8_t idx;
      uint32_t i;

      /* What happens to entry d if i*8 bits of zero data are
       * additionally passed through the CRC check?
       * Result in entry i*256+d.
       */

      crc = _lwroc_crc32_table[d];

      for (i = 1; i < LWROC_CRC32_MAX_CHUNK; i++)
	{
	  idx = (uint8_t) crc;
	  crc = _lwroc_crc32_table[idx] ^ (crc >> 8);
	  _lwroc_crc32_table[i * 256 + d] = crc;
	}
    }
}

void lwroc_crc32_init(uint32_t *pcrc)
{
  *pcrc = 0xffffffff;
}

void lwroc_crc32_final(uint32_t *pcrc)
{
  *pcrc = ~(*pcrc);
}

/* Evaluation by slicing-by-n (n=2,4,8,16).
 *
 * M. E. Kounavis; F. L. Berry, IEEE Transactions on Computers, Vol. 57,
 * No 11, Nov. 2008, pp. 1550-1560.
 * Novel Table Lookup-Based Algorithms for High-Performance CRC Generation.
 *
 * Effectively, the previous CRC value is combined with the first four
 * bytes of the next slice and the result is used to do a table lookup
 * to see how that will propagate until the end of the slice.  The
 * update due to each remaining byte in the slice are also
 * independent, and therefore each contribution can be determined
 * separately.
 *
 * The additional cost is the need to have a separate table for each
 * distance to the end of the slice, as calculated above.
 */

/* TODO: do some test on startup / writer init to see what is the
 * maximum best chunk size.  For some old systems, 16 is too large /
 * uses too much cache.
 */

int _lwroc_crc32_chunk = 16;

#if BYTE_ORDER == LITTLE_ENDIAN
# define LWROC_CRC32_BSWAP_16(x)  (x)
# define LWROC_CRC32_BSWAP_32(x)  (x)
#else
# define LWROC_CRC32_BSWAP_16(x)  (bswap_16(x))
# define LWROC_CRC32_BSWAP_32(x)  (bswap_32(x))
#endif

void lwroc_crc32_calc(const void *data, size_t n, uint32_t *pcrc)
{
  uint32_t crc = *pcrc;
  const uint8_t *p8 = (const uint8_t *) data;
  uint8_t idx;
  int chunk;

  /* Initial 32-bit unaligned bytes. */
  while (n && (((ptrdiff_t) p8) & 3))
    {
      /* Combine the lowest 8 CRC register bits with the input to get
       * the table index.
       */
      idx = (uint8_t) ((*(p8++)) ^ crc);
      /* Update the CRC register with the table data and the
       * down-shifted register value.
       */
      crc = _lwroc_crc32_table[idx] ^ (crc >> 8);
      n--;
    }

  /* TODO: if the data size is small (perhaps < 64 bytes), it would
   * probably be beneficial to only use a smaller chunk size, as more
   * lookups are served from cache.  Also pollutes the cache less.
   *
   * With cache-lines of 64 bytes, each lookup would pull in 16 other
   * entries.  With random entries, some will request the same as
   * before.  32 -> 1, 64 -> 2, 128 -> 4, 256 -> 8, 1024 -> 16.
   */

  chunk = _lwroc_crc32_chunk;

  if (n < 1024)
    {
      int max_chunk = (int) (n / 32);

      if (max_chunk < chunk)
	chunk = max_chunk;

      /* All non-2^n cases are also handled below. */
    }

  switch (_lwroc_crc32_chunk)
    {
    case 0: case 1: case 3:
    case 2:
      {
	const uint16_t *p16;

	p16 = (const uint16_t *) p8;

	for ( ; n >= 2; n -= 2)
	  {
	    uint8_t idxa;
	    uint8_t idxb;

	    uint16_t ab;

	    ab = *(p16++);

	    ab = LWROC_CRC32_BSWAP_16(ab);

	    crc ^= ab;

	    idxa = (uint8_t) ( crc);
	    idxb = (uint8_t) ((crc >> 8));
	    crc =
	      _lwroc_crc32_table[    256 + idxa] ^
	      _lwroc_crc32_table[          idxb] ^ (crc >> 16);
	  }

	p8 = (const uint8_t *) p16;
      }
      break;
    case 5: case 6: case 7:
    case 4:
    case_4:
      {
	const uint32_t *p32;

	p32 = (const uint32_t *) p8;

	for ( ; n >= 4; n -= 4)
	  {
	    uint8_t idxa;
	    uint8_t idxb;
	    uint8_t idxc;
	    uint8_t idxd;

	    uint32_t abcd;

	    abcd = *(p32++);

	    abcd = LWROC_CRC32_BSWAP_32(abcd);

	    crc ^= abcd;

	    idxa = (uint8_t) ( crc);
	    idxb = (uint8_t) ((crc >> 8));
	    idxc = (uint8_t) ((crc >> 16));
	    idxd = (uint8_t) ((crc >> 24));
	    crc =
	      _lwroc_crc32_table[3 * 256 + idxa] ^
	      _lwroc_crc32_table[2 * 256 + idxb] ^
	      _lwroc_crc32_table[    256 + idxc] ^
	      _lwroc_crc32_table[          idxd];
	  }

	p8 = (const uint8_t *) p32;
      }
      break;
    case 9: case 10: case 11: case 12: case 13: case 14: case 15:
    case 8:
      {
	const uint32_t *p32;

	p32 = (const uint32_t *) p8;

	for ( ; n >= 8; n -= 8)
	  {
	    uint8_t idxa;
	    uint8_t idxb;
	    uint8_t idxc;
	    uint8_t idxd;
	    uint8_t idxe;
	    uint8_t idxf;
	    uint8_t idxg;
	    uint8_t idxh;

	    uint32_t abcd;
	    uint32_t efgh;

	    abcd = *(p32++);
	    efgh = *(p32++);

	    abcd = LWROC_CRC32_BSWAP_32(abcd);
	    efgh = LWROC_CRC32_BSWAP_32(efgh);

	    crc ^= abcd;

	    idxa = (uint8_t) ( crc);
	    idxb = (uint8_t) ((crc >> 8));
	    idxc = (uint8_t) ((crc >> 16));
	    idxd = (uint8_t) ((crc >> 24));
	    idxe = (uint8_t) ( efgh);
	    idxf = (uint8_t) ((efgh >> 8));
	    idxg = (uint8_t) ((efgh >> 16));
	    idxh = (uint8_t) ((efgh >> 24));
	    crc =
	      _lwroc_crc32_table[7 * 256 + idxa] ^
	      _lwroc_crc32_table[6 * 256 + idxb] ^
	      _lwroc_crc32_table[5 * 256 + idxc] ^
	      _lwroc_crc32_table[4 * 256 + idxd] ^
	      _lwroc_crc32_table[3 * 256 + idxe] ^
	      _lwroc_crc32_table[2 * 256 + idxf] ^
	      _lwroc_crc32_table[    256 + idxg] ^
	      _lwroc_crc32_table[          idxh];
	  }

	p8 = (const uint8_t *) p32;

	if (n > 4)
	  goto case_4;
      }
      break;
    default:
    case 16:
      {
	const uint32_t *p32;

	p32 = (const uint32_t *) p8;

	for ( ; n >= 16; n -= 16)
	  {
	    uint8_t idxa;
	    uint8_t idxb;
	    uint8_t idxc;
	    uint8_t idxd;
	    uint8_t idxe;
	    uint8_t idxf;
	    uint8_t idxg;
	    uint8_t idxh;
	    uint8_t idxi;
	    uint8_t idxj;
	    uint8_t idxk;
	    uint8_t idxl;
	    uint8_t idxm;
	    uint8_t idxn;
	    uint8_t idxo;
	    uint8_t idxp;

	    uint32_t abcd;
	    uint32_t efgh;
	    uint32_t ijkl;
	    uint32_t mnop;

	    abcd = *(p32++);
	    efgh = *(p32++);
	    ijkl = *(p32++);
	    mnop = *(p32++);

	    abcd = LWROC_CRC32_BSWAP_32(abcd);
	    efgh = LWROC_CRC32_BSWAP_32(efgh);
	    ijkl = LWROC_CRC32_BSWAP_32(ijkl);
	    mnop = LWROC_CRC32_BSWAP_32(mnop);

	    crc ^= abcd;

	    idxa = (uint8_t) ( crc);
	    idxb = (uint8_t) ((crc >> 8));
	    idxc = (uint8_t) ((crc >> 16));
	    idxd = (uint8_t) ((crc >> 24));
	    idxe = (uint8_t) ( efgh);
	    idxf = (uint8_t) ((efgh >> 8));
	    idxg = (uint8_t) ((efgh >> 16));
	    idxh = (uint8_t) ((efgh >> 24));
	    idxi = (uint8_t) ( ijkl);
	    idxj = (uint8_t) ((ijkl >> 8));
	    idxk = (uint8_t) ((ijkl >> 16));
	    idxl = (uint8_t) ((ijkl >> 24));
	    idxm = (uint8_t) ( mnop);
	    idxn = (uint8_t) ((mnop >> 8));
	    idxo = (uint8_t) ((mnop >> 16));
	    idxp = (uint8_t) ((mnop >> 24));
	    crc =
	      _lwroc_crc32_table[15 * 256 + idxa] ^
	      _lwroc_crc32_table[14 * 256 + idxb] ^
	      _lwroc_crc32_table[13 * 256 + idxc] ^
	      _lwroc_crc32_table[12 * 256 + idxd] ^
	      _lwroc_crc32_table[11 * 256 + idxe] ^
	      _lwroc_crc32_table[10 * 256 + idxf] ^
	      _lwroc_crc32_table[ 9 * 256 + idxg] ^
	      _lwroc_crc32_table[ 8 * 256 + idxh] ^
	      _lwroc_crc32_table[ 7 * 256 + idxi] ^
	      _lwroc_crc32_table[ 6 * 256 + idxj] ^
	      _lwroc_crc32_table[ 5 * 256 + idxk] ^
	      _lwroc_crc32_table[ 4 * 256 + idxl] ^
	      _lwroc_crc32_table[ 3 * 256 + idxm] ^
	      _lwroc_crc32_table[ 2 * 256 + idxn] ^
	      _lwroc_crc32_table[     256 + idxo] ^
	      _lwroc_crc32_table[           idxp];
	  }

	p8 = (const uint8_t *) p32;

	if (n > 4)
	  goto case_4;
      }
      break;
    }

  /* Any remaining unaligned bytes. */
  while (n)
    {
      idx = (uint8_t) ((*(p8++)) ^ crc);
      crc = _lwroc_crc32_table[idx] ^ (crc >> 8);
      n--;
    }

  *pcrc = crc;
}

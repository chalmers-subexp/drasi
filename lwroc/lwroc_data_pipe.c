/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_data_pipe.h"

#include "lwroc_message.h"
#include "lwroc_main_iface.h"
#include "lwroc_net_proto.h"

#include "gdf/lwroc_gdf_util.h"

#include <string.h>
#include <assert.h>

#include "../dtc_arch/acc_def/myinttypes.h"
#include "../dtc_arch/acc_def/myprizd.h"

struct lwroc_data_pipe_handle_t
{
  pd_ll_item _data_pipes;

  /* Our name. */
  char *_name;

  /* The actual pipe of data. */
  lwroc_pipe_buffer_control *_ctrl;

  /* The thread which should do blocking on write access. */
  const lwroc_thread_block *_thread_block_write;

  /* Format of the data. */
  lwroc_gdf_format_functions *_fmt;

  /* The maximally allowed event size. */
  /* This is advisory info, mainly checked by our users. */
  uint32_t _max_ev_len;

  /* The maximum time between events (due to delayed EB holding,
   * and flush delays.  Waited for, before TS disabled.)  -1 = indefinite.
   */
  int _max_event_interval;

  /* Monitoring that the max_event_interval has not been exceeded. */
  uint64_t _last_events;
  time_t   _last_events_time;
  uint64_t _last_events_report;

  /* A dependent data format handler. */
  void *_extra;

  /* Ongoing request. */
  uint32_t  _req_size;
  uint32_t *_req_space_end;
  uint32_t  _req_space_end_canary;

  /* Where to put a marker that we are blocking due to buffer space. */
  volatile uint32_t *_readout_status_ptr;
  uint32_t _readout_status_mark_wait_buf_space;

  /* Extra data block in the pipe buffer. */
  lwroc_data_pipe_extra *_pipe_extra;

  /* Monitor info. */
  uint64_t *_mon_events_ptr;
  uint64_t *_mon_bytes_ptr;
};

PD_LL_SENTINEL(_lwroc_data_pipes);

lwroc_data_pipe_handle *
lwroc_data_pipe_init(const char *name,
		     lwroc_pipe_buffer_control *ctrl,
		     const lwroc_thread_block *thread_block_write,
		     lwroc_gdf_format_functions *fmt,
		     uint32_t max_ev_len,
		     uint64_t *mon_events_ptr,
		     uint64_t *mon_bytes_ptr)
{
  lwroc_data_pipe_handle *handle;

  handle = (lwroc_data_pipe_handle *)
    malloc (sizeof (lwroc_data_pipe_handle));

  if (handle == NULL)
    LWROC_FATAL("Memory allocation failure (data pipe info).");

  PD_LL_ADD_BEFORE(&_lwroc_data_pipes, &handle->_data_pipes);

  handle->_name = strdup_chk(name);

  handle->_ctrl = ctrl;
  handle->_thread_block_write = thread_block_write;

  assert(fmt);
  handle->_fmt = fmt;

  handle->_max_ev_len = max_ev_len;

  handle->_max_event_interval = 0;

  handle->_last_events = 0;
  handle->_last_events_time = time(NULL);
  handle->_last_events_report = (uint64_t) -1;

  handle->_extra = NULL;

  handle->_req_size = 0;
  handle->_req_space_end = NULL;
  handle->_req_space_end_canary = 1;

  handle->_readout_status_ptr = NULL;
  handle->_readout_status_mark_wait_buf_space = 0;

  handle->_pipe_extra =
    (lwroc_data_pipe_extra *) lwroc_pipe_buffer_get_extra(ctrl);

  handle->_mon_events_ptr = mon_events_ptr;
  handle->_mon_bytes_ptr  = mon_bytes_ptr;

  assert(sizeof (lwroc_data_pipe_extra) <=
	 LWROC_PIPE_BUFFER_PROD_EXTRA_UINT64_WORDS * sizeof (uint64_t));

  LWROC_INFO_FMT("Data buffer %s, fmt %s, "
		 "size %" MYPRIzd " = 0x%08" MYPRIzx ", "
		 "%d consumers.",
		 name, fmt->_name,
		 lwroc_pipe_buffer_size(ctrl),
		 lwroc_pipe_buffer_size(ctrl),
		 lwroc_pipe_buffer_num_consumers(ctrl));

  return handle;
}

lwroc_data_pipe_handle *lwroc_get_data_pipe(const char *name)
{
  pd_ll_item *iter;

  PD_LL_FOREACH(_lwroc_data_pipes, iter)
    {
      lwroc_data_pipe_handle *handle =
	PD_LL_ITEM(iter, lwroc_data_pipe_handle, _data_pipes);

      if (strcmp(handle->_name, name) == 0)
	return handle;
    }
  return NULL;
}

void lwroc_data_pipe_set_readout_notify(lwroc_data_pipe_handle *handle,
					volatile uint32_t *ptr,
					uint32_t mark_wait_buf_space)
{
  handle->_readout_status_ptr = ptr;
  handle->_readout_status_mark_wait_buf_space = mark_wait_buf_space;
}

uint32_t lwroc_data_pipe_get_max_ev_len(lwroc_data_pipe_handle *handle)
{
  return handle->_max_ev_len;
}

void lwroc_data_pipe_set_max_ev_interval(lwroc_data_pipe_handle *handle,
					 int interval)
{
  handle->_max_event_interval = interval;
}

int lwroc_data_pipe_get_max_ev_interval(lwroc_data_pipe_handle *handle)
{
  return handle->_max_event_interval;
}

void *lwroc_data_pipe_get_extra(lwroc_data_pipe_handle *handle)
{
  return handle->_extra;
}

void lwroc_data_pipe_set_extra(lwroc_data_pipe_handle *handle, void *extra)
{
  handle->_extra = extra;
}

lwroc_data_pipe_extra*
lwroc_data_pipe_get_pipe_extra(lwroc_data_pipe_handle *handle)
{
  return handle->_pipe_extra;
}

void lwroc_data_pipe_extra_clear(lwroc_data_pipe_extra *pipe_extra)
{
  pipe_extra->_events = 0;
  pipe_extra->_bytes = 0;
}

lwroc_pipe_buffer_control*
lwroc_data_pipe_get_pipe_ctrl(lwroc_data_pipe_handle *handle)
{
  return handle->_ctrl;
}

lwroc_gdf_format_functions *
lwroc_data_pipe_get_fmt(lwroc_data_pipe_handle *handle)
{
  return handle->_fmt;
}

char *lwroc_request_event_space(lwroc_data_pipe_handle *handle,
				uint32_t size)
{
  char *buf;
  uint32_t request;

  /* Space for canary value. */
  request = size + (uint32_t) sizeof (uint32_t);

  /* Make sure any triva control master may know why we are blocked. */
  /* TODO: make this part of pipe such that only set when really waiting. */
  if (handle->_readout_status_ptr)
    *handle->_readout_status_ptr |= handle->_readout_status_mark_wait_buf_space;

  buf = lwroc_pipe_buffer_alloc_write(handle->_ctrl, request, NULL,
				      handle->_thread_block_write, 1);

  if (handle->_readout_status_ptr)
    *handle->_readout_status_ptr &= ~handle->_readout_status_mark_wait_buf_space;

  assert (buf);

  handle->_req_size = size;
  handle->_req_space_end_canary =
    handle->_req_space_end_canary * 1103515245 + 12345;
  handle->_req_space_end = (uint32_t *) (buf + size);
  *handle->_req_space_end = handle->_req_space_end_canary;

  return buf;
}

void lwroc_used_event_space(lwroc_data_pipe_handle *handle,
			    uint32_t size, int no_hysteresis)
{
  if (handle->_req_space_end == NULL)
    LWROC_BUG_FMT("Used event space (%" PRIu32 ") reported "
		  "without previous request.",
		  size);
  if (size > handle->_req_size)
    LWROC_BUG_FMT("Used event space (%" PRIu32 ") larger "
		  "than requested (%" PRIu32 ").",
		  size, handle->_req_size);
  handle->_req_size = 0;
  if (*handle->_req_space_end != handle->_req_space_end_canary)
    LWROC_BUG_FMT("Event buffer overflow!  "
		  "Unexpected canary (0x%" PRIx32 " != 0x%" PRIx32 ").",
		  *handle->_req_space_end,
		  handle->_req_space_end_canary);
  *handle->_req_space_end = 0;
  handle->_req_space_end = NULL;

  lwroc_pipe_buffer_did_write(handle->_ctrl, size, no_hysteresis);

  /* TODO: is it not enough with the accounting in handle->_pipe_extra? */
  (*handle->_mon_events_ptr)++;
  (*handle->_mon_bytes_ptr) += size;

  handle->_pipe_extra->_events++;
  handle->_pipe_extra->_bytes += size;
}

/* Since when do we expect to see new events, i.e. since when is
 * acquisition (or merging) active.  0 if stopped.
 *
 * Changed from main thread, read by network thread, so volatile.
 */
volatile time_t _lwroc_readout_active_since = 0;

void lwroc_data_pipes_readout_active(int active)
{
  if (active)
    {
      /* If already active, do not reset the time. */
      if (!_lwroc_readout_active_since)
	_lwroc_readout_active_since = time(NULL);
    }
  else
    _lwroc_readout_active_since = 0;
}

void lwroc_data_pipes_check_max_ev_interval(struct timeval *now)
{
  pd_ll_item *iter;

  PD_LL_FOREACH(_lwroc_data_pipes, iter)
    {
      lwroc_data_pipe_handle *handle =
	PD_LL_ITEM(iter, lwroc_data_pipe_handle, _data_pipes);

      if (handle->_max_event_interval)
	{
	  uint64_t events;

	  events = handle->_pipe_extra->_events;

	  if (events != handle->_last_events)
	    {
	      handle->_last_events = events;
	      handle->_last_events_time = now->tv_sec;
	    }
	  else
	    {
	      time_t waited_ev;
	      time_t active_time;

	      waited_ev   = now->tv_sec - handle->_last_events_time;
	      active_time = now->tv_sec - _lwroc_readout_active_since;

	      if (events != handle->_last_events_report &&
		  _lwroc_readout_active_since &&
		  waited_ev   > (time_t) handle->_max_event_interval &&
		  active_time > (time_t) handle->_max_event_interval &&
		  !lwroc_pipe_buffer_blocked(handle->_ctrl))
		{
		  LWROC_ERROR_FMT("Data buffer %s has seen no new event "
				  "for %d s "
				  "(promised %d s by max-event-interval).",
				  handle->_name,
				  (int) waited_ev,
				  handle->_max_event_interval);

		  handle->_last_events_report = events;
		}
	    }
	}
    }
}

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_DATA_PIPE_H__
#define __LWROC_DATA_PIPE_H__

#include "lwroc_pipe_buffer.h"
#include "lwroc_thread_util.h"
#include "lwroc_filters.h"

/********************************************************************/

typedef struct lwroc_data_pipe_handle_t lwroc_data_pipe_handle;

/********************************************************************/

typedef struct lwroc_gdf_format_functions_t lwroc_gdf_format_functions;

/********************************************************************/

/* Structure to pass extra data from the data producer to the drasi
 * instance in the shared memory block provided by the pipe.  Thus we
 * need not set up an extra shared memory segment.
 */
typedef struct lwroc_data_pipe_extra_t
{
  uint64_t _events;
  uint64_t _bytes;
} lwroc_data_pipe_extra;

/********************************************************************/

extern lwroc_thread_instance *_lwroc_main_thread;
extern struct lwroc_monitor_main_block_t _lwroc_mon_main;

/********************************************************************/

#define LWROC_MAP_TYPE_PLAIN       0x01
#define LWROC_MAP_TYPE_DIRECT      0x02
#define LWROC_MAP_TYPE_MMAP_PHYS   0x04
#define LWROC_MAP_TYPE_MMAP_PEXOR  0x08
#define LWROC_MAP_TYPE_VALLOC      0x10

typedef struct lwroc_data_pipe_cfg_t
{
  size_t _map_size;    /* Total size of (mapped (or not)) buffer memory. */
  size_t _buf_size;    /* Main (readout/merge) buffer size. */
  size_t _input_size;  /* just holds -2 when inputfrac is given */
  double _input_frac;
  size_t _filter_size[LWROC_FILTER_NUM]; /* -2 when filterfrac is given */
  double _filter_frac[LWROC_FILTER_NUM];
  size_t _phys_offset;
  int    _map_type;
  int    _nohint;

} lwroc_data_pipe_cfg;

void lwroc_data_pipe_parse_cfg(lwroc_data_pipe_cfg *pipe_cfg,
			       const char *cfg);
void lwroc_data_pipe_post_parse(lwroc_data_pipe_cfg *pipe_cfg);

typedef struct lwroc_data_pipe_map_t
{
  void   *_buf_ptr;
  size_t  _buf_left;
} lwroc_data_pipe_map_info;

void lwroc_data_pipe_map(lwroc_data_pipe_map_info *map,
			 lwroc_data_pipe_cfg *pipe_cfg);

lwroc_pipe_buffer_control *
lwroc_data_pipe_alloc(lwroc_data_pipe_map_info *map,
		      lwroc_data_pipe_cfg *pipe_cfg,
		      int consumers,
		      int filter_no /* readout: -1 */);

/********************************************************************/

ssize_t lwroc_get_data_pipe_phys_offset(void);

int lwroc_data_pipe_is_phys_pexor(void);

lwroc_data_pipe_handle *
lwroc_data_pipe_init(const char *name,
		     lwroc_pipe_buffer_control *ctrl,
		     const lwroc_thread_block *thread_block_write,
		     lwroc_gdf_format_functions *fmt,
		     uint32_t max_ev_len,
		     uint64_t *mon_events_ptr,
		     uint64_t *mon_bytes_ptr);

lwroc_data_pipe_handle *lwroc_get_data_pipe(const char *name);

void lwroc_data_pipe_set_readout_notify(lwroc_data_pipe_handle *handle,
					volatile uint32_t *ptr,
					uint32_t mark_wait_buf_space);

uint32_t lwroc_data_pipe_get_max_ev_len(lwroc_data_pipe_handle *handle);

void lwroc_data_pipe_set_max_ev_interval(lwroc_data_pipe_handle *handle,
					 int interval);
int lwroc_data_pipe_get_max_ev_interval(lwroc_data_pipe_handle *handle);

void *lwroc_data_pipe_get_extra(lwroc_data_pipe_handle *handle);
void lwroc_data_pipe_set_extra(lwroc_data_pipe_handle *handle, void *extra);

lwroc_data_pipe_extra*
lwroc_data_pipe_get_pipe_extra(lwroc_data_pipe_handle *handle);
void lwroc_data_pipe_extra_clear(lwroc_data_pipe_extra *pipe_extra);

lwroc_pipe_buffer_control*
lwroc_data_pipe_get_pipe_ctrl(lwroc_data_pipe_handle *handle);

lwroc_gdf_format_functions *
lwroc_data_pipe_get_fmt(lwroc_data_pipe_handle *handle);

char *lwroc_request_event_space(lwroc_data_pipe_handle *handle,
				uint32_t size);

void lwroc_used_event_space(lwroc_data_pipe_handle *handle,
			    uint32_t size, int no_hysteresis);

/********************************************************************/

void lwroc_data_pipes_readout_active(int active);

void lwroc_data_pipes_check_max_ev_interval(struct timeval *now);

/********************************************************************/

#endif/*__LWROC_DATA_PIPE_H__*/

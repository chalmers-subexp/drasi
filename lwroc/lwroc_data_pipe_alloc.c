/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_data_pipe.h"
#include "lwroc_message.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <assert.h>
#include <string.h>
#include "lwroc_parse_util.h"
#include "lwroc_map_pexor.h"
#include "lwroc_main_iface.h"

#include "../dtc_arch/acc_def/has_valloc.h"
#include "../dtc_arch/acc_def/myinttypes.h"
#include "../dtc_arch/acc_def/myprizd.h"

#define LWROC_MIN_BUF_SIZE   0x10000

ssize_t _lwroc_data_pipe_phys_offset = -1;
int _lwroc_data_pipe_phys_pexor = 0;

void lwroc_data_pipe_usage(void)
{
  printf ("\n");
  printf ("--buf=<OPTIONS>:\n");
  printf ("\n");
  printf ("  size=N                   Size of data buffer (Mi|raw hex).\n");
  printf ("  nohint                   Do not hint about small buffer size.\n");
  printf ("  valloc                   Aligned memory allocation (for CES RIO4 Linux).\n");
  printf ("  direct@OFF               Direct mapping memory offset.\n");
  printf ("  physmmap@OFF             Physical mapping memory offset.\n");
  printf ("  physpexor@OFF            Physical mapping memory offset (via PEXOR driver).\n");
  /* Note! %% oinly uses one char, so below is aligned. */
  printf ("  input=N|frac|%%           Size or fraction to use for merge input buffers\n");
  printf ("                           (def 75%%).\n");
  printf ("  filter=N|frac|%%          Size or fraction (excl. input buffers) to use for\n");
  printf ("                           (optional) filter output buffer (def 50%%).\n");
  printf ("\n");
}

size_t lwroc_get_proc_memtotal(void)
{
  FILE *fid = fopen("/proc/meminfo","r");
  long memtotal = 0;
  int n;

  if (!fid)
    return 0;

  n = fscanf (fid, "MemTotal: %ld kB ", &memtotal);

  fclose(fid);

  return (n == 1) ? (size_t) (memtotal) * 1024: 0;
}

void lwroc_data_pipe_parse_cfg(lwroc_data_pipe_cfg *pipe_cfg,
			       const char *cfg)
{
  const char *request;
  lwroc_parse_split_list parse_info;

  memset(pipe_cfg, 0, sizeof (*pipe_cfg));

  lwroc_parse_split_list_setup(&parse_info, cfg, ',');

  while ((request = lwroc_parse_split_list_next(&parse_info)) != NULL)
    {
      const char *post;

      if (LWROC_MATCH_C_ARG("help"))
	{
	  lwroc_data_pipe_usage();
	  exit(0);
	}
      else if (LWROC_MATCH_C_PREFIX("size=",post))
	{
	  pipe_cfg->_map_size =
	    (size_t) lwroc_parse_hex(post, "buffer size");
	}
      else if (LWROC_MATCH_C_ARG("nohint"))
	{
	  pipe_cfg->_nohint = 1;
	}
      else if (LWROC_MATCH_C_PREFIX("input=",post))
	{
	  pipe_cfg->_input_size =
	    (size_t) lwroc_parse_hex_double(post, "input fraction",
					    &pipe_cfg->_input_frac, 0);
	}
      else if (LWROC_MATCH_C_PREFIX("filter=",post))
	{
	  pipe_cfg->_filter_size[LWROC_FILTER_USER] =
	    (size_t) lwroc_parse_hex_double(post, "filter fraction",
					    &pipe_cfg->_filter_frac
					    /**/[LWROC_FILTER_USER], 0);
	}
      else if (LWROC_MATCH_C_ARG("valloc"))
	{
#if !HAS_VALLOC
	  LWROC_BADCFG("valloc() not found, not compiled.");
#endif
	  pipe_cfg->_map_type = LWROC_MAP_TYPE_VALLOC;
	}
      else if (LWROC_MATCH_C_PREFIX("direct@",post))
	{
	  pipe_cfg->_phys_offset =
	    (size_t) lwroc_parse_hex(post, "direct map offset");
	  pipe_cfg->_map_type = LWROC_MAP_TYPE_DIRECT;
	}
      else if (LWROC_MATCH_C_PREFIX("physmmap@",post))
	{
	  pipe_cfg->_phys_offset =
	    (size_t) lwroc_parse_hex(post, "phys mmap offset");
	  pipe_cfg->_map_type = LWROC_MAP_TYPE_MMAP_PHYS;
	}
      else if (LWROC_MATCH_C_PREFIX("physpexor@",post))
	{
	  pipe_cfg->_phys_offset =
	    (size_t) lwroc_parse_hex(post, "phys pexor offset");
	  pipe_cfg->_map_type = LWROC_MAP_TYPE_MMAP_PEXOR;
	}
      else
	LWROC_BADCFG_FMT("Unrecognised buf option: %s", request);
    }

  if (!pipe_cfg->_map_type)
    pipe_cfg->_map_type = LWROC_MAP_TYPE_PLAIN;

  if (pipe_cfg->_map_type != LWROC_MAP_TYPE_PLAIN &&
      pipe_cfg->_map_type != LWROC_MAP_TYPE_VALLOC &&
      pipe_cfg->_map_type != LWROC_MAP_TYPE_DIRECT &&
      pipe_cfg->_map_type != LWROC_MAP_TYPE_MMAP_PHYS &&
      pipe_cfg->_map_type != LWROC_MAP_TYPE_MMAP_PEXOR)
    LWROC_BADCFG("Multiple methods to map physical memory given.");

  if ((pipe_cfg->_map_type & (LWROC_MAP_TYPE_DIRECT |
			      LWROC_MAP_TYPE_MMAP_PHYS |
			      LWROC_MAP_TYPE_MMAP_PEXOR)) &&
      !pipe_cfg->_phys_offset)
    LWROC_BADCFG("Refusing pipe map at physical offset 0.");

  /* This might be overridden (reduced) by the merge_in parsing,
   * and by filters.
   */
  pipe_cfg->_buf_size = pipe_cfg->_map_size;

  /* Further calculations and checks in lwroc_data_pipe_parse_cfg,
   * after the input sources have been parsed and allocated space for.
   */
}

void lwroc_data_pipe_post_parse(lwroc_data_pipe_cfg *pipe_cfg)
{
  int fltno;
  size_t bufsize;

  /* Continued checks, after merge source input buffers have been
   * handled.
   */

  if (pipe_cfg->_map_type & LWROC_MAP_TYPE_PLAIN)
    {
      size_t memtotal = lwroc_get_proc_memtotal();

      if (memtotal)
	{
	  size_t suggest = (memtotal * 3) / 4;
	  size_t pow2;

	  for (pow2 = 1; pow2 < suggest && pow2; pow2 <<= 1)
	    ;
	  suggest &=
	    pow2 | (pow2 >> 1) | (pow2 >> 2) | (pow2 >> 3) | (pow2 >> 4);

	  if (!pipe_cfg->_map_size)
	    LWROC_BADCFG_FMT("Data buffer size not given, "
			     "for plain allocation a 75%% suggestion based on "
			     "MemTotal of /proc/meminfo (%" MYPRIzd "MiB) "
			     "is %" MYPRIzd "Mi.",
			     memtotal >> 20, suggest >> 20);
	  else if (!pipe_cfg->_nohint &&
		   pipe_cfg->_map_size < memtotal / 4)
	    LWROC_WARNING_FMT("Small data buffer size %" MYPRIzd "MiB, "
			      "less than 25%% of "
			      "MemTotal of /proc/meminfo (%" MYPRIzd "MiB), "
			      "which is %" MYPRIzd "Mi.  "
			      "75%% suggestion is %" MYPRIzd "Mi.",
			      pipe_cfg->_map_size >> 20,
			      memtotal >> 20, (memtotal / 4) >> 20,
			      suggest >> 20);
	}
    }

  /* The size from which we calculate the filter fractions. */
  bufsize = pipe_cfg->_buf_size;

  for (fltno = 0; fltno < LWROC_FILTER_NUM; fltno++)
    {
      if (_config._use_filter[fltno])
	{
	  double filter_frac;

	  if (pipe_cfg->_filter_size[fltno] == (size_t) -2)
	    {
	      filter_frac = pipe_cfg->_filter_frac[fltno];
	    }
	  else if (pipe_cfg->_filter_size[fltno] != 0)
	    {
	      goto filter_size_set;
	    }
	  else
	    {
	      /* Unless explicit size give, we assume 50 %. */
	      filter_frac = 0.5;
	    }

	  /* Fractional space is relative to the buffer. */

	  pipe_cfg->_filter_size[fltno] =
	    (size_t) (((double) bufsize) * filter_frac);

	filter_size_set:
	  /* Either it was given explicitly, or as a fraction, we have a
	   * minimum requirement.
	   */
	  if (pipe_cfg->_filter_size[fltno] < LWROC_MIN_BUF_SIZE)
	    LWROC_BADCFG_FMT("Refusing filter (%d) "
			     "buffer size %" MYPRIzd " < %d.",
			     fltno,
			     pipe_cfg->_filter_size[fltno], LWROC_MIN_BUF_SIZE);

	  /* Check that we do not ask for too much. */
	  if (pipe_cfg->_filter_size[fltno] >= pipe_cfg->_buf_size)
	    LWROC_BADCFG_FMT("Filter (%d) buffer size %" MYPRIzd " larger than "
			     "remaining buffer size %" MYPRIzd ".",
			     fltno,
			     pipe_cfg->_filter_size[fltno],
			     pipe_cfg->_buf_size);

	  /* Subtract the sizes from the buffer size. */
	  pipe_cfg->_buf_size -= pipe_cfg->_filter_size[fltno];
	}
      else
	pipe_cfg->_filter_size[fltno] = 0;
    }

  /* Check the remaining buffer size. */

  if (pipe_cfg->_buf_size < LWROC_MIN_BUF_SIZE)
    LWROC_BADCFG_FMT("Refusing data buffer size %" MYPRIzd " < %d.",
		     pipe_cfg->_buf_size, LWROC_MIN_BUF_SIZE);
}

void lwroc_data_pipe_map(lwroc_data_pipe_map_info *map,
			 lwroc_data_pipe_cfg *pipe_cfg)
{
  map->_buf_ptr = NULL;
  map->_buf_left = 0;

  if (pipe_cfg->_map_type & LWROC_MAP_TYPE_VALLOC)
    {
#if HAS_VALLOC
      map->_buf_ptr = valloc(pipe_cfg->_map_size);
#else
      LWROC_FATAL("valloc() for pipe buffer not available.");
#endif

      if (map->_buf_ptr == NULL)
	{
	  LWROC_PERROR("valloc");
	  LWROC_BADCFG_FMT("Failed to valloc memory, "
			   "size 0x%08" MYPRIzx ".",
			   pipe_cfg->_map_size);
	}
    }

  if (pipe_cfg->_map_type & LWROC_MAP_TYPE_DIRECT)
    {
      map->_buf_ptr = (void *) pipe_cfg->_phys_offset;
    }

  if (pipe_cfg->_map_type & (LWROC_MAP_TYPE_MMAP_PHYS |
			     LWROC_MAP_TYPE_MMAP_PEXOR))
    {
      int fd_mem;
      void *p;

      if (pipe_cfg->_map_type & LWROC_MAP_TYPE_MMAP_PHYS)
	{
	  const char *filename = "/dev/mem";

	  fd_mem = open(filename, O_RDWR | O_SYNC);
	  if (fd_mem == -1)
	    {
	      LWROC_PERROR("open");
	      LWROC_BADCFG_FMT("Failed to open '%s' to mmap physical memory.",
			       filename);
	    }
	}
      else
	{
	  assert (pipe_cfg->_map_type & LWROC_MAP_TYPE_MMAP_PEXOR);

	  fd_mem = lwroc_get_pexor_fd();

	  /* Mmaping at offset 0 refused (above), as that would map
	   * pexor I/O resources instead.
	   */
	}

      p = mmap(0, pipe_cfg->_map_size,
	       PROT_READ | PROT_WRITE,
	       MAP_SHARED,
	       fd_mem, (off_t) pipe_cfg->_phys_offset);
      if (p == MAP_FAILED)
	{
	  LWROC_PERROR("mmap");
	  LWROC_BADCFG_FMT("Failed to mmap physical memory "
			   "at offset 0x%08" MYPRIzx ", "
			   "length 0x%08" MYPRIzx ".",
			   pipe_cfg->_phys_offset, pipe_cfg->_map_size);
	}

      map->_buf_ptr = p;
    }

  if (map->_buf_ptr)
    {
      volatile uint32_t *p;
      uint32_t keep[32];
      uint32_t expect;
      int i;
      size_t off, step, last;
      size_t failoffset = (size_t) -1;
      int failed = 0;

      /* All routines that mapped, did map this size. */
      map->_buf_left = pipe_cfg->_map_size;

      LWROC_INFO_FMT("Testing physically mapped data pipe "
		     "@ 0x%08" MYPRIzx ", "
		     "length 0x%08" MYPRIzx ", "
		     "virt@ 0x%08" MYPRIzx ".",
		     pipe_cfg->_phys_offset,
		     pipe_cfg->_map_size, (size_t) map->_buf_ptr);

      /* We do a simple check to verify that the physically mapped
       * region actually behaves like memory.
       *
       * Test by doing a 'memtest', read, write-read, restore(write),
       * at some 32 locations.
       */

      p = map->_buf_ptr;
      step = pipe_cfg->_map_size / sizeof (uint32_t) / 31;
      last = pipe_cfg->_map_size / sizeof (uint32_t) - 1;

      /*
      LWROC_INFO_FMT("0x%08x 0x%08x 0x%08x 0x%08x 0x%08x 0x%08x ",
		     p[0],p[1],p[2],p[3],p[4],p[5]);
      */

      /* Read values to temporary store. */
      for (i = 0; i < 32; i++)
	{
	  off = (i == 31 ? last : (size_t) i * step);
	  keep[i] = p[off];
	}
      /* Write some values. */
      for (i = 0; i < 32; i++)
	{
	  off = (i == 31 ? last : (size_t) i * step);
	  expect = (uint32_t) (1 << i) ^ 0x00123400;
	  p[off] = expect;
	}
      /* Read check the values. */
      for (i = 0; i < 32; i++)
	{
	  off = (i == 31 ? last : (size_t) i * step);
	  expect = (uint32_t) (1 << i) ^ 0x00123400;
	  if (p[off] != expect)
	    {
	      if (!failed)
		failoffset = off;
	      failed++;
	    }
	}
      /* Restore values from temporary store. */
      for (i = 0; i < 32; i++)
	{
	  off = (i == 31 ? last : (size_t) i * step);
	  p[off] = keep[i];
	}

      if (failed)
	LWROC_BADCFG_FMT("Data pipe memory failed (%d/%d) read-back, "
			 "first offset 0x%08" MYPRIzx " "
			 "= phys @ 0x%08" MYPRIzx ".",
			 failed, 32,
			 failoffset * sizeof (uint32_t),
			 pipe_cfg->_phys_offset +
			 failoffset * sizeof (uint32_t));

      _lwroc_data_pipe_phys_offset =
	(void *) pipe_cfg->_phys_offset - map->_buf_ptr;

      _lwroc_data_pipe_phys_pexor = 1;
    }
}

lwroc_pipe_buffer_control *lwroc_data_pipe_alloc(lwroc_data_pipe_map_info *map,
						 lwroc_data_pipe_cfg *pipe_cfg,
						 int consumers,
						 int filter_no)
{
  lwroc_pipe_buffer_control *ctrl;
  size_t size;

  if (filter_no == -1)
    size = pipe_cfg->_buf_size;
  else
    size = pipe_cfg->_filter_size[filter_no];

  if (map->_buf_ptr &&
      map->_buf_left < size)
    LWROC_BUG_FMT("Mapped buffer has 0x%08" MYPRIzx " bytes left, "
		  "but trying to use 0x%08" MYPRIzx " bytes.",
		  map->_buf_left, size);

  lwroc_pipe_buffer_init(&ctrl,
			 consumers,
			 map->_buf_ptr, size,
			 16);

  if (map->_buf_ptr)
    {
      map->_buf_ptr  += size;
      map->_buf_left -= size;
    }

  return ctrl;
}

/* Get the offset between virtual and physical address for the data
 * pipe.  Intended to be used by readout; add this to a virtual
 * pointer to know the corresponding physical memory address.
 */
ssize_t lwroc_get_data_pipe_phys_offset(void)
{
  return _lwroc_data_pipe_phys_offset;
}

/* This is an ugly hack however, to stay MBS compatible with camac
 * pointer deliviering the physical offset when pexor mapped.
 */
int lwroc_data_pipe_is_phys_pexor(void)
{
  return _lwroc_data_pipe_phys_pexor;
}

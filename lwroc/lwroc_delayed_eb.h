/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_DELAYED_EB_H__
#define __LWROC_DELAYED_EB_H__

#include "../dtc_arch/acc_def/mystdint.h"
#include "../dtc_arch/acc_def/time_include.h"

#define LWROC_DELAYED_EB_STATUS_ENABLED      0x0001u
#define LWROC_DELAYED_EB_STATUS_FORCED       0x0002u
#define LWROC_DELAYED_EB_STATUS_NEED_FORCE   0x0004u

#define LWROC_SPILL_STATE_ON   0x01
#define LWROC_SPILL_STATE_OFF  0x02

/* Updated by readout, used by transmission. */
extern volatile uint32_t _lwroc_spill_state;
extern volatile time_t   _lwroc_spill_state_since;

/* Updated by state machine, used by transmission. */
extern volatile int      _lwroc_spill_state_stuck;

/* Updated by control thread, used by transmission (and readout). */
extern volatile uint32_t _lwroc_delayed_eb_status;

#endif/*__LWROC_DELAYED_EB_H__*/

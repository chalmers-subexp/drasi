/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2019  Michael Munch  <mm.munk@gmail.com>
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_file_writer.h"
#include "lwroc_file_functions.h"
#include "lwroc_net_proto.h"
#include "lwroc_thread_block.h"
#include "lwroc_message.h"
#include "lwroc_message_internal.h"
#include "lwroc_main_iface.h"
#include "lwroc_parse_util.h"
#include "lwroc_net_io_util.h"

#include <string.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include "../dtc_arch/acc_def/myinttypes.h"
#include "../dtc_arch/acc_def/myprizd.h"

typedef struct lwroc_fd_writer_state_t {
  lwroc_file_writer_info _info;
  int   _fd;
  char* _filename_cur;
} lwroc_fd_writer_state;

void lwroc_fd_writer_init(lwroc_file_writer_info* info)
{
  (void) info;
}

void lwroc_fd_writer_free(lwroc_file_writer_info* info)
{
  lwroc_fd_writer_state* fd_ws;
  fd_ws = CONTAINER_OF(info, lwroc_fd_writer_state, _info);

  assert(info); /* We cannot really be given a NULL pointer. */

  free(fd_ws->_filename_cur);
  free(fd_ws);
}

int lwroc_fd_writer_open(char const *filename,
			 lwroc_file_writer_info* info)
{
  lwroc_fd_writer_state* fd_ws;
  fd_ws = CONTAINER_OF(info, lwroc_fd_writer_state, _info);

  free(fd_ws->_filename_cur);
  fd_ws->_filename_cur = strdup_chk(filename);

  fd_ws->_fd = open(filename,
		    O_WRONLY | O_CREAT | O_EXCL | O_TRUNC
#ifdef O_LARGEFILE
		    | O_LARGEFILE
#endif
		    ,
		    S_IRUSR|S_IWUSR|
		    S_IRGRP|S_IWGRP|
		    S_IROTH|S_IWOTH);

  if (fd_ws->_fd == -1)
    {
      LWROC_PERROR("open");
      LWROC_ERROR_FMT("Failed to open file '%s' for writing.",
		      filename);
      return 0;
    }

  return 1;
}

void lwroc_fd_writer_close(lwroc_file_writer_info* info)
{
  struct stat st;
  lwroc_fd_writer_state* fd_ws;
  fd_ws = CONTAINER_OF(info, lwroc_fd_writer_state, _info);

  /* We write-protect the file just before closing. */
  if (fstat(fd_ws->_fd, &st) != 0)
    LWROC_PERROR("fstat");
  else if (fchmod(fd_ws->_fd,
		  (st.st_mode &
		   ~(mode_t) (S_IWUSR | S_IWGRP | S_IWOTH))) != 0)
    LWROC_PERROR("fchmod");
  else
    goto write_protected;

  LWROC_ERROR_FMT("Failed to write protect '%s'.", fd_ws->_filename_cur);
 write_protected:;

  /* Close. */
  lwroc_safe_close(fd_ws->_fd);
  fd_ws->_fd = -1;

  free(fd_ws->_filename_cur);
  fd_ws->_filename_cur = NULL;
}

ssize_t lwroc_fd_writer_write(void* priv, const void* buf,
			      size_t count)
{
  lwroc_fd_writer_state* fd_ws;
  fd_ws = CONTAINER_OF(priv, lwroc_fd_writer_state, _info);

  return write(fd_ws->_fd, buf, count);
}

int lwroc_fd_handle_write_err(int err, int attempts,
			      lwroc_file_writer_info* info)
{
  (void) err;
  (void) attempts;
  (void) info;

  /* Do nothing */
  return 1;
}

static lwroc_file_writer_functions _lwroc_fd_funcs = {
  lwroc_fd_writer_init,
  lwroc_fd_writer_free,
  lwroc_fd_writer_write,
  lwroc_fd_handle_write_err,
  lwroc_fd_writer_open,
  lwroc_fd_writer_close,
  0
};

lwroc_file_writer_info* lwroc_fd_writer_alloc(void)
{
  lwroc_fd_writer_state* fd_ws;

  fd_ws = malloc(sizeof(lwroc_fd_writer_state));
  if (!fd_ws)
    LWROC_FATAL("Memory allocation failure (fd writer internal struct).");

  memset(fd_ws, 0, sizeof(lwroc_fd_writer_state));

  fd_ws->_fd = -1;
  fd_ws->_info._funcs = &_lwroc_fd_funcs;

  return &fd_ws->_info;
}

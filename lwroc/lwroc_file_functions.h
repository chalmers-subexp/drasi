/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2019  Michael Munch  <mm.munk@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_FILE_FUNCTIONS_H__
#define __LWROC_FILE_FUNCTIONS_H__

#include "lwroc_message_inject.h"

/* Note: 'priv' instead of 'private' to not clash with C++ keyword. */
/* Same prototype as write(2), except @priv instead of @fd. */
typedef ssize_t (*lwroc_write_func)(void *priv,
				    const void *buf, size_t count);

/* Calling write(2), @priv is a pointer to a int fd. */
/* Hmmm, more error codes to handle? */
ssize_t lwroc_write_fd_no_sigpipe(void *priv,
				  const void *buf, size_t count);


struct lwroc_file_writer_functions_t;

typedef struct lwroc_file_writer_info_t
{
  struct lwroc_file_writer_functions_t  *_funcs;
  lwroc_format_message_context  _msg_context;
} lwroc_file_writer_info;

/* Must perform any necessary init.  All user parameters have been assigned. */
typedef void  (*lwroc_fw_init_func)(lwroc_file_writer_info *f);

/* Must perform any necessary cleanup. */
typedef void  (*lwroc_fw_free_func)(lwroc_file_writer_info *f);

/* Perform the actual write. */
typedef lwroc_write_func lwroc_fw_write_func;

/* arg_errno instead of errno, when errno is a macro. */
typedef int  (*lwroc_fw_handle_write_err_func)(int arg_errno, int attempts,
					       lwroc_file_writer_info *f);

/* Open a file. */
typedef int (*lwroc_fw_open_func)(char const *filename,
				  lwroc_file_writer_info *f);

/* Close the currently open file. */
typedef void (*lwroc_fw_close_func)(lwroc_file_writer_info *f);

#define LWROC_FILE_WRITER_FLAGS_AUTO_UNJAM  0x01

typedef struct lwroc_file_writer_functions_t {
  lwroc_fw_init_func             init;
  lwroc_fw_free_func             free;
  lwroc_fw_write_func            write_func;
  lwroc_fw_handle_write_err_func handle_write_err;
  lwroc_fw_open_func             open;
  lwroc_fw_close_func            close;
  int                           _flags;
} lwroc_file_writer_functions;

#define CONTAINER_OF(ptr,type,item) \
  ((type*) (((char*) (ptr)) - offsetof(type,item)))

#endif/*__LWROC_FILE_FUNCTIONS_H__*/

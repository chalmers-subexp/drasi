/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_file_writer.h"
#include "lwroc_fd_writer.h"
#include "lwroc_tsm_writer.h"
#include "lwroc_net_proto.h"
#include "lwroc_thread_block.h"
#include "lwroc_message.h"
#include "lwroc_message_internal.h"
#include "lwroc_mon_block.h"
#include "lwroc_main_iface.h"
#include "lwroc_parse_util.h"
#include "lwroc_net_trans.h"
#include "lwroc_net_io_util.h"
#include "gdf/lwroc_gdf_util.h"
#include "lmd/lwroc_lmd_util.h"
#include "lmd/lwroc_lmd_sticky_store.h"
#include "ebye/lwroc_ebye_util.h"
#include "xfer/lwroc_xfer_util.h"
#include "lwroc_timeouts.h"
#include "lwroc_crc32.h"

#include "lwroc_net_legacy.h"
#include "lwroc_control.h"

#include "gen/lwroc_message_source_serializer.h"
#include "gen/lwroc_monitor_file_source_serializer.h"
#include "gen/lwroc_monitor_file_block_serializer.h"
#include "gen/lwroc_monitor_filewr_source_serializer.h"
#include "gen/lwroc_monitor_filewr_block_serializer.h"

#include "gen/lwroc_file_request_serializer.h"

#include <string.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <ctype.h>

#include "../dtc_arch/acc_def/myinttypes.h"
#include "../dtc_arch/acc_def/myprizd.h"
#include "../dtc_arch/acc_def/fall_through.h"

#define STATE_MACHINE_DEBUG 0

lwroc_control_buffer *_file_ctrl = NULL;

/* Hold mode is used to indicate the state after a file write failure.
 * (It can for symmetry also be set by the control program.)
 */

/* File writer is in hold mode. */
#define LWROC_FW_STATE_HOLD                    1
/* File is closed. */
#define LWROC_FW_STATE_CLOSED                  2
/* File is closed, wait for termination to complete. */
#define LWROC_FW_STATE_WAIT_TERM               3
/* Close current file and open a new file. */
#define LWROC_FW_STATE_NEW_FILE                4 /* transient */
/* Close current file. */
#define LWROC_FW_STATE_CLOSE                   5 /* transient */
/* Open a new file. */
#define LWROC_FW_STATE_OPEN                    6 /* transient */
/* A file was opened at startup,
 * complete the process. */
#define LWROC_FW_STATE_OPENED                  7 /* transient */
/* Prepare file header. */
#define LWROC_FW_STATE_FILE_HEADER_PREPARE     8 /* transient */
/* Write file header. */
#define LWROC_FW_STATE_FILE_HEADER_WRITE       9
/* Compactify sticky data. */
#define LWROC_FW_STATE_STICKY_COMPACT         10 /* transient */
/* Prepare sticky data. */
#define LWROC_FW_STATE_STICKY_PREPARE         11 /* transient */
/* Write sticky data. */
#define LWROC_FW_STATE_STICKY_WRITE           12
/* Collect a buffer of data (to write). */
#define LWROC_FW_STATE_BUFFER_COLLECT         13
/* Write a buffer of data. */
#define LWROC_FW_STATE_BUFFER_WRITE           14

/* Control requests for operations. */
#define LWROC_FW_REQUEST_HOLD           1
#define LWROC_FW_REQUEST_CLOSE          2
#define LWROC_FW_REQUEST_UNJAM          3
#define LWROC_FW_REQUEST_NEWFILE        4
#define LWROC_FW_REQUEST_OPEN           5 /* Not used. TODO: remove. */
#define LWROC_FW_REQUEST_MAX            (LWROC_FW_REQUEST_OPEN)

/* It is only allowed to issue an OPEN command when state is HOLD or CLOSED.
 * It is only allowed to issue an HOLD command when state is CLOSED,
 *   or we have been failing writes for at least 10 s.
 * It is only allowed to issue an UNJAM command when
 *   we have been failing writes for at least 10 s.
 * It is not allowed to issue an CLOSE command when state is CLOSED.
 *
 * Note: unjam goes to hold, instead of automatically open a new file.
 * Reason: last buffer may have been only partially written, so
 * we (kind of) alert the users to that by forcing an explicit open to
 * continue.
 *
 *  [x] means that this path is chosen with that pending request.
 * {*x} means that the pending request is fulfilled (removed).
 *
 *  .-[N|O]-- HOLD {*H,*U} <-[H|U]-.  <----.  -[C]-.
 *  |          v                   |       |       |
 *  |-[N|O]-- CLOSED {*C} <--[C]---|  -[H]-'  <----'
 *  |                              |
 *  |         NEW                  |          <-----------.
 *  |          v                   |                      |
 *  |         CLOSE   --[H|C|U]----'  <----.              |
 *  |          v [N]                       |              |
 *  '-------> OPEN {*O,*N}                 |              |
 *             v                           |              |
 *            FILE_HEADER_PREPARE          |              |
 *             v                           |              |
 *            FILE_HEADER_WRITE  --[H|C|U]-|              |
 *             v                           |              |
 *            STICKY_COMPACT               |              |
 *             v                           |              |
 *        .-> STICKY_PREPARE               |              |
 *        |    v           |[std]          |              |
 *        '-- STICKY_WRITE |     --[H|C|U]-|  --[N]-------|
 *                         v               |              |
 *        .-> BUFFER_COLLECT     --[H|C]---|              |
 *        |    v v [N]                     |              |
 *        '-- BUFFER_WRITE       --[H|C|U]-'  --[N|auto]--'
 *
 * [stdone]: no more sticky data.
 * [auto]:   auto file size.
 *
 * O  open
 * N  new
 * C  close
 * H  hold
 * U  unjam
 */

#if 0
typedef struct file_writer_state_scheme_t
{
  char _state;
  char _next;
  char _next_req_hold;
  char _next_req_close;
  char _next_req_open;
  char _next_req_new;
} file_writer_state_scheme;

file_writer_state_scheme _file_writer_state_scheme[] = {
  /* Request:     HOLD, CLOSE,  UNJAM, OPEN, NEW        def_next  */
  /*              ----  -----   ----   ----  ----       --------  */
  FWSD(HOLD,      x,    CLOSED, x,     OPEN, OPEN,      x         ),
  FWSD(CLOSED,    HOLD, x,      x,     OPEN, OPEN,      x         ),
  FWSD(NEW,       x,    x,      x,     x,    x,         CLOSE     ),
  FWSD(CLOSE,     HOLD, CLOSED, HOLD,  x,    OPEN,      OPEN      ),
  FWSD(OPEN,      x,    x,      x,     x,    x,         FH_PREP   ),
  FWSD(FH_PREP,   x,    x,      x,     x,    x,         FH_WRITE  ),
  FWSD(FH_WRITE,  CLOSE,CLOSE   CLOSE, x,    x,         ST_COMPCT ),
  FWSD(ST_COMPCT, x,    x,      x,     x,    x,         ST_PREP   ),
  FWSD(ST_PREP,   x,    x,      x,     x,    x,         ST_WRITE  ),
  FWSD(ST_WRITE,  CLOSE,CLOSE   CLOSE, x,    NEW,       ST_PREP   ),
  FWSD(BUF_COLL,  CLOSE,CLOSE   x,     x,    BUF_WRITE, BUF_WRITE ),
  FWSD(BUF_WRITE, CLOSE,CLOSE,  CLOSE, x,    NEW,       BUF_COLL  ),
};
#endif

typedef struct lwroc_monitor_file_filewr_block_t
{
  lwroc_monitor_file_block   _file;
  lwroc_monitor_filewr_block _filewr[1 /* actually more */];
} lwroc_monitor_file_filewr_block;

lwroc_monitor_file_filewr_block *_lwroc_mon_file;
lwroc_mon_block                 *_lwroc_mon_file_handle = NULL;
uint32_t                         _lwroc_mon_filewr_num = 0;

lwroc_file_writer_info* _lwroc_fw_info = NULL;

char *lwroc_mon_file_serialise(char *wire, void *block)
{
  lwroc_monitor_file_filewr_block *file_block =
    (lwroc_monitor_file_filewr_block *) block;
  uint32_t i;

  wire = lwroc_monitor_file_block_serialize(wire, &file_block->_file);

  for (i = 0; i < _lwroc_mon_filewr_num; i++)
    wire = lwroc_monitor_filewr_block_serialize(wire, &file_block->_filewr[i]);

  return wire;
}

void lwroc_file_writer_thread_at_prepare(lwroc_thread_instance *inst)
{
  (void) inst;

  /* Monitor information. */

  _lwroc_mon_filewr_num = 2; /* [0] for recently closed file,
				[1] for current. */

  {
    lwroc_message_source file_src_source;
    lwroc_message_source_sersize sz_file_src_source;
    lwroc_monitor_file_source file_src;
    char *wire;
    size_t sz_mon_file;

    file_src._filewr_blocks = _lwroc_mon_filewr_num;

    lwroc_mon_source_set(&file_src_source);

    /* The size ends where the file writer blocks end. */
    sz_mon_file = offsetof(lwroc_monitor_file_filewr_block,
			   _filewr[_lwroc_mon_filewr_num]);

    _lwroc_mon_file = malloc (sz_mon_file);

    if (!_lwroc_mon_file)
      LWROC_FATAL("Failure allocating memory for file write monitor.");

    memset (_lwroc_mon_file, 0, sz_mon_file);
    _lwroc_mon_file_handle =
      lwroc_reg_mon_block(0, &_lwroc_mon_file->_file, sz_mon_file,
			  lwroc_monitor_file_block_serialized_size() +
			  _lwroc_mon_filewr_num *
			  lwroc_monitor_filewr_block_serialized_size(),
			  lwroc_mon_file_serialise,
			  lwroc_message_source_serialized_size(&file_src_source,
							       &sz_file_src_source) +
			  lwroc_monitor_file_source_serialized_size(), NULL,
			  NULL);

    wire = _lwroc_mon_file_handle->_ser_source;

    wire = lwroc_message_source_serialize(wire, &file_src_source,
					  &sz_file_src_source);
    wire = lwroc_monitor_file_source_serialize(wire, &file_src);

    assert (wire == (_lwroc_mon_file_handle->_ser_source +
		     _lwroc_mon_file_handle->_ser_source_size));
  }

  _file_ctrl = lwroc_alloc_control_buffer(LWROC_CONTROL_OP_FILE, 0x400);
}

/********************************************************************/

typedef struct lwroc_filename_base_t
{
  char      _filename_base[256];
  uint32_t  _fileno;
  int       _fileno_len;
  uint32_t  _runno;
  int       _runno_len;
} lwroc_filename_base;

/* Append @n chars from @src to the end of the string @dest of size @size.
 * If @n == 0, then use all.
 * Replace all '%' with @repl.
 */
void lwroc_efb_cat(char *dest, size_t size,
		   const char *src, size_t n,
		   char repl)
{
  char *p;
  size_t len;

  p = dest;
  len = strlen(dest);
  if (n == (size_t) -1)
    n = strlen(src);

  if (len + n + 1 > size)
    n = (size - len - 1);

  memcpy(dest + len, src, n);
  dest[len + n] = 0;

  if (repl)
    {
      p = dest + len;

      while ((p = strchr(p, '%')) != NULL)
	*(p++) = repl;
    }
}

/* This function is adapted from UCESB,
 * limit_file_size::parse_open_first_file.
 */

void lwroc_extract_filename_base(lwroc_filename_base *info,
				 const char *filename, int newrun,
				 lwroc_gdf_format_functions *file_fmt)
{
  const char *last_slash;
  const char *dot;
  const char *dot_lmd;
  const char *digit_fileno;
  const char *format_fileno = "";
  const char *format_prefileno = "";
  const char *end_runno;
  const char *digit_runno;
  const char *format_runno = "";
  /* const char *format_prerunno = ""; */

  /* If the filename contains digit(s) before the dot, they are used as
   * the initial number, otherwise, we add an _%d and start with 1.
   */
  last_slash = strrchr(filename,'/');

  if (!last_slash)
    last_slash = filename;

  /* Only look for a dot after the last slash; use the last dot. */
  dot = strrchr(last_slash,'.');

  /* If no dot, we assume dot is at the end. */
  if (!dot)
    dot = last_slash + strlen(last_slash);

  /* If we find an occurrence of '.lmd', that takes precedence. */
  dot_lmd = strstr(last_slash, ".lmd");

  while (dot_lmd)
    {
      const char *next_dot_lmd = strstr(dot_lmd+1, ".lmd");

      if (!next_dot_lmd)
	break;
      dot_lmd = next_dot_lmd;
    }

  if (dot_lmd)
    dot = dot_lmd;

  digit_fileno = dot;
  while (digit_fileno > last_slash &&
	 isdigit((unsigned char) digit_fileno[-1]))
    digit_fileno--;

  format_fileno = "%0*" PRIu32;

  if (digit_fileno < dot)
    {
      /* Size of field to use. */
      if (digit_fileno[0] == '0')
	info->_fileno_len = (int) (dot - digit_fileno);
      else
	info->_fileno_len = 1;
      info->_fileno = (uint32_t) strtoul(digit_fileno, NULL, 10);
    }
  else
    {
      /* No digits found, use defaults. */

      info->_fileno_len = file_fmt->_default_fileno_digits;
      info->_fileno = 1;

      if (dot > last_slash &&
	  dot[-1] != '_' && dot[-1] != '-')
	{
	  /* Name did not end with underscore or dash, add an underscore. */
	  format_prefileno = "_";
	}
    }

  end_runno = digit_fileno;
  while (end_runno > last_slash &&
	 !isdigit((unsigned char) end_runno[-1]))
    end_runno--;

  digit_runno = end_runno;
  while (digit_runno > last_slash &&
	 isdigit((unsigned char) digit_runno[-1]))
    digit_runno--;

  format_runno = "%0*" PRIu32;

  if (digit_runno < end_runno) /* We found *more* digits
				* (before fileno digits).
				*/
    {
      /* Size of field to use. */
      if (digit_runno[0] == '0')
	info->_runno_len = (int) (end_runno - digit_runno);
      else
	info->_runno_len = 1;
      info->_runno = (uint32_t) strtoul(digit_runno, NULL, 10);
    }
#if 0
  /* If we do not find digits for a run number, we do not introduce
   * them.  We rather report an error.  User needs to supply a
   * suitable filename.  This solves the problem of user opening a
   * file for automatic file numbering (which sets up just one
   * automatic number), and then asking for newrun, which then only
   * get the file number incremented.
   */
  else if (newrun)
    {
      /* No digits found, use defaults. */
      /* Place just before fileno. */
      digit_runno = end_runno = digit_fileno;

      info->_runno_len = file_fmt->_default_runno_digits;
      info->_runno = 1;

      /* format_runno = "%0*_" PRIu32; Handled by format_prefileno. */

      if (end_runno > last_slash &&
	  end_runno[-1] != '_' && end_runno[-1] != '-')
	{
	  /* Name did not end with underscore or dash, add an underscore. */
	  format_prerunno = "_";
	}
    }
#endif
  else
    {
      digit_runno = end_runno = digit_fileno;

      format_runno = "";

      info->_runno = 0;
      info->_runno_len = 0;
    }

  if (newrun)
    {
      /* A new run restarts at filenumber. */
      info->_fileno = file_fmt->_default_first_fileno;
    }

#define LWROC_EFB_CAT(src, n, repl)					\
  lwroc_efb_cat(info->_filename_base, sizeof (info->_filename_base),	\
		(src), (size_t) (n), (repl));

  /* Replace any '%' in non-format strings with 'X',
   * to not have the sprintf fooled.
   */
  info->_filename_base[0] = 0;
  LWROC_EFB_CAT(filename,           (digit_runno - filename),   'X');
  /* LWROC_EFB_CAT(format_prerunno, -1, 'X'); */
  LWROC_EFB_CAT(format_runno,       -1,                          0 );
  LWROC_EFB_CAT(end_runno,          (digit_fileno - end_runno), 'X');
  LWROC_EFB_CAT(format_prefileno,   -1,                         'X');
  LWROC_EFB_CAT(format_fileno,      -1,                          0 );
  LWROC_EFB_CAT(dot,                -1,                         'X');

  assert(strlen(info->_filename_base) < strlen(filename) + 5 + 5 + 1);

  LWROC_INFO_FMT("Automatic filename: %s (first=%" PRIu32 ",%" PRIu32 ") "
		 "(width=%d,%d)",
		 info->_filename_base,
		 info->_runno,     info->_fileno,
		 info->_runno_len, info->_fileno_len);
}

int lwroc_filename_base_gen(lwroc_filename_base *info,
			    char *filename)
{
  int attempts;

  for (attempts = 0; attempts < 10000; attempts++)
    {
      struct stat st;

      if (info->_runno_len)
	sprintf(filename,
		info->_filename_base,
		info->_runno_len,
		info->_runno,
		info->_fileno_len,
		info->_fileno);
      else
	sprintf(filename,
		info->_filename_base,
		info->_fileno_len,
		info->_fileno);

      /* See if we can stat the file.  If so, we must continue with
       * another name.  If we cannot stat it, allow things to proceed.
       */

      /* TODO: stat'ing should be done by the file writer class in
       * effect.
       */

      if (stat(filename, &st) != 0)
	{
	  if (errno == ENOENT)
	    {
	      /* File does not exist, can be used! */
	      /* Increment for next file. */
	      info->_fileno++;
	      return 1;
	    }

	  /* All other errors mean that we have some unexpected error. */
	  LWROC_PERROR("stat");
	  LWROC_ERROR_FMT("Failed to stat new filename '%s'.",
			  filename);
	  return 0;
	}

      LWROC_WARNING_FMT("New numbered filename '%s' already exist.",
			filename);
      if (info->_runno_len)
	info->_runno++;
      else
	info->_fileno++;
    }

  LWROC_ERROR_FMT("Giving up after %d attempts to generate unique filename.",
		  attempts);
  return 0;
}

/********************************************************************/

/* Even if we write to multiple clients, we try to write fully to each
 * client in order.  The buffers are not that large, so makes little
 * sense to try round-robin.  We write fully, since otherwise if
 * failing client takes ages to report its failure, then it can also
 * be many rounds for the working files to complete.
 *
 * This will all be much better with an external writer and individual
 * threads, as we only have to worry about writing to one
 * (non-blocking) pipe.
 *
 * (Threads would not be needed had it not been for wanting to be able
 * to use various external archiving libraries, that wrap the writes
 * in who-knows what amount of glue...)
 */

int lwroc_file_writer_write(lwroc_gdf_buffer_chunks *lbc,
			    uint64_t *sizeadd, uint32_t *crc32)
{
  int last_errno;
  int attempts = 0;

  /* TODO: since we return, last_errno must be kept in the file_writer
   * function structure.
   */
  last_errno = 0;

  /* Emulate jam. */
  /*
  if (_lwroc_mon_file->_file._buffers > 100)
    {
      sleep(1);
      return 0;
    }
  */

  while (lwroc_gdf_buffer_chunks_pending(lbc))
    {
      ssize_t n =
	lwroc_gdf_buffer_chunks_write(lbc,
				      _lwroc_fw_info->_funcs->write_func,
				      _lwroc_fw_info, crc32);

      if (n == -1)
	{
	  if (errno == EINTR)
	    continue;

	  attempts++;

	  if (errno != last_errno)
	    {
	      /* Only print error if it changed, or happens a lot. */
	      /* TODO: Handled by the message rate-limiter? */

	      last_errno = errno;

	      LWROC_PERROR("write");
	      LWROC_ERROR_FMT("Error while writing file (%d attempts).",
			      attempts);
	    }

	  /* Then what? */

	  /* We try again.  Indefinitely.  Once per second.
	   *
	   * The only thing that can stop us is a control command,
	   * i.e. a user action (or intervention by the writer library?).
	   */

	  /* TODO: We could get an EPIPE, e.g. in case we write to a
	   * compressor.  Though, we'd also get the SIGPIPE...
	   */

	  sleep(1);

	  return 0;
	}
      else
	{
	  attempts = 0;

	  _lwroc_mon_file->_file._bytes += (uint64_t) n;
	  _lwroc_mon_file->_filewr[1]._bytes += (uint64_t) n;
	  *sizeadd += (uint64_t) n;
	}
    }

  return 1;
}

/********************************************************************/

void lwroc_file_writer_log(const char *filename,
			   uint64_t fileevents,
			   uint64_t filesize,
			   uint32_t filecrc32,
			   time_t open_at, time_t close_at)
{
  FILE *fid;
  const char *logname = "lwfile.log";
  int ret;

  struct tm *mt_tm;
  char mt_open[64];
  char mt_close[64];
  /*char mt_tz[64];*/

  fid = fopen(logname, "a");

  if (fid == NULL)
    {
      LWROC_ERROR_FMT("Failed to open file writer log '%s' to append.",
		      logname);
      return;
    }

  mt_tm = localtime(&open_at);
  strftime(mt_open,sizeof(mt_open),"%Y-%m-%d %H:%M:%S",mt_tm);
  mt_tm = localtime(&close_at);
  strftime(mt_close,sizeof(mt_close),"%Y-%m-%d %H:%M:%S",mt_tm);
  /*strftime(mt_tz,sizeof(mt_tz),"%z %Z",mt_tm);*/

  if (filesize == (uint64_t) -1)
    ret = fprintf (fid,
		   "open=%19s       %19s "
		   "      %8s "
		   "     %-11s        %-11s "
		   "file=%s\n",
		   mt_open, "",
		   "",
		   "",
		   "",
		   filename);
  else
    ret = fprintf (fid,
		   "open=%19s close=%19s "
		   "crc32=%08" PRIx32 " "
		   "size=%-11" PRIu64 " events=%-11" PRIu64 " "
		   "file=%s\n",
		   mt_open, mt_close,
		   filecrc32,
		   filesize,
		   fileevents,
		   filename);

  if (ret < 0)
    LWROC_ERROR_FMT("Failed to write to file writer log '%s'.",
		    logname);

  ret = fclose(fid);

  if (ret)
    LWROC_ERROR_FMT("Failed to close file writer log '%s'.",
		    logname);
}

/********************************************************************/

uint32_t _lwroc_file_buffer_size = 0;

/********************************************************************/

typedef struct lwroc_file_writer_state_t
{
  char _filename[256];
  int  _start_state;
  int  _terminate_on_close;
  uint64_t _max_events;
  int _flush_timeout;

  lwroc_gdf_format_functions *_file_fmt;

  lwroc_gdf_buffer_chunks *_bufchunks;
  lwroc_lmd_buffer_chunks *_lmd_bufchunks;

} lwroc_file_writer_state;

lwroc_file_writer_state *_lwroc_fw_state = NULL;

/********************************************************************/

#define DEBUG_FILE_WRITER_PRINT 0

void lwroc_file_writer_thread_loop(lwroc_thread_instance *inst)
{
  lwroc_lmd_sticky_store *sticky_store;
  size_t sticky_iter_ev_offset = (size_t) -1;

  lwroc_file_request file_req;
  lwroc_filename_base autoname;

  uint64_t fileevents = 0;

  uint64_t filesize = 0;
  uint32_t filecrc32 = 0;

  time_t fileopen_at = 0;

  uint64_t autosize = 0;
  time_t autotime = 0;
  uint64_t autoevents = _lwroc_fw_state->_max_events;

  int file_state = _lwroc_fw_state->_start_state;
  int request = 0;
  int newrun = 0;

  int is_jammed = 0;
  int failed_writes = 0;
  int term_hold_warn = 0;
  struct timeval write_fail_since;
  struct timeval last_write_at;

  struct timeval now;

  autoname._filename_base[0] = 0;

  _file_ctrl->_thread_notify_ready = inst->_block;
  _file_ctrl->_state = LWROC_CONTROL_STATE_WAIT_MESSAGE;

  sticky_store = lwroc_lmd_sticky_store_init();

  lwroc_msg_wait_any_msg_client(inst->_block);

  LWROC_INFO_FMT("File writer, writing %d kiB buffers.",
		 _lwroc_file_buffer_size / 1024);

  for ( ; ; )
    {
      uint32_t done_events;
      int partial_buffer = 0;

#if STATE_MACHINE_DEBUG
      printf ("FILE: [%d] (req: %d)\n",
	      file_state, request);
      fflush(stdout);
#endif

      if (inst->_terminate)
	{
	  size_t fill;

	  /* We only terminate once the buffer of data to handle is
	   * empty.  If we are not in hold mode, we will be skipping
	   * the data below and come back here.
	   */

	  fill =
	    lwroc_pipe_buffer_fill_due_to_consumer(_lwroc_main_data_cons_file);

	  if (fill == 0 &&
	      (file_state == LWROC_FW_STATE_CLOSED ||
	       file_state == LWROC_FW_STATE_WAIT_TERM ||
	       file_state == LWROC_FW_STATE_HOLD))
	    break;

	  if (file_state == LWROC_FW_STATE_HOLD &&
	      !term_hold_warn)
	    {
	      LWROC_WARNING("File writer in hold mode, not terminating.");
	      term_hold_warn = 1;
	    }
	}

      /* Do we have a control request? */

      if (_file_ctrl->_state & LWROC_CONTROL_STATE_USER_ACTION_MARK)
      switch (_file_ctrl->_state)
	{
	  /* Note: there are other states too, while control requests
	   * are being received by the network thread.  We do not
	   * react on those!
	   */
	case LWROC_CONTROL_STATE_MESSAGE_READY:
	{
	  lwroc_deserialize_error desererr;
	  const char *end;

	  /* Consume tokens...  Hmmm... */
	  if (file_state != LWROC_FW_STATE_CLOSED)
	    {
	      struct timeval timeout;
	      timeout.tv_sec = 0;
	      timeout.tv_usec = 0;
	      lwroc_thread_block_get_token_timeout(_lwroc_file_writer_thread->
						   /* */ _block,
						   &timeout);
	    }

	  /* Make sure we read data after checking the state. */
	  MFENCE;

	  /* Prepare a response. */

	  _file_ctrl->_response._tag    = _file_ctrl->_request._tag;
	  _file_ctrl->_response._op     = _file_ctrl->_request._op;
	  _file_ctrl->_response._value  = 0;
	  _file_ctrl->_response._response_data_size = 0;
	  LWROC_SET_CONTROL_STATUS(_file_ctrl, UNKNOWN,
				   PERFORMER, "Unknown control request.");

	  /* Can we parse the request? */

	  end =
	    lwroc_file_request_deserialize_inplace(&file_req,
						   _file_ctrl->_request_data,
						   _file_ctrl->_request.
						   /* */ _request_data_size,
						   &desererr);

	  if (end == NULL)
	    {
	      LWROC_SET_CONTROL_STATUS(_file_ctrl, INVALID,
				       PERFORMER,
				       "Invalid control message.");
	      goto message_send;
	    }

	  /* We need (in worst case) 10 chars for a int number,
	   * '_' and '\0'.
	   */
	  if (strlen(file_req._filename) >
	      sizeof(_lwroc_fw_state->_filename) - 12)
	    {
	      /* We refuse filenames which we will not be able to
	       * handle auto-numbering of...
	       */
	      LWROC_SET_CONTROL_STATUS(_file_ctrl, INVALID,
				       PERFORMER,
				       "Too long filename "
				       "(cannot handle auto-numbering).");
	      goto message_send;
	    }

	  assert (request == 0); /* No request should be ongoing. */

	  switch (file_req._op)
	    {
	    case LWROC_FILE_OP_OPEN:
	      if (file_state != LWROC_FW_STATE_HOLD &&
		  file_state != LWROC_FW_STATE_CLOSED)
		{
		  LWROC_SET_CONTROL_STATUS(_file_ctrl, ALREADY,
					   PERFORMER,
					   "A file is already open.");
		  break;
		}

	      FALL_THROUGH;

	      /* New file is always ok, either we are closed or not. */
	    case LWROC_FILE_OP_NEWFILE:
	    case LWROC_FILE_OP_NEWRUN:

	      newrun = (file_req._op == LWROC_FILE_OP_NEWRUN);

	      /* If it is a newrun, and we use the previous automatic
	       * naming, then update the run number.
	       * (Otherwise, filenumber update was prepared).
	       */
	      if (newrun)
		{
		  autoname._runno++;
		  autoname._fileno =
		    _lwroc_fw_state->_file_fmt->_default_first_fileno;
		}

	      /* Check the arguments of the call. */

	      if (strlen(file_req._filename))
		lwroc_extract_filename_base(&autoname, file_req._filename,
					    newrun,
					    _lwroc_fw_state->_file_fmt);

	      if (autoname._filename_base[0] == 0 &&
		  !strlen(file_req._filename))
		{
		  /* Cannot work without a filename. */
		  LWROC_SET_CONTROL_STATUS(_file_ctrl, INVALID,
					   PERFORMER,
					   "Cannot make new file, no name.");
		  break;
		}

	      if (newrun &&
		  autoname._runno_len == 0)
		{
		  /* Cannot make new run without a run number field. */
		  LWROC_SET_CONTROL_STATUS(_file_ctrl, INVALID,
					   PERFORMER,
					   "Cannot make new run, "
					   "no run number field in filename.");
		  break;
		}

	      /* As arguments are ok, we now defer the opening to the
	       * state machine.  Such that it can use the same routine
	       * to auto-change files.
	       */
	      autosize = file_req._size;
	      autotime = (time_t) file_req._time;
	      autoevents = file_req._events;

	      /* OP_OPEN also goes as NEWFILE request. */
	      request = LWROC_FW_REQUEST_NEWFILE;
	      break;

	    case LWROC_FILE_OP_CLOSE:
	      if (file_state == LWROC_FW_STATE_CLOSED ||
		  file_state == LWROC_FW_STATE_WAIT_TERM)
		{
		  LWROC_SET_CONTROL_STATUS(_file_ctrl, ALREADY,
					   PERFORMER,
					   "No file is open, already closed.");
		  break;
		}

	      /* We may not close a file while we have buffer data
	       * collected but not yet written.  Since we have already
	       * updated the sticky state, this would make the next
	       * sticky recovery buffer come from later events than
	       * the ones collected.  Also, the sticky replay would
	       * destroy the collected information.
	       *
	       * I.e., we must at least try to write the buffer of
	       * data.  If the write fails, then it is discarded, so
	       * we are again in sync with the sticky replay that will
	       * happen at the beginning of the following file.
	       */
	      request = LWROC_FW_REQUEST_CLOSE;
	      break;

	    case LWROC_FILE_OP_HOLD:
	      if (file_state == LWROC_FW_STATE_HOLD)
		{
		  LWROC_SET_CONTROL_STATUS(_file_ctrl, ALREADY,
					   PERFORMER,
					   "No file is open, already held.");
		  break;
		}

	      /* Same argumentation about mid-buffer abort as for
	       * close above.
	       */
	      request = LWROC_FW_REQUEST_HOLD;
	      break;

	    case LWROC_FILE_OP_UNJAM:
	      if (!is_jammed)
		{
		  LWROC_SET_CONTROL_STATUS(_file_ctrl, ALREADY,
					   PERFORMER,
					   "No file is jammed.");
		  break;
		}

	      request = LWROC_FW_REQUEST_UNJAM;
	      break;

	    case LWROC_FILE_OP_STATUS:
	      LWROC_SET_CONTROL_STATUS(_file_ctrl, SUCCESS,
				       PERFORMER, NULL);
	      break;
	    }
	  _file_ctrl->_state = LWROC_CONTROL_STATE_MESSAGE_WORKING;
	}
	FALL_THROUGH;

	case LWROC_CONTROL_STATE_MESSAGE_WORKING:
	  /* If status is unknown, then we do not have a response yet. */
	  if (_file_ctrl->_response._status == LWROC_CONTROL_STATUS_UNKNOWN)
	    break;

	  FALL_THROUGH;
	/* We are supposed to send the control message response. */
	case LWROC_CONTROL_STATE_MESSAGE_SEND:
	  ;
	message_send:
	  /*
	  LWROC_INFO("write response...");
	  */
	  /* We tell if file is currently being written as status value. */
	  _file_ctrl->_response._value = (uint32_t) file_state;

	  memset(&file_req, 0, sizeof (file_req));

	  switch (file_state)
	    {
	    case LWROC_FW_STATE_HOLD:
	      file_req._op = LWROC_FILE_OP_HOLD;
	      break;
	    case LWROC_FW_STATE_CLOSED:
	    case LWROC_FW_STATE_WAIT_TERM:
	      file_req._op = LWROC_FILE_OP_CLOSE;
	      break;
	    default:
	      /* In all other states we are open (even in CLOSE state,
	       * since we have not closed yet.  (And do not want to
	       * report intermittent closed status.)
	       */
	      /* Unless we are jammed. */
	      if (is_jammed)
		file_req._op = LWROC_FILE_OP_JAMMED;
	      else
		file_req._op = LWROC_FILE_OP_OPEN;
	      break;
	    }

	  file_req._size = filesize;
	  file_req._events = fileevents;
	  file_req._filename = _lwroc_fw_state->_filename;

	  {
	    lwroc_file_request_sersize sersize;
	    size_t sz_response;

	    sz_response =
	      lwroc_file_request_serialized_size(&file_req, &sersize);

	    if (sz_response > _file_ctrl->_request._response_data_maxsize)
	      {
		LWROC_SET_CONTROL_STATUS(_file_ctrl, TOO_BIG,
					 PERFORMER,
					 "Response structure too large.");
	      }
	    else
	      {
		lwroc_file_request_serialize(_file_ctrl->_response_data,
					     &file_req, &sersize);
		_file_ctrl->_response._response_data_size =
		  (uint32_t) sz_response;
	      }
	  }
	  /*
	  LWROC_INFO_FMT("%d bytes...",
			 _file_ctrl->_response._response_data_size);
	  */
	  lwroc_control_done(_file_ctrl, _lwroc_file_writer_thread->_block);
	  break;
	}

      assert(request >= 0 &&
	     request <= LWROC_FW_REQUEST_MAX);

#if STATE_MACHINE_DEBUG
      printf ("FILE: {%d} (req: %d)\n",
	      file_state, request);
      fflush(stdout);
#endif

      /* We cannot both have a new request, and an old unfinished request. */
      /* assert(!request || !pending); */

      switch (file_state)
	{
	case LWROC_FW_STATE_WAIT_TERM:
	case LWROC_FW_STATE_CLOSED:
	  /* We are not holding, so we should consume whatever data is
	   * in the pipeline.  Use the normal routines to parse past
	   * events (to get any sticky ones; see comment for network
	   * server).
	   */

	  /* TODO: is it really healthy to do this init every time? */
	  _lwroc_fw_state->_file_fmt->
	    chunks_init(_lwroc_fw_state->_bufchunks,
			_lwroc_file_buffer_size, 1);

	  if (_lwroc_fw_state->_file_fmt->
	      chunks_collect(_lwroc_fw_state->_bufchunks,
			     _lwroc_main_data_cons_file,
			     _lwroc_file_writer_thread->
			     /* */ _block,
			     NULL, sticky_store,
			     0, 1))
	    {
	      lwroc_gdf_buffer_chunks_release(_lwroc_fw_state->_bufchunks,
					      _lwroc_main_data_cons_file,
					      NULL);
	      /* TODO: Unless we ate some reasonable amount, like a
	       * quarter, should we sleep?
	       * (if, then must consider next item, as we no longer
	       * eat small chunks).
	       *
	       * TODO: Also, when a new file is opened, and we were
	       * closed before, should we empty the buffer first?
	       */

	      break;
	    }

	  if (file_state == LWROC_FW_STATE_WAIT_TERM)
	    {
	      /* We are waiting for the entire program to terminate.
	       * Do not open any new files.
	       */
	      break;
	    }

	  FALL_THROUGH;

	case LWROC_FW_STATE_HOLD:
	  if (!request)
	    {
	      struct timeval timeout;
	      /* Try again in 0.1 s.
	       * Should select on control request token?
	       */
	      timeout.tv_sec = 0;
	      timeout.tv_usec = 100000;
	      lwroc_thread_block_get_token_timeout(_lwroc_file_writer_thread->
						   /* */ _block,
						   &timeout);
	    }
	  /* For both hold and close mode, if we have a pending file
	   * new / open request, do that.
	   */
	  if (request == LWROC_FW_REQUEST_NEWFILE ||
	      request == LWROC_FW_REQUEST_OPEN)
	    {
	      /* New after hold/close need not close, so go to open. */
	      file_state = LWROC_FW_STATE_OPEN;
	      break;
	    }
	  /* Go between HOLD and CLOSED by request. */
	  if (request == LWROC_FW_REQUEST_HOLD ||
	      request == LWROC_FW_REQUEST_UNJAM)
	    {
	      LWROC_WARNING("File writer entered hold mode.");
	      _lwroc_mon_file->_filewr[0]._state = LWROC_FILEWR_STATE_HOLD;
	      file_state = LWROC_FW_STATE_HOLD;
	      request = 0;
	      LWROC_SET_CONTROL_STATUS(_file_ctrl, SUCCESS,
				       PERFORMER, NULL);
	    }
	  if (request == LWROC_FW_REQUEST_CLOSE)
	    {
	      if (file_state == LWROC_FW_STATE_HOLD)
		LWROC_WARNING("File writer entered non-hold mode.");
	      _lwroc_mon_file->_filewr[0]._state = LWROC_FILEWR_STATE_CLOSED;
	      file_state = LWROC_FW_STATE_CLOSED;
	      request = 0;
	      LWROC_SET_CONTROL_STATUS(_file_ctrl, SUCCESS,
				       PERFORMER, NULL);
	    }
	  break;
	case LWROC_FW_STATE_NEW_FILE: /* transient, no pending check */
	  /* This state uses the OPEN and CLOSE states by falling
	   * through them.
	   */
	  (void) 0; /* Avoid complaint about empty label for fall_through. */
	  FALL_THROUGH;

	case LWROC_FW_STATE_CLOSE: /* transient, no pending check */

	  _lwroc_fw_info->_funcs->close(_lwroc_fw_info);

	  /* This is now the most recently closed file. */
	  _lwroc_mon_file->_filewr[0] =
	    _lwroc_mon_file->_filewr[1];
	  _lwroc_mon_file->_filewr[0]._state = LWROC_FILEWR_STATE_CLOSED;

	  gettimeofday(&now, NULL);

	  _lwroc_mon_file->_filewr[0]._open_close_time._sec  =
	    (uint64_t) now.tv_sec;
	  _lwroc_mon_file->_filewr[0]._open_close_time._nsec =
	    (uint32_t) now.tv_usec * 1000;

	  /* Clear the info on the current file. */
	  memset (&_lwroc_mon_file->_filewr[1], 0,
		  sizeof (_lwroc_mon_file->_filewr[1]));

	  lwroc_crc32_final(&filecrc32);

	  LWROC_ACTION_FMT("Closed file '%s', "
			   "%" PRIu64 " bytes, crc32=%08" PRIx32 ".",
			   _lwroc_fw_state->_filename, filesize, filecrc32);

	  lwroc_file_writer_log(_lwroc_fw_state->_filename, fileevents,
				filesize, filecrc32,
				fileopen_at, now.tv_sec);

	  /* We are no longer failing! :-) */
	  is_jammed = 0;
	  failed_writes = 0;

	  if (_lwroc_fw_state->_terminate_on_close)
	    {
	      LWROC_WARNING("Initiating program termination after file close, "
			    "as requested.");
	      lwroc_notify_quit();
	      file_state = LWROC_FW_STATE_WAIT_TERM;
	      /* If there was a request ongoing, it is no longer... */
	      request = 0;
	      LWROC_SET_CONTROL_STATUS(_file_ctrl, SHUTDOWN,
				       PERFORMER,
				       "Note!  "
				       "Shutdown initiated after close.");
	      break;
	    }

	  if (request == LWROC_FW_REQUEST_CLOSE)
	    {
	      file_state = LWROC_FW_STATE_CLOSED;
	      break;
	    }
	  if (request == LWROC_FW_REQUEST_HOLD ||
	      request == LWROC_FW_REQUEST_UNJAM)
	    {
	      _lwroc_mon_file->_filewr[0]._state = LWROC_FILEWR_STATE_HOLD;
	      file_state = LWROC_FW_STATE_HOLD;
	      break;
	    }
	  /* We may have a newfile request, or we got here by
	   * auto-change of files, in which case there is no request.
	   */

	  /* Such that we respond to the request. */
	  FALL_THROUGH;

	case LWROC_FW_STATE_OPEN: /* transient, no pending check */

	  /* There really should be no request to hold or close things
	   * when we are about to open a new file.
	   */
	  assert(!request ||
		 request == LWROC_FW_REQUEST_OPEN ||
		 request == LWROC_FW_REQUEST_NEWFILE);

	  /*
	  printf ("autosize:%" PRIu64 " autotime:%" PRIu64 " "
		  "autoeventssize:%" PRIu64 " "
		  "request:%d len:%" MYPRIzd "\n",
		  autosize, autotime, autoevents,
		  request, strlen(file_req._filename));
	  */
	  /* Are we handling a/the direct open/new request?
	   * And with no auto-naming?
	   * Then use actual name requested.
	   */
	  if ((!autosize && !autotime && !autoevents) &&
	      request &&
	      strlen(file_req._filename) &&
	      !newrun)
	    {
	      /* This use of file_req._filename is while the
	       * request is ongoing.  So we are not colliding with any
	       * other use of file_req.
	       */
	      strncpy(_lwroc_fw_state->_filename,
		      file_req._filename,
		      sizeof (_lwroc_fw_state->_filename)-1);
	      _lwroc_fw_state->
		_filename[sizeof (_lwroc_fw_state->_filename)-1] = 0;
	    }
	  else
	    {
	      if (!lwroc_filename_base_gen(&autoname,
					   _lwroc_fw_state->_filename))
		goto open_fail_hold;
	    }

	  if (!_lwroc_fw_info->_funcs->open(_lwroc_fw_state->_filename,
					    _lwroc_fw_info))
	    {
	      if (request)
		{
		  /* It cannot have been a close/hold request when we
		   * are here, must be for new or open (was checked in
		   * beginning of case).
		   */
		  request = 0;

		  LWROC_SET_CONTROL_STATUS(_file_ctrl, FAILED,
					   PERFORMER,
					   "Failed to open file for writing "
					   "(existing?).");
		}

	    open_fail_hold:
	      /* We *always* go to hold mode after a failed open.
	       *
	       * It makes it more likely that the user realises that
	       * there is a problem.
	       *
	       * It also solves the problem of a failure with an open
	       * when in hold mode - should return to hold.
	       */

	      LWROC_WARNING("File writer in hold mode after failed open.");

	      _lwroc_mon_file->_filewr[0]._state = LWROC_FILEWR_STATE_HOLD;
	      file_state = LWROC_FW_STATE_HOLD;
	      break;
	    }

	  /* Open was successful! */
	  FALL_THROUGH;

	case LWROC_FW_STATE_OPENED: /* Only directly from start. */

	  LWROC_ACTION_FMT("Opened output file '%s'.",
			   _lwroc_fw_state->_filename);

	  gettimeofday(&now, NULL);

	  lwroc_file_writer_log(_lwroc_fw_state->_filename, 0,
				(uint64_t) -1, 0,
				now.tv_sec, 0);

	  timerclear(&write_fail_since);
	  timerclear(&last_write_at);
	  _lwroc_mon_file->_file._files += 1;

	  _lwroc_mon_file->_filewr[1]._state = LWROC_FILEWR_STATE_OPEN;
	  _lwroc_mon_file->_filewr[1]._bytes = 0;
	  strncpy(_lwroc_mon_file->_filewr[1]._filename,
		  _lwroc_fw_state->_filename,
		  sizeof (_lwroc_mon_file->_filewr[1]._filename));
	  _lwroc_mon_file->_filewr[1]._filename
	    [sizeof (_lwroc_mon_file->_filewr[1]._filename) - 1] = 0;

	  _lwroc_mon_file->_filewr[1]._open_close_time._sec  =
	    (uint64_t) now.tv_sec;
	  _lwroc_mon_file->_filewr[1]._open_close_time._nsec =
	    (uint32_t) now.tv_usec * 1000;

	  fileopen_at = now.tv_sec;
	  filesize = 0;
	  lwroc_crc32_init(&filecrc32);
	  fileevents = 0;

	  if (request)
	    {
	      /* Cannot have been a close/hold - see above. */
	      request = 0;
	      /* Declare success.  Even if we so far have written nothing.
	       * (Well, request was for 'open' :-) )
	       */
	      LWROC_SET_CONTROL_STATUS(_file_ctrl, SUCCESS,
				       PERFORMER, NULL);
	    }

	  file_state = LWROC_FW_STATE_FILE_HEADER_PREPARE;
	  break;

	case LWROC_FW_STATE_FILE_HEADER_PREPARE: /* transient */
	  /* Init, and begin by writing file header. */

	  _lwroc_fw_state->_file_fmt->
	    chunks_init(_lwroc_fw_state->_bufchunks,
			_lwroc_file_buffer_size, 1);

	  /* TODO: fill out the fields of the file header! */
	  _lwroc_fw_state->_file_fmt->
	    chunks_file_header(_lwroc_fw_state->_bufchunks);

	  file_state = LWROC_FW_STATE_FILE_HEADER_WRITE;
	  /* Newfile requests will be handled by later states once
	   * we've completed the header.  I.e. try to avoid incomplete
	   * file headers.
	   */
	  goto write_data;

	case LWROC_FW_STATE_STICKY_COMPACT: /* transient */
	  /* Since file writing never looses/drops events (at least once a
	   * file is open), we can write the stored sticky data
	   * directly from the sticky store.  (There will not be a
	   * need to fast forward through the input buffer, as we will
	   * hold that until we have written the old sticky events.)
	   *
	   * To be able to efficiently write directly from the store,
	   * first compactify the data.  That will make it continuous!
	   * (It will remain continuous as long as no new events are
	   * injected.)
	   */
	  lwroc_lmd_sticky_store_compact_data(sticky_store);
	  /* We begin at the first event in the sticky store. */
	  sticky_iter_ev_offset = 0;

	  file_state = LWROC_FW_STATE_STICKY_PREPARE;
	  FALL_THROUGH;

	case LWROC_FW_STATE_STICKY_PREPARE:
	  if (lwroc_lmd_sticky_store_write_chunks(sticky_store,
						  _lwroc_fw_state->
						  _lmd_bufchunks,
						  &sticky_iter_ev_offset, 0))
	    {
	      /* Since lbc->fully_used is not set (i.e. still at 0),
	       * is is harmless to call lwroc_lmd_buffer_chunks_release().
	       * (No real data will be released).
	       */
#if DEBUG_FILE_WRITER_PRINT
	      fprintf (stderr,"replayed %zd %zd\n",
		       bufchunks.fully_used,
		       bufchunks.chunks[LWROC_LMD_BUF_CHUNK_DATA_1].size);
#endif
	      file_state = LWROC_FW_STATE_STICKY_WRITE;
	      goto write_data;
	    }
	  /* No (more) sticky data. */
	  sticky_iter_ev_offset = (size_t) -1;
	  file_state = LWROC_FW_STATE_BUFFER_COLLECT;
	  FALL_THROUGH;

	case LWROC_FW_STATE_BUFFER_COLLECT:
	{
	  struct timeval t_diff;
	  int terminate;

	  gettimeofday(&now, NULL);
	  timersub(&now, &last_write_at, &t_diff);

	  /* printf ("since last write: %d\n", (int) t_diff.tv_sec); */

	  if (t_diff.tv_sec < 0 ||
	      t_diff.tv_sec >= _lwroc_fw_state->_flush_timeout)
	    partial_buffer = 1;

	  terminate = inst->_terminate;
	  /* Any request that we have (CLOSE, HOLD, NEW) wants to
	   * close this file now, so no need to check which request.
	   */
	  if (request ||
	      terminate)
	    partial_buffer = 1;

	  if (!_lwroc_fw_state->_file_fmt->
	      chunks_collect(_lwroc_fw_state->_bufchunks,
			     _lwroc_main_data_cons_file,
			     _lwroc_file_writer_thread->
			     /* */ _block,
			     NULL, sticky_store,
			     partial_buffer, 0))
	    {
	      struct timeval timeout;

	      /* Only do if terminate was set _before_ the check, such
	       * that data did not appear after the check, but before
	       * terminate was set.
	       */
	      if (terminate)
		{
		  /* Do not try to open a new file. */
		  if (!request)
		    request = LWROC_FW_REQUEST_CLOSE;
		}

	      if (request)
		{
		  /* There is no more data, close the file. */
		  file_state = LWROC_FW_STATE_CLOSE;
		  break;
		}

	      /* Try again in 0.1 s. */
	      /* This also means that we will not answer control requests
	       * until this timeout is over.  TODO: Should be fixed...
	       */
	      timeout.tv_sec = 0;
	      timeout.tv_usec = 100000;
	      lwroc_thread_block_get_token_timeout(_lwroc_file_writer_thread->
						   /* */ _block,
						   &timeout);
	      break;
	    }
#if DEBUG_FILE_WRITER_PRINT
	  fprintf (stderr,"collected %zd %zd\n",
		   bufchunks.fully_used,
		   bufchunks.chunks[LWROC_LMD_BUF_CHUNK_DATA_1].size);
#endif
	  file_state = LWROC_FW_STATE_BUFFER_WRITE;
	}
	  FALL_THROUGH;

	case LWROC_FW_STATE_FILE_HEADER_WRITE:
	case LWROC_FW_STATE_STICKY_WRITE:
	case LWROC_FW_STATE_BUFFER_WRITE:
	  ;
	write_data:
	{
	  int unjam_new = 0;

	  /*
	  LWROC_LOG_FMT("write_data: "
			"{ %p, %" MYPRIzd " }, "
			"{ %p, %" MYPRIzd " }, "
			"{ %p, %" MYPRIzd " }, "
			"{ %p, %" MYPRIzd " }, "
			"{ %p, %" MYPRIzd " }, "
			"%zd %d",
			bufchunks.chunks[0].ptr, bufchunks.chunks[0].size,
			bufchunks.chunks[1].ptr, bufchunks.chunks[1].size,
			bufchunks.chunks[2].ptr, bufchunks.chunks[2].size,
			bufchunks.chunks[3].ptr, bufchunks.chunks[3].size,
			bufchunks.chunks[4].ptr, bufchunks.chunks[4].size,
			bufchunks.fully_used, bufchunks.fixed.bufhe.l_buf);
	  */

#if DEBUG_FILE_WRITER_PRINT
	  fprintf (stderr,"write %zd %zd, bufno: %d %08x %08x %08x %08x=%d\n",
		   bufchunks.fully_used,
		   bufchunks.chunks[LWROC_LMD_BUF_CHUNK_DATA_1].size,
		   bufchunks.fixed.bufhe.l_buf,
		   ((uint32_t*) bufchunks.chunks[LWROC_LMD_BUF_CHUNK_DATA_1].
		    ptr)[0],
		   ((uint32_t*) bufchunks.chunks[LWROC_LMD_BUF_CHUNK_DATA_1].
		    ptr)[1],
		   ((uint32_t*) bufchunks.chunks[LWROC_LMD_BUF_CHUNK_DATA_1].
		    ptr)[2],
		   ((uint32_t*) bufchunks.chunks[LWROC_LMD_BUF_CHUNK_DATA_1].
		    ptr)[3],
		   ((uint32_t*) bufchunks.chunks[LWROC_LMD_BUF_CHUNK_DATA_1].
		    ptr)[3]);
#endif
	  if (!lwroc_file_writer_write(_lwroc_fw_state->_bufchunks,
				       &filesize, &filecrc32))
	    {
	      struct timeval t_diff;

	      /* Write has failed.  We should try again. */

	      failed_writes++;

	      if (!timerisset(&write_fail_since))
		gettimeofday(&write_fail_since, NULL);

	      gettimeofday(&now, NULL);

	      timersub(&now, &write_fail_since, &t_diff);

	      if (t_diff.tv_sec >= LWROC_FILE_WRITE_FAIL_JAM_TIMEOUT)
		{
		  is_jammed = 1;
		  _lwroc_mon_file->_filewr[1]._state =
		    LWROC_FILEWR_STATE_JAMMED;

		  LWROC_WARNING_FMT("File writing is failing since %d s "
				    "(%d attempts).  "
				    "(If the problem persists, use "
				    "'lwrocctrl --file-unjam' to abort "
				    "the current file (goes to hold mode).",
				    (int) t_diff.tv_sec, failed_writes);
		}
	      else
		  LWROC_WARNING_FMT("File writing is failing since %d s "
				    "(%d attempts).  ",
				    (int) t_diff.tv_sec, failed_writes);

	      /* Exception is if we have a request to UNJAM, which we
	       * take after failing for a certain amount of time.
	       *
	       * At least for the moment, we also accept HOLD and CLOSE.
	       *
	       * Also take newfile.  Then we have no race condition
	       * between accepting the request as such and trying to
	       * perform it.
	       *
	       * TODO: requests should time out if not completed
	       * within a reasonable time...
	       */

	      if (is_jammed && (autosize || autotime || autoevents) &&
		  (_lwroc_fw_info->_funcs->_flags &
		   LWROC_FILE_WRITER_FLAGS_AUTO_UNJAM) &&
		  t_diff.tv_sec >= 2 * LWROC_FILE_WRITE_FAIL_JAM_TIMEOUT)
		{
		  LWROC_ERROR("Automatic unjam after failing write - "
			      "trying new file.");
		  /* Fall through down, so we release the current buffer
		   * of data.
		   */
		  unjam_new = 1;
		}
	      else if (is_jammed /* timeout (10 s?) */ &&
		       (request == LWROC_FW_REQUEST_CLOSE ||
			request == LWROC_FW_REQUEST_HOLD ||
			request == LWROC_FW_REQUEST_UNJAM ||
			request == LWROC_FW_REQUEST_NEWFILE))
		{
		  LWROC_WARNING("Unjam request - giving up on failing write.");
		  /* Fall through down, so we release the current buffer
		   * of data.
		   */
		}
	      else
		{
		  /* TODO: A potential lock-up is if user has
		   * requested a new file, but the writes fail.  Then
		   * we never get out of this, as the request is also
		   * not revoked, even if the control circuit itself
		   * has timed out...
		   */

		  break;
		}
	    }

	  /* Even if the write has failed, we declare the buffer as
	   * written.
	   *
	   * We simply do not know how much of the data made it, and we
	   * definitely do not want to write it twice!
	   */

#if DEBUG_FILE_WRITER_PRINT
	  fprintf (stderr,"release %zd %zd\n",
		   bufchunks.fully_used,
		   bufchunks.chunks[LWROC_LMD_BUF_CHUNK_DATA_1].size);
#endif
	  lwroc_gdf_buffer_chunks_release(_lwroc_fw_state->_bufchunks,
					  _lwroc_main_data_cons_file,
					  &done_events);

	  gettimeofday(&last_write_at, NULL);

	  _lwroc_mon_file->_file._buffers += 1;
	  _lwroc_mon_file->_file._events  += done_events;
	  fileevents += done_events;

	  /* We've gotten out of jammed state. */
	  is_jammed = 0;
	  timerclear(&write_fail_since);
	  failed_writes = 0;
	  _lwroc_mon_file->_filewr[1]._state = LWROC_FILEWR_STATE_OPEN;

	  /*
	  printf ("%d, %" PRIu64", %" PRIu64"\n",
		  file_state == LWROC_FW_STATE_COLLECT_BUFFER,
		  autosize,
		  filesize + _lwroc_file_buffer_size);
	  */
	  if (request == LWROC_FW_REQUEST_CLOSE ||
	      request == LWROC_FW_REQUEST_HOLD ||
	      request == LWROC_FW_REQUEST_UNJAM)
	    {
	      file_state = LWROC_FW_STATE_CLOSE;
	    }
	  else if (request == LWROC_FW_REQUEST_NEWFILE)
	    {
	      /* If the user has requested a new file, we do that even
	       * if we have not written any data buffer or sticky
	       * replay buffer yet.  Or even incomplete sticky replay.
	       * We have however written the file header if we reach
	       * this point, so the file is not corrupt.
	       */
	      file_state = LWROC_FW_STATE_NEW_FILE;
	    }
	  else if (unjam_new)
	    {
	      file_state = LWROC_FW_STATE_NEW_FILE;
	    }
	  else if (file_state == LWROC_FW_STATE_FILE_HEADER_WRITE)
	    {
	      file_state = LWROC_FW_STATE_STICKY_COMPACT;
	    }
	  else if (file_state == LWROC_FW_STATE_STICKY_WRITE)
	    {
	      file_state = LWROC_FW_STATE_STICKY_PREPARE;
	    }
	  else if (file_state == LWROC_FW_STATE_BUFFER_WRITE)
	    {
	      time_t elapsed;

	      gettimeofday(&now, NULL);

	      elapsed = now.tv_sec -
		((time_t) _lwroc_mon_file->_filewr[1]._open_close_time._sec);

	      if (autotime &&
		  (elapsed >= autotime || elapsed < -10))
		{
		  file_state = LWROC_FW_STATE_NEW_FILE;
		}
	      else if (autosize &&
		       filesize + _lwroc_file_buffer_size > autosize)
		{
		  file_state = LWROC_FW_STATE_NEW_FILE;
		}
	      else if (autoevents &&
		       fileevents > autoevents)
		{
		  file_state = LWROC_FW_STATE_NEW_FILE;
		}
	      else
		file_state = LWROC_FW_STATE_BUFFER_COLLECT;
	    }
	  else
	    LWROC_BUG_FMT("Unknown file_writer state (%d) after write.",
			  file_state);

	  break;
	}
	default:
	  LWROC_BUG_FMT("Unknown file_writer state (%d).",
			file_state);
	  break;
	}

      /* TODO: use force update on file state change. */
      LWROC_MON_CHECK_COPY_BLOCK(_lwroc_mon_file_handle,
				 &(_lwroc_mon_file->_file),0);
    }

  lwroc_lmd_sticky_store_deinit(sticky_store);

  _lwroc_fw_info->_funcs->free(_lwroc_fw_info);
  _lwroc_fw_info = NULL;
}

/********************************************************************/

lwroc_thread_info _file_writer_thread_info =
{
  lwroc_file_writer_thread_at_prepare,
  lwroc_file_writer_thread_loop,
  LWROC_MSG_BUFS_FILE,
  "file writer",
  0,
  LWROC_THREAD_TERM_FILE,
  LWROC_THREAD_CORE_PRIO_FILE_WRITE,
};

/********************************************************************/

lwroc_thread_instance *_lwroc_file_writer_thread = NULL;

void lwroc_prepare_file_writer_thread(void)
{
  _lwroc_file_writer_thread =
    lwroc_thread_prepare(&_file_writer_thread_info);
}

/********************************************************************/

void lwroc_file_writer_usage(void)
{
  printf ("\n");
  printf ("--file-writer=<OPTIONS>:\n");
  printf ("\n");
  printf ("  flush=N                  Flush data every N seconds.\n");
  printf ("  bufsize=N                Size of output protocol buffers (Mi|raw hex).\n");
  printf ("  output=FILENAME          Destination file (.lmd) at start.\n");
  printf ("  max-events=N             Maximum number of events in initial file.\n");
  printf ("  terminate-on-close       Terminate program after file close.\n");
  printf ("\n");
}

void lwroc_file_writer_cfg(const char *cfg)
{
  size_t bufsize = 0;
  const char *request;
  lwroc_parse_split_list parse_info;
  lwroc_file_writer_info* tsm_info;

  _lwroc_fw_state = malloc (sizeof (lwroc_file_writer_state));

  if (!_lwroc_fw_state)
    LWROC_FATAL("Failure allocating memory for file writer state.");

  memset(_lwroc_fw_state, 0, sizeof (lwroc_file_writer_state));

  _lwroc_fw_state->_flush_timeout = LWROC_FILEWRITE_PARTIAL_BUFFER_TIMEOUT;

  /* Alloc default file writer */
  _lwroc_fw_info = lwroc_fd_writer_alloc();

  lwroc_parse_split_list_setup(&parse_info, cfg, ',');

  while ((request = lwroc_parse_split_list_next(&parse_info)) != NULL)
    {
      const char *post;

      if (LWROC_MATCH_C_ARG("help"))
	{
	  lwroc_file_writer_usage();
	  exit(0);
	}
      else if (LWROC_MATCH_C_PREFIX("bufsize=",post))
	{
	  bufsize =
	    (size_t) lwroc_parse_hex(post, "file buffer size");
	}
      else if (LWROC_MATCH_C_PREFIX("output=",post))
	{
	  if (strlen(post) > sizeof(_lwroc_fw_state->_filename) - 1)
	    LWROC_BADCFG_FMT("Output file name too long: %s", request);
	  strcpy(_lwroc_fw_state->_filename, post);
	}
      else if (LWROC_MATCH_C_PREFIX("max-events=",post))
	{
	  _lwroc_fw_state->_max_events =
	    lwroc_parse_hex(post, "file maximum number of events");
	}
      else if (LWROC_MATCH_C_PREFIX("flush=",post))
	{
	  _lwroc_fw_state->_flush_timeout =
	    lwroc_parse_time(post, "file flush timeout");
	}
      else if (LWROC_MATCH_C_ARG("terminate-on-close"))
	{
	  _lwroc_fw_state->_terminate_on_close = 1;
	}
      else
	LWROC_BADCFG_FMT("Unrecognised server option: %s", request);
    }

  if (_lwroc_fw_state->_max_events &&
      !_lwroc_fw_state->_filename[0])
    LWROC_BADCFG("Maximum number of events "
		 "cannot be given without filename.");

  tsm_info = lwroc_tsm_writer_alloc();

  if (tsm_info)
    {
      /* We brutally discard the normal file writer.
       * When we fix multiple writers it will be a list, so
       * then no need to free it.  Let's leak for the moment.
       */
      _lwroc_fw_info = tsm_info;
    }

  _lwroc_file_buffer_size = (uint32_t) bufsize;
}

/********************************************************************/

void lwroc_file_writer_init(lwroc_data_pipe_handle *data_handle)
{
  lwroc_pipe_buffer_control *pipe_ctrl;
  size_t pipe_size;
  uint32_t max_ev_len;

  pipe_ctrl = lwroc_data_pipe_get_pipe_ctrl(data_handle);
  pipe_size = lwroc_pipe_buffer_size(pipe_ctrl);
  max_ev_len = lwroc_data_pipe_get_max_ev_len(data_handle);

  _lwroc_fw_state->_file_fmt = lwroc_data_pipe_get_fmt(data_handle);

  _lwroc_fw_state->_bufchunks =
    _lwroc_fw_state->_file_fmt->chunks_alloc(LWROC_GDF_FLAGS_FILE);

  if (_lwroc_fw_state->_file_fmt == &_lwroc_lmd_format_functions)
    {
      /* For LMD format, we have explicit handling of sticky data. */

      _lwroc_fw_state->_lmd_bufchunks =
	(lwroc_lmd_buffer_chunks *) _lwroc_fw_state->_bufchunks;
    }
  else if (_lwroc_fw_state->_file_fmt == &_lwroc_ebye_format_functions)
    {
      /* No special handling. */
    }
  else if (_lwroc_fw_state->_file_fmt == &_lwroc_xfer_format_functions)
    {
      /* No special handling. */
    }
  else
    {
      LWROC_FATAL_FMT("File writer cannot handle format of data pipe (%s).",
		      _lwroc_fw_state->_file_fmt->_name);
    }

  /* _lwroc_file_buffer_size = ; */

  if (!_lwroc_file_buffer_size)
    {
      _lwroc_file_buffer_size = 2 * max_ev_len + 512; /* 512 for headers */;
      _lwroc_file_buffer_size =
	(uint32_t) lwroc_power_2_round_up(_lwroc_file_buffer_size);

      if (_lwroc_file_buffer_size <
	  _lwroc_fw_state->_file_fmt->_default_file_buffer_size)
	_lwroc_file_buffer_size =
	  _lwroc_fw_state->_file_fmt->_default_file_buffer_size;
    }

  lwroc_net_check_bufsize_limits(_lwroc_file_buffer_size,
				 pipe_size, max_ev_len,
				 NULL, "file writer");

  lwroc_crc32_table_init();

  _lwroc_fw_info->_funcs->init(_lwroc_fw_info);

  _lwroc_fw_state->_start_state = LWROC_FW_STATE_CLOSED;

  if (_lwroc_fw_state->_filename[0])
    {
      if (!_lwroc_fw_info->_funcs->open(_lwroc_fw_state->_filename,
					_lwroc_fw_info))
	LWROC_BADCFG("Opening initial output file failed.");

      _lwroc_fw_state->_start_state = LWROC_FW_STATE_OPENED;
    }
}

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Bastian Loeher  <b.loeher@gsi.de>
 * Copyright (C) 2020  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_main_iface.h"
#include "lwroc_filter_loop.h"
#include "lwroc_filter.h"
#include "lwroc_filter_ts.h"
#include "lwroc_message.h"
#include "lwroc_message_internal.h"

#include <string.h>

struct lwroc_filter_functions _lwroc_filter_functions[LWROC_FILTER_NUM];

struct lwroc_filter_functions *_lwroc_user_filter_functions =
  &_lwroc_filter_functions[LWROC_FILTER_USER];

void lwroc_main_filter_pre_parse_setup(void)
{
  int fltno;

  /* This function may only initialise structures. */

  for (fltno = 0; fltno < LWROC_FILTER_NUM; fltno++)
    {
      memset(&_lwroc_filter_functions[fltno], 0,
	     sizeof(struct lwroc_filter_functions));
    }

  lwroc_user_filter_pre_setup_functions(); /* LWROC_FILTER_USER */

  /* LWROC_FILTER_TS is statically set up. */

  lwroc_ts_filter_pre_setup_functions(&_lwroc_filter_functions[LWROC_FILTER_TS]);

}

void lwroc_main_filter_cmdline_usage(void)
{
  int fltno;

  for (fltno = 0; fltno < LWROC_FILTER_NUM; fltno++)
    {
      if (_lwroc_filter_functions[fltno].cmdline_fcns.usage)
	_lwroc_filter_functions[fltno].cmdline_fcns.usage();
    }
}

int lwroc_main_filter_parse_cmdline_arg(const char *request)
{
  int fltno;
  int ret;

  /*NOHELPCALL*/ /* We are a option subset, --help handled elsewhere. */

  for (fltno = 0; fltno < LWROC_FILTER_NUM; fltno++)
    if (_lwroc_filter_functions[fltno].cmdline_fcns.parse_arg)
      {
	ret = _lwroc_filter_functions[fltno].cmdline_fcns.parse_arg(request);

	if (ret)
	  return ret;
      }
  return 0;
}

void lwroc_filter_conn_monitor_fetch(void *ptr)
{
  lwroc_net_conn_monitor *conn_mon = (lwroc_net_conn_monitor *)
    (((char *) ptr) - offsetof(lwroc_net_conn_monitor, _block));

  /* Fills conn_mon->_block._buf_data_fill. */
  lwroc_net_conn_monitor_fetch(ptr);

  /* printf (" %olld\n", (long long) conn_mon->_block._buf_data_fill); */

  (void) conn_mon;

  /* printf ("Updated: ev %d\n", (int) conn_mon->_block._events); */
}

void lwroc_main_filter_setup(int fltno,
			     const char *pipe_name)
{
  if (_lwroc_filter_functions[fltno].loop)
    {
      lwroc_mon_block_fetch fetch_fcn =
	lwroc_filter_conn_monitor_fetch;

      lwroc_pipe_buffer_control *pipe_buf;
      lwroc_data_pipe_extra *pipe_extra;

      uint32_t kind = 0;
      uint32_t kind_sub = 0;

      _lwroc_filter_data_handle[fltno] = lwroc_get_data_pipe(pipe_name);

      if (!_lwroc_filter_data_handle[fltno])
	LWROC_FATAL_FMT("No handle for data pipe %s.", pipe_name);

      pipe_buf =
	lwroc_data_pipe_get_pipe_ctrl(_lwroc_filter_data_handle[fltno]);

      pipe_extra =
	lwroc_data_pipe_get_pipe_extra(_lwroc_filter_data_handle[fltno]);

      lwroc_data_pipe_extra_clear(pipe_extra);

      _lwroc_filter_conn_mon[fltno] =
	lwroc_net_conn_monitor_init(LWROC_CONN_TYPE_FILTER,
				    LWROC_CONN_DIR_OUTGOING,
				    NULL, NULL, NULL,
				    pipe_buf, NULL, NULL, pipe_extra, NULL,
				    fetch_fcn,
				    kind, kind_sub);
    }

  if (_lwroc_filter_functions[fltno].setup)
    _lwroc_filter_functions[fltno].setup();
}

int lwroc_has_filter(int fltno)
{
  return _lwroc_filter_functions[fltno].loop != NULL;
}

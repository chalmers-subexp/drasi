/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Bastian Loeher  <b.loeher@gsi.de>
 * Copyright (C) 2020  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_FILTER_H__
#define __LWROC_FILTER_H__

#include "lwroc_thread_util.h"
#include "lwroc_pipe_buffer.h"
#include "lwroc_data_pipe.h"
#include "lwroc_filters.h"
#include "lwroc_cmdline_fcns.h"
#include "gdf/lwroc_gdf_util.h"

typedef void  (*lwroc_filter_setup_func)(void);
typedef void  (*lwroc_filter_init_func)(void);
typedef void  (*lwroc_filter_uninit_func)(void);
typedef void  (*lwroc_filter_loop_func)(lwroc_pipe_buffer_consumer *pipe_buf,
					const lwroc_thread_block *thread_block,
					lwroc_data_pipe_handle *data_handle,
					volatile int *terminate);
typedef void  (*lwroc_filter_max_ev_len_func)(uint32_t *max_len,
					      uint32_t *add_len);

/* Interface for user code */
struct lwroc_filter_functions
{
  const char                     *name;
  lwroc_filter_setup_func         setup; /* No massive logging yet! */
  lwroc_filter_init_func          init;  /* Do most init _here_. */
  lwroc_filter_uninit_func        uninit;
  lwroc_filter_loop_func          loop;
  struct lwroc_cmdline_functions  cmdline_fcns;
  lwroc_gdf_format_functions     *fmt;   /* The data format generated. */
  lwroc_filter_max_ev_len_func    max_ev_len;
};

/********************************************************************/

extern struct lwroc_filter_functions _lwroc_filter_functions[LWROC_FILTER_NUM];

extern struct lwroc_filter_functions *_lwroc_user_filter_functions;

extern struct lwroc_filter_functions *_lwroc_user_filter_functions;

/* This function needs to be called to setup the user functions. */
void lwroc_user_filter_pre_setup_functions(void);

#endif/*__LWROC_FILTER_H__*/

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2020  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_filter_loop.h"
#include "lwroc_filter.h"
#include "lwroc_thread_block.h"
#include "lwroc_message_internal.h"
#include "lwroc_main_iface.h"

#include <string.h>
#include <assert.h>

#include "../dtc_arch/acc_def/myinttypes.h"

#include "gen/lwroc_message_source_serializer.h"
#include "gen/lwroc_monitor_filter_source_serializer.h"
#include "gen/lwroc_monitor_filter_block_serializer.h"

/********************************************************************/

lwroc_monitor_filter_block  _lwroc_mon_filter[LWROC_FILTER_NUM];
lwroc_mon_block            *_lwroc_mon_filter_handle[LWROC_FILTER_NUM] =
  { NULL, NULL };
lwroc_net_conn_monitor     *_lwroc_filter_conn_mon[LWROC_FILTER_NUM] =
  { NULL, NULL };

char *lwroc_mon_filter_serialise(char *wire, void *block)
{
  lwroc_monitor_filter_block *filter_block =
    (lwroc_monitor_filter_block *) block;

  return lwroc_monitor_filter_block_serialize(wire, filter_block);
}

/********************************************************************/

int lwroc_filter_no_from_msg_buf_source(int msg_buf_source)
{
  if (msg_buf_source == LWROC_MSG_BUFS_USER_FILTER)
    return LWROC_FILTER_USER;
  if (msg_buf_source == LWROC_MSG_BUFS_TS_FILTER)
    return LWROC_FILTER_TS;

  LWROC_BUG_FMT("Unknown msg_buf_source %d.", msg_buf_source);

  return -1; /* unreachable */
}

void lwroc_filter_thread_at_prepare(lwroc_thread_instance *inst)
{
  int fltno =
    lwroc_filter_no_from_msg_buf_source(inst->_info->_msg_buf_source);

  (void) inst;

  {
    lwroc_message_source filter_src_source;
    lwroc_message_source_sersize sz_filter_src_source;
    lwroc_monitor_filter_source filter_src;
    char *wire;

    if (_lwroc_filter_functions[fltno].name)
      {
	strncpy(filter_src._label, _lwroc_filter_functions[fltno].name,
		sizeof (filter_src._label));
	filter_src._label[sizeof (filter_src._label)-1] = 0;
      }
    else
      filter_src._label[0] = 0;

    lwroc_mon_source_set(&filter_src_source);

    memset (&_lwroc_mon_filter, 0, sizeof (_lwroc_mon_filter));
    _lwroc_mon_filter_handle[fltno] =
      lwroc_reg_mon_block(0, &_lwroc_mon_filter, sizeof (_lwroc_mon_filter),
			  lwroc_monitor_filter_block_serialized_size(),
			  lwroc_mon_filter_serialise,
			  lwroc_message_source_serialized_size(&filter_src_source,
							       &sz_filter_src_source) +
			  lwroc_monitor_filter_source_serialized_size(), NULL,
			  NULL);

    wire = _lwroc_mon_filter_handle[fltno]->_ser_source;

    wire = lwroc_message_source_serialize(wire, &filter_src_source,
					  &sz_filter_src_source);
    wire = lwroc_monitor_filter_source_serialize(wire, &filter_src);

    assert (wire == (_lwroc_mon_filter_handle[fltno]->_ser_source +
		     _lwroc_mon_filter_handle[fltno]->_ser_source_size));
  }
}

/********************************************************************/

lwroc_pipe_buffer_consumer *_lwroc_filter_source_consumer[LWROC_FILTER_NUM] =
  { NULL, NULL };
lwroc_data_pipe_handle *_lwroc_filter_data_handle[LWROC_FILTER_NUM] =
  { NULL, NULL };

lwroc_gdf_format_functions *lwroc_main_filter_get_fmt(int fltno)
{
  return _lwroc_filter_functions[fltno].fmt;
}

uint32_t lwroc_main_filter_get_max_ev_len(int fltno,
					  uint32_t parent_max_len)
{
  uint32_t max_len = 0;
  uint32_t add_len = 0;

  if (_lwroc_filter_functions[fltno].max_ev_len)
    _lwroc_filter_functions[fltno].max_ev_len(&max_len, &add_len);

  if (!max_len)
    max_len = parent_max_len;

  max_len += add_len;

  return max_len;
}

void lwroc_main_filter_get_consumer(int fltno,
				    lwroc_data_pipe_handle *data_handle,
				    int *pipe_cons_no)
{
  lwroc_pipe_buffer_control *pipe_ctrl;
  lwroc_pipe_buffer_consumer *source;

  pipe_ctrl = lwroc_data_pipe_get_pipe_ctrl(data_handle);

  source =
    lwroc_pipe_buffer_get_consumer(pipe_ctrl, (*pipe_cons_no)++);

  _lwroc_filter_source_consumer[fltno] = source;
}

/********************************************************************/

void lwroc_filter_thread_loop(lwroc_thread_instance *inst)
{
  int fltno =
    lwroc_filter_no_from_msg_buf_source(inst->_info->_msg_buf_source);

  (void) inst;

  LWROC_INFO("call filter_init...");

  /* This is before the filter_functions.init() on purpose.
   *
   * Those init functions might generate a lot of log messages, and
   * therefore must happen after we have a serious log destination.
   */
  lwroc_msg_wait_any_msg_client(_lwroc_filter_thread[fltno]->_block);

  if (_lwroc_filter_functions[fltno].init)
    _lwroc_filter_functions[fltno].init();

  /* There better be a filter function, or we have no purpose... */
  assert (_lwroc_filter_functions[fltno].loop);
  _lwroc_filter_functions[fltno].loop(_lwroc_filter_source_consumer[fltno],
				      _lwroc_filter_thread[fltno]->_block,
				      _lwroc_filter_data_handle[fltno],
				      &(inst->_terminate));

  if (_lwroc_filter_functions[fltno].uninit)
    _lwroc_filter_functions[fltno].uninit();
}

/********************************************************************/

lwroc_thread_info _lwroc_filter_thread_info[LWROC_FILTER_NUM] =
{
  {
    lwroc_filter_thread_at_prepare,
    lwroc_filter_thread_loop,
    LWROC_MSG_BUFS_USER_FILTER,
    "filter",
    0,
    LWROC_THREAD_TERM_FILTER,
    LWROC_THREAD_CORE_PRIO_FILTER,
  },
  {
    lwroc_filter_thread_at_prepare,
    lwroc_filter_thread_loop,
    LWROC_MSG_BUFS_TS_FILTER,
    "ts-filter",
    0,
    LWROC_THREAD_TERM_TS_FILTER,
    LWROC_THREAD_CORE_PRIO_TS_FILTER,
  }
};

/********************************************************************/

lwroc_thread_instance *_lwroc_filter_thread[LWROC_FILTER_NUM] = { NULL, NULL };

void lwroc_prepare_filter_thread(int fltno)
{
  _lwroc_filter_thread[fltno] =
    lwroc_thread_prepare(&_lwroc_filter_thread_info[fltno]);
}


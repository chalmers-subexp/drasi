/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2020  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_FILTER_LOOP_H__
#define __LWROC_FILTER_LOOP_H__

#include "lwroc_net_client_base.h"
#include "lwroc_message.h"
#include "lwroc_data_pipe.h"
#include "lwroc_thread_util.h"
#include "lwroc_filters.h"

/********************************************************************/

extern lwroc_monitor_filter_block  _lwroc_mon_filter[LWROC_FILTER_NUM];
extern lwroc_mon_block            *_lwroc_mon_filter_handle[LWROC_FILTER_NUM];
extern lwroc_net_conn_monitor     *_lwroc_filter_conn_mon[LWROC_FILTER_NUM];

/********************************************************************/

extern lwroc_thread_instance *_lwroc_filter_thread[LWROC_FILTER_NUM];

void lwroc_prepare_filter_thread(int fltno);

/********************************************************************/

extern lwroc_data_pipe_handle *_lwroc_filter_data_handle[LWROC_FILTER_NUM];

/********************************************************************/

void lwroc_main_filter_pre_parse_setup(void);
void lwroc_main_filter_cmdline_usage(void);
int lwroc_main_filter_parse_cmdline_arg(const char *request);
void lwroc_main_filter_setup(int fltno,
			     const char *pipe_name);

lwroc_gdf_format_functions *lwroc_main_filter_get_fmt(int fltno);
uint32_t lwroc_main_filter_get_max_ev_len(int fltno,
					  uint32_t parent_max_len);
void lwroc_main_filter_get_consumer(int fltno,
				    lwroc_data_pipe_handle *data_handle,
				    int *pipe_cons_no);

int lwroc_has_filter(int fltno);

/********************************************************************/

#define LWROC_FILTER_MON_CHECK(fltno, flush) do {			\
    LWROC_MON_CHECK_COPY_BLOCK(_lwroc_mon_filter_handle[fltno],		\
			       &_lwroc_mon_filter[fltno],		\
			       flush);					\
    LWROC_MON_CHECK_COPY_CONN_MON_BLOCK(_lwroc_filter_conn_mon[fltno],	\
					flush);				\
  } while (0)

/* Unused: useful? */
#define LWROC_FILTER_MON_PENDING(fltno) \
  LWROC_MON_UPDATE_PENDING(_lwroc_mon_filter_handle[fltno])

/********************************************************************/

#endif/*__LWROC_FILTER_LOOP_H__*/

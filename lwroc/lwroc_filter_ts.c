/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2021  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_filter.h"
#include "lwroc_filter_ts.h"
#include "lwroc_message.h"
#include "lwroc_mon_block.h"
#include "lwroc_filter_loop.h"
#include "lwroc_parse_util.h"
#include "lmd/lwroc_lmd_util.h"

#include <string.h>

/*****************************************************************************/

#define LWROC_FILTER_TS_MODE_TITRIS         0x01
#define LWROC_FILTER_TS_MODE_WHITE_RABBIT   0x02

typedef struct lwroc_filter_ts_info_t
{
  int _mode;
} lwroc_filter_ts_info;

lwroc_filter_ts_info _lwroc_filter_ts_info = { 0 };

void lwroc_ts_filter_pre_setup_functions(struct lwroc_filter_functions *
					 filter_functions)
{
  filter_functions->cmdline_fcns.usage     = lwroc_filter_ts_cmdline_usage;
  filter_functions->cmdline_fcns.parse_arg = lwroc_filter_ts_parse_cmdline_arg;
  filter_functions->loop                   = lwroc_filter_ts_loop;
  filter_functions->name                   = "filter_ts";
  filter_functions->fmt                    = &_lwroc_lmd_format_functions;
  filter_functions->max_ev_len             = lwroc_filter_ts_max_ev_len;
}

/*****************************************************************************/

void lwroc_filter_ts_mode_opt(const char *mode)
{
  if (strcmp(mode,"wr") == 0)
    _lwroc_filter_ts_info._mode = LWROC_FILTER_TS_MODE_WHITE_RABBIT;
  else if (strcmp(mode,"titris") == 0)
    _lwroc_filter_ts_info._mode = LWROC_FILTER_TS_MODE_TITRIS;
  else
    LWROC_BADCFG_FMT("Unknown TS filter mode '%s'.", mode);
}

int lwroc_filter_ts_mode_set(void)
{
  return _lwroc_filter_ts_info._mode != 0;
}

void lwroc_filter_ts_cmdline_usage(void)
{
  printf ("  --ts-filter-mode=(wr|titris)  Timestamp filter mode.\n");
  printf ("\n");
}

int lwroc_filter_ts_parse_cmdline_arg(const char *request)
{
  const char *post;

  /*NOHELPCALL*/ /* We are a option subset, --help handled elsewhere. */

  if (LWROC_MATCH_C_PREFIX("--ts-filter-mode=",post)) {
    lwroc_filter_ts_mode_opt(post);
  }
  else
    return 0;
  return 1;
}

#define LWROC_FILTER_TS_MAX_ALLOC  1024

void lwroc_filter_ts_max_ev_len(uint32_t *max_len,
				 uint32_t *add_len)
{
  *max_len = LWROC_FILTER_TS_MAX_ALLOC;
  (void) add_len;
}

/* We send the timestamp data packaged in LMD events.  Type/subtype
 * 10/1 to allow easy handling by the normal routines.  We also place
 * a subevent header such that typical printing tools work without
 * complaints.
 *
 * Note: this is just for convenience.  The data format is only
 * intended for transmission between running DAQ instances, and not for
 * long-term storage.
 *
 * The rationale is to be able to use the existing data transmission
 * and reception routines with minimum changes, reducing the
 * cross-section for bugs.
 */

void lwroc_filter_ts_commit(lwroc_data_pipe_handle *data_handle,
			    char *dest,
			    uint32_t used, int flush)
{
  /* Write a header first in the buffer. */

  lmd_event_10_1_host *header = (lmd_event_10_1_host *) dest;

  lmd_subevent_10_1_host *subheader = (lmd_subevent_10_1_host *) (header + 1);

  header->_header.l_dlen    =
    DLEN_FROM_EVENT_DATA_LENGTH(used -
				(uint32_t) sizeof (header->_header));
  header->_header.i_type    = LMD_EVENT_10_1_TYPE;
  header->_header.i_subtype = LMD_EVENT_10_1_SUBTYPE;
  header->_info.i_trigger   = 0;
  header->_info.i_dummy     = 0;
  header->_info.l_count     = 0;

  subheader->_header.l_dlen    =
    DLEN_FROM_EVENT_DATA_LENGTH(used -
				(uint32_t) sizeof (header->_header) -
				(uint32_t) sizeof (lmd_event_10_1_host));
  subheader->_header.i_type    = LMD_SUBEVENT_TS_WR_TYPE;
  subheader->_header.i_subtype = LMD_SUBEVENT_TS_WR_SUBTYPE;
  subheader->h_control      = 0;
  subheader->h_subcrate     = 0;
  subheader->i_procid       = 0;

  lwroc_used_event_space(data_handle,
			 used,
			 flush);
}

void lwroc_filter_ts_loop(lwroc_pipe_buffer_consumer *pipe_buf,
			  const lwroc_thread_block *thread_block,
			  lwroc_data_pipe_handle *data_handle,
			  volatile int *terminate)
{
  const lmd_event_10_1_host *header;
  size_t size;
  lwroc_iterate_event iter_data;
  int ret;

  char *dest = NULL;

  uint32_t header_size;

  uint32_t alloc = LWROC_FILTER_TS_MAX_ALLOC;
  uint32_t used;

  uint64_t first_timestamp = 0;
  uint64_t timestamp = 0;

  time_t firstblock, now;

  header_size =
    (uint32_t) (sizeof (lmd_event_10_1_host) +
		sizeof (lmd_subevent_10_1_host));

  used = header_size;
  firstblock = 0;

  /* We search the stream for event, and get all timestamps.  The
   * timestamps are packaged into events, for consumption by
   * e.g. other nodes, when reordering data.
   *
   * We therefore never emit the original events.
   */

  for ( ; ; )
    {
      uint32_t *p_dest;

      /* Get next event from the source. */
      ret = lwroc_lmd_pipe_get_event(pipe_buf, thread_block,
				     &header, &size,
				     &iter_data);

      if (ret == LWROC_LMD_PIPE_GET_EVENT_STICKY ||
	  ret == LWROC_LMD_PIPE_GET_EVENT_INTERNAL)
	goto consumed_event;

      if (ret == LWROC_LMD_PIPE_GET_EVENT_FAILURE)
	{
	  LWROC_BUG("Timestamp filter failed to get event.");
	}
      if (ret == LWROC_LMD_PIPE_GET_EVENT_WOULD_BLOCK)
	{
	  struct timeval timeout;

	  if (!dest)
	    continue;

	  /* We do not allow data to pile up for more than a second
	   * before emitting the timestamp buffer.
	   */

	  now = time(NULL);

	  if (!firstblock)
	    firstblock = now;
	  else if (now != firstblock ||
		   *terminate)
	    {
	      /* We are out of data.  Flush the timestamps along... */
	      lwroc_filter_ts_commit(data_handle, dest, used, 1 /* flush */);
	      dest = NULL;
	    }

	  if (*terminate)
	    break; /* No more data, and asked to quit. */
	  /* Try again in 0.1 s. */
	  timeout.tv_sec = 0;
	  timeout.tv_usec = 100000;
	  lwroc_thread_block_get_token_timeout(thread_block, &timeout);
	  goto update_monitor;
	}

      if (dest == NULL ||
	  used + 3 * sizeof (uint32_t) > alloc ||
	  timestamp > first_timestamp + 1000000000)
	{
	  /* First emit the present buffer. */

	  if (dest)
	    lwroc_filter_ts_commit(data_handle, dest, used, 0 /* no flush */);

	  /* Need to get a new destination buffer. */

	  /* Get destination space. */
	  dest = lwroc_request_event_space(data_handle,
					   alloc);
	  used = header_size;
	  firstblock = 0;
	}

      /* Add this chunk of data. */

      p_dest = (uint32_t *) (dest + used);

      p_dest[0] = (uint32_t) size;
      p_dest[1] = 0;
      p_dest[2] = 0;

      used += 3 * (uint32_t) sizeof (uint32_t);

    consumed_event:
      /* We have used the event data, will not use it any more! */
      lwroc_pipe_buffer_did_read(pipe_buf, size);

    update_monitor:
      LWROC_FILTER_MON_CHECK(LWROC_FILTER_TS, 0 /* flush */);
    }

  /* If we break out of loop (finalise / on termination). */
  /* Send last buffer! */
}

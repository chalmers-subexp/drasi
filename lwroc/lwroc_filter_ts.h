/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2021  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_FILTER_TS_H__
#define __LWROC_FILTER_TS_H__

#include "lwroc_filter.h"

void lwroc_ts_filter_pre_setup_functions(struct lwroc_filter_functions *
					 filter_functions);

void lwroc_filter_ts_cmdline_usage(void);
int lwroc_filter_ts_parse_cmdline_arg(const char *request);

void  lwroc_filter_ts_max_ev_len(uint32_t *max_len,
				 uint32_t *add_len);

int lwroc_filter_ts_mode_set(void);

void lwroc_filter_ts_loop(lwroc_pipe_buffer_consumer *pipe_buf,
			  const lwroc_thread_block *thread_block,
			  lwroc_data_pipe_handle *data_handle,
			  volatile int *terminate);

#endif/*__LWROC_FILTER_TS_H__*/

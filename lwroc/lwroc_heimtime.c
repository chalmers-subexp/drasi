/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2024  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_heimtime.h"

/*****************************************************************************/

/* Strategy:
 *
 * First determine the periodic period.  I.e. the distance between
 * signals when they are not having any short signals in between.
 *
 * That can be found be requiring the distance to previous to be
 * within 1 % of the most recent such distance for e.g. 5 consecutive
 * pulses (2 would be enough).  Can do 10, to get a better measurement
 * of this reference period.  (That will anyhow only be used for tracking,
 * not for time calculations.
 *
 * Decode bits as located at (1 and 2) or (4 and 8) times 1/32 of the
 * determined period.
 *
 * Do dead counting using the known period, up to 100 periods.
 */

/*****************************************************************************/

/* This refers to the incoming times as they are being considered for
 * the decoder.  Since we want one pulse look-ahead, the most recent
 * incoming is the 'next'.  The previous incoming is therefore the
 * 'current' time considered.
 */
#define LWROC_HEIMTIME_T_HIST_NEXT   0
#define LWROC_HEIMTIME_T_HIST_CUR    1
#define LWROC_HEIMTIME_T_HIST_PREV   2
#define LWROC_HEIMTIME_T_HIST_PREV2  3

#define T64_HIST(name)  _t64_hist[LWROC_HEIMTIME_T_HIST_##name]

int lwroc_process_heimtime(lwroc_heimtime_state *state,
			   uint64_t t, uint64_t mask)
{
  uint64_t diff1, diff2, diff3;
  uint64_t t_full;
  int status = 0;

  if (rand() % 512 <= 2 && 0)
    {
      printf (".");
      /*printf ("\n\nOMITTING\n\n");*/
      return 0;
    }

  if (t < state->_t_prev)
    {
      /* We had a wrap. */

      state->_hi_add += mask + 1;
    }

  t_full = state->_hi_add + t;

  /* We shift the times into the state already here.
   * I.e. before use in the routine.
   */
  state->_t_prev  = t;

  state->T64_HIST(PREV2) = state->T64_HIST(PREV);
  state->T64_HIST(PREV ) = state->T64_HIST(CUR );
  state->T64_HIST(CUR  ) = state->T64_HIST(NEXT);
  state->T64_HIST(NEXT ) = t_full;

  /* We look at the previous signal.  This allows to check that
   * there is not a next signal (current) close-by, which may be
   * the actual one we are looking for.
   */

  diff1 = (state->T64_HIST(NEXT) - state->T64_HIST(CUR)  );
  diff2 = (state->T64_HIST(CUR)  - state->T64_HIST(PREV) );
  diff3 = (state->T64_HIST(PREV) - state->T64_HIST(PREV2));

  if (0)
    printf ("\n"
	    "t_next %" PRIu64 " "
	    "t_cur  %" PRIu64 " "
	    "t_prev %" PRIu64 "\n",
	    state->T64_HIST(NEXT),
	    state->T64_HIST(CUR),
	    state->T64_HIST(PREV));

  if (diff2 > diff3 - (diff3 / 128) &&
      diff2 < diff3 + (diff3 / 128) &&
      diff1 >          diff2 / 128 ) /* Next not too close. */
    {
      state->_consecutive_periodic++;

      if (state->_consecutive_periodic > 10)
	{
	  state->_period = diff2;

	  if (0)
	    printf ("period: %3d %10" PRIu64 "\n",
		    state->_consecutive_periodic,
		    state->_period);

	  /* In order for minor tracker to be able to find bits at
	   * startup...
	   */
	  if (!state->_lock_period)
	    state->_t1 = state->T64_HIST(CUR);

	  /* After this many bad pulses, we claim to have
	   * lost lock.  A time message would contain 96
	   * signals that do not match period.
	   */
	  state->_lock_period = 200;
	}
    }
  else
    {
      state->_consecutive_periodic = 0;

      if (state->_lock_period)
	state->_lock_period--;
    }

  /* We cannot continue without a determined period. */

  if (!state->_lock_period)
    return LWROC_HEIMTIME_NO_PERIOD;

  /* Is there a spurious pulse too close? */

  if (diff2 < state->_period / 128 || /* Previous too close. */
      diff1 < state->_period / 128)   /* Next too close. */
    {
      /* Spurious pulse too close. */
      return LWROC_HEIMTIME_PULSE_AMBIGUOUS;
    }

  /* Are we some multiple of a periodic distance since the
   * previous periodic distance?
   */

  {
    uint64_t minor_dt = state->_period / 32;

    uint64_t num_minor =
      ((state->T64_HIST(CUR) - state->_t1) +
       minor_dt / 2) / minor_dt;

    uint64_t expect_time_minor = state->_t1 + num_minor * minor_dt;

    if (0)
      printf ("minor? %" PRIu64 " "
	      "diff2 %" PRIu64 " "
	      "t_next %" PRIu64 " "
	      "t_cur %" PRIu64 " "
	      "t1 %" PRIu64 " "
	      "expect_t %" PRIu64 "\n",
	      num_minor,
	      diff2,
	      state->T64_HIST(NEXT),
	      state->T64_HIST(CUR),
	      state->_t1,
	      expect_time_minor);

    if (num_minor < 32)
      {
	if (state->T64_HIST(CUR) > expect_time_minor - state->_period / 128 &&
	    state->T64_HIST(CUR) < expect_time_minor + state->_period / 128)
	  {
	    if (0)
	      printf ("Minor mark!\n");

	    if (num_minor == 1 || num_minor == 4) /* First bit. */
	      {
		uint32_t bit = (num_minor == 4);

		state->_timestamp |= bit << state->_bitcount;
		state->_first_bit  = bit;

		state->_bitcount++;

		state->_had_bit |= 1;

		status |= LWROC_HEIMTIME_PULSE_MINOR_1;
	      }
	    else if (num_minor == 2 || num_minor == 8) /* Second bit. */
	      {
		uint32_t bit = (num_minor == 8);

		if (state->_first_bit == bit)
		  {
		    state->_had_bit |= 2;

		    status |= LWROC_HEIMTIME_PULSE_MINOR_2;
		  }
		else
		  {
		    LWROC_ERROR_FMT("Heimtime 2nd bit does not match 1st bit "
				    "(1st: %d, 2nd: %d) (bit %d).\n",
				    state->_first_bit, bit,
				    state->_bitcount);

		    status |= LWROC_HEIMTIME_PULSE_MINOR_2_MISM;
		  }
	      }
	    else
	      {
		/* Minor mark at unexpected location (!= 1, 2, 4, 8). */
		status |= LWROC_HEIMTIME_PULSE_MINOR_UNEXPECT;
	      }
	  }
	else
	  {
	    /* Minor mark at wrong location. */
	    status |= LWROC_HEIMTIME_PULSE_MINOR_WRONG;
	  }
      }
  }

  {
    uint64_t num_periods =
      ((state->T64_HIST(CUR) - state->_t1) +
       state->_period / 2) / state->_period;

    uint64_t expect_time_per = state->_t1 + num_periods * state->_period;

    if (0)
      printf ("period? %" PRIu64 " "
	      "diff2 %" PRIu64 " "
	      "t_next %" PRIu64 " "
	      "t_cur %" PRIu64 " expect_t %" PRIu64 "\n",
	      num_periods,
	      diff2,
	      state->T64_HIST(NEXT),
	      state->T64_HIST(CUR),
	      expect_time_per);

    if (num_periods > 100)
      {
	/* We do not dare to do dead tracking that far.
	 *
	 * Restart!
	 */
	state->_lock_period = 0;
	state->_good = 0;

	LWROC_ERROR_FMT("Heimtime lost > 100 periodic pulses "
			"(%" PRIu64 ").  "
			"Not daring to extrapolate - restart!",
			num_periods);

	status |= LWROC_HEIMTIME_PULSE_TOO_MANY_LOST;
      }
    else if (state->T64_HIST(CUR) > expect_time_per - state->_period / 128 &&
	     state->T64_HIST(CUR) < expect_time_per + state->_period / 128)
      {
	/* We have a periodic mark. */

	state->_ts0  = state->_ts1;
	state->_ts1 += ((num_periods << 26) / 128);

	state->_t0 = state->_t1;
	state->_t1 = state->T64_HIST(CUR);

	state->_periods = num_periods;

	status |= LWROC_HEIMTIME_PULSE_PERIODIC;

	if (state->_good)
	  status |= LWROC_HEIMTIME_NEW_PERIODIC;

	if (0)
	  printf ("Periodic mark: %" PRIu64 " : %" PRIu64 "\n",
		  state->_t1  - state->_t0,
		  state->_ts1 - state->_ts0);

	if (0)
	  printf ("had_bit: %d bitcount: %d\n",
		  state->_had_bit, state->_bitcount);

	/* Did we see both message bits, and distance to previous
	 * periodic marker was just one period?
	 */
	if (state->_had_bit == 0x3 &&
	    num_periods == 1)
	  {
	    if (state->_bitcount == 32)
	      {
		/* Timestamp message completed! */

		state->_extended_timestamp =
		  ((uint64_t) state->_timestamp) << 24;

		if (0)
		  printf ("Timestamp!  %" PRIu32 " (=> %" PRIu64 ") "
			  "@  %" PRIu64 "\n",
			  state->_timestamp,
			  state->_extended_timestamp,
			  state->T64_HIST(CUR));

		if (state->_ts1 != state->_extended_timestamp)
		  {
		    /* Mismatch vs. previous message. */
		    LWROC_ERROR_FMT("Decoded heimtime mismatch vs. tracked! "
				    "(%" PRIu64 " != expect %" PRIu64 ")",
				    state->_extended_timestamp,
				    state->_ts1);

		    state->_ts1  = state->_extended_timestamp;
		    state->_t1   = state->T64_HIST(CUR); /* Already done. */
		    state->_good = 0;
		    state->_matches = 0;

		    status |= LWROC_HEIMTIME_MESSAGE_MISMATCH;
		  }
		else
		  {
		    /* Timestamp message matched previous + 1. */
		    if (!state->_good)
		      LWROC_ERROR_FMT("Heimtime synced! "
				      "(%" PRIu64 ")",
				      state->_extended_timestamp);

		    state->_good = 1;
		    state->_matches++;

		    status |= LWROC_HEIMTIME_MESSAGE_COMPLETE;
		  }
	      }
	  }
	else
	  {
	    /* Restart search for timestamp message. */
	    state->_bitcount = 0;
	    state->_timestamp = 0;
	  }

	state->_had_bit = 0;
      }
    else
      {
	/* Periodic mark at wrong location, or minor pulse
	 * with lost periodic mark.
	 */
	status |=
	  LWROC_HEIMTIME_PULSE_PERIODIC_ERR;
      }
  }

  if (state->_good)
    status |= LWROC_HEIMTIME_LOCKED;
  else
    status |= LWROC_HEIMTIME_NOT_LOCKED;

  return status;
}

uint64_t lwroc_interp_heimtime(lwroc_heimtime_state *state,
			       uint64_t t, uint64_t mask)
{
  uint64_t hi_add = state->_hi_add;
  uint64_t t_full;
  double ts_offset;
  uint64_t ts;

  /* If the (short counter) time is smaller than the reference
   * value for the short counter, we had a wrap.  However, since
   * different channels may be slightly out of order, do not
   * consider a small backwards jump to be a wrap.
   *
   * A 32nd of the scale is considered a small wrap.
   */
  if (t < state->_t_prev)
    {
      if ((state->_t_prev - t) > (mask / 32))
	{
	  /* We had a wrap. */
	  hi_add += mask + 1;
	}
      else if ((state->_t_prev - t) > (mask / 16))
	{
	  /* We were kind of close to not be called a wrap,
	   * but are outside the limit.  This is too close
	   * for comfort - should not happen!
	   *
	   * Warn the user!
	   */

	  LWROC_ERROR_FMT("Limited range time close to wrap/non-wrap!  "
			  "Cycle assignment may be wrong!  "
			  "(%" PRIu64 " < %" PRIu64 ")",
			  (state->_t_prev - t),
			  (mask / 16));

	}
    }

  t_full = hi_add + t;

  /*
  printf ("t: %" PRIx32 ", t_prev: %" PRIx32 " "
	  "hi: %" PRIx64 " t_full: %" PRIx64 "\n",
	  t, state->_t_prev, hi, t_full);
  */

  /* Interpolate the reference time given the actual time. */

  ts_offset =
    (double) (int64_t) (t_full - state->_t1) *
    (double) (state->_ts1 - state->_ts0) /
    (double) (state->_t1 - state->_t0);

  ts = (uint64_t) ((int64_t) state->_ts1 + (int64_t) ts_offset);

  /*
  printf ("Interp: %" PRIx64 " + %" PRIx64 " (%.2f) "
	  "(%" PRIx64 " %" PRIx64 " %" PRIx64 ")\n",
	  state->_ts1, (uint64_t) ts_offset,
	  ts_offset,
	  (int64_t) (t_full - state->_t1),
	  (state->_ts1 - state->_ts0),
	  (state->_t1  - state->_t0));
  */

  /* If decoding a second timestamp has not matched previous, then
   * give up.
   */
  if (!state->_good)
    return 0;

  return ts;
}

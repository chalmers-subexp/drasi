/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2024  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_HEIMTIME_H__
#define __LWROC_HEIMTIME_H__

typedef struct lwroc_heimtime_state_t
{
  /* The previous timestamp from input. */
  uint64_t _t_prev;

  /* Wraps added. */
  uint64_t _hi_add;

  /* How many periodic pulses have been seen in a row.
   * Note: this will go to zero whenever the timestamp message is transmitted,
   * but would count to ~96 during the non-message part of the protocol.
   */
  int      _consecutive_periodic;
  /* The period determined. */
  uint64_t _period;
  /* Is the period currently determined. */
  int      _lock_period;

  /* Input times extended to 64 bits (including wraps applied), and
   * being considered for the decoder.
   */
  uint64_t _t64_hist[4];

  /* Timestamp information being accumulated. */
  uint32_t _timestamp;
  /* Bit seen as first bit. */
  uint32_t _first_bit;
  /* Number of bits seen. */
  int      _bitcount;
  /* Which bits have the message decoder seen since the previous
   * periodic pulse.
   */
  int      _had_bit;

  /* Decoded timestamp, and shifted up by 24 bits.  I.e. output timescale. */
  uint64_t _extended_timestamp;

  /* Current reference points for timestamp interpolation.
   *
   * 0 is first (i.e. previous), 1 is second (i.e. most recent).
   *
   * t is the input timescale.
   * ts is the output timescale.
   */
  uint64_t _t0,  _t1;
  uint64_t _ts0, _ts1;

  /* When using the deltas of the two samples above to calculate
   * period externally.
   */
  int      _periods;

  /* We have seen two timestamp messages where the second time matches
   * with the first time reported.
   */
  int      _good;

  /* Number of matching timestamp messages seen in total since good
   * set last time.
   */
  int      _matches;

} lwroc_heimtime_state;

/* Categorisation of the (previous) pulse. */

#define LWROC_HEIMTIME_PULSE_PERIODIC        0x0001
#define LWROC_HEIMTIME_PULSE_MINOR_1         0x0002
#define LWROC_HEIMTIME_PULSE_MINOR_2         0x0003
#define LWROC_HEIMTIME_PULSE_AMBIGUOUS       0x0004  /* Spurious close-by. */
#define LWROC_HEIMTIME_PULSE_MINOR_2_MISM    0x0005
#define LWROC_HEIMTIME_PULSE_MINOR_UNEXPECT  0x0006
#define LWROC_HEIMTIME_PULSE_MINOR_WRONG     0x0007
#define LWROC_HEIMTIME_PULSE_TOO_MANY_LOST   0x0008
#define LWROC_HEIMTIME_PULSE_PERIODIC_ERR    0x0009  /* Likely minor
						      * pulse after
						      * periodic lost.
						      */

/* Status. */

#define LWROC_HEIMTIME_NO_PERIOD             0x0100
#define LWROC_HEIMTIME_NOT_LOCKED            0x0200
#define LWROC_HEIMTIME_LOCKED                0x0400

/* Happenings. */

#define LWROC_HEIMTIME_MESSAGE_COMPLETE      0x1000
#define LWROC_HEIMTIME_MESSAGE_MISMATCH      0x2000
#define LWROC_HEIMTIME_NEW_PERIODIC          0x4000  /* Only when locked. */


int lwroc_process_heimtime(lwroc_heimtime_state *state,
			   uint64_t t, uint64_t mask);

uint64_t lwroc_interp_heimtime(lwroc_heimtime_state *state,
			       uint64_t t, uint64_t mask);

#endif/*__LWROC_HEIMTIME_H__*/

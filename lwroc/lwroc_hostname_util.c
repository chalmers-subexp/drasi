/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_hostname_util.h"
#include "lwroc_message.h"
#include "lwroc_parse_util.h"
#include "../dtc_arch/acc_def/has_getifaddrs.h"
#include "lwroc_macros.h"

#include <string.h>
#include <stdlib.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <net/if.h>

/********************************************************************/

int lwroc_get_host_port(const char *server,int default_port,
			struct sockaddr_storage *serv_addr)
{
  struct hostent *h;
  char *hostname;
  char dotted[INET6_ADDRSTRLEN];

  int port = default_port;

  /* If there is a colon in the hostname, interpret what follows as a
   * port number, overriding the default port.
   */
  {
    char *colon;

    hostname = strdup_chk(server);

    colon = strchr(hostname,':');

    if (colon)
      {
	*colon = 0; /* cut the hostname */

	if (*(colon+1) == '+')
	  port += lwroc_parse_hex_u16(colon+2, "port offset");
	else
	  port = lwroc_parse_hex_u16(colon+1, "port number");
      }

    /* get server IP address (no check if input is IP address or DNS name) */
    h = gethostbyname(hostname);
    free(hostname);
  }

  if (h == NULL)
    {
      LWROC_WARNING_FMT("Unknown host '%s'.",server);
      return 0;
    }

  serv_addr->ss_family = (sa_family_t) h->h_addrtype;

  switch (h->h_addrtype)
    {
    case AF_INET:
    {
      struct sockaddr_in *serv_addr_in = (struct sockaddr_in *) serv_addr;

      memcpy((char *) &serv_addr_in->sin_addr,
	     h->h_addr_list[0], (size_t) h->h_length);
      serv_addr_in->sin_port = htons((uint16_t) port);
      break;
    }
#ifdef AF_INET6
    case AF_INET6:
    {
      struct sockaddr_in6 *serv_addr_in6 = (struct sockaddr_in6 *) serv_addr;

      memcpy((char *) &serv_addr_in6->sin6_addr,
	     h->h_addr_list[0], (size_t) h->h_length);
      serv_addr_in6->sin6_port = htons((uint16_t) port);
      break;
    }
#endif
    default:
      LWROC_ERROR_FMT("Unknown address family for hostname '%s'.",
		      h->h_name);
      return 0;
    }

  lwroc_inet_ntop(serv_addr, dotted, sizeof (dotted));

  LWROC_INFO_FMT("Host '%s' known as %s (port: %d).",
		 h->h_name, dotted, port);

  return 1;
}

/********************************************************************/

void lwroc_copy_sockaddr_port(struct sockaddr_storage *addr_a,
			      struct sockaddr_storage *addr_b,
			      uint16_t port_b)
{
  *addr_a = *addr_b;

  switch (addr_a->ss_family)
    {
    case AF_INET:
    {
      struct sockaddr_in *addr_in_a = (struct sockaddr_in *) addr_a;
      addr_in_a->sin_port = htons(port_b);
      break;
    }
#ifdef AF_INET6
    case AF_INET6:
    {
      struct sockaddr_in6 *addr_in6_a = (struct sockaddr_in6 *) addr_a;
      addr_in6_a->sin6_port = htons(port_b);
      break;
    }
#endif
    default:
      break;
    }
}

int lwroc_compare_sockaddr(struct sockaddr_storage *addr_a,
			   struct sockaddr_storage *addr_b)
{
  if (addr_a->ss_family != addr_b->ss_family)
    return 0;

  switch (addr_a->ss_family)
    {
    case AF_INET:
    {
      struct sockaddr_in *addr_in_a = (struct sockaddr_in *) addr_a;
      struct sockaddr_in *addr_in_b = (struct sockaddr_in *) addr_b;

      if (addr_in_a->sin_port != addr_in_b->sin_port)
	return 0;

      if (addr_in_a->sin_addr.s_addr != addr_in_b->sin_addr.s_addr)
	return 0;

      break;
    }
#ifdef AF_INET6
    case AF_INET6:
    {
      struct sockaddr_in6 *addr_in6_a = (struct sockaddr_in6 *) addr_a;
      struct sockaddr_in6 *addr_in6_b = (struct sockaddr_in6 *) addr_b;

      if (addr_in6_a->sin6_port != addr_in6_b->sin6_port)
	return 0;

      if (!IN6_ARE_ADDR_EQUAL(&addr_in6_a->sin6_addr,
			      &addr_in6_b->sin6_addr))
	return 0;

      break;
    }
#endif
    default:
      return 0;
    }

  return 1;
}

/********************************************************************/

int lwroc_compare_sockaddr_pb(struct sockaddr_storage *addr_a,
			      struct sockaddr_storage *addr_b,
			      uint16_t port_b)
{
  /*
  printf ("cmp family %d %d\n",
	  addr_a->ss_family, addr_b->ss_family);
  */

  if (addr_a->ss_family != addr_b->ss_family)
    return 0;

  switch (addr_a->ss_family)
    {
    case AF_INET:
    {
      struct sockaddr_in *addr_in_a = (struct sockaddr_in *) addr_a;
      struct sockaddr_in *addr_in_b = (struct sockaddr_in *) addr_b;

      /*
      printf ("cmp port %d %d\n",
	      ntohs(addr_in_a->sin_port), port_b);
      */

      if (addr_in_a->sin_port != htons(port_b))
	return 0;

      /*
      printf ("cmp addr %x %x\n",
	      addr_in_a->sin_addr.s_addr, addr_in_b->sin_addr.s_addr);
      */

      if (addr_in_a->sin_addr.s_addr != addr_in_b->sin_addr.s_addr)
	return 0;

      break;
    }
#ifdef AF_INET6
    case AF_INET6:
    {
      struct sockaddr_in6 *addr_in6_a = (struct sockaddr_in6 *) addr_a;
      struct sockaddr_in6 *addr_in6_b = (struct sockaddr_in6 *) addr_b;

      if (addr_in6_a->sin6_port != htons(port_b))
	return 0;

      if (!IN6_ARE_ADDR_EQUAL(&addr_in6_a->sin6_addr,
			      &addr_in6_b->sin6_addr))
	return 0;

      break;
    }
#endif
    default:
      return 0;
    }

  return 1;
}

/********************************************************************/

void lwroc_get_hostname(struct sockaddr_storage *addr,
			char *dst, size_t size)
{
  struct hostent *h = NULL;

  switch (addr->ss_family)
    {
    case AF_INET:
    {
      struct sockaddr_in *addr_in = (struct sockaddr_in *) addr;
      /* Old BSD style prototype is with char pointer instead of void. */
      /* And some systems not even with a const... */
      h = gethostbyaddr(LWROC_UNCONST(char *,
				      (const char *) &addr_in->sin_addr),
			sizeof (addr_in->sin_addr),
			addr->ss_family);
      break;
    }
#ifdef AF_INET6
    case AF_INET6:
    {
      struct sockaddr_in6 *addr_in6 = (struct sockaddr_in6 *) addr;
      h = gethostbyaddr(LWROC_UNCONST(char *,
				      (const char *) &addr_in6->sin6_addr),
			sizeof (addr_in6->sin6_addr),
			addr->ss_family);
      break;
    }
#endif
    }

  if (h == NULL)
    {
      /* Try to format as a dotted number instead. */
      lwroc_inet_ntop(addr, dst, (socklen_t) size);
      return;
    }

  strncpy(dst, h->h_name, size);
  dst[size-1] = 0;
}

/********************************************************************/

void lwroc_inet_ntop(const struct sockaddr_storage *addr,
		     char *dst, socklen_t size)
{
  const char *ret = NULL;

  switch (addr->ss_family)
    {
    case AF_INET:
    {
      const struct sockaddr_in *addr_in = (const struct sockaddr_in *) addr;

#ifdef AF_INET6
      ret = inet_ntop(AF_INET, &addr_in->sin_addr, dst, size);
#else
      ret = inet_ntoa(addr_in->sin_addr);
      strncpy(dst, ret, size);
      if (size > 0)
	dst[size-1] = 0;
      ret = dst;
#endif
      break;
    }
#ifdef AF_INET6
    case AF_INET6:
    {
      const struct sockaddr_in6 *addr_in6 = (const struct sockaddr_in6 *) addr;

      ret = inet_ntop(AF_INET6, &addr_in6->sin6_addr, dst, size);
    }
#endif
    }

  if (ret == NULL)
    {
      strncpy(dst, "-", size);
      if (size > 0)
	dst[size-1] = 0;
    }
}

/********************************************************************/

uint32_t lwroc_get_ipv4_addr(struct sockaddr_storage *addr)
{
  switch (addr->ss_family)
    {
    case AF_INET:
    {
      struct sockaddr_in *addr_in = (struct sockaddr_in *) addr;

      return (uint32_t) ntohl(addr_in->sin_addr.s_addr);
    }
    }

  return 0;
}

/********************************************************************/

void lwroc_set_ipv4_addr(struct sockaddr_storage *addr,
			 uint32_t ipv4_addr)
{
  struct sockaddr_in *addr_in = (struct sockaddr_in *) addr;

  addr_in->sin_family = AF_INET;
  addr_in->sin_addr.s_addr = htonl(ipv4_addr);
}

/********************************************************************/

uint16_t lwroc_get_port(struct sockaddr_storage *addr)
{
  uint16_t port = 0;

  switch (addr->ss_family)
    {
    case AF_INET:
    {
      struct sockaddr_in *addr_in = (struct sockaddr_in *) addr;
      port = ntohs(addr_in->sin_port);
      break;
    }
#ifdef AF_INET6
    case AF_INET6:
    {
      struct sockaddr_in6 *addr_in6 = (struct sockaddr_in6 *) addr;
      port = ntohs(addr_in6->sin6_port);
      break;
    }
#endif
    default:
      break;
    }
  return port;
}

/********************************************************************/

void lwroc_set_port(struct sockaddr_storage *addr, uint16_t port)
{
  switch (addr->ss_family)
    {
    case AF_INET:
    {
      struct sockaddr_in *addr_in = (struct sockaddr_in *) addr;
      addr_in->sin_port = htons(port);
      break;
    }
#ifdef AF_INET6
    case AF_INET6:
    {
      struct sockaddr_in6 *addr_in6 = (struct sockaddr_in6 *) addr;
      addr_in6->sin6_port = htons(port);
      break;
    }
#endif
    default:
      break;
    }
}

/********************************************************************/

int lwroc_gethostname(char *hostname, size_t size)
{
  int ret;

  hostname[0] = 0;
  ret = gethostname(hostname, size);
  hostname[size - 1] = 0;

  return ret;
}

/********************************************************************/

void lwroc_list_ifaddrs(void)
{
#if HAS_GETIFADDRS
  struct ifaddrs *ifaddr_all;
  struct ifaddrs *ifaddr;

  char dotted[INET6_ADDRSTRLEN];
  char dotted_mask[INET6_ADDRSTRLEN];

  if (getifaddrs(&ifaddr_all) == -1)
    {
      LWROC_PERROR("getifaddrs");
      return;
    }

  for (ifaddr = ifaddr_all; ifaddr; ifaddr = ifaddr->ifa_next)
    {

      if ((ifaddr->ifa_addr->sa_family == AF_INET ||
	   ifaddr->ifa_addr->sa_family == AF_INET6) &&
	  !(ifaddr->ifa_flags & IFF_LOOPBACK))
	{
	  lwroc_inet_ntop((struct sockaddr_storage *) ifaddr->ifa_addr,
			  dotted, sizeof (dotted));
	  lwroc_inet_ntop((struct sockaddr_storage *) ifaddr->ifa_netmask,
			  dotted_mask, sizeof (dotted_mask));

	  LWROC_INFO_FMT("Own address: %s/%s (%s).",
			 dotted, dotted_mask,
			 ifaddr->ifa_name);
	}
    }

  freeifaddrs(ifaddr_all);
#else
  LWROC_INFO_FMT("Cannot list network addresses, no getifaddrs().");
#endif
}

/********************************************************************/

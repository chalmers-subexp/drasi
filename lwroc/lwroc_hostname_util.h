/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_HOSTNAME_UTIL_H__
#define __LWROC_HOSTNAME_UTIL_H__

#include "../dtc_arch/acc_def/netinet_in_include.h"

#include "../dtc_arch/acc_def/socket_include.h"
#include "../dtc_arch/acc_def/socket_types.h"
#include "../dtc_arch/acc_def/mystdint.h"

int lwroc_get_host_port(const char *server, int default_port,
			struct sockaddr_storage *serv_addr);

void lwroc_copy_sockaddr_port(struct sockaddr_storage *addr_a,
			      struct sockaddr_storage *addr_b,
			      uint16_t port_b);

int lwroc_compare_sockaddr(struct sockaddr_storage *addr_a,
			   struct sockaddr_storage *addr_b);

int lwroc_compare_sockaddr_pb(struct sockaddr_storage *addr_a,
			      struct sockaddr_storage *addr_b,
			      uint16_t port_b);

void lwroc_get_hostname(struct sockaddr_storage *addr,
			char *dst, size_t size);
void lwroc_inet_ntop(const struct sockaddr_storage *addr,
		     char *dst, socklen_t size);

uint32_t lwroc_get_ipv4_addr(struct sockaddr_storage *addr);

void lwroc_set_ipv4_addr(struct sockaddr_storage *addr,
			 uint32_t ipv4_addr);

uint16_t lwroc_get_port(struct sockaddr_storage *addr);

void lwroc_set_port(struct sockaddr_storage *addr, uint16_t port);

int lwroc_gethostname(char *dst, size_t size);

void lwroc_list_ifaddrs(void);

#endif/*__LWROC_HOSTNAME_UTIL_H__*/

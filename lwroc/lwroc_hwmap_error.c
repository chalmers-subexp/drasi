/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2023  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

/* Must be early, or stdio may miss vs(n)printf. */
#include <stdarg.h>

#include "lwroc_message_internal.h"
#include "hwmap_error.h"


void lwroc_hwmap_error_internal(int fatal,
				const char *file, int line,
				const char *fmt,...)
{
  va_list ap;

  va_start(ap, fmt);
  lwroc_do_message_internal(fatal ? LWROC_MSGLVL_FATAL : LWROC_MSGLVL_LOG,
			    0, NULL,
			    NULL, file, line, fmt, ap);
  va_end(ap);
}

hwmap_error_internal_func _hwmap_error_internal = lwroc_hwmap_error_internal;

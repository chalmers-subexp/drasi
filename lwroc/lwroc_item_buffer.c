/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_item_buffer.h"

#include <stdlib.h>

LWROC_STRUCT_ITEM_BUFFER(lwroc_item_buffer_plain,void);

void lwroc_item_buffer_alloc(void *buffer_ptr,
			     size_t elements, size_t elem_size)
{
  struct lwroc_item_buffer_plain *buffer =
    (struct lwroc_item_buffer_plain *) buffer_ptr;
  size_t max_elements = 1;
  void *items;

  while (max_elements < elements)
    max_elements <<= 1;

  items = malloc (max_elements * elem_size);

  buffer->items = items;

  buffer->avail = buffer->done = 0;
  buffer->mask = max_elements - 1;
  buffer->notify_consumer = NULL;
}

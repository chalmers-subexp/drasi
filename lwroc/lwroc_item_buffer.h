/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_ITEM_BUFFER_H__
#define __LWROC_ITEM_BUFFER_H__

#include "lwroc_thread_block.h"
#include "lwroc_optimise.h"

/* A simple buffer for holding a number of items, e.g. pointers to be
 * transported between two threads.  The optimisation/simplification
 * comes from the fact that the maximum number of items to store in
 * the buffer is known, and the buffer allocated to handle at least
 * that many items.  It is therefore always possible to insert an
 * item.  There is thus never a need to alert the producer that space
 * is available.
 */

#define LWROC_STRUCT_ITEM_BUFFER(struct_t,item_t)	\
  struct struct_t					\
  {							\
    volatile size_t avail;				\
    volatile size_t done;				\
    size_t mask;					\
    const lwroc_thread_block *volatile notify_consumer;	\
    item_t *items;					\
  }

void lwroc_item_buffer_alloc(void *buffer, size_t elements, size_t elem_size);

#define LWROC_ITEM_BUFFER_INIT_ALLOC(buffer, elements) do {	\
    lwroc_item_buffer_alloc(buffer, elements,			\
			    sizeof ((buffer)->items[0]));	\
  } while (0)

#define LWROC_ITEM_BUFFER_NONEMPTY(buffer)	\
  ((buffer).avail - (buffer).done != 0)

#define LWROC_ITEM_BUFFER_SET_NOTIFY(buffer,notify) do {	\
    (buffer).notify_consumer = (notify);			\
    MFENCE;							\
  } while (0)

#define LWROC_ITEM_BUFFER_NONEMPTY_OR_SET_NOTIFY(buffer,notify,nonempty) do { \
    if (LWROC_ITEM_BUFFER_NONEMPTY(buffer))				\
      nonempty = 1;							\
    else {								\
      LWROC_ITEM_BUFFER_SET_NOTIFY(buffer,notify);			\
      /* Item may have become available in the meantime. */		\
      if (LWROC_ITEM_BUFFER_NONEMPTY(buffer))				\
	nonempty = 1;							\
    }									\
  } while (0)


#define LWROC_ITEM_BUFFER_INSERT(buffer, item) do {			\
    size_t __insert_at = (buffer).avail & (buffer).mask;		\
    (buffer).items[__insert_at] = item;					\
    /*printf ("notify: %p %p\n",&(buffer),(buffer).notify_consumer);*/	\
    SFENCE;								\
    (buffer).avail++;							\
    /*printf ("notify: %p\n",(buffer).notify_consumer);*/		\
    LWROC_THREAD_BLOCK_TEST_AND_NOTIFY(&(buffer).notify_consumer);	\
  } while (0)

#define LWROC_ITEM_BUFFER_REMOVE(buffer, item_ptr) do {		\
    size_t __remove_at = (buffer).done & (buffer).mask;		\
    *(item_ptr) = (buffer).items[__remove_at];			\
    MFENCE;							\
    (buffer).done++;						\
  } while (0)

#endif/*__LWROC_ITEM_BUFFER_H__*/

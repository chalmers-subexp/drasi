/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2018  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_leap_seconds.h"
#include "lwroc_message.h"

#include <string.h>
#include <stdio.h>
#include <limits.h>

typedef struct lwroc_leap_info_t
{
  time_t prev_t,    next_t;
  int    prev_leap, next_leap;
} lwroc_leap_info;

/* Number of seconds from 1st of Jan 1900 to 1st of Jan 1970. */
#define LWROC_EPOCH_OFFSET_1900_TO_1970  2208988800ll

/* In case download of leap-seconds file has failed, we'll
 * simply use a fixed value of the last known leapsecond
 * applying at all times.
 *
 * 2016	Dec 31: 37
 */
lwroc_leap_info _lwroc_leap_info = { 0, 0, 0, 37 };

void lwroc_read_leap_seconds(const char *filename)
{
  FILE *fid = fopen(filename,"r");
  char line[256];

  if (fid == NULL)
    {
      LWROC_WARNING_FMT("Failed to open leap second file '%s'.  "
			"Using fixed value: %d.\n",
			filename, _lwroc_leap_info.next_leap);
      return;
    }

  memset(&_lwroc_leap_info, 0, sizeof (_lwroc_leap_info));

  while (!feof(fid))
    {
      char *comment;
      int n;
      long long t;
      int leap_seconds;

      if (!fgets(line, sizeof (line), fid))
	break;

      if (line[0] == '#')
	{
	  if (line[1] == '@')
	    {
	      time_t expire, now;

	      n = sscanf(line+2, "%lld", &t);

	      if (n == 1)
		{
		  expire = (time_t) (t - LWROC_EPOCH_OFFSET_1900_TO_1970);

		  now = time(NULL);

		  if (now > expire)
		    LWROC_WARNING_FMT("Leap second file has expired "
				      "(%d days).  "
				      "(Run 'make' to download a fresh file.)",
				      (int) ((now - expire) / (24 * 3600)));
		}
	      else
		LWROC_FATAL("Bad expire mark in leap second file.");
	    }
	  continue;
	}

      comment = strchr(line, '#');

      if (comment)
	*comment = 0;

      n = sscanf(line, "%lld %d", &t, &leap_seconds);

      if (n == 2)
	{
	  if (leap_seconds < -100 || leap_seconds > 100)
	    LWROC_FATAL_FMT("Crazy leap second value (%d) "
			    "in leap second file.",
			    leap_seconds);
	  if (t < _lwroc_leap_info.next_t)
	    LWROC_FATAL_FMT("Unordered leap second information (%lld < %lld) "
			    "in leap second file.",
			    t, (long long) _lwroc_leap_info.next_t);

	  _lwroc_leap_info.prev_t    = _lwroc_leap_info.next_t;
	  _lwroc_leap_info.prev_leap = _lwroc_leap_info.next_leap;

	  _lwroc_leap_info.next_t    =
	    (time_t) (t - LWROC_EPOCH_OFFSET_1900_TO_1970);
	  _lwroc_leap_info.next_leap = leap_seconds;
	}
    }
  /*
  printf ("%lld %d\n",
	  (long long int) _lwroc_leap_info.prev_t, _lwroc_leap_info.prev_leap);
  printf ("%lld %d\n",
	  (long long int) _lwroc_leap_info.next_t, _lwroc_leap_info.next_leap);
  */
  fclose(fid);
}

int lwroc_get_leap_seconds(time_t t)
{
  if (t <= _lwroc_leap_info.prev_t)
    return INT_MIN;
  if (t <= _lwroc_leap_info.next_t)
    return _lwroc_leap_info.prev_leap;

  /* No check against expire.  We just give the next value. */

  return _lwroc_leap_info.next_leap;
}

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_net_io.h"
#include "lwroc_message.h"
#include "lwroc_message_internal.h"
#include "lwroc_thread_block.h"
#include "lwroc_net_proto.h"
#include "lwroc_mon_block.h"
#include "lwroc_main_iface.h"
#include "lwroc_data_pipe.h"
#include "lwroc_net_legacy.h"
#include "lwroc_slow_async_loop.h"
#include "lwroc_file_writer.h"
#include "lwroc_filter_loop.h"
#include "lwroc_filter_ts.h"
#include "lwroc_net_trans.h"
#include "lwroc_triva_kind.h"
#include "lwroc_parse_util.h"
#include "lwroc_cpu_affinity.h"
#include "lwroc_timeouts.h"
#include "lwroc_hostname_util.h"

#include "lwroc_net_reverse_link.h"
#include "lwroc_net_trigbus.h"
#include "lwroc_net_control.h"
#include "lwroc_net_ntp.h"

#include "gen/lwroc_message_source_serializer.h"
#include "gen/lwroc_monitor_main_source_serializer.h"
#include "gen/lwroc_monitor_main_block_serializer.h"

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <assert.h>
#include "../dtc_arch/acc_def/myinttypes.h"
#include "../dtc_arch/acc_def/myprizd.h"

#include "git-describe-include.h"

lwrocexe_config _config;

lwroc_thread_instance *_lwroc_main_thread = NULL;

/* This is changed from 0 to 1 by sighandler,
 * then further by network thread.
 */
volatile int _lwroc_shutdown = 0;

void lwroc_notify_quit(void)
{
  /* Set shutdown mark, such that network thread do not accept further
   * incoming connections.  Once it has noticed that, it will tell the
   * first thread in the shutdown sequence (main) to go terminate.
   */
  if (!_lwroc_shutdown)
    {
      _lwroc_shutdown = 1;
      MFENCE;
      /* Wake the thread up if it exists. */
      if (_lwroc_net_io_thread)
	lwroc_thread_block_send_token(_lwroc_net_io_thread->_block);
    }
}

/* TODO: It is most certainly not signal-safe to call the message
 * routines from the signal handler.
 *
 * Is there a reasonable alternative?  Or can one at least say that
 * for the cases where it is done below, the program will soon
 * terminate anyhow, such that possibly corrupting stdout or the
 * message pipe is something that is tolerable?
 *
 * Not reporting at all the the user is worse.
 */

void lwroc_sighandler(int sig)
{
  switch(sig)
    {
    case SIGINT:
      if (_lwroc_quit_count < 2)
	{
	  _lwroc_quit_count++;
	  MFENCE;
	  LWROC_WARNING("SIGINT received.");
	  lwroc_notify_quit();
	  return;
	}
      LWROC_ERROR("3rd signal SIGINT received.");
      break;
    case SIGTERM:
      if (_lwroc_quit_count < 2)
	{
	  _lwroc_quit_count++;
	  MFENCE;
	  LWROC_WARNING("SIGTERM received.");
	  lwroc_notify_quit();
	  return;
	}
      LWROC_ERROR("3rd signal SIGTERM received.");
     break;
    case SIGBUS:
      /* TODO: is it really safe to call the message routines from the
       * signal handler?
       */
      LWROC_SIGNAL("SIGBUS received.");
      break;
    case SIGSEGV:
      /* TODO: is it really safe to call the message routines from the
       * signal handler?
       */
      LWROC_SIGNAL("SIGSEGV received.");
      break;
    case SIGPIPE:
      /* TODO: is it really safe to call the message routines from the
       * signal handler?
       */
      LWROC_ERROR("SIGPIPE received.  "
		  "If you did not pipe stdout somewhere and "
		  "just interrupted the program, "
		  "please contact the author.");
      break;
    }
  exit(0);
}

void lwroc_main_usage(char *cmdname)
{
  printf ("drasi/LWROC main executable (%s).\n",
	  GIT_DESCRIBE_STRING);
  printf ("\n");
  printf ("Usage: %s <options>\n", cmdname);
  printf ("\n");
  printf ("  --gdb                    Run me in GDB (must be the first "
	  "argument).\n");
  printf ("  --port=[+]PORT|none      Network TCP/IP port to bind to.\n");
  printf ("  --server=[drasi|trans[:PORT]][,[no]hold][,forcemap][,clients=N][,bufsize=N]\n");
  printf ("                           Network data server (output).\n");
  printf ("           [,dest=HOST]    Allowed drasi destinations.\n");
  printf ("  --log-no-start-wait      Do not wait for log client to connect.  (Do not use!)\n");
  printf ("  --log-ack-wait           Wait for log client ack of each message.\n");
  printf ("  --log-no-rate-limit      Do not rate-limit log messages.\n");
  printf ("  --file-writer[=bufsize=N]  File writer thread.\n");
  printf ("  --ltsm-opt=OPT           Options for LTSM file writer.\n");
  printf ("  --buf=[size=N][,valloc][,direct|physmmap|physpexor@off]\n");
  printf ("                           Size of data buffer (Mi|raw hex).\n");
  printf ("  --max-ev-size=N          Maximum event size (default 0x1000).\n");
  printf ("  --max-ev-interval=Ns     Max interval between events (for info).\n");
  printf ("  --[triva|trimi|trixor]=(master|slave),[@0xADDR][,ctime=N][,fctime=N]\n");
  printf ("           [,spill1213]    Spill on/off marked by trigger 12/13 for delayed EB.\n");
  printf ("                           Trigger module (addr bits 31-24).\n");
  printf ("           |[,sim=MOD[@HOST]]  Simulated trigger bus.\n");
  printf ("  --inspill-stuck-timeout=Ns  Mark inspill as stuck (and ignore) after N s.\n");
  printf ("  --master=HOST[:[+]PORT]  TRIVA/MI master instance.\n");
  printf ("  --slave=HOST[:[+]PORT]   TRIVA/MI slave instance.\n");
  printf ("  --eb-master=HOST[:[+]PORT]  Event builder master instance (=TRIVA/MI).\n");
  printf ("  --eb=HOST[:[+]PORT]      Event builder (actual) instance.\n");
  printf ("  --label=STRING           Label (e.g. detector name), used for monitoring.\n");
  printf ("  --ntp=HOST               NTP server (when local clock has sloppy discipline).\n");
  printf ("  --ts-track-log           Log each new timestamp track fit.\n");
  printf ("  --no-filter              Disable filter stage (if compiled in [%s]).\n",
	  lwroc_has_filter(LWROC_FILTER_USER) ? "yes" : "no");
  printf ("  --[action|info|log|debug|spam]  Minimum severity of messages to print on\n");
  printf ("                           console (default is info).\n");
  printf ("  --debug=MASK             Enable debug messages.\n");
  printf ("  --insomniac              Disable debugger sleep.\n");
  printf ("  --help                   Show this message.\n");
  printf ("\n");
  printf ("Note: most options offer additional usage info when passed the argument 'help'.\n");
  printf ("\n");

  lwroc_main_cmdline_usage();
  lwroc_main_filter_cmdline_usage();
}

/********************************************************************/

lwroc_thread_info _lwroc_main_thread_info =
{
  NULL,
  NULL,
  LWROC_MSG_BUFS_MAIN,
  "main",
  LWROC_THREAD_MAIN,
  LWROC_THREAD_TERM_MAIN,
  LWROC_THREAD_CORE_PRIO_READOUT,
};

/********************************************************************/

lwroc_pipe_buffer_control *_lwroc_readout_data_mon = NULL;/*TEMP!for mon only!*/
lwroc_pipe_buffer_consumer *_lwroc_readout_data_cons_dummy = NULL;
lwroc_pipe_buffer_consumer *_lwroc_filter_data_cons_dummy = NULL;
lwroc_pipe_buffer_consumer *_lwroc_main_data_cons_file = NULL;

lwroc_monitor_main_block  _lwroc_mon_main;
lwroc_mon_block          *_lwroc_mon_main_handle = NULL;

char *lwroc_mon_main_serialise(char *wire, void *block)
{
  lwroc_monitor_main_block *main_block =
    (lwroc_monitor_main_block *) block;

  return lwroc_monitor_main_block_serialize(wire, main_block);
}

int main(int argc,char *argv[])
{
  int i;
  int log_no_rate_limit = 0; /* Default does rate limit (after setup). */

  _lwroc_main_argv0 = argv[0];

  lwroc_msg_bufs_early_init(); /* Before any message printing. */

  MFENCE_CHECK(LWROC_BADCFG); /* BADCFG instead of FATAL to exit directly. */

  /* Setup things needed to parse arguments. */

  lwroc_main_pre_parse_setup();

  lwroc_main_filter_pre_parse_setup();

  lwroc_cpu_affinity_init();

  memset(&_config, 0, sizeof (_config));

  _config._port = LWROC_NET_DEFAULT_PORT;
  _config._max_ev_len = 0x1000;
  _config._inspill_stuck_timeout = LWROC_INSPILL_STUCK_TIMEOUT;
  _config._max_event_interval = 0; /* No promise from user. */
  _config._merge_ts_nodata_warn_timeout =
    LWROC_MERGE_SORT_NODATA_STUCK_TIMEOUT;
  _config._merge_keepalive_timeout = LWROC_MERGE_KEEPALIVE_TIMEOUT;

  if (lwroc_has_filter(LWROC_FILTER_USER))
    _config._use_filter[LWROC_FILTER_USER] = 1;

  /* No args. They probably need some guidance */
  if (argc == 1) {
    lwroc_main_usage(argv[0]);
    exit(1);
  }

  for (i = 1; i < argc; i++)
    {
      char *request = argv[i];
      char *post;

      if (LWROC_MATCH_C_ARG("--help")) {
	lwroc_main_usage(argv[0]);
	exit(0);
      } else if (LWROC_MATCH_C_PREFIX("--port=",post)) {
	if (strcmp(post,"none") == 0)
	  _config._port = -2;
	else if (*post == '+')
	  _config._port = (LWROC_NET_DEFAULT_PORT +
			   lwroc_parse_hex_u16(post, "port offset"));
	else
	  _config._port = lwroc_parse_hex_u16(post, "port number");
      } else if (LWROC_MATCH_C_PREFIX("--label=",post)) {
	_config._label = strdup_chk(post);
      } else if (LWROC_MATCH_C_PREFIX("--buf=",post)) {
	/*SUBUSAGE:lwroc_data_pipe_usage*/
	_config._bufspec = post;
      } else if (LWROC_MATCH_C_PREFIX("--max-ev-size=",post)) {
	_config._max_ev_len =
	  (uint32_t) lwroc_parse_hex(post, "maximum event size");
      } else if (LWROC_MATCH_C_PREFIX("--max-ev-interval=",post)) {
	_config._max_event_interval =
	  lwroc_parse_time(post,"max event interval");
      } else if (LWROC_MATCH_C_PREFIX("--server=",post)) {
	/*SUBUSAGE:lwroc_trans_server_usage*/
	lwroc_add_trans_server_cfg(post);
      } else if (LWROC_MATCH_C_ARG("--log-no-start-wait")) {
	_config._log_no_start_wait = 1;
      } else if (LWROC_MATCH_C_ARG("--log-ack-wait")) {
	_lwroc_log_ack_wait = 1;
      } else if (LWROC_MATCH_C_ARG("--log-no-rate-limit")) {
	/* Actual (global) control will be set after setup. */
	log_no_rate_limit = 1;
      } else if (LWROC_MATCH_C_ARG("--file-writer")) {
	/*SUBUSAGE:lwroc_file_writer_usage*/
	_config._use_file_writer = 1;
      } else if (LWROC_MATCH_C_PREFIX("--file-writer=",post)) {
	_config._file_writer_cfg = post;
	_config._use_file_writer = 1;
      } else if (LWROC_MATCH_C_PREFIX("--ltsm-opt=",post)) {
	/*SUBUSAGE:lwroc_tsm_writer_usage*/
	_config._ltsm_opt = post;
      } else if (LWROC_MATCH_C_PREFIX("--triva=",post)) {
	/*SUBUSAGE:lwroc_triva_usage*/
	_config._triva = post;
	_config._triva_kind = LWROC_TRIVA_KIND_TRIVA;
      } else if (LWROC_MATCH_C_PREFIX("--trimi=",post)) {
	_config._triva = post;
	_config._triva_kind = LWROC_TRIVA_KIND_TRIMI;
      } else if (LWROC_MATCH_C_PREFIX("--trixor=",post)) {
	_config._triva = post;
	_config._triva_kind = LWROC_TRIVA_KIND_TRIXOR;
      } else if (LWROC_MATCH_C_PREFIX("--inspill-stuck-timeout=",post)) {
	_config._inspill_stuck_timeout =
	  lwroc_parse_time(post,"inspill stuck timeout");
      } else if (LWROC_MATCH_C_PREFIX("--master=",post)) {
	lwroc_add_master(post, LWROC_REQUEST_TRIVAMI_BUS);
      } else if (LWROC_MATCH_C_PREFIX("--eb-master=",post)) {
	lwroc_add_master(post, LWROC_REQUEST_TRIVAMI_EB);
      } else if (LWROC_MATCH_C_PREFIX("--slave=",post)) {
	lwroc_add_slave(post,
			LWROC_REQUEST_TRIVAMI_BUS,
			lwroc_add_reverse_link(post,
					       LWROC_REQUEST_TRIVAMI_BUS, 0));
      } else if (LWROC_MATCH_C_PREFIX("--eb=",post)) {
	lwroc_add_slave(post,
			LWROC_REQUEST_TRIVAMI_EB,
			lwroc_add_reverse_link(post,
					       LWROC_REQUEST_TRIVAMI_EB, 0));
      } else if (LWROC_MATCH_C_PREFIX("--ntp=",post)) {
	lwroc_add_ntp_client(post);
      } else if (LWROC_MATCH_C_ARG("--ts-track-log")) {
	_config._log_track_timestamp = 1;
      } else if (LWROC_MATCH_C_ARG("--no-filter")) {
	if (!_config._use_filter[LWROC_FILTER_USER])
	  LWROC_BADCFG("Cannot disable filter, none compiled in.");
	_config._use_filter[LWROC_FILTER_USER] = 0;
      } else if (LWROC_MATCH_C_ARG("--action")) {
	_lwroc_log_console_level = LWROC_MSGLVL_ACTION;
      } else if (LWROC_MATCH_C_ARG("--info")) {
	_lwroc_log_console_level = LWROC_MSGLVL_INFO;
      } else if (LWROC_MATCH_C_ARG("--log")) {
	_lwroc_log_console_level = LWROC_MSGLVL_LOG;
      } else if (LWROC_MATCH_C_ARG("--debug")) {
	_lwroc_log_console_level = LWROC_MSGLVL_DEBUG;
	_lwroc_debug_mask_enabled |= 1; /* If we print debug, also enable. */
      } else if (LWROC_MATCH_C_ARG("--spam")) {
	_lwroc_log_console_level = LWROC_MSGLVL_SPAM;
	_lwroc_debug_mask_enabled |= 1; /* If we print >debug, also enable. */
      } else if (LWROC_MATCH_C_PREFIX("--debug=",post)) {
	_lwroc_debug_mask_enabled = lwroc_parse_hex_u32(post, "debug mask");
      } else if (LWROC_MATCH_C_ARG("--insomniac")) {
	_lwroc_message_no_sleep = 1;
      } else {
	if (!lwroc_main_parse_cmdline_arg(argv[i]) &&
	    !lwroc_main_filter_parse_cmdline_arg(argv[i]))
	  LWROC_BADCFG_FMT("Unrecognized option: '%s'", argv[i]);
      }
    }

  if (_config._ltsm_opt &&
      !_config._use_file_writer)
    LWROC_BADCFG("--ltsm-opt= makes no sense without --file-writer.");

  /* Sanitize timeout value. */
  if (_config._max_event_interval &&
      _config._max_event_interval < 1)
    LWROC_BADCFG_FMT("Max event interval (%d s) too small.",
		     _config._max_event_interval);


  if (lwroc_filter_ts_mode_set())
    _config._use_filter[LWROC_FILTER_TS] = 1;

  /* TODO: where available, use sigaction with SA_RESTART ? */

  signal(SIGINT, lwroc_sighandler);
  signal(SIGTERM,lwroc_sighandler);
  signal(SIGBUS, lwroc_sighandler);
  signal(SIGSEGV,lwroc_sighandler);
  signal(SIGPIPE,lwroc_sighandler);

  lwroc_msg_bufs_init(_config._port, argc, argv);
  lwroc_net_control_setup(); /* read tokens - rename ? - more generic */
  lwroc_mon_prepare_source(argc, argv);
  _lwroc_main_thread = lwroc_thread_prepare(&_lwroc_main_thread_info);
  /* Log messages are recorded after this point! */

  lwroc_list_ifaddrs();

  lwroc_net_wait_any_msg_client_init();
  if (_config._log_no_start_wait)
    {
      LWROC_WARNING("Running with --log-no-start-wait.  "
		    "Do *not* use for production or testing!!  "
		    "Only use for short demonstration purposes.\n");
      _lwroc_message_client_seen = 1;
    }

  /* TODO: should we try to do all argument parsing before we require
   * a message network connection?
   * Does not really matter, since a failure is a failure...
   */

  if (_config._use_file_writer)
    lwroc_file_writer_cfg(_config._file_writer_cfg);

  {
    int readout_consumers = 0;
    int readout_server_consumers = 0;
    int readout_dummy_consumer = 0;
    int readout_ncons = 0;

    int filter_consumers = 0;
    int filter_server_consumers = 0;
    int filter_dummy_consumer = 0;
    int filter_ncons = 0;

    int *final_consumers;

    int ts_filter_consumers = 0;
    int ts_filter_server_consumers = 0;
    int ts_filter_ncons = 0;

    lwroc_pipe_buffer_control *readout_data;
    lwroc_data_pipe_handle *readout_data_handle;
    lwroc_gdf_format_functions *readout_fmt;

    lwroc_pipe_buffer_control *filter_data = NULL;
    lwroc_data_pipe_handle *filter_data_handle = NULL;
    lwroc_gdf_format_functions *filter_fmt = NULL;

    lwroc_pipe_buffer_control *ts_filter_data = NULL;
    lwroc_data_pipe_handle *ts_filter_data_handle = NULL;
    lwroc_gdf_format_functions *ts_filter_fmt = NULL;

    lwroc_pipe_buffer_control *final_data;
    lwroc_data_pipe_handle *final_data_handle;
    int *final_ncons;

    lwroc_data_pipe_cfg main_data_cfg;
    lwroc_data_pipe_map_info main_data_map;

    lwroc_data_pipe_parse_cfg(&main_data_cfg, _config._bufspec);
    lwroc_main_parse_setup(&main_data_cfg);
    lwroc_data_pipe_post_parse(&main_data_cfg);

    final_consumers = &readout_consumers;
    if (_config._use_filter[LWROC_FILTER_USER])
      final_consumers = &filter_consumers;

    if (_config._use_file_writer)
      {
	/* File writing is always done from the last buffer. */
	(*final_consumers)++;
      }

    if (_config._use_filter[LWROC_FILTER_TS])
      {
	/* Timestamp filter takes data from final buffer. */
	(*final_consumers)++;
      }

    lwroc_net_trans_servers_clients(&readout_server_consumers,
				    &filter_server_consumers,
				    &ts_filter_server_consumers);

    readout_consumers += readout_server_consumers;
    filter_consumers += filter_server_consumers;
    ts_filter_consumers += ts_filter_server_consumers;

    if (_config._use_filter[LWROC_FILTER_USER])
      readout_consumers++;

    if (!readout_consumers)
      readout_dummy_consumer = readout_consumers = 1;
    if (!filter_consumers && _config._use_filter[LWROC_FILTER_USER])
      filter_dummy_consumer = filter_consumers = 1;

    /* Number of consumers for each stage figured out, now allocate... */

    lwroc_data_pipe_map(&main_data_map,
			&main_data_cfg);

    readout_fmt = lwroc_main_get_fmt();

    readout_data = lwroc_data_pipe_alloc(&main_data_map,
					 &main_data_cfg,
					 readout_consumers,
					 -1 /* readout */);

    readout_data_handle =
      lwroc_data_pipe_init("READOUT_PIPE",
			   readout_data,
			   _lwroc_main_thread->_block,
			   readout_fmt,
			   _config._max_ev_len,
			   &_lwroc_mon_main._events,
			   &_lwroc_mon_main._bytes);

    final_data        = readout_data;
    final_data_handle = readout_data_handle;
    final_ncons       = &readout_ncons;

    _lwroc_readout_data_mon = readout_data;

    /* The promise (if any) from the user regarding how often events
     * will be generated (at least, i.e. max interval).
     */
    lwroc_data_pipe_set_max_ev_interval(readout_data_handle,
					_config._max_event_interval);

    if (_config._use_filter[LWROC_FILTER_USER])
      {
	uint32_t max_ev_len =
	  lwroc_data_pipe_get_max_ev_len(readout_data_handle);

	lwroc_prepare_filter_thread(LWROC_FILTER_USER);

	filter_fmt = lwroc_main_filter_get_fmt(LWROC_FILTER_USER);
	if (!filter_fmt)
	  filter_fmt = readout_fmt;

	max_ev_len = lwroc_main_filter_get_max_ev_len(LWROC_FILTER_USER,
						      max_ev_len);

	filter_data = lwroc_data_pipe_alloc(&main_data_map,
					    &main_data_cfg,
					    filter_consumers,
					    LWROC_FILTER_USER);

	filter_data_handle =
	  lwroc_data_pipe_init("FILTER_PIPE",
			       filter_data,
			       _lwroc_filter_thread[LWROC_FILTER_USER]->
			       /**/_block,
			       filter_fmt,
			       max_ev_len,
			       &_lwroc_mon_filter[LWROC_FILTER_USER]._events,
			       &_lwroc_mon_filter[LWROC_FILTER_USER]._bytes);

	final_data        = filter_data;
	final_data_handle = filter_data_handle;
	final_ncons       = &filter_ncons;

	lwroc_data_pipe_set_max_ev_interval(filter_data_handle,
					    _config._max_event_interval);
      }

    if (ts_filter_consumers)
      {
	uint32_t max_ev_len =
	  lwroc_data_pipe_get_max_ev_len(final_data_handle);

	lwroc_prepare_filter_thread(LWROC_FILTER_TS);

	/* TS filter does not inherit.
	 * Should it?  Then it needs to switch processing depending on input.
	 */
	ts_filter_fmt = lwroc_main_filter_get_fmt(LWROC_FILTER_TS);

	max_ev_len = lwroc_main_filter_get_max_ev_len(LWROC_FILTER_TS,
						      max_ev_len);

	ts_filter_data = lwroc_data_pipe_alloc(&main_data_map,
					       &main_data_cfg,
					       ts_filter_consumers,
					       LWROC_FILTER_TS);

	ts_filter_data_handle =
	  lwroc_data_pipe_init("TS_FILTER_PIPE",
			       ts_filter_data,
			       _lwroc_filter_thread[LWROC_FILTER_TS]->_block,
			       ts_filter_fmt,
			       _config._max_ev_len,
			       &_lwroc_mon_filter[LWROC_FILTER_TS]._events,
			       &_lwroc_mon_filter[LWROC_FILTER_TS]._bytes);

	lwroc_data_pipe_set_max_ev_interval(ts_filter_data_handle,
					    _config._max_event_interval);
      }

    /* _lwroc_filter_data_mon = filter_data; */

    if (_config._use_file_writer)
      {
	_lwroc_main_data_cons_file =
	  lwroc_pipe_buffer_get_consumer(final_data, (*final_ncons)++);

	/* Should this do the job above, and get the ncons++ ? */
	lwroc_file_writer_init(final_data_handle);
      }

    lwroc_net_trans_servers_init(readout_data_handle, &readout_ncons,
				 filter_data_handle, &filter_ncons,
				 ts_filter_data_handle, &ts_filter_ncons);

    if (_config._use_filter[LWROC_FILTER_USER])
      lwroc_main_filter_get_consumer(LWROC_FILTER_USER,
				     readout_data_handle, &readout_ncons);

    if (_config._use_filter[LWROC_FILTER_TS])
      lwroc_main_filter_get_consumer(LWROC_FILTER_TS,
				     final_data_handle, final_ncons);

    if (readout_dummy_consumer)
      _lwroc_readout_data_cons_dummy =
	lwroc_pipe_buffer_get_consumer(readout_data, readout_ncons++);
    if (filter_dummy_consumer)
      _lwroc_filter_data_cons_dummy =
	lwroc_pipe_buffer_get_consumer(filter_data, filter_ncons++);

    assert(readout_ncons == readout_consumers);
    assert(filter_ncons == filter_consumers);
    assert(ts_filter_ncons == ts_filter_consumers);
  }

  /* Monitor information. */

  {
    lwroc_message_source main_src_source;
    lwroc_message_source_sersize sz_main_src_source;
    lwroc_monitor_main_source main_src;
    char *wire;

    /* main_src._dummy = 0; */
    if (_config._label)
      {
	strncpy(main_src._label, _config._label, sizeof (main_src._label));
	main_src._label[sizeof (main_src._label)-1] = 0;
      }
    else
      main_src._label[0] = 0;

    /* lwroc_mon_prepare_source() sets _lwroc_mon_ident, already called. */
    main_src._mon_ident1 = _lwroc_mon_ident._ident1;
    main_src._mon_ident2 = _lwroc_mon_ident._ident2;
    main_src._mon_ident3 = _lwroc_mon_ident._ident3;
    main_src._mon_ident4 = _lwroc_mon_ident._ident4;

    lwroc_mon_source_set(&main_src_source);

    memset (&_lwroc_mon_main, 0, sizeof (_lwroc_mon_main));
    _lwroc_mon_main_handle =
      lwroc_reg_mon_block(0, &_lwroc_mon_main, sizeof (_lwroc_mon_main),
			  lwroc_monitor_main_block_serialized_size(),
			  lwroc_mon_main_serialise,
			  lwroc_message_source_serialized_size(&main_src_source,
							       &sz_main_src_source) +
			  lwroc_monitor_main_source_serialized_size(),
			  lwroc_mon_main_fetch,
			  NULL);

    wire = _lwroc_mon_main_handle->_ser_source;

    wire = lwroc_message_source_serialize(wire, &main_src_source,
					  &sz_main_src_source);
    wire = lwroc_monitor_main_source_serialize(wire, &main_src);

    assert (wire == (_lwroc_mon_main_handle->_ser_source +
		     _lwroc_mon_main_handle->_ser_source_size));
  }

  /* Setup of additional items before creating any threads. */

  lwroc_main_setup();

  lwroc_main_filter_setup(LWROC_FILTER_USER, "FILTER_PIPE");

  if (_config._use_filter[LWROC_FILTER_TS])
    lwroc_main_filter_setup(LWROC_FILTER_TS, "TS_FILTER_PIPE");

  /* Running a file writer thread? */

  if (_config._use_file_writer)
    lwroc_prepare_file_writer_thread();

  /* Now we can create the server thread. */

  lwroc_prepare_net_trans_thread();

  /* Thread for slow (possibly lockup) async tasks. */

  lwroc_prepare_slow_async_thread();

  /* This thread must be created last, as it sets up sums of all
   * monitor blocks.  This will also bind the server sockets.
   */
  lwroc_prepare_net_io_thread(_config._port);

  /* Start rate limit of log messages after setup is done.
   * Do it before thread creation to make it deterministic.
   */
  _lwroc_log_no_rate_limit = log_no_rate_limit;
  LWROC_INFO_FMT("Log message rate limit %sin effect.",
		 _lwroc_log_no_rate_limit ? "not " : "");

  lwroc_all_threads_create();
  lwroc_thread_apply_cpu_set(_lwroc_main_thread);

  lwroc_main_loop();

  /* We are done, mark next thread in termination queue. */
  lwroc_thread_done(_lwroc_main_thread);

  lwroc_all_threads_wait();

  exit(0);

  /*
    pl_io_reg.nim_pulse(3);
  */

}




/* TODO: remove these descriptions.  Are in the documentation by now? */

/* Overall plan:
 *
 * One thread runs the readout (talking to modules).  It fills data
 * into a common buffer; waits until the necessary space is ready.
 *
 * The readout process knows nothing about file writing, or how to
 * combine data from multiple slave / subsystems into events.  This is
 * handled by another instance, usually running on some
 * separate plain PC node (server).
 *
 * Networking activity is handled by second thread, which accepts
 * incoming connections, and reads and writes to sockets.
 *
 * Outgoing data from the readout is ever only sent to one receiver.
 * This empties the buffer.  When no receiver is connected, it can be
 * configured to either just skip the data, or to wait for a receiver
 * to connect.
 *
 * Monitoring of status can be done via a network connection.  This
 * then gets some vital stats from the readout process, like events
 * read, total amount of data produced etc.  It is up to any program
 * at the other end to calculate rates etc.
 *
 * Logging of messages from the readout or other program parts is
 * handled in the same way as data.  Messages are written to a buffer
 * which can be read (once) by an external reader.  We set up one
 * buffer per program thread, simplifying locking between the threads.
 * With no receiver, the network thread will discard messages
 * (e.g. when more than half full), otherwise it may hold
 * indefinitely.  Note that this is a potential deadlock if the
 * network thread could generate an arbitrary number of messages
 * in between being emptied...
 *
 * Each log message has an associated time, such that a message logger
 * can print the messages in order if it wants to.  And a severity
 * level, to differentiate between BUG, FATAL, ERROR, WARNING, INFO,
 * LOG and DEBUG.  Also the hostname and a port number, identifying
 * the sending process.  (This could be sent when the connection is
 * set up, as it will not change.)
 *
 */


/* Continuous readout mode:
 *
 * The readout thread continuously reads modules, and injects (forces)
 * deadtime at some locations to verify sync between participating
 * modules.  Data for many physical events are collected into the same
 * readout event.  No trigger synchronisation module is required.
 */

/* Trigger synchronous mode:
 *
 * A special hardware module (TRIVA/TRIMI) is used to take triggers,
 * and keeps track of (global) deadtime.  The readout code is notified
 * about each trigger (via interrupt or polling), readout actions are
 * taken, and deadtime is released.
 *
 * In interrupt mode, the readout thread can run at elevated (realtime)
 * priorities.  This potentially dangerous (deadlock if infinite loop)
 * operation is monitored by a simple watchdog thread at even higher
 * prio.
 *
 * With the trigger module connecting several systems, one is master
 * and the rest slaves.  In order to have a simple startup scheme, the
 * master node keeps network connections with each slave node.
 * Generally, no information is exchanged between the systems, as this
 * is fully handled by the trigger modules.  However, before each
 * startup, the master communicate with each slave.  In case of TRIMI,
 * ensuring that each slave sees the correct trigger link.  In case
 * some configured systems do not exist, they could be ignored.  The
 * master can then also tell the combining (ucesb) process which
 * systems it should expect data from.  Slaves can also report that
 * they (believe) to have good sink connections for data, such that
 * they will not clog up.  The master is in TRIMI configurations
 * capable of ignoring deadtimes from (groups) of slaves, and may do
 * so if they are the cause of extended periods of deadtime.  Possibly
 * after consulting both with the combiner process and the direct
 * slave network connection.  If the network connection goes down, the
 * slave is immediately considered dead.
 *
 * If the connection to the data combiner (event builder) process is
 * lost, we go back to basics.  A slave losing the connection just
 * makes it disconnected and subject to the usual on-the-fly bind-in
 * sequence.  First of course, it is considered lost...
 *
 * If the master is lost, we go through the start-up sequence for
 * everyone.
 */


/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_MAIN_IFACE_H__
#define __LWROC_MAIN_IFACE_H__

#include "lwroc_pipe_buffer.h"
#include "lwroc_data_pipe.h"
#include "lwroc_filters.h"
#include "lwroc_merge_analyse_defs.h"

void lwroc_notify_quit(void);

void lwroc_main_pre_parse_setup(void);

void lwroc_main_cmdline_usage(void);
int lwroc_main_parse_cmdline_arg(const char *request);

void lwroc_main_parse_setup(lwroc_data_pipe_cfg *pipe_cfg);

lwroc_gdf_format_functions *lwroc_main_get_fmt(void);

void lwroc_main_setup(void);

void lwroc_main_bind(void);

void lwroc_main_loop(void);

void lwroc_mon_main_fetch(void *ptr);

typedef void (*lwroc_hardware_cleanup)(void);

extern volatile lwroc_hardware_cleanup _lwroc_hardware_cleanup;

/********************************************************************/

extern lwroc_pipe_buffer_consumer *_lwroc_main_data_cons_file;
extern lwroc_pipe_buffer_consumer *_lwroc_readout_data_cons_dummy;
extern lwroc_pipe_buffer_consumer *_lwroc_filter_data_cons_dummy;

/********************************************************************/

typedef struct lwrocexe_config_t
{
  int      _port;
  const char *_bufspec;
  uint32_t _max_ev_len;
  int      _use_file_writer;
  int      _use_filter[LWROC_FILTER_NUM];
  const char *_file_writer_cfg;
  const char *_ltsm_opt;
  int      _merge_no_validate;
  int      _merge_ts_nodata_warn_timeout;
  int      _merge_ts_disable_timeout;
  int      _merge_ts_wr_check;
  int      _merge_keepalive_timeout;
  uint32_t _merge_ts_analyse_ref_id;
  uint16_t _merge_ts_analyse_sync_trig;
  uint16_t _merge_ts_analyse_sync_trigs[LWROC_MERGE_ANA_NUM_SRC_ID];
  uint64_t _merge_ts_analyse_expect_id_mask;
  uint64_t _merge_ts_analyse_optional_id_mask;
  uint64_t _merge_ts_analyse_broken_id_mask;
  int      _merge_ts_analyse_has_expect_id_masks;
  int      _log_no_start_wait;
  int      _max_event_interval;
  const char *_triva;
  int      _triva_kind;
  int      _inspill_stuck_timeout;
  const char *_label;
  int      _log_track_timestamp;
} lwrocexe_config;

extern lwrocexe_config _config;

/********************************************************************/

extern volatile int _lwroc_shutdown;

extern volatile int _lwroc_threads_created;

/********************************************************************/

#endif/*__LWROC_MAIN_IFACE_H__*/

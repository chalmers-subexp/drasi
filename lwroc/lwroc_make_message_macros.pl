#!/usr/bin/perl

# This file is part of DRASI - a data acquisition data pump.
#
# Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301  USA

use strict;
use warnings;

########################################################################
# Subroutines

sub make_macro($$$$);
sub make_macros($$);

########################################################################

my @levels = ("SIGNAL", "BUG", "FATAL", "BADCFG",
	      "ERROR", "WARNING", "ACTION", "INFO",
	      "LOG", "DEBUG", "SPAM",
	      # dual-level selection
	      "INFO/WARNING");

print "#ifndef __LWROC_MESSAGE_MACROS_H__\n";
print "#define __LWROC_MESSAGE_MACROS_H__\n";
print "\n";
print "#ifdef VARIADIC_MACROS_OLD\n";
print "\n";

foreach my $level (@levels)
{
    make_macros($level,1);
}

print "\n";
print "#endif/* VARIADIC_MACROS_OLD */\n";
print "#ifdef VARIADIC_MACROS_c11\n";
print "\n";

foreach my $level (@levels)
{
    make_macros($level,0);
}

print "\n";
print "#endif/* VARIADIC_MACROS_c11 */\n";
print "\n";
print "#endif/*__LWROC_MESSAGE_MACROS_H__*/\n";


########################################################################

sub make_macro($$$$)
{
    my $level = shift;
    my $oldvar = shift;
    my $context = shift;
    my $fmt = shift;

    my $level1 = $level;
    my $level2 = "";

    my $levelselparam = "";
    my $levelsep = "";

    my $lwroclevel = "LWROC_MSGLVL_${level}";

    if ($oldvar == 1 && $fmt eq "_XFMT") {
	return;
    }

    if ($level =~ /(.*)\/(.*)/)
    {
	$level1 = $1;
	$level2 = $2;

	$levelsep = "_";
	$levelselparam = "lvl2,";

	$lwroclevel = "((lvl2) ? LWROC_MSGLVL_${level2} : LWROC_MSGLVL_${level1})";
    }

    my $ctxtparam = $context ? "c," : "";
    my $ctxtarg   = $context ? ($context eq "C" ?
				"&((c)->_msg_context)" :
				"(c)") : "NULL";
    my $fmtparam  = $fmt ? ($oldvar ? "x,__VA_ARGS__..." : "x,...") : "x";
    my $fmtarg    = $fmt ? "x,__VA_ARGS__" : "x";
    my $ifdebug =
	$level eq "DEBUG" ? "if (_lwroc_debug_mask_enabled) \\\n       " : "";

    if ($fmt eq "_XFMT") {
	$fmtparam = "...";
	$fmtarg   = "__VA_ARGS__";
    }

    # Help compiler know these macros will not return.
    my $noreturn =
	($level eq "SIGNAL" ||
	 $level eq "BUG" ||
	 $level eq "FATAL" ||
	 $level eq "BADCFG") ? "exit(0);" : "";

    print "#define ".
	"LWROC_${context}${level1}${levelsep}${level2}${fmt}".
	"(${ctxtparam}${levelselparam}${fmtparam}) \\\n";
    print "  do { ${ifdebug}lwroc_message_internal(${lwroclevel}, \\\n";
    print "                              ".
	"$ctxtarg, \\\n";
    print "                              ".
	"__FILE__,__LINE__,${fmtarg}); $noreturn } while (0)\n";
}

########################################################################

sub make_macros($$)
{
    my $level = shift;
    my $oldvar = shift;

    print "/* ${level} */\n";
    make_macro($level, $oldvar, "", "");
    make_macro($level, $oldvar, "", "_FMT");
    make_macro($level, $oldvar, "", "_XFMT");
    make_macro($level, $oldvar, "C", "");
    make_macro($level, $oldvar, "C", "_FMT");
    make_macro($level, $oldvar, "C", "_XFMT");
    make_macro($level, $oldvar, "CC", "");
    make_macro($level, $oldvar, "CC", "_FMT");
    make_macro($level, $oldvar, "CC", "_XFMT");
}

########################################################################

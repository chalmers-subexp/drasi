/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

/* From pex_user.h */

#define GET_BAR0_TRIX_BASE  0x1235

/* End pex_user.h */

#include "lwroc_map_pexor.h"
#include "lwroc_message.h"

int _lwroc_fd_pexor = -1;

int lwroc_get_pexor_fd(void)
{
  const char *devpath = "/dev/pexor";

  if (_lwroc_fd_pexor == -1)
    {
      LWROC_LOG_FMT("Open PEXOR driver (%s).", devpath);

      _lwroc_fd_pexor = open(devpath, O_RDWR);

      if (_lwroc_fd_pexor == -1)
	{
	  LWROC_PERROR("open");
	  LWROC_FATAL_FMT("Failed to open '%s'.", devpath);
	}
    }
  return _lwroc_fd_pexor;
}

volatile void *lwroc_map_pexor(size_t len)
{
  int fd_pexor;
  int prot = PROT_WRITE | PROT_READ;
  int flags = MAP_SHARED
#ifdef MAP_LOCKED
    | MAP_LOCKED
#endif
    ;
  volatile void *pexor_bar0 = NULL;

  fd_pexor = lwroc_get_pexor_fd();

  {
    /* This is not required?  Just for testing? */

    long trixor_base;

    int ret = ioctl (fd_pexor, GET_BAR0_TRIX_BASE, &trixor_base);

    if (ret == -1)
      {
	LWROC_PERROR("ioctl");
	LWROC_FATAL("ioctl(GET_BAR0_TRIX_BASE) failed\n");
      }

    LWROC_LOG_FMT("Trixor base: 0x%08x\n", (int) trixor_base);
  }

  pexor_bar0 =
    (volatile void *) mmap (NULL, len, prot, flags, fd_pexor, 0);

  if (pexor_bar0 == MAP_FAILED)
    {
      LWROC_PERROR("mmap");
      LWROC_FATAL("Failed to mmap bar0 for pexor.");
    }

  return pexor_bar0;
}

#define PEXOR_TRIXOR_BASE      0x40000

volatile void *lwroc_map_trixor(size_t len)
{
  void volatile *pexor_bar0;
  void volatile *trixor;

  pexor_bar0 = lwroc_map_pexor(PEXOR_TRIXOR_BASE + len);

  trixor = ((volatile char *) pexor_bar0 + PEXOR_TRIXOR_BASE);

  return trixor;
}

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_message_internal.h"
#include "lwroc_message.h"

#include "../mbscompat/f_ut_printm.h"
#include "../mbscompat/f_ut_error.h"
#include "../mbscompat/err_mask_def.h"

#undef printm

void printm(const char *fmt,...)
{
  va_list ap;

  va_start(ap, fmt);
  lwroc_do_message_internal(LWROC_MSGLVL_INFO, 0, NULL,
			    NULL, "printm", 0, fmt, ap);
  va_end(ap);
}

#include "lwroc_triva.h"

int f_user_trig_clear(unsigned char bh_trig_typ)
{
  /* Trigger 15 protection is in lwroc_triva_user_release_dt().  We
   * use the actual trigger number, and not what the user may have
   * cooked up.  We will not operate correctly if deadtime is released
   * after trigger 15, so playing tricks there is no good anyhow.
   */
  (void) bh_trig_typ;
  lwroc_triva_user_release_dt();
  return 0;
}

void f_ut_error_fileline(const char *file, int line,
			 const char *mod,
			 int32_t errnum,
			 char *retstr,
			 const char *str,
			 uint32_t mask)
{
  int level;

  /* We do not copy the output message (to avoid overflows).
   * But at least terminate the buffer.
   */
  if (retstr)
    *retstr = 0;

  if (errnum & 1)
    level = LWROC_MSGLVL_INFO;
  else
    level = LWROC_MSGLVL_ERROR;

  if ((mask & (MASK__PRTSLOG | MASK__PRTTERM)) == MASK__PRTSLOG &&
      level == LWROC_MSGLVL_INFO)
    {
      /* Info level, and only log file requested, degrade level. */
      level = LWROC_MSGLVL_LOG;
    }

  lwroc_message_internal(level, NULL, file, line,
			 "%s:%s ", str, mod);
}

void f_ut_error(const char *mod,
		int32_t errnum,
		char *retstr,
		const char *str,
		uint32_t mask)
{
  f_ut_error_fileline("f_ut_error", 0, mod, errnum, retstr, str, mask);
}

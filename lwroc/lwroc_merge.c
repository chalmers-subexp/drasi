/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_main_iface.h"
#include "lwroc_message.h"
#include "lwroc_message_internal.h"
#include "lwroc_thread_block.h"
#include "lwroc_net_proto.h"
#include "lwroc_mon_block.h"
#include "lwroc_merge_struct.h"
#include "lwroc_merge_in.h"
#include "lwroc_merge_sort.h"
#include "lwroc_parse_util.h"
#include "lwroc_merge_input_type.h"
#include "lmd/lwroc_lmd_util.h"
#include "ebye/lwroc_ebye_util.h"
#include "xfer/lwroc_xfer_util.h"

#include "gen/lwroc_message_source_serializer.h"
#include "gen/lwroc_monitor_in_source_serializer.h"
#include "gen/lwroc_monitor_in_block_serializer.h"

#include <string.h>
#include <assert.h>

PD_LL_SENTINEL(_lwroc_merge_source_opts);

void lwroc_add_merge_source_opt(const char *config, int type)
{
  lwroc_merge_source_opt *opt;

  opt = (lwroc_merge_source_opt *)
    malloc (sizeof (lwroc_merge_source_opt));

  if (!opt)
    LWROC_FATAL("Memory allocation error (merge source opt).");

  opt->_config = config;
  opt->_type = type;
  opt->_conn = NULL;

  PD_LL_ADD_BEFORE(&_lwroc_merge_source_opts, &opt->_source_opts);
}

lwroc_monitor_in_block    _lwroc_mon_in;
lwroc_mon_block          *_lwroc_mon_in_handle = NULL;

char *lwroc_mon_in_serialise(char *wire, void *block)
{
  lwroc_monitor_in_block *in_block =
    (lwroc_monitor_in_block *) block;

  return lwroc_monitor_in_block_serialize(wire, in_block);
}

void lwroc_main_pre_parse_setup(void)
{
  memset(&_lwroc_merge_info, 0, sizeof (_lwroc_merge_info));
}

void lwroc_merge_mode_opt(const char *mode)
{
  if (strcmp(mode,"wr") == 0)
    _lwroc_merge_info._mode = LWROC_MERGE_MODE_WHITE_RABBIT;
  else if (strcmp(mode,"titris") == 0)
    _lwroc_merge_info._mode = LWROC_MERGE_MODE_TITRIS;
  else if (strcmp(mode,"event") == 0)
    _lwroc_merge_info._mode = LWROC_MERGE_MODE_EVENT_BUILDER;
  else if (strcmp(mode,"ebye") == 0)
    _lwroc_merge_info._mode = LWROC_MERGE_MODE_EBYE_HITS;
  else if (strcmp(mode,"ebye-unsorted") == 0)
    _lwroc_merge_info._mode = LWROC_MERGE_MODE_EBYE_UNSORTED;
  else if (strcmp(mode,"xfer") == 0)
    _lwroc_merge_info._mode = LWROC_MERGE_MODE_XFER_BLOCK;
  else
    LWROC_BADCFG_FMT("Unknown merge mode '%s'.", mode);
}

/* Different names to let chkcmdlineoptsdoc.pl differentiate. */
#define lwroc_main_merge_cmdline_usage      lwroc_main_cmdline_usage
#define lwroc_main_merge_parse_cmdline_arg  lwroc_main_parse_cmdline_arg

void lwroc_main_merge_cmdline_usage(void)
{
  printf ("  --drasi=[inbufsize=N,]HOST[:[+]PORT]  Network data source (native protocol).\n");
  printf ("  --trans=[inbufsize=N,]HOST[:PORT]  Network data source (MBS transp. proto.).\n");
  printf ("  --ebye-push-sink=[inbufsize=N,][port=PORT]   Netw. data sink (EBYE xfer pr.).\n");
  printf ("  --ebye-pull=[inbufsize=N,]HOST[:PORT]   Network data source (EBYE proto.).\n");
  printf ("  --fnet=[inbufsize=N,]HOST[:PORT]   Network data source (Fakernet LMD).\n");
  printf ("  --file=[inbufsize=N,]FILENAME  File input source (.lmd/.ebye).\n");
  printf ("  --merge-mode=(wr|titris|event|ebye[-unsorted]|xfer)  Merge mode.\n");
  printf ("  --merge-no-validate      Do not validate sources.  (Disables reconnects.)\n");
  printf ("  --merge-keepalive=Ns     Re-connect source if no data seen for N s.\n");
  printf ("  --merge-ts-nodata-warn=Ns  Warn when timesort sources missing data after N s.\n");
  printf ("  --merge-ts-disable=Ns    Disable timesort sources after N s without data.\n");
  printf ("  --merge-ts-wr-check      Check WR timestamps against current time.\n");
  printf ("  --ts-ana-ref=N           Timestamp ID of alignment analysis reference.\n");
  printf ("  --ts-ana-sync-trig=N     Trigger number for sync triggers.\n");
  printf ("  --ts-ana-sync-trig-id=ID:N  Sync trigger number for particular ID.\n");
  printf ("  --ts-ana-expect-id=ID[,ID...]    Timestamp ID(s) expected every event.\n");
  printf ("  --ts-ana-optional-id=ID[,ID...]  Timestamp ID(s) that may miss events.\n");
  printf ("  --ts-ana-broken-id=ID[,ID...]    Timestamp ID(s) that can fail sync check.\n");
  printf ("\n");
}

int lwroc_main_merge_parse_cmdline_arg(const char *request)
{
  const char *post;

  /*NOHELPCALL*/ /* We are a option subset, --help handled elsewhere. */

  if (LWROC_MATCH_C_PREFIX("--drasi=",post)) {
    /*SUBUSAGE:lwroc_merge_in_src_usage*/
    lwroc_add_merge_source_opt(post, LWROC_MERGE_IN_TYPE_DRASI);
  } else if (LWROC_MATCH_C_PREFIX("--trans=",post)) {
    lwroc_add_merge_source_opt(post, LWROC_MERGE_IN_TYPE_MBS_TRANS);
  } else if (LWROC_MATCH_C_ARG/*NODOC*/("--ebye-push") ||
	     LWROC_MATCH_C_ARG/*NODOC*/ ("--ebye-push-sink") ||
	     LWROC_MATCH_C_ARG/*NODOC*/ ("--xfer-push-sink")/*remove*/) {
    /* --ebye-push is unified/old name for --ebye-push-sink */
    /* Sending side for --ebye-sink is --ebye-relay. */
    lwroc_add_merge_source_opt(NULL, LWROC_MERGE_IN_TYPE_EBYE_PUSH);
  } else if (LWROC_MATCH_C_PREFIX/*NODOC*/("--ebye-push=",post) ||
	     LWROC_MATCH_C_PREFIX("--ebye-push-sink=",post) ||
	     LWROC_MATCH_C_PREFIX/*NODOC*/("--xfer-push-sink=",post)/*rm.*/) {
    lwroc_add_merge_source_opt(post, LWROC_MERGE_IN_TYPE_EBYE_PUSH);
  } else if (LWROC_MATCH_C_PREFIX("--ebye-pull=",post) ||
	     LWROC_MATCH_C_PREFIX/*NODOC*/("--xfer-pull=",post)/*remove*/) {
    lwroc_add_merge_source_opt(post, LWROC_MERGE_IN_TYPE_EBYE_PULL);
  } else if (LWROC_MATCH_C_PREFIX("--fnet=",post)) {
    lwroc_add_merge_source_opt(post, LWROC_MERGE_IN_TYPE_FAKERNET);
  } else if (LWROC_MATCH_C_PREFIX("--file=",post)) {
    lwroc_add_merge_source_opt(post, LWROC_MERGE_IN_TYPE_FILE);
  } else if (LWROC_MATCH_C_PREFIX("--merge-mode=",post)) {
    lwroc_merge_mode_opt(post);
  } else if (LWROC_MATCH_C_ARG("--merge-no-validate")) {
    _config._merge_no_validate = 1;
  } else if (LWROC_MATCH_C_PREFIX/*NODOC*/("--merge-eb-max-event-interval=",post)) {
    LWROC_WARNING("--merge-eb-max-event-interval= option is deprecated, "
		  "please use --max-ev-interval");
    _config._max_event_interval =
      lwroc_parse_time(post,"max event interval");
  } else if (LWROC_MATCH_C_PREFIX("--merge-ts-nodata-warn=",post)) {
    _config._merge_ts_nodata_warn_timeout =
      lwroc_parse_time(post,"merge TS nodata warn");
  } else if (LWROC_MATCH_C_PREFIX("--merge-ts-disable=",post)) {
    _config._merge_ts_disable_timeout =
      lwroc_parse_time(post,"merge TS disable");
  } else if (LWROC_MATCH_C_ARG("--merge-ts-wr-check")) {
    _config._merge_ts_wr_check = 1;
  } else if (LWROC_MATCH_C_PREFIX("--merge-keepalive=",post)) {
    _config._merge_keepalive_timeout =
      lwroc_parse_time(post,"merge keepalive timeout");
  } else if (LWROC_MATCH_C_PREFIX("--ts-ana-ref=",post) ||
	     LWROC_MATCH_C_PREFIX/*NODOC*/("--merge-ts-analyse-ref=",post)) {
    _config._merge_ts_analyse_ref_id =
      (uint32_t) lwroc_parse_hex_limit(post, "analyse ref ID",
				       1, LWROC_MERGE_ANA_NUM_SRC_ID - 1);
  } else if (LWROC_MATCH_C_PREFIX("--ts-ana-sync-trig=",post) ||
	     LWROC_MATCH_C_PREFIX/*NODOC*/("--merge-ts-analyse-sync-trig=",post)) {
    _config._merge_ts_analyse_sync_trig =
      (uint16_t) lwroc_parse_hex_limit(post, "sync trig", 1, 15);
  } else if (LWROC_MATCH_C_PREFIX("--ts-ana-sync-trig-id=",post) ||
	     LWROC_MATCH_C_PREFIX/*NODOC*/("--merge-ts-analyse-sync-trig-id=",post)) {
    uint32_t src_id;
    uint16_t sync_trig;
    char *postcopy = strdup_chk(post);
    char *colon = strchr(postcopy, ':');

    if (!colon)
      LWROC_BADCFG_FMT("Bad sync trigger ID specification (missing :) '%s'.",
		       post);

    *colon = '\0'; /* To allow parse check. */

    src_id =
      (uint32_t) lwroc_parse_hex_limit(post, "analyse ref ID",
				       1, LWROC_MERGE_ANA_NUM_SRC_ID - 1);
    sync_trig =
      (uint16_t) lwroc_parse_hex_limit(colon+1, "sync trig", 1, 15);

    free (postcopy);

    if (src_id < LWROC_MERGE_ANA_NUM_SRC_ID)
      _config._merge_ts_analyse_sync_trigs[src_id] = sync_trig;
    else
      LWROC_BADCFG_FMT("Bad sync trigger ID (ID outside range) '%s'.",
		       post);
  } else if (LWROC_MATCH_C_PREFIX("--ts-ana-expect-id=",post)) {
    lwroc_parse_hex_list_mask(&_config._merge_ts_analyse_expect_id_mask,
			      post, "expected timestamp IDs",
			      LWROC_MERGE_ANA_NUM_SRC_ID-1);
  } else if (LWROC_MATCH_C_PREFIX("--ts-ana-optional-id=",post)) {
    lwroc_parse_hex_list_mask(&_config._merge_ts_analyse_optional_id_mask,
			      post, "optional timestamp IDs",
			      LWROC_MERGE_ANA_NUM_SRC_ID-1);
  } else if (LWROC_MATCH_C_PREFIX("--ts-ana-broken-id=",post)) {
    lwroc_parse_hex_list_mask(&_config._merge_ts_analyse_broken_id_mask,
			      post, "broken timestamp IDs",
			      LWROC_MERGE_ANA_NUM_SRC_ID-1);
  }
  else
    return 0;

  return 1;
}

void lwroc_main_parse_setup(lwroc_data_pipe_cfg *pipe_cfg)
{
  if (_config._triva)
    LWROC_BADCFG("In merge mode, but TRIVA/MI option given.");

  if (!_lwroc_merge_info._mode)
    LWROC_BADCFG("Merge mode not specified.");

  /* Sanitize timeout value. */
  if (_config._merge_ts_nodata_warn_timeout < 1)
    LWROC_BADCFG_FMT("Merge timesort nodata warning (%d s) too small.",
		     _config._merge_ts_nodata_warn_timeout);

  if (_config._merge_ts_disable_timeout < 0)
    LWROC_BADCFG_FMT("Merge timesort disable timeout (%d s) too small.",
		     _config._merge_ts_disable_timeout);

  if (_config._merge_keepalive_timeout < 0)
    LWROC_BADCFG_FMT("Merge keepalive timeout (%d s) too small.",
		     _config._merge_keepalive_timeout);

  if (_config._merge_ts_analyse_expect_id_mask ||
      _config._merge_ts_analyse_optional_id_mask ||
      _config._merge_ts_analyse_broken_id_mask)
    _config._merge_ts_analyse_has_expect_id_masks = 1;

  lwroc_parse_merge_sources(pipe_cfg);
}

void lwroc_merge_conn_monitor_fetch(void *ptr)
{
  lwroc_net_conn_monitor *conn_mon = (lwroc_net_conn_monitor *)
    (((char *) ptr) - offsetof(lwroc_net_conn_monitor, _block));

  /* Fills conn_mon->_block._buf_data_fill. */
  lwroc_net_conn_monitor_fetch(ptr);

  /* printf (" %olld\n", (long long) conn_mon->_block._buf_data_fill); */

  (void) conn_mon;

  /* printf ("Updated: ev %d\n", (int) conn_mon->_block._events); */
}

lwroc_gdf_format_functions *lwroc_merge_get_fmt(void)
{
  if (_lwroc_merge_info._mode & LWROC_MERGE_MODE_LMD)
    return &_lwroc_lmd_format_functions;
  else if (_lwroc_merge_info._mode & LWROC_MERGE_MODE_EBYE)
    return &_lwroc_ebye_format_functions;
  else if (_lwroc_merge_info._mode & LWROC_MERGE_MODE_XFER)
    return &_lwroc_xfer_format_functions;
  else
    {
      LWROC_BUG_FMT("Unknown data format for merge mode 0x%x.",
		    _lwroc_merge_info._mode);
      return NULL;
    }
}

void lwroc_main_setup(void)
{
  /* TODO: should be MERGE_PIPE here, or something such... */
  _lwroc_merge_info._data_handle = lwroc_get_data_pipe("READOUT_PIPE");

  if (!_lwroc_merge_info._data_handle)
    LWROC_FATAL("No handle for data pipe READOUT_PIPE.");

  _lwroc_merge_info._data_fmt =
    lwroc_data_pipe_get_fmt(_lwroc_merge_info._data_handle);;

  assert(_lwroc_merge_info._data_fmt);

  {
    lwroc_mon_block_fetch fetch_fcn =
      lwroc_merge_conn_monitor_fetch;
    /* For updating info from the merging, and pass along to the
     * monitor.  TODO: Should we really have this one?
     */
    lwroc_data_pipe_extra *pipe_extra;

    uint32_t kind = 0;
    uint32_t kind_sub = 0;

    lwroc_pipe_buffer_control *pipe_buf =
      lwroc_data_pipe_get_pipe_ctrl(_lwroc_merge_info._data_handle);

    pipe_extra =
      lwroc_data_pipe_get_pipe_extra(_lwroc_merge_info._data_handle);

    lwroc_data_pipe_extra_clear(pipe_extra);

    if (_lwroc_merge_info._mode & LWROC_MERGE_MODE_EB)
      kind = LWROC_CONN_KIND_EB;
    else
      {
	kind = LWROC_CONN_KIND_TS;

	if (_lwroc_merge_info._mode & LWROC_MERGE_MODE_WHITE_RABBIT)
	  kind_sub = LWROC_CONN_KIND_SUB_WR;
	if (_lwroc_merge_info._mode & LWROC_MERGE_MODE_TITRIS)
	  kind_sub = LWROC_CONN_KIND_SUB_TITRIS;
	if (_lwroc_merge_info._mode & LWROC_MERGE_MODE_EBYE_HITS)
	  kind_sub = LWROC_CONN_KIND_SUB_EBYE_HITS;
	if (_lwroc_merge_info._mode & LWROC_MERGE_MODE_EBYE_UNSORTED)
	  kind_sub = LWROC_CONN_KIND_SUB_EBYE_HITS; /* Not used anywhere. */
	if (_lwroc_merge_info._mode & LWROC_MERGE_MODE_XFER_BLOCK)
	  kind_sub = LWROC_CONN_KIND_SUB_XFER_BLOCK;
      }

    _lwroc_merge_info._mon =
      lwroc_net_conn_monitor_init(LWROC_CONN_TYPE_SYSTEM,
				  LWROC_CONN_DIR_OUTGOING,
				  NULL, NULL,
				  NULL, pipe_buf, NULL, NULL, pipe_extra, NULL,
				  fetch_fcn,
				  kind, kind_sub);
  }

  lwroc_add_merge_sources();

  lwroc_prepare_merge_in_thread(); /* also creates val(idation) thread */

  /* Monitor information. */

  {
    lwroc_message_source in_src_source;
    lwroc_message_source_sersize sz_in_src_source;
    lwroc_monitor_in_source in_src;
    char *wire;

    in_src._dummy = 0;

    lwroc_mon_source_set(&in_src_source);

    memset (&_lwroc_mon_in, 0, sizeof (_lwroc_mon_in));
    _lwroc_mon_in_handle =
      lwroc_reg_mon_block(0,&_lwroc_mon_in, sizeof (_lwroc_mon_in),
			  lwroc_monitor_in_block_serialized_size(),
			  lwroc_mon_in_serialise,
			  lwroc_message_source_serialized_size(&in_src_source,
							       &sz_in_src_source) +
			  lwroc_monitor_in_source_serialized_size(),
			  NULL,
			  _lwroc_merge_in_thread->_block);

    wire = _lwroc_mon_in_handle->_ser_source;

    wire = lwroc_message_source_serialize(wire, &in_src_source,
					  &sz_in_src_source);
    wire = lwroc_monitor_in_source_serialize(wire, &in_src);

    assert (wire == (_lwroc_mon_in_handle->_ser_source +
		     _lwroc_mon_in_handle->_ser_source_size));
  }

  lwroc_merge_setup();
}

void lwroc_main_bind(void)
{
  pd_ll_item *iter;

  PD_LL_FOREACH(_lwroc_merge_in_serv, iter)
    {
      lwroc_merge_in_unit *unit =
	PD_LL_ITEM(iter, lwroc_merge_in_unit, _servers);

      lwroc_merge_in_unit_bind(unit);
    }
}

void lwroc_main_loop(void)
{
  lwroc_merge_loop();
}

void lwroc_mon_main_fetch(void *ptr)
{
  lwroc_merge_monitor_fetch(ptr);
}

lwroc_gdf_format_functions *lwroc_main_get_fmt(void)
{
  return lwroc_merge_get_fmt();
}

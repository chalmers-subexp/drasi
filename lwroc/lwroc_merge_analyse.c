/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2019  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_merge_struct.h"
#include "lwroc_merge_analyse.h"

#include "lwroc_message_internal.h"
#include "lwroc_mon_block.h"
#include "lwroc_main_iface.h"

#include "lmd/lwroc_lmd_white_rabbit_stamp.h"

#include "lwroc_calc_mean_sigma.h"

#include "gen/lwroc_message_source_serializer.h"
#include "gen/lwroc_monitor_analyse_source_serializer.h"
#include "gen/lwroc_monitor_analyse_block_serializer.h"
#include "gen/lwroc_monitor_ana_ts_source_serializer.h"
#include "gen/lwroc_monitor_ana_ts_block_serializer.h"

#include "../dtc_arch/acc_def/myinttypes.h"
#include "../dtc_arch/acc_def/myprizd.h"
#include <string.h>
#include <assert.h>
#include <math.h>

/* Debug the sync check facility? */

#define LWROC_ANALYSE_SYNC_CHECK_DEBUG 0

lwroc_merge_analyse *_lwroc_merge_analyse = NULL;

/********************************************************************/

typedef struct lwroc_monitor_analyse_ana_ts_block_t
{
  lwroc_monitor_analyse_block _analyse;
  lwroc_monitor_ana_ts_block  _ana_ts[1 /* actually more */];
} lwroc_monitor_analyse_ana_ts_block;

lwroc_monitor_analyse_ana_ts_block *_lwroc_mon_analyse;
lwroc_mon_block                    *_lwroc_mon_analyse_handle = NULL;
uint32_t                            _lwroc_mon_ana_ts_num = 0;

char *lwroc_mon_analyse_serialise(char *wire, void *block)
{
  lwroc_monitor_analyse_ana_ts_block *analyse_block =
    (lwroc_monitor_analyse_ana_ts_block *) block;
  uint32_t i;

  wire = lwroc_monitor_analyse_block_serialize(wire, &analyse_block->_analyse);

  for (i = 0; i < _lwroc_mon_ana_ts_num; i++)
    wire =
      lwroc_monitor_ana_ts_block_serialize(wire, &analyse_block->_ana_ts[i]);

  return wire;
}

void lwroc_merge_analyse_thread_at_prepare(lwroc_thread_instance *inst)
{
  (void) inst;

  /* Monitor information. */

  /* TODO: for now, we have statically allocated one for each 16 possible.
   * Needs to change if we have many more...
   */
  _lwroc_mon_ana_ts_num = LWROC_MERGE_ANA_NUM_SRC_ID;

  {
    lwroc_message_source analyse_src_source;
    lwroc_message_source_sersize sz_analyse_src_source;
    lwroc_monitor_analyse_source analyse_src;
    char *wire;
    size_t sz_mon_analyse;
    uint32_t mon_idx = 0;

    analyse_src._ana_ts_blocks = _lwroc_mon_ana_ts_num;

    lwroc_mon_source_set(&analyse_src_source);

    /* The size ends where the analyse writer blocks end. */
    sz_mon_analyse = offsetof(lwroc_monitor_analyse_ana_ts_block,
			      _ana_ts[_lwroc_mon_ana_ts_num]);

    _lwroc_mon_analyse = malloc (sz_mon_analyse);

    if (!_lwroc_mon_analyse)
      LWROC_FATAL("Failure allocating memory for merge analyse monitor.");

    memset (_lwroc_mon_analyse, 0, sz_mon_analyse);
    for ( ; mon_idx < _lwroc_mon_ana_ts_num; )
      {
	lwroc_monitor_ana_ts_block *mon =
	  &_lwroc_mon_analyse->_ana_ts[mon_idx++];
	mon->_src_index_mask = 0;
	mon->_ts_ref_off_sig_k = (uint64_t) -2;
      }

    _lwroc_mon_analyse_handle =
      lwroc_reg_mon_block(0, &_lwroc_mon_analyse->_analyse, sz_mon_analyse,
			  lwroc_monitor_analyse_block_serialized_size() +
			  _lwroc_mon_ana_ts_num *
			  lwroc_monitor_ana_ts_block_serialized_size(),
			  lwroc_mon_analyse_serialise,
			  lwroc_message_source_serialized_size(&analyse_src_source,
							       &sz_analyse_src_source) +
			  lwroc_monitor_analyse_source_serialized_size(), NULL,
			  NULL);

    wire = _lwroc_mon_analyse_handle->_ser_source;

    wire = lwroc_message_source_serialize(wire, &analyse_src_source,
					  &sz_analyse_src_source);
    wire = lwroc_monitor_analyse_source_serialize(wire, &analyse_src);

    assert (wire == (_lwroc_mon_analyse_handle->_ser_source +
		     _lwroc_mon_analyse_handle->_ser_source_size));
  }
}

/********************************************************************/

typedef struct lwroc_merge_sync_issue_info_t
{
  uint16_t    _issue_idx;
  int         _level;
  const char *_name;
} lwroc_merge_sync_issue_info;

#define LWROC_MSII_ITEM(issue, level, str) \
  { LWROC_MERGE_ANA_SC_IDX_##issue, LWROC_MSGLVL_##level, str}

lwroc_merge_sync_issue_info _lwroc_merge_sync_issue_info[] =
  {
    LWROC_MSII_ITEM(HAD_EVENT,   INFO,    "ID seen:"        ),
    LWROC_MSII_ITEM(UNEXPECTED,  ERROR,   "ID unexpected:"  ),
    LWROC_MSII_ITEM(NO_CHECKVAL, ERROR,   "No SC value:"    ),
    LWROC_MSII_ITEM(NO_REF,      ERROR,   "No ref SC value:"),
    LWROC_MSII_ITEM(GOOD,        ERROR,   "No good ev.:"    ), /* Inverted! */
    LWROC_MSII_ITEM(AMBIGUOUS,   ERROR,   "Ambiguous:"      ),
    LWROC_MSII_ITEM(MISMATCH,    ERROR,   "Mismatch (bad):" ),
    LWROC_MSII_ITEM(MISSING,     ERROR,   "Missing ev.:"    ),
    LWROC_MSII_ITEM(MISSING_OK,  INFO,    "Missing (opt.):" ),
    LWROC_MSII_ITEM(SPURIOUS,    WARNING, "Spurious ev.:"   ),
    { (uint16_t) -1, 0, NULL }
  };

void lwroc_merge_report_sync_check_issues(uint16_t have_issues)
{
  lwroc_merge_sync_issue_info *issue_info;
  /* Four characters per item: */
  char id_string[LWROC_MERGE_ANA_NUM_SRC_ID*4+1];

  /* Print a table with all IDs that have been seen at all, and for
   * each kind of issue, print each ID that had that issue.
   */

  LWROC_WARNING("Sync check issues (list timestamp IDs 0x??00):");

  for (issue_info = _lwroc_merge_sync_issue_info;
       issue_info->_issue_idx != (uint16_t) -1; issue_info++)
    {
      uint16_t issue_bit = (uint16_t) (1 << issue_info->_issue_idx);

      if (have_issues & issue_bit)
	{
	  char *p = id_string;
	  uint32_t src_id;

	  for (src_id = 0; src_id < LWROC_MERGE_ANA_NUM_SRC_ID; src_id++)
	    {
	      lwroc_merge_ana_src *src =
		&_lwroc_merge_analyse->_srcs[src_id];
	      /*
	      printf ("%d 0x%04x 0x%04x\n", src_id,
		      src->_sc_issues_seen[LWROC_MERGE_ANA_SC_SEEN_CUR_PERIOD],
		      ((uint16_t) 1) << issue_info->_issue);
	      */
	      /* If the item has been seen, space will be used for it. */
	      if (src->_sc_issues_seen[LWROC_MERGE_ANA_SC_SEEN_CUR_PERIOD] &
		  LWROC_MERGE_ANA_SC_ISSUE_BIT(HAD_EVENT))
		{
		  if (src->_sc_issues_seen[LWROC_MERGE_ANA_SC_SEEN_CUR_PERIOD]&
		      issue_bit)
		    {
		      p += snprintf (p, sizeof (id_string), " %02x", src_id);
		    }
		  else
		    {
		      strcpy(p, "   ");
		      p += 3;
		    }
		}
	    }

	  lwroc_message_internal(issue_info->_level,NULL,__FILE__,__LINE__,
				 "%-16s%s",
				 issue_info->_name,
				 id_string);
	}
    }
}

/********************************************************************/

void lwroc_merge_do_analyse(void)
{
  uint32_t src_id;
  uint32_t mon_idx = 0;
  lwroc_monitor_ana_ts_block *mon_ref;
  struct timeval now;

  /*
  struct timeval a, b;
  gettimeofday(&a, NULL);
  */

  gettimeofday(&now, NULL);

  /* Make a mark for the reference ID */
  {
    mon_ref = &_lwroc_mon_analyse->_ana_ts[mon_idx++];

    mon_ref->_src_index_mask = 0;
    mon_ref->_id = (uint32_t) _lwroc_merge_analyse->_ref_src_id;
    mon_ref->_ts_ref_off_sig_k = (uint64_t) -1;
  }

  /* */

  for (src_id = 0; src_id < LWROC_MERGE_ANA_NUM_SRC_ID; src_id++)
    {
      lwroc_merge_ana_src *src =
	&_lwroc_merge_analyse->_srcs[src_id];
      lwroc_monitor_ana_ts_block *mon;
      uint32_t num_diffs3;
      uint32_t num_diffs, num_refs, num_lost;
      int j;
      uint64_t src_index_mask;
      int i_sync;

      mon = &_lwroc_mon_analyse->_ana_ts[mon_idx];
      mon->_src_index_mask = 0;
      mon->_id = (uint32_t) src_id;

      mon->_ts_ref_off_sig_k = (uint64_t) -3;
      mon->_ts_sync_ref_off_sig_k = (uint64_t) -3;
      mon->_sync_check_good = 0;

      for (i_sync = 0; i_sync < LWROC_MERGE_ANA_COLL_NUM; i_sync++)
	{
	  lwroc_merge_ana_src_collect *coll = &src->_coll[i_sync];

	  /*
	    uint32_t num_lost_debug[5];
	    int j;
	  */

	  num_diffs3 =
	    (coll->_history[0]._num_diffs -
	     coll->_history[3]._num_diffs);
	  num_diffs =
	    (coll->_history[0]._num_diffs -
	     coll->_history[LWROC_MERGE_ANA_NUM_HISTORY-1]._num_diffs);
	  /*
	    for (j = 0; j < 5; j++)
	      num_lost_debug[j]  =
		(coll->_history[0]._num_lost_debug[j] -
		 coll->_history[LWROC_MERGE_ANA_NUM_HISTORY-1].
		   _num_lost_debug[j]);
	  */

	  if (i_sync == LWROC_MERGE_ANA_COLL_NON_SYNC)
	    {
	      src_index_mask = 0;
	      for (j = 0; j < LWROC_MERGE_ANA_NUM_HISTORY; j++)
		src_index_mask |= coll->_history[j]._src_index_mask;

	      if (src_id == _lwroc_merge_analyse->_ref_src_id)
		mon_ref->_src_index_mask = src_index_mask;
	      else
		mon->_src_index_mask = src_index_mask;
	    }
	  /*
	  if (num_diffs)
	    printf ("[%d] %2d : %6d %6d %6d %4d ",
		    i_sync, src_id,
		    num_diffs3, num_diffs, num_refs, num_lost);
	  */
	  if (num_diffs3 > 0 && /* some data in last 3 cycles required. */
	      num_diffs >=
	      (uint32_t) (i_sync == LWROC_MERGE_ANA_COLL_SYNC_TRIG ? 16 : 50))
	    /* Reasonable limit above ?? */
	    {
	      /* int i; */
	      uint32_t use_diffs;
	      uint32_t start, i;

	      use_diffs =
		(i_sync < LWROC_MERGE_ANA_COLL_SYNC_CHECK) ?
		LWROC_MERGE_ANA_MAX_DIFFS : LWROC_MERGE_ANA_MAX_SYNC_CHECKS;
	      if (use_diffs > num_diffs)
		use_diffs = num_diffs;

	      /*
	      printf ("analyse! %4d : ",
		      use_diffs);
	      */
	      /*
		for (i = 0; i < LWROC_MERGE_ANA_MAX_DIFFS; i++)
		  printf (" %d", coll->_diffs[i]);
		printf ("\n");
	      */

	      start = coll->_history[0]._num_diffs - use_diffs;

	      if (i_sync < LWROC_MERGE_ANA_COLL_SYNC_CHECK)
		{
		  lwroc_mean_sigma mean_sigma;
		  int32_t *tmp_diffs = _lwroc_merge_analyse->_u._tmp_diffs;

		  for (i = 0; i < use_diffs; i++)
		    {
		      tmp_diffs[i] =
			coll->_u._diffs[(start + i) &
					(LWROC_MERGE_ANA_MAX_DIFFS-1)];
		    }

		  coll->_history[0]._num_refs =
		    _lwroc_merge_analyse->_num_ref_timestamps[i_sync];

		  num_refs =
		    (coll->_history[0]._num_refs -
		     coll->_history[LWROC_MERGE_ANA_NUM_HISTORY-1]._num_refs);
		  num_lost =
		    (coll->_history[0]._num_lost -
		     coll->_history[LWROC_MERGE_ANA_NUM_HISTORY-1]._num_lost);

		  if (lwroc_calc_mean_sigma(tmp_diffs,
					    use_diffs, 5,
					    &mean_sigma) == LWROC_CMS_OK)
		    {
		      double sigma = sqrt(mean_sigma.var);
		      uint32_t within_5sigma = 0;
		      uint32_t within_20sigma = 0;
		      double frac_within5;
		      double frac_within20;
		      double frac_loss;
		      double normalize;

		      /*
			printf ("%8.2f %8.2f ",
				mean_sigma.mean,
				mean_sigma.var);
		      */
		      /*
			printf ("%6d %6d %6d %6d | "
				"%7.0f %4.0f",
				use_diffs, num_diffs, num_refs, num_lost,
				mean_sigma.mean, sigma);
		      */
		      for (i = 0; i < use_diffs; i++)
			{
			  if (fabs(tmp_diffs[i] -
				   mean_sigma.mean) < 5 * sigma)
			    within_5sigma++;
			  else if (fabs(tmp_diffs[i] -
					mean_sigma.mean) < 20 * sigma)
			    within_20sigma++;
			}

		      frac_within5  = within_5sigma  / (double) use_diffs;
		      frac_within20 = within_20sigma / (double) use_diffs;
		      frac_loss =
			(1.0 - frac_within5 - frac_within20) +
			num_lost / (double) num_diffs;

		      normalize = num_diffs / (double) num_refs;
		      /*
			printf (" | %4d %4d | %5.1f%% %5.1f%% %5.1f%%",
				within_5sigma,
				within_20sigma,
				normalize * frac_within5 * 100.,
				normalize * frac_within20 * 100.,
				normalize * frac_loss * 100.);
		      */
		      if (i_sync == LWROC_MERGE_ANA_COLL_SYNC_TRIG)
			{
			  mon->_ts_sync_ref_offset_k =
			    (uint64_t) (mean_sigma.mean * 1000.0);
			  mon->_ts_sync_ref_off_sig_k =
			    (uint64_t) (sqrt(mean_sigma.var) * 1000.0);

			  mon->_sync_outside20sigma_k =
			    (uint32_t) ((1 - frac_within5 -
					 frac_within20)* 1000.);
			}
		      else
			{
			  mon->_ts_ref_offset_k =
			    (uint64_t) (mean_sigma.mean * 1000.0);
			  mon->_ts_ref_off_sig_k =
			    (uint64_t) (sqrt(mean_sigma.var) * 1000.0);

			  /*
			    printf ("\n %6d %6d %6d %6d %6d",
				    num_lost_debug[0],
				    num_lost_debug[1],
				    num_lost_debug[2],
				    num_lost_debug[3],
				    num_lost_debug[4]);
			  */
			  mon->_within5sigma_k =
			    (uint32_t) (normalize * frac_within5 * 1000.);
			  mon->_within20sigma_k =
			    (uint32_t) (normalize * frac_within20 * 1000.);
			  mon->_outside_k =
			    (uint32_t) (normalize * frac_loss * 1000.);
			}
		    }
		}
	      else /* LWROC_MERGE_ANA_COLL_SYNC_CHECK */
		{
		  lwroc_linear_fit_sol *sol = &src->_sync_check_sol;
		  lwroc_linear_fit_data *tmp =
		    _lwroc_merge_analyse->_u._sync_checks._tmp;
		  double *tmp_2array =
		    _lwroc_merge_analyse->_u._sync_checks._tmp2_array;
		  double *tmp_2array2 =
		    tmp_2array + LWROC_MERGE_ANA_MAX_SYNC_CHECKS;
		  size_t fit_num_used;
		  size_t num_vals, num_val_diffs;
		  double std_dev = 0;
		  double sync_val_dist = 0;

		  src->_sync_check_tol = 0.0;
		  src->_sync_check_separation = 0;

		  for (i = 0; i < use_diffs; i++)
		    {
		      lwroc_merge_ana_sync_check *s =
			&(coll->_u.
			  _sync_checks[(start + i) &
				       (LWROC_MERGE_ANA_MAX_SYNC_CHECKS-1)]);

		      tmp[i]._x = s->_ref;
		      tmp[i]._y = s->_check;
		      tmp[i]._flags = LWROC_TIMESTAMP_FIT_CONSIDER;
		    }

		  lwroc_linear_fit_filter_fit(sol,
					      tmp, use_diffs,
					      tmp_2array,
					      LWROC_TIMESTAMP_FIT_CONSIDER,
					      &fit_num_used);

#if LWROC_ANALYSE_SYNC_CHECK_DEBUG
		  printf ("analyse! %4d -> %" MYPRIzd " ",
			  use_diffs, fit_num_used);
#endif
		  /*
		  for (i = 0; i < use_diffs; i++)
		    printf (" (%.1f %1f)", tmp[i]._x, tmp[i]._y);
		  printf ("\n");
		  */

#if LWROC_ANALYSE_SYNC_CHECK_DEBUG
		  printf ("%.2f (%.5f)  %.5f (%.5f)\n",
			  sol->y0,   sol->var_y0,
			  sol->dydx, sol->var_dydx);
#endif

		  /* To find out further characteristics of the data,
		   * use the filtered data from the fit.  That way
		   * most 'bad' values should already be removed.
		   */

		  /* First find the typical width of one sync check
		   * value.  That we do by removing the linear fit
		   * contribution.
		   */

		  num_vals = 0;

		  for (i = 0; i < use_diffs; i++)
		    if (tmp[i]._flags & LWROC_TIMESTAMP_FIT_FIT2)
		      {
			double y_expect =
			  sol->y0 + sol->dydx * tmp[i]._x;

			tmp_2array[num_vals++] = tmp[i]._y - y_expect;
		      }

#if LWROC_ANALYSE_SYNC_CHECK_DEBUG
		  for (i = 0; i < num_vals; i++)
		    printf ("%.1f ", tmp_2array[i]);
		  printf ("\n");
#endif

		  /* The linear fit has already removed outliers,
		   * so we simply calculate directly.
		   */

		  {
		    double sum;
		    double sum_x;
		    double sum_x2;

		    double mean;
		    double var;

		    sum = 0;
		    sum_x = 0;
		    sum_x2 = 0;

		    sum = (double) (num_vals);

		    for (i = 0; i < num_vals; i++)
		      {
			sum_x  += tmp_2array[i];
			sum_x2 += tmp_2array[i] * tmp_2array[i];
		      }

		    mean = sum_x / sum;
		    var  = (sum_x2 - sum_x*sum_x / sum)/(sum-1);

		    std_dev = sqrt(var);

		    /* Allow values within 5 sigma, and add 1.1 for
		     * integer discretisation (slightly more than 1.0
		     * to allow when fit was perfect but we end up
		     * moving one unit).
		     *
		     * Add quadratically, to not give an excessive
		     * tolerance.
		     */
		    src->_sync_check_tol = sqrt(5. * 5. * var + 1.1);

		    (void) mean;
#if LWROC_ANALYSE_SYNC_CHECK_DEBUG
		    printf ("%8.2f %8.2f (tol %8.2f)\n",
			    mean, sqrt(var),
			    src->_sync_check_tol);
#endif
		  }

		  /* Plan to find the interval between (and number of)
		   * the sync check points.  We cannot check the
		   * reference, since that might have continuous
		   * values.  A client however is supposed to be
		   * distinct, so can use that.
		   *
		   * With all (fit-accepted) values sorted, calculate
		   * the differences.  Do the differences between
		   * next- neighbours to avoid some possible
		   * stragglers in-between.
		   *
		   * With say nominally 256 values in the history,
		   * and 128 kept by the fit, this still has margin for
		   * a 32-value sequence.
		   *
		   * Most of these values should be small, since they
		   * are the differences within the different check
		   * values, but some are large, being the differences
		   * between check values.
		   */

		  num_vals = 0;

		  for (i = 0; i < use_diffs; i++)
		    if (tmp[i]._flags & LWROC_TIMESTAMP_FIT_FIT2)
		      tmp_2array[num_vals++] = tmp[i]._y;

		  qsort(tmp_2array, num_vals, sizeof (double),
			lwroc_compare_double);

		  num_val_diffs = (num_vals > 2 ? num_vals-2 : 0);

		  for (i = 0; i < num_val_diffs; i++)
		    {
		      tmp_2array2[i] =
			tmp_2array[i+2] - tmp_2array[i];
		    }

		  qsort(tmp_2array2, num_val_diffs, sizeof (double),
			lwroc_compare_double);

#if LWROC_ANALYSE_SYNC_CHECK_DEBUG
		  for (i = 0; i < num_val_diffs; i++)
		    printf ("%.1f ", tmp_2array2[i]);
		  printf ("\n");
#endif

		  /* To find the typical (underestimated, due to near
		   * differences, i.e. high tail end to low tail
		   * beginning of next) sync value distance,
		   * find the peak of 'large' differences.
		   *
		   * We define that as where we have the most
		   * differences within a +/- 20 % interval, but larger
		   * than 2 * sigma + 1 estimated above.
		   */

		  {
		    size_t low_i, high_i;
		    size_t most = 0;
		    size_t best_low_i = 0, best_high_i = 0;

		    double sum_x;

		    for (low_i = 0; low_i < num_val_diffs; low_i++)
		      {
			if (tmp_2array2[low_i] > 2 * std_dev + 1.)
			  break; /* This is where we start. */
		      }

		    high_i = low_i;

		    for ( ; low_i < num_val_diffs; )
		      {
			/* Try to move the upper limit up, as long
			 * as it is not 40 % larger.
			 */

			for ( ; high_i < num_val_diffs; high_i++)
			  if (tmp_2array2[high_i] > tmp_2array2[low_i] * 1.4)
			    break;

#if LWROC_ANALYSE_SYNC_CHECK_DEBUG
			/* high_i point to the next after the allowed */
			printf ("%zd %zd: \n",
				low_i, high_i);
#endif

			if (high_i - low_i > most)
			  {
			    most = high_i - low_i;
			    best_low_i  = low_i;
			    best_high_i = high_i;
			  }

			/* Move low point at least one forward. */
			low_i++;

			/* And move low_i up while value is the same. */
			for ( ; low_i < num_val_diffs; low_i++)
			  if (tmp_2array2[low_i] > tmp_2array2[low_i - 1])
			    break;
		      }

		    if (most)
		      {
			sum_x = 0;

			for (i = (uint32_t) best_low_i; i < best_high_i; i++)
			  sum_x += tmp_2array2[i];

			sync_val_dist = sum_x / (double) most;

#if LWROC_ANALYSE_SYNC_CHECK_DEBUG
			printf ("%zd %zd: %.1f\n",
				best_low_i, best_high_i,
				sync_val_dist);
#endif

			src->_sync_check_separation = sync_val_dist;
		      }
		  }

		  /* Copy the sync check results. */

#define LWROC_GET_SCI_CNT(issue) \
		  (src->_sync_check_issues[LWROC_MERGE_ANA_SC_IDX_##issue])

		  mon->_sync_check_no_checkval = LWROC_GET_SCI_CNT(NO_CHECKVAL);
		  mon->_sync_check_no_ref      = LWROC_GET_SCI_CNT(NO_REF     );
		  mon->_sync_check_good        = LWROC_GET_SCI_CNT(GOOD       );
		  mon->_sync_check_ambiguous   = LWROC_GET_SCI_CNT(AMBIGUOUS  );
		  mon->_sync_check_mismatch    = LWROC_GET_SCI_CNT(MISMATCH   );
		  mon->_sync_check_missing     = LWROC_GET_SCI_CNT(MISSING    );
		  mon->_sync_check_spurious    = LWROC_GET_SCI_CNT(SPURIOUS   );

		  mon->_sync_check_sigmas_k =
		    (uint32_t) (1000. * sync_val_dist /
				(std_dev > 0.5 ? std_dev : 0.5));
		}

	      /*
		if (num_diffs)
		  printf ("\n");
	      */
	      /* If we have not collected at least a few samples, do
	       * not bump history every time.  But at least sometimes
	       * (force bump at least every 10 s).
	       */

	      if ((coll->_last_history_bump + 10 < now.tv_sec  ||
		   coll->_last_history_bump      > now.tv_sec) || /* Safety. */
		  (coll->_history[0]._num_diffs -
		   coll->_history[1]._num_diffs) >=
		  (uint32_t) (i_sync ==
			      LWROC_MERGE_ANA_COLL_SYNC_TRIG ? 4 : 10))
		{
		  coll->_last_history_bump = now.tv_sec;

		  memmove(coll->_history+1, coll->_history+0,
			  (LWROC_MERGE_ANA_NUM_HISTORY-1) *
			  sizeof (coll->_history[0]));
		  coll->_history[0]._src_index_mask = 0;
		}
	    }
	}

      if (mon->_src_index_mask ||
	  mon->_ts_ref_off_sig_k != (uint64_t) -3 ||
	  mon->_ts_sync_ref_off_sig_k != (uint64_t) -3 ||
	  mon->_sync_check_good)
	{
	  mon->_time._sec  = (uint64_t) now.tv_sec;
	  mon->_time._nsec = (uint32_t) now.tv_usec * 1000;

	  /* Something useful (non-0) was filled into the structure. */
	  mon_idx++;
	}
    }

  for ( ; mon_idx < _lwroc_mon_ana_ts_num; )
    {
      lwroc_monitor_ana_ts_block *mon =
	&_lwroc_mon_analyse->_ana_ts[mon_idx++];

      mon->_src_index_mask = 0;
      mon->_ts_ref_off_sig_k = (uint64_t) -2;
    }

  /* Make sure we do not use too old sync period data. */
  if (_lwroc_merge_analyse->_last_bump_sync_diffs + 10 < now.tv_sec ||
      _lwroc_merge_analyse->_last_bump_sync_diffs      > now.tv_sec)
    {
      _lwroc_merge_analyse->_last_bump_sync_diffs = now.tv_sec;
      /*
      printf ("Bump for sync trigger interval est. (%d, %d, %d).\n",
	      _lwroc_merge_analyse->_num_sync_diffs,
	      _lwroc_merge_analyse->_fill_sync_diffs,
	      _lwroc_merge_analyse->_has_sync_period);
      */
      /* Every 10 s, we reduce the history claimed by one sample.
       * if it reaches zero, we remove the estimate completely.
       */
      _lwroc_merge_analyse->_num_sync_diffs--;
      if (_lwroc_merge_analyse->_num_sync_diffs <= 0)
	{
	  _lwroc_merge_analyse->_num_sync_diffs = 0;
	  /*
	  printf ("Clear sync trigger interval lower estimate.\n");
	  */
	  _lwroc_merge_analyse->_has_sync_period = 0;
	}
    }

  /*
  gettimeofday(&b, NULL);
  printf ("elapsed: %d\n", (int) (b.tv_usec - a.tv_usec));
  */

  /* Loop over all ids and merge the seen issues. */
  for (src_id = 0; src_id < LWROC_MERGE_ANA_NUM_SRC_ID; src_id++)
    {
      lwroc_merge_ana_src *src =
	&_lwroc_merge_analyse->_srcs[src_id];

      src->_sc_issues_seen[LWROC_MERGE_ANA_SC_SEEN_CUR_PERIOD] |=
	src->_sc_issues_seen[LWROC_MERGE_ANA_SC_SEEN_CUR_ROUND];

      src->_sc_issues_seen[LWROC_MERGE_ANA_SC_SEEN_CUR_ROUND] = 0;
     }

  /* Only make the reporting every so often. */
  if (_lwroc_merge_analyse->_last_dump_sync_issues + 15 < now.tv_sec ||
      _lwroc_merge_analyse->_last_dump_sync_issues      > now.tv_sec)
    {
      uint16_t have_issues = 0;

      _lwroc_merge_analyse->_last_dump_sync_issues = now.tv_sec;

      /* First loop over all ids and find issues. */
      for (src_id = 0; src_id < LWROC_MERGE_ANA_NUM_SRC_ID; src_id++)
	{
	  lwroc_merge_ana_src *src =
	    &_lwroc_merge_analyse->_srcs[src_id];
	  uint64_t src_id_mask = ((uint64_t) 1) << src_id;
	  int is_id_expect =
	    !!(_config._merge_ts_analyse_expect_id_mask & src_id_mask);
	  int is_id_optional =
	    !!(_config._merge_ts_analyse_optional_id_mask & src_id_mask);
	  int is_id_broken =
	    !!(_config._merge_ts_analyse_broken_id_mask & src_id_mask);
	  int is_id_ref =
	    (src_id == _config._merge_ts_analyse_ref_id);

	  uint16_t *p_src_issue_cur =
	    &(src->_sc_issues_seen[LWROC_MERGE_ANA_SC_SEEN_CUR_PERIOD]);

#define SRC_HAS_ISSUE(issue)						\
	  (!!(*p_src_issue_cur & \
	      LWROC_MERGE_ANA_SC_ISSUE_BIT(issue)))
#define SRC_SET_ISSUE(issue) do { 					\
	    *p_src_issue_cur |=	\
	      LWROC_MERGE_ANA_SC_ISSUE_BIT(issue);			\
	  } while (0)
#define SRC_CLEAR_ISSUE(issue) do { 					\
	    *p_src_issue_cur &=	\
	      (uint16_t) ~LWROC_MERGE_ANA_SC_ISSUE_BIT(issue);		\
	  } while (0)
#define SRC_INVERT_ISSUE(issue) do { 					\
	    *p_src_issue_cur ^=	\
	      LWROC_MERGE_ANA_SC_ISSUE_BIT(issue);			\
	  } while (0)

	  /* Invert the good events seen bit.
	   * Good events are ... good.
	   * Having seen no good event is however bad.
	   */
	  SRC_INVERT_ISSUE(GOOD);

	  /* If we have seen an event... */
	  if (SRC_HAS_ISSUE(HAD_EVENT))
	    {
	      /* If we do not expect this ID (not in any of the
	       * expect/optional/broken list), then it is unexpected.
	       */
	      if (_config._merge_ts_analyse_has_expect_id_masks &&
		  !(is_id_expect | is_id_optional | is_id_broken))
		SRC_SET_ISSUE(UNEXPECTED);
	    }

	  if (!(is_id_expect | is_id_optional | is_id_broken) || is_id_ref)
	    {
	      /* Unknown (not known as expected, optional or broken) ID:
	       * for this the 'anti'-issues do not apply.
	       */
	      SRC_CLEAR_ISSUE(GOOD);
	      SRC_CLEAR_ISSUE(MISSING);
	    }

	  if (SRC_HAS_ISSUE(MISSING) &&
	      is_id_optional)
	    {
	      /* Optional ID: missing is missing-ok. */
	      SRC_CLEAR_ISSUE(MISSING);
	      SRC_SET_ISSUE(MISSING_OK);
	    }

	  if (!is_id_broken)
	    {
	      /* We have issues if any issues are detected. */
	      have_issues |= *p_src_issue_cur;
	    }
	}

      if (have_issues & ~(LWROC_MERGE_ANA_SC_ISSUE_BIT(HAD_EVENT) |
			  LWROC_MERGE_ANA_SC_ISSUE_BIT(MISSING_OK)))
	lwroc_merge_report_sync_check_issues(have_issues);

      /* Clear which issues have been seen. */
      /* TODO: ..._SEEN_PREV_PERIOD not really used - remove? */
      for (src_id = 0; src_id < LWROC_MERGE_ANA_NUM_SRC_ID; src_id++)
	{
	  lwroc_merge_ana_src *src =
	    &_lwroc_merge_analyse->_srcs[src_id];

	  src->_sc_issues_seen[LWROC_MERGE_ANA_SC_SEEN_PREV_PERIOD] =
	    src->_sc_issues_seen[LWROC_MERGE_ANA_SC_SEEN_CUR_PERIOD];

	  src->_sc_issues_seen[LWROC_MERGE_ANA_SC_SEEN_CUR_PERIOD] =
	    0;
	}
     }
}

/********************************************************************/

void lwroc_merge_analyse_add_diff(lwroc_merge_ana_src *src,
				  lwroc_merge_ana_src_collect *coll,
				  int i_sync,
				  int64_t diff,
				  uint32_t ref_sync_check,
				  uint32_t sync_check,
				  uint64_t ref_timestamp,
				  uint64_t sync_timestamp)
{
  int32_t diff32;
  uint32_t idx;

  int ok_tdiff = 1;

  /* We ignore differences that are outside 32 bits.
   *
   * With such large timing differences, there are not any sane
   * correlations anyhow.
   *
   * It also makes sense to not record them for the fitting of the
   * sync check values.  Note: this is just for fitting.  The sync
   * check still happens.
   */

  diff32 = (int32_t) diff;

  if (((int64_t) diff32) != diff)
    {
      coll->_history[0]._num_lost++;
      /* coll->_history[0]._num_lost_debug[0]++; */
      ok_tdiff = 0;
    }

  if (i_sync == LWROC_MERGE_ANA_COLL_SYNC_TRIG)
    {
      if (!_lwroc_merge_analyse->_has_sync_period ||
	  ((uint64_t) abs(diff32)) > _lwroc_merge_analyse->_sync_period / 10)
	ok_tdiff = 0;
    }

  idx = coll->_history[0]._num_diffs++;

  if (i_sync < LWROC_MERGE_ANA_COLL_SYNC_CHECK)
    {
      if (ok_tdiff)
	coll->_u._diffs[idx & (LWROC_MERGE_ANA_MAX_DIFFS-1)] = diff32;
    }
  else
    {
      uint16_t ref_value   = ref_sync_check & SYNC_CHECK_VALUE_MASK;
      uint16_t check_value = sync_check     & SYNC_CHECK_VALUE_MASK;

      int ref_hasval  = (ref_sync_check) &&
	(ref_sync_check & (SYNC_CHECK_REF | SYNC_CHECK_RECV));
      int src_hasval = (    sync_check) &&
	(    sync_check & (SYNC_CHECK_REF | SYNC_CHECK_RECV));

      int ref_local = !!(ref_sync_check & SYNC_CHECK_LOCAL);
      int src_local = !!(    sync_check & SYNC_CHECK_LOCAL);

      /* To evaluate the sync_check, we first see if the reference has a
       * good value.  Then we can consider the investigated system.
       *
       * We have nine cases:
       *
       * Ref:     Client:
       * No val   No val    [bad]        Bad sender, bad client, cannot check.
       * No val   Value     [spurious]   Bad sender, cannot check.
       * No val   Local                  No sender, ok.
       * Value    No val    [bad]        Bad client, cannot check.
       * Value    Value     [mismatch?]  CHECK.
       * Value    Local     [missing]    Missed the real one.
       * Local    No val    [bad]        Bad client, (random coinc).
       * Local    Value     [mismatch?/  Extra - but CHECK.
       *                     spurious]
       * Local    Local                  Random coinc., ok.
       *
       * In addition, a client can account a spurious count for any
       * event that ends up lost in-between reference events, and
       * missing when the reference system got no client event at all
       * to compare to.
       */

      if (!src_hasval)
	{
	  if (!src_local)
	    {
	      LWROC_MERGE_ANA_SC_ISSUE(src, NO_CHECKVAL);
	      /*
	      LWROC_WARNING_FMT("Sync check no value, id=%02x.  "
				"[fl:%x]",
				src->_id,
				(sync_check >> 16) & 0x0f);
	      */
	    }
	  else if (ref_hasval && !ref_local)
	    LWROC_MERGE_ANA_SC_ISSUE(src, MISSING);
	  else
	    { } /* Actually local.  TODO: count. */
	}
      else
	{
	  if (!ref_hasval)
	    {
	      /* Reference has no value.  If source has non-local
	       * value, where did it get if from?  -> Spurious.
	       */
	      if (!src_local)
		LWROC_MERGE_ANA_SC_ISSUE(src, SPURIOUS);
	      else
		LWROC_MERGE_ANA_SC_ISSUE(src, NO_REF);
	    }
	  else
	    {
	      /* We are now down to four cases:
	       *
	       * Both reference and src (client) has values, may be
	       * local.
	       *
	       * If the client claims a non-local value, then we check
	       * match regardless of of reference is local or not.
	       * Client may have been listened to a global trigger
	       * anyhow (and it claims non-local!), so better match.
	       */

	      if (!src_local)
		{
		  lwroc_linear_fit_sol *sol = &src->_sync_check_sol;
		  lwroc_merge_ana_sync_check *sync_check_item =
		    &(coll->_u.
		      _sync_checks[idx &
				   (LWROC_MERGE_ANA_MAX_SYNC_CHECKS-1)]);

		  /* Add to the fit. */

		  if (ok_tdiff)
		    {
		      sync_check_item->_ref   = ref_value;
		      sync_check_item->_check = check_value;
		    }

		  /* Regardless of the timestamp match or not, the sync
		   * check value should match.
		   */

		  if (sol->dydx != 0.0)
		    {
		      double check_expect;
		      double abs_diff;

		      check_expect = sol->y0 + ref_value * sol->dydx;

		      abs_diff = fabs(check_expect - check_value);

		      /* To possibly be a good event, we must be within
		       * the expected value tolerance.
		       */
		      if (abs_diff < src->_sync_check_tol)
			{
			  /* To not be ambiguous, we must be further
			   * away of a neighbour than the tolerance
			   * for that one.
			   */
			  if (abs_diff < (src->_sync_check_separation -
					  src->_sync_check_tol))
			    LWROC_MERGE_ANA_SC_ISSUE(src, GOOD);
			  else
			    LWROC_MERGE_ANA_SC_ISSUE(src, AMBIGUOUS);
			}
		      else
			{
			  LWROC_MERGE_ANA_SC_ISSUE(src, MISMATCH);

#if LWROC_ANALYSE_SYNC_CHECK_DEBUG && 0
			  printf ("(%" PRId64 ") %08x %08x\n",
				  diff,
				  ref_sync_check, sync_check);

			  printf ("%6.1f + %6d * %6.1f = %6.1f, "
				  "got %d (tol %6.1f) "
				  "reffromval: %.1f "
				  "(ref_ts: %" PRIu64 ", ts: %" PRIu64 ", "
				  "diff: %" PRId64 ")",
				  sol->y0, ref_value, sol->dydx,
				  check_expect,
				  check_value,
				  src->_sync_check_tol,
				  (check_value - sol->y0) / sol->dydx,
				  ref_timestamp, sync_timestamp, diff);
			  printf (" - MISMATCH");
			  printf ("\n");
#endif
			  lwroc_message_rate_ctrl
			    (LWROC_MESSAGE_RATE_CTRL_LIMIT);
			  LWROC_WARNING_FMT("Sync check mismatch, id=%02x.  "
					    "ref=%d [fl:%x] val=%d [fl: %x] "
					    "expect = %.1f +/- %.1f = "
					    "%.1f + ref * %.1f "
					    "(ref-from-value: %.1f) "
					    "(ref_ts: %04x:%04x:%04x:%04x, "
					    "ts: %04x:%04x:%04x:%04x, "
					    "diff: %" PRId64 ")",
					    src->_id,
					    ref_value,
					    (ref_sync_check >> 16) & 0x0f,
					    check_value,
					    (sync_check >> 16) & 0x0f,
					    check_expect,
					    src->_sync_check_tol,
					    sol->y0, sol->dydx,
					    (check_value -
					     sol->y0) / sol->dydx,
					    (int)(ref_timestamp  >> 48)&0xffff,
					    (int)(ref_timestamp  >> 32)&0xffff,
					    (int)(ref_timestamp  >> 16)&0xffff,
					    (int)(ref_timestamp       )&0xffff,
					    (int)(sync_timestamp >> 48)&0xffff,
					    (int)(sync_timestamp >> 32)&0xffff,
					    (int)(sync_timestamp >> 16)&0xffff,
					    (int)(sync_timestamp      )&0xffff,
					    diff);
			}
		    }
		  else
		    {
		      /* No fit.
		       * Could be its own issue, but count as ambiguous.
		       */
		      LWROC_MERGE_ANA_SC_ISSUE(src, AMBIGUOUS);
		    }
		}
	      else
		{
		  /* If the client has a local value while reference
		   * had global, then we believe the good one to be
		   * missing.
		   */
		  if (!ref_local)
		    LWROC_MERGE_ANA_SC_ISSUE(src, MISSING);
		  /* TODO: count the actually local. */
		}
	    }
	}
    }
}

/********************************************************************/

int
lwroc_compare_uint64_t(const void *a, const void *b)
{
  if (*((const uint64_t *) a) < *((const uint64_t *) b))
    return -1;
  return *((const uint64_t *) a) > *((const uint64_t *) b);
}

void lwroc_merge_ana_ref_sync_timestamp(uint64_t ref_prev_timestamp,
					uint64_t ref_timestamp)
{
  uint64_t ref_sync_diff = ref_timestamp - ref_prev_timestamp;

  _lwroc_merge_analyse->
    _stamp_sync_diffs[(_lwroc_merge_analyse->
		       _fill_sync_diffs) %
		      LWROC_MERGE_ANA_MAX_SYNC_DIFFS] =
    ref_sync_diff;
  /*
    printf ("sync diff %d: %" PRIu64".\n",
	    (_lwroc_merge_analyse->
	     _fill_sync_diffs) %
	    LWROC_MERGE_ANA_MAX_SYNC_DIFFS,
	    ref_sync_diff);
  */

  /* If someone sends us a very short interval, we quickly
   * drop that we have an estimate (it is clearly
   * out-of-whack).
   */
  if (_lwroc_merge_analyse->_has_sync_period &&
      ref_sync_diff < _lwroc_merge_analyse->_sync_period / 5)
    {
      _lwroc_merge_analyse->_num_sync_diffs -= 8;

      if (_lwroc_merge_analyse->_num_sync_diffs <= 0)
	{
	  _lwroc_merge_analyse->_num_sync_diffs = 0;
	  /*
	    printf ("Fast clear sync trigger interval estimate.\n");
	  */
	  _lwroc_merge_analyse->_has_sync_period = 0;
	}
    }

  _lwroc_merge_analyse->_fill_sync_diffs++;
  _lwroc_merge_analyse->_num_sync_diffs++;

  if (_lwroc_merge_analyse->_num_sync_diffs >=
      LWROC_MERGE_ANA_MAX_SYNC_DIFFS)
    {
      uint64_t tmp_diffs[LWROC_MERGE_ANA_MAX_SYNC_DIFFS];

      /* Temporary since we sort. */
      memcpy(tmp_diffs, _lwroc_merge_analyse->_stamp_sync_diffs,
	     sizeof (tmp_diffs));
      /*
	for (int i = 0; i < 16; i++)
	  printf ("  %" PRIu64"", tmp_diffs[i]);
	printf ("\n");
      */
      qsort (tmp_diffs,
	     LWROC_MERGE_ANA_MAX_SYNC_DIFFS, sizeof (uint64_t),
	     lwroc_compare_uint64_t);
      /*
	for (int i = 0; i < 16; i++)
	  printf ("  %" PRIu64"", tmp_diffs[i]);
	printf ("\n");
      */
      _lwroc_merge_analyse->_sync_period = tmp_diffs[2];
      _lwroc_merge_analyse->_has_sync_period = 1;
      /*
	printf ("sync trigger interval lower estimate: %" PRIu64".\n",
		_lwroc_merge_analyse->_sync_period);
      */
      /* Forget half the data. */
      _lwroc_merge_analyse->_num_sync_diffs =
	LWROC_MERGE_ANA_MAX_SYNC_DIFFS / 2;
    }
}

void lwroc_merge_ana_ref_vs_own_timestamp(uint64_t ref_prev_timestamp,
					  uint64_t ref_timestamp,
					  uint32_t ref_prev_sync_check,
					  uint32_t ref_sync_check,
					  lwroc_merge_ana_src *src,
					  lwroc_merge_ana_src_collect *coll,
					  int i_sync)
{
  /* Both cur and prev timestamps of reference are good.
   * We can calculate the differences.
   *
   * A bad (zero) src timestamp will generally be killed by
   * the ordering check.
   */

  /* If a source has seen (at least) two timestamps between
   * the two reference stamps, then the first belongs to the
   * previous timestamp and the second to the current.
   *
   * If a source only has seen one timestamp, we assume it to
   * belong to one one which it it closest to.
   */

  if (coll->_stamps == (LWROC_MERGE_ANA_SRC_STAMPS1 |
			LWROC_MERGE_ANA_SRC_STAMPS2))
    {
      /* Difference is always src - reference. */

      int64_t diff1 =
	(int64_t) (coll->_timestamp1 - ref_prev_timestamp);
      int64_t diff2 =
	(int64_t) (coll->_timestamp2 - ref_timestamp);

      /* Do not use the differences if the timestamps were
       * unordered.
       */
      if (diff1 >= 0)
	{
	  lwroc_merge_analyse_add_diff(src, coll, i_sync, diff1,
				       ref_prev_sync_check,
				       coll->_sync_check1,
				       ref_prev_timestamp,
				       coll->_timestamp1);
	  /* Tested with the first reference stamp. */
	  src->_stamps_sync_check |= LWROC_MERGE_ANA_SRC_STAMPS1;
	}
      else
	{
	  coll->_history[0]._num_lost++;
	  /* coll->_history[0]._num_lost_debug[1]++; */
	}
      if (diff2 <= 0)
	{
	  lwroc_merge_analyse_add_diff(src, coll, i_sync, diff2,
				       ref_sync_check,
				       coll->_sync_check2,
				       ref_timestamp,
				       coll->_timestamp2);
	  /* Tested with the second reference stamp. */
	  src->_stamps_sync_check |= LWROC_MERGE_ANA_SRC_STAMPS2;
	}
      else
	{
	  coll->_history[0]._num_lost++;
	  /* coll->_history[0]._num_lost_debug[2]++; */
	}
    }
  else if (coll->_stamps == LWROC_MERGE_ANA_SRC_STAMPS1)
    {
      int64_t diff1 =
	(int64_t) (coll->_timestamp1 - ref_prev_timestamp);
      int64_t diff2 =
	(int64_t) (coll->_timestamp1 - ref_timestamp);

      /* Only use the timestamps if correctly ordered. */
      if (diff1 >= 0 && diff2 <= 0)
	{
	  if (diff1 < -diff2)
	    {
	      lwroc_merge_analyse_add_diff(src, coll, i_sync, diff1,
					   ref_prev_sync_check,
					   coll->_sync_check1,
					   ref_prev_timestamp,
					   coll->_timestamp1);
	      /* Tested with the first reference stamp. */
	      src->_stamps_sync_check |= LWROC_MERGE_ANA_SRC_STAMPS1;
	    }
	  else
	    {
	      lwroc_merge_analyse_add_diff(src, coll, i_sync, diff2,
					   ref_sync_check,
					   coll->_sync_check1,
					   ref_timestamp,
					   coll->_timestamp1);
	      /* Tested with the second reference stamp. */
	      src->_stamps_sync_check |= LWROC_MERGE_ANA_SRC_STAMPS2;
	    }
	}
      else
	{
	  coll->_history[0]._num_lost++;
	  /* coll->_history[0]._num_lost_debug[3]++; */
	  /*
	    printf ("%016" PRIu64" %016" PRIu64" %016" PRIu64" \n"
		    "%016" PRIu64" %016" PRIu64"\n",
		    REF_PREV_TIMESTAMP,
		    coll->_timestamp1,
		    event->_timestamp,
		    diff1, diff2);
	  */
	}
    }

  coll->_stamps = 0;

  if (i_sync == LWROC_MERGE_ANA_COLL_SYNC_CHECK)
    {
      int stamp2;

      /* The first reference stamp is now going out of scope, so src
       * had all its chances.  If we have not tried to match against
       * any, then the src has missed.
       */

      if (!(src->_stamps_sync_check & LWROC_MERGE_ANA_SRC_STAMPS1))
	LWROC_MERGE_ANA_SC_ISSUE(src, MISSING);

      /* Second reference stamp will become the first. */

      stamp2 = (src->_stamps_sync_check & LWROC_MERGE_ANA_SRC_STAMPS2);

      src->_stamps_sync_check =
	(stamp2 ? LWROC_MERGE_ANA_SRC_STAMPS1 : 0);
    }
}

int lwroc_merge_ana_insert_own_timestamp(lwroc_merge_ana_timestamp *event,
					 lwroc_merge_ana_src_collect *coll)
{
  int spurious = 0;

  /* Have we seen a timestamp already since the last reference
   * event?
   *
   * This could easily happen: reference has event, we slightly
   * after (tstamp-wise).  And next one, we are just before the
   * reference.
   */

  if (!coll->_stamps)
    {
      coll->_timestamp1 = event->_timestamp;
      coll->_sync_check1 = event->_sync_check;
      coll->_stamps = LWROC_MERGE_ANA_SRC_STAMPS1;
    }
  else
    {
      if (coll->_stamps & LWROC_MERGE_ANA_SRC_STAMPS2)
	{
	  coll->_history[0]._num_lost++;
	  /* coll->_history[0]._num_lost_debug[4]++; */

	  /* Spurious, unless it is a local trigger. */
	  if ((event->_sync_check & SYNC_CHECK_LOCAL))
	    spurious = 1;
	}

      coll->_timestamp2 = event->_timestamp;
      coll->_sync_check2 = event->_sync_check;
      coll->_stamps = (LWROC_MERGE_ANA_SRC_STAMPS1 |
		       LWROC_MERGE_ANA_SRC_STAMPS2);
    }

  return spurious;
}

void lwroc_merge_analyse_handle_timestamp(lwroc_merge_ana_timestamp *event)
{
  lwroc_merge_ana_src *src_this = NULL;
  lwroc_merge_ana_src_collect *coll;

  /* Can we add the info about the src index?
   * And if it is not the reference, its own info?
   */
  if (event->_src_id < LWROC_MERGE_ANA_NUM_SRC_ID)
    {
      /* Fixed 0 - we keep track of the src_index in the
       * (normal) collection.  This such that the
       * monitoring report can share structure.
       */
      int i_sync = LWROC_MERGE_ANA_COLL_NON_SYNC;

      src_this = &_lwroc_merge_analyse->_srcs[event->_src_id];
      coll = &src_this->_coll[i_sync];

      /* Record that we have seen this timestamp ID. */
      /* printf ("%d\n",  event->_src_i); */
      LWROC_MERGE_ANA_SC_ISSUE_SEEN(src_this, HAD_EVENT);

      if (event->_src_index <
	  sizeof (coll->_history[0]._src_index_mask) * 8)
	{
	  /* Mask of sources that have contributed to this ID. */
	  coll->_history[0]._src_index_mask |=
	    ((uint64_t) 1) << event->_src_index;
	}
      else
	{
	  _lwroc_merge_analyse->_warn_unhandled_idx++;

	  if (_lwroc_merge_analyse->_warn_unhandled_idx >=
	      _lwroc_merge_analyse->_next_warn_unhandled_idx)
	    {
	      _lwroc_merge_analyse->_next_warn_unhandled_idx *= 2;
	      LWROC_WARNING_FMT("Timestamp for system with "
				"source index %d (> %d), "
				"cannot be handled by alignment analysis "
				"(occurrence %" PRIu64 ").",
				event->_src_index,
				(int) sizeof (coll->_history[0].
					      _src_index_mask) * 8,
				_lwroc_merge_analyse->_warn_unhandled_idx);
	    }
	}
    }
  else
    {
      _lwroc_merge_analyse->_warn_unhandled_id++;

      if (_lwroc_merge_analyse->_warn_unhandled_id >=
	  _lwroc_merge_analyse->_next_warn_unhandled_id)
	{
	  _lwroc_merge_analyse->_next_warn_unhandled_id *= 2;
	  LWROC_WARNING_FMT("Timestamp for system with ID %d (> %d), "
			    "cannot be handled by alignment analysis "
			    "due to compile-time limit "
			    "(occurrence %" PRIu64 ").",
			    event->_src_id,
			    LWROC_MERGE_ANA_NUM_SRC_ID,
			    _lwroc_merge_analyse->_warn_unhandled_id);
	}
    }

  /*
  if (event->_trigger == _lwroc_merge_analyse->_sync_trig)
    {
      printf ("periodic %d: %" PRIu64 "\n",
	      event->_src_id,
	      event->_timestamp);
    }
  */
  /*
  printf ("%d: t: %d  ts: %" PRIx64 "  check: %08x\n",
	  event->_src_id,
	  event->_trigger,
	  event->_timestamp,
	  event->_sync_check);
  */

  /* We have two different cases: either it is a timestamp
   * for the reference (master) system, or it is a
   * timestamp for another system.
   */

  if (event->_src_id == _lwroc_merge_analyse->_ref_src_id)
    {
      int r_i_sync = LWROC_MERGE_ANA_COLL_NON_SYNC;
      int j_i_sync;
      int i_sync;

      if (event->_trigger == _lwroc_merge_analyse->_sync_trigs[event->_src_id])
	r_i_sync = LWROC_MERGE_ANA_COLL_SYNC_TRIG;

      /* Do this both for (no)sync trigger, and sync check values. */

      for (j_i_sync = 0, i_sync = r_i_sync;   j_i_sync < 2;
	   j_i_sync++,   i_sync = LWROC_MERGE_ANA_COLL_SYNC_CHECK)
	{
#define REF_PREV_TIMESTAMP   _lwroc_merge_analyse->_ref_prev_timestamp[i_sync]
#define REF_PREV_SYNC_CHECK  _lwroc_merge_analyse->_ref_prev_sync_check

	  if (REF_PREV_TIMESTAMP &&
	      event->_timestamp &&
	      REF_PREV_TIMESTAMP < event->_timestamp)
	    {
	      int src_id;

	      if (i_sync == LWROC_MERGE_ANA_COLL_SYNC_TRIG)
		{
		  /* Investigate the period _between_ reference sync
		   * trigger timestamps.
		   */
		  lwroc_merge_ana_ref_sync_timestamp(REF_PREV_TIMESTAMP,
						     event->_timestamp);
		}

	      /* TODO: this loop is expensive as it visits all IDs.
	       * Optimise by only visiting those that have been seen
	       * (lately).
	       */

	      for (src_id = 0; src_id < LWROC_MERGE_ANA_NUM_SRC_ID; src_id++)
		{
		  lwroc_merge_ana_src *src =
		    &_lwroc_merge_analyse->_srcs[src_id];

		  coll = &src->_coll[i_sync];

		  lwroc_merge_ana_ref_vs_own_timestamp(REF_PREV_TIMESTAMP,
						       event->_timestamp,
						       REF_PREV_SYNC_CHECK,
						       event->_sync_check,
						       src, coll, i_sync);
		}
	    }
	  else
	    {
	      int src_id;

	      /* Current or previous reference timestamp bad.  Discard
	       * all source values.
	       */

	      /* TODO: same optimisation as above. */

	      for (src_id = 0; src_id < LWROC_MERGE_ANA_NUM_SRC_ID; src_id++)
		{
		  lwroc_merge_ana_src *src =
		    &_lwroc_merge_analyse->_srcs[src_id];
		  coll = &src->_coll[i_sync];

		  coll->_stamps = 0;
		}
	    }

	  REF_PREV_TIMESTAMP = event->_timestamp;
	}

      REF_PREV_SYNC_CHECK = event->_sync_check;

      /* We count either it is good or bad. */
      _lwroc_merge_analyse->_num_ref_timestamps[r_i_sync]++;
    }
  else if (src_this != NULL)
    {
      int i_sync = LWROC_MERGE_ANA_COLL_NON_SYNC;

      if (event->_trigger == _lwroc_merge_analyse->_sync_trigs[event->_src_id])
	i_sync = LWROC_MERGE_ANA_COLL_SYNC_TRIG;

      coll = &src_this->_coll[i_sync];

      lwroc_merge_ana_insert_own_timestamp(event, coll);

      /* And insert for the sync check. */

      coll = &src_this->_coll[LWROC_MERGE_ANA_COLL_SYNC_CHECK];

      if (lwroc_merge_ana_insert_own_timestamp(event, coll))
	LWROC_MERGE_ANA_SC_ISSUE(src_this, SPURIOUS);
    }
}

/********************************************************************/

void lwroc_merge_analyse_add_timestamp(size_t src_index,
				       uint32_t src_id,
				       uint64_t timestamp,
				       uint32_t sync_check,
				       uint16_t trigger)
{
  lwroc_merge_ana_timestamp *entry;

  /* printf ("%2zd  %2d  %" PRIu64 "\n", src_index, src_id, timestamp); */

  entry = (lwroc_merge_ana_timestamp *)
    lwroc_pipe_buffer_alloc_write(_lwroc_merge_analyse->_queue,
				  sizeof (lwroc_merge_ana_timestamp),
				  NULL,
				  _lwroc_main_thread->_block,
				  1);

  /* We got an item to write to.  Fill it out. */

  entry->_src_index  = (uint32_t) src_index;
  entry->_src_id     = (uint16_t) src_id;
  entry->_trigger    = trigger;
  entry->_timestamp  = timestamp;
  entry->_sync_check = sync_check;

  lwroc_pipe_buffer_did_write(_lwroc_merge_analyse->_queue,
			      sizeof (lwroc_merge_ana_timestamp), 0);
}

/********************************************************************/

void lwroc_merge_analyse_thread_loop(lwroc_thread_instance *inst)
{
  lwroc_pipe_buffer_consumer *consumer;

  consumer = lwroc_pipe_buffer_get_consumer(_lwroc_merge_analyse->_queue, 0);

  for ( ; ; )
    {
      char *ptr;
      size_t avail;
      size_t fill;
      int terminate;

      terminate = inst->_terminate;

      /* Look for data. */

      avail = lwroc_pipe_buffer_avail_read(consumer,
					   inst->_block,
					   &ptr, NULL, 0, 0);

      /* If terminate was set before we checked (such that not more
       * data became available after the check, but before terminate
       * being set), we quit when we are out of data.
       */
      if (!avail)
	{
	  if (terminate)
	    break;
	}

      /* Only add items from queue if we are past some reasonable
       * amount of hysteresis.  Or if we are about to update
       * the monitor block.
       */

      if (avail/* &&
	  ((avail >
	    (LWROC_MERGE_ANA_BUF_ENTRIES / 8) *
	    sizeof (lwroc_merge_ana_timestamp)) ||
	    (_lwroc_mon_update_seq != _lwroc_mon_analyse_handle->_update_seq))*/)
	{
	  char *end = ptr + avail;
	  lwroc_merge_ana_timestamp *entry =
	    (lwroc_merge_ana_timestamp *) ptr;
	  size_t consumed;
	  /*
	  printf ("got: %zd\n",
		  avail / sizeof (lwroc_merge_ana_timestamp));
	  */
	  while (((char *) entry) < end)
	    {
	      lwroc_merge_analyse_handle_timestamp(entry);
	      entry++;
	    }

	  consumed = (size_t) ((char *) entry - ptr);

	  lwroc_pipe_buffer_did_read(consumer, consumed);
	}

      /* Should we perform the analysis? */

      if (_lwroc_mon_update_seq != _lwroc_mon_analyse_handle->_update_seq)
	{
	  /*
	  printf ("seq: %d - %d\n",
		  _lwroc_mon_update_seq,
		  _lwroc_mon_analyse_handle->_update_seq);
	  */

	  lwroc_merge_do_analyse();

	  LWROC_MON_CHECK_COPY_BLOCK(_lwroc_mon_analyse_handle,
				     &(_lwroc_mon_analyse->_analyse),0);

	  continue;
	}

      /* If we got more than half the hysteresis value, try directly
       * again.  Otherwise, wait for a token.
       */

      if (avail > (LWROC_MERGE_ANA_BUF_ENTRIES / 3) *
	  sizeof (lwroc_merge_ana_timestamp))
	continue;

      lwroc_pipe_buffer_notify_consumer(consumer, inst->_block,
					(LWROC_MERGE_ANA_BUF_ENTRIES / 2) *
					sizeof (lwroc_merge_ana_timestamp));

      /* Did it fill up while we requested notification? */

      fill =
	lwroc_pipe_buffer_fill_due_to_consumer(consumer);

      if (fill >= (LWROC_MERGE_ANA_BUF_ENTRIES / 2) *
	  sizeof (lwroc_merge_ana_timestamp))
	{
	  continue;
	}

      if (terminate)
	{
	  /* Either we are now empty, or we have more to process.  It
	   * will not be a loop without progress.
	   */
	  continue;
	}

      /* We got no data.  Wait for a wake-up token. */

      lwroc_thread_block_get_token(inst->_block);
    }

}

/********************************************************************/

lwroc_thread_info _merge_analyse_thread_info =
{
  lwroc_merge_analyse_thread_at_prepare,
  lwroc_merge_analyse_thread_loop,
  LWROC_MSG_BUFS_ANALYSE,
  "merge analyse",
  0,
  LWROC_THREAD_TERM_MERGE_ANALYSE,
  LWROC_THREAD_CORE_PRIO_ANALYSE,
};

/********************************************************************/

lwroc_thread_instance *_lwroc_merge_analyse_thread = NULL;

/********************************************************************/

void lwroc_merge_analyse_init(void)
{
  uint16_t src_id;

  _lwroc_merge_analyse =
    (lwroc_merge_analyse *) malloc (sizeof (lwroc_merge_analyse));

  if (!_lwroc_merge_analyse)
    LWROC_FATAL("Memory allocation failure (merge (tstamp) analysis).");

  memset(_lwroc_merge_analyse, 0, sizeof (lwroc_merge_analyse));

  _lwroc_merge_analyse->_next_warn_unhandled_id = 1;

  _lwroc_merge_analyse->_ref_src_id = _config._merge_ts_analyse_ref_id;

  for (src_id = 0; src_id < LWROC_MERGE_ANA_NUM_SRC_ID; src_id++)
    {
      _lwroc_merge_analyse->_srcs[src_id]._id = src_id;

      _lwroc_merge_analyse->_sync_trigs[src_id] =
	_config._merge_ts_analyse_sync_trig;

      if (_config._merge_ts_analyse_sync_trigs[src_id])
	_lwroc_merge_analyse->_sync_trigs[src_id] =
	  _config._merge_ts_analyse_sync_trigs[src_id];

      /* printf ("< %d:%d >",
	      src_id, _lwroc_merge_analyse->_sync_trigs[src_id]); */
    }

  lwroc_pipe_buffer_init(&_lwroc_merge_analyse->_queue,
			 1, NULL,
			 LWROC_MERGE_ANA_BUF_ENTRIES *
			 sizeof (lwroc_merge_ana_timestamp),
			 (LWROC_MERGE_ANA_BUF_ENTRIES / 2) *
			 sizeof (lwroc_merge_ana_timestamp));

  _lwroc_merge_analyse_thread =
    lwroc_thread_prepare(&_merge_analyse_thread_info);
}

/********************************************************************/

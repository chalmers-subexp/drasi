/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2019  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_MERGE_ANALYSE_H__
#define __LWROC_MERGE_ANALYSE_H__

#include "../dtc_arch/acc_def/mystdint.h"

#include "lwroc_merge_analyse_defs.h"
#include "lwroc_track_timestamp_extra.h"

typedef struct lwroc_merge_ana_timestamp_t
{
  /* Must be first item, as it is also used for waste marker. */
  uint32_t _src_index;
  uint16_t _src_id;
  uint16_t _trigger;
  uint64_t _timestamp;  /* Is 64-bit aligned. */
  uint32_t _sync_check;

} lwroc_merge_ana_timestamp;

/* This buffer size matters - but not so much... */
#define LWROC_MERGE_ANA_BUF_ENTRIES  2048/*1024*//*8192*//*16384*/

#define LWROC_MERGE_ANA_SRC_STAMPS1  0x01
#define LWROC_MERGE_ANA_SRC_STAMPS2  0x02

/* Analysis time: 4 GHz CPU (E3-v6), does 0x1000 = 4k entries in ~250 us. */
/* A 2.5 GHz CPU (sandy bridge), does 0x1000 = 4k entries in ~500 us. */
#define LWROC_MERGE_ANA_MAX_DIFFS    0x1000 /* 4k (Must be 2^n.) */

#define LWROC_MERGE_ANA_NUM_HISTORY  8

/* Do at least 100 sync checks values, in case the span of reference
 * values is 64 (very unlikely).  (Large span means large transmission
 * times, so smaller spans much more likely.
 */
#define LWROC_MERGE_ANA_MAX_SYNC_CHECKS  0x100 /* (Must be 2^n.) */

typedef struct lwroc_merge_ana_src_history_t
{
  uint32_t _num_diffs;
  uint32_t _num_refs;  /* Number of reference timestamps seen. */
  uint32_t _num_lost;  /* Number of timestamps we had that were
			* not added to the diff list at all. */

  /* uint32_t _num_lost_debug[5]; */

  uint64_t _src_index_mask;

} lwroc_merge_ana_src_history;

typedef struct lwroc_merge_ana_sync_check_t
{
  uint16_t _ref;
  uint16_t _check;
} lwroc_merge_ana_sync_check;

/* We do three kinds of collections:
 * 0: this-vs-ref timestamp for non-sync triggers (mean+std).
 * 1: this-vs-ref timestamp for sync triggers     (mean+std).
 * 2: this-vs-ref sync check value for any trigger (linear fit).
 *
 * Since the sync check values should match (or not) regardless of
 * sync triggers, that collection is separate (in order to not
 * break the sequence of last timestamp due to sync trigger).
 * The timestamps are used to assign to the 'closest' event.
 * I.e., the verification of sync check values in a sense check
 * that this closest-to-reference value is working.
 */
#define LWROC_MERGE_ANA_COLL_NON_SYNC    0
#define LWROC_MERGE_ANA_COLL_SYNC_TRIG   1
#define LWROC_MERGE_ANA_COLL_TRIG_NUM    2 /* Just the two above. */
#define LWROC_MERGE_ANA_COLL_SYNC_CHECK  2
#define LWROC_MERGE_ANA_COLL_NUM         3 /* All three. */

typedef struct lwroc_merge_ana_src_collect_t
{
  uint64_t _timestamp1;
  uint64_t _timestamp2;
  int      _stamps;
  /* In principle only used for coll[LWROC_MERGE_ANA_COLL_SYNC_CHECK],
   * but easier coding to let them follow all.
   */
  uint32_t _sync_check1;
  uint32_t _sync_check2;

  time_t   _last_history_bump;

  /* This is the expensive part, the history buffer. */
  union
  {
    int32_t  _diffs[LWROC_MERGE_ANA_MAX_DIFFS]; /* TODO! reduce for sync */

    lwroc_merge_ana_sync_check _sync_checks[LWROC_MERGE_ANA_MAX_SYNC_CHECKS];
  } _u;

  lwroc_merge_ana_src_history _history[LWROC_MERGE_ANA_NUM_HISTORY];

} lwroc_merge_ana_src_collect;

#define LWROC_MERGE_ANA_SC_IDX_NO_CHECKVAL   0
#define LWROC_MERGE_ANA_SC_IDX_NO_REF        1
#define LWROC_MERGE_ANA_SC_IDX_GOOD          2  /* Issue as such inverted. */
#define LWROC_MERGE_ANA_SC_IDX_AMBIGUOUS     3
#define LWROC_MERGE_ANA_SC_IDX_MISMATCH      4
#define LWROC_MERGE_ANA_SC_IDX_MISSING       5
#define LWROC_MERGE_ANA_SC_IDX_SPURIOUS      6
#define LWROC_MERGE_ANA_SC_NUM_ISSUES        7

#define LWROC_MERGE_ANA_SC_IDX_HAD_EVENT     7   /* No issue - just seen event.
						  * Only used for seen mask.
						  */
#define LWROC_MERGE_ANA_SC_IDX_UNEXPECTED    8   /* Only used for reporting. */
#define LWROC_MERGE_ANA_SC_IDX_MISSING_OK    9   /* Only used for reporting. */

#define LWROC_MERGE_ANA_SC_ISSUE_BIT(issue)		\
  (((uint16_t) 1) << LWROC_MERGE_ANA_SC_IDX_##issue)

#define LWROC_MERGE_ANA_SC_SEEN_CUR_ROUND    0
#define LWROC_MERGE_ANA_SC_SEEN_CUR_PERIOD   1
#define LWROC_MERGE_ANA_SC_SEEN_PREV_PERIOD  2
#define LWROC_MERGE_ANA_SC_SEEN_NUM_RECORD   3

#define LWROC_MERGE_ANA_SC_ISSUE_SEEN(src, issue) do {			\
    (src)->_sc_issues_seen[LWROC_MERGE_ANA_SC_SEEN_CUR_ROUND] |=	\
      LWROC_MERGE_ANA_SC_ISSUE_BIT(issue);				\
  } while (0)

#define LWROC_MERGE_ANA_SC_ISSUE(src, issue) do {			\
    (src)->_sync_check_issues[LWROC_MERGE_ANA_SC_IDX_##issue]++;	\
    LWROC_MERGE_ANA_SC_ISSUE_SEEN(src, issue);				\
  } while (0)

typedef struct lwroc_merge_ana_src_t
{
  lwroc_merge_ana_src_collect _coll[LWROC_MERGE_ANA_COLL_NUM];

  int      _stamps_sync_check;

  lwroc_linear_fit_sol _sync_check_sol;

  double   _sync_check_tol;
  double   _sync_check_separation;

  uint64_t _sync_check_issues[LWROC_MERGE_ANA_SC_NUM_ISSUES];

  uint16_t _id;

  uint16_t _sc_issues_seen[LWROC_MERGE_ANA_SC_SEEN_NUM_RECORD];

} lwroc_merge_ana_src;

/* Number of diffs to collect before guessing) sync trigger period. */
#define LWROC_MERGE_ANA_MAX_SYNC_DIFFS  16

typedef struct lwroc_merge_analyse_t
{
  uint64_t _ref_prev_timestamp[LWROC_MERGE_ANA_COLL_NUM];
  uint32_t _ref_prev_sync_check;

  uint32_t _ref_src_id;

  uint16_t _sync_trigs[LWROC_MERGE_ANA_NUM_SRC_ID];

  uint64_t _warn_unhandled_id;
  uint64_t _next_warn_unhandled_id;

  uint64_t _warn_unhandled_idx;
  uint64_t _next_warn_unhandled_idx;

  uint32_t _num_ref_timestamps[LWROC_MERGE_ANA_COLL_TRIG_NUM];

  lwroc_merge_ana_src _srcs[LWROC_MERGE_ANA_NUM_SRC_ID];

  /* Estimation of intervals between sync triggers. */
  uint64_t _stamp_sync_diffs[LWROC_MERGE_ANA_MAX_SYNC_DIFFS];
  uint32_t _fill_sync_diffs;
  int      _num_sync_diffs;
  time_t   _last_bump_sync_diffs;

  time_t   _last_dump_sync_issues;

  /* Intervals between sync triggers. */
  uint64_t _sync_period;
  int      _has_sync_period;

  /* During analysis (sorts values, so must be scratch). */
  union
  {
    int32_t  _tmp_diffs[LWROC_MERGE_ANA_MAX_DIFFS];

    struct
    {
      lwroc_linear_fit_data _tmp[LWROC_MERGE_ANA_MAX_SYNC_CHECKS];
      double _tmp2_array[2 * LWROC_MERGE_ANA_MAX_SYNC_CHECKS];
    } _sync_checks;
  } _u;

  /* Communication with the merging stage. */

  lwroc_pipe_buffer_control  *_queue;

} lwroc_merge_analyse;

extern lwroc_merge_analyse *_lwroc_merge_analyse;

void lwroc_merge_analyse_add_timestamp(size_t src_index,
				       uint32_t src_id,
				       uint64_t timestamp,
				       uint32_t sync_check,
				       uint16_t trigger);

void lwroc_merge_analyse_init(void);

#endif/*__LWROC_MERGE_ANALYSE_H__*/

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2019  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_MERGE_ANALYSE_DEFS_H__
#define __LWROC_MERGE_ANALYSE_DEFS_H__

/* TODO: if we need to support many more sources, then they should be
 * allocated for as many as we actually have (with some margin), and
 * the list has to be searched to find the relevant one.  For now,
 * just use a fixed table.
 *
 * One might get away with a look-up table pointing into an array of
 * actual sources.  I.e. a two-stage.
 */
#define LWROC_MERGE_ANA_NUM_SRC_ID  64

#endif/*__LWROC_MERGE_ANALYSE_DEFS_H__*/

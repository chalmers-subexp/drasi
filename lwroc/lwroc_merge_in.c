/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_merge_struct.h"
#include "lwroc_merge_in.h"
#include "lwroc_merge_analyse.h"
#include "lwroc_merge_sort.h"
#include "lwroc_net_proto.h"
#include "lwroc_hostname_util.h"
#include "lwroc_thread_block.h"
#include "lwroc_message.h"
#include "lwroc_message_internal.h"
#include "lwroc_mon_block.h"
#include "lwroc_optimise.h"
#include "lwroc_main_iface.h"
#include "lwroc_parse_util.h"
#include "lwroc_net_io_util.h"
#include "lwroc_serializer_util.h"

#include "gdf/lwroc_gdf_util.h"
#include "ebye/lwroc_ebye_util.h"

#include "lwroc_net_outgoing.h"
#include "lwroc_net_trigbus.h"
#include "lwroc_timeouts.h"

#include "gen/lwroc_request_msg_serializer.h"
#include "gen/lwroc_data_transport_setup_serializer.h"

#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <assert.h>
#include <unistd.h>
#include <fcntl.h>

#include "../dtc_arch/acc_def/myinttypes.h"
#include "../dtc_arch/acc_def/myprizd.h"
#include "../dtc_arch/acc_def/byteswap_include.h"
#include "../dtc_arch/acc_def/fall_through.h"

#define STATE_MACHINE_DEBUG 0

#define LWROC_MIN_INPUT_BUFFER_BUF_SIZE      0x10000

/* TODO: move this back here.  (callbacks or something such in net_io) */
/* lwroc_merge_info _lwroc_merge_info; */

void lwroc_merge_in_thread_loop(lwroc_thread_instance *inst);

/********************************************************************/

PD_LL_SENTINEL(_lwroc_merge_sources); /* active drasi + all trans */
PD_LL_SENTINEL(_lwroc_merge_val_sources); /* sources in validation */
PD_LL_SENTINEL(_lwroc_merge_all_sources); /* sources for check pipe fill */

/********************************************************************/

lwroc_thread_info _merge_in_thread_info =
{
  NULL,
  lwroc_merge_in_thread_loop,
  LWROC_MSG_BUFS_IN,
  "merge input",
  0,
  LWROC_THREAD_TERM_MERGE_IN,
  LWROC_THREAD_CORE_PRIO_MERGE_IN,
};

lwroc_thread_info _merge_val_thread_info =
{
  NULL,
  lwroc_merge_val_thread_loop,
  LWROC_MSG_BUFS_VAL,
  "merge validation",
  0,
  LWROC_THREAD_TERM_MERGE_VAL,
  LWROC_THREAD_CORE_PRIO_OTHER,
};

/********************************************************************/

lwroc_thread_instance *_lwroc_merge_in_thread = NULL;
lwroc_thread_instance *_lwroc_merge_val_thread = NULL;

/********************************************************************/

void lwroc_prepare_merge_in_thread(void)
{
  if (!_lwroc_merge_info._num_sources)
    LWROC_BADCFG("No sources to merge given.");

  _lwroc_merge_info._sources = (lwroc_merge_source **)
    malloc ((size_t) _lwroc_merge_info._num_sources *
	    sizeof (lwroc_merge_source *));
  _lwroc_merge_info._sources_removed = (lwroc_merge_source **)
    malloc ((size_t) _lwroc_merge_info._num_sources *
	    sizeof (lwroc_merge_source *));

  if (!_lwroc_merge_info._sources ||
      !_lwroc_merge_info._sources_removed)
    LWROC_FATAL("Memory allocation failure (merge source lists).");

  LWROC_INFO_FMT("Merge sources: %" MYPRIzd "",
		 _lwroc_merge_info._num_sources);
  LWROC_ITEM_BUFFER_INIT_ALLOC(&_lwroc_merge_info._fresh_sort_sources,
			       (size_t) _lwroc_merge_info._num_sources);
  LWROC_ITEM_BUFFER_INIT_ALLOC(&_lwroc_merge_info._dead_sort_sources,
			       (size_t) _lwroc_merge_info._num_sources);

  LWROC_ITEM_BUFFER_INIT_ALLOC(&_lwroc_merge_info._fresh_val_sources,
			       (size_t) _lwroc_merge_info._num_sources);
  LWROC_ITEM_BUFFER_INIT_ALLOC(&_lwroc_merge_info._redo_val_sources,
			       (size_t) _lwroc_merge_info._num_sources);
  LWROC_ITEM_BUFFER_INIT_ALLOC(&_lwroc_merge_info._dead_val_sources,
			       (size_t) _lwroc_merge_info._num_sources);

  LWROC_ITEM_BUFFER_INIT_ALLOC(&_lwroc_merge_info._connected_sources,
			       (size_t) _lwroc_merge_info._num_sources);
  LWROC_ITEM_BUFFER_INIT_ALLOC(&_lwroc_merge_info._discon_sources,
			       (size_t) _lwroc_merge_info._num_sources);

  _lwroc_merge_info._revalidate_sources = 0;
  _lwroc_merge_info._changed_sources = 0;

  _lwroc_merge_info._num_active_sources = 0;
  _lwroc_merge_info._num_activated_sources = 0;
  _lwroc_merge_info._num_finished_sources = 0;
  _lwroc_merge_info.event_number = 0;

  _lwroc_merge_info._sticky_update_pending = 0;

  /****************************************************/

  _lwroc_merge_in_thread =
    lwroc_thread_prepare(&_merge_in_thread_info);

  /****************************************************/

  if (_lwroc_merge_info._mode & LWROC_MERGE_MODE_EB)
    {
      if (!_config._merge_no_validate)
	{
	  if (PD_LL_IS_EMPTY(&_lwroc_net_eb_masters))
	    LWROC_BADCFG("Cannot merge events without master "
			 "to validate connections.");

	  _lwroc_merge_val_thread =
	    lwroc_thread_prepare(&_merge_val_thread_info);
	  /* Wake the validation thread up when terminating main,
	   * such that a LAM is sent...
	   */
	  lwroc_thread_also_wakeup_on_term(_lwroc_main_thread,
					   _lwroc_merge_val_thread);
	}
    }
  else
    {
      /* Hmmm, one might also validate time sort sources. */
      if (!PD_LL_IS_EMPTY(&_lwroc_net_eb_masters))
	LWROC_BADCFG("Time sorting merger need no master "
		     "to validate connections.");

      /* We do however analyse the timestamp alignment of the data.
       */

      if (_config._merge_ts_analyse_ref_id)
	lwroc_merge_analyse_init();
    }

  if (!PD_LL_IS_EMPTY(&_lwroc_net_masters) ||
      !PD_LL_IS_EMPTY(&_lwroc_net_slaves) ||
      !PD_LL_IS_EMPTY(&_lwroc_net_ebs))
    LWROC_BADCFG("Not in TRIVA/MI mode, but (external) "
		 "master, slave, or event builder instance given.");
}

/********************************************************************/

extern lwroc_monitor_in_block    _lwroc_mon_in;
extern lwroc_mon_block          *_lwroc_mon_in_handle;

/********************************************************************/

void lwroc_merge_in_connected(lwroc_merge_source *conn)
{
  if (conn->_out._state == LWROC_MERGE_IN_STATE_UDP_RESET_WRITE_PREPARE)
    return;
  /* printf ("conn->_out._base._mon: %p\n", conn->_out._base._mon); */
  conn->_out._base._mon->_block._status = LWROC_CONN_STATUS_CONNECTED;
  conn->_out._base._mon->_block._ipv4_addr =
    lwroc_get_ipv4_addr(&conn->_out._base._addr);
  conn->_out._base._mon->_block._port =
    lwroc_get_port(&conn->_out._base._addr);
  LWROC_MON_CHECK_COPY_CONN_MON_BLOCK(conn->_out._base._mon, 1);
}

void lwroc_merge_in_native_connected(lwroc_select_item *item,
				     lwroc_select_info *si)
{
  lwroc_merge_source *conn =
    PD_LL_ITEM(item, lwroc_merge_source, _out._base._select_item);

  (void) si;

  /* Give the connection to the operating thread.
   * (Note: we are called from the network thread.)
   */
  /* printf("Giving source to merge_in.\n"); */
  LWROC_ITEM_BUFFER_INSERT(_lwroc_merge_info._connected_sources,
			   conn);

  lwroc_merge_in_connected(conn);
}

/********************************************************************/

void lwroc_merge_in_failed_setup_retry(lwroc_merge_source *conn,
				       struct timeval *now)
{
  if (conn->_type == LWROC_MERGE_IN_TYPE_MBS_TRANS)
    {
      /* Try the legacy port after failing to set up with the port map
       * port.  After all other failures, retry the port map port.
       */
      /* printf ("Fail re-choose, prev: %d\n", conn->_port_map); */
      if (conn->_port_map == LWROC_MERGE_IN_PMAP_PMAP_PORT)
	{
	  conn->_port_map = LWROC_MERGE_IN_PMAP_LEGACY_PORT;
	  /* This is retried immediately. */
	  conn->_out._state = LWROC_MERGE_IN_STATE_FAILED_SLEEP;
	  conn->_out._next_time = *now;
	  return;
	}
      else
	conn->_port_map = LWROC_MERGE_IN_PMAP_PMAP_PORT;
    }

  if (conn->_type == LWROC_MERGE_IN_TYPE_MBS_TRANS ||
      conn->_type == LWROC_MERGE_IN_TYPE_EBYE_PULL ||
      conn->_type == LWROC_MERGE_IN_TYPE_FAKERNET)
    {
      conn->_out._state = LWROC_MERGE_IN_STATE_FAILED_SLEEP;
      conn->_out._next_time = *now;
      conn->_out._next_time.tv_sec += LWROC_CONNECTION_TRANS_RETRY_TIMEOUT;
      return;
    }
  if (conn->_type == LWROC_MERGE_IN_TYPE_FILE)
    {
      /* We will not recover, a file that failed/ended is dead. */
      conn->_out._state = LWROC_MERGE_IN_STATE_FAILED_FILE;
      return;
    }
  if (conn->_type == LWROC_MERGE_IN_TYPE_EBYE_PUSH)
    {
      /* printf("... wait_client\n"); */
      conn->_out._state = LWROC_MERGE_IN_STATE_EBYE_PUSH_WAIT_CLIENT;
      return;
    }

  assert(conn->_type == LWROC_MERGE_IN_TYPE_DRASI);

  /* Remove from our list of connections. */
  PD_LL_REMOVE(&conn->_out._base._select_item._items);
  /* Give the connection to the network thread for reconnection. */
  LWROC_ITEM_BUFFER_INSERT(_lwroc_merge_info._discon_sources, conn);
}

/********************************************************************/

/* This is also called for push clients, when the item is returned
 * to this thread (in lwroc_merge_in_loop_after_service()).
 */
void lwroc_merge_in_clear(lwroc_merge_source *conn)
{
  conn->_had_first_complete_event = 0;
  conn->_expect_fragment = 0;
  conn->_disabled_nodata = 0;
  conn->_buffer_no = 0;
  timerclear(&conn->_first_bad_stamp);
  lwroc_pipe_buffer_clear(conn->_buffer);
  /* Abused for EBYE/XFER push/pull, to tell first resize buffer size. */
  conn->_fragment_length = 1024;
  /*
  printf ("Buffer cleared: %p (%" MYPRIzd ")\n", conn,
	  lwroc_pipe_buffer_fill(conn->_buffer));
  */
}

/********************************************************************/

void lwroc_merge_in_check_fill(lwroc_merge_source *conn)
{
  if (_lwroc_merge_info._delayed_eb_enabled &&
      _lwroc_merge_info._notify_force_need)
    {
      size_t fill =
	lwroc_pipe_buffer_fill(conn->_buffer);

      if (fill > conn->_force_threshold)
	{
	  /* printf ("Notify fill watch\n"); */
	  LWROC_THREAD_BLOCK_NOTIFY(&_lwroc_merge_info._notify_force_need);
	}
    }
}

/********************************************************************/

void lwroc_merge_input_tickle_disabled(lwroc_merge_source *conn)
{
  if (conn->_disabled_nodata)
    {
      MFENCE; /* Make sure updates that made new info available is seen. */
      _lwroc_merge_info._check_nodata_sources = 1;
      MFENCE; /* Before we 'ping'. */

      /* Alert the main thread to check on this source. */
      lwroc_thread_block_send_token(_lwroc_main_thread->_block);
    }
}

/********************************************************************/

int lwroc_merge_in_buffer_read_prepare(int filestart)
{
  switch (_lwroc_merge_info._data_fmt->_buffer_format)
    {
    case LWROC_DATA_TRANSPORT_FORMAT_LMD:
      if (filestart)
	return LWROC_MERGE_IN_STATE_DATA_FILHE_READ_PREPARE;
      else
	return LWROC_MERGE_IN_STATE_DATA_BUFHE_READ_PREPARE;

    case LWROC_DATA_TRANSPORT_FORMAT_EBYE:
      if (filestart)
	return LWROC_MERGE_IN_STATE_EBYE_RECORD_READ_PREPARE;
      else
	return LWROC_MERGE_IN_STATE_EBYE_XFER_READ_PREPARE;
    case LWROC_DATA_TRANSPORT_FORMAT_XFER:
      if (filestart)
	{
	  LWROC_BUG("File input not supported in xfer mode (yet).");
	  /* No converter from EBYEDATA header to xfer header. */
	  return LWROC_MERGE_IN_STATE_EBYE_RECORD_READ_PREPARE;
	}
      else
	return LWROC_MERGE_IN_STATE_EBYE_XFER_READ_PREPARE;
    default:
      LWROC_BUG_FMT("Unhandled reading of buffer format %x.  "
		    "(Read prepare state.)",
		    _lwroc_merge_info._data_fmt->_buffer_format);
      return -1;
    }
}

/********************************************************************/

void lwroc_merge_in_setup_select(lwroc_select_item *item,
				 lwroc_select_info *si)
{
  lwroc_merge_source *conn =
    PD_LL_ITEM(item, lwroc_merge_source, _out._base._select_item);
  int socket_type;
  struct sockaddr_storage socket_addr;

#if STATE_MACHINE_DEBUG
  printf ("IN %p: [%d] (buf: %" MYPRIzd ")\n", conn, conn->_out._state,
	  lwroc_pipe_buffer_fill(conn->_buffer));
  fflush(stdout);
#endif

 switch_again:
  assert(conn->_out._state != 0);
  switch (conn->_out._state)
    {
    case LWROC_MERGE_IN_STATE_FAILED_FILE:
      break;
    case LWROC_MERGE_IN_STATE_FAILED_SLEEP:
      if (!lwroc_select_info_time_passed_else_setup(si,
						    &conn->_out._next_time))
	break;
      /* Try connection again. */
      conn->_out._state = LWROC_MERGE_IN_STATE_UDP_CREATE;
      lwroc_merge_in_clear(conn);
      FALL_THROUGH;
    case LWROC_MERGE_IN_STATE_UDP_CREATE:
      {
	if (!(conn->_type == LWROC_MERGE_IN_TYPE_FAKERNET))
	  {
	    conn->_out._state = LWROC_MERGE_IN_STATE_DATA_CREATE;
	    goto switch_again;
	  }
      }
      socket_type = LWROC_SOCKET_UDP;
      lwroc_copy_sockaddr_port(&socket_addr, &conn->_out._base._addr,
			       LMD_UDP_PORT_FAKERNET_IDEMPOTENT);
      goto create_socket;

    case LWROC_MERGE_IN_STATE_DATA_CREATE:
      socket_type = LWROC_SOCKET_TCP;
      socket_addr = conn->_out._base._addr;

      if (conn->_type == LWROC_MERGE_IN_TYPE_MBS_TRANS)
	{
	  /* printf ("Connecting choice: %d\n", conn->_port_map); */
	  switch (conn->_port_map)
	    {
	    case LWROC_MERGE_IN_PMAP_PMAP_PORT:
	      /* Add the offset to the port-map port. */
	      lwroc_set_port(&socket_addr,
			     (uint16_t) (lwroc_get_port(&socket_addr) +
					 LMD_TCP_PORT_TRANS_MAP_ADD));
	      break;
	    case LWROC_MERGE_IN_PMAP_DATA_PORT:
	      {
		/* Use data port that we got in info message. */
		uint16_t data_port;

		data_port =
		  (uint16_t) (conn->_data._info.streams &
			      LMD_TCP_INFO_STREAMS_PORT_MAP_PORT_MASK);

		lwroc_set_port(&socket_addr, data_port);
		break;
	      }
	    case LWROC_MERGE_IN_PMAP_LEGACY_PORT:
	      /* Do nothing. */
	      break;
	    }
	}
      goto create_socket;

    create_socket:

      if (!lwroc_net_client_base_create_socket(&conn->_out._base,
					       socket_type))
	{
	  /* Take some time, and then try again. */
	  lwroc_merge_in_failed_setup_retry(conn, &si->now);
	  goto switch_again;
	}
      conn->_out._state++; /* xxx_WAIT_CONNECTED */

      switch (lwroc_net_client_base_connect_socket(&conn->_out._base,
						   &socket_addr))
	{
	case 1:
	  conn->_out._state++; /* state after xxx_WAIT_CONNECTED */

	  lwroc_merge_in_connected(conn);
	  break;
	case 0:
	  break;
	case -1:
	  /* Take some time, and then try again. */
	  lwroc_merge_in_failed_setup_retry(conn, &si->now);
	  break;
	}

      /* What to do depends on how far we got. */
      goto switch_again;

    case LWROC_MERGE_IN_STATE_UDP_RESET_WRITE_PREPARE:
      LWROC_CINFO(&conn->_out._base, "Send Fakernet TCP reset via UPD.");
      assert (conn->_type == LWROC_MERGE_IN_TYPE_FAKERNET);
      {
	uint32_t *udp_packet;

#define FAKERNET_REG_ACCESS_ADDR_WRITE      0x80000000 /* Write request. */
#define FAKERNET_REG_ACCESS_ADDR_WRITTEN    0x40000000 /* Write response. */
#define FAKERNET_REG_ACCESS_ADDR_RESET_TCP  0x08000800 /* 0x1 */

	conn->_out._base._buf._size = 4 * sizeof (uint32_t);
	assert(sizeof (conn->_data._raw) >= conn->_out._base._buf._size);

	udp_packet = conn->_data._raw;

	udp_packet[0] = htonl(0);
	udp_packet[1] = htonl(0);
	udp_packet[2] = htonl(FAKERNET_REG_ACCESS_ADDR_WRITE |
			      FAKERNET_REG_ACCESS_ADDR_RESET_TCP);
	udp_packet[3] = htonl(0);

	conn->_out._base._buf._ptr = (void *) &conn->_data;
      }
      goto common_prepare_write;
      /* */
    common_prepare_write:
      /* Reset the send pointer. */
      conn->_out._base._buf._offset = 0;
      conn->_out._state++; /* xxx_WRITE */
      /* Prepare timeout of 1 s to do the (UDP) write. */
      conn->_out._next_time = si->now;
      conn->_out._next_time.tv_sec += 1;
      FALL_THROUGH;
    case LWROC_MERGE_IN_STATE_UDP_RESET_WRITE:
      LWROC_WRITE_FD_SET(conn->_out._base._fd, si);
      lwroc_select_info_time_setup(si, &conn->_out._next_time);
      break;

    case LWROC_MERGE_IN_STATE_UDP_RESET_READ_PREPARE:
      conn->_out._base._buf._size = 4 * sizeof (uint32_t);
      assert(sizeof (conn->_data._raw) >= conn->_out._base._buf._size);
      conn->_out._base._buf._ptr = (char *) conn->_data._raw;
      conn->_out._base._buf._offset = 0;
      conn->_out._state++; /* xxx_READ */
      /* Prepare timeout of 1 s to do the UDP read. */
      conn->_out._next_time = si->now;
      conn->_out._next_time.tv_sec += 1;
      FALL_THROUGH;
    case LWROC_MERGE_IN_STATE_UDP_RESET_READ:
      LWROC_READ_FD_SET(conn->_out._base._fd, si);
      lwroc_select_info_time_setup(si, &conn->_out._next_time);
      break;

    case LWROC_MERGE_IN_STATE_DATA_SEND_ORDERS_PREPARE:
      if (conn->_type == LWROC_MERGE_IN_TYPE_MBS_TRANS ||
	  conn->_type == LWROC_MERGE_IN_TYPE_FAKERNET)
	{
	  conn->_out._state = LWROC_MERGE_IN_STATE_DATA_INFO_READ_PREPARE;
	  goto switch_again;
	}
      if (/* conn->_type == LWROC_MERGE_IN_TYPE_EBYE_PUSH || */
	  conn->_type == LWROC_MERGE_IN_TYPE_EBYE_PULL)
	{
	  conn->_out._state = LWROC_MERGE_IN_STATE_EBYE_XFER_READ_PREPARE;
	  goto switch_again;
	}
      LWROC_BUG("This should not be reached.");  break;
#if 0 /* This might be useful for reading from a stream server. */
      printf("drasi send order...\n");
      assert (conn->_type == LWROC_MERGE_IN_TYPE_DRASI);
      /* We are to send a connection request. */
      {
	lwroc_request_msg req_msg;

	req_msg._request = LWROC_REQUEST_DATA_TRANS;

	conn->_out._base._buf._size = lwroc_request_msg_serialized_size();
	assert(sizeof (conn->_data) >= conn->_out._base._buf._size);
	lwroc_request_msg_serialize((char *) &conn->_data, &req_msg);

	conn->_out._base._buf._offset = 0;
	conn->_out._base._buf._ptr = (void *) &conn->_data;
      }
      conn->_out._state++; /* xxx_SEND */
      FALL_THROUGH;
    case LWROC_MERGE_IN_STATE_DATA_SEND_ORDERS:
      LWROC_WRITE_FD_SET(conn->_out._base._fd, si);
      break;
#endif
    case LWROC_MERGE_IN_STATE_DATA_INFO_READ_PREPARE:
      /* trans protocol */
      conn->_out._base._buf._size = sizeof (conn->_data._info);
      conn->_out._base._buf._ptr = (char *) &conn->_data._info;
      goto common_prepare_read;
    case LWROC_MERGE_IN_STATE_DATA_SETUP_READ_PREPARE:
      /* drasi protocol */
      conn->_out._base._buf._size =
	lwroc_data_transport_setup_serialized_size();
      assert(sizeof (conn->_data._raw) >= conn->_out._base._buf._size);
      conn->_out._base._buf._ptr = (char *) conn->_data._raw;
      /* printf("want: %zd\n",conn->_out._base._buf._size); */
      goto common_prepare_read;
    case LWROC_MERGE_IN_STATE_DATA_FILHE_READ_PREPARE:
    case LWROC_MERGE_IN_STATE_DATA_BUFHE_READ_PREPARE:
      conn->_out._base._buf._size = sizeof (conn->_data._bufhe);
      conn->_out._base._buf._ptr = (char *) &conn->_data._bufhe;
      goto common_prepare_read;
    case LWROC_MERGE_IN_STATE_EBYE_RECORD_READ_PREPARE:
      conn->_out._base._buf._size = sizeof (conn->_data._ebye_record);
      conn->_out._base._buf._ptr = (char *) &conn->_data._ebye_record;
      goto common_prepare_read;
    case LWROC_MERGE_IN_STATE_EBYE_XFER_READ_PREPARE:
      conn->_out._base._buf._size = sizeof (conn->_data._ebye_xfer);
      conn->_out._base._buf._ptr = (char *) &conn->_data._ebye_xfer;
      goto common_prepare_read;
    case LWROC_MERGE_IN_STATE_DATA_FEVHE_READ_PREPARE:
      /* FEVHE: fragmented event header. */
      conn->_out._base._buf._size = sizeof (conn->_fevhe);
      conn->_out._base._buf._ptr = (char *) &conn->_fevhe;
      goto common_prepare_read;
    case LWROC_MERGE_IN_STATE_DATA_FRAG_READ_PREPARE:
      /* We just continue to read into the buffer. */
      goto common_prepare_read;
    case LWROC_MERGE_IN_STATE_DATA_CHUNK_READ_PREPARE:
    case LWROC_MERGE_IN_STATE_EBYE_CHUNK_READ_PREPARE:
    {
      size_t reserve_size;

      /* We are to read remainder of payload (including padding). */
      conn->_out._base._buf._size =
	conn->_used_left + conn->_padding_left;

      if (_lwroc_merge_info._data_fmt->_buffer_format ==
	  LWROC_DATA_TRANSPORT_FORMAT_XFER)
	{
	  /* Header that was read will be copied to be before data. */
	  conn->_out._base._buf._size += sizeof (conn->_data._ebye_xfer);
	}

      /* We need to allocate for remainder of payload, and fragment. */
      reserve_size = conn->_out._base._buf._size;

      if (conn->_out._state == LWROC_MERGE_IN_STATE_DATA_CHUNK_READ_PREPARE)
	{
	  if (conn->_data._bufhe.h_begin == 1)
	    {
	      size_t margin_size;

	      /* We are to read a fragmented event. */
	      conn->_expect_fragment =
		EVENT_DATA_LENGTH_FROM_DLEN(conn->_data._bufhe.l_free[1]);
	      conn->_fragment_length =
		sizeof (lmd_event_header_host) + conn->_expect_fragment;
	      /*
		printf ("expect frag: %" MYPRIzd " (len: %" MYPRIzd ")\n",
			conn->_expect_fragment, conn->_fragment_length);
	      */
	      margin_size = conn->_used_left + conn->_expect_fragment;

	      if (margin_size > reserve_size)
		reserve_size = margin_size;
	    }
	  else
	    conn->_expect_fragment = 0;
	}

      conn->_chunk_start =
	lwroc_pipe_buffer_alloc_write(conn->_buffer,
				      (uint32_t) reserve_size, NULL,
				      _lwroc_merge_in_thread->_block, 0);

      if (!conn->_chunk_start)
	{
	  /* No space available at the moment.
	   * We will be notified.
	   */
	  if (_lwroc_merge_info._delayed_eb_enabled &&
	      _lwroc_merge_info._notify_force_need)
	    {
	      /* Notify validation thread that is should check.  It
	       * will find us waiting, and send a request to do forced
	       * event building.
	       */
	      /* printf ("Notify buf blocked\n"); */
	      LWROC_THREAD_BLOCK_NOTIFY(&_lwroc_merge_info._notify_force_need);
	    }

	  break;
	}
      conn->_out._base._buf._ptr = conn->_chunk_start;
      conn->_chunk_end = conn->_chunk_start + conn->_out._base._buf._size;

      conn->_chunk_reserve_end = conn->_chunk_start + reserve_size;

      if (_lwroc_merge_info._data_fmt->_buffer_format ==
	  LWROC_DATA_TRANSPORT_FORMAT_XFER)
	{
	  /* Copy the header.  Then patch up the size info. */
	  ebye_xfer_header *cd_xfer = &conn->_data._ebye_xfer;
	  ebye_xfer_header *buf_xfer =
	    (ebye_xfer_header *) conn->_out._base._buf._ptr;

	  memcpy(buf_xfer, cd_xfer, sizeof (ebye_xfer_header));

	  /* Restore network order. */
	  lwroc_ebye_xfer_ntoh(buf_xfer);

	  /* Just act as if we already did read the header. */
	  conn->_out._base._buf._offset = sizeof (ebye_xfer_header);
	  goto common_prepare_read_offset_set;
	}
    }
      goto common_prepare_read;
      /* */
    common_prepare_read:
      /* Reset the receive pointer. */
      conn->_out._base._buf._offset = 0;
    common_prepare_read_offset_set:
      conn->_out._state++; /* xxx_READ */
      if (conn->_out._base._buf._offset == conn->_out._base._buf._size)
	{
	  /* Nothing to read.  Can happen for keep-alive data. */
	  conn->_out._state++; /* xxx_READ_CHECK */
	  /* Make sure it gets handled immediately, set timeout to 0 */
	  lwroc_select_info_set_timeout_now(si);
	  break;
	}
      /* Setup keepalive timeout.
       * (Needed for first info data, to not trigger for that,
       * since all receptions are checked.)
       */
      conn->_out._next_time = si->now;
      conn->_out._next_time.tv_sec += conn->_keepalive_timeout;
      FALL_THROUGH;
    case LWROC_MERGE_IN_STATE_DATA_INFO_READ:
    case LWROC_MERGE_IN_STATE_DATA_SETUP_READ:
    case LWROC_MERGE_IN_STATE_DATA_FILHE_READ:
    case LWROC_MERGE_IN_STATE_DATA_BUFHE_READ:
    case LWROC_MERGE_IN_STATE_DATA_FEVHE_READ:
    case LWROC_MERGE_IN_STATE_DATA_FRAG_READ:
    case LWROC_MERGE_IN_STATE_DATA_CHUNK_READ:
    case LWROC_MERGE_IN_STATE_EBYE_RECORD_READ:
    case LWROC_MERGE_IN_STATE_EBYE_XFER_READ:
    case LWROC_MERGE_IN_STATE_EBYE_CHUNK_READ:
      LWROC_READ_FD_SET(conn->_out._base._fd, si);
      /* Setup a timeout for getting any data (keep-alive). */
      if (conn->_keepalive_timeout)
	lwroc_select_info_time_setup(si, &conn->_out._next_time);
      break;

    case LWROC_MERGE_IN_STATE_DATA_WAIT_CONNECTED:
      LWROC_WRITE_FD_SET(conn->_out._base._fd, si);
      break;

    case LWROC_MERGE_IN_STATE_FAILED_INJECT_ENDMARK:
    {
      /* Mark the source as ended.  The consumer end will check this
       * flag whenever it reaches the data end, i.e. when it can get
       * no more data.
       */
      conn->_status_out_of_data = 1;
      SFENCE; /* Make sure it is written before tickle below. */

      printf ("%p: Marked out-of-data.\n", conn);

      lwroc_merge_in_check_fill(conn);
      lwroc_merge_input_tickle_disabled(conn);
      conn->_out._state++;
      break;
    }

    case LWROC_MERGE_IN_STATE_SHUTDOWN:
      /* Make sure it gets handled immediately, set timeout to 0 */
      lwroc_select_info_set_timeout_now(si);
      break;
    }

#if STATE_MACHINE_DEBUG
  printf ("IN %p:  -> %d (%" MYPRIzd "/%" MYPRIzd ")\n",
	  conn, conn->_out._state,
	  conn->_out._base._buf._offset,
	  conn->_out._base._buf._size);
  fflush(stdout);
#endif
}

/********************************************************************/

int lwroc_merge_in_format_compatible(uint32_t src_fmt,
				     uint32_t dest_fmt)
{
  if (src_fmt == dest_fmt)
    return 1;
  if (src_fmt == LWROC_DATA_TRANSPORT_FORMAT_EBYE &&
      dest_fmt == LWROC_DATA_TRANSPORT_FORMAT_XFER)
    return 1;
  if (src_fmt == LWROC_DATA_TRANSPORT_FORMAT_XFER &&
      dest_fmt == LWROC_DATA_TRANSPORT_FORMAT_EBYE)
    return 1;
  return 0;
}

/********************************************************************/

/* Prerequisite: file descriptor ok for read by select, or we'll block
 * will only issue one read call, return -1 => failure, disconnect.
 */
ssize_t lwroc_merge_in_read_buffer(lwroc_merge_source *conn,int fd)
{
  size_t max_recv =
    conn->_out._base._buf._size - conn->_out._base._buf._offset;

  ssize_t n =
    read(fd,conn->_out._base._buf._ptr + conn->_out._base._buf._offset,
	 max_recv);
  /*
  printf ("R: %" MYPRIzd " %" MYPRIzd " %" MYPRIzd " %" MYPRIzd " (%p -- %p)\n",
	  max_recv, n, conn->_out._base._buf._size, conn->_out._base._buf._offset,
	  conn->_out._base._buf._ptr + conn->_out._base._buf._offset,
	  conn->_out._base._buf._ptr + conn->_out._base._buf._offset + max_recv);
  */
  if (n == 0)
    {
      /* Socket closed on other end, this we treat as an error. */
      return -1;
    }

  if (n < 0)
    {
      if (errno == EINTR ||
	  errno == EAGAIN)
	return 0; /* try again next time */

      LWROC_CPERROR(&conn->_out._base,"read");
      LWROC_CWARNING(&conn->_out._base,"Error while reading data.");
      /* Treat it as some general failure, disconnect if that's an
       * option.
       */
      return -1;
    }

  _lwroc_mon_in._bytes += (uint64_t) n;
  conn->_out._base._mon->_block._bytes += (uint64_t) n;

  conn->_out._base._buf._offset += (size_t) n;

  return n;
}

/********************************************************************/

ssize_t lwroc_merge_in_write_buffer(lwroc_merge_source *conn,int fd)
{
  size_t max_recv =
    conn->_out._base._buf._size - conn->_out._base._buf._offset;

  ssize_t n =
    write(fd,conn->_out._base._buf._ptr + conn->_out._base._buf._offset,
	  max_recv);
  /*
  printf ("W: %" MYPRIzd " %" MYPRIzd " %" MYPRIzd " %" MYPRIzd " (%p -- %p)\n",
	  max_recv, n, conn->_out._base._buf._size, conn->_out._base._buf._offset,
	  conn->_out._base._buf._ptr + conn->_out._base._buf._offset,
	  conn->_out._base._buf._ptr + conn->_out._base._buf._offset + max_recv);
  */
  if (n == 0)
    {
      /* Socket closed on other end, this we treat as an error. */
      return -1;
    }

  if (n < 0)
    {
      if (errno == EINTR ||
	  errno == EAGAIN)
	return 0; /* try again next time */

      LWROC_CPERROR(&conn->_out._base,"write");
      LWROC_CWARNING(&conn->_out._base,"Error while writing data.");
      /* Treat it as some general failure, disconnect if that's an
       * option.
       */
      return -1;
    }

  conn->_out._base._buf._offset += (size_t) n;

  return n;
}

/********************************************************************/

void lwroc_merge_input_got_complete_event(lwroc_merge_source *conn,
					  lwroc_select_info *si)
{
  /* Update last time we got data from source. */

  conn->_last_got_data = si->now;

  /* If we have gotten the first complete event, tell the
   * merger thread.  This may only be done once!
   */

  if (!conn->_had_first_complete_event)
    {
      conn->_had_first_complete_event = 1;
      conn->_status_out_of_data = 0;
      SFENCE; /* Make sure it is written before other end might take it.
	       * That may be before the SFENCE in LWROC_ITEM_BUFFER_INSERT.
	       */
      /* Move the connection into the merger's active lists. */
      if ((_lwroc_merge_info._mode & LWROC_MERGE_MODE_EB) &&
	  !_config._merge_no_validate)
	{
	  /* printf("Giving input to validator.\n"); */
	  LWROC_ITEM_BUFFER_INSERT(_lwroc_merge_info._fresh_val_sources, conn);
	}
      else
	{
	  /* printf("Giving input to sorter.\n"); */
	  LWROC_ITEM_BUFFER_INSERT(_lwroc_merge_info._fresh_sort_sources, conn);
	}
      _lwroc_mon_in._active++;
    }

  lwroc_merge_input_tickle_disabled(conn);
}

/********************************************************************/

int lwroc_merge_in_check_buffer(lwroc_merge_source *conn,
				lwroc_select_info *si)
{
  char *buffer = conn->_chunk_start;
  size_t used = conn->_used_left;

  /* TODO: checksum of buffer. */
  /* TODO: checksum of events. */

  /* We check that the events inside the buffer has lengths that
   * add up to the used space.  We are also responsible for finding
   * the last event, in case it is the beginning of a fragmented
   * event.
   */

  /* Any fragmented event at the beginning of the buffer has already
   * been consumed.
   */

  uint32_t events_left = conn->_data._bufhe.l_evt;
  uint32_t complete_events;
  lmd_event_header_host *evhe;
  char *end_complete;
  ssize_t complete;

  if (conn->_data._bufhe.h_end == 1)
    events_left--;

  complete_events = events_left;

  evhe = NULL;

  for ( ; events_left; events_left--)
    {
      size_t event_data_size;
      size_t event_full_size;

      if (sizeof (lmd_event_header_host) > used)
	{
	  LWROC_CERROR(&conn->_out._base,
		       "No space for event header in buffer.");
	  return 0;
	}
      used -= sizeof (lmd_event_header_host);

      evhe = (lmd_event_header_host *) buffer;

      event_data_size =
	EVENT_DATA_LENGTH_FROM_DLEN(evhe->l_dlen);

      if (event_data_size > used)
	{
	  LWROC_CERROR_FMT(&conn->_out._base,
			   "No space for event data (%" MYPRIzd ") "
			   "in buffer (%" MYPRIzd " left).",
			   event_data_size, used);
	  return 0;
	}

      event_full_size = event_data_size + sizeof (lmd_event_header_host);

      if (event_full_size > conn->_max_ev_len)
	{
	  LWROC_CERROR_FMT(&conn->_out._base,
			   "Event longer than promised (%" MYPRIzd " > "
			   "%" MYPRIzd ") by %s.",
			   event_full_size,
			   conn->_max_ev_len,
			   conn->_max_ev_len_source);
	  return 0;
	}

      used -= event_data_size;
      buffer += sizeof (lmd_event_header_host) + event_data_size;
    }

  if (used)
    {
      LWROC_CERROR_FMT(&conn->_out._base,
		       "Buffer payload space not fully used "
		       "(%" MYPRIzd " left).",
		       used);
      /* This return might be too evil?  Warning above enough? */
      return 0;
    }

  /* Fragmented event at end? */

  end_complete = buffer;

  if (conn->_data._bufhe.h_begin == 1)
    {
      size_t this_frag_size;

      if (!evhe)
	{
	  printf ("event start @%p\n", buffer);
	  LWROC_CBUG(&conn->_out._base,
		     "Fragment begin with no event header.");
	}

      /* The fragmented event is not to be sent to the sorter. */

      end_complete = (char *) evhe;

      /* We have gotten a piece of the fragment. */

      this_frag_size = EVENT_DATA_LENGTH_FROM_DLEN(evhe->l_dlen);
      conn->_expect_fragment -= this_frag_size;
      /*
      printf ("first frag [-%" MYPRIzd "]: %" MYPRIzd " (len: %" MYPRIzd ")\n",
	      this_frag_size,
	      conn->_expect_fragment, conn->_fragment_length);
      */
      /* Rewrite the length of the event header, such that when it is finally
       * send, it is correct.
       */

      evhe->l_dlen = conn->_data._bufhe.l_free[1];
    }

  complete = end_complete - conn->_chunk_start;

  if (complete)
    {
      /* There was some event besides the fragmented one. */

      lwroc_pipe_buffer_did_write(conn->_buffer, (size_t) complete, 0);
      lwroc_merge_in_check_fill(conn);

      lwroc_merge_input_got_complete_event(conn, si);
      conn->_out._base._mon->_block._events += complete_events;
    }

  /* In case we are to continue reading a fragmented event. */
  conn->_chunk_start = buffer;

  return 1;
}

/********************************************************************/

int lwroc_merge_in_check_ebye_record(lwroc_merge_source *conn,
				     lwroc_select_info *si)
{
  /* char *buffer = conn->_chunk_start; */
  size_t used = conn->_used_left;

  /* For the time being, we just check that there is an even number of
   * 32-bit words.
   */

  if (used % (2 * sizeof (uint32_t)) != 0)
    {
      LWROC_CERROR_FMT(&conn->_out._base,
		       "EBYE record (%" MYPRIzd ") not a multiple "
		       "of 2 32-bit words(%" MYPRIzd ").",
		       used, 2 * sizeof (uint32_t));
      return 0;
    }

  /* Got some data? */

  if (used)
    {
      size_t hits = used / (2 * sizeof (uint32_t));

      lwroc_pipe_buffer_did_write(conn->_buffer, used, 0);
      lwroc_merge_in_check_fill(conn);

      lwroc_merge_input_got_complete_event(conn, si);
      conn->_out._base._mon->_block._events += hits; /* Count as events. */
    }

  return 1;
}

/* Used when we handle complete XFER blocks (without checking contents). */
int lwroc_merge_in_check_xfer_block(lwroc_merge_source *conn,
				    lwroc_select_info *si)
{
  ebye_xfer_header *cd_xfer = &conn->_data._ebye_xfer;

  size_t used = conn->_out._base._buf._size;

  if (((int32_t) cd_xfer->_data_length) == -1)
    {
      /* Restart buffer.  Shall not be passed along. */
      used = 0;
    }

  /* Got some data? */

  if (used)
    {
      /*
      printf ("EBYE wrote to input buffer: %" MYPRIzd "\n", used);
      fflush(stdout);
      */

      lwroc_pipe_buffer_did_write(conn->_buffer, used, 0);
      lwroc_merge_in_check_fill(conn);

      lwroc_merge_input_got_complete_event(conn, si);
      conn->_out._base._mon->_block._events += 1; /* Count as events. */
    }

  return 1;
}

/********************************************************************/

void lwroc_byteswap32(void *ptr, ssize_t len)
{
  uint32_t* p = (uint32_t *) ptr;
  ssize_t i;

  for (i = len / (ssize_t) sizeof(uint32_t); i; i--)
    {
      *p = bswap_32(*p);
      p++;
    }
}

/********************************************************************/

int lwroc_merge_check_source_ev_size(lwroc_merge_source *conn)
{
  if (_lwroc_merge_info._mode & LWROC_MERGE_MODE_TS)
    {
      uint32_t max_out_ev_len;

      max_out_ev_len =
	lwroc_data_pipe_get_max_ev_len(_lwroc_merge_info._data_handle);

      /* Event size may not be larger than output event size. */

      if (conn->_max_ev_len > max_out_ev_len)
	{
	  LWROC_CERROR_FMT(&conn->_out._base,
			   "Server provides larger events "
			   "(%" MYPRIzd ", by %s), "
			   "than output accepts (%" PRIu32 ").",
			   conn->_max_ev_len,
			   conn->_max_ev_len_source,
			   max_out_ev_len);
	  return 0;
	}
    }

  return 1;
}

/********************************************************************/

int lwroc_merge_in_after_select(lwroc_select_item *item,
				lwroc_select_info *si)
{
  lwroc_merge_source *conn =
    PD_LL_ITEM(item, lwroc_merge_source, _out._base._select_item);

#if STATE_MACHINE_DEBUG
  printf ("IN %p: {%d}\n", conn, conn->_out._state);
  fflush(stdout);
#endif

  /* TODO: this should really be after all the processing, if we keep
   * track of amount of data sent/received.
   */
  LWROC_MON_CHECK_COPY_CONN_MON_BLOCK(conn->_out._base._mon, 0);

 switch_again:
#if STATE_MACHINE_DEBUG
  printf ("IN %p: {%d} . /%" MYPRIzd "/\n", conn, conn->_out._state,
	  conn->_expect_fragment);
  fflush(stdout);
#endif
  assert(conn->_out._state != 0);
  switch (conn->_out._state)
    {
#if 0 /* This would be useful for reading from a stream server. */
    case LWROC_MERGE_IN_STATE_DATA_SEND_ORDERS:
#endif
    case LWROC_MERGE_IN_STATE_UDP_RESET_WRITE:
    {
      ssize_t n;

      if (!LWROC_WRITE_FD_ISSET(conn->_out._base._fd,si))
	break;

      n = lwroc_merge_in_write_buffer(conn,conn->_out._base._fd);

      if (n < 0)
	return 0; /* failure */

      if (conn->_out._base._buf._offset < conn->_out._base._buf._size)
	{
	  /* we've not reached end of buffer yet */
	  break;
	}
      conn->_out._state++;
      goto switch_again;
    }
    case LWROC_MERGE_IN_STATE_UDP_RESET_READ:
    case LWROC_MERGE_IN_STATE_DATA_INFO_READ:
    case LWROC_MERGE_IN_STATE_DATA_SETUP_READ:
    case LWROC_MERGE_IN_STATE_DATA_FILHE_READ:
    case LWROC_MERGE_IN_STATE_DATA_BUFHE_READ:
    case LWROC_MERGE_IN_STATE_DATA_FEVHE_READ:
    case LWROC_MERGE_IN_STATE_DATA_FRAG_READ:
    case LWROC_MERGE_IN_STATE_DATA_CHUNK_READ:
    case LWROC_MERGE_IN_STATE_EBYE_RECORD_READ:
    case LWROC_MERGE_IN_STATE_EBYE_XFER_READ:
    case LWROC_MERGE_IN_STATE_EBYE_CHUNK_READ:
    {
      ssize_t n;

      if (!LWROC_READ_FD_ISSET(conn->_out._base._fd,si))
	{
	  if (conn->_keepalive_timeout &&
	      timercmp(&si->now, &conn->_out._next_time, >))
	    {
	      if (conn->_out._state == LWROC_MERGE_IN_STATE_UDP_RESET_READ)
		LWROC_ERROR_FMT("No reset response received "
				"for %d s - re-connecting.",
				1);
	      else
		LWROC_ERROR_FMT("No data (even keep-alive) received "
				"for %d s - re-connecting.",
				conn->_keepalive_timeout);
	      return 0;
	    }
	  break;
	}

      n = lwroc_merge_in_read_buffer(conn,conn->_out._base._fd);

      /* printf ("q... %" MYPRIzd "\n", n); */

      if (n < 0)
	return 0; /* failure */

      /* We got at least one byte of data, postpone keepalive timeout. */
      conn->_out._next_time = si->now;
      conn->_out._next_time.tv_sec += conn->_keepalive_timeout;

      if (conn->_out._base._buf._offset < conn->_out._base._buf._size)
	{
	  /* we've not reached end of buffer yet */
	  break;
	}

      /* printf ("R: got %" MYPRIzd "\n", conn->_out._base._buf._offset); */

      conn->_out._state++;

      goto switch_again;
    }
    case LWROC_MERGE_IN_STATE_UDP_RESET_READ_CHECK:
    {
      uint32_t expect_2 =
	FAKERNET_REG_ACCESS_ADDR_WRITTEN |
	FAKERNET_REG_ACCESS_ADDR_RESET_TCP;

      /* Whatever we do, we are done with the UDP socket. */
      lwroc_safe_close(conn->_out._base._fd);
      conn->_out._base._fd = -1;
      /*
      printf ("R: got %" PRIx32 " %" PRIx32 " %" PRIx32 " %" PRIx32 "\n",
	      conn->_data._raw[0],
	      conn->_data._raw[1],
	      conn->_data._raw[2],
	      conn->_data._raw[3]);
      */
      /* If we fail, then */
      if (ntohl(conn->_data._raw[2]) != expect_2)
	{
	  LWROC_ERROR_FMT("Bad Fakernet TCP reset via UDP "
			  "(%08" PRIx32" != expect %08" PRIx32").",
			  ntohl(conn->_data._raw[2]), expect_2);
	  return 0;
	}

      conn->_out._state = LWROC_MERGE_IN_STATE_DATA_CREATE;
      break;
    }
    case LWROC_MERGE_IN_STATE_DATA_INFO_READ_CHECK:
      {
	size_t max_combined_bufsize;

	if (conn->_data._info.testbit != 0x00000001 &&
	    conn->_data._info.testbit != 0x01000000)
	  {
	    LWROC_CERROR_FMT(&conn->_out._base,
			     "Info swapping marker bad (%08x).",
			     conn->_data._info.testbit);
	    return 0;
	  }

	if (conn->_data._info.testbit == 0x01000000)
	  lwroc_byteswap32(&conn->_data._info,
			   (ssize_t) conn->_out._base._buf._size);

	/* Estimate maximum event size as buffer size, times number
	 * of buffers per stream.
	 */

	max_combined_bufsize =
	  (size_t) conn->_data._info.bufsize *
	  (size_t) conn->_data._info.bufs_per_stream;

	LWROC_CLOG_FMT(&conn->_out._base,
		       "Info: test %d bufsize %d numbuf %d streams %d "
		       "(test 0x%x bufsize 0x%x numbuf 0x%x streams 0x%x)",
		       conn->_data._info.testbit,
		       conn->_data._info.bufsize,
		       conn->_data._info.bufs_per_stream,
		       conn->_data._info.streams,
		       conn->_data._info.testbit,
		       conn->_data._info.bufsize,
		       conn->_data._info.bufs_per_stream,
		       conn->_data._info.streams);

	if (conn->_port_map == LWROC_MERGE_IN_PMAP_PMAP_PORT &&
	    (conn->_data._info.streams &
	     LMD_TCP_INFO_STREAMS_PORT_MAP_MARK_MASK) ==
	    LMD_TCP_INFO_STREAMS_PORT_MAP_MARK)
	  {
	    uint16_t data_port =
	      conn->_data._info.streams &
	      LMD_TCP_INFO_STREAMS_PORT_MAP_PORT_MASK;

	    LWROC_CINFO_FMT(&conn->_out._base,
			    "Redirected -> port %d",
			    data_port);

	    /* We are done with the UDP socket. */
	    lwroc_safe_close(conn->_out._base._fd);
	    conn->_out._base._fd = -1;
	    /* Try again, with data port. */
	    conn->_port_map = LWROC_MERGE_IN_PMAP_DATA_PORT;
	    conn->_out._state = LWROC_MERGE_IN_STATE_DATA_CREATE;
	    break;
	  }

	if (conn->_data._info.bufsize ==
	    (uint32_t) LMD_TCP_INFO_BUFSIZE_NODATA)
	  {
	    /* This should not happen to us!, since we know how to
	     * interpret the map.
	     */
	    LWROC_CERROR(&conn->_out._base,
			 "Buffer size -1, "
			 "hint that only port mapped clients allowed.");
	    return 0;
	  }

	if (conn->_data._info.bufsize ==
	    (uint32_t) LMD_TCP_INFO_BUFSIZE_MAXCLIENTS)
	  {
	    if ((conn->_data._info.streams &
		 LMD_TCP_INFO_STREAMS_NODATA_HOLDOFF_MARK_MASK) ==
		LMD_TCP_INFO_STREAMS_NODATA_HOLDOFF_MARK)
	      {
		int rate =
		  (conn->_data._info.streams &
		   LMD_TCP_INFO_STREAMS_NODATA_HOLDOFF_RATE_MASK);

		LWROC_CINFO_FMT(&conn->_out._base,
				"Failing connection rate at server: %d/s.",
				rate);
		if (rate > 5)
		  {
		    LWROC_CWARNING(&conn->_out._base,
				   "We are you hammering?  "
				   "With someone else?");
		    /* With someone else, since we should have a sleep
		     * after the retry...
		     */
		  }
	      }

	    LWROC_CERROR(&conn->_out._base,
			 "Buffer size -2, hint that maximum "
			 "number of clients are already connected.");
	    return 0;
	  }

	/* For buffering to have any kind of efficiency, we need to
	 * be able to handle at least two such buffers.
	 */

	if (max_combined_bufsize >
	    conn->_buffer_size / 2)
	  {
	    LWROC_CERROR_FMT(&conn->_out._base,
			     "Server provides too large "
			     "bufsize (%d) * numbuf (%d) = (%" MYPRIzd ") > "
			     "input buffer = %" MYPRIzd "/2 = %" MYPRIzd ".  "
			     "(Increase bufsize?)",
			     conn->_data._info.bufsize,
			     conn->_data._info.bufs_per_stream,
			     max_combined_bufsize,
			     conn->_buffer_size,
			     conn->_buffer_size / 2);
	    return 0;
	  }

	conn->_max_buffer_size = conn->_data._info.bufsize;

	if (!conn->_max_ev_len_config ||
	    max_combined_bufsize < conn->_max_ev_len_config)
	  {
	    conn->_max_ev_len = max_combined_bufsize;
	    conn->_max_ev_len_source = "info message";
	  }
	else
	  {
	    conn->_max_ev_len = conn->_max_ev_len_config;
	    conn->_max_ev_len_source = "config";
	  }

	if (!lwroc_merge_check_source_ev_size(conn))
	  {
	    /* Since the info message is only sent to log
	     * above, we amend the error message a bit here.
	     */
	    LWROC_CERROR_FMT(&conn->_out._base,
			     "From info message: "
			     "bufsize (%d) * numbuf (%d) = (%" MYPRIzd ").",
			     conn->_data._info.bufsize,
			     conn->_data._info.bufs_per_stream,
			     max_combined_bufsize);
	    return 0;
	  }

	LWROC_CINFO_FMT(&conn->_out._base,
			"%s, protobuf=%" MYPRIzd ", "
			"max-ev-size=%" MYPRIzd " (%s)",
			conn->_type == LWROC_MERGE_IN_TYPE_FAKERNET ?
			"Fakernet" : "Transport",
			conn->_max_buffer_size,
			conn->_max_ev_len,
			conn->_max_ev_len_source);

	conn->_out._state = LWROC_MERGE_IN_STATE_DATA_BUFHE_READ_PREPARE;
	break;
      }
    case LWROC_MERGE_IN_STATE_DATA_SETUP_READ_CHECK:
      {
	lwroc_data_transport_setup setup_info;
	lwroc_deserialize_error desererr;
	const char *end;
	int allow_event_interval;
	const char *allow_str = "";

	end =
	  lwroc_data_transport_setup_deserialize(&setup_info,
						 conn->_out._base._buf._ptr,
						 conn->_out._base._buf._size,
						 &desererr);

	if (end == NULL)
	  {
	    LWROC_ERROR_FMT("Data transport setup malformed (%s, 0x%x, 0x%x).",
			    desererr._msg, desererr._val1, desererr._val2);
	    return 0;
	  }

	if (!lwroc_merge_in_format_compatible(setup_info._data_format,
					      _lwroc_merge_info._data_fmt->
					      /**/_buffer_format))
	  {
	    char hollerith_got[5];
	    char hollerith_expect[5];

	    lwroc_uint32_to_hollerith(hollerith_got, setup_info._data_format);
	    lwroc_uint32_to_hollerith(hollerith_expect,
				      _lwroc_merge_info._data_fmt->
				      /**/_buffer_format);

	    LWROC_CERROR_FMT(&conn->_out._base,
			     "Server provides wrong data format, "
			     "%s != expected %s.",
			     hollerith_got,
			     hollerith_expect);
	    return 0;
	  }

	conn->_max_buffer_size = setup_info._max_buf_size;

	if (conn->_max_buffer_size >
	    conn->_buffer_size / 2)
	  {
	    LWROC_CERROR_FMT(&conn->_out._base,
			     "Server provides too large "
			     "buffer (%" MYPRIzd ") > "
			     "input buffer = %" MYPRIzd "/2 = %" MYPRIzd".  "
			     "(Increase bufsize?)",
			     conn->_max_buffer_size,
			     conn->_buffer_size,
			     conn->_buffer_size / 2);
	    return 0;
	  }

	conn->_max_ev_len = setup_info._max_ev_len;
	conn->_max_ev_len_source = "*dummy*";

	if (conn->_max_ev_len_config &&
	    conn->_max_ev_len > conn->_max_ev_len_config)
	  {
	    LWROC_CERROR_FMT(&conn->_out._base,
			     "Server can provide larger events than "
			     "configured for source (%" MYPRIzd " > "
			     "%" MYPRIzd ").",
			     conn->_max_ev_len,
			     conn->_max_ev_len_config);
	    return 0;
	  }

	/* As server only can reduce the size, we are always given
	 * by the server start message (unless equal).
	 */
	if (conn->_max_ev_len == conn->_max_ev_len_config)
	  conn->_max_ev_len_source = "config";
	else
	  conn->_max_ev_len_source = "setup message";

	if (!lwroc_merge_check_source_ev_size(conn))
	  return 0;

	/* If we have made a promise about maximum interval between
	 * events, then any source must have made a stricter promise.
	 *
	 * If we have a timesort disable timeout for a source, then it
	 * must also have made a promise to us.
	 */

	/* Might be 0 (no requirement). */
	allow_event_interval = _config._max_event_interval;
	allow_str = "max event interval (--max-ev-interval)";

	if (conn->_ts_disable_timeout &&
	    (!allow_event_interval ||
	     conn->_ts_disable_timeout < allow_event_interval))
	  {
	    allow_event_interval = conn->_ts_disable_timeout;
	    allow_str = "input timesort disable (ts-disable) timeout";
	  }

	/* If source has a disable timeout, it must be larger than the
	 * largest (expected) time between events of the source.
	 */
	if (allow_event_interval &&
	    (setup_info._max_event_interval == (uint32_t) -1 ||
	     setup_info._max_event_interval +
	     LWROC_MERGE_SOURCE_DISABLE_INPUT_MARGIN >
	     (uint32_t) allow_event_interval))
	  {
	    /* Convert to int, such that -1 (infinity) is seen. */
	    LWROC_CERROR_FMT(&conn->_out._base,
			     "Source max event interval "
			     "(time between events) "
			     "(%d s + margin %d s), larger than "
			     "than %s (%d s).  "
			     "Option --max-ev-interval in source system "
			     "missing or too small.",
			     (int) setup_info._max_event_interval,
			     LWROC_MERGE_SOURCE_DISABLE_INPUT_MARGIN,
			     allow_str,
			     (int) allow_event_interval);
	    return 0;
	  }

	if ((_lwroc_merge_info._mode & LWROC_MERGE_MODE_TS) &&
	    setup_info._max_event_interval == (uint32_t) -1)
	  LWROC_CWARNING(&conn->_out._base,
			 "Source system does not specify max event interval "
			 "(--max-ev-interval).  "
			 "This is suggested when delivering data to "
			 "a time sorter.");

	LWROC_CINFO_FMT(&conn->_out._base,
			"drasi-proto, protobuf=%" MYPRIzd ", "
			"max-ev-size=%" MYPRIzd " (%s)",
			conn->_max_buffer_size,
			conn->_max_ev_len,
			conn->_max_ev_len_source);

	conn->_out._state = lwroc_merge_in_buffer_read_prepare(0);
	break;
      }
    case LWROC_MERGE_IN_STATE_DATA_FILHE_READ_CHECK:
    case LWROC_MERGE_IN_STATE_DATA_BUFHE_READ_CHECK:
      {
	size_t buf_size;
	size_t chunk_size;
	s_bufhe_host *cd_bufhe = &conn->_data._bufhe;
	int varsize = 0;

	/*
	LWROC_INFO_FMT("bufhe: %d %d %d %d %d %d %d",
			cd_bufhe->l_dlen,
			cd_bufhe->i_type,
			cd_bufhe->i_subtype,
			cd_bufhe->h_end,
			cd_bufhe->h_begin,
			cd_bufhe->i_used,
			cd_bufhe->l_free[1]);
	*/

	if (cd_bufhe->l_free[0] != 0x00000001 &&
	    cd_bufhe->l_free[0] != 0x01000000)
	  {
	    LWROC_CERROR_FMT(&conn->_out._base,
			     "Swapping marker bad (%08x).",
			     cd_bufhe->l_free[0]);
	    return 0;
	  }

	conn->_swap32 = (cd_bufhe->l_free[0] == 0x01000000);

	if (conn->_swap32)
	  lwroc_byteswap32(cd_bufhe,
			   sizeof (*cd_bufhe));

	if (cd_bufhe->i_type    == LMD_BUF_HEADER_10_1_TYPE &&
	    cd_bufhe->i_subtype == LMD_BUF_HEADER_10_1_SUBTYPE)
	  ;
	else if (cd_bufhe->i_type    == LMD_BUF_HEADER_100_1_TYPE &&
		 cd_bufhe->i_subtype == LMD_BUF_HEADER_100_1_SUBTYPE)
	  varsize = 1;
	else if (cd_bufhe->i_type    == LMD_FILE_HEADER_2000_1_TYPE &&
		 cd_bufhe->i_subtype == LMD_FILE_HEADER_2000_1_SUBTYPE &&
		 conn->_out._state ==
		 LWROC_MERGE_IN_STATE_DATA_FILHE_READ_CHECK)
	  ;
	else if (cd_bufhe->i_type == LMD_BUF_HEADER_HAS_STICKY_TYPE &&
		 cd_bufhe->i_subtype == LMD_BUF_HEADER_HAS_STICKY_SUBTYPE)
	  ;
	else if (cd_bufhe->i_type == LMD_BUF_HEADER_HAS_STICKY_VARSZ_TYPE &&
		 cd_bufhe->i_subtype ==
		 /*               */ LMD_BUF_HEADER_HAS_STICKY_VARSZ_SUBTYPE)
	  ;
	else
	  {
	    LWROC_CERROR_FMT(&conn->_out._base,
			     "Type/subtype (%d/%d) not supported.",
			     cd_bufhe->i_type,
			     cd_bufhe->i_subtype);
	    return 0;
	  }

	if ((cd_bufhe->h_end   & ~1) ||
	    (cd_bufhe->h_begin & ~1))
	  {
	    LWROC_CERROR_FMT(&conn->_out._base,
			     "Unknown bits set in "
			     "h_end (0x%x) / h_begin (0x%x).",
			     cd_bufhe->h_end,
			     cd_bufhe->h_begin);
	    return 0;
	  }

	if (conn->_data._bufhe.l_evt <
	    (uint32_t) (cd_bufhe->h_end | cd_bufhe->h_begin))
	  {
	    LWROC_CERROR_FMT(&conn->_out._base,
			     "Too few events (0x%x) compared to "
			     "fragment end (0x%x) or begin (0x%x).",
			     conn->_data._bufhe.l_evt,
			     cd_bufhe->h_end,
			     cd_bufhe->h_begin);
	    return 0;
	  }

	if ((cd_bufhe->h_end == 1) !=
	    (conn->_expect_fragment != 0))
	  {
	    if (conn->_expect_fragment)
	      LWROC_CERROR_FMT(&conn->_out._base,
			       "Expected fragment at beginning of new buffer, "
			       "but has none (%d).", cd_bufhe->h_end);
	    else
	      LWROC_CERROR_FMT(&conn->_out._base,
			       "Buffer had unexpected fragment at start (%d).",
			       cd_bufhe->h_end);

	    /* Malformed, we do not handle such things, break connection. */
	    return 0;
	  }

	if (cd_bufhe->l_dlen <= LMD_BUF_HEADER_MAX_IUSED_DLEN &&
	    !varsize)
	  {
	    /* Old style, used in i_used, l_free[2] either 0 or same. */

	    conn->_used_left =
	      BUFFER_USED_FROM_IUSED((uint32_t) (uint16_t) cd_bufhe->i_used);

	    if (cd_bufhe->l_free[2] != 0 &&
		cd_bufhe->l_free[2] != cd_bufhe->i_used)
	      LWROC_CERROR_FMT(&conn->_out._base,
			       "Used buffer space double defined differently "
			       "(small buffer) "
			       "(i_used: 2*%" MYPRIzd ", "
			       "l_free[2]: 2*%" MYPRIzd ")",
			       (size_t) cd_bufhe->i_used,
			       (size_t) cd_bufhe->l_free[2]);
	  }
	else
	  {
	    /* New style, used only in l_free[2], i_used 0. */

	    conn->_used_left =
	      BUFFER_USED_FROM_IUSED(cd_bufhe->l_free[2]);

	    if (cd_bufhe->i_used != 0 &&
		cd_bufhe->l_free[2] != cd_bufhe->i_used)
	      LWROC_CERROR_FMT(&conn->_out._base,
			       "Used buffer space double defined differently "
			       "(large buffer) "
			       "(i_used: 2*%" MYPRIzd ", "
			       "l_free[2]: 2*%" MYPRIzd ")",
			       (size_t) cd_bufhe->i_used,
			       (size_t) cd_bufhe->l_free[2]);

	  }

	buf_size =
	  (size_t) BUFFER_SIZE_FROM_DLEN((uint32_t) cd_bufhe->l_dlen);

	if (cd_bufhe->i_type    == LMD_FILE_HEADER_2000_1_TYPE &&
	    cd_bufhe->i_subtype == LMD_FILE_HEADER_2000_1_SUBTYPE &&
	    cd_bufhe->l_dlen > LMD_BUF_HEADER_MAX_IUSED_DLEN)
	  {
	    /* For large buffers, the LMD file format lies about the
	     * buffer size in file headers.  Instead it is stored in
	     * the i_used member.
	     */

	    buf_size =
	      BUFFER_USED_FROM_IUSED((uint32_t) cd_bufhe->i_used) +
	      sizeof (s_bufhe_host);

	    /* This also means that the space has been consumed for
	     * the header.
	     */
	    conn->_used_left = 0;
	  }

	if (buf_size > conn->_max_buffer_size)
	  {
	    LWROC_CERROR_FMT(&conn->_out._base,
			     "Buffer larger than promised (%" MYPRIzd " > "
			     "%" MYPRIzd ") by %s.",
			     buf_size,
			     conn->_max_buffer_size,
			     "start message");
	    return 0;
	  }

	chunk_size = buf_size - sizeof (s_bufhe_host);

	if (conn->_used_left > chunk_size)
	  {
	    LWROC_CERROR_FMT(&conn->_out._base,
			     "Buffer claims more data used "
			     "(%" MYPRIzd ") than size "
			     "of payload (%" MYPRIzd ").",
			     conn->_used_left, chunk_size);
	    return 0;
	  }

	if (!conn->_data._bufhe.l_evt)
	  {
	    /* It is a keep-alive buffer.
	     * Has no meaningful buffer number.
	     */
	    /* printf ("Keep-alive.\n"); */
	  }
	else
	  {
	    if (cd_bufhe->l_buf != conn->_buffer_no + 1 &&
		conn->_buffer_no)
	      {
		LWROC_CERROR_FMT(&conn->_out._base,
				 "Buffer number out of order "
				 "(got %" PRIu32 ", "
				 "expected %" PRIu32 ").",
				 cd_bufhe->l_buf,
				 conn->_buffer_no + 1);
		return 0;
	      }
	    conn->_buffer_no = cd_bufhe->l_buf;
	  }

	/* printf ("%" PRIu32 "\n", cd_bufhe->l_buf); */

	conn->_padding_left = chunk_size - conn->_used_left;

	/* If we are to have a fragment at the end, there shall be a
	 * reasonable statement about the length of the last event.
	 */

	if (cd_bufhe->h_begin == 1)
	  {
	    size_t event_data_size;
	    size_t event_full_size;

	    event_data_size =
	      EVENT_DATA_LENGTH_FROM_DLEN(cd_bufhe->l_free[1]);

	    event_full_size = event_data_size + sizeof (lmd_event_header_host);

	    if (event_full_size > conn->_max_ev_len)
	      {
		LWROC_CERROR_FMT(&conn->_out._base,
				 "Size of fragmented event (%" MYPRIzd ") "
				 "larger than maximum event size "
				 "(%" MYPRIzd ") given by %s.",
				 event_full_size,
				 conn->_max_ev_len,
				 conn->_max_ev_len_source);
		return 0;
	      }

	    /* We can handle padding at the end of the first buffer
	     * with a fragmented event at the end.  We will anyhow
	     * ensure enough space in the output area (read
	     * destination) to last for the entire buffer if that
	     * happens to be longer than the fragmented event.
	     *
	    if (conn->_padding_left)
	      {
		LWROC_CERROR_FMT(&conn->_out._base,
				 "Fragmented event starts even though "
				 "buffer (%" MYPRIzd ") "
				 "has unused space (%" MYPRIzd ").  "
				 "Not supported.  "
				 "(chunk: %" MYPRIzd ", used: %" MYPRIzd ")",
				 buf_size, conn->_padding_left,
				 chunk_size, conn->_used_left);
		return 0;
	      }
	    */
	    /* Do not spam the user...
	     *
	    if (conn->_padding_left)
	      {
		LWROC_CWARNING_FMT(&conn->_out._base,
				   "Fragmented event starts, but "
				   "buffer (%" MYPRIzd ") also "
				   "has unused space (%" MYPRIzd ").  "
				   "(chunk: %" MYPRIzd ", used: %" MYPRIzd ")",
				   buf_size, conn->_padding_left,
				   chunk_size, conn->_used_left);
	      }
	    */
	  }

	/* Perhaps one should account the buffer after it is
	 * completely received?  (then several places to check)
	 */

	_lwroc_mon_in._buffers++;

	/* Are we to continue reading a fragmented event? */

	if (cd_bufhe->h_end)
	  {
	    if (conn->_used_left < sizeof (conn->_fevhe))
	      {
		LWROC_CERROR_FMT(&conn->_out._base,
				 "Buffer payload (%" MYPRIzd " left) "
				 "has no space "
				 "for fragmented event header (%" MYPRIzd ").",
				 conn->_used_left, sizeof (conn->_fevhe));
		return 0;
	      }
	    conn->_used_left -= sizeof (conn->_fevhe);
	    conn->_out._state = LWROC_MERGE_IN_STATE_DATA_FEVHE_READ_PREPARE;
	    break;
	  }

	conn->_out._state = LWROC_MERGE_IN_STATE_DATA_CHUNK_READ_PREPARE;
	break;
      }

    case LWROC_MERGE_IN_STATE_DATA_FEVHE_READ_CHECK:
      {
	size_t this_frag_size;

	if (conn->_swap32)
	  lwroc_byteswap32(&conn->_fevhe,
			   sizeof (conn->_fevhe));

	this_frag_size =
	  EVENT_DATA_LENGTH_FROM_DLEN(conn->_fevhe.l_dlen);

	if (this_frag_size > conn->_expect_fragment)
	  {
	    LWROC_CERROR_FMT(&conn->_out._base,
			     "Size of event fragment (%" MYPRIzd ") larger "
			     "than expected/remaining (%" MYPRIzd ").",
			     this_frag_size, conn->_expect_fragment);
	    return 0;
	  }
	if (this_frag_size > conn->_used_left)
	  {
	    LWROC_CERROR_FMT(&conn->_out._base,
			     "Size of event fragment (%" MYPRIzd ") larger "
			     "than buffer payload (%" MYPRIzd " left).",
			     this_frag_size, conn->_used_left);
	    return 0;
	  }

	conn->_used_left -= this_frag_size;
	conn->_expect_fragment -= this_frag_size;
	conn->_out._base._buf._size = this_frag_size;
	conn->_out._base._buf._ptr = conn->_chunk_start;
	conn->_chunk_end = conn->_chunk_start + this_frag_size;

	if (conn->_out._base._buf._ptr +
	    conn->_out._base._buf._size >
	    conn->_chunk_reserve_end)
	  LWROC_CBUG_FMT(&conn->_out._base,
			 "Fragment read (%" MYPRIzd ") would overshoot "
			 "remaining reserved area (%" MYPRIzd ").",
			 conn->_out._base._buf._size,
			 conn->_chunk_reserve_end-conn->_out._base._buf._ptr);

	/*
	printf ("expect frag [-%" MYPRIzd "]: "
		"%" MYPRIzd " (len: %" MYPRIzd ") "
		"end: %p\n",
		this_frag_size,
		conn->_expect_fragment, conn->_fragment_length,
		conn->_chunk_end);
	*/
	/* TODO: Check that type/subtype matches the event we have started. */

	conn->_out._state++; /* LWROC_MERGE_IN_STATE_DATA_FRAG_READ_PREPARE */
	break;
      }

    case LWROC_MERGE_IN_STATE_DATA_FRAG_READ_CHECK:
      {
	/* One would expect buffers that have spanning events between
	 * them to have the same byteorder, such that event can be
	 * swapped at the end.  No good - first part will be swapped
	 * already to be able to find the real fragmented event
	 * header.
	 */

	if (conn->_swap32)
	  lwroc_byteswap32(conn->_chunk_start,
			   conn->_chunk_end - conn->_chunk_start);

	if (!conn->_expect_fragment)
	  {
	    /* Fragment has been fully read.  Give it to the sorter. */

	    lwroc_pipe_buffer_did_write(conn->_buffer,
					conn->_fragment_length, 0);
	    lwroc_merge_in_check_fill(conn);

	    lwroc_merge_input_got_complete_event(conn, si);
	    conn->_out._base._mon->_block._events++;
	  }
	else
	  {
	    /* Expecting further data? */

	    /* We consider it to be an error if there is unused space
	     * in the buffer.
	     */

	    if (conn->_used_left)
	      {
		LWROC_CERROR_FMT(&conn->_out._base,
				 "Fragmented event intermediate part "
				 "(%" MYPRIzd " still needed) "
				 "did not use all payload "
				 "(%" MYPRIzd " left).",
				 conn->_expect_fragment, conn->_used_left);
		return 0;
	      }
	    /*assert (!conn->_padding_left);*/ /* checked earlier */
	    if (conn->_padding_left)
	      {
		/* This is a bit complicated to support due to our design.
		 * We'd have to eat that padding...
		 *
		 * One problem is to avoid having the following buffer
		 * checker look at the padding.  (As there is not
		 * event proper, it will get confused.)  Another issue
		 * is that we did not when ensuring space in the
		 * output buffer know how much padding may be needed,
		 * so may not have enough space to do the dummy
		 * reads...
		 */
		LWROC_CERROR_FMT(&conn->_out._base,
				 "Fragmented event intermediate part "
				 "leaving buffer "
				 "with unused space (%" MYPRIzd ").  "
				 "Not supported.  ",
				 conn->_padding_left);
		return 0;
	      }

	    /* Next read to continue here. */
	    conn->_chunk_start = conn->_chunk_end;
	  }

	if (conn->_used_left + conn->_padding_left == 0)
	  {
	    /* No more data in buffer, get next buffer. */
	    conn->_out._state = LWROC_MERGE_IN_STATE_DATA_BUFHE_READ_PREPARE;
	    break;
	  }

	/* Get remaining data. (or padding) */

	conn->_out._state = LWROC_MERGE_IN_STATE_DATA_CHUNK_READ_PREPARE;
	break;
      }
    case LWROC_MERGE_IN_STATE_DATA_CHUNK_READ_CHECK:
      {
	if (conn->_swap32)
	  lwroc_byteswap32(conn->_chunk_start,
			   conn->_chunk_end - conn->_chunk_start);

	if (!lwroc_merge_in_check_buffer(conn, si))
	  return 0;

	/*
	LWROC_CINFO_FMT(&conn->_out._base,
	  "Done reading chunk (used = %" MYPRIzd " / %" MYPRIzd ")...",
		       used, conn->_out._base._buf._size);
	*/

	/* So, get next buffer. */
	conn->_out._state = LWROC_MERGE_IN_STATE_DATA_BUFHE_READ_PREPARE;
	break;
      }

    case LWROC_MERGE_IN_STATE_EBYE_RECORD_READ_CHECK:
      {
	size_t buf_size;
	size_t chunk_size;
	ebye_record_header *cd_record = &conn->_data._ebye_record;

	if (conn->_flags & LWROC_MERGE_SOURCE_FLAGS_DEBUG)
	  lwroc_ebye_record_dump(cd_record);

	if (memcmp (cd_record->_id, EBYE_RECORD_HEADER_ID,
		    sizeof (cd_record->_id)) != 0)
	  {
	    char print_id[9];

	    lwroc_string_to_print(print_id, cd_record->_id, 8);

	    LWROC_CERROR_FMT(&conn->_out._base,
			     "EBYE record id wrong "
			     "(got '%s', expected '%s').",
			     print_id,
			     EBYE_RECORD_HEADER_ID);
	    return 0;
	  }

	if (cd_record->_endian_tape != 0x0001 &&
	    cd_record->_endian_tape != 0x0100)
	  {
	    LWROC_CERROR_FMT(&conn->_out._base,
			     "EBYE record tape (header) "
			     "swapping marker bad (%04x).",
			     cd_record->_endian_tape);
	    return 0;
	  }

	if (cd_record->_endian_data != 0x0001 &&
	    cd_record->_endian_data != 0x0100)
	  {
	    LWROC_CERROR_FMT(&conn->_out._base,
			     "EBYE record data "
			     "swapping marker bad (%04x).",
			     cd_record->_endian_data);
	    return 0;
	  }

	conn->_swap32 = (cd_record->_endian_data == 0x0100);

	/* Note: this swapping will 'damage' the _id and _endian_data
	 * members.
	 */
	if (cd_record->_endian_tape == 0x0100)
	  lwroc_byteswap32(cd_record,
			   sizeof (*cd_record));

	conn->_used_left = cd_record->_data_length;

	if (conn->_type == LWROC_MERGE_IN_TYPE_FILE)
	  buf_size = conn->_max_buffer_size;
	else
	  buf_size = cd_record->_data_length + sizeof (ebye_record_header);

	if (buf_size > conn->_max_buffer_size)
	  {
	    LWROC_CERROR_FMT(&conn->_out._base,
			     "EBYE record (data) larger than given buffer size "
			     "(%" MYPRIzd " > %" MYPRIzd ").",
			     buf_size,
			     conn->_max_buffer_size);
	    return 0;
	  }

	chunk_size = buf_size - sizeof (ebye_record_header);

	if (conn->_used_left > chunk_size)
	  {
	    LWROC_CERROR_FMT(&conn->_out._base,
			     "EBYE record claims more data used "
			     "(%" MYPRIzd ") than size "
			     "of payload (%" MYPRIzd ").",
			     conn->_used_left, chunk_size);
	    return 0;
	  }

	conn->_padding_left = chunk_size - conn->_used_left;

	if (!cd_record->_data_length)
	  {
	    /* It is a keep-alive buffer.
	     * Has no meaningful buffer number.
	     */
	  }
	else
	  {
	    if (cd_record->_sequence != conn->_buffer_no + 1 &&
		conn->_buffer_no)
	      {
		LWROC_CERROR_FMT(&conn->_out._base,
				 "EBYE sequence number out of order "
				 "(got %" PRIu32 ", "
				 "expected %" PRIu32 ").",
				 cd_record->_sequence,
				 conn->_buffer_no + 1);
		return 0;
	      }
	  }
	conn->_buffer_no = cd_record->_sequence;

	conn->_out._state = LWROC_MERGE_IN_STATE_EBYE_CHUNK_READ_PREPARE;

	break;
      }

    case LWROC_MERGE_IN_STATE_EBYE_XFER_READ_CHECK:
      {
	size_t buf_size;
	size_t chunk_size;
	ebye_xfer_header *cd_xfer = &conn->_data._ebye_xfer;

	lwroc_ebye_xfer_ntoh(cd_xfer);

	if (conn->_flags & LWROC_MERGE_SOURCE_FLAGS_DEBUG)
	  lwroc_ebye_xfer_dump(cd_xfer);

	if (!(cd_xfer->_id1 == EBYE_XFER_HEADER_ID1 &&
	      cd_xfer->_id2 == EBYE_XFER_HEADER_ID2) &&
	    !(cd_xfer->_data_length == (uint32_t) -1 &&
	      cd_xfer->_id1 == 0 &&
	      cd_xfer->_id2 == 0))
	  {
	    LWROC_CERROR_FMT(&conn->_out._base,
			     "EBYE xfer id1:2 wrong "
			     "(got %08x:%08x, expected %08x:%08x, "
			     "data len: 0x%x).",
			     cd_xfer->_id1, cd_xfer->_id2,
			     cd_xfer->_data_length,
			     EBYE_XFER_HEADER_ID1, EBYE_XFER_HEADER_ID2);
	    return 0;
	  }

	if (cd_xfer->_endian != 0x0001 &&
	    cd_xfer->_endian != 0x0100)
	  {
	    LWROC_CERROR_FMT(&conn->_out._base,
			     "EBYE xfer swapping marker bad (%04x).",
			     cd_xfer->_endian);
	    return 0;
	  }

	conn->_swap32 = (cd_xfer->_endian == 0x0100);

	conn->_used_left = cd_xfer->_data_length;
	buf_size = cd_xfer->_block_length;

	if (((int32_t) cd_xfer->_data_length) < 0)
	  {
	    if (((int32_t) cd_xfer->_data_length) < -1)
	      {
		LWROC_CERROR_FMT(&conn->_out._base,
				 "EBYE xfer data length (%" PRId32 ") < -1.",
				 (int32_t) cd_xfer->_data_length);
		return 0;
	      }

	    /* A (re)start buffer.  Used to tell the block_length of
	     * following buffers.  (We do not care; are adaptive).
	     */
	    conn->_used_left = 0;

	    if (conn->_type != LWROC_MERGE_IN_TYPE_DRASI)
	      {
		/* The block_length is however telling the size of following
		 * blocks.  This block has the same size as the previous.
		 * Or 1 kiB if it is the first.
		 *
		 * _fragment_length is abused to remember previous block size.
		 */
		buf_size = conn->_fragment_length;
	      }

	    /* printf ("Get resize buffer: %zd bytes.\n", buf_size); */
	  }

	/* Remember previous block size. */
	conn->_fragment_length = cd_xfer->_block_length;

	if (buf_size > conn->_max_buffer_size)
	  {
	    LWROC_CERROR_FMT(&conn->_out._base,
			     "EBYE xfer (data) larger than given buffer size "
			     "(%" MYPRIzd " > %" MYPRIzd ").",
			     buf_size,
			     conn->_max_buffer_size);
	    return 0;
	  }

	chunk_size = buf_size - sizeof (ebye_xfer_header);

	if (conn->_used_left > chunk_size)
	  {
	    LWROC_CERROR_FMT(&conn->_out._base,
			     "EBYE xfer claims more data used "
			     "(%" MYPRIzd ") than size "
			     "of payload (%" MYPRIzd ").",
			     conn->_used_left, chunk_size);
	    return 0;
	  }

	conn->_padding_left = chunk_size - conn->_used_left;

	if (conn->_used_left == 0)
	  {
	    /* It is a keep-alive buffer.
	     * Has no meaningful buffer number.
	     */
	  }
	else
	  {
	    if (cd_xfer->_sequence != conn->_buffer_no + 1 &&
		conn->_buffer_no)
	      {
		/* This could be a warning.  But since it should not
		 * happen, better complain loudly.
		 */
		LWROC_CERROR_FMT(&conn->_out._base,
				 "EBYE xfer sequence number out of order "
				 "(got %" PRIu32 ", "
				 "expected %" PRIu32 ").",
				 cd_xfer->_sequence,
				 conn->_buffer_no + 1);
		return 0;
	      }
	    conn->_buffer_no = cd_xfer->_sequence;
	  }

	conn->_out._state = LWROC_MERGE_IN_STATE_EBYE_CHUNK_READ_PREPARE;

	break;
      }

    case LWROC_MERGE_IN_STATE_EBYE_CHUNK_READ_CHECK:
      {
	if (conn->_swap32)
	  lwroc_byteswap32(conn->_chunk_start,
			   conn->_chunk_end - conn->_chunk_start);

	if (_lwroc_merge_info._data_fmt->_buffer_format ==
	    LWROC_DATA_TRANSPORT_FORMAT_XFER)
	  {
	    if (!lwroc_merge_in_check_xfer_block(conn, si))
	      return 0;
	  }
	else
	  {
	    if (!lwroc_merge_in_check_ebye_record(conn, si))
	      return 0;
	  }

	/*
	LWROC_CINFO_FMT(&conn->_out._base,
	  "Done reading chunk (%" MYPRIzd ")...",
			conn->_out._base._buf._size);
	*/

	/* So, get next buffer. */
	conn->_out._state =
	  lwroc_merge_in_buffer_read_prepare(conn->_type ==
					     LWROC_MERGE_IN_TYPE_FILE);
	break;
      }

    case LWROC_MERGE_IN_STATE_UDP_WAIT_CONNECTED:
    case LWROC_MERGE_IN_STATE_DATA_WAIT_CONNECTED:
    {
      int so_error;

      if (!LWROC_WRITE_FD_ISSET(conn->_out._base._fd,si))
	break;

      /* So the connect has finished, figure out if it was successful. */

      if (!lwroc_net_client_base_connect_get_status(&conn->_out._base,
						    &so_error))
	{
	  LWROC_CWARNING_FMT(&conn->_out._base,
			     "Delayed connect: %s",
			     strerror(so_error));
	  return 0; /* connection failed */
	}

      conn->_out._state++; /* state after xxx_WAIT_CONNECTED */
      /* Next state shall either read or write. */

      lwroc_merge_in_connected(conn);

      /* must wait for allowance to write, break */
      break;
    }
    case LWROC_MERGE_IN_STATE_FAILED_WAIT_DEAD:
      /* Will be reclaimed by the dead-source reaper. */
      break;

    }

#if STATE_MACHINE_DEBUG
  printf ("IN %p:  -> %d [ok]\n", conn, conn->_out._state);
  fflush(stdout);
#endif
  return 1;
}

/********************************************************************/

int lwroc_merge_in_after_fail(lwroc_select_item *item,
			      lwroc_select_info *si)
{
  lwroc_merge_source *conn =
    PD_LL_ITEM(item, lwroc_merge_source, _out._base._select_item);

  conn->_out._base._mon->_block._status = LWROC_CONN_STATUS_NOT_CONNECTED;
  LWROC_MON_CHECK_COPY_CONN_MON_BLOCK(conn->_out._base._mon, 1);

  /* printf ("after fail...\n"); */

  /* Connection closed, remove it. */

  if (conn->_out._base._fd != -1)
    {
      if (conn->_type == LWROC_MERGE_IN_TYPE_FILE &&
	  (conn->_flags & LWROC_MERGE_SOURCE_FLAGS_LOOP))
	{
	  if (conn->_out._state == LWROC_MERGE_IN_STATE_DATA_BUFHE_READ ||
	      conn->_out._state == LWROC_MERGE_IN_STATE_DATA_FEVHE_READ ||
	      conn->_out._state == LWROC_MERGE_IN_STATE_DATA_FRAG_READ ||
	      conn->_out._state == LWROC_MERGE_IN_STATE_DATA_CHUNK_READ ||
	      conn->_out._state == LWROC_MERGE_IN_STATE_EBYE_RECORD_READ ||
	      conn->_out._state == LWROC_MERGE_IN_STATE_EBYE_XFER_READ ||
	      conn->_out._state == LWROC_MERGE_IN_STATE_EBYE_CHUNK_READ)
	    {
	      lseek(conn->_out._base._fd, 0, SEEK_SET);
	      /* This can happen quite often... */
	      lwroc_message_rate_ctrl(LWROC_MESSAGE_RATE_CTRL_LIMIT);
	      LWROC_CINFO(&conn->_out._base,
			  "Rewind file for loop.");
	      conn->_buffer_no = 0; /* Do not warn. */
	      conn->_out._state = lwroc_merge_in_buffer_read_prepare(1);
	      /* File is connected again. */
	      conn->_out._base._mon->_block._status =
		LWROC_CONN_STATUS_CONNECTED;
	      LWROC_MON_CHECK_COPY_CONN_MON_BLOCK(conn->_out._base._mon, 1);
	      return 1;
	    }
	  else
	    LWROC_CINFO_FMT(&conn->_out._base,
			    "Not rewinding file in unexpected "
			    "end-of-file state (%d).",
			    conn->_out._state);
	}
      lwroc_safe_close(conn->_out._base._fd);
      conn->_out._base._fd = -1;
      LWROC_CINFO(&conn->_out._base,
		  "Connection closed...");
    }

  /* If we have been given to the sorter, we have to tell that we
   * failed, and wait for us to be removed from the sorter list.
   */
  if (conn->_had_first_complete_event)
    {
      conn->_out._state = LWROC_MERGE_IN_STATE_FAILED_INJECT_ENDMARK;
    }
  else
    {
      /* Try connection again... */
      lwroc_merge_in_failed_setup_retry(conn, &si->now);
    }

  return 1;
}

/********************************************************************/

lwroc_select_item_info lwroc_merge_in_select_item_info =
{
  lwroc_merge_in_setup_select,
  lwroc_merge_in_after_select,
  lwroc_merge_in_after_fail,
  NULL,
  0,
};

lwroc_outgoing_select_item_info lwroc_native_merge_in_select_item_info =
{
  {
    lwroc_merge_in_setup_select,
    lwroc_merge_in_after_select,
    lwroc_merge_in_after_fail,
    NULL,
    0,
  },
  lwroc_merge_in_native_connected,
  LWROC_REQUEST_DATA_TRANS,
  "--server=drasi,dest= option",
};

/********************************************************************/

/* TODO: lifted from lwrocmon.c
 * MERGE!!!!
 */

void lwroc_merge_in_loop_setup_service(void *arg,
				       lwroc_select_info *si)
{
  int nonempty = 0;
  int i;
  (void) arg;

  for (i = 0; i < 2; i++)
    LWROC_ITEM_BUFFER_NONEMPTY_OR_SET_NOTIFY(_lwroc_merge_info._dead_sources[i],
					     _lwroc_merge_in_thread->_block,
					     nonempty);
  LWROC_ITEM_BUFFER_NONEMPTY_OR_SET_NOTIFY(_lwroc_merge_info._connected_sources,
					   _lwroc_merge_in_thread->_block,
					   nonempty);

  if (nonempty)
    lwroc_select_info_time_setup(si, &si->now);
}

void lwroc_merge_in_loop_after_service(void *arg,
				       lwroc_select_info *si)
{
  int i;
  (void) arg;

  /* Add any freshly connected sources. */

  while (LWROC_ITEM_BUFFER_NONEMPTY(_lwroc_merge_info._connected_sources))
    {
      lwroc_merge_source *conn;
      int add_to_list = 0;

      LWROC_ITEM_BUFFER_REMOVE(_lwroc_merge_info._connected_sources, &conn);

      /* printf ("freshly connected... %d\n", conn->_type); */

      conn->_out._base._select_item.item_info =
	&lwroc_merge_in_select_item_info;

      if (conn->_type == LWROC_MERGE_IN_TYPE_DRASI)
	{
	  conn->_out._state = LWROC_MERGE_IN_STATE_DATA_SETUP_READ_PREPARE;
	  add_to_list = 1;
	}
      else if (conn->_type == LWROC_MERGE_IN_TYPE_EBYE_PUSH)
	{
	  assert(conn->_out._state ==
		 LWROC_MERGE_IN_STATE_EBYE_PUSH_WAIT_CLIENT);

	  conn->_out._state = LWROC_MERGE_IN_STATE_EBYE_XFER_READ_PREPARE;

	  /* No add_to_list; we are already in list. */
	}
      else
	assert (0);

      lwroc_merge_in_clear(conn);

      /* Insert into the list. */

      if (add_to_list)
	{
	  PD_LL_ADD_BEFORE(&_lwroc_merge_sources,
			   &conn->_out._base._select_item._items);
	}
    }

  /* Reclaim any dead sources. */

  for (i = 0; i < 2; i++)
    while (LWROC_ITEM_BUFFER_NONEMPTY(_lwroc_merge_info._dead_sources[i]))
      {
	lwroc_merge_source *conn;

	/* printf ("dead reclaim...\n"); */

	/* TODO: remove the notify marker. */

	LWROC_ITEM_BUFFER_REMOVE(_lwroc_merge_info._dead_sources[i], &conn);

	/* This source is hereby no longer given to the sorting thread!
	 */
	conn->_had_first_complete_event = 0;
	_lwroc_mon_in._active--;

	/* Possibly close, and setup the timeout. */
	lwroc_merge_in_after_fail(&conn->_out._base._select_item, si);
      }

  LWROC_MON_CHECK_COPY_BLOCK(_lwroc_mon_in_handle,&_lwroc_mon_in, 0);
}

void lwroc_merge_in_thread_loop(lwroc_thread_instance *inst)
{
  lwroc_select_loop(&_lwroc_merge_sources, &inst->_terminate,
		    _lwroc_merge_in_thread->_block,
		    lwroc_merge_in_loop_setup_service, NULL,
		    NULL, NULL,
		    lwroc_merge_in_loop_after_service, NULL,
		    1);
}

/********************************************************************/

char *lwroc_merge_in_fmt_msg_context(char *buf, size_t size,
				     const  void *ptr)
{
  const lwroc_merge_source *conn = (const lwroc_merge_source *)
    (ptr - offsetof(lwroc_merge_source, _out._base._msg_context));
  char *p;

  /* TODO: lwroc_message_fmt_msg_context should return the size, so
   * we do not need to do this arithmetic.
   */

  switch (conn->_type)
    {
    case LWROC_MERGE_IN_TYPE_MBS_TRANS:
      p = lwroc_message_fmt_msg_context(buf, size,
					"trans-cli: %s", conn->_out._hostname);
      break;
    case LWROC_MERGE_IN_TYPE_EBYE_PUSH:
    {
      char dotted[INET6_ADDRSTRLEN];

      lwroc_inet_ntop(&conn->_out._base._addr, dotted, sizeof (dotted));
      p = lwroc_message_fmt_msg_context(buf, size,
					"ebye-push-sink: %s",
					dotted);
      break;
    }
    case LWROC_MERGE_IN_TYPE_EBYE_PULL:
      p = lwroc_message_fmt_msg_context(buf, size,
					"ebye-pull-cli: %s",
					conn->_out._hostname);
      break;
    case LWROC_MERGE_IN_TYPE_FAKERNET:
      p = lwroc_message_fmt_msg_context(buf, size,
					"fnet-cli: %s", conn->_out._hostname);
      break;
    case LWROC_MERGE_IN_TYPE_DRASI:
      p = lwroc_message_fmt_msg_context(buf, size,
					"drasi-cli: %s", conn->_out._hostname);
      break;
    case LWROC_MERGE_IN_TYPE_FILE:
      p = lwroc_message_fmt_msg_context(buf, size,
					"file-in: %s", conn->_out._hostname);
      break;
    default:
      p = lwroc_message_fmt_msg_context(buf, size, "?");
      break;
    }

  size -= (size_t) (p - buf);

  /* Are we the sorting thread, and have an active header with information? */

  if (lwroc_get_msg_buf_source() == LWROC_MSG_BUFS_MAIN)
    {
      char *pold;

      if (conn->_event_10_1_context_set &
	  (LWROC_MERGE_SOURCE_EVENT_10_1_SET_HEADER |
	   LWROC_MERGE_SOURCE_EVENT_10_1_SET_INFO))
	{
	  pold = p;
	  p = lwroc_message_fmt_msg_context(p, size,
					    "] [");
	  size -= (size_t) (p - pold);
	}

      if (conn->_event_10_1_context_set &
	  LWROC_MERGE_SOURCE_EVENT_10_1_SET_INFO)
	{
	  pold = p;
	  p = lwroc_message_fmt_msg_context(p, size,
					    "trig: %d, eventno: %u",
					    conn->event_10_1._info.i_trigger,
					    conn->event_10_1._info.l_count);
	  size -= (size_t) (p - pold);
	}

      if (conn->_event_10_1_context_set &
	  LWROC_MERGE_SOURCE_EVENT_10_1_SET_HEADER)
	{
	  size_t event_data_size;
	  size_t event_full_size;

	  event_data_size =
	    EVENT_DATA_LENGTH_FROM_DLEN(conn->event_10_1._header.l_dlen);
	  event_full_size = event_data_size + sizeof (lmd_event_header_host);

	  pold = p;
	  p = lwroc_message_fmt_msg_context(p, size,
					    ", size %" MYPRIzd "",
					    event_full_size);
	  size -= (size_t) (p - pold);
	}

      if (conn->_disabled_nodata)
	{
	  const char *reason = NULL;
	  time_t since;
	  time_t now;

	  if (conn->_disabled_nodata == LWROC_MERGE_DISABLED_BROKEN)
	    reason = "semi-ignore (broken ts)";
	  if (conn->_disabled_nodata == LWROC_MERGE_DISABLED_NODATA)
	    reason = "disabled (nodata)";

	  now = time(NULL);

	  since = now - conn->_disabled_at;

	  pold = p;
	  p = lwroc_message_fmt_msg_context(p, size,
					    "] [%s, %d s",
					    reason, (int) since);
	  size -= (size_t) (p - pold);
	}
    }

  return p;
}

/********************************************************************/

void lwroc_merge_in_src_usage(void)
{
  printf ("\n");
  printf ("--[drasi|trans|ebye-[push|pull]|fnet|file]=[<OPTIONS>,]HOST[:[+]PORT]\n");
  printf ("\n");
  printf ("  inbufsize=N              Size of input buffer (Mi|raw hex|frac|%%).\n");
  printf ("  max-ev-size=N            Maximum event size.\n");
  printf ("  ts-disable=Ns            Disable timesort source after N s without data.\n");
  printf ("  keepalive=Ns             Re-connect source if no data seen for N s.\n");
  printf ("  label=STRING             Label, used for monitoring.  (only --trans)\n");
  printf ("  loop                     Loop file (rewind on read failure).  (only --file)\n");
  printf ("  debug                    Print incoming headers.\n");
  printf ("  id=ID                    Expected time source id.  Other disallowed.\n");
  printf ("                           (Can be given multiple times.)\n");
  printf ("  port=PORT                Server port number.  (only --ebye-push)\n");
  printf ("  HOST[:[+]PORT]           Network data source.\n");
  printf ("\n");
}

/********************************************************************/

void lwroc_parse_merge_source(lwroc_merge_source_opt *opt)
{
  /* Mostly lifted from lwrocmon.c lwrocmon_add_mon_host()
   * Resync / use common function?
   */

  lwroc_merge_source *conn;

  const char *hostname = NULL;
  const char *label = NULL;
  size_t bufsize = 0;
  double buffrac = 0.0;
  size_t max_ev_len = 0;
  /* Default to the global disable value (if any). */
  int ts_disable_timeout = _config._merge_ts_disable_timeout;
  int keepalive_timeout = _config._merge_keepalive_timeout;
  int loop = 0;
  int debug = 0;
  uint64_t id_mask = 0;
  int server_port = -1;

  const char *request;
  lwroc_parse_split_list parse_info;

  if ((opt->_type == LWROC_MERGE_IN_TYPE_EBYE_PUSH) &&
      opt->_config == NULL)
    goto no_options;

  lwroc_parse_split_list_setup(&parse_info, opt->_config, ',');

  while ((request = lwroc_parse_split_list_next(&parse_info)) != NULL)
    {
      const char *post;

      if (LWROC_MATCH_C_ARG("help"))
	{
	  lwroc_merge_in_src_usage();
	  exit(0);
	}
      else if (opt->_type != LWROC_MERGE_IN_TYPE_EBYE_PUSH &&
	       lwroc_parse_split_list_final(&parse_info))
	{
	  /*CMDLINEARG:HOST*/
	  hostname = strdup_chk(request);
	}
      else if (LWROC_MATCH_C_PREFIX("inbufsize=",post))
	{
	  bufsize =
	    (size_t) lwroc_parse_hex_double(post, "input buffer size",
					    &buffrac, 1);
	  /* printf("%zd  %.6f\n",bufsize,buffrac); */
	}
      else if (LWROC_MATCH_C_PREFIX("max-ev-size=",post))
	{
	  max_ev_len =
	    (size_t) lwroc_parse_hex(post, "input max event size");
	}
      else if (LWROC_MATCH_C_PREFIX("ts-disable=",post))
	{
	  ts_disable_timeout = lwroc_parse_time(post,"TS disable");
	}
      else if (LWROC_MATCH_C_PREFIX("keepalive=",post))
	{
	  keepalive_timeout = lwroc_parse_time(post,"keepalive timeout");
	}
      else if (LWROC_MATCH_C_PREFIX("label=",post))
	{
	  label = strdup_chk(post);
	}
      else if (LWROC_MATCH_C_PREFIX("id=",post))
	{
	  int id = (int) lwroc_parse_hex(post, "time source id");

	  id_mask |= ((uint64_t) 1) << id;
	}
      else if (LWROC_MATCH_C_ARG("loop"))
	loop = 1;
      else if (LWROC_MATCH_C_ARG("debug"))
	debug = 1;
      else if (LWROC_MATCH_C_PREFIX("port=",post))
	server_port = lwroc_parse_hex_u16(post, "server port number");
      else
	LWROC_BADCFG_FMT("Unrecognised input option: %s", request);
    }
 no_options:
  ;

  /* Create the connection structure early, such that error printing
   * can use the context.
   */

  conn = (lwroc_merge_source *) malloc (sizeof (lwroc_merge_source));

  if (!conn)
    LWROC_FATAL("Memory allocation failure (merge source conn info).");

  memset (conn, 0, sizeof (lwroc_merge_source));

  /* When adding a host, we immediately look the hostname up.  But we
   * do not care about any port or if it responds.  Also portmapping
   * is done later.  (In part as it has to be retried after
   * communication failure.)
   */
  conn->_type = opt->_type;
  conn->_label = label;

  conn->_out._hostname = hostname;
  conn->_out._base._msg_context = lwroc_merge_in_fmt_msg_context;

  /* Now continue to check the options. */

  if ((_lwroc_merge_info._mode & LWROC_MERGE_MODE_EB) &&
      ts_disable_timeout)
    LWROC_CBADCFG(&conn->_out._base,
		  "Source timesort disable (ts-disable) timeout "
		  "cannot be used in event builder mode.");

  if (ts_disable_timeout < 0)
    LWROC_CBADCFG_FMT(&conn->_out._base,
		      "Source timesort disable (ts-disable) timeout "
		      "(%d s) too small.",
		      ts_disable_timeout);

  /* Transport sources do not have any way to report what they promise,
   * and typically do not check it internally either.
   * We cannot check.  At least, do not allow small timeout values.
   */
  if ((opt->_type == LWROC_MERGE_IN_TYPE_MBS_TRANS ||
       opt->_type == LWROC_MERGE_IN_TYPE_EBYE_PUSH ||
       opt->_type == LWROC_MERGE_IN_TYPE_EBYE_PULL ||
       opt->_type == LWROC_MERGE_IN_TYPE_FAKERNET) &&
      ts_disable_timeout &&
      ts_disable_timeout < 60)
    LWROC_CBADCFG_FMT(&conn->_out._base,
		      "Refusing timesort disable (ts-disable) "
		      "timeout (%d s) < 60 s with transport source.",
		      ts_disable_timeout);

  /* There is never any need to provide a timesort disable timeout.
   * (It is a dangerous option, so use on own risk / with care.)
   *
   * However, if we have made a promise to following systems about a
   * maximum event interval, then all our sources must have a disable
   * timeout, such that we actually do not get stuck ourselves.
   *
   * This makes sense since the only reason to make a promise to later
   * systems is for that later system to in turn apply a disable
   * timeout.
   *
   * If a disable timeout is given, then it must be smaller than any
   * promise we have made on maximum event intervals regarding our
   * output to next stages.
   */
  if ((_lwroc_merge_info._mode & LWROC_MERGE_MODE_TS) &&
      _config._max_event_interval)
    {
      if (!ts_disable_timeout)
	LWROC_CBADCFG(&conn->_out._base,
		      "Timesort disable (ts-disable) timeout "
		      "is required for all sources when "
		      "max event interval (max-ev-interval) is given.");
      else if (ts_disable_timeout + LWROC_MERGE_SOURCE_DISABLE_INPUT_MARGIN >
	       _config._max_event_interval)
	LWROC_CBADCFG_FMT(&conn->_out._base,
			  "Refusing "
			  "timesort disable (ts-disable) timeout "
			  "(%d s + margin %d s) > "
			  "max event interval (max-ev-interval) (%d s).",
			  ts_disable_timeout,
			  LWROC_MERGE_SOURCE_DISABLE_INPUT_MARGIN,
			  _config._max_event_interval);
    }

  if (keepalive_timeout < 0)
    LWROC_CBADCFG_FMT(&conn->_out._base,
		      "Source keepalive timeout (%d s) too small.",
		      keepalive_timeout);

  if (bufsize != (size_t) -2 &&
      bufsize != 0)
    {
      if (bufsize < LWROC_MIN_INPUT_BUFFER_BUF_SIZE)
	LWROC_CBADCFG_FMT(&conn->_out._base,
			  "Refusing input buffer size %" MYPRIzd " < %d.",
			  bufsize, LWROC_MIN_INPUT_BUFFER_BUF_SIZE);

      if (max_ev_len != 0 &&
	  bufsize < 2 * max_ev_len)
	LWROC_CBADCFG_FMT(&conn->_out._base,
			  "Input buffer size %" MYPRIzd " < "
			  "2*max-event-len = 2*%" MYPRIzd " = %" MYPRIzd ".",
			  bufsize, max_ev_len, 2 * max_ev_len);
    }

  if (label &&
      opt->_type != LWROC_MERGE_IN_TYPE_MBS_TRANS &&
      opt->_type != LWROC_MERGE_IN_TYPE_EBYE_PUSH &&
      opt->_type != LWROC_MERGE_IN_TYPE_EBYE_PULL &&
      opt->_type != LWROC_MERGE_IN_TYPE_FAKERNET &&
      opt->_type != LWROC_MERGE_IN_TYPE_FILE)
    LWROC_CBADCFG(&conn->_out._base,
		  "Source label=STRING only allowed for "
		  "--trans/--ebye-[push|pull]/--fnet/--file.");

  if (loop &&
      opt->_type != LWROC_MERGE_IN_TYPE_FILE)
    LWROC_CBADCFG(&conn->_out._base,
		  "Source loop only allowed for --file.");

  if (opt->_type == LWROC_MERGE_IN_TYPE_FILE)
    {
      int fd;
      const char *filename = conn->_out._hostname;

      fd = open(filename,
		O_RDONLY
#ifdef O_LARGEFILE
		| O_LARGEFILE
#endif
		);

      if (fd == -1)
	{
	  LWROC_CPERROR(&conn->_out._base,
			"open");
	  LWROC_CBADCFG_FMT(&conn->_out._base,
			    "Failed to open '%s' for input.",
			    filename);
	}

      LWROC_CACTION_FMT(&conn->_out._base,
			"Opened input file '%s'.",filename);

      /* Format not known yet. */
      conn->_out._state = LWROC_MERGE_IN_STATE_FILE_FMT_SWITCH;
      conn->_out._base._fd = fd;
    }
  else if (conn->_type != LWROC_MERGE_IN_TYPE_EBYE_PUSH)
    {
      if (!lwroc_get_host_port(hostname,
			       (conn->_type == LWROC_MERGE_IN_TYPE_MBS_TRANS ?
				LMD_TCP_PORT_TRANS :
				conn->_type == LWROC_MERGE_IN_TYPE_EBYE_PUSH ?
				LMD_TCP_PORT_EBYE_XFER_PUSH :
				conn->_type == LWROC_MERGE_IN_TYPE_EBYE_PULL ?
				LMD_TCP_PORT_EBYE_XFER_PULL :
				conn->_type == LWROC_MERGE_IN_TYPE_FAKERNET ?
				LMD_TCP_PORT_FAKERNET :
				LWROC_NET_DEFAULT_PORT),
			       &conn->_out._base._addr))
	exit(1);
    }

  conn->_buffer_size = bufsize;
  conn->_buffer_frac = buffrac;
  conn->_max_ev_len_config = max_ev_len;
  conn->_ts_disable_timeout = ts_disable_timeout;
  conn->_keepalive_timeout = keepalive_timeout;
  conn->_ts_id_mask = id_mask;

  if (loop)
    conn->_flags |= LWROC_MERGE_SOURCE_FLAGS_LOOP;
  if (debug)
    conn->_flags |= LWROC_MERGE_SOURCE_FLAGS_DEBUG;

  /* Any source having 0 ts_disable_timeout means that we can stall
   * forever, so keep track of that separately.
   */
#if 0
  if (conn->_ts_disable_timeout == 0)
    _lwroc_merge_info._zero_ts_disable_timeout = 1;
  else if (conn->_ts_disable_timeout >
	   _lwroc_merge_info._max_ts_disable_timeout)
    _lwroc_merge_info._max_ts_disable_timeout = conn->_ts_disable_timeout;
#endif

  conn->_event_10_1_context_set = 0;

  opt->_conn = conn;

  if (opt->_type == LWROC_MERGE_IN_TYPE_EBYE_PUSH)
    lwroc_merge_in_unit_setup(conn, server_port);
}

void lwroc_parse_merge_sources(lwroc_data_pipe_cfg *pipe_cfg)
{
  pd_ll_item *iter;
  size_t sum_buf_size = 0;
  double sum_buf_frac = 0;
  size_t num_src = 0;
  size_t num_frac = 0;
  size_t num_size = 0;
  size_t num_auto;
  double auto_frac = 0;
  double rescale_frac = 1;
  double total_input_frac;
  size_t tot_input_size;
  double input_frac;
  size_t sum_input_size = 0;

  lwroc_merge_init_conn_list();

  /* TODO: this parsing should be earlier.  Such that one can get an
   * answer to --drasi=help without having to specify a buffer size.
   */
  PD_LL_FOREACH(_lwroc_merge_source_opts, iter)
    {
      lwroc_merge_source_opt *opt =
	PD_LL_ITEM(iter, lwroc_merge_source_opt, _source_opts);

      lwroc_parse_merge_source(opt);
    }

  /* There are a number of checks below to try to catch the most skew
   * buffer configurations.  This does not mean that what is not
   * refused or warned for is performant.
   */

  /* Now see if there are sources that have no input size specified,
   * and we thus cut out the memory from the output buffer.  For
   * specified input sources, we also subtract that much from the buffer
   * size of the output buffer.
   */

  PD_LL_FOREACH(_lwroc_merge_source_opts, iter)
    {
      lwroc_merge_source_opt *opt =
	PD_LL_ITEM(iter, lwroc_merge_source_opt, _source_opts);

      num_src++;

      if (opt->_conn->_buffer_size == (size_t) -2)
	{
	  sum_buf_frac += opt->_conn->_buffer_frac;
	  num_frac++;
	}
      else if (opt->_conn->_buffer_size != 0)
	{
	  sum_buf_size += opt->_conn->_buffer_size;
	  num_size++;
	}
    }

  /* If there are sources with no fixed size, and no fraction, we need
   * to apply a default.
   */

  num_auto = num_src - num_frac - num_size;

  if (num_auto)
    {
      /* If the remaining space gives less than 1% per input, we warn,
       * and if it gives less then 0.1% we refuse.
       */

      auto_frac = (1 - sum_buf_frac) / (double) num_auto;

      if (auto_frac < 0.001)
	LWROC_BADCFG_FMT("Fraction of input buffer size very small (%.2f %%) "
			 "for automatic (unspecified) sources.  Refusing.",
			 auto_frac * 100.0);
      if (auto_frac < 0.01)
	LWROC_WARNING_FMT("Fraction of input buffer size small (%.2f %%) "
			  "for automatic (unspecified) sources.",
			  auto_frac * 100.0);
    }
  else if (num_frac)
    {
      /* All sources are specified with either size or fraction.
       * In case the fractions do not sum to 1, help the user.
       *
       * But if some sources are specified by size, we still imagine
       * those here.  This to achieve minimum surprise: avoid
       * allocations to change drastically when some sources are
       * changed between using size and fraction.
       */
      rescale_frac = 1. / sum_buf_frac *
	((double) num_frac / (double) (num_frac + num_size));
    }

  /* We cannot have the fixed size over-exhausting the space.
   * (There is a tighter check at the end for 95%, but this avoids
   * negative size for the fractional parts.)
   */

  if (sum_buf_size >= pipe_cfg->_map_size)
    LWROC_BADCFG_FMT("Sum size of fixed input buffers (%" MYPRIzd ") "
		     "exhausts total buffer (%" MYPRIzd ").",
		     sum_buf_size, pipe_cfg->_map_size);

  /* How much size is nominally set aside for inputs? */

  if (pipe_cfg->_input_size == (size_t) -2)
    {
      input_frac = pipe_cfg->_input_frac;
    }
  else
    input_frac = 0.75;

  tot_input_size =
    (size_t) ((double) (pipe_cfg->_map_size - sum_buf_size) * input_frac);

  /* We can now apply the sizes. */

  PD_LL_FOREACH(_lwroc_merge_source_opts, iter)
    {
      lwroc_merge_source_opt *opt =
	PD_LL_ITEM(iter, lwroc_merge_source_opt, _source_opts);

      double use_frac = 0.0;

      if (opt->_conn->_buffer_size == (size_t) -2)
	{
	  use_frac = opt->_conn->_buffer_frac * rescale_frac;

	  /* printf("rescale_frac: %.6f\n",rescale_frac); */

	  if (use_frac < 0.001)
	    LWROC_CBADCFG_FMT(&opt->_conn->_out._base,
			       "Very small effective fraction "
			       "of input buffer size (%.2f %%).  Refusing.",
			       use_frac * 100.0);
	  if (use_frac < 0.01)
	    LWROC_CWARNING_FMT(&opt->_conn->_out._base,
			       "Small effective fraction "
			       "of input buffer size (%.2f %%).",
			       use_frac * 100.0);
	}
      else if (opt->_conn->_buffer_size == 0)
	use_frac = auto_frac;

      if (use_frac != 0.)
	opt->_conn->_buffer_size =
	  (size_t) ((double) tot_input_size * use_frac);

      sum_input_size += opt->_conn->_buffer_size;
    }

  /* Make sure that no input buffer is very small compared to others. */

  PD_LL_FOREACH(_lwroc_merge_source_opts, iter)
    {
      lwroc_merge_source_opt *opt =
	PD_LL_ITEM(iter, lwroc_merge_source_opt, _source_opts);

      double frac =
	(double) opt->_conn->_buffer_size / (double) sum_input_size;

      if (frac < 0.001)
	LWROC_CBADCFG_FMT(&opt->_conn->_out._base,
			  "Very small actual fraction "
			  "of input buffer size (%.2f %%).  Refusing.",
			  frac * 100.0);
      if (frac < 0.01)
	LWROC_CWARNING_FMT(&opt->_conn->_out._base,
			   "Small actual fraction "
			   "of input buffer size (%.2f %%).",
			   frac * 100.0);

      if (opt->_conn->_max_ev_len_config &&
	  opt->_conn->_buffer_size <
	  2 * opt->_conn->_max_ev_len_config)
	LWROC_CBADCFG_FMT(&opt->_conn->_out._base,
			  "Input buffer size %" MYPRIzd " < "
			  "2*max-event-len = 2*%" MYPRIzd " = %" MYPRIzd ".",
			  opt->_conn->_buffer_size,
			  opt->_conn->_max_ev_len_config,
			  2 * opt->_conn->_max_ev_len_config);
    }

  total_input_frac = (double) sum_input_size / (double) pipe_cfg->_map_size;

  if (total_input_frac > 0.95)
    LWROC_BADCFG_FMT("Using %" MYPRIzd " for input buffers, "
		     "of total %" MYPRIzd ", is %.2f %%.  Refusing above 95 %%.",
		     sum_input_size,
		     pipe_cfg->_map_size,
		     total_input_frac * 100.);

  if (total_input_frac < 0.05)
    LWROC_BADCFG_FMT("Using %" MYPRIzd " for input buffers, "
		     "of total %" MYPRIzd ", is %.2f %%.  Refusing below 5 %%.",
		     sum_input_size,
		     pipe_cfg->_map_size,
		     total_input_frac * 100.);

  LWROC_INFO_FMT("Using %" MYPRIzd " for input buffers, "
		 "of total %" MYPRIzd ", is %.2f %%.",
		 sum_input_size,
		 pipe_cfg->_map_size,
		 total_input_frac * 100.);

  PD_LL_FOREACH(_lwroc_merge_source_opts, iter)
    {
      lwroc_merge_source_opt *opt =
	PD_LL_ITEM(iter, lwroc_merge_source_opt, _source_opts);

      double frac =
	(double) opt->_conn->_buffer_size / (double) sum_input_size;

      LWROC_CINFO_FMT(&opt->_conn->_out._base,
		      "Input buffer using %" MYPRIzd ", is %.2f %% of input.",
		      opt->_conn->_buffer_size,
		      frac * 100.0);
    }

  pipe_cfg->_buf_size = pipe_cfg->_map_size - sum_input_size;
}

/********************************************************************/

void lwroc_add_merge_source(lwroc_merge_source_opt *opt)
{
  lwroc_merge_source *conn = opt->_conn;
  uint64_t *timestamp_ptr = NULL;

#if 0
  {
    size_t sz_portmap_msg = lwroc_portmap_msg_serialized_size();
    size_t sz_request_msg = lwroc_request_msg_serialized_size();

    size_t sz = sz_portmap_msg;
    if (sz_request_msg > sz)
      sz = sz_request_msg;

    lwrocmon_create_buffer(&conn->_out._base._buf, sz);
  }
#endif

  /* Data buffer. */

  lwroc_pipe_buffer_init(&conn->_buffer,
			 1, NULL, conn->_buffer_size,
			 0);
  conn->_buffer_consumer =
    lwroc_pipe_buffer_get_consumer(conn->_buffer, 0);
  conn->_force_threshold = (conn->_buffer_size * 9) / 10;

  if (conn->_max_ev_len_config &&
      (_lwroc_merge_info._mode & LWROC_MERGE_MODE_TS))
    {
      uint32_t max_out_ev_size;

      max_out_ev_size =
	lwroc_data_pipe_get_max_ev_len(_lwroc_merge_info._data_handle);

      /* Event size may not be larger than output event size. */

      if (conn->_max_ev_len_config > max_out_ev_size)
	{
	  LWROC_CBADCFG_FMT(&conn->_out._base,
			    "Source max-event-size (%" MYPRIzd ") "
			    "larger than output buffer accepts (%" PRIu32 ").",
			    conn->_max_ev_len_config,
			    max_out_ev_size);
	}
    }

  if (_lwroc_merge_info._mode & LWROC_MERGE_MODE_TS)
    {
      timestamp_ptr = &conn->pre.mrg.stamp._next;
      assert(&conn->pre.mrg.stamp._prev == timestamp_ptr+1);

      /* Not really doing time sorting - no timestamps. */
      if (_lwroc_merge_info._mode & LWROC_MERGE_MODE_TS_PASSTHROUGH)
	timestamp_ptr = NULL;

      /* Make sure we are initialised? */
      conn->pre.mrg.stamp._next = 0;
      conn->pre.mrg.stamp._prev = 0;
    }

  conn->_out._base._select_item.item_info = &lwroc_merge_in_select_item_info;

  /* Not needed list?   Due to client_base... */
  PD_LL_INIT(&conn->_out._base._clients);

  conn->_sticky_store_full = lwroc_lmd_sticky_store_init();
  conn->_sticky_store_val_pending = lwroc_lmd_sticky_store_init();

  /* Source number.  For keeping subevent order fixed. */

  conn->_src_index = _lwroc_merge_info._num_sources++;

  LWROC_CINFO_FMT(&conn->_out._base,
		  "Add source #%" MYPRIzd " (buf=%" MYPRIzd ").",
		  conn->_src_index,
		  conn->_buffer_size);

  if (conn->_type == LWROC_MERGE_IN_TYPE_MBS_TRANS ||
      conn->_type == LWROC_MERGE_IN_TYPE_EBYE_PUSH ||
      conn->_type == LWROC_MERGE_IN_TYPE_EBYE_PULL ||
      conn->_type == LWROC_MERGE_IN_TYPE_FAKERNET ||
      conn->_type == LWROC_MERGE_IN_TYPE_FILE)
    {
      if (conn->_type == LWROC_MERGE_IN_TYPE_MBS_TRANS ||
	  conn->_type == LWROC_MERGE_IN_TYPE_EBYE_PULL ||
	  conn->_type == LWROC_MERGE_IN_TYPE_FAKERNET)
	{
	  conn->_out._state = LWROC_MERGE_IN_STATE_UDP_CREATE;
	  conn->_out._base._fd = -1;
	}
      if (conn->_type == LWROC_MERGE_IN_TYPE_EBYE_PUSH)
	{
	  conn->_out._state = LWROC_MERGE_IN_STATE_EBYE_PUSH_WAIT_CLIENT;
	  conn->_out._base._fd = -1;
	}
      if (conn->_type == LWROC_MERGE_IN_TYPE_EBYE_PUSH ||
	  conn->_type == LWROC_MERGE_IN_TYPE_EBYE_PULL)
	{
	  conn->_max_buffer_size = conn->_buffer_size / 2;
	}
      if (conn->_type == LWROC_MERGE_IN_TYPE_FILE)
	{
	  /* _fd and _state was set at open() at parse time.  */

	  /* Fake the max buffers size (no start message). */

	  switch (_lwroc_merge_info._data_fmt->_buffer_format)
	    {
	    case LWROC_DATA_TRANSPORT_FORMAT_LMD:
	      conn->_max_buffer_size = conn->_buffer_size / 2;
	      break;

	    case LWROC_DATA_TRANSPORT_FORMAT_EBYE:
	    case LWROC_DATA_TRANSPORT_FORMAT_XFER:
	      conn->_max_buffer_size =
		_lwroc_merge_info._data_fmt->_default_net_buffer_size;
	      break;

	    default:
	      LWROC_BUG_FMT("Unhandled file buffer format %x.  "
			    "(Default buffer size.)",
			    _lwroc_merge_info._data_fmt->_buffer_format);
	    }

	  if (!conn->_max_ev_len_config)
	    {
	      conn->_max_ev_len = 1000;
	      conn->_max_ev_len_source = "file default";
	    }
	  else
	    {
	      conn->_max_ev_len = conn->_max_ev_len_config;
	      conn->_max_ev_len_source = "config";
	    }
	}

      /* Insert into the list. */
      PD_LL_ADD_BEFORE(&_lwroc_merge_sources,
		       &conn->_out._base._select_item._items);
    }
  else
    {
      assert(conn->_type == LWROC_MERGE_IN_TYPE_DRASI);

      lwroc_net_outgoing_init(&conn->_out,
			      &lwroc_native_merge_in_select_item_info);
    }

  PD_LL_ADD_BEFORE(&_lwroc_merge_all_sources,
		   &conn->_all_sources);

  conn->_out._base._mon =
    lwroc_net_conn_monitor_init(conn->_type == LWROC_MERGE_IN_TYPE_MBS_TRANS ?
				LWROC_CONN_TYPE_TRANS :
				conn->_type == LWROC_MERGE_IN_TYPE_EBYE_PUSH ?
				LWROC_CONN_TYPE_EBYE_PUSH :
				conn->_type == LWROC_MERGE_IN_TYPE_EBYE_PULL ?
				LWROC_CONN_TYPE_EBYE_PULL :
				conn->_type == LWROC_MERGE_IN_TYPE_FAKERNET ?
				LWROC_CONN_TYPE_FNET :
				conn->_type == LWROC_MERGE_IN_TYPE_FILE ?
				LWROC_CONN_TYPE_FILE :
				LWROC_REQUEST_DATA_TRANS,
				LWROC_CONN_DIR_OUTGOING,
				conn->_out._hostname, conn->_label,
				&conn->_out._base._addr,
				conn->_buffer, NULL,
				timestamp_ptr, NULL, NULL, NULL, 0, 0);
}

void lwroc_add_merge_sources(void)
{
  pd_ll_item *iter;
  size_t sum_max_ev_len_config = 0;

  PD_LL_FOREACH(_lwroc_merge_source_opts, iter)
    {
      lwroc_merge_source_opt *opt =
	PD_LL_ITEM(iter, lwroc_merge_source_opt, _source_opts);
      lwroc_merge_source *conn = opt->_conn;

      lwroc_add_merge_source(opt);

      sum_max_ev_len_config += opt->_conn->_max_ev_len_config;

      if (conn->_out._state == LWROC_MERGE_IN_STATE_FILE_FMT_SWITCH)
	conn->_out._state = lwroc_merge_in_buffer_read_prepare(1);
    }

  if (_lwroc_merge_info._mode & LWROC_MERGE_MODE_EB)
    {
      uint32_t max_out_ev_size;

      max_out_ev_size =
	lwroc_data_pipe_get_max_ev_len(_lwroc_merge_info._data_handle);

      if (sum_max_ev_len_config > max_out_ev_size)
	{
	  LWROC_BADCFG_FMT("Sum of event build sources max-event-size "
			   "(%" MYPRIzd "=0x%" MYPRIzx ") "
			   "larger than output accepts "
			   "(%" PRIu32 "=0x%" PRIx32 ").",
			   sum_max_ev_len_config, sum_max_ev_len_config,
			   max_out_ev_size, max_out_ev_size);
	}
    }
}

/********************************************************************/

void lwroc_merge_init_conn_list(void)
{
  /* Create a dummy connection to make linking easy.
   */

  PD_LL_INIT(&_lwroc_merge_sources);
  PD_LL_INIT(&_lwroc_merge_val_sources);
  PD_LL_INIT(&_lwroc_merge_all_sources);
}

/********************************************************************/

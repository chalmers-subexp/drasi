/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_MERGE_IN_H__
#define __LWROC_MERGE_IN_H__

#include "lwroc_thread_block.h"
#include "lwroc_merge_struct.h"
#include "lwroc_thread_util.h"

#include "../dtc_arch/acc_def/mystdint.h"

void lwroc_merge_init_conn_list(void);
void lwroc_parse_merge_sources(lwroc_data_pipe_cfg *pipe_cfg);
void lwroc_add_merge_sources(void);

extern lwroc_thread_instance *_lwroc_merge_in_thread;
extern lwroc_thread_instance *_lwroc_merge_val_thread;

void lwroc_prepare_merge_in_thread(void);

void lwroc_merge_in_native_connected(lwroc_select_item *item,
				     lwroc_select_info *si);

void lwroc_merge_in_unit_setup(lwroc_merge_source *conn, int server_port);

void lwroc_merge_in_unit_bind(lwroc_merge_in_unit *unit);

#endif/*__LWROC_MERGE_IN_H__*/

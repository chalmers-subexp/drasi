/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_merge_struct.h"
#include "lwroc_merge_in.h"
#include "lwroc_message.h"
#include "lwroc_net_io_util.h"
#include "lwroc_net_io.h"

#include <string.h>

/* The merge_in_unit is responsible for accepting incoming connections.
 *
 * This is used for the ebye-push mode, where the data source connects
 * to the data sink.
 */

/********************************************************************/

const char *
lwroc_merge_in_unit_kind_str(const lwroc_merge_in_unit *unit,
			     int *port)
{
  switch (unit->_conn->_type)
    {
    case LWROC_MERGE_IN_TYPE_EBYE_PUSH:
      *port =
	(unit->_server_port ==
	 LMD_TCP_PORT_EBYE_XFER_PUSH) ? -1 : unit->_server_port;
      return "ebye-push-serv";

    default:
      return "(unknown)";
    }
}

char *lwroc_merge_in_unit_fmt_msg_context(char *buf, size_t size,
					  const  void *ptr)
{
  const lwroc_merge_in_unit *unit = (const lwroc_merge_in_unit *)
    (ptr - offsetof(lwroc_merge_in_unit, _msg_context));
  int port = -1;
  const char *kind_str;

  kind_str = lwroc_merge_in_unit_kind_str(unit, &port);

  if (port == -1)
    return lwroc_message_fmt_msg_context(buf, size,
					 "%s", kind_str);
  else
    return lwroc_message_fmt_msg_context(buf, size,
					 "%s:%d", kind_str, port);
}

/********************************************************************/

void lwroc_merge_in_unit_setup_select(lwroc_select_item *item,
				     lwroc_select_info *si)
{
  lwroc_merge_in_unit *unit =
    PD_LL_ITEM(item, lwroc_merge_in_unit, _select_item);

  lwroc_merge_source *conn = unit->_conn;

  if (unit->_server_fd != -1)
    {
      /* Only accept when connection is free. */
      if (conn->_out._state == LWROC_MERGE_IN_STATE_EBYE_PUSH_WAIT_CLIENT)
	LWROC_READ_FD_SET(unit->_server_fd, si);
    }

  return;
}

int lwroc_merge_in_unit_after_select(lwroc_select_item *item,
				     lwroc_select_info *si)
{
  lwroc_merge_in_unit *unit =
    PD_LL_ITEM(item, lwroc_merge_in_unit, _select_item);

  struct sockaddr_storage cli_addr;
  int client_fd;
  lwroc_merge_source *conn = unit->_conn;

  /* If using multiple ports, e.g. for portmapping, see comments
   * and code in lwroc_net_trans_unit_setup_select()/..._accept().
   */

  if (unit->_server_fd == -1)
    return 1;
  if (!LWROC_READ_FD_ISSET(unit->_server_fd, si))
    return 1;

  client_fd = lwroc_net_io_accept(unit->_server_fd,
				  "merge_in connection", &cli_addr);

  if (client_fd == -1)
    return 1;

  conn->_out._base._fd = client_fd;
  conn->_out._base._addr = cli_addr;

  /* This function gives the source to input thread. */
  lwroc_merge_in_native_connected(&conn->_out._base._select_item, NULL);

  _lwroc_mon_net._ncon_total++;

  LWROC_CINFO(&conn->_out._base, "Accepted connection.");

  return 1;
}

/********************************************************************/

lwroc_select_item_info lwroc_merge_in_unit_select_item_info =
{
  lwroc_merge_in_unit_setup_select,
  lwroc_merge_in_unit_after_select,
  NULL,
  NULL,
  0,
};

/********************************************************************/

void lwroc_merge_in_unit_setup(lwroc_merge_source *conn,
			       int server_port)
{
  lwroc_merge_in_unit *unit =
    (lwroc_merge_in_unit *) malloc (sizeof (lwroc_merge_in_unit));

  if (!unit)
    LWROC_FATAL("Memory allocation failure (merge source unit info).");

  memset (unit, 0, sizeof (lwroc_merge_in_unit));

  unit->_conn = conn;

  if (server_port == -1)
    server_port = LMD_TCP_PORT_EBYE_XFER_PUSH;

  unit->_server_port = server_port;

  unit->_msg_context = lwroc_merge_in_unit_fmt_msg_context;

  /* Insert into the list. */
  PD_LL_ADD_BEFORE(&_lwroc_merge_in_serv,
		   &unit->_servers);
}

void lwroc_merge_in_unit_bind(lwroc_merge_in_unit *unit)
{
  unit->_server_fd =
    lwroc_net_io_socket_bind(unit->_server_port, 1, "ebye/xfer-push socket",
			     1 /* reuseaddr */);

  LWROC_CINFO_FMT(unit,
		  "Bound EBYE/XFER push sink to port %d.",
		  unit->_server_port);

  unit->_select_item.item_info =
    &lwroc_merge_in_unit_select_item_info;

  PD_LL_ADD_BEFORE(&_lwroc_net_clients, &unit->_select_item._items);
}

/********************************************************************/

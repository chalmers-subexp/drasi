/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_MERGE_INPUT_TYPE_H__
#define __LWROC_MERGE_INPUT_TYPE_H__

#define LWROC_MERGE_IN_TYPE_DRASI       1
#define LWROC_MERGE_IN_TYPE_MBS_TRANS   2
#define LWROC_MERGE_IN_TYPE_EBYE_PUSH   3
#define LWROC_MERGE_IN_TYPE_EBYE_PULL   4
#define LWROC_MERGE_IN_TYPE_FAKERNET    5
#define LWROC_MERGE_IN_TYPE_FILE        6

#endif/*__LWROC_MERGE_INPUT_TYPE_H__*/

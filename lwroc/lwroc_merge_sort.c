/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_merge_struct.h"
#include "lwroc_merge_sort.h"
#include "lwroc_merge_in.h"
#include "lwroc_merge_analyse.h"
#include "lwroc_message.h"
#include "lwroc_message_internal.h"
#include "lwroc_mon_block.h"
#include "lwroc_optimise.h"
#include "lmd/lwroc_lmd_titris_stamp.h"
#include "lmd/lwroc_lmd_white_rabbit_stamp.h"
#include "lwroc_array_heap.h"
#include "lwroc_net_trigbus.h"
#include "lwroc_triva_control.h"
#include "lwroc_main_iface.h"
#include "lwroc_data_pipe.h"
#include "lmd/lwroc_lmd_util.h"
#include "lwroc_delayed_eb.h"
#include "lwroc_timeouts.h"
#include "lwroc_timestamp_marks.h"
#include "lwroc_mon_block.h"
#include "lwroc_control.h"
#include "lwroc_leap_seconds.h"

#include "gen/lwroc_merge_request_serializer.h"

#include <assert.h>
#include <string.h>
#include "../dtc_arch/acc_def/myinttypes.h"
#include "../dtc_arch/acc_def/myprizd.h"
#include "../dtc_arch/acc_def/fall_through.h"

/********************************************************************/

int lwroc_fetch_titris_stamp(const lwroc_merge_source *conn,
			     const uint32_t *subev_data,
			     uint32_t subev_data_length,
			     uint64_t *timestamp,
			     const uint64_t *prevstamp,
			     uint32_t *branch_id)
{
  uint32_t error_branch_id;
  uint32_t ts_l16;
  uint32_t ts_m16;
  uint32_t ts_h16;

  if (subev_data_length < 4 * sizeof (uint32_t))
    {
      lwroc_message_rate_ctrl(LWROC_MESSAGE_RATE_CTRL_LIMIT);
      LWROC_CERROR_FMT(&conn->_out._base,
		       "First subevent payload smaller than "
		       "TITRIS-style timestamp "
		       "(%" PRIu32 " < %" MYPRIzd ").  "
		       "Sorting as time 0.",
		       subev_data_length, 4 * sizeof (uint32_t));
      /* Make sure this source gets sorted as the next one. */
      goto bad_subev_return;
    }

  error_branch_id = subev_data[0];
  ts_l16 = subev_data[1];
  ts_m16 = subev_data[2];
  ts_h16 = subev_data[3];

  if ((ts_l16 & TITRIS_STAMP_LMH_ID_MASK) != TITRIS_STAMP_L16_ID ||
      (ts_m16 & TITRIS_STAMP_LMH_ID_MASK) != TITRIS_STAMP_M16_ID ||
      (ts_h16 & TITRIS_STAMP_LMH_ID_MASK) != TITRIS_STAMP_H16_ID)
    {
      lwroc_message_rate_ctrl(LWROC_MESSAGE_RATE_CTRL_LIMIT);
      LWROC_CERROR_FMT(&conn->_out._base,
		       "TITRIS time stamp word has wrong marker.  "
		       "(ID: 0x%08x) (0x%08x 0x%08x 0x%08x)  "
		       "Sorting as time 0.",
		       error_branch_id, ts_l16, ts_m16, ts_h16);
      goto bad_subev_return;
    }

  *branch_id =
    (error_branch_id &
     TITRIS_STAMP_EBID_BRANCH_ID_MASK) >>
    TITRIS_STAMP_EBID_BRANCH_ID_SHIFT;

  if (error_branch_id & TITRIS_STAMP_EBID_UNUSED)
    LWROC_CWARNING_FMT(&conn->_out._base,
		       "Unused bits set in "
		       "TITRIS time stamp branch ID word: 0x%08x "
		       "(full: 0x%08x).",
		       error_branch_id & TITRIS_STAMP_EBID_UNUSED,
		       error_branch_id);

  if (error_branch_id & TITRIS_STAMP_EBID_ERROR)
    {
      lwroc_message_rate_ctrl(LWROC_MESSAGE_RATE_CTRL_LIMIT);
      LWROC_CERROR_FMT(&conn->_out._base,
		       "Error marker set in "
		       "TITRIS time stamp branch ID word: 0x%08x "
		       "(full: 0x%08x).  Sorting as time 0.",
		       error_branch_id & TITRIS_STAMP_EBID_ERROR,
		       error_branch_id);
      /* Make sure this source gets sorted as the next one. */
      goto zero_ts_return;
    }

  *timestamp =
    (             ts_l16 & TITRIS_STAMP_LMH_TIME_MASK)         |
    (            (ts_m16 & TITRIS_STAMP_LMH_TIME_MASK)  << 16) |
    (((uint64_t) (ts_h16 & TITRIS_STAMP_LMH_TIME_MASK)) << 32);

  if (*timestamp < *prevstamp)
    LWROC_CWARNING_FMT(&conn->_out._base,
		       "TITRIS time stamp less than previous.  "
		       "(ID: 0x%08x) (0x%" PRIx64 " < 0x%" PRIx64 ")",
		       error_branch_id, *timestamp, *prevstamp);
  /*
  lwroc_merge_analyse_add_timestamp(conn->_src_index,
				    (error_branch_id &
				     TITRIS_STAMP_EBID_BRANCH_ID_MASK) >>
				    TITRIS_STAMP_EBID_BRANCH_ID_SHIFT,
				    *timestamp);
  */
  return 1;

 bad_subev_return:
  *branch_id = (uint32_t) -1;
 zero_ts_return:
  *timestamp = 0;
  return 1;
}

/********************************************************************/

/* Keep current time value to not have to get it for every event.
 * Is updated every time a timestamp seems to be way off.
 */
struct timeval _lwroc_merge_ts_check_now = { 0, 0 };

int lwroc_fetch_white_rabbit_stamp(const lwroc_merge_source *conn,
				   const uint32_t *subev_data,
				   uint32_t subev_data_length,
				   uint64_t *timestamp,
				   const uint64_t *prevstamp,
				   uint32_t *branch_id,
				   uint32_t *sync_check)
{
  uint32_t error_branch_id;
  uint32_t ts_ll16;
  uint32_t ts_lh16;
  uint32_t ts_hl16;
  uint32_t ts_hh16;

  if (subev_data_length < 5 * sizeof (uint32_t))
    {
      lwroc_message_rate_ctrl(LWROC_MESSAGE_RATE_CTRL_LIMIT);
      LWROC_CERROR_FMT(&conn->_out._base,
		       "First subevent payload smaller than "
		       "White Rabbit-style timestamp "
		       "(%" PRIu32 " < %" MYPRIzd ").  "
		       "Sorting as time 0.",
		       subev_data_length, 4 * sizeof (uint32_t));
      /* Make sure this source gets sorted as the next one. */
      goto bad_subev_return;
    }

  error_branch_id = subev_data[0];
  ts_ll16 = subev_data[1];
  ts_lh16 = subev_data[2];
  ts_hl16 = subev_data[3];
  ts_hh16 = subev_data[4];

  /*
  printf ("(ID: 0x%08x) (0x%08x 0x%08x 0x%08x 0x%08x)\n",
	  error_branch_id, ts_ll16, ts_lh16, ts_hl16, ts_hh16);
  */

  if ((ts_ll16 & WHITE_RABBIT_STAMP_LH_ID_MASK) != WHITE_RABBIT_STAMP_LL16_ID||
      (ts_lh16 & WHITE_RABBIT_STAMP_LH_ID_MASK) != WHITE_RABBIT_STAMP_LH16_ID||
      (ts_hl16 & WHITE_RABBIT_STAMP_LH_ID_MASK) != WHITE_RABBIT_STAMP_HL16_ID||
      (ts_hh16 & WHITE_RABBIT_STAMP_LH_ID_MASK) != WHITE_RABBIT_STAMP_HH16_ID)
    {
      lwroc_message_rate_ctrl(LWROC_MESSAGE_RATE_CTRL_LIMIT);
      LWROC_CERROR_FMT(&conn->_out._base,
		       "White Rabbit time stamp word has wrong marker.  "
		       "(ID: 0x%08x) (0x%08x 0x%08x 0x%08x 0x%08x)  "
		       "Sorting as time 0.",
		       error_branch_id, ts_ll16, ts_lh16, ts_hl16, ts_hh16);
      goto bad_subev_return;
    }

  *branch_id =
    (error_branch_id &
     WHITE_RABBIT_STAMP_EBID_BRANCH_ID_MASK) >>
    WHITE_RABBIT_STAMP_EBID_BRANCH_ID_SHIFT;

  if (error_branch_id & WHITE_RABBIT_STAMP_EBID_UNUSED)
    LWROC_CWARNING_FMT(&conn->_out._base,
		       "Unused bits set in "
		       "White Rabbit time stamp branch ID word: 0x%08x "
		       "(full: 0x%08x).",
		       error_branch_id & WHITE_RABBIT_STAMP_EBID_UNUSED,
		       error_branch_id);

  if (error_branch_id & WHITE_RABBIT_STAMP_EBID_ERROR)
    {
      lwroc_message_rate_ctrl(LWROC_MESSAGE_RATE_CTRL_LIMIT);
      LWROC_CERROR_FMT(&conn->_out._base,
		       "Error marker set in "
		       "White Rabbit time stamp branch ID word: 0x%08x "
		       "(full: 0x%08x).  Sorting as time 0.",
		       error_branch_id & WHITE_RABBIT_STAMP_EBID_ERROR,
		       error_branch_id);
      /* Make sure this source gets sorted as the next one. */
      goto zero_ts_return;
    }

  *timestamp =
    (             ts_ll16 & WHITE_RABBIT_STAMP_LH_TIME_MASK)         |
    (            (ts_lh16 & WHITE_RABBIT_STAMP_LH_TIME_MASK)  << 16) |
    (((uint64_t) (ts_hl16 & WHITE_RABBIT_STAMP_LH_TIME_MASK)) << 32) |
    (((uint64_t) (ts_hh16 & WHITE_RABBIT_STAMP_LH_TIME_MASK)) << 48);

  /* Fake the source to be late?  To Check stall detector.
  *timestamp +=
    conn->_src_index * 40 * (uint64_t) 1000000000;
  */

  if (*timestamp < *prevstamp)
    LWROC_CWARNING_FMT(&conn->_out._base,
		       "White Rabbit time stamp less than previous.  "
		       "(ID: 0x%08x) (0x%" PRIx64 " < 0x%" PRIx64 ")",
		       error_branch_id, *timestamp, *prevstamp);

  if (*timestamp >= LWROC_MERGE_TIMESTAMP_MARK_BEGIN)
    {
      /* The highest few values are used for special marks.
       * So do not let them into the flow.
       */
      *timestamp = LWROC_MERGE_TIMESTAMP_MARK_BEGIN - 1;
    }

  if (_config._merge_ts_wr_check)
    {
      int64_t timestamp_s = (int64_t) (*timestamp / 1000000000);

      /* UTC and TAI timescales are pretty close.  But TAI typically
       * has a larger time (+ 37 leap seconds 2024), so cannot
       * diagnose having a UTC timestamp.  (Monitor shows that though.)
       */

      /* Timestamps from the future are not possible.
       *
       * Old timestamps could happen if things get stalled in the data
       * pipelines before us.  So that can only be checked if they
       * have given us some guarantees, which is required if the
       * timestamp disable timeout has been set.  Since our purpose is
       * to find broken timestamps, not to be nit-picky, we allow a
       * further factor of 10 before considering the timestamps broken.
       */

      if (timestamp_s > ((int64_t) _lwroc_merge_ts_check_now.tv_sec + 5) ||
	  (conn->_ts_disable_timeout &&
	   timestamp_s < ((int64_t) _lwroc_merge_ts_check_now.tv_sec -
			  conn->_ts_disable_timeout * 10)))
	{
	  /* Before considering it an error, make sure the current time
	   * is up-to-date.
	   */

	  int leap;

	  gettimeofday(&_lwroc_merge_ts_check_now, NULL);

	  leap = lwroc_get_leap_seconds(_lwroc_merge_ts_check_now.tv_sec);

	  _lwroc_merge_ts_check_now.tv_sec += leap;

	  /* printf ("re-check\n");  fflush(stdout); */

	  if (timestamp_s > ((int64_t) _lwroc_merge_ts_check_now.tv_sec +5) ||
	      (conn->_ts_disable_timeout &&
	       timestamp_s < ((int64_t) _lwroc_merge_ts_check_now.tv_sec -
			      conn->_ts_disable_timeout * 10)))
	    {
	      lwroc_message_rate_ctrl(LWROC_MESSAGE_RATE_CTRL_LIMIT);
	      LWROC_CERROR_FMT(&conn->_out._base,
			       "White Rabbit time stamp way off.  "
			       "(ID: 0x%08x) (0x%" PRIx64 " = "
			       "now + %" PRId64 " s)  "
			       "Sorting as time 0.",
			       error_branch_id, *timestamp,
			       (int64_t) (timestamp_s -
					  (int64_t) _lwroc_merge_ts_check_now.
					  /* */tv_sec));
	      /* Make sure this source gets sorted as the next one. */
	      goto zero_ts_return;
	    }
	}
    }

  /* Timestamp unpacking was successful.
   * See if the next word is available and might be a sync check value.
   */

  if (subev_data_length >= 6 * sizeof (uint32_t))
    {
      uint32_t sc;

      sc = subev_data[5];

      if ((sc & SYNC_CHECK_MAGIC_MASK) == SYNC_CHECK_MAGIC)
	*sync_check = sc;
    }

  /*
  lwroc_merge_analyse_add_timestamp(conn->_src_index,
				    (error_branch_id &
				     WHITE_RABBIT_STAMP_EBID_BRANCH_ID_MASK) >>
				    WHITE_RABBIT_STAMP_EBID_BRANCH_ID_SHIFT,
				    *timestamp);
  */

  /*
  printf ("(ID: 0x%08x) (0x%08x 0x%08x 0x%08x 0x%08x) %" PRIx64 "\n",
	  error_branch_id, ts_ll16, ts_lh16, ts_hl16, ts_hh16,
	  *timestamp);
  */

  return 1;

 bad_subev_return:
  *branch_id = (uint32_t) -1;
 zero_ts_return:
  *timestamp = 0;
  return 1;
}

/********************************************************************/

void lwroc_merge_done_event(lwroc_merge_source *conn)
{
  conn->_event_10_1_context_set = 0;

  lwroc_pipe_buffer_did_read(conn->_buffer_consumer,
			     conn->pre.event_size);

  conn->pre.ptr += conn->pre.event_size;
  conn->pre.avail -= conn->pre.event_size;
}

/********************************************************************/

void lwroc_merge_report_timestamps(void)
{
  /* The sources are kept in a heap, which means that order is random.
   * Not nice for user to inspect.  We do the brute-force approach, of
   * looping over the number of configured sources and printing those
   * that are active.
   */

  pd_ll_item *iter;
  size_t i;

  /* Find out smallest prev time stamp, so we can report differences
   * too.
   */

  uint64_t reference = (uint64_t) -1;

  if (_lwroc_merge_info._last_good_sort)
    reference = _lwroc_merge_info._last_good_sort;
  else
    for (i = 0; i < _lwroc_merge_info._num_active_sources; i++)
      {
	lwroc_merge_source *conn = _lwroc_merge_info._sources[i];

	if (conn->pre.mrg.stamp._prev < reference)
	  reference = conn->pre.mrg.stamp._prev;
      }

  /* First print all active sources. */
  /* This all_sources list does not change, so prints in order. */
  PD_LL_FOREACH(_lwroc_merge_all_sources, iter)
    {
      lwroc_merge_source *conn =
	PD_LL_ITEM(iter, lwroc_merge_source, _all_sources);

      for (i = 0; i < _lwroc_merge_info._num_active_sources; i++)
	if (_lwroc_merge_info._sources[i] == conn)
	  {
	    /* In principle INFO, but since this info is badly needed
	     * for debugging when this report is made...
	     * Let's make it easy for the user.
	     */
	    if (conn->pre.mrg.stamp._next < LWROC_MERGE_TIMESTAMP_MARK_BEGIN)
	      LWROC_CWARNING_FMT(&conn->_out._base,
				 "Prev stamp 0x%016" PRIx64" (%+8" PRId64 "), "
				 "next: 0x%016" PRIx64" (%+8" PRId64 ").",
				 conn->pre.mrg.stamp._prev,
				 (int64_t) (conn->pre.mrg.stamp._prev -
					    reference),
				 conn->pre.mrg.stamp._next,
				 (int64_t) (conn->pre.mrg.stamp._next -
					    reference));
	    else
	      LWROC_CWARNING_FMT(&conn->_out._base,
				 "Prev stamp 0x%016" PRIx64" (%+8" PRId64 "), "
				 "next: -.",
				 conn->pre.mrg.stamp._prev,
				 (int64_t) (conn->pre.mrg.stamp._prev -
					    reference));
	  }
    }

  /* Then print the inactive. */
  PD_LL_FOREACH(_lwroc_merge_all_sources, iter)
    {
      lwroc_merge_source *conn =
	PD_LL_ITEM(iter, lwroc_merge_source, _all_sources);

      for (i = 0; i < _lwroc_merge_info._num_active_sources; i++)
	if (_lwroc_merge_info._sources[i] == conn)
	  goto next_source;

      LWROC_CWARNING(&conn->_out._base,
		     "Not active/connected.");
    next_source:
      ;
    }
}

/********************************************************************/

void lwroc_merge_delay_stall_detector(void)
{
  gettimeofday(&_lwroc_merge_info._future_stall_detector_after, NULL);
  _lwroc_merge_info._future_stall_detector_after.tv_sec += 2;
}

#define LWROC_FETCH_EVENT_FAILURE       0
#define LWROC_FETCH_EVENT_SUCCESS       1
#define LWROC_FETCH_EVENT_IDENT         2
#define LWROC_FETCH_EVENT_WOULD_BLOCK   3
#define LWROC_FETCH_EVENT_TIMEOUT       4
#define LWROC_FETCH_EVENT_QUIT          5
#define LWROC_FETCH_EVENT_MISMATCH      6 /* Not from fetch itself. */

int lwroc_merge_wait_avail_data(lwroc_merge_source *conn,
				const lwroc_thread_block *thread_notify,
				int wait)
{
  /* Remove waste space. */

  /*printf ("%p: A\n", conn);*/

  /* We just set to zero here, since this is executed often.
   * Actual value is taken when we have to wait...
   */
  int report_interval = 0;

  for ( ; ; )
    {
      /*printf ("%p: D\n", conn);*/

      if (conn->pre.avail)
	break;

      /*printf ("%p: E\n", conn);*/

      /* Need to fetch some data. */

      conn->pre.avail = lwroc_pipe_buffer_avail_read(conn->_buffer_consumer,
						     thread_notify,
						     &conn->pre.ptr,
						     NULL, 0, 0);

      /*printf ("%p: F\n", conn);*/

      if (!conn->pre.avail)
	{
	  LFENCE; /* Make sure the read below is after whatever got us here. */
	  if (conn->_status_out_of_data)
	    {
	      /* printf ("%p: Got out-of-data marker.\n", conn); */

	      LWROC_CINFO(&conn->_out._base,
			  "Got out-of-data marker.");

	      /* There is no more data available, and the input has
	       * marked the source as corrupt!
	       *
	       * It is to be removed from the active lists, and
	       * returned to the input for connection reestablishment.
	       */

	      return LWROC_FETCH_EVENT_FAILURE;
	    }
	  /*printf ("%p: G (%p)\n", conn, conn->_buffer);*/
	  /* We will wait until this source has something to report.
	   */
	  if (wait <= 0)
	    return LWROC_FETCH_EVENT_WOULD_BLOCK;

	  if (_lwroc_main_thread->_terminate &&
	      ((_lwroc_merge_info._mode & LWROC_MERGE_MODE_TS) ||
	       _config._merge_no_validate))
	    return LWROC_FETCH_EVENT_QUIT;

	  /*printf ("%p: G1 (%p)\n", conn, conn->_buffer);*/

	  if (_lwroc_merge_info._mode & LWROC_MERGE_MODE_TS)
	    {
	      struct timeval now;
	      struct timeval waited;
	      struct timeval last_got;
	      struct timeval next_report;
	      struct timeval timeout;

	      /* We have no data, and are in time-sorter mode.
	       *
	       * If this persists for some time, then try to diagnose
	       * the situation.
	       */

	      /* Make a copy since it is volatile. */
	      /* It may actually be updated in-between.  But that
	       * is not a major problem, as we then just has a
	       * fractional second error.
	       */
	      last_got = conn->_last_got_data;

	      gettimeofday(&now, NULL);

	      /* Protect against reversing system clock. */
	      if (timercmp(&now, &last_got, <))
		conn->_last_got_data = last_got = now;

	      if (timerisset(&conn->_first_bad_stamp))
		{
		  /* We have had bad timestamps.  They get converted
		   * to 0, so will sort before all other sources.
		   * Thus it can starve the entire sorting process.
		   *
		   * Starving happens since we would then only sort
		   * this source with 0 timestamps, as this is always
		   * the next in line.
		   *
		   * We avoid that by detecting that we are waiting
		   * for data for this source.  If this has been going
		   * on for too long (about a second), then we instead
		   * give this source a dummy late time, such that it
		   * does not prevent sorting other sources.  It will
		   * periodically be checked for data by the same
		   * mechanism that tries to resurrect sources that
		   * deliver no data at all (see just below).
		   */

		  timersub(&now, &conn->_first_bad_stamp, &waited);

		  if (conn->_ts_disable_timeout &&
		      waited.tv_sec >=
		      LWROC_MERGE_BROKEN_SOURCE_SEMI_IGNORE_TIMEOUT)
		    {
		      if (conn->_disabled_nodata == 0)
			{
			  /* Since we get back here often if the
			   * source continues to deliver broken
			   * timestamps, only report first time.
			   */
			  lwroc_message_rate_ctrl
			    (LWROC_MESSAGE_RATE_CTRL_LIMIT);
			  LWROC_CERROR(&conn->_out._base,
				       "Broken timestamps (sort as 0) - "
				       "semi-ignoring source "
				       "(= ignore when empty).");

			  conn->_disabled_at = now.tv_sec;
			}
		      conn->pre.mrg.stamp._next =
			LWROC_MERGE_TIMESTAMP_BROKEN;
		      conn->_disabled_nodata = LWROC_MERGE_DISABLED_BROKEN;
		      /* Setup a check if we got (better) data.  See
		       * note below at LWROC_MERGE_DISABLED_NODATA in
		       * this function.
		       */
		      _lwroc_merge_info._check_nodata_sources = 1;
		      return LWROC_FETCH_EVENT_TIMEOUT;
		    }
		}

	      timersub(&now, &last_got, &waited);

	      if (!report_interval)
		{
		  report_interval = _config._merge_ts_nodata_warn_timeout;
		  /*
		  printf ("%d %d\n",
			  report_interval,
			  LWROC_MERGE_SORT_STUCK_TIMEOUT);
		  */
		}
	      else if (waited.tv_sec >= report_interval ||
		       (conn->_ts_disable_timeout &&
			waited.tv_sec >= conn->_ts_disable_timeout))
		{
		  /* Time to report! */

		  LWROC_CWARNING_FMT(&conn->_out._base,
				     "Waited %d seconds for timesorter data.",
				     (int) waited.tv_sec);

		  lwroc_merge_report_timestamps();

		  /*
		  printf ("%d %lld xxx\n",
			  report_interval,
			  (long long) waited.tv_sec);
		  */
		  while (waited.tv_sec >= report_interval)
		    {
		      report_interval *= 2;
		      /* Protection against stupidity. */
		      /* Happens if time goes crazy.  Then we'd multiply
		       * up to extreme values, then go negative and
		       * finally 0, (and then loop indefinitely).
		       */
		      if (report_interval <= 0)
			{
			  report_interval = 0;
			  break;
			}
		    }

		  if (conn->_ts_disable_timeout &&
		      waited.tv_sec >= conn->_ts_disable_timeout)
		    {
		      LWROC_CERROR(&conn->_out._base,
				   "No data - disabling timesorter source.");
		      conn->pre.mrg.stamp._next =
			LWROC_MERGE_TIMESTAMP_DISABLED;
		      conn->_disabled_nodata = LWROC_MERGE_DISABLED_NODATA;
		      conn->_disabled_at = now.tv_sec;
		      /* Setup a check if we got data.  Prevents any race
		       * condition with data having become available just
		       * after we timed out here, but input was faster
		       * than to notice the disable mark.
		       */
		      _lwroc_merge_info._check_nodata_sources = 1;
		      return LWROC_FETCH_EVENT_TIMEOUT;
		    }
		}

	      next_report = last_got;

	      if (report_interval < conn->_ts_disable_timeout)
		next_report.tv_sec += report_interval;
	      else
		next_report.tv_sec += conn->_ts_disable_timeout;

	      timersub(&next_report, &now, &timeout);

	      /* In case we already passed the time. */
	      if (timeout.tv_sec < 0)
		timerclear(&timeout);

	      /*printf ("%p: G2 (%p), %d\n", conn, conn->_buffer,
		(int) timeout.tv_sec);*/

	      lwroc_thread_block_get_token_timeout(thread_notify, &timeout);

	      /*printf ("%p: G3 (%p), %d\n", conn, conn->_buffer,
		(int) timeout.tv_sec);*/
	    }
	  else
	    lwroc_thread_block_get_token(thread_notify);

	  continue;
	}

      /*printf ("%p: H\n", conn);*/
    }

  if (report_interval &&
      report_interval != _config._merge_ts_nodata_warn_timeout)
    {
      /* Tell that we are unstuck. */
      LWROC_CWARNING(&conn->_out._base,
		     "Got timesorter data.");
      /* Do not fire the stall detector immediately. */
      lwroc_merge_delay_stall_detector();
    }

  return LWROC_FETCH_EVENT_SUCCESS;
}


int lwroc_merge_fetch_event_header(lwroc_merge_source *conn,
				   const lwroc_thread_block *thread_notify,
				   lmd_event_header_host *header,
				   uint32_t **data,
				   int wait)
{
  lmd_event_header_host *header_ptr;
  int ret;

  *data = NULL; /* Keep compiler happy. */

  ret = lwroc_merge_wait_avail_data(conn, thread_notify, wait);

  if (ret != LWROC_FETCH_EVENT_SUCCESS)
    return ret;

  /*
  printf ("%p Got data: %" MYPRIzd "  ptr: %p\n",
	  conn, conn->pre.avail, conn->pre.ptr);
  */
  /* Data is available.  Is it a complete event?
   * (Fragmented events are handled by the input thread.)
   */

  /* We believe to have an event. */

  if (conn->pre.avail < sizeof (lmd_event_10_1_host))
    {
      LWROC_CERROR_FMT(&conn->_out._base,
		       "Input buffer smaller than event header "
		       "(%" MYPRIzd " < %" MYPRIzd ").",
		       conn->pre.avail, sizeof (lmd_event_header_host));
      /* This input channel is broken! */
      return LWROC_FETCH_EVENT_FAILURE;
    }

  header_ptr = (lmd_event_header_host *) conn->pre.ptr;
  memcpy(header, header_ptr, sizeof (*header));

  conn->pre.event_size = (uint32_t) sizeof (lmd_event_header_host) +
    (uint32_t) EVENT_DATA_LENGTH_FROM_DLEN(header->l_dlen);
  conn->pre.sync_check = 0; /* No value fetched from data. */

  if (conn->pre.event_size < sizeof (lmd_event_10_1_host))
    {
      LWROC_CERROR_FMT(&conn->_out._base,
		       "Event smaller than 10/1 header "
		       "(%" PRIu32 " < %" MYPRIzd ").",
		       conn->pre.event_size, sizeof (lmd_event_10_1_host));
      return LWROC_FETCH_EVENT_FAILURE;
    }

  if (conn->pre.avail < conn->pre.event_size)
    {
      /* If we handle fragmented events, we may have to get it in
       * multiple chunks here.  Note: there may also be space that
       * we have to skip...
       */

      /* For the moment: not! */

      LWROC_CERROR_FMT(&conn->_out._base,
		       "Event size larger than input buffer "
		       "(%d < %" MYPRIzd ").",
		       conn->pre.event_size, conn->pre.avail);
      /* printf ("bad ev size: %p (%p)\n", conn, conn->_buffer); */
      return LWROC_FETCH_EVENT_FAILURE;
    }

  *data = (uint32_t *) (header_ptr+1);

  return LWROC_FETCH_EVENT_SUCCESS;
}

int lwroc_merge_fetch_ebye_event(lwroc_merge_source *conn,
				 int wait)
{
  /* Only time-sorting, no event-building.
   *
   * Strategy: entries of kind 4 and 5 (parts of timestamps) are
   * directly eaten and update the mid and hi timestamp parts.
   *
   * Other entries are treated together, as far as they have the same
   * lo timestamp.  That may mean that several hits get handled
   * together, but that is harmess.  With the same timestamp, they
   * would end up adjacent anyhow.
   */

  for ( ; ; )
    {
      uint32_t t_lo, t_mid, t_hi;
      uint32_t recent_info;
      uint32_t hit_size;
      int had_non_info;
      int ret;

      if (!conn->pre.avail)
	{
	  ret = lwroc_merge_wait_avail_data(conn, _lwroc_main_thread->_block,
					    wait);

	  if (ret != LWROC_FETCH_EVENT_SUCCESS)
	    return ret;

	  if (conn->pre.avail < 2 * sizeof (uint32_t))
	    {
	      LWROC_CERROR_FMT(&conn->_out._base,
			       "Input buffer smaller than "
			       "minimum EBYE item size "
			       "(%" MYPRIzd " < %" MYPRIzd ").",
			       conn->pre.avail, 2 * sizeof (uint32_t));
	      /* This input channel is broken! */
	      return LWROC_FETCH_EVENT_FAILURE;
	    }
	}

      /* For now we do not make any attempts at removing redundant
       * timestamp information.  That is made almost useless anyhow
       * by the fact that asic and caen hits use different time-scales
       * (factor of 4).
       *
       * Process data double-words as the same hit while the low (w1)
       * timestamp is the same.  Also break processing if we hit an
       * info item after a non-info item, or if the info-item sequence
       * is known to be a complete sequence (sync100, trig).
       */

#define RECENT_INFO_1(code1, high1) \
  (((high1) << 4) | ((code1)))
#define RECENT_INFO_2(code1, high1, code2, high2) \
  ((RECENT_INFO_1(code1, high1) << 8) | \
   (RECENT_INFO_1(code2, high2)     ))

      recent_info = 0;
      had_non_info = 0;
      hit_size = 0;
      t_lo = t_mid = t_hi = 0;

      for ( ; ; )
	{
	  uint32_t w0 = ((uint32_t*) (conn->pre.ptr + hit_size))[0];
	  uint32_t w1 = ((uint32_t*) (conn->pre.ptr + hit_size))[1];

	  uint32_t w0_type = (w0 >> 30) & 0x03;

	  uint32_t w1_ts   = (w1      ) & 0x0fffffff;

	  if (hit_size &&
	      w1_ts != t_lo)
	    break; /* Different low timestamp.  Break hit. */

	  t_lo = w1_ts;

	  if (w0_type == 0x2) /* info */
	    {
	      uint32_t w0_code   = (w0 >> 20) & 0x0f;
	      uint32_t w0_field  = (w0      ) & 0x000fffff;

	      uint32_t w1_high   = (w1 >> 28) & 0x0f;

	      /* TODO: this would break for trace data, since that has
	       * raw data words.
	       */
	      if (had_non_info)
		break; /* Info after non-info.  Break hit. */

	      recent_info <<= 8;
	      recent_info |= ((w1_high << 4) | w0_code);

	      if      ( w0_code == 5   && w1_high == 1) /* ASIC */
		t_hi  = w0_field;
	      else if ((w0_code == 4 ||
			w0_code == 7 ||
			w0_code == 14 ||
			w0_code == 15) && w1_high == 1) /* ASIC */
		t_mid = w0_field;
	      else if ((w0_code == 4 ||
			w0_code == 5)  && w1_high == 0) /* CAEN */
		t_mid = w0_field;
	      else if ( w0_code == 7   && w1_high == 0) /* FEBEX */
		t_mid = w0_field;
	      else if ( w0_code == 8   && w1_high == 0) /* FEBEX */
		t_hi  = w0_field;
	      else
		{
		  /*
		  LWROC_WARNING_FMT("Unknown info code (c%d,h%d).",
				    w0_code, w1_high);
		  */
		}
	    }
	  else
	    {
	      had_non_info = 1;
	    }

	  /* printf ("%08x %08x : %08x\n", w0, w1, recent_info); */

	  /* Include also this data item in the hit ('event'). */
	  hit_size += (uint32_t) (2 * sizeof (uint32_t));

	  if (recent_info == RECENT_INFO_2(5,1,4,1) ||
	      recent_info == RECENT_INFO_2(5,1,14,1) ||
	      recent_info == RECENT_INFO_2(5,1,15,1))
	    {
	      /* printf ("-\n"); */
	      break;
	    }

	  if (hit_size + 2 * sizeof (uint32_t) > conn->pre.avail)
	    break; /* Not enough data for another double-word. */
	}

      /* printf ("%08x\n", recent_info); */

      conn->pre.mrg.stamp._next = (             t_lo |
				   (((uint64_t) t_mid) << 28) |
				   (((uint64_t) t_hi) << 48));

      if (recent_info == RECENT_INFO_1(4,0))
	{
	  /* CAEN style hits are stored in units of 4 ns. */
	  conn->pre.mrg.stamp._next *= 4;
	}

      conn->pre.event_size = hit_size;

      if (conn->pre.mrg.stamp._next < conn->pre.mrg.stamp._prev)
	{
	  if (_lwroc_merge_info._mode != LWROC_MERGE_MODE_EBYE_UNSORTED)
	    {
	      uint32_t w0 = ((uint32_t*) (conn->pre.ptr + hit_size))[0];
	      uint32_t w1 = ((uint32_t*) (conn->pre.ptr + hit_size))[1];

	      LWROC_CWARNING_FMT(&conn->_out._base,
				 "EBYE time stamp less than previous.  "
				 "(0x%08x 0x%08x ...) "
				 "(0x%" PRIx64 " < 0x%" PRIx64 ")",
				 w0, w1,
				 conn->pre.mrg.stamp._next,
				 conn->pre.mrg.stamp._prev);
	    }
	}

      if (conn->pre.mrg.stamp._next >= LWROC_MERGE_TIMESTAMP_MARK_BEGIN)
	{
	  /* The highest few values are used for special marks. */
	  conn->pre.mrg.stamp._next = LWROC_MERGE_TIMESTAMP_MARK_BEGIN - 1;
	}

      /*
      printf ("@%016" PRIx64" Hit: %d bytes\n",
	      conn->pre.mrg.stamp._next,
	      conn->pre.event_size);
      */
      break;
    }

  return LWROC_FETCH_EVENT_SUCCESS;
}

int lwroc_merge_fetch_xfer_block(lwroc_merge_source *conn,
				 int wait)
{
  ebye_xfer_header *xfer;
  uint32_t s;
  int ret;

  /* Not even time-sorting, we fetch one xfer block. */

  if (!conn->pre.avail)
    {
      ret = lwroc_merge_wait_avail_data(conn, _lwroc_main_thread->_block,
					wait);

      if (ret == LWROC_FETCH_EVENT_WOULD_BLOCK)
	{
	  /* This is typical in xfer mode, since we emit data as soon
	   * as it is available.  I.e. most of the time there will be
	   * no new data available.
	   */
	  if (conn->_disabled_nodata == 0)
	    {
	      struct timeval now;
	      gettimeofday(&now, NULL);

	      conn->pre.mrg.stamp._next =
		LWROC_MERGE_TIMESTAMP_WAITDATA;
	      conn->_disabled_nodata = LWROC_MERGE_DISABLED_WAITDATA;
	      conn->_disabled_at = now.tv_sec;
	    }
	  /* See comment at LWROC_MERGE_DISABLED_NODATA in
	   * lwroc_merge_wait_avail_data().
	   */
	  _lwroc_merge_info._check_nodata_sources = 1;
	  return ret;
	}

      if (ret != LWROC_FETCH_EVENT_SUCCESS)
	return ret;
    }

  if (conn->pre.avail < sizeof (ebye_xfer_header))
    {
      LWROC_CBUG_FMT(&conn->_out._base,
		     "Input buffer smaller than "
		     "XFER entry (block header) "
		     "(%" MYPRIzd " < %" MYPRIzd ").",
		     conn->pre.avail, sizeof (ebye_xfer_header));
      /* This input channel is broken! */ /* Unreachable due to bug above. */
      return LWROC_FETCH_EVENT_FAILURE;
    }

  xfer = (ebye_xfer_header *) conn->pre.ptr;

  /* As some kind of (internal) sanity check, verify the magic words. */
  if (!(xfer->_id1 == htonl(EBYE_XFER_HEADER_ID1) &&
	xfer->_id2 == htonl(EBYE_XFER_HEADER_ID2)))
    {
      /* Bug, since it was checked by input thread. */
      LWROC_CBUG_FMT(&conn->_out._base,
		     "Bad XFER id1:2 "
		     "(got %08x:%08x, expected %08x:%08x).",
		     ntohl(xfer->_id1), ntohl(xfer->_id2),
		     EBYE_XFER_HEADER_ID1, EBYE_XFER_HEADER_ID2);
    }

  /* The actual block size. */
  s = ntohl(xfer->_block_length);

  if (conn->pre.avail < s)
    {
      LWROC_CBUG_FMT(&conn->_out._base,
		     "Incomplete XFER entry (one block) in input buffer "
		     "(%" MYPRIzd " bytes available < %d).",
		     conn->pre.avail, s);
    }

  /* The entire buffer block is what we got. */
  conn->pre.event_size = s;

  /* Dummy timestamp. */
  conn->pre.mrg.stamp._next = 0;

  /* This function needs to be implemented. */
  return LWROC_FETCH_EVENT_SUCCESS;
}

int lwroc_merge_fetch_event(lwroc_merge_source *conn, int wait)
{
  lmd_event_10_1_host *header = &conn->event_10_1;
  lmd_event_info_host *info_ptr;
  uint32_t *data;
  int ret;

  data = NULL; /* Silence compiler. */

  if (_lwroc_merge_info._mode & LWROC_MERGE_MODE_EBYE)
    return lwroc_merge_fetch_ebye_event(conn, wait);
  if (_lwroc_merge_info._mode & LWROC_MERGE_MODE_XFER)
    return lwroc_merge_fetch_xfer_block(conn, wait);

 fetch_again:
  ret = lwroc_merge_fetch_event_header(conn, _lwroc_main_thread->_block,
				       &(header->_header), &data, wait);

  if (ret != LWROC_FETCH_EVENT_SUCCESS)
    return ret;

  if (header->_header.i_type    != LMD_EVENT_10_1_TYPE ||
      header->_header.i_subtype != LMD_EVENT_10_1_SUBTYPE)
    {
      if (header->_header.i_type    == LMD_EVENT_STICKY_TYPE ||
	  header->_header.i_subtype == LMD_EVENT_STICKY_SUBTYPE)
	{
	  _lwroc_merge_info._sticky_pending = 1;
	}
      else if (header->_header.i_type    == LWROC_LMD_IDENT_TYPE &&
	       header->_header.i_subtype == LWROC_LMD_IDENT_SUBTYPE)
	{
	  if (_lwroc_merge_info._mode & LWROC_MERGE_MODE_TS)
	    {
	      lwroc_merge_done_event(conn);
	      goto fetch_again;
	    }
	  return LWROC_FETCH_EVENT_IDENT;
	}
      else
	{
	  LWROC_CERROR_FMT(&conn->_out._base,
			   "Event has unknown type/subtype (%d/%d).",
			   header->_header.i_type,
			   header->_header.i_subtype);
	  return LWROC_FETCH_EVENT_FAILURE;
	}
    }

  /* The header has been set (some while ago...) */
  conn->_event_10_1_context_set |= LWROC_MERGE_SOURCE_EVENT_10_1_SET_HEADER;

  /* Get the rest of the header. */
  info_ptr = (lmd_event_info_host *) data;
  memcpy(&(header->_info), info_ptr, sizeof (header->_info));

  data = (uint32_t *) (info_ptr+1);

  conn->_event_10_1_context_set |= LWROC_MERGE_SOURCE_EVENT_10_1_SET_INFO;

  /*
  printf ("%p Fetch event, total size: %" PRIu32 ", eventno: %" PRIu32 "\n",
	  conn, conn->pre.event_size,
	  header._info.l_count);
  */

  /* Now things depend on what kind of sorting or merging we are doing.
   *
   * If we are sorting on some kind of time-stamp, we need to extract
   * that.  If we are to merge (subevents / a.k.a. fragments), we just
   * keep track of the event header.
   */

  if (_lwroc_merge_info._mode & LWROC_MERGE_MODE_TS)
    {
      uint32_t data_size =
	conn->pre.event_size - (uint32_t) sizeof (lmd_event_10_1_host);
      lmd_subevent_10_1_host *subev_header;
      uint32_t *subev_data;
      uint32_t subev_data_length;
      uint32_t subev_size;

      /* We need to find ourselves the first subevent.
       *
       * It will be: directly under our nose!
       */

      if (data_size < sizeof (lmd_subevent_10_1_host))
	{
	  if (data_size == 0)
	    {
	      /* Event is completely empty.
	       * This it at least in some sense legal, so allow it.
	       * With a complaint.
	       */
	      lwroc_message_rate_ctrl(LWROC_MESSAGE_RATE_CTRL_LIMIT);
	      LWROC_CERROR(&conn->_out._base,
			   "No timestamp - event payload empty.  "
			   "Sorting as time 0.");
	      conn->pre.mrg.stamp._next = 0;
	      goto timestamp_set;
	    }
	  LWROC_CERROR_FMT(&conn->_out._base,
			   "No timestamp - event payload smaller than "
			   "subevent header size "
			   "(%" PRIu32 " < %" MYPRIzd ").",
			   data_size, sizeof (lmd_subevent_10_1_host));
	  /* We could also think about setting this event to
	   * having a broken timestamp and do immediate dump of it.
	   * But...  Let's hard alert the user to fix their data!
	   */
	  return LWROC_FETCH_EVENT_FAILURE;
	}

      subev_header = (lmd_subevent_10_1_host *) data;

      subev_data_length =
	SUBEVENT_DATA_LENGTH_FROM_DLEN(subev_header->_header.l_dlen);

      subev_size =
	(uint32_t) sizeof (lmd_subevent_10_1_host) + subev_data_length;

      if (subev_size > data_size)
	{
	  LWROC_CERROR_FMT(&conn->_out._base,
			   "Subevent size larger than event payload "
			   "(%" PRIu32 " < %" PRIu32 ").",
			   data_size, subev_size);
	  return LWROC_FETCH_EVENT_FAILURE;
	}

      subev_data = (uint32_t *) (subev_header+1);

      /* Now that we have a subevent, try to get the timestamp. */

      if (_lwroc_merge_info._mode & LWROC_MERGE_MODE_TS)
	{
	  if ((_lwroc_merge_info._mode & LWROC_MERGE_MODE_TITRIS) &&
	      !lwroc_fetch_titris_stamp(conn,
					subev_data, subev_data_length,
					&conn->pre.mrg.stamp._next,
					&conn->pre.mrg.stamp._prev,
					&conn->pre.mrg.stamp._branch_id))
	    return LWROC_FETCH_EVENT_FAILURE;

	  if ((_lwroc_merge_info._mode & LWROC_MERGE_MODE_WHITE_RABBIT) &&
	      !lwroc_fetch_white_rabbit_stamp(conn,
					      subev_data, subev_data_length,
					      &conn->pre.mrg.stamp._next,
					      &conn->pre.mrg.stamp._prev,
					      &conn->pre.mrg.stamp._branch_id,
					      &conn->pre.sync_check))
	    return LWROC_FETCH_EVENT_FAILURE;

	  if (conn->pre.mrg.stamp._branch_id != (uint32_t) -1 &&
	      conn->_ts_id_mask &&
	      !(conn->_ts_id_mask &
		(((uint64_t) 1) << conn->pre.mrg.stamp._branch_id)))
	    {
	      LWROC_CERROR_FMT(&conn->_out._base,
			       "Unexpected timestamp ID %d=0x%02x "
			       "(expect mask 0x%" PRIu64 ").",
			       conn->pre.mrg.stamp._branch_id,
			       conn->pre.mrg.stamp._branch_id,
			       conn->_ts_id_mask);
	    }

	  if (conn->pre.mrg.stamp._next != 0)
	    {
	      /* Timestamp is good.  It is cheaper to just clear, than
	       * to check if we need to clear.
	       */

	      timerclear(&conn->_first_bad_stamp);

	      /* Had we reported this? */

	      if (conn->_disabled_nodata == LWROC_MERGE_DISABLED_BROKEN)
		{
		  conn->_disabled_nodata = 0;
		  lwroc_message_rate_ctrl(LWROC_MESSAGE_RATE_CTRL_LIMIT);
		  LWROC_CWARNING(&conn->_out._base,
				 "Got non-broken data - sorting as usual.");
		}
	    }
	  else if (!timerisset(&conn->_first_bad_stamp))
	    {
	      struct timeval now;

	      gettimeofday(&now, NULL);

	      /* Remember that we have had bad timestamps from now. */

	      conn->_first_bad_stamp = now;
	    }
	}

      /*
      printf ("Fetch event next time: 0x%016" PRIu64 "\n",
	      conn->pre.mrg.stamp._next);
      */

    timestamp_set:
      ;
    }

  return LWROC_FETCH_EVENT_SUCCESS;
}

/********************************************************************/

/*
 * Emergency work-around function used with qsort.
 */
/*
int lwroc_merge_sources_next_stamp_compare(const void *p1,
					   const void *p2)
{
  const lwroc_merge_source **conn1 = (const lwroc_merge_source **) p1;
  const lwroc_merge_source **conn2 = (const lwroc_merge_source **) p2;

  if ((*conn1)->pre.mrg.stamp._next < (*conn2)->pre.mrg.stamp._next)
    return -1;
  if ((*conn1)->pre.mrg.stamp._next > (*conn2)->pre.mrg.stamp._next)
    return 1;
  if ((*conn1)->_src_index < (*conn2)->_src_index)
    return -1;
  return 1;
}
*/

int lwroc_merge_sources_next_stamp_compare_less(lwroc_merge_source *conn1,
						lwroc_merge_source *conn2,
						void *extra_ptr)
{
  (void) extra_ptr;

  if (conn1->pre.mrg.stamp._next < conn2->pre.mrg.stamp._next)
    return 1;
  if (conn1->pre.mrg.stamp._next == conn2->pre.mrg.stamp._next &&
      conn1->_src_index < conn2->_src_index)
    return 1;
  return 0;
}

/* TODO: disable the verifiction once we trust things again... */
/* or better: check on emitted data that it is sorted... */
void lwroc_merge_sort_verify_heap(void)
{
  size_t i;
  size_t entries = _lwroc_merge_info._num_active_sources;
  lwroc_merge_source **array = _lwroc_merge_info._sources;
  /*
  for (i = 0; i < entries; i++)
    LWROC_INFO_FMT("%zd: %" PRIu64 " ",
		   i, array[i]->pre.mrg.stamp._next);
  */
  for (i = 0; i < entries; i++)
    {
      size_t child1 = i*2+1;
      size_t child2 = child1+1;

      if (child1 < entries &&
	  !lwroc_merge_sources_next_stamp_compare_less(array[i],
						       array[child1],
						       NULL))
	LWROC_BUG_FMT("Heap out of order "
		      "%" MYPRIzd " vs child1 %" MYPRIzd ".", i, child1);
      if (child2 < entries &&
	  !lwroc_merge_sources_next_stamp_compare_less(array[i],
						       array[child2],
						       NULL))
	LWROC_BUG_FMT("Heap out of order "
		      "%" MYPRIzd " vs child2 %" MYPRIzd ".", i, child2);
    }
  /* LWROC_INFO(" "); */
}

/********************************************************************/

void lwroc_merge_update_active(void)
{
  if (_lwroc_merge_info._mode & LWROC_MERGE_MODE_EB)
    {
      int all_present;

      all_present = (_lwroc_merge_info._num_active_sources >=
		     _lwroc_merge_info._num_sources);

      lwroc_data_pipes_readout_active(all_present);
    }

  if (_lwroc_merge_info._mode & LWROC_MERGE_MODE_TS)
    {
      int any_present;

      any_present =
	!!_lwroc_merge_info._num_active_sources;

      lwroc_data_pipes_readout_active(any_present);
    }

}

/********************************************************************/

void lwroc_merge_report_active_sources(lwroc_merge_source *conn,
				       int add)
{
  /* size_t i; */

  LWROC_CINFO_FMT(&conn->_out._base,
		  "Active sources: %" MYPRIzd "/%" MYPRIzd " "
		 "(%s #%" MYPRIzd ")",
		 _lwroc_merge_info._num_active_sources,
		 _lwroc_merge_info._num_sources,
		 add == 1 ? "added" : "removed", conn->_src_index);
}

/********************************************************************/

void lwroc_merge_insert_source(lwroc_merge_source *conn)
{
  /* size_t i; */

  /* lwroc_merge_report_active_sources(1,conn->_src_index); */
  /*
  printf ("active sources: %" MYPRIzd "/%" MYPRIzd " "
	  "(insert %" MYPRIzd ":%p)\n",
	  _lwroc_merge_info._num_active_sources,
	  _lwroc_merge_info._num_sources,
	  conn->_src_index,
	  conn);
  for (i = 0; i < _lwroc_merge_info._num_active_sources; i++)
    printf ("%" MYPRIzd ": %p (#%" MYPRIzd ")\n",
	    i, _lwroc_merge_info._sources[i],
	    _lwroc_merge_info._sources[i]->_src_index);
  */

  if (_lwroc_merge_info._mode & LWROC_MERGE_MODE_EB)
    {
      /* We are merging.  Subevent order should always be the same;
       * insert the source in the correct location.
       */

      size_t insert_at = _lwroc_merge_info._num_active_sources;
      _lwroc_merge_info._num_active_sources++;

      while (insert_at > 0 &&
	     _lwroc_merge_info._sources[insert_at-1]->_src_index >
	     conn->_src_index)
	{
	  _lwroc_merge_info._sources[insert_at] =
	    _lwroc_merge_info._sources[insert_at-1];
	  insert_at--;
	}
      _lwroc_merge_info._sources[insert_at] = conn;
    }

  if (_lwroc_merge_info._mode & LWROC_MERGE_MODE_TS)
    {
      /* We are sorting, we should insert the new item at the
       * right position in the heap.
       *
       * The heap property is that each parent node has a smaller
       * timestamp than its two children.  We may have to
       * bubble this item up.
       */

      LWROC_HEAP_INSERT(lwroc_merge_source *,
			_lwroc_merge_info._sources,
			_lwroc_merge_info._num_active_sources,
			lwroc_merge_sources_next_stamp_compare_less,
			conn, NULL);

      lwroc_merge_sort_verify_heap();
    }

  lwroc_merge_report_active_sources(conn, 1);
  /*
  printf ("active sources: %" MYPRIzd "/%" MYPRIzd "\n",
	  _lwroc_merge_info._num_active_sources,
	  _lwroc_merge_info._num_sources);
  for (i = 0; i < _lwroc_merge_info._num_active_sources; i++)
    printf ("%" MYPRIzd ": %p (#%" MYPRIzd ")\n",
	    i, _lwroc_merge_info._sources[i],
	    _lwroc_merge_info._sources[i]->_src_index);
  */

  conn->_source_add_removed |= LWROC_MERGE_SOURCE_STATUS_ADDED;
  _lwroc_merge_info._sticky_update_pending = 1; /* Need to check. */
  /* If it was removed earlier, that no longer matters.  Still listed though. */
  /* conn->_source_add_removed &= ~LWROC_MERGE_SOURCE_STATUS_REMOVED; */

  lwroc_merge_update_active();
}

void lwroc_merge_resort_first_source(void)
{
  /* When we have picked the data of the smallest stamp event,
   * and have gotten its next timestamp, it may no longer be
   * the smallest.  Move it down in the heap to the place
   * where it now belongs.
   */

  /*
  LWROC_INFO_FMT("insert %" PRIu64 "",
		 _lwroc_merge_info._sources[0]->pre.mrg.stamp._next);
  */

  LWROC_HEAP_MOVE_DOWN(lwroc_merge_source *,
		       _lwroc_merge_info._sources,
		       _lwroc_merge_info._num_active_sources,
		       lwroc_merge_sources_next_stamp_compare_less,
		       0, NULL);

  /*
  qsort(_lwroc_merge_info._sources,
	_lwroc_merge_info._num_active_sources,
	sizeof (_lwroc_merge_info._sources[0]),
	lwroc_merge_sources_next_stamp_compare);
  */

  lwroc_merge_sort_verify_heap();

  /* Note!
   *
   * In case we are operating a (semi)-unstable sorting, e.g. by
   * some mixed-mode handling of multiple time-scales, where the
   * order of items may change as more information comes with new
   * events from other sources, we may have to recover the heap.
   *
   * Basically: when new sorting information becomes available, we
   * need to run a 'heapify' over the heap to ensure it is ordered.
   * Note that this still will be very cheap, as we expect
   * the items to still be in almost order.  Only items that
   * have become unordered will cause swapping.
   */
}

/********************************************************************/

void lwroc_merge_eject_source(lwroc_merge_source *conn, int ret)
{
  if (conn->_type == LWROC_MERGE_IN_TYPE_FILE)
    {
      /* A file source that fails will be set to
       * LWROC_MERGE_IN_STATE_FAILED_FILE by the input thread.
       * That state is then permanent.
       * This means that we have one source less.
       */
      _lwroc_merge_info._num_finished_sources++;
      /* Set up a check for having no sources left at all. */
      _lwroc_merge_info._check_nodata_sources = 1;
    }
  else
    {
      /* Failed file is handled as stop for that source, thus this
       * check does not apply to files.
       */
      if (_config._merge_no_validate &&
	  _lwroc_merge_info._num_sources > 1)
	LWROC_CFATAL(&conn->_out._base,
		     "Source failed and no validation requested: "
		     "is fatal with > 1 source.");
    }

  if (ret == LWROC_FETCH_EVENT_IDENT)
    LWROC_ITEM_BUFFER_INSERT(_lwroc_merge_info._redo_val_sources, conn);
  else
    {
      LWROC_ITEM_BUFFER_INSERT(_lwroc_merge_info._dead_sort_sources, conn);
      conn->_source_remove_reconnect = 1;
    }

  /* No harm in adding if it was an immediate connect.  There will
   * just be no events to revoke.
   */
  if (!(conn->_source_add_removed & LWROC_MERGE_SOURCE_STATUS_REMOVE_LISTED))
    {
      _lwroc_merge_info._sources_removed[_lwroc_merge_info.
					 _num_removed_sources++] = conn;
    }
  conn->_source_add_removed |=
    /* LWROC_MERGE_SOURCE_STATUS_REMOVED | */
    LWROC_MERGE_SOURCE_STATUS_REMOVE_LISTED;
  _lwroc_merge_info._sticky_update_pending = 1; /* Need to check. */

  lwroc_merge_update_active();
}

void lwroc_merge_remove_source(lwroc_merge_source *conn,
			       size_t srcind, int ret)
{
  /* size_t i; */

  if (_lwroc_merge_info._mode & LWROC_MERGE_MODE_EB)
    {
      /* Event building keeps sources in order, so move all
       * later sources one step ahead.
       */

      _lwroc_merge_info._num_active_sources--;
      for ( ; srcind < _lwroc_merge_info._num_active_sources; srcind++)
	_lwroc_merge_info._sources[srcind] =
	  _lwroc_merge_info._sources[srcind+1];
    }
  else
    {
      size_t replace_by = --_lwroc_merge_info._num_active_sources;
      /* Source failed.  Remove from the list. */
      _lwroc_merge_info._sources[srcind] =
	_lwroc_merge_info._sources[replace_by];

      /* When we are in sorting mode, we will be followed by a call
       * to lwroc_merge_resort_first_source, which will move the
       * replacement source, now [0], to the correct position in the heap.
       */

      /* Except if we removed a disabled source, which then was
       * somewhere at the end...  Then we need to reheapify.
       * That is handled by the caller.
       */
    }

  /* We have removed it.  Let the input stage reconnect. */

  lwroc_merge_report_active_sources(conn, -1);
  /*
  printf ("active sources: %" MYPRIzd "/%" MYPRIzd " "
	  "(removed %" MYPRIzd ":%p)\n",
	  _lwroc_merge_info._num_active_sources,
	  _lwroc_merge_info._num_sources,
	  conn->_src_index,
	  conn);
  for (i = 0; i < _lwroc_merge_info._num_active_sources; i++)
    printf ("%" MYPRIzd ": %p (#%" MYPRIzd ")\n",
	    i, _lwroc_merge_info._sources[i],
	    _lwroc_merge_info._sources[i]->_src_index);
  */

  lwroc_merge_eject_source(conn, ret);
}

int lwroc_merge_fetch_event_wrap(lwroc_merge_source *conn, size_t srcind,
				 int wait, int *removed)
{
  int ret = lwroc_merge_fetch_event(conn, wait);

  if (ret == LWROC_FETCH_EVENT_SUCCESS)
    {
      return ret;
    }
  else if (ret == LWROC_FETCH_EVENT_TIMEOUT ||
	   (ret == LWROC_FETCH_EVENT_WOULD_BLOCK && wait <= 0))
    {
      if (wait < 0 &&
	  conn->_disabled_nodata)
	{
	  LWROC_CERROR(&conn->_out._base,
		       "Disabled source reconnection "
		       "by user control request.");
	  goto remove_source;
	}

      return ret;
    }

 remove_source:
  lwroc_merge_remove_source(conn, srcind, ret);
  if (removed)
    *removed = 1;
  return ret;
}

void lwroc_merge_try_fetch_missing(void)
{
  size_t i;
  int ret;
  int got_any = 0;
  int wait = 0;

  if (_lwroc_merge_info._try_reconnect_disabled_sources)
    wait = -1;

  /* Loop backwards, in case sources are ejected. */
  for (i = _lwroc_merge_info._num_active_sources; i; )
    {
      lwroc_merge_source *conn = _lwroc_merge_info._sources[--i];

      if (conn->pre.mrg.stamp._next == LWROC_MERGE_TIMESTAMP_DISABLED ||
	  conn->pre.mrg.stamp._next == LWROC_MERGE_TIMESTAMP_WAITDATA ||
	  conn->pre.mrg.stamp._next == LWROC_MERGE_TIMESTAMP_BROKEN)
	{
	  /* got_any gets marked in case of removal too. */
	  ret = lwroc_merge_fetch_event_wrap(conn, i, wait, &got_any);

	  if (ret == LWROC_FETCH_EVENT_SUCCESS)
	    {
	      if (conn->_disabled_nodata == LWROC_MERGE_DISABLED_NODATA)
		{
		  conn->_disabled_nodata = 0;
		  LWROC_CWARNING(&conn->_out._base,
				 "Got data - re-enabling timesorter source.");
		}
	      if (conn->_disabled_nodata == LWROC_MERGE_DISABLED_WAITDATA)
		{
		  conn->_disabled_nodata = 0;
		  /* This happens all the time, so no messages! */
		}
	      got_any = 1;
	    }
	}
    }

  if (got_any)
    {
      /* We have changed the time of at least one source in the array,
       * which is a heap.  The heap property must be restored.
       *
       * Since this happens seldom, we'll take the somewhat
       * inefficient approach of reinserting every item (except the
       * first) in the heap again.
       *
       * I.e., this is a heapify operation.
       */

      for (i = 1; i < _lwroc_merge_info._num_active_sources; )
	{
	  /* i++ is done by LWROC_HEAP_INSERT */

	  lwroc_merge_source *conn = _lwroc_merge_info._sources[i];

	  LWROC_HEAP_INSERT(lwroc_merge_source *,
			    _lwroc_merge_info._sources,
			    i,
			    lwroc_merge_sources_next_stamp_compare_less,
			    conn, NULL);
	}
    }

  lwroc_merge_sort_verify_heap();
}

void lwroc_merge_revalidate_all_sources(void)
{
  size_t i;

  for (i = 0; i < _lwroc_merge_info._num_active_sources; i++)
    {
      lwroc_merge_source *conn = _lwroc_merge_info._sources[i];

      LWROC_ITEM_BUFFER_INSERT(_lwroc_merge_info._redo_val_sources, conn);
    }
  _lwroc_merge_info._num_active_sources = 0;
}

/********************************************************************/

extern lwroc_monitor_main_block  _lwroc_mon_main;
extern lwroc_mon_block          *_lwroc_mon_main_handle;

/********************************************************************/

/* We are called to update the output state just before the next
 * event, in case some source has been removed, added or
 * removed+added.
 *
 * For time sorting, we can consider the removed+added case to be very
 * seldom - if a source is removed, there will likely be events from
 * others before it is re-added.  And thus its removal will be dealt
 * with before the addition.
 *
 * Removal then just means to for each active sticky subevent
 * producing a revoke sticky subevent.  This also applies to merge
 * event mode.
 *
 * a) If the source actually was removed, i.e. lost connection: In the
 *    list or current stickies, revoke all, and clear the current list.
 *
 *    This is a bit overkill, if the source is actually added again
 *    right after.  But is less complicated and therefore preferable.
 *    Sources that are re-added in event merge mode have usually not
 *    lost connection, but just went another cycle in the validation
 *    thread.  They are not affected by this operation.
 *
 * For event-merging, separate removal and addition is just the same
 * as for time sorting.  The special case is removed+added without any
 * events in between.  In this mode, this can happen often - when even
 * merging is dependent on some source (master, or in the TRIVA case:
 * all sources), then it will return with no events in between.
 *
 * For this case, we would like to avoid churning sticky events, i.e.
 * if there is no change, we should neither revoke nor reinsert the same
 * sticky subevent.  This can be achieved thus:
 *
 * a) Verify that all stickies in the active store are active.
 *
 * b) Merge the list if stickies from the validation thread.  That
 *    would have the set of stickies, as stickies are sent before the
 *    ident marker.
 *
 *    If an sticky subevent is the same as an old one, mark it as
 *    active directly.
 *
 *    If an sticky subevent is new, then mark as inactive.
 *
 * c) Now write all (inactive) stickies to the output.
 *
 * d) Remove revokes from the list of active stickies.
 */

#define LWROC_MERGE_DEBUG_STICKY  0

void lwroc_merge_revoke_sticky(lwroc_merge_source *conn)
{
  size_t revoke_size;

  /* This flag may be written asynchronously by the validation thread.
   * Harmless, the worst that can happen is that we get called
   * multiple times.  But as we clear the sticky store, repeated calls
   * will have no effect.  Since _sticky_store_full is managed only by
   * the merging thread, that does not cause any race conditions.
   */

  if (!(conn->_source_remove_reconnect))
    return;

#if LWROC_MERGE_DEBUG_STICKY
  printf ("revoke-sticky: %zd (%x)\n",
	  conn->_src_index, conn->_source_add_removed);
#endif

  /* Hmmm, what if this source has been sent back to reconnect, and
   * reconnected already.  And also spent some time in the validation
   * thread.  Then it may have played around with the sticky store already?
   *
   * We actually also need to ensure that we and the validation
   * thread do not both access the sticky store at the same time!
   */

  /* We can get a list of the stickies of this source be
   * investigating its sticky store.  We will not touch the data,
   * but easiest to first compact its metadata.
   */

  lwroc_lmd_sticky_store_compact_meta(conn->_sticky_store_full);
  lwroc_lmd_sticky_store_populate_hash(conn->_sticky_store_full);

  /* How many sticky subevents does it have that are active?
   * How much space is needed.
   */

  revoke_size =
    lwroc_lmd_sticky_store_size_revoke(conn->_sticky_store_full);

#if LWROC_MERGE_DEBUG_STICKY
  printf ("revoke-sz: %zd\n", revoke_size);
#endif

  if (revoke_size)
    {
      char *dest;
      size_t total_size;

      /* TODO: need to ensure that we do not request too much
       * space, or it has to be ensured that the amount of active
       * sticky events cannot be larger than what we can request.
       */

      /* In any case, revokes for subevents should not be merged
       * into one event, as that may then create an event that is
       * unusually large (even though there is no payload, only
       * headers.)
       */

      dest = lwroc_request_event_space(_lwroc_merge_info._data_handle,
				       (uint32_t) revoke_size);

      /* Create revoke events for all active sticky subevents.
       * Also marks them as inactive.
       */

      total_size =
	lwroc_lmd_sticky_store_write_revoke(conn->_sticky_store_full,
					    dest);

      assert(total_size <= revoke_size);

      lwroc_used_event_space(_lwroc_merge_info._data_handle,
			     (uint32_t) total_size,
			     0 /* no flush */);
    }

  /* We revoked all stickies, so there are no active sticky events. */

  lwroc_lmd_sticky_store_clear(conn->_sticky_store_full);

  conn->_source_remove_reconnect = 0;
}

void lwroc_merge_pending_sticky_update(void)
{
  size_t i;

  /* We do the sticky update due to reconnected or disconnected
   * sources as late as possible, i.e. right before the next real
   * event.
   *
   * The rationale is to introduce as little churn of sticky events as
   * possible.  E.g. in case the trigger system detected a
   * malfunction, sources will be removed, and likely added again.
   * Unchanged.  Then we should not revoke all stickies just to add
   * them back again.
   */

#if LWROC_MERGE_DEBUG_STICKY
  printf ("*** lwroc_merge_pending_sticky_update\n");
#endif

  /* There is no need to have sticky events in order.  We walk over
   * the sources, and deal with anyone having an issue.
   */

  /* For source that have been reconnected (i.e. recycled all the way
   * back to reconnect), revoke all stickies and clear the list of
   * active stickies.
   *
   * We must also investigate sources that are marked as added, since
   * the reconnect may have been ordered by the validation thread
   * after we dealt with removals.
   */

  for (i = 0; i < _lwroc_merge_info._num_removed_sources; i++)
    {
      lwroc_merge_source *conn =
	_lwroc_merge_info._sources_removed[i];

      conn->_source_add_removed &=
	~(LWROC_MERGE_SOURCE_STATUS_REMOVE_LISTED);

      lwroc_merge_revoke_sticky(conn);
    }

  _lwroc_merge_info._num_removed_sources = 0;

  for (i = 0; i < _lwroc_merge_info._num_active_sources; i++)
    {
      lwroc_merge_source *conn =
	_lwroc_merge_info._sources[i];

      lwroc_merge_revoke_sticky(conn);
    }

  /* All revokes have been performed.  Now do additions.  Revokes are
   * done first - will be needed when we support the case that some
   * stickies are supplied by multiple sources.
   */

  /* Sources that have been (re)-added need to inject any stickies
   * that they have seen earlier.
   *
   * As we keep track of active subevents, only those that are
   * inactive (i.e. not currently in the output stream) need to be
   * sent.
   */

  for (i = 0; i < _lwroc_merge_info._num_active_sources; i++)
    {
      size_t sticky_iter_ev_offset;
      size_t need_size;
      size_t total_size;
      lwroc_merge_source *conn =
	_lwroc_merge_info._sources[i];

      if (!(conn->_source_add_removed & LWROC_MERGE_SOURCE_STATUS_ADDED))
	continue;

      conn->_source_add_removed &= ~LWROC_MERGE_SOURCE_STATUS_ADDED;

#if LWROC_MERGE_DEBUG_STICKY
      printf ("update-added: %zd (%x)\n",
	      conn->_src_index, conn->_source_add_removed);
#endif

      /* Before emitting sticky events, it makes sense to first apply
       * the pending sticky events to the real store (as inactive
       * events).  They may update existing events.  Only then we emit
       * the still surviving (inactive) events.
       *
       * Note that there may be a pending revoke event.  This needs
       * to be propagated to the full store, such that the revoke is
       * emitted.  However, the full store need not retain revoke
       * events, so it is cleaned after emitting whatever needs to be.
       */

      lwroc_lmd_sticky_store_compact_data(conn->_sticky_store_val_pending);
      /* No need to compact the source metadata. */

      lwroc_lmd_sticky_store_apply(conn->_sticky_store_full,
				   conn->_sticky_store_val_pending,
				   0 /* as_inactive */);
      /* Clear the pending store. */
      lwroc_lmd_sticky_store_clear(conn->_sticky_store_val_pending);

      /* As we will emit event data, first make it compact . */
      lwroc_lmd_sticky_store_compact_data(conn->_sticky_store_full);
      lwroc_lmd_sticky_store_compact_meta(conn->_sticky_store_full);
      lwroc_lmd_sticky_store_populate_hash(conn->_sticky_store_full);

      /* The sticky store shall never be larger then our buffer.
       * TODO: ensure this!
       * Thus we can write all data in one go.
       * Hmm, that actually requires that buffers (from net/file) writing
       * are never larger than half the buffer, and that the sticky store
       * is at most half the buffer.  Such that we are not prevented
       * from getting space do to waste space being prevented due to
       * trying to fill up an incomplete buffer.
       */

      sticky_iter_ev_offset = 0;

      /* Actually doing it one event at a time. */
      /* Get size of first inactive event. */
      total_size =
	lwroc_lmd_sticky_store_write_inactive(conn->_sticky_store_full,
					      NULL, 0,
					      &need_size,
					      &sticky_iter_ev_offset);
      assert(total_size == 0);

      while (need_size)
	{
	  char *dest;
	  size_t alloc_size = need_size;

	  dest = lwroc_request_event_space(_lwroc_merge_info._data_handle,
					   (uint32_t) alloc_size);

	  total_size =
	    lwroc_lmd_sticky_store_write_inactive(conn->_sticky_store_full,
						  dest, alloc_size,
						  &need_size,
						  &sticky_iter_ev_offset);
	  assert(total_size <= alloc_size);

	  lwroc_used_event_space(_lwroc_merge_info._data_handle,
				 (uint32_t) total_size,
				 0 /* no flush */);
	}

      /* Finally, remove the revoke events. */
      lwroc_lmd_sticky_store_remove_revokes(conn->_sticky_store_full);
    }

  _lwroc_merge_info._sticky_update_pending = 0;
}

int lwroc_merge_event(void)
{
  lwroc_merge_source *first;
  size_t i;
  int mismatch = 0;
  int flush = 0;
  uint32_t total_size;
  int sticky = _lwroc_merge_info._sticky_pending;

  /* If we have some sticky event pending, then it must be handled
   * first.  Since normal events always are present in all sources.
   */

  if (sticky)
    {
      /* Find which sticky event to handle first.  They are to be
       * handled in event order, and then sorted by trigger number.
       */

      first = NULL;

      for (i = 0; i < _lwroc_merge_info._num_active_sources; i++)
	{
	  lwroc_merge_source *conn =
	    _lwroc_merge_info._sources[i];

	  if (conn->event_10_1._header.i_type    == LMD_EVENT_STICKY_TYPE &&
	      conn->event_10_1._header.i_subtype == LMD_EVENT_STICKY_SUBTYPE)
	    if (!first ||
		conn->event_10_1._info.l_count <
		 first->event_10_1._info.l_count ||
		(conn->event_10_1._info.l_count ==
		 first->event_10_1._info.l_count &&
		 conn->event_10_1._info.i_trigger <
		 first->event_10_1._info.i_trigger))
	      first = conn;
	}

      if (!first)
	{
	  /* Hmm, if individual sources can be disconnected, then we
	   * may end up like this!
	   */
	  LWROC_WARNING("Sticky pending, but no pending found.");
	  sticky = 0;
	  goto no_sticky;
	}

      /* Then find out which sources have this subevent. */

      total_size = (uint32_t) sizeof (lmd_event_10_1_host);

      for (i = 0; i < _lwroc_merge_info._num_active_sources; i++)
	{
	  lwroc_merge_source *conn =
	    _lwroc_merge_info._sources[i];

	  if (conn->event_10_1._info.l_count ==
	      first->event_10_1._info.l_count &&
	      conn->event_10_1._info.i_trigger ==
	      first->event_10_1._info.i_trigger)
	    {
	      total_size +=
		conn->pre.event_size -
		(uint32_t) sizeof (lmd_event_10_1_host);
	      conn->_include = 1;
	    }
	  else
	    conn->_include = 0;
	}
    }
  else
    {
    no_sticky:

      /* We take the first source as reference... */

      first = _lwroc_merge_info._sources[0];

      total_size = first->pre.event_size;

      /* LWROC_INFO_FMT("total size1: %d", total_size); */

      for (i = 1; i < _lwroc_merge_info._num_active_sources; i++)
	{
	  lwroc_merge_source *conn =
	    _lwroc_merge_info._sources[i];

	  if (conn->event_10_1._info.i_trigger !=
	      first->event_10_1._info.i_trigger)
	    {
	      mismatch = 1;
	      LWROC_CERROR_FMT(&conn->_out._base,
			       "Trigger number mismatch "
			       "(src%" MYPRIzd "=%d != first=%d)",
			       i, conn->event_10_1._info.i_trigger,
			       first->event_10_1._info.i_trigger);

	      lwroc_merge_remove_source(conn, i, LWROC_FETCH_EVENT_MISMATCH);
	      i--;
	      continue;
	    }
	  if (conn->event_10_1._info.l_count !=
	      first->event_10_1._info.l_count)
	    {
	      mismatch = 1;
	      LWROC_CERROR_FMT(&conn->_out._base,
			       "Event number mismatch "
			       "(src%" MYPRIzd "=%d != first=%d)",
			       i, conn->event_10_1._info.l_count,
			       first->event_10_1._info.l_count);

	      lwroc_merge_remove_source(conn, i, LWROC_FETCH_EVENT_MISMATCH);
	      i--;
	      continue;
	    }

	  total_size +=
	    conn->pre.event_size -
	    (uint32_t) sizeof (lmd_event_10_1_host);
	}

      if (first->event_10_1._info.i_trigger >= 14)
	LWROC_ACTION_FMT("Event with trigger %d seen.",
			 first->event_10_1._info.i_trigger);

      flush = (first->event_10_1._info.i_trigger >= 12);
    }

  /* Hmmm, in case of mismatch we should remove the
   * mismatching source.  (May also be the master/first, and
   * then all others reported wrong, but how to know that?
   * Likely: a trigger mismatch is a data transfer error,
   * while a event number mismatch is a missed event).  A
   * missed event should be very very unlikely thanks to the
   * checks performed by the TRIVA/readout.
   *
   * It is possibly a bit overkill, but as none of the errors
   * really should happen, the prudent thing to do would
   * be to notify the master TRIVA and request a stop/start acq
   * cycle, and to flush any data
   */

  /* By definition, master cannot be mismatching. */

  (void) mismatch; /* TODO: deal with the mismatch. */

  /* If we had a mismatch, we will have kicked at least one source.
   * Do not build this event now.  Even if we are able to handle
   * missing some sources, some kind of notifier should be inserted?
   * So come back here and try again then...
   */

  if (mismatch)
    return 1; /* Flushing reports may make sense.  Harmless. */

  /* We are about to eject the event.  Are there any sticky updates
   * pending?
   */

  if (_lwroc_merge_info._sticky_update_pending)
    lwroc_merge_pending_sticky_update();

  {
    char *dest;
    lmd_event_10_1_host *event_header;

    /* LWROC_INFO_FMT("total size: %d", total_size); */

    dest = lwroc_request_event_space(_lwroc_merge_info._data_handle,
				     total_size);

    event_header = (lmd_event_10_1_host *) dest;

    *event_header = first->event_10_1;

    event_header->_header.l_dlen = (uint32_t)
      DLEN_FROM_EVENT_DATA_LENGTH(total_size -
				  sizeof (lmd_event_header_host));

    dest += sizeof (lmd_event_10_1_host);

#if LWROC_MERGE_DEBUG_STICKY
    printf ("Merge event: %" PRIu32 " %s\n",
	    event_header->_info.l_count,
	    sticky ? "(sticky)" : "");
#endif

    /* Copy the event data. */

    for (i = 0; i < _lwroc_merge_info._num_active_sources; i++)
      {
	lwroc_merge_source *conn =
	  _lwroc_merge_info._sources[i];
	uint32_t data_size;

	if (sticky)
	  {
	    if (!conn->_include)
	      continue;
	    /* Event is being written, so can be inserted in our
	     * sticky store.  (If write would be allowed to fail after
	     * this point, i.e. before lwroc_used_event_space(), then
	     * the store would be out of sync).  This includes that
	     * sticky store inserts cannot fail!
	     */
	    lwroc_lmd_sticky_store_insert(conn->_sticky_store_full,
					  conn->pre.ptr,
					  conn->pre.event_size,
					  1 /* discard_revoke */,
					  1 /* as_active */);
	  }

	data_size =
	  conn->pre.event_size -
	  (uint32_t) sizeof (lmd_event_10_1_host);

	memcpy (dest,
		conn->pre.ptr + sizeof (lmd_event_10_1_host),
		data_size);

	dest += data_size;

	lwroc_merge_done_event(conn);
      }

    lwroc_used_event_space(_lwroc_merge_info._data_handle, total_size, flush);
  }

  if (flush)
    {
      /* Need to be before fetch of next event. */
      LWROC_MON_CHECK_COPY_BLOCK(_lwroc_mon_main_handle,&_lwroc_mon_main, 1);

      /* Doing this check inside flush since flush only (and strictly) set
       * for triggers >= 12 above, and no-sticky.
       */
      if (first->event_10_1._info.i_trigger == 15 &&
	  _lwroc_main_thread->_terminate)
	{
	  /* Event 15 seen, and we want to terminate.  Eject all sources. */
	  lwroc_merge_revalidate_all_sources();
	  /* There are no active sources any longer, so harmless to
	   * just fall through.
	   */
	}
    }

  /* We now need to fetch data from all sources. */

  _lwroc_merge_info._sticky_pending = 0;

  /* Loop backwards, in case sources are ejected. */
  for (i = _lwroc_merge_info._num_active_sources; i; )
    {
      lwroc_merge_source *conn =
	_lwroc_merge_info._sources[--i];

      if (sticky && !conn->_include)
	{
	  if (conn->event_10_1._header.i_type    == LMD_EVENT_STICKY_TYPE &&
	      conn->event_10_1._header.i_subtype == LMD_EVENT_STICKY_SUBTYPE)
	    _lwroc_merge_info._sticky_pending = 1;
	  continue;
	}

      lwroc_merge_fetch_event_wrap(conn, i, 1, NULL);
    }

  return flush;
}

#define LWROC_MERGE_MON_CHECK(flush) do {				\
    LWROC_MON_CHECK_COPY_BLOCK(_lwroc_mon_main_handle,&_lwroc_mon_main,	\
			       flush);					\
    LWROC_MON_CHECK_COPY_CONN_MON_BLOCK(_lwroc_merge_info._mon, flush);	\
  } while (0)

void lwroc_merge_after_token(const lwroc_thread_block *block,
			     int after_token)
{
  (void) block;

  /* printf ("after token %d...\n", after_token); */

  if (!after_token)
    {
      /* Make sure we get woken up. */
      _lwroc_mon_main_handle->_notify_thread = block;
      return;
    }
  else
    {
      /* Do not spam us. */
      _lwroc_mon_main_handle->_notify_thread = NULL;
    }

  /* This function is called when our blocker thread has received
   * a token.  This gives us an opportunity to update the monitor
   * information even when we are stuck waiting for free space.
   */
  LWROC_MERGE_MON_CHECK(0);
}

lwroc_control_buffer *_merge_ctrl = NULL;

void lwroc_merge_setup(void)
{
  _merge_ctrl = lwroc_alloc_control_buffer(LWROC_CONTROL_OP_MERGE, 0x100);

  _merge_ctrl->_thread_notify_ready = _lwroc_main_thread->_block;
  _merge_ctrl->_state = LWROC_CONTROL_STATE_WAIT_MESSAGE;
}

void lwroc_merge_loop(void)
{
  lwroc_merge_request merge_req;
  int need_first_fetch = 0;
  int wait_data = 1;

  /* Normally, we wait for new data after connect, or after emitting
   * the current event.
   */
  wait_data = 1;
  if (_lwroc_merge_info._mode & LWROC_MERGE_MODE_XFER)
    {
      /* Very likely to not have any data, since we are not sorting
       * but emitting immediately.
       */
      wait_data = 0;
    }

  lwroc_thread_block_set_after_token(_lwroc_main_thread->_block,
				     lwroc_merge_after_token);

  LWROC_MERGE_MON_CHECK(0);

  lwroc_msg_wait_any_msg_client(_lwroc_main_thread->_block);

  for ( ; ; )
    {
      int need_source;
      int flush = 0;

      if (_merge_ctrl->_state & LWROC_CONTROL_STATE_USER_ACTION_MARK)
	switch (_merge_ctrl->_state)
	  {
	  /* Note: there are other states too, while control requests
	   * are being received by the network thread.  We do not
	   * react on those!
	   */
	  case LWROC_CONTROL_STATE_MESSAGE_READY:
	  {
	    lwroc_deserialize_error desererr;
	    const char *end;

	    /* Prepare a response. */

	    _merge_ctrl->_response._tag    = _merge_ctrl->_request._tag;
	    _merge_ctrl->_response._op     = _merge_ctrl->_request._op;
	    _merge_ctrl->_response._value  = 0;
	    _merge_ctrl->_response._response_data_size = 0;
	    LWROC_SET_CONTROL_STATUS(_merge_ctrl, UNKNOWN,
				     PERFORMER, "Unknown control request.");

	    /* Can we parse the request? */

	    end =
	      lwroc_merge_request_deserialize(&merge_req,
					      _merge_ctrl->_request_data,
					      _merge_ctrl->_request.
					      /* */ _request_data_size,
					      &desererr);

	    if (end == NULL)
	      {
		LWROC_SET_CONTROL_STATUS(_merge_ctrl, INVALID,
					 PERFORMER,
					 "Invalid control message.");
		goto message_send;
	      }

	    switch (merge_req._op)
	      {
	      case LWROC_MERGE_OP_TRY_RECONNECT_DIS_SRC:
		_lwroc_merge_info._check_nodata_sources = 1;
		_lwroc_merge_info._try_reconnect_disabled_sources = 1;
		_merge_ctrl->_state = LWROC_CONTROL_STATE_MESSAGE_WORKING;
		goto after_request_loop;
	      }
	    _merge_ctrl->_state = LWROC_CONTROL_STATE_MESSAGE_WORKING;
	  }
	    FALL_THROUGH;
	  case LWROC_CONTROL_STATE_MESSAGE_WORKING:
	    /*
	    printf ("merge control op: %d\n", merge_req._op);
	    */
	    switch (merge_req._op)
	      {
	      case LWROC_MERGE_OP_TRY_RECONNECT_DIS_SRC:
		/* The _lwroc_merge_info._check_nodata_sources
		 * is handled every iteration, so will be done.
		 */
		LWROC_SET_CONTROL_STATUS(_merge_ctrl, SUCCESS,
					 PERFORMER, NULL);
		break;
	      }
	    _merge_ctrl->_state = LWROC_CONTROL_STATE_MESSAGE_SEND;

	    FALL_THROUGH;

	    /* We are supposed to send the control message response. */
	  case LWROC_CONTROL_STATE_MESSAGE_SEND:
	    ;
	  message_send:
	    /* Our status is always 1? */
	    _merge_ctrl->_response._value = 1;

	  {
	    size_t sz_response;

	    merge_req._op = 0;
	    merge_req._num_active_sources =
	      (uint32_t) _lwroc_merge_info._num_active_sources;
	    merge_req._num_sources =
	      (uint32_t) _lwroc_merge_info._num_sources;

	    sz_response =
	      lwroc_merge_request_serialized_size();

	    if (sz_response > _merge_ctrl->_request._response_data_maxsize)
	      {
		LWROC_SET_CONTROL_STATUS(_merge_ctrl, TOO_BIG,
					 PERFORMER,
					 "Response structure too large.");
	      }
	    else
	      {
		lwroc_merge_request_serialize(_merge_ctrl->_response_data,
					      &merge_req);
		_merge_ctrl->_response._response_data_size =
		  (uint32_t) sz_response;
	      }
	  }

	    lwroc_control_done(_merge_ctrl, _lwroc_main_thread->_block);
	    break;
	  }
    after_request_loop:
      ;

      /* Has the input stage opened another fresh source? */

      while (LWROC_ITEM_BUFFER_NONEMPTY(_lwroc_merge_info._fresh_sort_sources))
	{
	  lwroc_merge_source *conn;
	  int ret;

	  /* TODO: remove the notify marker. */

	  LWROC_ITEM_BUFFER_REMOVE(_lwroc_merge_info._fresh_sort_sources,&conn);

	  memset (&conn->pre, 0, sizeof (conn->pre));

#if LWROC_MERGE_DEBUG_STICKY
	  printf("Sorter got input %p.\n", conn);
#endif

	  /* Before we activate the last source for an event merger,
	   * check that the sum of event lengths is permissible.
	   */

	  if ((_lwroc_merge_info._mode & LWROC_MERGE_MODE_EB) &&
	      _lwroc_merge_info._num_activated_sources >=
	      _lwroc_merge_info._num_sources - 1)
	    {
	      /* TODO: it would be much nicer to have the check the
	       * max event size here, before we report to the trigger
	       * state machine that all sources are validated.
	       * However, in that case, we actually have to tell the
	       * trigger state machine that we failed, and that a
	       * complete revalidation is required.  Which means quite
	       * some more logics in the trigger state machine, to
	       * kick all pending validations etc...
	       *
	       * Look for sum_max_ev_len below.
	       */
	    }

	  _lwroc_merge_info._num_activated_sources++;

	  /* In event building mode, where we have gotten validated
	   * sources, we still have one issue.  The validation thread
	   * is to tell the master state machine when all sources are
	   * validated.  And then later report any losses.  The
	   * clearest way to do that is to always report how many
	   * sources are active (in this thread).  But that runs into
	   * the problem when each source we get as first things does
	   * a blocking wait on getting an event.  We get a catch 22.
	   * So in event building mode, we add all sources first, and
	   * once we have gotten them all, only then do we fetch the
	   * next (first) event to be merged.
	   *
	   * In time sorting mode, we get the event immediately here,
	   * since it is needed to determine the source sorting.
	   */

	  if (_lwroc_merge_info._mode & LWROC_MERGE_MODE_EB)
	    {
	      conn->pre.event_size = 0; /* Need to fetch event. */
	      need_first_fetch = 1;
	      lwroc_merge_insert_source(conn);

	      /* Careful: do not write to val_thread_block unless we
	       * have such a thread, i.e. are a event builder.
	       */
	      _lwroc_merge_info._changed_sources = 1;
	      MFENCE;
	      if (_lwroc_merge_val_thread)
		lwroc_thread_block_send_token(_lwroc_merge_val_thread->_block);
	    }
	  else
	    {
	      conn->pre.mrg.stamp._prev = 0;
	      conn->pre.mrg.stamp._next = LWROC_MERGE_TIMESTAMP_MISSING;
	      conn->lag._next_seen = 0;
	      gettimeofday(&conn->lag._time_seen, NULL);
	      conn->lag._report_interval =
		_config._merge_ts_nodata_warn_timeout;

	      ret = lwroc_merge_fetch_event(conn, wait_data);

	      /* Would-block only happens with wait = 0, i.e. in
	       * no-sort mode (xfer).
	       */
	      if (ret == LWROC_FETCH_EVENT_SUCCESS ||
		  ret == LWROC_FETCH_EVENT_WOULD_BLOCK)
		{
		  /* The fetch was successful, place the source in
		   * our list.
		   */

		  lwroc_merge_insert_source(conn);
		}
	      else
		{
		  /*
		  printf("Immediate fail.\n");
		  */
		  /* Source failed immediately. */
		  lwroc_merge_eject_source(conn, ret);
		}
	    }
	}

      if (_lwroc_merge_info._check_nodata_sources)
	{
	  _lwroc_merge_info._check_nodata_sources = 0;
	  MFENCE;
	  lwroc_merge_try_fetch_missing();
	  _lwroc_merge_info._try_reconnect_disabled_sources = 0;

	  if (_lwroc_merge_info._num_finished_sources ==
	      _lwroc_merge_info._num_sources)
	    {
	      LWROC_INFO("All sources finished.");
	      break;
	    }
	}

#if 0
      /* If we are to event-build, then they must all be accepted by the
       * master trigger state machine...
       */

      if (_lwroc_merge_info._mode & LWROC_MERGE_MODE_EB)
	{
	  if (_lwroc_merge_info._num_validated_sources <
	      _lwroc_merge_info._num_sources)
	    {
	      size_t i;

	      /* We need to go through the sources that are not
	       * validated...
	       */

	      for (i = 0; i < _lwroc_merge_info._num_active_sources; i++)
		{
		  lwroc_merge_source *conn =
		    _lwroc_merge_info._sources[i];

		  if (!conn->_validated)
		    {
		      /* TODO: the mechanics of this entire loop has
		       * to be changed a bit.  (A thread for
		       * pre-validation?)  We shall not wait for each
		       * sourec to have an event before we try to
		       * activate/validate others.  This is required
		       * when operating with a TRIMI, where we might
		       * selectively disable sources (slaves) when
		       * they become unresponsive.
		       *
		       * Hmm, with a separate pre-validation the
		       * mechanics need not be modified: every source
		       * that is valid is supposedly a good one, and
		       * it deserves being waited for.  Only reason
		       * not to hang waiting for just one is that we
		       * may want to be able to tell which ones have
		       * something, as which ones are holding us up...
		       */

		      /* As things are operating now, there is an
		       * event available.  Either it as in
		       * identification event, in which case we check
		       * that.  If not, we should skip it and get the
		       * next event.
		       */

		    }
		}
	    }
	}
#endif
      /* If we have no active sources, then there is nothing more to
       * do.  If we are to event-build, we require to have all sources
       * present.
       */

      need_source = 0;

      if (_lwroc_merge_info._mode & LWROC_MERGE_MODE_EB)
	{
	  if (_lwroc_merge_info._num_active_sources <
	      _lwroc_merge_info._num_sources)
	    {
	      /* We only do source expelling once we have merged
	       * whatever data can be merged.  One way to know that is
	       * that some source has gone missing - then we cannot
	       * continue to merge?
	       *
	       * Note: this will change once we start to do partial
	       * event building.
	       */
	      if (_lwroc_merge_info._revalidate_sources)
		{
		  lwroc_merge_revalidate_all_sources();
		  _lwroc_merge_info._num_activated_sources = 0;
		  SFENCE;
		  _lwroc_merge_info._revalidate_sources = 0;
		  /* No need to wake the validator up - it will have
		   * been notified if some sources were put into its
		   * incoming item buffer.
		   */
		}
	      need_source = 1;

	      if (_lwroc_main_thread->_terminate)
		{
		  /* When in event-building mode, we build until no
		   * further events are available.  The validator
		   * forwards the termination request to the
		   * master/trigger state machine, which orderly stops
		   * the acq.  It will then plant an (bogus)
		   * identification event in the master stream, which
		   * kicks that out of the merger (i.e. us), in turn
		   * reaching this point.
		   */
		  break;
		}
	    }
	}
      if (_lwroc_merge_info._mode & LWROC_MERGE_MODE_TS)
	{
	  if (!_lwroc_merge_info._num_active_sources)
	    need_source = 1;

	  if (_lwroc_main_thread->_terminate)
	    {
	      /* In time-sorting mode we abort immediately.  Data will
	       * be lost anyhow as we will not be able to time-sort,
	       * beyond the end of the first ending stream.  (Unless
	       * we want to continue to read data indefinitely.)
	       */
	      break;
	    }
	}

      if (need_source)
	{
	  /*printf("Sorter wait 1.\n");*/
	  LWROC_ITEM_BUFFER_SET_NOTIFY(_lwroc_merge_info._fresh_sort_sources,
				       _lwroc_main_thread->_block);
	  /*printf ("set notify: %p %p\n",&_lwroc_merge_info,_lwroc_merge_info._fresh_sources.notify_consumer);*/
	  /*printf("Sorter wait 2.\n");*/
	  if (LWROC_ITEM_BUFFER_NONEMPTY(_lwroc_merge_info._fresh_sort_sources))
	    continue;
	  /* Hmmm, got a source in the meantime? */
	  /*printf("Sorter wait 3.\n");*/
	  lwroc_thread_block_get_token(_lwroc_main_thread->_block);
	  /*printf("Sorter wait 4.\n");*/
	  continue;
	}

      if (need_first_fetch)
	{
	  size_t i;
	  size_t sum_max_ev_len = 0;
	  uint32_t max_out_ev_size;

	  for (i = 0; i < _lwroc_merge_info._num_active_sources; i++)
	    {
	      lwroc_merge_source *conn2 =
		_lwroc_merge_info._sources[i];

	      sum_max_ev_len += conn2->_max_ev_len;
	    }

	  max_out_ev_size =
	    lwroc_data_pipe_get_max_ev_len(_lwroc_merge_info._data_handle);

	  if (sum_max_ev_len > max_out_ev_size)
	    {
	      LWROC_ERROR_FMT("Sum of event build sources "
			      "actual max-event-size "
			      "(%" MYPRIzd "=0x%" MYPRIzx ") "
			      "larger than output accepts "
			      "(%" PRIu32 "=0x%" PRIx32 ").",
			      sum_max_ev_len, sum_max_ev_len,
			      max_out_ev_size, max_out_ev_size);

	      /* In principle, the user has to restart some source (or
	       * us), to reconfigure the maximum event size.  But that
	       * will kick it further out to find another maximum
	       * event size.
	       *
	       * We just need to force full revalidation to show our
	       * contempt of the situation.
	       */

	      /* Loop backwards, as sources are removed. */
	      for (i = _lwroc_merge_info._num_active_sources; i; )
		{
		  lwroc_merge_source *conn =
		    _lwroc_merge_info._sources[--i];

		  /* Just kick them back to validator. */
		  lwroc_merge_remove_source(conn, i, LWROC_FETCH_EVENT_IDENT);
		}
	      /* _lwroc_merge_info._num_activated_sources = 0; */
	      flush = 1;
	      goto continue_flush_mon;
	    }

	  /* Loop backwards, in case sources are ejected. */
	  for (i = _lwroc_merge_info._num_active_sources; i; )
	    {
	      lwroc_merge_source *conn =
		_lwroc_merge_info._sources[--i];

	      if (!conn->pre.event_size)
		lwroc_merge_fetch_event_wrap(conn, i, 1, NULL);
	    }
	  need_first_fetch = 0;
	  /* In case some source was thrown out, check again. */
	  flush = 1;
	  goto continue_flush_mon;
	}

      /*
      LWROC_INFO_FMT("%d / %d",
		     (int) _lwroc_merge_info._num_active_sources,
		     (int) _lwroc_merge_info._num_sources);
      */
      /* We have active sources.  Whenever we reach this point, they
       * are all ready with data, so we can eject the next event.
       */

      /* If we are event-building, we first check that all event
       * headers are matching (event number and trigger number).
       *
       * We then eject all data.
       */

      if (_lwroc_merge_info._mode & LWROC_MERGE_MODE_EB)
	flush = lwroc_merge_event();

      /* If we are sorting, simply find the next item in the list, and
       * eject it.
       */

      if (_lwroc_merge_info._mode & LWROC_MERGE_MODE_TS)
	{
	  lwroc_merge_source *conn =
	    _lwroc_merge_info._sources[0];

	  if (conn->pre.mrg.stamp._next == LWROC_MERGE_TIMESTAMP_DISABLED ||
	      conn->pre.mrg.stamp._next == LWROC_MERGE_TIMESTAMP_WAITDATA ||
	      conn->pre.mrg.stamp._next == LWROC_MERGE_TIMESTAMP_BROKEN)
	    {
	      /* All sources are missing data. */
	      /* LWROC_ERROR("All timesorter sources are missing data."); */
	      /* Do some sleep to not run a busy-loop looking for data. */
	      usleep(50000);
	      /* Force a check for data. */
	      _lwroc_merge_info._check_nodata_sources = 1;
	      goto continue_flush_mon;
	    }

	  /*
	  printf ("%p, Event to eject, %" PRIu32 " bytes.\n",
		  conn,
		  conn->pre.event_size);
	  */

	  /* Any sticky events pending? */

	  if (_lwroc_merge_info._sticky_update_pending)
	    lwroc_merge_pending_sticky_update();

	  /* Sticky?  Then insert into store. */

	  if (conn->event_10_1._header.i_type    == LMD_EVENT_STICKY_TYPE &&
	      conn->event_10_1._header.i_subtype == LMD_EVENT_STICKY_SUBTYPE)
	    {
#if LWROC_MERGE_DEBUG_STICKY
	      printf ("Insert sticky - %d\n", conn->pre.event_size);
	      fflush(stdout);
#endif

	      /* See comment in lwroc_merge_event(): basically, this
	       * or event writing below may not fail - else store gets
	       * out of sync.
	       */
	      lwroc_lmd_sticky_store_insert(conn->_sticky_store_full,
					    conn->pre.ptr,
					    conn->pre.event_size,
					    1 /* discard_revoke */,
					    1 /* as_active */);
	    }

	  /* Insert into the output stream. */

	  _lwroc_merge_info.event_number++;

	  {
	    char *dest;

	    dest = lwroc_request_event_space(_lwroc_merge_info._data_handle,
					     conn->pre.event_size);

	    /* Copy the event. */

	    memcpy (dest, conn->pre.ptr, conn->pre.event_size);

	    lwroc_merge_done_event(conn);

	    if (_lwroc_merge_info._mode & LWROC_MERGE_MODE_LMD)
	      {
		lmd_event_10_1_host *event_header;

		event_header = (lmd_event_10_1_host *) dest;

		/* For time-sorted data, we give new event numbers,
		 * such that they are continuously increasing.
		 */

#if LWROC_MERGE_DEBUG_STICKY
		printf ("Sort event [%zd] : %" PRIu32 " -> %" PRIu32 "\n",
			conn->_src_index,
			event_header->_info.l_count,
			_lwroc_merge_info.event_number+1);
#endif

		event_header->_info.l_count =
		  _lwroc_merge_info.event_number;

		flush = (event_header->_info.i_trigger >= 12);

		/* We sorted this timestamp - insert into analysis. */

		if (_lwroc_merge_analyse &&
		    conn->pre.mrg.stamp._branch_id != (uint32_t) -1)
		  lwroc_merge_analyse_add_timestamp(conn->_src_index,
		    /* */ conn->pre.mrg.stamp._branch_id,
			  conn->pre.mrg.stamp._next,
			  conn->pre.sync_check,
			  event_header->_info.i_trigger);
	      }

	    if (_lwroc_merge_info._mode & LWROC_MERGE_MODE_XFER)
	      {
		ebye_xfer_header *xfer = (ebye_xfer_header *) dest;

		if (_lwroc_merge_info._num_sources > 1)
		  {
		    /* Give new sequence number,
		     * such that they are continuously increasing.
		     */

		    xfer->_sequence =
		      htonl(_lwroc_merge_info.event_number);
		  }
	      }

	    lwroc_used_event_space(_lwroc_merge_info._data_handle,
				   conn->pre.event_size, flush);
	  }

	  /* Fetch next event. */

	  if (conn->pre.mrg.stamp._next < LWROC_MERGE_TIMESTAMP_MARK_BEGIN)
	    _lwroc_merge_info._last_good_sort = conn->pre.mrg.stamp._next;

	  conn->pre.mrg.stamp._prev = conn->pre.mrg.stamp._next;
	  conn->pre.mrg.stamp._next = LWROC_MERGE_TIMESTAMP_MISSING;

	  lwroc_merge_fetch_event_wrap(conn, 0 /*srcind*/, wait_data, NULL);

	  /* Move new first (or replaced first) to correct position. */

	  lwroc_merge_resort_first_source();
	}

    continue_flush_mon:
      LWROC_MERGE_MON_CHECK(0);
    }
}

void lwroc_merge_detect_future_stall(void)
{
  struct timeval latest = { 0, 0 };
  int report_all = 0;
  size_t i;
  struct timeval now;

  gettimeofday(&now, NULL);


  for (i = 0; i < _lwroc_merge_info._num_active_sources; i++)
    {
      lwroc_merge_source *conn = _lwroc_merge_info._sources[i];

      /* Has this source moved? */

      if (conn->pre.mrg.stamp._next != conn->lag._next_seen)
	{
	  if (conn->lag._report_interval !=
	      _config._merge_ts_nodata_warn_timeout)
	    {
	      /* We had reported stall, but now see some progress. */
	      LWROC_CWARNING(&conn->_out._base,
			     "Source sorted event.  (Un-stalled?)");
	    }

	  conn->lag._next_seen = conn->pre.mrg.stamp._next;
	  conn->lag._time_seen = now;

	  conn->lag._report_interval = _config._merge_ts_nodata_warn_timeout;
	}

      /* Find the latest source sort seen. */

      if (timercmp(&latest, &conn->lag._time_seen,<))
	latest = conn->lag._time_seen;
    }

  if (timercmp(&now,&_lwroc_merge_info._future_stall_detector_after,<) &&
      now.tv_sec >= _lwroc_merge_info._future_stall_detector_after.tv_sec - 2)
    return;

  for (i = 0; i < _lwroc_merge_info._num_active_sources; i++)
    {
      lwroc_merge_source *conn = _lwroc_merge_info._sources[i];
      struct timeval diff;

      timersub(&latest, &conn->lag._time_seen, &diff);

      /* Do not report for sources that miss next data.
       * That is another kind of problem, which is diagnosed separately.
       */
      if (conn->pre.mrg.stamp._next < LWROC_MERGE_TIMESTAMP_MARK_BEGIN &&
	  diff.tv_sec >= conn->lag._report_interval)
	{
	  LWROC_CWARNING_FMT(&conn->_out._base,
			     "Source got last event time-sorted "
			     "%d s ago, earlier than last source.  "
			     "(Report limit: %d s.)  "
			     "Stall due to future timestamp?  Dump follows:",
			     (int) diff.tv_sec,conn->lag._report_interval);
	  conn->lag._report_interval *= 5;
	  report_all = 1;
	}
    }

  if (report_all)
    lwroc_merge_report_timestamps();
}

void lwroc_merge_monitor_fetch(void *ptr)
{
  (void) ptr;

  /* We abuse the monitor fetch function to do periodic checking
   * if some sort source is lagging far behind the other sources
   * (likely due to having timestamps too far in the future).
   *
   * But since we iteratite over the structures owned by the main
   * thread, we better be called by it (and not the input thread).
   */

  if (_lwroc_merge_info._mode & LWROC_MERGE_MODE_TS)
    {
      lwroc_merge_detect_future_stall();
    }
}

void lwroc_merge_val_thread_loop(lwroc_thread_instance *inst)
{
  lwroc_net_trigbus *eb_master = NULL;
  lwroc_merge_source *ask_ident = NULL; /* Source we expect answer for. */
  int i;
  int warn_full_count = 0;
  int warn_full_next = 1;

  {
    pd_ll_item *iter;

    PD_LL_FOREACH(_lwroc_net_eb_masters, iter)
      {
	lwroc_net_trigbus *conn =
	  PD_LL_ITEM(iter, lwroc_net_trigbus, _masters);

	/* TODO: we take first master.  We should check and only have
	 * one master.  The one being a local triva/mi master. */
	eb_master = conn;
	break;
    }
  }

  if (!eb_master)
    LWROC_BADCFG("Event builder, but no master drasi instance given.");

  eb_master->_thread_notify = _lwroc_merge_val_thread->_block;

  lwroc_msg_wait_any_msg_client(_lwroc_merge_val_thread->_block);

  for ( ; !inst->_terminate; )
    {
      pd_ll_item *iter, *tmp_iter;
      int consumed_data;
      int had_issue;
      uint32_t force_needed = 0;

      if (_lwroc_main_thread->_terminate == 1)
	{
	  /* Even though we are not the main_thread, it is ok to change
	   * the value, as long as it is non-zero.  main_thread does not
	   * modify the value.
	   */
	  _lwroc_main_thread->_terminate = 2;
	  lwroc_net_trigbus_send_look_at_me(eb_master);
	}

      if (_lwroc_merge_info._delayed_eb_enabled &
	  LWROC_DELAYED_EB_STATUS_ENABLED)
	{
	  /* Check if any input pipe is over the mark, or full. */

	  PD_LL_FOREACH(_lwroc_merge_all_sources, iter)
	    {
	      lwroc_merge_source *conn =
		PD_LL_ITEM(iter, lwroc_merge_source, _all_sources);
	      size_t fill =
		lwroc_pipe_buffer_fill(conn->_buffer);

	      if (lwroc_pipe_buffer_blocked(conn->_buffer) ||
		  fill > conn->_force_threshold)
		{
		  /* printf ("force: %d \n", fill > conn->_force_threshold); */
		  force_needed = LWROC_DELAYED_EB_STATUS_NEED_FORCE;
		}
	    }
	}

      if (force_needed != _lwroc_merge_info._delayed_eb_need_force)
	{
	  /*
	  if (force_needed)
	    printf ("*** NEED FORCE ***\n");
	  else
	    printf ("* No need force *\n");
	  */
	  _lwroc_merge_info._delayed_eb_need_force = force_needed;
	  if (!force_needed)
	    {
	      MFENCE;
	      _lwroc_merge_info._notify_force_need =
		_lwroc_merge_val_thread->_block;
	    }

	  /* And tell the trigger state machine to tell all data producers. */
	  lwroc_net_trigbus_send_look_at_me(eb_master);

	  if (force_needed &&
	      !lwroc_pipe_buffer_has_hold(
		lwroc_data_pipe_get_pipe_ctrl(_lwroc_merge_info._data_handle)))
	    {
	      warn_full_count++;

	      if (warn_full_count >= warn_full_next)
		{
		  /* We need to force event building.  Warn user that
		   * pipe is too small.  Note that we do not warn if
		   * the cause is that the output pipe is being held.
		   * (That is warned for separately.)
		   */

		  /* If any input pipe is empty, likely the buffers
		   * are too small.  Otherwise, likely the merger thread
		   * is too slow.
		   */

		  LWROC_WARNING_FMT("Global event building forced "
				    "(occurrence %d).  Likely, "
				    "(1) some merge input buffer is too small "
				    "to hold one spill, or "
				    "(2) some bandwidth to small to send data "
				    "during spill pause, or "
				    "(3) event builder thread is too slow, or "
				    "(4) an consumer (e.g. data storage) "
				    "is not accepting the data, or "
				    "(5) a following time-sorter is stuck, "
				    "e.g. due to inconsistent time-stamps.  "
				    "If issue persists, "
				    "upgrade network interfaces and/or "
				    "the event builder hardware.",
				    warn_full_count);
		  /* TODO: list inputs which are over the threshold. */

		  warn_full_next *= 10;
		}
	    }
	}
      /* TODO: we can also get sources from the merger thread, if their
       * validation was revoked.
       */

      consumed_data = 0;

      for (i = 0; i < 2; i++)
	while (LWROC_ITEM_BUFFER_NONEMPTY(_lwroc_merge_info._to_val_sources[i]))
	  {
	    lwroc_merge_source *conn;

	    /* TODO: remove the notify marker. */

	    LWROC_ITEM_BUFFER_REMOVE(_lwroc_merge_info._to_val_sources[i],
				     &conn);

	    memset (&conn->pre, 0, sizeof (conn->pre));

	    /* printf("Validator got input %p.\n", conn); */

	    PD_LL_ADD_BEFORE(&_lwroc_merge_val_sources,&conn->_val_sources);

	    conn->_val_state = LWROC_MERGE_VAL_FETCH_EVENT;

	    if (i == 0) /* From input thread, i.e. was reconnected. */
	      {
		/* Forgot any stickies we may have gotten during previous
		 * validation attempts.
		 */
		lwroc_lmd_sticky_store_clear(conn->_sticky_store_val_pending);
	      }
	  }

      /* Have we gotten some message from the master? */

      had_issue = 0;

      /* printf ("lwroc_net_trigbus_recv_msg\n"); */

      if (lwroc_net_trigbus_recv_msg(eb_master,
				     LWROC_TRIVA_CONTROL_EB_INVALIDATE_IDENT |
				     LWROC_TRIVA_CONTROL_EB_QUERY_IDENT |
				     LWROC_TRIVA_CONTROL_EB_BAD_IDENT |
				     LWROC_TRIVA_CONTROL_EB_GOOD_IDENT |
				     LWROC_TRIVA_CONTROL_EB_STATUS_QUERY,
				     &had_issue))
	{
	  lwroc_trigbus_msg *msg = &eb_master->_msg;

	  /* Such that merge in thread knows if we need to be woken up
	   * if situation becomes urgent.
	   */
	  _lwroc_merge_info._delayed_eb_enabled =
	    msg->delayed_eb_status & LWROC_DELAYED_EB_STATUS_ENABLED;
	  MFENCE;

	  if (had_issue == 1)
	    {
	      /* We have lost the connection. */

	      if (ask_ident)
		{
		  ask_ident->_val_state = LWROC_MERGE_VAL_QUERY_IDENT;
		  ask_ident = NULL;
		}

	      /* All sources need to be revalidated? */

	    }
	  else
	    {
	      /* Two kinds of messages come to us:
	       * question (request) if we have a source to validate.
	       * the validation response for a source.
	       */

	      /* printf ("validation thread got message %x\n",msg->_type); */

	      switch (msg->_type)
		{
		case LWROC_TRIVA_CONTROL_EB_INVALIDATE_IDENT:
		  /* The triva state has restarted.  So whatever has
		   * been validated with earlier messages is invalid.
		   * Get them out of the sorting thread.
		   */
		  _lwroc_merge_info._revalidate_sources = 1;
		  /* May need to wake the sorter up to clear the
		   * variable (in case it has no sources and waits for
		   * us to inject some).
		   */
		  SFENCE;
		  lwroc_thread_block_send_token(_lwroc_main_thread->_block);

		  if (ask_ident)
		    {
		      /* Since no ident has been sent before we have
		       * responded to this message, any old ident
		       * message is invalid.
		       */
		      ask_ident->_val_state = LWROC_MERGE_VAL_FETCH_EVENT;
		      ask_ident = NULL;
		    }

		  /* sleep(3); */

		  msg->_type = LWROC_TRIVA_CONTROL_EB_INVALIDATED_IDENT;
		  break;

		case LWROC_TRIVA_CONTROL_EB_BAD_IDENT:
		case LWROC_TRIVA_CONTROL_EB_GOOD_IDENT:

		  if (!ask_ident)
		    {
		      LWROC_CERROR(&eb_master->_out._base,
				   "EB master sent IDENT answer "
				   "with no question pending.");
		      /* Communication mishap - master broken? */
		      /* Tear connection down! */
		      lwroc_net_trigbus_discon(eb_master);
		      break;
		    }

		  /* Verify the ident message. */
		  if (msg->_eb_ident0 != ask_ident->pre.mrg._eb_ident[0] ||
		      msg->_eb_ident1 != ask_ident->pre.mrg._eb_ident[1] ||
		      msg->_eb_ident2 != ask_ident->pre.mrg._eb_ident[2])
		    {
		      LWROC_CERROR(&eb_master->_out._base,
				   "EB master sent IDENT answer "
				   "with wrong ident.");
		      /* Communication mishap - master broken? */
		      /* Tear connection down! */
		      lwroc_net_trigbus_discon(eb_master);
		      break;
		    }

		  if (msg->_type ==
		      LWROC_TRIVA_CONTROL_EB_GOOD_IDENT)
		    {
		      /*
			printf("Validator pass input on %p.\n", ask_ident);
		      */

		      /* We need to get the iterator for this item! */

		      PD_LL_REMOVE(&ask_ident->_val_sources);

		      LWROC_ITEM_BUFFER_INSERT(_lwroc_merge_info._fresh_sort_sources,
					       ask_ident);
		    }
		  else
		    {
		      /*
			printf("Validator rejects input on %p.\n", ask_ident);
		      */

		      /* So it was a bad identification event, i.e.
		       * old.  Get another validation event.
		       */

		      ask_ident->_val_state = LWROC_MERGE_VAL_FETCH_EVENT;
		    }

		  ask_ident = NULL;

		  FALL_THROUGH;

		case LWROC_TRIVA_CONTROL_EB_QUERY_IDENT:
		  /* In case we have no-one to query. */
		  msg->_type = LWROC_TRIVA_CONTROL_EB_NO_IDENT;
		  if (_lwroc_merge_info._revalidate_sources)
		    {
		      /* We do not start to move sources to the merger
		       * until it has expelled all sources with old
		       * validation.
		       */
		      msg->_eb_srcs_waiting =
			(uint32_t) _lwroc_merge_info._num_sources;
		      break;
		    }
		  MFENCE; /* Must read _num_activated_sources after
			   * _revalidate_sources. */
		  msg->_eb_srcs_waiting =
		    (uint32_t) (_lwroc_merge_info._num_sources -
				_lwroc_merge_info._num_activated_sources);

		  /* Do we have any outstanding sources to verify? */

		  PD_LL_FOREACH(_lwroc_merge_val_sources, iter)
		    {
		      lwroc_merge_source *conn =
			PD_LL_ITEM(iter, lwroc_merge_source, _val_sources);

		      if (conn->_val_state != LWROC_MERGE_VAL_QUERY_IDENT)
			continue;

		      ask_ident = conn;

		      ask_ident->_val_state = LWROC_MERGE_VAL_ASK_IDENT;

		      msg->_type = LWROC_TRIVA_CONTROL_EB_ASK_IDENT;
		      msg->_eb_ident0 = ask_ident->pre.mrg._eb_ident[0];
		      msg->_eb_ident1 = ask_ident->pre.mrg._eb_ident[1];
		      msg->_eb_ident2 = ask_ident->pre.mrg._eb_ident[2];
		      /*
		      printf ("Ask IDENT %08x %08x %08x\n",
			      msg->_eb_ident0,
			      msg->_eb_ident1,
			      msg->_eb_ident2);
		      */
		      break;
		    }

		  break;

		case LWROC_TRIVA_CONTROL_EB_STATUS_QUERY:
		  /* Are all our sources still working? */
		  msg->_eb_srcs_waiting =
		    (uint32_t) (_lwroc_merge_info._num_sources -
				_lwroc_merge_info._num_active_sources);
		  msg->_type = LWROC_TRIVA_CONTROL_EB_STATUS_RESPONSE;
		  break;

		default:
		  LWROC_CERROR(&eb_master->_out._base,
			       "Unexpected message from EB master.");
		  lwroc_net_trigbus_discon(eb_master);
		  break;
		}

	      msg->status = 0;

	      if (_lwroc_main_thread->_terminate)
		{
		  /* printf ("tell master that we want to quit\n"); */
		  msg->status |= LWROC_READOUT_STATUS_TERM_REQ;
		}

	      msg->delayed_eb_status =
		_lwroc_merge_info._delayed_eb_need_force;

	      if (_lwroc_bug_fatal_reported)
		msg->status |= LWROC_READOUT_STATUS_STHR_BUG_FATAL;

	      /* printf ("send message %x\n", msg->_type); */
	      lwroc_net_trigbus_send_msg(eb_master, 0);
	    }
	}

      /* Then go through all sources that we have, and see if something
       * can be done.
       */

      PD_LL_FOREACH_TMP(_lwroc_merge_val_sources, iter, tmp_iter)
	{
	  lwroc_merge_source *conn =
	    PD_LL_ITEM(iter, lwroc_merge_source, _val_sources);

	  /* printf ("val %p : %d\n", conn, conn->_val_state); */

	  while (conn->_val_state == LWROC_MERGE_VAL_FETCH_EVENT)
	    {
	      lmd_event_header_host header;
	      uint32_t *data;

	      int ret =
		lwroc_merge_fetch_event_header(conn,
					       _lwroc_merge_val_thread->_block,
					       &header, &data, 0);

	      switch (ret)
		{
		case LWROC_FETCH_EVENT_WOULD_BLOCK:
		  goto next_val_conn;

		case LWROC_FETCH_EVENT_SUCCESS:
		case LWROC_FETCH_EVENT_IDENT:
		  break;

		case LWROC_FETCH_EVENT_FAILURE:
		case LWROC_FETCH_EVENT_QUIT:
		broken_input:
		  /* This source has failed and needs to be recycled to
		   * the input thread.
		   */
		  /*
		    printf("Validator rejects failed input %p.\n", conn);
		  */

		  PD_LL_REMOVE(iter);
		  LWROC_ITEM_BUFFER_INSERT(_lwroc_merge_info._dead_val_sources,
					   conn);
		  conn->_source_remove_reconnect = 1;
		  goto next_val_conn;

		default:
		  LWROC_BUG_FMT("Unhandled fetch code %d in valdidator.", ret);
		}

	      /* LWROC_FETCH_EVENT_SUCCESS */

	      /* Normal event, then skip it. */

	      if (header.i_type    == LMD_EVENT_10_1_TYPE &&
		  header.i_subtype == LMD_EVENT_10_1_SUBTYPE)
		{
		consume_event:
		  lwroc_merge_done_event(conn);
		  /* printf ("val skipped normal event.\n"); */
		  consumed_data = 1;
		  continue; /* Try next one. */
		}

	      if (header.i_type    == LMD_EVENT_STICKY_TYPE &&
		  header.i_subtype == LMD_EVENT_STICKY_SUBTYPE)
		{
		  /* Insert the event into the pending sticky store.
		   * Revokes need to be retained.
		   */
		  lwroc_lmd_sticky_store_insert(conn->_sticky_store_val_pending,
						conn->pre.ptr,
						conn->pre.event_size,
						0 /* discard_revoke */,
						0 /* as_active */);
		  goto consume_event;
		}

	      /*
	      printf ("val %p : %d has event %d/%d\n",
		      conn, conn->_val_state,
		      header.i_type, header.i_subtype);
	      */

	      if (header.i_type    != LWROC_LMD_IDENT_TYPE ||
		  header.i_subtype != LWROC_LMD_IDENT_SUBTYPE)
		{
		  LWROC_CERROR_FMT(&conn->_out._base,
				   "Event has unknown type/subtype (%d/%d).",
				   header.i_type,
				   header.i_subtype);
		  goto broken_input;
		}

	      /* Account. */
	      {
		uint32_t data_size =
		  conn->pre.event_size -
		  (uint32_t) sizeof (lmd_event_header_host);
		uint32_t expect_size = 3 * sizeof (uint32_t);

		if (data_size != expect_size)
		  {
		    LWROC_CERROR_FMT(&conn->_out._base,
				     "IDENT pseudo-event with wrong size "
				     "(%d, expect %d).",
				     data_size, expect_size);
		    goto broken_input;
		  }
		/*
		printf ("Seen IDENT %08x %08x %08x\n",
			data[0], data[1], data[2]);
		*/
		conn->pre.mrg._eb_ident[0] = data[0];
		conn->pre.mrg._eb_ident[1] = data[1];
		conn->pre.mrg._eb_ident[2] = data[2];
	      }

	      /* Eat the event. */

	      lwroc_merge_done_event(conn);

	      /* Ident event, then queue it up to validate its info. */

	      conn->_val_state = LWROC_MERGE_VAL_QUERY_IDENT;

	      /* Try to ping the master. */
	      lwroc_net_trigbus_send_look_at_me(eb_master);
	    }

	next_val_conn:
	  ;
	}

      if (consumed_data)
	continue;

      if (_lwroc_merge_info._changed_sources)
	{
	  _lwroc_merge_info._changed_sources = 0;
	  MFENCE;
	  lwroc_net_trigbus_send_look_at_me(eb_master);
	}

      /* There is no more data to process.
       * We need either more data, or some new source.
       */

      for (i = 0; i < 2; i++)
	{
	  LWROC_ITEM_BUFFER_SET_NOTIFY(_lwroc_merge_info._to_val_sources[i],
				       _lwroc_merge_val_thread->_block);

	  /* Something became available in the meantime (possibly
	   * without a token).
	   */

	  if (LWROC_ITEM_BUFFER_NONEMPTY(_lwroc_merge_info._to_val_sources[i]))
	    goto loop_again;
	}

      /* Wait. */

      lwroc_thread_block_get_token(_lwroc_merge_val_thread->_block);

    loop_again:
      ;
    }
}

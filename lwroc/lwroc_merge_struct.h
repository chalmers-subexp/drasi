/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_MERGE_STRUCT_H__
#define __LWROC_MERGE_STRUCT_H__

#include "lwroc_pipe_buffer.h"
#include "lwroc_select_util.h"
#include "lwroc_item_buffer.h"

#include "lwroc_net_outgoing.h"
#include "lwroc_net_legacy.h"
#include "lmd/lwroc_lmd_event.h"
#include "lmd/lwroc_lmd_sticky_store.h"
#include "ebye/lwroc_ebye_event.h"

#include "lwroc_merge_input_type.h"
#include "lwroc_data_pipe.h"

#include "../dtc_arch/acc_def/time_include.h"
#include "../dtc_arch/acc_def/netinet_in_include.h"

#include "../dtc_arch/acc_def/mystdint.h"
#include <netdb.h>
#include <arpa/inet.h>

/* Handling of fragmented events:
 *
 * Internally, we never fragment events in the buffers.  Which also
 * means that an event never wraps the circular buffer.  This
 * simplifies any part of code that need to inspect an event.
 *
 * New network protocols are also designed to not send fragmented
 * events.  They are sent as whole.  The receiver can anyhow not do
 * anything useful with it until it has fully arrived.
 *
 * We are thus just left with the task of being able to read
 * fragmented events from the transport protocol, without having it
 * end up wrapping the buffer.  And without having to copy it.
 *
 * Normally, when reading from the transport protocol, there are three
 * pieces of data: header, data and end padding.  The header is read
 * into an internal structure, while the data is read directly into
 * the buffer sent to the merger.  Thus the event data is provided
 * directly.  The padding is also read into the buffer, but just
 * ignored.
 *
 * For a fragmented event, there will in the next buffer be more to
 * observe: header, dummy event header, data (of event), data of
 * following events and padding.  The dummy event header shall not
 * make it to the event stream and must therefore also be ignored.
 * The reason to divide the data between the event and following
 * events is for buffer allocation purposes, see below.
 *
 * One should also note that the real event header of the event will
 * have not the event length set, but the length of the event in its
 * first buffer.  It must therefore be found and rewritten in the
 * buffer after the first buffer has been read.  This in turn requires
 * the full buffer to be inspected, since nothing tells where the last
 * (fragmented) event starts.
 *
 * Data buffer allocation is rather straightforward.  When a
 * fragmented event starts, the buffer header tells how long the last
 * event fully is.  Therefore, when about to allocate the buffer for
 * that part of the data, we add the that full length to the amount to
 * be allocated.  It will be too much by the amount the event is
 * inside the first buffer, but never too small.
 *
 * We always read the fragmented data by itself and not together with
 * the following event data or padding.  This ensures that the
 * allocation is enough for the event, and also ensures that a stream
 * of events that are continuously fragmented in buffers could be
 * handled.
 */

/* Fakernet preparation: reset TCP via UDP. */
#define LWROC_MERGE_IN_STATE_UDP_CREATE                 1
#define LWROC_MERGE_IN_STATE_UDP_WAIT_CONNECTED         2
#define LWROC_MERGE_IN_STATE_UDP_RESET_WRITE_PREPARE    3
#define LWROC_MERGE_IN_STATE_UDP_RESET_WRITE            4
#define LWROC_MERGE_IN_STATE_UDP_RESET_READ_PREPARE     5
#define LWROC_MERGE_IN_STATE_UDP_RESET_READ             6
#define LWROC_MERGE_IN_STATE_UDP_RESET_READ_CHECK       7
/* Normal operation: */
#define LWROC_MERGE_IN_STATE_DATA_CREATE               11
#define LWROC_MERGE_IN_STATE_DATA_WAIT_CONNECTED       12
#define LWROC_MERGE_IN_STATE_DATA_SEND_ORDERS_PREPARE  13
#define LWROC_MERGE_IN_STATE_DATA_SEND_ORDERS  /*unused*/
#define LWROC_MERGE_IN_STATE_DATA_INFO_READ_PREPARE    15
#define LWROC_MERGE_IN_STATE_DATA_INFO_READ            16
#define LWROC_MERGE_IN_STATE_DATA_INFO_READ_CHECK      17
#define LWROC_MERGE_IN_STATE_DATA_SETUP_READ_PREPARE   18
#define LWROC_MERGE_IN_STATE_DATA_SETUP_READ           19
#define LWROC_MERGE_IN_STATE_DATA_SETUP_READ_CHECK     20
#define LWROC_MERGE_IN_STATE_DATA_FILHE_READ_PREPARE   21
#define LWROC_MERGE_IN_STATE_DATA_FILHE_READ           22
#define LWROC_MERGE_IN_STATE_DATA_FILHE_READ_CHECK     23
#define LWROC_MERGE_IN_STATE_DATA_BUFHE_READ_PREPARE   24
#define LWROC_MERGE_IN_STATE_DATA_BUFHE_READ           25
#define LWROC_MERGE_IN_STATE_DATA_BUFHE_READ_CHECK     26
#define LWROC_MERGE_IN_STATE_DATA_FEVHE_READ_PREPARE   27
#define LWROC_MERGE_IN_STATE_DATA_FEVHE_READ           28
#define LWROC_MERGE_IN_STATE_DATA_FEVHE_READ_CHECK     29
#define LWROC_MERGE_IN_STATE_DATA_FRAG_READ_PREPARE    30
#define LWROC_MERGE_IN_STATE_DATA_FRAG_READ            31
#define LWROC_MERGE_IN_STATE_DATA_FRAG_READ_CHECK      32
#define LWROC_MERGE_IN_STATE_DATA_CHUNK_READ_PREPARE   33
#define LWROC_MERGE_IN_STATE_DATA_CHUNK_READ           34
#define LWROC_MERGE_IN_STATE_DATA_CHUNK_READ_CHECK     35

#define LWROC_MERGE_IN_STATE_EBYE_PUSH_WAIT_CLIENT     36

#define LWROC_MERGE_IN_STATE_EBYE_XFER_READ_PREPARE    37
#define LWROC_MERGE_IN_STATE_EBYE_XFER_READ            38
#define LWROC_MERGE_IN_STATE_EBYE_XFER_READ_CHECK      39
#define LWROC_MERGE_IN_STATE_EBYE_RECORD_READ_PREPARE  40
#define LWROC_MERGE_IN_STATE_EBYE_RECORD_READ          41
#define LWROC_MERGE_IN_STATE_EBYE_RECORD_READ_CHECK    42
#define LWROC_MERGE_IN_STATE_EBYE_CHUNK_READ_PREPARE   43
#define LWROC_MERGE_IN_STATE_EBYE_CHUNK_READ           44
#define LWROC_MERGE_IN_STATE_EBYE_CHUNK_READ_CHECK     45

#define LWROC_MERGE_IN_STATE_FAILED_INJECT_ENDMARK     46
#define LWROC_MERGE_IN_STATE_FAILED_WAIT_DEAD          47

#define LWROC_MERGE_IN_STATE_FAILED_SLEEP              48
#define LWROC_MERGE_IN_STATE_FAILED_FILE               49
#define LWROC_MERGE_IN_STATE_SHUTDOWN                  50

/* Dummy state, after file open. */
#define LWROC_MERGE_IN_STATE_FILE_FMT_SWITCH           51


#define LWROC_MERGE_IN_PMAP_PMAP_PORT                   0
#define LWROC_MERGE_IN_PMAP_LEGACY_PORT                 1
#define LWROC_MERGE_IN_PMAP_DATA_PORT                   2

#define LWROC_MERGE_VAL_FETCH_EVENT   1
#define LWROC_MERGE_VAL_QUERY_IDENT   2
#define LWROC_MERGE_VAL_ASK_IDENT     3

#define LWROC_MERGE_SOURCE_STATUS_ADDED          0x01
/* #define LWROC_MERGE_SOURCE_STATUS_REMOVED        0x02 */
#define LWROC_MERGE_SOURCE_STATUS_REMOVE_LISTED  0x04

#define LWROC_MERGE_SOURCE_EVENT_10_1_SET_HEADER  0x01
#define LWROC_MERGE_SOURCE_EVENT_10_1_SET_INFO    0x02

#define LWROC_MERGE_SOURCE_FLAGS_LOOP   0x01
#define LWROC_MERGE_SOURCE_FLAGS_DEBUG  0x02

#define LWROC_MERGE_DISABLED_NODATA    1
#define LWROC_MERGE_DISABLED_WAITDATA  2  /* No data in no-sort mode (xfer). */
#define LWROC_MERGE_DISABLED_BROKEN    3

typedef struct lwroc_merge_source_t
{
  lwroc_net_outgoing _out;

  /* int _state; */
  /* int _fd;    */

  int _type;

  const char *_label;

  /* Used for transport server port mapping. */
  uint16_t _port_map;  /* Use method at next attempt. */

  /*struct timeval _next_time;
    uint32_t _last_failure; */

  int _had_first_complete_event;

  /* struct sockaddr_in _serv_addr; */

  /* lwroc_select_item _select_item; */

  /*struct
  {
    size_t _size;
    size_t _offset;
    char  *_ptr;
    } _buf; */

  size_t _buffer_size;
  double _buffer_frac;

  union
  {
    s_bufhe_host                _bufhe;
    ltcp_stream_trans_open_info _info;
    ebye_record_header          _ebye_record;
    ebye_xfer_header            _ebye_xfer;
    uint32_t                    _raw[8];
  } _data;

  char    *_chunk_start;
  char    *_chunk_end;
  char    *_chunk_reserve_end;
  int      _swap32;

  /* Handling of fragmented event input: */

  lmd_event_header_host _fevhe;
  size_t                _expect_fragment;
  size_t                _fragment_length;
  uint32_t              _buffer_no;

  /* Sizes of pieces to read. */

  size_t   _used_left;
  size_t   _padding_left;

  /* Maximum buffer size given in start message. */
  size_t   _max_buffer_size;
  /* Maximum size of event given on command line.
   * Overrides estimate from buffer size.
   * May not be smaller than actual size reported in start message of
   * a drasi server.
   */
  size_t   _max_ev_len_config;
  /* Actual maximum event size.  Each event is checked vs. this limit.
   * Either comes from command line directive or start message.  If
   * violated, the source is disconnected (to show our contempt).  But
   * soon reconnected again.  (Since we do not crash, but expect the
   * user to do something about the issue...)
   *
   * Before the merger starts to process events, the sum (in case of
   * building), or max (in case of sorting) is checked vs. its maximum
   * output size.
   */
  size_t   _max_ev_len;
  /* String describing who promised the maximum event size. */
  const char *_max_ev_len_source;

  /* Threshold above which other transmissions for event building need
   * to be forced.
   */
  size_t   _force_threshold;

  /* Items above are used by the input thread. */

  lwroc_pipe_buffer_control *_buffer;
  lwroc_pipe_buffer_consumer *_buffer_consumer;

  /* The following flags are used to circulate the buffers
   * between the input thread and the sorting thread.
   *
   * As they are accessed by different threads, it is important that
   * they cannot stomp on any adjacent variables.  On e.g.  ALPHA,
   * they cannot be in char variables, as they do masking in the
   * processor, but (re)write a much larger memory region (64 bits).
   */

  volatile int _status_out_of_data;

  /* Index of source, to keep fixed order for event mode merging. */

  size_t _src_index;

  /* Source has been added or removed to/from active status. */

  int _source_add_removed;

  /* Source has been reconnected.  May be written by validation thread. */

  volatile int _source_remove_reconnect;

  /* Timeout before disabling a time-sort source. */

  int _ts_disable_timeout;

  int _keepalive_timeout;

  /* Timestamp (e.g. WR) id(s) expected (= allowed). */

  uint64_t _ts_id_mask;

  /* Loop a file?  Debug incoming headers? */

  int _flags;

  /* Source has been disabled due to having no data.  (timesort) */

  volatile int _disabled_nodata;

  /* Time when we reported to be disabled. */

  time_t _disabled_at;

  /* Last time we got a complete event. */

  volatile struct timeval _last_got_data;

  /* First time we got bad timestamps (in this streak). */

  struct timeval _first_bad_stamp;

  /* Sticky events, that (while validated and contributing) are
   * active.
   */

  lwroc_lmd_sticky_store *_sticky_store_full;

  /* Sticky events seen by the validation thread.  Once the source is
   * validated, they are ejected and added(moved) to the full list above.
   *
   * This is handled by the validation thread.  The store is only
   * modified by the merge thread when the source is being activated
   * in it, thus no competition by the validation thread.
   */

  lwroc_lmd_sticky_store *_sticky_store_val_pending;

  /* Items below are used by the merging thread. */

  struct
  {
    size_t avail;
    char  *ptr;

    /* If avail above reports data available, the relevant unions
     * below are filled out.  Failure to parse such data will have
     * led to the input source being disconnected.
     */

    uint32_t event_size;

    uint32_t sync_check;

    union
    {
      struct
      {
	/* When time-sorting, we need the next time. */

	uint64_t _next;

	/* For debugging, we need the previous timestamp. */
	/* Must be next member, due to conn monitor timestamp_ptr. */

	uint64_t _prev;

	/* For EBYE, keep track of the current mid and hi part of the
	 * timestamp.
	 */
	uint64_t _cur_mid_hi;

	/* We need the branch ID, for the timestamp analysis. */

	uint32_t _branch_id;

      } stamp;

      /* During validation, we need the ident info. */

      uint32_t _eb_ident[3];

    } mrg;

  } pre;

  /* When (subevent)-merging, we need the trigger and event number,
   * for checking.  Also needed in sorting mode (to know if sticky).
   */

  lmd_event_10_1_host event_10_1;

  int _event_10_1_context_set;

  int _include;

  /* When time-sorting, we periodically check that the source is not
   * lagging the other sources (due to future time-stamps).
   */

  struct
  {
    uint64_t       _next_seen; /* Timestamp seen. */
    struct timeval _time_seen; /* Time when we had seen move. */
    int            _report_interval;

  } lag;

  /* Used by the validation thread. */

  int _val_state;

  pd_ll_item _val_sources;
  pd_ll_item _all_sources;

} lwroc_merge_source;

extern PD_LL_SENTINEL_ITEM(_lwroc_merge_sources);
extern PD_LL_SENTINEL_ITEM(_lwroc_merge_val_sources);
extern PD_LL_SENTINEL_ITEM(_lwroc_merge_all_sources);

typedef struct lwroc_merge_in_unit_t
{
  lwroc_select_item _select_item;

  int _server_fd;

  int _server_port;

  lwroc_merge_source *_conn;

  pd_ll_item _servers;

  /* Easy formatting of our name and kind for messages. */
  lwroc_format_message_context _msg_context;
} lwroc_merge_in_unit;

extern PD_LL_SENTINEL_ITEM(_lwroc_merge_in_serv);

typedef struct lwroc_merge_source_opt_t
{
  pd_ll_item _source_opts;

  const char *_config;
  int _type;

  lwroc_merge_source *_conn;

} lwroc_merge_source_opt;

extern PD_LL_SENTINEL_ITEM(_lwroc_merge_source_opts);

typedef LWROC_STRUCT_ITEM_BUFFER(lwroc_buffer_merge_sources_t,
				 lwroc_merge_source *)
  lwroc_buffer_merge_sources;

#define LWROC_MERGE_MODE_EVENT_BUILDER  0x01
#define LWROC_MERGE_MODE_TITRIS         0x02
#define LWROC_MERGE_MODE_WHITE_RABBIT   0x04
#define LWROC_MERGE_MODE_EBYE_HITS      0x08
#define LWROC_MERGE_MODE_EBYE_UNSORTED  0x10
#define LWROC_MERGE_MODE_XFER_BLOCK     0x20

#define LWROC_MERGE_MODE_EB              (LWROC_MERGE_MODE_EVENT_BUILDER)
#define LWROC_MERGE_MODE_TS              (LWROC_MERGE_MODE_TITRIS |	   \
					  LWROC_MERGE_MODE_WHITE_RABBIT |  \
					  LWROC_MERGE_MODE_EBYE_HITS |	   \
					  LWROC_MERGE_MODE_EBYE_UNSORTED | \
					  LWROC_MERGE_MODE_XFER_BLOCK)
#define LWROC_MERGE_MODE_TS_PASSTHROUGH  (LWROC_MERGE_MODE_XFER_BLOCK)

#define LWROC_MERGE_MODE_LMD             (LWROC_MERGE_MODE_EVENT_BUILDER | \
					  LWROC_MERGE_MODE_TITRIS |	   \
					  LWROC_MERGE_MODE_WHITE_RABBIT)
#define LWROC_MERGE_MODE_EBYE            (LWROC_MERGE_MODE_EBYE_HITS |	   \
					  LWROC_MERGE_MODE_EBYE_UNSORTED)
#define LWROC_MERGE_MODE_XFER            (LWROC_MERGE_MODE_XFER_BLOCK)

typedef struct lwroc_merge_info_t
{
  lwroc_buffer_merge_sources _fresh_sort_sources; /* IN/VAL share -> MERGE */
#define _fresh_val_sources _to_val_sources[0]     /* IN share -> VAL */
#define _redo_val_sources  _to_val_sources[1]     /* MERGE reshare -> VAL */
  lwroc_buffer_merge_sources _to_val_sources[2];
#define _dead_sort_sources _dead_sources[0]       /* MERGE unshare -> IN */
#define _dead_val_sources  _dead_sources[1]       /* VAL unshare -> IN */
  lwroc_buffer_merge_sources _dead_sources[2];

  lwroc_buffer_merge_sources _connected_sources; /* NET -> IN */
  lwroc_buffer_merge_sources _discon_sources;    /* IN  -> NET */
#if 0
  /* This single ping-pong interface to transport established
   * connections from the input thread to the sorting thread, and then
   * again broken ones from the sorting to the input thread is not too
   * efficient when multiple items are to be moved.  But: it is used
   * very seldom, basically only when things not are working.  And
   * then it does not matter if there is an overhead in terms of
   * ping-pong between the threads.
   */

  /* Write by the input, consume and clear by the sorter. */
  lwroc_merge_source *volatile _fresh_source;
  /* Input needs to be notified of either a new dead source, or that a fresh
   * has been consumed (as there are more fresh ones waiting).
   */
  const lwroc_thread_block *volatile _notify_input;

  /* Write by the sorter, consume and clear by the input. */
  lwroc_merge_source *volatile _dead_source;
  /* Sorter needs to be notified of either a new fresh source, or that
   * a dead was consumed (as there are more dead ones waiting).
   */
  const lwroc_thread_block *volatile _notify_sort;
#endif

  lwroc_data_pipe_handle *_data_handle;

  lwroc_gdf_format_functions *_data_fmt;

  lwroc_net_conn_monitor *_mon;

  volatile int _revalidate_sources;

  /* Some change happened - validator should LAM master, in case it
   * wants to react.
   */
  volatile int _changed_sources;

  /* Written by validator thread, inspected by merge_in thread. */
  volatile uint32_t _delayed_eb_enabled;
  volatile uint32_t _delayed_eb_need_force;
  /* Validator needs to be notified to inspect for force requirement. */
  const lwroc_thread_block *volatile _notify_force_need;

  /* */
  int _mode;
  uint32_t event_number;

  int _sticky_pending; /* Used internally by sorter. */

  int _sticky_update_pending; /* Sources removed or added as active. */

  /* Most recent (good) timestamp. */
  uint64_t _last_good_sort;

  lwroc_merge_source **_sources;
  volatile size_t _num_active_sources;
  volatile size_t _num_activated_sources;
  lwroc_merge_source **_sources_removed;
  volatile size_t _num_removed_sources;
  volatile size_t _num_finished_sources;
  size_t _num_sources; /* The maximum number of source = number configured. */

  /* Hold off stall detector report. */
  struct timeval _future_stall_detector_after;

  /* We might have data again for a source that was kicked from timesorting. */
  volatile int _check_nodata_sources;
  /* Should disabled sources be reconnected? */
  int _try_reconnect_disabled_sources;

#if 0
  /* Maximum disable timeout value. */
  int _max_ts_disable_timeout;
  /* Anyone with a 0 (infinity) timeout? */
  int _zero_ts_disable_timeout;
#endif

  /* Current outstanding control request. */
  lwroc_merge_request *_merge_req;

} lwroc_merge_info;

extern lwroc_merge_info _lwroc_merge_info;

#endif/*__LWROC_MERGE_STRUCT_H__*/

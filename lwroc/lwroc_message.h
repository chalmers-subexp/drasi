/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_MESSAGE_H__
#define __LWROC_MESSAGE_H__

#if !NO_LWROC_MESSAGE

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include "../dtc_arch/acc_def/mystdint.h"

#include "lwroc_message_inject.h"

#include "../dtc_arch/acc_def/variadic_macros.h"

#define LWROC_PERROR(s)							\
  do { lwroc_perror_internal(NULL, __FILE__, __LINE__, s); } while (0)
#define LWROC_CPERROR(c,s)						\
  do { lwroc_perror_internal(&((c)->_msg_context),			\
			     __FILE__, __LINE__, s); } while (0)

#include "gen/lwroc_message_macros.h"

#ifdef __cplusplus
extern "C" {
#endif

extern uint32_t _lwroc_debug_mask_enabled;

#ifdef __cplusplus
}
#endif

/* Flags to control rate-limiting of message printing.
 *
 * Only used for messages which would otherwise hinder unrelated
 * (working) systems to proceed at high speed, e.g. in diagnostics of
 * malfunctioning time-sorter sources.
 */
#define LWROC_MESSAGE_RATE_CTRL_LIMIT          0x0001 /* Limit rate */

void lwroc_message_rate_ctrl(int ctrl);

#endif/*!NO_LWROC_MESSAGE*/

/* Does not really belong here, but... */

char *strdup_chk(const char *s);
char *strndup_chk(const char *s, size_t n);

#endif/*__LWROC_MESSAGE_H__*/

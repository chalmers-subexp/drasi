/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_MESSAGE_INJECT_H__
#define __LWROC_MESSAGE_INJECT_H__

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

#include "lwroc_message_level.h"

/* Be careful with this header!
 *
 * It is used to allow user readout routines to interface with
 * message the injection, without including the error macros.
 *
 * Do not put unnecessary stuff here.
 */

#ifdef __cplusplus
extern "C" {
#endif

typedef char * (*lwroc_format_message_context)(char *buf, size_t size,
					       const void *ptr);

void lwroc_perror_internal(const lwroc_format_message_context *context,
			   const char *file, int line,
			   const char *s);
void lwroc_message_internal(int level,
			    const lwroc_format_message_context *context,
			    const char *file, int line,
			    const char *fmt,...)
  __attribute__ ((__format__ (__printf__, 5, 6)));

char *lwroc_message_fmt_msg_context(char *buf, size_t size,
				    const char *fmt,...)
  __attribute__ ((__format__ (__printf__, 3, 4)));

#ifdef __cplusplus
}
#endif

#endif/*__LWROC_MESSAGE_INJECT_H__*/

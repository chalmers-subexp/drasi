/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

/* Must be early, or stdio may miss vs(n)printf. */
#include <stdarg.h>

#include "lwroc_message_internal.h"
#include "lwroc_net_proto.h"
#include "lwroc_message.h"
#include "lwroc_main_iface.h"
#include "lwroc_hostname_util.h"
#include "hwmap_error.h"

#include "gen/lwroc_message_source_serializer.h"
#include "gen/lwroc_message_item_serializer.h"

#include "../dtc_arch/acc_def/has_pthread_getname_np.h"
#include "../dtc_arch/acc_def/has_strerror_r.h"
#include "../dtc_arch/acc_def/has_vsnprintf.h"

#include <stdio.h>
#include <string.h>
#include "../dtc_arch/acc_def/netinet_in_include.h"
#include <arpa/inet.h>

#include <stdlib.h>
#include "../dtc_arch/acc_def/time_include.h"
#include "../dtc_arch/acc_def/has_backtrace.h"
#include <pthread.h>
#include <errno.h>
#include <unistd.h>
#include <assert.h>

#include <sys/stat.h>
#include <sys/types.h>

#include "../dtc_arch/acc_def/myinttypes.h"
#include "../dtc_arch/acc_def/myprizd.h"

#include "../dtc_arch/acc_def/has_curses.h"
#if HAS_CURSES
#define USE_CURSES 1
#endif
#include "../lu_common/colourtext.h"

#include "git-describe-include.h"

#define DEBUG_MSG_WRITER_CONSUMER 0

volatile lwroc_hardware_cleanup _lwroc_hardware_cleanup = NULL;

uint32_t _lwroc_debug_mask_enabled = 0;

int _lwroc_message_local = 0;
int _lwroc_message_no_sleep = 0;

volatile int _lwroc_message_printed = 0;

volatile uint32_t _lwroc_bug_fatal_reported = 0;

/* This should not be used globally, use _lwroc_shutdown. */
volatile int _lwroc_quit_count = 0;

const char *_lwroc_main_argv0 = NULL;

volatile int _lwroc_message_client_seen = 0;

/* Should perhaps be elsewhere. */
volatile int _lwroc_threads_created = 0;

/* Do not do rate limit until setup is done.  Set to 0 in main(). */
int _lwroc_log_no_rate_limit = 1;

/* Wait for ack from client before returning from message handler.
 * Useful in case of hard crashes of the hardware.
 */
int _lwroc_log_ack_wait = 0;

int _lwroc_log_console_level = LWROC_MSGLVL_INFO;

static pthread_key_t _lwroc_msg_key;

#define LWROC_MSG_NUM_RECENT  32

typedef struct lwroc_message_recent_t
{
  const lwroc_format_message_context *_context;
  const char *_file;
  int      _line;

  int      _level;
  uint64_t _hits;
  uint64_t _unreported;

  time_t   _t_first;
  time_t   _t_reported;
  time_t   _t_next_report;

  struct timeval _t_last;

  char _msg[32];

} lwroc_message_recent;

/* Previous message shown (plain). */
#define LWROC_MESSAGE_RATE_LIMIT_SHOW  1
/* Previous message was shown, but with a limit mark printed. */
#define LWROC_MESSAGE_RATE_LIMIT_MARK  2
/* Previous message was eaten. */
#define LWROC_MESSAGE_RATE_LIMIT_EAT   3

typedef struct lwroc_message_buffer_t
{
  lwroc_pipe_buffer_control  *_msgs;
  lwroc_pipe_buffer_consumer *_msgs_consumer;

  const lwroc_thread_block *_thread_notify;

  uint32_t _source;

  int      _source_info_sent;

  int      _message_calls;

  int      _skipped_messages;

  int      _lost_messages;

  volatile uint32_t _last_ack_ident;

#if DEBUG_MSG_WRITER_CONSUMER
  FILE *_debug_file;
#endif

  int                  _has_recent_unreported;
  int                  _num_recent;
  lwroc_message_recent _recent[LWROC_MSG_NUM_RECENT];

  int                  _rate_ctrl;

  /* This item must be last as we append the serialised message! */
  uint32_t _source_info_wire[1];
  /* Nother here - hostname is here... */

} lwroc_message_buffer;

lwroc_message_buffer *_lwroc_msg_bufs[LWROC_MSG_BUFS_NUM];

const char *_lwroc_thread_names[LWROC_MSG_BUFS_NUM + 1] =
  { "MAIN", "NET", "FILE", "SERV", "IN", "VAL", "ANLY", "WDOG", "TCTL",
    "FILT", "TSFL", "SASY",
    "USR1", "USR2", "USR3",
    "\0" /* To catch bugs when missing entries. */};

/* Buffer size must be < 4 GB (or at least a power of 2), or the waste
 * markers need to be able to handle 64 bit values.
 */
#define LWROC_MSG_BUF_SIZE  0x2000 /* 8 KiB */

#if DEBUG_MSG_WRITER_CONSUMER
FILE *_debug_consumer = NULL;
#endif

const char _lwroc_msg_level_mark[LWROC_MSGLVL_MAX+1] =
  { LWROC_MSGLVL_MARKS };

#define HOSTNAME_LEN  256

void lwroc_msg_bufs_early_init(void)
{
  pthread_key_create(&_lwroc_msg_key, NULL);
}

void lwroc_msg_bufs_init(int server_port,
			 int argc,char *argv[])
{
  char hostname[HOSTNAME_LEN];
  int i, j;
#if DEBUG_MSG_WRITER_CONSUMER
  long starttime = time(NULL);
#endif

  lwroc_gethostname(hostname, sizeof (hostname));

  printf ("HOST: %s\n", hostname);

#if DEBUG_MSG_WRITER_CONSUMER
  {
    char filename[256];

    mkdir("debug_msg", 0777);

    snprintf(filename,sizeof(filename), "debug_msg/%ld", starttime);

    mkdir(filename, 0777);

    snprintf(filename,sizeof(filename), "debug_msg/%ld/consumer.txt", starttime);

    _debug_consumer = fopen(filename, "a");

    if (_debug_consumer == NULL)
      {
	fprintf (stderr, "Failed to open %s for appending.\n", filename);
      }
    if (_debug_consumer)
      {
	fprintf(_debug_consumer,
		"*** Consumer start: %ld\n", (long) time(NULL));
	fflush(_debug_consumer);
      }
  }
#endif

  for (i = 0; i < LWROC_MSG_BUFS_NUM; i++)
    {
      lwroc_message_source source_info;
      lwroc_message_source_sersize sz_source_info;
      size_t size_source;
      size_t size;
      const char *thread_name = _lwroc_thread_names[i];

      if (!thread_name[0])
	LWROC_BUG("_lwroc_thread_names missing items");

      source_info._source   = (uint32_t) i;
      source_info._hostname = hostname;
      source_info._port     = (uint16_t) server_port;
      source_info._mainthread = (uint16_t) (i == 0 ? 1 : 0);
      source_info._thread   = thread_name; /* we will not free! */
      /* TODO: should these be kept?
       * Same values as in lwroc_mon_source_set() ?  Reuse that?
       */
      source_info._mon_ident1   = 0;
      source_info._mon_ident2   = 0;
      source_info._mon_ident3   = 0;
      source_info._mon_ident4   = 0;

      size_source = lwroc_message_source_serialized_size(&source_info,
							 &sz_source_info);

      size = sizeof (lwroc_message_buffer) - sizeof (uint32_t) + size_source;

      _lwroc_msg_bufs[i] = (lwroc_message_buffer *)
	malloc (size);

      if (!_lwroc_msg_bufs[i])
	LWROC_FATAL("Failed to allocate message buffer.");

      lwroc_pipe_buffer_init(&_lwroc_msg_bufs[i]->_msgs,
			     1, NULL, LWROC_MSG_BUF_SIZE * (i == 0 ? 2 : 1),
			     0);

      _lwroc_msg_bufs[i]->_msgs_consumer =
	lwroc_pipe_buffer_get_consumer(_lwroc_msg_bufs[i]->_msgs, 0);

      lwroc_message_source_serialize((char *) &_lwroc_msg_bufs[i]->_source_info_wire,
				     &source_info, &sz_source_info);

      _lwroc_msg_bufs[i]->_thread_notify = NULL;
      _lwroc_msg_bufs[i]->_source_info_sent = 0;
      _lwroc_msg_bufs[i]->_source = (uint32_t) i;
      _lwroc_msg_bufs[i]->_message_calls = 0;
      _lwroc_msg_bufs[i]->_skipped_messages = 0;
      _lwroc_msg_bufs[i]->_lost_messages = 0;
      _lwroc_msg_bufs[i]->_num_recent = 0;
      _lwroc_msg_bufs[i]->_has_recent_unreported = 0;
      _lwroc_msg_bufs[i]->_rate_ctrl = 0;

#if DEBUG_MSG_WRITER_CONSUMER
      {
	char filename[256];

	snprintf(filename,sizeof(filename), "debug_msg/%ld/writer_%d.txt", starttime, i);

	_lwroc_msg_bufs[i]->_debug_file = fopen(filename, "a");

	if (_lwroc_msg_bufs[i]->_debug_file == NULL)
	  {
	    fprintf (stderr, "Failed to open %s for appending.\n", filename);
	  }
	if (_lwroc_msg_bufs[i]->_debug_file)
	  {
	    fprintf(_lwroc_msg_bufs[i]->_debug_file,
		    "*** Writer start: %ld\n", (long) time(NULL));
	    fflush(_lwroc_msg_bufs[i]->_debug_file);
	  }
      }
#endif
    }

  for (i = 0; i < 2; i++)
    {
      lwroc_pipe_buffer_control *buf = _lwroc_msg_bufs[0]->_msgs;
      lwroc_message_item item;
      lwroc_message_item_sersize sz_item;
      char *wire, *wire_start;
      uint32_t size;
      const char *msg_start = "Starting... " GIT_DESCRIBE_STRING " on ";
      const char *msg_cmdline = "Cmd:";
      uint32_t sz;
      struct timespec ts;
      char msg[LWROC_MSG_BUF_SIZE / 2]; /* Allow quite large command line. */

      assert (LWROC_MSG_BUF_SIZE / 2 > HOSTNAME_LEN + strlen(msg_start));

      if (i == 0)
	{
	  strcpy(msg, msg_start);
	  strcat(msg, hostname);

	  item._level  = (uint16_t) _lwroc_msg_level_mark[LWROC_MSGLVL_ACTION];
	}
      else
	{
	  char *p = msg;
	  size_t left = sizeof (msg) - 1; /* trailing 0 */
	  size_t n;

	  n = strlen(msg_cmdline);

	  strcpy(p, msg_cmdline);
	  p += n; left -= n;

	  for (j = 0; j < argc; j++)
	    {
	      const char *arg = argv[j];

	      n = strlen(arg);

	      if (n + 1 + 6 <= left) /* space for ' ' and one " [...]" */
		{
		  *(p++) = ' '; left--;
		  strcpy(p, arg);
		  p += n; left -= n;
		}
	      else
		{
		  strcpy(p, " [...]"); left -= 6;
		  break;
		}
	    }
	  assert(left == sizeof (msg) - 1 - strlen(msg));
	  item._level  = (uint16_t) _lwroc_msg_level_mark[LWROC_MSGLVL_LOG];
	}

      clock_gettime(CLOCK_REALTIME, &ts);

      item._time._sec  = (uint64_t) ts.tv_sec;
      item._time._nsec = (uint32_t) ts.tv_nsec;
      item._lost       = 0;
      item._source     = 0;
      item._file       = __FILE__;
      item._line       = __LINE__;
      item._ack_ident  = 0;
      item._msg        = (char *) msg;

      size = (uint32_t) lwroc_message_item_serialized_size(&item, &sz_item);

      sz = size;

      wire_start = wire = (char *)
	lwroc_pipe_buffer_alloc_write(buf, sz, NULL, NULL, 1);

      sz_item.size = sz;

      /* printf ("HW alignment -> %" MYPRIzd "\n", sz_item.size); */

      wire = lwroc_message_item_serialize(wire, &item, &sz_item);

      assert(wire == wire_start + sz);

      lwroc_pipe_buffer_did_write(buf, sz_item.size, 0);
    }
}

void lwroc_msg_thread_set_buf(int source,
			      const lwroc_thread_block *thread_notify)
{
  /* We are called from the thread in question, so can set the specific
   * value.
   */

  lwroc_message_buffer *buf = _lwroc_msg_bufs[source];
  const char *thread_name = _lwroc_thread_names[source];

  buf->_thread_notify = thread_notify;

  pthread_setspecific(_lwroc_msg_key, buf);

  if (source != 0)
    {
#if HAS_PTHREAD_GETNAME_NP
#define THREADNAMELEN 16
      char fullname[THREADNAMELEN];
      size_t len;
      char *p;
      pthread_t thread;
      size_t maxlen = 4; /* maximum name of thread (thread_names) */

      thread = pthread_self();

      if (pthread_getname_np(thread, fullname, sizeof (fullname)) != 0)
	{
	  fullname[0] = 0;
	}

      p = strchr(fullname, '\n');
      if (p)
	*p = 0;

      len = strlen(fullname);

      if (len > THREADNAMELEN - 1 - maxlen - 1)
	len = THREADNAMELEN - 1 - maxlen - 1;

      fullname[len] = 0;

      strcat(fullname, "/");
      strncat(fullname, thread_name, maxlen);

      pthread_setname_np(thread, fullname);
#else
      (void) thread_name;
#endif
    }
}

int lwroc_get_msg_buf_source(void)
{
  lwroc_message_buffer *msg_buf;

  msg_buf = (lwroc_message_buffer *) pthread_getspecific(_lwroc_msg_key);

  if (msg_buf)
    {
      return (int) msg_buf->_source;
    }

  return -1;
}

int lwroc_msg_num_msgs(void)
{
  int total = 0;
  int i;

  for (i = 0; i < LWROC_MSG_BUFS_NUM; i++)
    {
      total += _lwroc_msg_bufs[i]->_message_calls;
    }

  return total;
}

void lwroc_msg_bufs_new_client(void)
{
  int i;

  /* New client, all source info blocks must be sent first. */

  for (i = 0; i < LWROC_MSG_BUFS_NUM; i++)
    {
      _lwroc_msg_bufs[i]->_source_info_sent = 0;
    }

  if (!_lwroc_message_client_seen)
    {
      LWROC_INFO("Message client connected!");

      _lwroc_message_client_seen = 1;
      MFENCE;

      /* Wake all thread blocks up. */
      /* Everyone that might be waiting for this marker have also
       * registered as a potential message source, so we just wake all
       * sources up.
       */

      for (i = 0; i < LWROC_MSG_BUFS_NUM; i++)
	{
	  const lwroc_thread_block *thread_notify =
	    _lwroc_msg_bufs[i]->_thread_notify;

	  if (thread_notify)
	    lwroc_thread_block_send_token(thread_notify);
	}
    }
}

int lwroc_msg_bufs_read_avail(const lwroc_thread_block *thread_notify)
{
  int i;
  size_t avail;

  for (i = 0; i < LWROC_MSG_BUFS_NUM; i++)
    {
      if (!_lwroc_msg_bufs[i]->_source_info_sent)
	return 1;

      /* Note, this gets rid of waste space if available.  Otherwise
       * we do not get to know if there is a real message waiting.
       */
      avail = lwroc_pipe_buffer_avail_read(_lwroc_msg_bufs[i]->_msgs_consumer,
					   thread_notify, NULL, NULL, 0, 0);

      if (avail)
	return 1;
    }
  return 0;
}

/* We write one message at a time.  Rationale behind that is that they
 * then are ejected in time order between the different streams.  (If
 * two threads have produced them simultaneously so that their
 * time-stamps are ambiguous, well, then it does not matter really...)
 * Messages appear seldom, such that the additional cost of doing many
 * write and select system calls is of no concern.
 */

int lwroc_msg_bufs_source_info_read_avail(lwroc_msg_writing *writing)
{
  int i;

  for (i = 0; i < LWROC_MSG_BUFS_NUM; i++)
    {
      uint32_t *source_info_wire =
	_lwroc_msg_bufs[i]->_source_info_wire;

      if (_lwroc_msg_bufs[i]->_source_info_sent)
	continue;

      writing->_buf_consumer = NULL;
      writing->_data = (char *) source_info_wire;
      writing->_size = ntohl(*source_info_wire);

      /* We can mark already now, as it will be sent! */
      _lwroc_msg_bufs[i]->_source_info_sent = 1;

      /* printf ("src info: %" MYPRIzd "\n", writing->_size); */

      return 1;
    }
  return 0;
}

void lwroc_msg_bufs_oldest_read_avail(lwroc_msg_writing *writing)
{
  lwroc_message_item_wire *oldest_item = NULL;
  int i;
#if DEBUG_MSG_WRITER_CONSUMER
  int choose_i = -1;
#endif

  for (i = 0; i < LWROC_MSG_BUFS_NUM; i++)
    {
      lwroc_pipe_buffer_consumer *buf_consumer =
	_lwroc_msg_bufs[i]->_msgs_consumer;
      size_t avail, size;
      lwroc_message_item_wire *item;
      char *data;

      avail = lwroc_pipe_buffer_avail_read(buf_consumer, NULL,
					   &data, NULL, 0, 0);

      if (!avail)
	continue;

      /* Is it some skipping space? */

      item = (lwroc_message_item_wire *) data;

      size = ntohl(item->_header._msg_size);

#if DEBUG_MSG_WRITER_CONSUMER
      if (_debug_consumer)
	{
	  fprintf (_debug_consumer,
		   "[%d] size: [%p] %" MYPRIzd " (avail: %" MYPRIzd ")\n",
		   i, item, size, avail);
	  fflush(_debug_consumer);
	}
#endif

      assert (size <= avail);

      /* printf ("[%p] size: [%p] %" MYPRIzd " \n", buf, item, size); */

      if (item->_header._magic != htonl(LWROC_MESSAGE_ITEM_SERPROTO_MAGIC1))
	{
#if DEBUG_MSG_WRITER_CONSUMER
	  if (_debug_consumer)
	    {
	      fprintf (_debug_consumer,
		       "[%d] bad magic (0x%08x)\n",
		       i, ntohl(item->_header._magic));
	      fflush(_debug_consumer);
	    }
#endif

	  LWROC_BUG_FMT("Message item with bad magic "
			"(0x%08x, size %" MYPRIzd ").",
			ntohl(item->_header._magic), size);
	}

      assert(size >= sizeof (lwroc_message_item_wire));

      if (oldest_item)
	{
	  uint32_t *p_item_time = (uint32_t *) &item->_time;
	  uint32_t *p_oldest_item_time = (uint32_t *) &oldest_item->_time;

	  /* There are three uint32_t, starting with hi-word of the
	   * seconds, then lo-word, then nanoseconds.
	   */
	  if (ntohl(p_item_time[0]) > ntohl(p_oldest_item_time[0]) ||
	      (ntohl(p_item_time[0]) == ntohl(p_oldest_item_time[0]) &&
	       (ntohl(p_item_time[1]) > ntohl(p_oldest_item_time[1]) ||
		(ntohl(p_item_time[1]) == ntohl(p_oldest_item_time[1]) &&
		 ntohl(p_item_time[2]) > ntohl(p_oldest_item_time[2])))))
	    continue; /* Younger. */
	}

      /* printf ("%x %" MYPRIzd " %x\n", item->_header._msg_size, size, item->_header._magic); */

      if (size > avail)
	LWROC_BUG_FMT("Message item longer (%d) than "
		      "available buffer (linear) space (%" MYPRIzd ").",
		      ntohl(item->_header._msg_size), avail);

      writing->_buf_consumer = buf_consumer;
      writing->_data = data;
      writing->_size = size;
#if DEBUG_MSG_WRITER_CONSUMER
      choose_i = i;
#endif

      /* printf ("msg item: %" MYPRIzd "\n", writing->_size); */

      oldest_item = item;
    }

#if DEBUG_MSG_WRITER_CONSUMER
  if (choose_i != -1)
    if (_debug_consumer)
      {
	fprintf (_debug_consumer,
		 "[%d] take: [%p] %" MYPRIzd " @%ld\n",
		 choose_i,
		 writing->_data,
		 writing->_size,
		 time(NULL));
	fflush(_debug_consumer);
      }
#endif

  /* Prerequisite was that one buffer has data available.  Hmmm, we
   * might end up with no data to output if the only data available
   * was skipping space...
   *
   * In that case we return without setting writing->_data.
   */

  /* We are the transmitting thread.  If we have had some lost
   * messages, and that still is the case, then send a dummy message
   * to get the loss-message flushed.  Only needed if there are no
   * messages waiting.
   */
  {
    lwroc_message_buffer *msg_buf = _lwroc_msg_bufs[LWROC_MSG_BUFS_NET_IO];

    lwroc_pipe_buffer_consumer *buf = msg_buf->_msgs_consumer;

    if (msg_buf->_lost_messages &&
	lwroc_pipe_buffer_fill_due_to_consumer(buf) == 0)
      LWROC_LOG("Dummy message flush.");
  }
}

void lwroc_msg_buf_consume(lwroc_msg_writing *writing)
{
  if (writing->_buf_consumer)
    {
      /* If we were writing a message, it is now done. */
      lwroc_pipe_buffer_did_read(writing->_buf_consumer, writing->_size);
    }
  else
    {
      /* Was a source info block. */
    }
}

void lwroc_msg_bufs_skip_oldest(void)
{
  int i;

  /* We get called if there are no message receivers.  We discard
   * messages until 90 % of the buffer is free.  Such that we do not
   * block.  Also free up more space if the producer has told us a
   * certain amount is needed.
   */

  for (i = 0; i < LWROC_MSG_BUFS_NUM; i++)
    {
      lwroc_pipe_buffer_consumer *buf_consumer =
	_lwroc_msg_bufs[i]->_msgs_consumer;
      ssize_t bufsize =
	(ssize_t) lwroc_pipe_buffer_size_from_consumer(buf_consumer);
      ssize_t fill =
	(ssize_t) lwroc_pipe_buffer_fill_due_to_consumer(buf_consumer);
      ssize_t need = (ssize_t) lwroc_pipe_buffer_need_by_write(buf_consumer);
      ssize_t overthreshold = fill - (bufsize * 9) / 10;

      if (overthreshold > need)
	need = overthreshold;

      while (need > 0)
	{
	  size_t avail, size;
	  char *data;
	  lwroc_message_item_wire *item;

	  /*
	  printf ("%d: %" MYPRIzd " %" MYPRIzd " %" MYPRIzd " %" MYPRIzd "\n",
		  i, bufsize, fill, need, overthreshold);
	  */

	  avail =
	    lwroc_pipe_buffer_avail_read(buf_consumer, NULL,
					 &data, NULL, 0, 0);

	  if (!avail)
	    LWROC_BUG("Suddenly no data while skipping messages.");

	  /* We check if it is skipping space, such that we can
	   * correctly identify if we have skipped messages.
	   */

	  item = (lwroc_message_item_wire *) data;

	  size = ntohl(item->_header._msg_size);

	  assert (size <= avail);

	  if (item->_header._magic !=
	      htonl(LWROC_MESSAGE_ITEM_SERPROTO_MAGIC1))
	    {
	      LWROC_BUG_FMT("Message item with bad magic "
			    "(0x%08x, size %" MYPRIzd ") "
			    "while skipping.",
			    ntohl(item->_header._magic), size);
	    }
	  else
	    _lwroc_msg_bufs[i]->_skipped_messages++;

	  /* We do in any case skip the space. */
	  lwroc_pipe_buffer_did_read(buf_consumer, size);
	  need -= (ssize_t) size;
	}
    }
}

void lwroc_msg_bufs_got_ack(uint32_t source, uint32_t ack_ident)
{
  lwroc_message_buffer *msg_buf;

  if (source >= LWROC_MSG_BUFS_NUM)
    return;

  msg_buf = _lwroc_msg_bufs[source];

  if (msg_buf->_thread_notify)
    {
      msg_buf->_last_ack_ident = ack_ident;
      MFENCE;
      /* Alert that the ack has been received. */
      lwroc_thread_block_send_token(msg_buf->_thread_notify);
    }
}

#if !HAS_STRERROR_R
void strerror_r(int errnum, char *buf, size_t buflen)
{
  char *non_thread_safe_errmsg;

  /* TODO: locking, since strerror is not thread-safe. */
  non_thread_safe_errmsg = strerror(errnum);
  strncpy(buf, non_thread_safe_errmsg, buflen);
  /* end locking */
}
#endif

void lwroc_perror_internal(const lwroc_format_message_context *context,
			   const char *file, int line,
			   const char *s)
{
  char errmsg[256];
  const char *perrmsg = errmsg;
  int errnum = errno;
  strcpy(errmsg, "errno not translated!"); /* just in case */
  errno = 0;
#if STRERROR_R_RET_CHAR_PTR
  /* If we happen to have the GNU version of strerror_r, we have to
   * use the returned pointer.
   */
  perrmsg =
#endif
  strerror_r(errnum, errmsg, sizeof (errmsg));
  errmsg[sizeof (errmsg) - 1] = 0; /* just in case */
  switch (errno)
    {
    case 0:                                                break;
    case EINVAL: perrmsg = "Invalid errno.";               break;
    case ERANGE: perrmsg = "Insufficient message buffer."; break;
    default:     perrmsg = "Unknown error in strerror.";   break;
    }

  lwroc_message_internal(LWROC_MSGLVL_ERROR, context, file, line,
			 "%s: %s", s, perrmsg);
}

char *lwroc_do_message_fmt_msg_context(char *buf, size_t size,
				       const char *fmt, va_list ap)
{
  int n;

#if HAS_VSNPRINTF
  n = vsnprintf(buf, size, fmt, ap);
#else
  /* Let's hope we do not overflow, see below... */
  n = vsprintf(buf, fmt, ap);

  buf[size-1] = 0;
  /* We cannot change message type here.  But this is only used
   * internally to format a context.  There will be an message that
   * will do the overflow...
   */
#endif

  return buf + n;
}

char *lwroc_message_fmt_msg_context(char *buf, size_t size,
				    const char *fmt, ...)
{
  va_list ap;
  char *ret;

  va_start(ap, fmt);
  ret = lwroc_do_message_fmt_msg_context(buf, size, fmt, ap);
  va_end(ap);

  return ret;
}

#if HAS_BACKTRACE
int lwroc_dump_addr2line(int level, const char *cmd_fmt,
			 const char *main_argv0, void *addr,
			 int combine_lines)
{
  char addr2line_cmd[256];
  FILE *fid;

  snprintf(addr2line_cmd, sizeof (addr2line_cmd),
	   cmd_fmt,
	   addr, main_argv0);

  /* printf ("CMD: %s\n", addr2line_cmd); */

  fid = popen(addr2line_cmd, "r");

  if (fid != NULL)
    {
      char buf[1024];

      if (!combine_lines)
	{
	  while(fgets(buf, (int) sizeof(buf), fid) != NULL)
	    {
	      char *new_line = strchr(buf, '\n');

	      if (new_line)
		*new_line = '\0';

	      lwroc_message_internal_extra(level, LWROC_MESSAGE_FLAGS_NO_HOLD,
					   NULL, NULL, "-", 0,
					   "%s", buf);
	    }
	}
      else
	{
	  buf[0] = '\0';

	  while(fgets(buf + strlen(buf),
		      (int) (sizeof(buf) - strlen(buf)), fid) != NULL)
	    {
	      char *new_line = strchr(buf, '\n');

	      if (new_line)
		{
		  *new_line = ' ';
		  *(new_line+1) = '\0';
		}
	    }
	  if (strlen(buf))
	    lwroc_message_internal_extra(level, LWROC_MESSAGE_FLAGS_NO_HOLD,
					 NULL, NULL, "-", 0,
					 "%s", buf);
	}
      if (pclose(fid) != 0)
	return 0;
    }
  else
    return 0;

  return 1;
}

void lwroc_dump_backtrace(int level)
{
  /* Try to produce a backtrace. */

# define NUM_BACKTRACE_ADDRS 100

  void *addrs[NUM_BACKTRACE_ADDRS];
  int naddrs;
  char **symbols;
  int fail = 0;
  int i;

  lwroc_message_internal_extra(level, LWROC_MESSAGE_FLAGS_NO_HOLD,
			       NULL, NULL, "-", 0,
			       "Backtrace:");

  naddrs = backtrace(addrs, NUM_BACKTRACE_ADDRS);

  symbols = backtrace_symbols(addrs, naddrs);

  if (symbols)
    {
      for (i = 0; i < naddrs; i++)
	lwroc_message_internal_extra(level, LWROC_MESSAGE_FLAGS_NO_HOLD,
				     NULL, NULL, "-", 0,
				     "%s", symbols[i]);
    }

  lwroc_message_internal_extra(level, LWROC_MESSAGE_FLAGS_NO_HOLD,
			       NULL, NULL, "-", 0,
			       "Backtrace (again, with addr2line):");

  if (_lwroc_main_argv0)
    for (i = 0; i < naddrs; i++)
      {
	void *use_addr;
	const char *plus;

	if (symbols[i] &&
	    (plus = strchr(symbols[i], '+')) != NULL &&
	    sscanf(plus+1, "%p", &use_addr) == 1)
	  ;
	else
	  use_addr = addrs[i];

	if (!lwroc_dump_addr2line(level,
				  "addr2line -a -f -i -C -p %p -e %s"
				  " 2> /dev/null",
				  _lwroc_main_argv0, use_addr, 0) &&
	    !lwroc_dump_addr2line(level,
				  "addr2line -i -f -C %p -e %s"
				  " 2> /dev/null",
				  _lwroc_main_argv0, use_addr, 1))
	  fail = 1;
      }
  else if (!symbols)
    fail = 1;

  if (fail)
    {
      lwroc_message_internal_extra(level, LWROC_MESSAGE_FLAGS_NO_HOLD,
				   NULL, NULL, "-", 0,
				   "Backtrace (again, addresses):");

      for (i = 0; i < naddrs; i++)
	lwroc_message_internal_extra(level, LWROC_MESSAGE_FLAGS_NO_HOLD,
				     NULL, NULL, "-", 0,
				     "0x%p:", addrs[i]);
    }

  if (symbols)
    free(symbols);
}
#endif/*HAS_BACKTRACE*/

/* TODO: this function should be called regularly, e.g. by each thread
 * at the monitor update, to dump any recent messages that have not
 * been seen for the last 10 s.  (Same as timeout for considering a
 * recent message so old that it is a new one).  Reason: otherwise,
 * the dump will only happen whenever the next message is printed,
 * which may be much later.
 */
void lwroc_message_dump_recent(lwroc_message_buffer *msg_buf)
{
  char msg[128];
  int i;
  struct timespec ts;

  /* We get called when a new recent message is about to be known.
   * Since this signals that something has changed, we dump all
   * previous counts.
   */

  if (!msg_buf->_has_recent_unreported)
    return;

  for ( ; ; )
    {
      lwroc_message_recent *recent = NULL;

      /* Find the oldest message to report. */

      for (i = 0; i < msg_buf->_num_recent; i++)
	{
	  lwroc_message_recent *r = &msg_buf->_recent[i];

	  if (!r->_unreported)
	    continue;

	  if (!recent ||
	      timercmp(&r->_t_last, &recent->_t_last, <))
	    recent = r;
	}

      if (!recent)
	break;

      /* So this message has unreported occurrences... */
      {
	time_t t_span  = recent->_t_last.tv_sec - recent->_t_reported;
	time_t t_total = recent->_t_last.tv_sec - recent->_t_first;

	snprintf(msg,sizeof(msg), "[RL: %" PRIu64 "/%d s; %" PRIu64 "/%d s] ",
		 recent->_unreported, (int) t_span,
		 recent->_hits, (int) t_total);
      }

      /* Give the time as of the last occurrence.
       * Note: this may mean that the messages are reported
       * out of order (with other threads), but is still better
       * than giving the current time.
       */
      ts.tv_sec  = recent->_t_last.tv_sec;
      ts.tv_nsec = recent->_t_last.tv_usec * 1000;

      /* We cannot use the context pointer, since things may have
       * changed since it was given.
       *
       * Not nice to format if we ever do bold-face...
       */
      lwroc_message_internal_extra(recent->_level,
				   LWROC_MESSAGE_FLAGS_NO_RATE_LIMIT,
				   &ts,
				   NULL, /* Do not use context. */
				   recent->_file, recent->_line,
				   "%s%s...", msg, recent->_msg);

      recent->_t_reported = recent->_t_last.tv_sec;
      recent->_unreported = 0;
    }

  msg_buf->_has_recent_unreported = 0;
}

void lwroc_message_rate_ctrl(int ctrl)
{
  lwroc_message_buffer *msg_buf = NULL;

  msg_buf = (lwroc_message_buffer *) pthread_getspecific(_lwroc_msg_key);

  if (!msg_buf)
    LWROC_BUG("lwroc_message_rate_ctrl called for thread with no msg_buf.");

  msg_buf->_rate_ctrl = ctrl;
}

#define LWROC_MSG_LIMIT_AT_COUNT  5

void lwroc_do_message_internal(int level, int flags, struct timespec *t,
			       const lwroc_format_message_context *context,
			       const char *file, int line,
			       const char *fmt,va_list ap)
{
  lwroc_message_buffer *msg_buf = NULL;

#if HAS_VSNPRINTF
  char msg[1024];
#else
  char msg[10240];
#endif
  char *e;
  char *ptrmsg = msg;
  lwroc_message_recent *recent = NULL;

  struct timespec ts;

  assert(file);
  assert(fmt);

  if (t)
    ts = *t;
  else
    clock_gettime(CLOCK_REALTIME, &ts);

  /* Before we try to format the message, consider if we should
   * rate-limit it.
   *
   * There is no need to avoid checking messages that would stop
   * execution, as they will abrt soon enough anyhow, except when
   * marked with @nohold, since those are part of the final dump.
   */

  /* It also means we must get the message buffer already here. */
  /* If _lwroc_message_local is set, then _lwroc_msg_key was not initialised. */
  if (!_lwroc_message_local)
    msg_buf = (lwroc_message_buffer *) pthread_getspecific(_lwroc_msg_key);

  if (msg_buf &&
      !(flags & LWROC_MESSAGE_FLAGS_NO_RATE_LIMIT))
    {
      msg_buf->_message_calls++;

      if ((msg_buf->_rate_ctrl & LWROC_MESSAGE_RATE_CTRL_LIMIT) &&
	  !(flags & LWROC_MESSAGE_FLAGS_NO_HOLD) &&
	  !_lwroc_log_no_rate_limit)
	{
	  int i;
	  int try_string = 0;
	  int limit_time;

	  /* We keep a history of the most recent messages seen.  they
	   * are identified by file and line number.  First we try to
	   * match with the pointers.  If that fails, use a more
	   * expensive string comparison.
	   */

	  for (i = 0; i < msg_buf->_num_recent; i++)
	    {
	      recent = &msg_buf->_recent[i];

	      if (context == recent->_context &&
		  line == recent->_line)
		{
		  if (file == recent->_file)
		    goto found_recent;
		  else
		    try_string = 1;
		}
	    }

	  if (try_string)
	    for (i = 0; i < msg_buf->_num_recent; i++)
	      {
		recent = &msg_buf->_recent[i];

		if (context == recent->_context &&
		    line == recent->_line &&
		    strcmp(file,recent->_file) == 0)
		  goto found_recent;
	      }

	  /* No matching item, need to make a new one. */
	  if (msg_buf->_num_recent < LWROC_MSG_NUM_RECENT)
	    {
	      recent = &msg_buf->_recent[msg_buf->_num_recent++];
	      /* Make sure this new (still uninitialised item) does
	       * not get picked by lwroc_message_dump_recent().
	       */
	      recent->_unreported = 0;
	    }
	  else
	    {
	      /* Find the oldest item, and replace that one. */
	      recent = &msg_buf->_recent[0];

	      for (i = 1; i < LWROC_MSG_NUM_RECENT; i++)
		{
		  lwroc_message_recent *r = &msg_buf->_recent[i];

		  if (timercmp(&r->_t_last, &recent->_t_last, <))
		    recent = r;
		}
	    }

	recent_new:
	  /* If a new (previously unknown) message makes it into the
	   * list, we dump all previous unreported occurrences.
	   *
	   * Strategically, this will also make sure there is nothing
	   * unreported in the structure we are about to reuse.
	   */
	  lwroc_message_dump_recent(msg_buf);

	  /* Set the item. */
	  recent->_context = context;
	  recent->_file = file;
	  recent->_line = line;

	  recent->_level = level;
	  recent->_hits = 1;
	  recent->_unreported = 0;

	  recent->_t_reported =
	    recent->_t_first  = ts.tv_sec;
	  recent->_t_next_report = ts.tv_sec + 1;

	  recent->_t_last.tv_sec  = ts.tv_sec;
	  /* Was cats to suseconds_t, but unknown on some platforms. */
	  recent->_t_last.tv_usec = (int) (ts.tv_nsec / 1000);

	  goto recent_report;

	found_recent:
	  /* If the previous occurrence was more than 10 s ago, then we
	   * treat it as a new message. */

	  limit_time = 10;

	  /* Except if it is an action message (e.g. file open), in which
	   * case we reduce the limit. */
	  if (level == LWROC_MSGLVL_ACTION)
	    limit_time = 1;

	  if (ts.tv_sec >= recent->_t_last.tv_sec + limit_time ||
	      ts.tv_sec <  recent->_t_last.tv_sec)
	    goto recent_new;

	  /* Last time we saw this message. */
	  recent->_t_last.tv_sec  = ts.tv_sec;
	  recent->_t_last.tv_usec = (int) (ts.tv_nsec / 1000);

	  recent->_hits++;

	  if (recent->_hits <= LWROC_MSG_LIMIT_AT_COUNT)
	    goto recent_report;

	/* recent_timelimit: */
	  if (ts.tv_sec >= recent->_t_next_report ||
	      ts.tv_sec <  recent->_t_reported)
	    {
	      if (ts.tv_sec < recent->_t_first)
		{
		  /* Clock has moved backwards; report in 5 seconds... */
		  recent->_t_next_report = ts.tv_sec + 5;
		}
	      else
		{
		  time_t t_total = ts.tv_sec - recent->_t_first;
		  time_t t_delay = 1;

		  /* We report at nice points in time after the first
		   * occurrence.
		   */

		  if (t_total >= 1000)
		    t_delay = (t_total / 1000) * 2000;
		  else if (t_total >=  500) t_delay = 1000;
		  else if (t_total >=  200) t_delay =  500;
		  else if (t_total >=  100) t_delay =  200;
		  else if (t_total >=   50) t_delay =  100;
		  else if (t_total >=   20) t_delay =   50;
		  else if (t_total >=   10) t_delay =   20;
		  else if (t_total >=    5) t_delay =   10;
		  else if (t_total >=    2) t_delay =    5;
		  else if (t_total >=    1) t_delay =    2;
		  else                      t_delay =    1;

		  recent->_t_next_report = recent->_t_first + t_delay;
		}
	      goto recent_report;
	    }

	  /* We have seen enough of this message for the moment... */
	  recent->_unreported++;
	  msg_buf->_has_recent_unreported = 1;
	  /* Reset flags. */
	  msg_buf->_rate_ctrl = 0;
	  return;

	recent_report:
	  /* Decided to continue with the message. */

	  if (recent->_hits >= LWROC_MSG_LIMIT_AT_COUNT)
	    {
	      /* This is first message before silence... */

	      time_t t_span  = ts.tv_sec - recent->_t_reported;
	      time_t t_total = ts.tv_sec - recent->_t_first;

	      ptrmsg +=
		sprintf (ptrmsg, "[RL: %" PRIu64 "/%d s; %" PRIu64 "/%d s] ",
			 recent->_unreported+1/*this one*/, (int) t_span,
			 recent->_hits, (int) t_total);

	      recent->_unreported = 0;
	    }

	  recent->_t_reported = ts.tv_sec;

	  /*
	report_slaved:
	  ;
	  */
	}

      /* Reset flags. */
      msg_buf->_rate_ctrl = 0;
    }

  if (context && *context)
    {
      /* Note, if we get the idea of marking the context boundary,
       * remember that lwroc_merge_in_fmt_msg_context fakes a second
       * context with the event info.  "] [..."
       */
      *(ptrmsg++) = '[';
      ptrmsg = (*context)(ptrmsg, sizeof (msg) - (size_t) (ptrmsg - msg)-4,
			  context); /* -4 to allow space. */
      /* Add a space. */
      *(ptrmsg++) = ']';
      *(ptrmsg++) = ' ';
      *(ptrmsg) = 0;
    }

#if HAS_VSNPRINTF
  vsnprintf(ptrmsg, sizeof (msg) - (size_t) (ptrmsg - msg), fmt, ap);
#else
  /* Note!  Danger, since we cannot check against buffer overflow. */
  /* We handle this by allocating a larger buffer.
   * TODO: complain if the user exceeds say a tenth of the buffer.
   */
  vsprintf(ptrmsg, fmt, ap);

  msg[1024] = 0;
  if (strlen(msg) >= 1023)
    {
      /* Hmmm, can we just call the error routine from in here? */
      /* Ha, lets just change the message, and make it level BUG!
       * then it will stop the process and force program fix...
       */
      strcpy(msg, "Error message too long.  "
	     "Possible buffer overflow due to lack of vsnprintf on platform.  "
	     "Changed message level to BUG!");
      level = LWROC_MSGLVL_BUG;
    }
#endif
  /* _trcom_error_handler(level, file, line, fmt, ap); */

  /* Kill trailing newlines. */
  e = msg + strlen(msg);
  while (e > msg && *(e-1) == '\n')
    *(--e) = 0;

  if (recent)
    {
      /* Keep a bit of the message for recent printing.
       *
       * Do not use the context, just the real message.
       */

      strncpy(recent->_msg, ptrmsg, sizeof (recent->_msg));
      recent->_msg[sizeof (recent->_msg)-1] = '\0';
    }

  if (_lwroc_message_local)
    {
      const char *ctext;
      const char *ctextback;

      lwroc_message_colourstr(level, 1, &ctext, &ctextback);

      fflush(stdout); /* To avoid mixing of lines. */
      if (0)
	fprintf (stderr, "%d: %s:%d: ", level, file, line);

      fprintf (stderr, "%s%s%s\n", ctext, msg, ctextback);

      if (level == LWROC_MSGLVL_SIGNAL ||
	  level == LWROC_MSGLVL_BUG ||
	  level == LWROC_MSGLVL_FATAL ||
	  level == LWROC_MSGLVL_BADCFG)
	exit(1);

      _lwroc_message_printed = 1;
      SFENCE;
    }
  else
    {
      int to_console = 0;

      if (level <= _lwroc_log_console_level)
	to_console = 1;

      if (!msg_buf &&
	  level < LWROC_MSGLVL_INFO)
	to_console = 1;

      if (to_console)
	{
	  fflush(stdout); /* To avoid mixing of lines. */
	  fprintf (stderr, "%d: %s:%d: %s\n",
		   level, file, line, msg);
	}

      if (msg_buf)
	{
	  lwroc_pipe_buffer_control *buf = msg_buf->_msgs;
	  lwroc_message_item item;
	  lwroc_message_item_sersize sz_item;
	  char *wire, *wire_start;
	  uint32_t size;

	  item._time._sec  = (uint64_t) ts.tv_sec;
	  item._time._nsec = (uint32_t) ts.tv_nsec;
	  if (level <= LWROC_MSGLVL_MAX)
	    item._level      = (uint16_t) _lwroc_msg_level_mark[level];
	  else
	    item._level      = (uint16_t) -1;
	  item._lost       = msg_buf->_lost_messages ? 1 : 0;
	  item._source     = msg_buf->_source;
	  item._file       = file;
	  item._line       = (uint32_t) line;
	  item._msg        = msg;

	  if (_lwroc_log_ack_wait)
	    {
	      item._ack_ident =
		0x80000000 |
		((uint32_t) ((msg_buf->_message_calls & 0x7fff) << 16)) |
		((uint32_t) (item._time._nsec & 0xffff));
	    }
	  else
	    item._ack_ident = 0;

	  size =
	    (uint32_t) lwroc_message_item_serialized_size(&item, &sz_item);

	  /* printf ("msg item -> %d\n", size); */

	  for ( ; ; )
	    {
	      wire_start = wire = (char *)
		lwroc_pipe_buffer_alloc_write(buf, size, NULL,
					      msg_buf->_thread_notify,
					      /* Cannot wait in NET thread,
					       * or if threads not yet created.
					       */
					      (msg_buf->_source !=
					       LWROC_MSG_BUFS_NET_IO) &&
					      _lwroc_threads_created);

	      if (wire == NULL)
		{
		  /* We would have reached a deadlock - network
		   * thread is what empties the messages...
		   */
		  fprintf (stderr,
			   "*** Message lost - not logged - "
			   "network thread message buffer full! ***\n");
		  msg_buf->_lost_messages++;
		  goto message_lost;
		}
	      break;
	    }

#if DEBUG_MSG_WRITER_CONSUMER
	  if (msg_buf->_debug_file)
	    {
	      fprintf (msg_buf->_debug_file,
		       "[%d] alignment -> [%p] %d @%ld\n",
		       msg_buf->_source, wire, size, time(NULL));
	      fflush(msg_buf->_debug_file);
	    }
#endif

	  wire = lwroc_message_item_serialize(wire, &item, &sz_item);
	  /* pad the item?? */
	  /*
	    item_pad = item_msg + sz_msg + 1;
	    memset (item_pad, 0, sz_pad);
	  */

	  assert (wire == wire_start + sz_item.size);

	  lwroc_pipe_buffer_did_write(buf, sz_item.size, 0);

	  if (msg_buf->_lost_messages)
	    fprintf (stderr,"report loss!\n");
	  msg_buf->_lost_messages = 0; /* We have reported. */

	  /* Wait for an acknowledge if requested.  But not if we are
	   * the network thread (which does the actual sending), or if
	   * the threads have not been created yet.
	   */
	  if (item._ack_ident &&
	      (msg_buf->_source != LWROC_MSG_BUFS_NET_IO) &&
	      _lwroc_threads_created)
	    {
	      struct timeval tstart, now, diff;

	      /* Perhaps not the most beautiful wait loop.
	       */
	      gettimeofday(&tstart, NULL);

	      for ( ; ; )
		{
		  /* timeout() is a curses.hj function. */
		  struct timeval timeout_a;

		  /* We use _lwroc_quit_count and not _lwroc_shutdown
		   * such that the SIGINT handler can have the logging
		   * before notifying the rest of the system.
		   */
		  if (_lwroc_quit_count ||
		      msg_buf->_last_ack_ident == item._ack_ident)
		    break;

		  gettimeofday(&now, NULL);

		  timersub(&now, &tstart, &diff);

		  if (diff.tv_sec >= 10 ||
		      diff.tv_sec < 0)
		    {
		      if (_lwroc_message_client_seen)
			fprintf (stderr,
				 "Timeout waiting for message ack "
				 "from log message client.\n");
		    }

		  timeout_a.tv_sec  = 1;
		  timeout_a.tv_usec = 0;

		  lwroc_thread_block_get_token_timeout(msg_buf->_thread_notify,
						       &timeout_a);
		}
	    }

	message_lost:
	  ;
	}
      else
	{
	  fprintf (stderr,
		   "Message not logged - thread has no error buffer yet...\n");
	}

      if (!(flags & LWROC_MESSAGE_FLAGS_NO_HOLD) &&
	  (level == LWROC_MSGLVL_SIGNAL ||
	   level == LWROC_MSGLVL_BUG ||
	   level == LWROC_MSGLVL_FATAL))
	{
	  /* We do further reporting using the normal error reporting
	   * routines.  Note that we may have come here through an
	   * signal handler (SIGBUS, SIGSEGV).  But since we may also
	   * be daemonised, this may be the only chance to tell the
	   * outside world what went wrong.
	   */

#if HAS_BACKTRACE
	  lwroc_dump_backtrace(level);
#else
	  lwroc_message_internal_extra(level, LWROC_MESSAGE_FLAGS_NO_HOLD,
				       NULL, NULL, "-", 0,
				       "Backtrace not available on platform.");
#endif/*HAS_BACKTRACE*/

	  if (level == LWROC_MSGLVL_SIGNAL)
	    _lwroc_bug_fatal_reported = LWROC_NET_SYS_HEALTH_SIGNAL;
	  else if (level == LWROC_MSGLVL_BUG)
	    _lwroc_bug_fatal_reported = LWROC_NET_SYS_HEALTH_BUG;
	  else
	    _lwroc_bug_fatal_reported = LWROC_NET_SYS_HEALTH_FATAL;
	  SFENCE;

	  lwroc_message_internal_extra(level, LWROC_MESSAGE_FLAGS_NO_HOLD,
				       NULL, NULL, "-", 0,
				       "BUG or FATAL reported.");

	  if (_lwroc_message_no_sleep == 1)
	    {
	      lwroc_message_internal_extra(level, LWROC_MESSAGE_FLAGS_NO_HOLD,
					   NULL, NULL, "-", 0,
					   "Exiting.  "
					   "Run without --insomniac to debug.");
	      /* Some sleep, in the hope it gets logged. */
	      sleep(2);
	      exit(1);
	    }

	  lwroc_message_internal_extra(level, LWROC_MESSAGE_FLAGS_NO_HOLD,
				       NULL, NULL, "-", 0,
				       "Sleeping INDEFINITELY "
				       "(to allow debugger attachment).");

	  {
	    char cwd[1024];

	    if (getcwd(cwd, sizeof (cwd)) == NULL)
	      strcpy(cwd, "?");

	    lwroc_message_internal_extra(level, LWROC_MESSAGE_FLAGS_NO_HOLD,
					 NULL, NULL, "-", 0,
					 "Debug cmd: cd %s ; gdb %s %ld",
					 cwd, _lwroc_main_argv0, (long) getpid());
	  }

	  /* As last action before actual sleep, we try to put the TRIVA
	   * to sleep.  Due to misbehaving TRIXOR PCs, that needs reboots
	   * after some cases of unterminated processes.
	   */

	  if (_lwroc_hardware_cleanup)
	    {
	      lwroc_hardware_cleanup cleanup = _lwroc_hardware_cleanup;

	      /* Only do once, in case of SIGBUS loop due to missing TRIVA. */
	      _lwroc_hardware_cleanup = NULL;

	      lwroc_message_internal_extra(level, LWROC_MESSAGE_FLAGS_NO_HOLD,
					   NULL, NULL, "-", 0,
					   "Performing hardware cleanup "
					   "(TRIVA HALT, RESET) in 2 s...");
	      sleep(2);
	      cleanup();
	      (void) cleanup;
	      lwroc_message_internal_extra(level, LWROC_MESSAGE_FLAGS_NO_HOLD,
					   NULL, NULL, "-", 0,
					   "Hardware cleanup done.");
	    }

	  /* Hmmm, this however means that other threads would continue to
	   * run.  If we are the main thread, we should sleep to allow
	   * debugging a faulty readout code.  And to allow the network
	   * thread to send the message out.  If we are the network
	   * thread, we anyhow cannot report and may as well just die.
	   */
	  while ( 1 )
	    sleep (100);
	}
      if (level == LWROC_MSGLVL_BADCFG)
	exit(1);
    }
}

void lwroc_message_context(const char *file,int line,
			   const lwroc_format_message_context *context)
{
  /* This is somewhat hacky in that we store the context info into out
   * global message state for this thread.  This is done such that one
   * does not need to come up with a macro version for each kind of
   * message with or without a context.  It in principle also gives
   * the option of having multiple contexts.
   */
  (void) context;
  (void) file;
  (void) line;
 }

void lwroc_message_internal(int level,
			    const lwroc_format_message_context *context,
			    const char *file, int line,
			    const char *fmt,...)
{
  va_list ap;

  va_start(ap, fmt);
  lwroc_do_message_internal(level, 0, NULL, context, file, line, fmt, ap);
  va_end(ap);
}

void lwroc_message_internal_extra(int level, int flags, struct timespec *t,
				  const lwroc_format_message_context *context,
				  const char *file, int line,
				  const char *fmt,...)
{
  va_list ap;

  va_start(ap, fmt);
  lwroc_do_message_internal(level, flags, t, context, file, line, fmt, ap);
  va_end(ap);
}

void lwroc_message_colourstr(int level, int outerr,
			     const char **ctext,
			     const char **ctextback)
{
  *ctext = "";
  *ctextback = "";

  (void) outerr; /* In case not used in CT_OUTERR below. */

  switch (level)
    {
    case LWROC_MSGLVL_SIGNAL:
      *ctext = CT_OUTERR(outerr,YELLOW_BG_MAGENTA);
      *ctextback = CT_OUTERR(outerr,DEF_COL);
      break;
    case LWROC_MSGLVL_BUG:
      *ctext = CT_OUTERR(outerr,RED_BG_GREEN);
      *ctextback = CT_OUTERR(outerr,DEF_COL);
      break;
    case LWROC_MSGLVL_FATAL:
      *ctext = CT_OUTERR(outerr,WHITE_BG_MAGENTA);
      *ctextback = CT_OUTERR(outerr,DEF_COL);
      break;
    case LWROC_MSGLVL_BADCFG:
      *ctext = CT_OUTERR(outerr,YELLOW_BG_MAGENTA);
      *ctextback = CT_OUTERR(outerr,DEF_COL);
      break;
    case LWROC_MSGLVL_ERROR:
      *ctext = CT_OUTERR(outerr,WHITE_BG_RED);
      *ctextback = CT_OUTERR(outerr,DEF_COL);
      break;
    case LWROC_MSGLVL_ATTACH:
      *ctext = CT_OUTERR(outerr,WHITE_BG_GREEN);
      *ctextback = CT_OUTERR(outerr,DEF_COL);
      break;
    case LWROC_MSGLVL_DETACH:
      *ctext = CT_OUTERR(outerr,YELLOW_BG_RED);
      *ctextback = CT_OUTERR(outerr,DEF_COL);
      break;
    case LWROC_MSGLVL_WARNING:
      *ctext = CT_OUTERR(outerr,BLACK_BG_YELLOW);
      *ctextback = CT_OUTERR(outerr,DEF_COL);
      break;
    case LWROC_MSGLVL_ACTION:
      *ctext = CT_OUTERR(outerr,YELLOW_BG_BLUE);
      *ctextback = CT_OUTERR(outerr,DEF_COL);
      break;
    case LWROC_MSGLVL_INFO:
      *ctext = CT_OUTERR(outerr,GREEN);
      *ctextback = CT_OUTERR(outerr,DEF_COL);
      break;
    case LWROC_MSGLVL_LOG:
      /* Normal terminal colour. */
      break;
    case LWROC_MSGLVL_DEBUG:
      *ctext = CT_OUTERR(outerr,MAGENTA);
      *ctextback = CT_OUTERR(outerr,DEF_COL);
      break;
    case LWROC_MSGLVL_SPAM:
      *ctext = CT_OUTERR(outerr,BLUE);
      *ctextback = CT_OUTERR(outerr,DEF_COL);
      break;
    }
}

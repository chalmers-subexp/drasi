/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_MESSAGE_INTERNAL_H__
#define __LWROC_MESSAGE_INTERNAL_H__

/* Must be early, or stdio may miss vs(n)printf. */
#include <stdarg.h>

#include "lwroc_pipe_buffer.h"
#include "lwroc_thread_block.h"
#include "lwroc_message.h"

#define LWROC_MSG_BUFS_MAIN        0
#define LWROC_MSG_BUFS_NET_IO      1
#define LWROC_MSG_BUFS_FILE        2
#define LWROC_MSG_BUFS_NET_TRANS   3
#define LWROC_MSG_BUFS_IN          4
#define LWROC_MSG_BUFS_VAL         5
#define LWROC_MSG_BUFS_ANALYSE     6
#define LWROC_MSG_BUFS_WATCHDOG    7
#define LWROC_MSG_BUFS_TRIVA_CTRL  8
#define LWROC_MSG_BUFS_USER_FILTER 9
#define LWROC_MSG_BUFS_TS_FILTER   10
#define LWROC_MSG_BUFS_SLOW_ASYNC  11
#define LWROC_MSG_BUFS_USER_1      12
#define LWROC_MSG_BUFS_USER_2      13
#define LWROC_MSG_BUFS_USER_3      14
#define LWROC_MSG_BUFS_NUM         15
#define LWROC_MSG_BUFS_NUM_USER    3

extern const char *_lwroc_main_argv0;

/* To be called from the master thread.  (Before *any* message.) */
void lwroc_msg_bufs_early_init(void);

/* To be called from the master thread.  Early. */
void lwroc_msg_bufs_init(int server_port,
			 int argc,char *argv[]);

void lwroc_msg_thread_set_buf(int source,
			      const lwroc_thread_block *thread_notify);

int lwroc_get_msg_buf_source(void); /* Get current thread. */

int lwroc_msg_bufs_read_avail(const lwroc_thread_block *thread_notify);

typedef struct lwroc_msg_writing_t
{
  lwroc_pipe_buffer_consumer *_buf_consumer;
  char   *_data;
  size_t  _size;
} lwroc_msg_writing;

void lwroc_msg_bufs_new_client(void);

int lwroc_msg_num_msgs(void);

int lwroc_msg_bufs_source_info_read_avail(lwroc_msg_writing *writing);

void lwroc_msg_bufs_oldest_read_avail(lwroc_msg_writing *writing);

void lwroc_msg_buf_consume(lwroc_msg_writing *writing);

void lwroc_msg_bufs_skip_oldest(void);

void lwroc_msg_bufs_got_ack(uint32_t source, uint32_t ack_ident);

extern int _lwroc_message_local;
extern int _lwroc_message_no_sleep;

extern volatile int _lwroc_message_printed;

extern int _lwroc_log_ack_wait;

extern int _lwroc_log_no_rate_limit;

extern int _lwroc_log_console_level;

extern volatile uint32_t _lwroc_bug_fatal_reported;

/* This should not be used globally, use _lwroc_shutdown. */
extern volatile int _lwroc_quit_count;

#define LWROC_MESSAGE_FLAGS_NO_HOLD        1
#define LWROC_MESSAGE_FLAGS_NO_RATE_LIMIT  2

void lwroc_do_message_internal(int level, int flags, struct timespec *t,
			       const lwroc_format_message_context *context,
			       const char *file, int line,
			       const char *fmt,va_list ap);

void lwroc_message_internal_extra(int level, int flags, struct timespec *t,
				  const lwroc_format_message_context *context,
				  const char *file, int line,
				  const char *fmt,...)
  __attribute__ ((__format__ (__printf__, 7, 8)));

extern volatile int _lwroc_message_client_seen;

void lwroc_msg_wait_any_msg_client(lwroc_thread_block *block);

void lwroc_net_wait_any_msg_client_init(void);

void lwroc_message_colourstr(int level, int outerr,
			     const char **ctext,
			     const char **ctextback);

#endif/*__LWROC_MESSAGE_INTERNAL_H__*/

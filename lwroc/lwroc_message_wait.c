/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_message_internal.h"
#include "lwroc_net_client_base.h"
#include "lwroc_net_io.h"

#include <string.h>

void lwroc_msg_wait_any_msg_client(lwroc_thread_block *block)
{
  /* Hold the current thread until we have some log client connected.
   *
   * The purpose is to try to ensure that log messages get recorded
   * somewhere, by holding startup until a client is connected.
   *
   * It of course does not help if the client disappears again for
   * some reason.  But in that case we would have two choices: hold
   * operation or continue.  We continue, hoping that a client will
   * reconnect.
   */

  /* After the flag marking that a message client has been connected
   * is set, it will send a token to all thread block pipes.
   */

  while (!_lwroc_message_client_seen)
    {
      /* Wait. */

      lwroc_thread_block_get_token(block);
    }
}

/* Abuse a dummy network connection to remind user on startup until a
 * message client has been connection.
 */

typedef struct lwroc_net_wait_any_msg_client_t
{
  lwroc_net_client_base _base;

  struct timeval _next_report;
  int _report_interval;

} lwroc_net_wait_any_msg_client;

void lwroc_net_wait_any_msg_client_setup_select(lwroc_select_item *item,
						lwroc_select_info *si)
{
  lwroc_net_wait_any_msg_client *client =
    PD_LL_ITEM(item, lwroc_net_wait_any_msg_client, _base._select_item);

  lwroc_select_info_time_setup(si, &client->_next_report);
}

int lwroc_net_wait_any_msg_client_after_select(lwroc_select_item *item,
					       lwroc_select_info *si)
{
  lwroc_net_wait_any_msg_client *client =
    PD_LL_ITEM(item, lwroc_net_wait_any_msg_client, _base._select_item);

  if (_lwroc_message_client_seen)
    return 0;

  if (timercmp(&si->now, &client->_next_report, >))
    {
      LWROC_WARNING_FMT("Waited %d seconds for msg client.",
			client->_report_interval);
      if (client->_report_interval == 10)
	LWROC_ERROR("Startup will not proceed without msg client.  "
		    "(lwrocmon --log <HOST>)");

      client->_report_interval =
	(client->_report_interval < 5 ? 5 : client->_report_interval * 2);

      client->_next_report = si->now;
      client->_next_report.tv_sec += client->_report_interval;
    }

  return 1;
}

lwroc_select_item_info lwroc_net_wait_any_msg_client_select_item_info =
  {
    lwroc_net_wait_any_msg_client_setup_select,
    lwroc_net_wait_any_msg_client_after_select,
    lwroc_net_client_base_after_fail,
    lwroc_net_client_base_delete,
    0,
  };

void lwroc_net_wait_any_msg_client_init(void)
{
  lwroc_net_wait_any_msg_client *client;
  struct timeval now;

  client = (lwroc_net_wait_any_msg_client *)
    malloc (sizeof (lwroc_net_wait_any_msg_client));

  if (!client)
    LWROC_FATAL("Memory allocation failure (wait_msg_client).");

  memset (client, 0, sizeof (*client));

  client->_base._fd = -1;

  gettimeofday(&now, NULL);

  client->_next_report = now;
  client->_next_report.tv_sec += 1;
  client->_report_interval = 1;

  PD_LL_INIT(&client->_base._clients);
  PD_LL_ADD_BEFORE(&_lwroc_net_clients,
		   &client->_base._select_item._items);

  client->_base._select_item.item_info =
    &lwroc_net_wait_any_msg_client_select_item_info;
}

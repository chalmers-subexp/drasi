/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_message.h"
#include "lwroc_mon_block.h"
#include "lwroc_optimise.h"
#include "lwroc_hostname_util.h"
#include "lwroc_main_iface.h"

#include "../dtc_arch/acc_def/time_include.h"
#include "../dtc_arch/acc_def/has_getrusage.h"
#include "../dtc_arch/acc_def/myinttypes.h"
#include <string.h>
#include <assert.h>
/*
#include <sys/syscall.h>
*/

/* Values just to differentiate us from other process. */
lwroc_mon_ident _lwroc_mon_ident = { 0, 0, 0, 0 };

void lwroc_string_simple_cksum(uint32_t *pck, const char *p)
{
  uint32_t ck = *pck;

  while (*p)
    {
      uint32_t c = (uint32_t) *(p++);

      ck ^= c;
      ck = (ck << 1) | (ck >> 31);
    }

  *pck = ck;
}

void lwroc_mon_prepare_source(int argc,char *argv[])
{
  struct timeval now;
  int i;
  char hostname[256];

  lwroc_gethostname(hostname, sizeof (hostname));

  gettimeofday(&now, NULL);

  /* TODO: all the functions in here can be considered to be security
   * leaks, since we give away semi-internal information.
   *
   * They should at least be hashed.
   */

  _lwroc_mon_ident._ident1 = (uint32_t) now.tv_sec;
  _lwroc_mon_ident._ident2 =
    ((uint32_t) _config._port) |
    (((((uint32_t) now.tv_usec) << 2) ^
      ((uint32_t) getpid())) &
     0xffff0000);
  _lwroc_mon_ident._ident3 = 0;
  _lwroc_mon_ident._ident4 = 0;

  for (i = 0; i < argc; i++)
    lwroc_string_simple_cksum(&_lwroc_mon_ident._ident3, argv[i]);

  lwroc_string_simple_cksum(&_lwroc_mon_ident._ident4, hostname);
}

void lwroc_mon_source_set(lwroc_message_source *source)
{
  /* This structure is a dummy mainly for the monitoring.
   * The members are used for log messages.
   * Try to use them also for the monitoring?
   */

  source->_source     = 1;
  source->_hostname   = "hst";
  source->_port       = 2;
  source->_mainthread = 0; /* Used for log messages */
  source->_thread     = "thread";

  source->_mon_ident1 = _lwroc_mon_ident._ident1;
  source->_mon_ident2 = _lwroc_mon_ident._ident2;
  source->_mon_ident3 = _lwroc_mon_ident._ident3;
  source->_mon_ident4 = _lwroc_mon_ident._ident4;
  /*
  printf ("%" PRIu32 " %" PRIu32 " %" PRIu32 " %" PRIu32 "\n",
	  source->_ident1, source->_ident2,
	  source->_ident3, source->_ident4);
  */
}

volatile int _lwroc_mon_update_seq = 0;

lwroc_mon_block *_lwroc_mon_blocks[2] = { NULL, NULL };
lwroc_mon_block **_lwroc_mon_blocks_add_at[2] = { &_lwroc_mon_blocks[0],
						  &_lwroc_mon_blocks[1] };

lwroc_mon_block *lwroc_reg_mon_block(int conn,
				     void *data, size_t data_size,
				     size_t ser_size,
				     lwroc_mon_block_serializer ser_fcn,
				     size_t ser_source_size,
				     lwroc_mon_block_fetch fetch_fcn,
				     lwroc_thread_block *notify_thread)
{
  lwroc_mon_block *handle;
  size_t sz_alloc;
  void *copy01;

  sz_alloc = sizeof (lwroc_mon_block) +
    2 * data_size + ser_source_size + 64 + data_size;
  /*
  printf ("%d %zd %zd %zd  %zd\n",
	  conn, data_size, ser_size, ser_source_size, sz_alloc);
  */
  handle = (lwroc_mon_block *) malloc (sz_alloc);

  if (!handle)
     LWROC_FATAL("Failure allocating memory for monitor item handle.");

  handle->_next      = NULL;
  handle->_data      = data;
  handle->_ser_size  = ser_size;
  handle->_serialize_fcn = ser_fcn;
  handle->_fetch_fcn = fetch_fcn;
  handle->_ser_source_size = ser_source_size;
  handle->_notify_thread = notify_thread;

  handle->_update_seq = _lwroc_mon_update_seq - 1;

  copy01              = (handle+1);
  handle->_ser_source = ((char *) copy01) + 2 * data_size;
  /* Move 64 bytes away to (ensure?) that the stage area (which is
   * written by the network thread) does not overlap in cache-line.
   */
  handle->_stage      = ((char *) handle->_ser_source) + ser_source_size + 64;

  assert((char *) handle->_stage + data_size == ((char *) handle) + sz_alloc);

  /* A linear search is fine, we are only called on startup. */
  *_lwroc_mon_blocks_add_at[conn] = handle;
  _lwroc_mon_blocks_add_at[conn] = &(handle->_next);

  lwroc_two_copy_init(&handle->_two_copy, copy01, data_size,
		      handle->_data /* Place valid data in first copy. */);

  return handle;
}

/* The following is the *only* function which is called by the thread
 * updating the information.  All other functions (including setup),
 * are called by the network I/O thread.
 */

void lwroc_mon_block_update(lwroc_mon_block *handle,
			    lwroc_item_time *block_time,
			    uint64_t *block_cputime_usec)
{
  struct timeval now;
#if HAS_RUSAGE_THREAD
  struct rusage usage;
#endif

  /* Update the time of the block.  All other variables
   * are expected to be continuously updated.  (Hmmm, we might
   * want to have a callback for members which are easier to evaluate
   * when needed (about 10 times per second)...)
   */

  gettimeofday(&now,NULL);

  block_time->_sec  = (uint64_t) now.tv_sec;
  block_time->_nsec = (uint32_t) now.tv_usec * 1000;

  if (block_cputime_usec)
    {
#if HAS_RUSAGE_THREAD
      if (getrusage(RUSAGE_THREAD, &usage) == 0)
	{
	  /*
	  long tid;
	  tid = syscall(SYS_gettid);
	  */
	  *block_cputime_usec =
	    ((uint64_t) usage.ru_utime.tv_sec +
	     (uint64_t) usage.ru_stime.tv_sec) * 1000000 +
	    (uint64_t) (usage.ru_utime.tv_usec +
			usage.ru_stime.tv_usec);
	  /*
	  printf ("%p [%ld]: %lld  %d %d  %d %d\n",
		  handle, tid,
		  (long long)*block_cputime_usec,
		  (int) usage.ru_utime.tv_sec,
		  (int) usage.ru_utime.tv_usec,
		  (int) usage.ru_stime.tv_sec,
		  (int) usage.ru_stime.tv_usec);
	  */
	}
      else
	*block_cputime_usec = 0;
#else
      *block_cputime_usec = 0;
#endif
    }

  /* Update the two-copy data. */

  lwroc_two_copy_update(&handle->_two_copy, handle->_data);

  /* The reading end will in turn make a copy from the copy chunk
   * which has the highest (relative - signed difference comparison)
   * marker, that also is matching.
   *
   * Only in case we would be updating quickly in succession, or the
   * copying on the reading end takes an extraordinary amount of time,
   * it will fail, by after the copy finding that the markers did not
   * match.  If so, it shall reschedule the copy to happen again soon.
   */
}

uint32_t lwroc_mon_blocks_num(int conns)
{
  lwroc_mon_block *handle;
  uint32_t num = 0;

  for (handle = _lwroc_mon_blocks[conns]; handle; handle = handle->_next)
    {
      num++;
    }
  return num;
}

size_t lwroc_mon_blocks_source_serialized_size(int conns)
{
  lwroc_mon_block *handle;
  size_t ser_size = 0;

  for (handle = _lwroc_mon_blocks[conns]; handle; handle = handle->_next)
    {
      /* printf ("%zd\n", handle->_ser_source_size); */
      ser_size += handle->_ser_source_size;
    }
  return ser_size;
}

char *lwroc_mon_blocks_source_serialized_copy(char *wire, int conns)
{
  lwroc_mon_block *handle;

  for (handle = _lwroc_mon_blocks[conns]; handle; handle = handle->_next)
    {
      memcpy (wire, handle->_ser_source, handle->_ser_source_size);
      /* printf ("C: %zd\n", handle->_ser_source_size); */
      wire += handle->_ser_source_size;
    }
  return wire;
}

size_t lwroc_mon_blocks_serialized_size(int conns)
{
  lwroc_mon_block *handle;
  size_t ser_size = 0;

  for (handle = _lwroc_mon_blocks[conns]; handle; handle = handle->_next)
    {
      ser_size += handle->_ser_size;
    }
  return ser_size;
}

int lwroc_mon_blocks_stage(int conns)
{
  lwroc_mon_block *handle;

  for (handle = _lwroc_mon_blocks[conns]; handle; handle = handle->_next)
    {
      /* Try to update; report if unsuccessful. */
      if (!lwroc_two_copy_fetch(&handle->_two_copy, handle->_stage))
	return 0;
    }

  return 1;
}

char *lwroc_mon_blocks_serialize(char *wire, int conns)
{
  lwroc_mon_block *handle;

  for (handle = _lwroc_mon_blocks[conns]; handle; handle = handle->_next)
    {
      wire = (handle->_serialize_fcn)(wire, handle->_stage);
    }
  return wire;
}

void lwroc_mon_blocks_notify(void)
{
  lwroc_mon_block *handle;
  int conns;

  for (conns = 0; conns < 2; conns++)
    for (handle = _lwroc_mon_blocks[conns]; handle; handle = handle->_next)
      if (handle->_notify_thread)
	lwroc_thread_block_send_token(handle->_notify_thread);
}

lwroc_pipe_buffer_control *_lwroc_readout_dt_trace_ctr = NULL;

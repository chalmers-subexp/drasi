/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_MON_BLOCK_H__
#define __LWROC_MON_BLOCK_H__

#include "lwroc_net_proto.h"
#include "lwroc_thread_block.h"
#include "lwroc_two_copy.h"
#include "lwroc_pipe_buffer.h"

#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct lwroc_mon_ident_t
{
  uint32_t _ident1;
  uint32_t _ident2;
  uint32_t _ident3;
  uint32_t _ident4;
} lwroc_mon_ident;

extern lwroc_mon_ident _lwroc_mon_ident;

extern volatile int _lwroc_mon_update_seq;

typedef char *(*lwroc_mon_block_serializer)(char *, void *);

typedef void (*lwroc_mon_block_fetch)(void *);

typedef struct lwroc_mon_block_t
{
  struct lwroc_mon_block_t *_next;

  /* This is the (external) source of data. */
  void    *_data;
  size_t   _ser_size;

  void    *_ser_source;
  size_t   _ser_source_size;

  lwroc_mon_block_serializer _serialize_fcn;
  lwroc_mon_block_fetch _fetch_fcn;

  /* This holds a copy of the data, updated by thread.
   * Network thread will copy from here, and retry if data was
   * changed during fetch.
   */
  lwroc_two_copy _two_copy;

  int      _update_seq;

  const lwroc_thread_block *_notify_thread;

  /* The following entry is used by the network thread, as source for
   * serialisation.  Cannot source from copy, since those may change
   * behind our backs.
   */
  void    *_stage;
} lwroc_mon_block;

extern lwroc_pipe_buffer_control *_lwroc_readout_dt_trace_ctrl;

void lwroc_mon_prepare_source(int argc,char *argv[]);

void lwroc_mon_source_set(lwroc_message_source *source);

lwroc_mon_block *lwroc_reg_mon_block(int conn,
				     void *data, size_t data_size,
				     size_t ser_size,
				     lwroc_mon_block_serializer ser_fcn,
				     size_t ser_source_size,
				     lwroc_mon_block_fetch fetch_fcn,
				     lwroc_thread_block *notify_thread);

void lwroc_mon_block_update(lwroc_mon_block *handle,
			    lwroc_item_time *block_time,
			    uint64_t *block_cputime_usec);

uint32_t lwroc_mon_blocks_num(int conns);

size_t lwroc_mon_blocks_source_serialized_size(int conns);

char *lwroc_mon_blocks_source_serialized_copy(char *wire, int conns);

size_t lwroc_mon_blocks_serialized_size(int conns);

int lwroc_mon_blocks_stage(int conns);

char *lwroc_mon_blocks_serialize(char *wire, int conns);

void lwroc_mon_blocks_notify(void);

/* First we check if an update has been scheduled (requested) by the
 * network thread (which handles the timer).
 *
 * Each block is expected to contain a lwroc_item_time _time member,
 * which is filled before doing the copying.
 */

#define LWROC_MON_CHECK_COPY_BLOCK_INT(handle, block, force,	\
				       cputime_usec)		\
  do {								\
    int __update_seq = _lwroc_mon_update_seq;			\
    if (__update_seq != (handle)->_update_seq || force) {	\
      (handle)->_update_seq = __update_seq;			\
      if ((handle)->_fetch_fcn)					\
	(handle)->_fetch_fcn((handle)->_data);			\
      lwroc_mon_block_update((handle), &(block)->_time,		\
			     cputime_usec);			\
    }								\
  } while (0)

#define LWROC_MON_CHECK_COPY_BLOCK(handle, block, force)	\
  LWROC_MON_CHECK_COPY_BLOCK_INT(handle, block, force,		\
				 &(block)->_cputime_usec)

#define LWROC_MON_UPDATE_PENDING(handle)			\
  (_lwroc_mon_update_seq != (handle)->_update_seq)

#ifdef __cplusplus
}
#endif

#endif/*__LWROC_MON_BLOCK_H__*/

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2019  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_net_badrequest.h"
#include "lwroc_hostname_util.h"
#include "lwroc_serializer_util.h"

#include <stdlib.h>
#include <arpa/inet.h>

/********************************************************************/

const char *lwroc_badreq_reason_string(uint32_t failure)
{
  const char *reason = NULL;

  switch (failure)
    {
    case LWROC_BADREQUEST_BAD_MAGIC:
      reason = "bad magic";
      break;
    case LWROC_BADREQUEST_BAD_SIZE:
      reason = "bad size";
      break;
    case LWROC_BADREQUEST_BAD_REQUEST:
      reason = "bad request";
      break;
    case LWROC_BADREQUEST_NONCFG_REQUEST:
      reason = "non-configured option";
      break;
    case LWROC_BADREQUEST_MULTIPLE_RECEIVER:
      reason = "multiple receivers";
      break;
    case LWROC_BADREQUEST_IN_USE:
      reason = "in use";
      break;
    case LWROC_BADREQUEST_NO_REVERSE_LINK:
      reason = "no (reverse) link";
      break;
    case LWROC_BADREQUEST_BAD_REVERSE_LINK:
      reason = "bad (reverse) link ack";
      break;
    case LWROC_BADREQUEST_WAIT_LOG_CLIENT:
      reason = "waiting for log client/destination";
      break;
    case LWROC_BADREQUEST_SHUTDOWN:
      reason = "shutting down";
      break;
    }

  return reason;
}

/********************************************************************/

void lwroc_net_badrequest_report(lwroc_format_message_context *context,
				 const char *who,
				 lwroc_badrequest *badreq,
				 const char *rev_link_missing)
{
  const char *reason =
    lwroc_badreq_reason_string(badreq->_failure);

  if (badreq->_failure == LWROC_BADREQUEST_BAD_REVERSE_LINK &&
      badreq->_hint == LWROC_BADREVLNK_HINT_NOT_FOUND)
    {
      char look_dotted[INET6_ADDRSTRLEN];

      struct sockaddr_storage look_addr;

      lwroc_set_ipv4_addr(&look_addr, badreq->_seen_ipv4_addr);
      lwroc_inet_ntop(&look_addr, look_dotted, sizeof(look_dotted));

      LWROC_CCWARNING_FMT(context,
			  "%s: "
			  "request is bad (%s - not listed).  "
			  "IPv4?, we look like %s [%08x] to other end "
			  "(missing %s ?).",
			  who,
			  reason, look_dotted, badreq->_seen_ipv4_addr,
			  rev_link_missing ?
			  rev_link_missing :
			  "*internal confusion*");
    }
  else if (reason)
    {
      /* No need to tell if we sent a 'waiting for log client message.
       * In that case, we are regularly printing the overall wait message.
       * (This would just spam the console at startup.)
       */
      if (badreq->_failure != LWROC_BADREQUEST_WAIT_LOG_CLIENT)
	LWROC_CCWARNING_FMT(context,
			    "%s: request is bad (%s, hint %d).",
			    who,
			    reason, badreq->_hint);
    }
  else
    {
      char hollerith[5];

      lwroc_uint32_to_hollerith(hollerith, badreq->_failure);

      LWROC_CCWARNING_FMT(context,
			  "%s: request is bad "
			  "(unknown: 0x%08x/%s, hint %d).",
			  who,
			  badreq->_failure, hollerith, badreq->_hint);
    }
}

/********************************************************************/

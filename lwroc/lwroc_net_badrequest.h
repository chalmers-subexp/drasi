/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2019  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_NET_BADREQUEST_H__
#define __LWROC_NET_BADREQUEST_H__

#include "lwroc_net_proto.h"
#include "lwroc_message.h"

/********************************************************************/

void lwroc_net_badrequest_report(lwroc_format_message_context *context,
				 const char *who,
				 lwroc_badrequest *badreq,
				 const char *rev_link_missing);

/********************************************************************/


#endif/*__LWROC_NET_BADREQUEST_H__*/

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_net_client_base.h"
#include "lwroc_message.h"
#include "lwroc_net_io_util.h"

#include <fcntl.h>
#include <sys/types.h>
#include <stdio.h>
#include <errno.h>
#include <assert.h>

#include "../dtc_arch/acc_def/myprizd.h"
#include "../dtc_arch/acc_def/has_msg_nosignal.h"

/********************************************************************/

int lwroc_net_client_base_after_fail(lwroc_select_item *item,
				     lwroc_select_info *si)
{
  lwroc_net_client_base *client =
    PD_LL_ITEM(item, lwroc_net_client_base, _select_item);
  (void) si;

  /* printf ("close %d\n",client->_fd); */

  if (client->_fd != -1)
    lwroc_safe_close(client->_fd);
  client->_fd = -1;

  PD_LL_REMOVE(&client->_clients);

  return 0;
}

void lwroc_net_client_base_delete(lwroc_select_item *item)
{
  lwroc_net_client_base *client =
    PD_LL_ITEM(item, lwroc_net_client_base, _select_item);

  assert(client->_fd == -1);

  free (client);
}

/********************************************************************/

int lwroc_discard_read_check_eof(int fd,
				 lwroc_select_info *si,
				 size_t *account)
{
  ssize_t n;
  char buf[256];

  if (!LWROC_READ_FD_ISSET(fd, si))
    return 1; /* still alive, no data to contribute */

#if STATE_MACHINE_DEBUG
  printf ("data avail to read...\n");
#endif

  /* We will just discard data. */
  n = read (fd, buf, sizeof (buf));

#if STATE_MACHINE_DEBUG
  printf ("n = %" MYPRIzd "...\n", n);
#endif

  if (n == 0)
    return 0; /* Socket closed. */

  if (n == -1)
    {
      if (errno == EINTR ||
	  errno == EAGAIN)
	return 1;

      /* Errors are sort of usual here, only print in debug mode... */
      /* in DEBUG? (PERROR)
      LWROC_PERROR("read");
      LWROC_ERROR("Error while discard reading from client.");
      */
      return 0;
    }

  if (account)
    *account += (size_t) n;

  _lwroc_mon_net._recv_misc += (uint64_t) n;

  return 1; /* still alive */
}

/********************************************************************/

int lwroc_fail_on_read_check_eof(int fd,
				 lwroc_select_info *si)
{
  size_t got = 0;

  if (!lwroc_discard_read_check_eof(fd, si, &got))
    return 0;

  /* We fail also if we got some data. */

  return got == 0;
}

/********************************************************************/

int lwroc_net_client_base_create_socket(lwroc_net_client_base *conn,
					int socket_type)
{
  int type = 0;
  int protocol = 0;

  switch(socket_type)
    {
    case LWROC_SOCKET_TCP:
      type = SOCK_STREAM;
      protocol = IPPROTO_TCP;
      break;
    case LWROC_SOCKET_UDP:
      type = SOCK_DGRAM;
      protocol = IPPROTO_UDP;
      break;
    default:
      LWROC_BUG_FMT("Unknown socket type %d.", socket_type);
    }

  conn->_fd = socket(PF_INET, type, protocol);

  if (conn->_fd < 0)
    {
      LWROC_CPERROR(conn,"socket");
      LWROC_CWARNING(conn,"Cannot create socket.");
      return 0;
    }

  /* Make the server socket non-blocking, such that we're not hit by
   * false selects (see Linux man page bug notes).
   */

  lwroc_net_io_nonblock(conn->_fd, 1,
			"<unknown>" /* TODO: need a descr/purpose in conn? */);

  return 1;
}

/********************************************************************/

int lwroc_net_client_base_connect_socket(lwroc_net_client_base *conn,
					 struct sockaddr_storage *serv_addr)
{
  for ( ; ; )
    {
      int ret;

      ret = lwroc_net_io_connect(conn->_fd, serv_addr);

      if (ret == 0)
	{
	  /* Success. */
	  return 1;
	}
      else
	{
	  if (errno == EINTR)
	    continue; /* Try again. */

	  if (errno == EINPROGRESS ||
	      errno == EWOULDBLOCK)
	    return 0;

	  LWROC_CPERROR(conn,"connect");
	  LWROC_CWARNING(conn,"Failure connecting to server.");

	  /* This socket failed, remove it. */
	  lwroc_safe_close(conn->_fd);
	  conn->_fd = -1;
	  return -1;
	}
    }
}

int lwroc_net_client_base_connect_get_status(lwroc_net_client_base *conn,
					     int *so_error)
{
  socklen_t optlen;
  int ret;

  /* Get the status when the result is delayed (EINPROGRESS /
   * EWOULDBLOCK above).
   */

  optlen = sizeof(*so_error);

  ret = getsockopt(conn->_fd,SOL_SOCKET,SO_ERROR,so_error,&optlen);

  if (ret != 0)
    {
      LWROC_CPERROR(conn,"getsockopt");
      exit(1);
    }

  if (*so_error != 0)
    return 0;

  return 1; /* Success. */
}

/********************************************************************/

void lwroc_net_client_rw_set(lwroc_net_client_rw *buf,
			     char *ptr, size_t maxsize,
			     size_t size, const char *descr)
{
  if (size > maxsize)
    LWROC_BUG_FMT("Attempt to set too large rw buffer size for '%s' "
		  "(%" MYPRIzd " > %" MYPRIzd ").",
		  descr, size, maxsize);

  buf->_ptr = ptr;
  buf->_size = size;
  buf->_offset = 0;
}

/********************************************************************/

int lwroc_net_client_rw_read(lwroc_net_client_rw *buf,
			     int fd,
			     const char *purpose, uint64_t *account)
{
  size_t left = buf->_size - buf->_offset;
  ssize_t n;

  assert (left != 0);

  n = read(fd, buf->_ptr + buf->_offset, left);

  if (n == 0)
    {
      /* Socket closed on other end, this we treat as an error. */

      /* TODO: can we have false selects?, such that we should
       * consult some socket routine to verify the socket really
       * is closed?
       */
      return 0;
    }

  if (n == -1)
    {
      if (errno == EINTR ||
	  errno == EAGAIN)
	return 1;

      LWROC_PERROR("read");
      LWROC_ERROR_FMT("Error while reading from client (%s).", purpose);
      return 0;
    }

  /*
  printf ("%p: read %" MYPRIzd " (%d, %s)\n", buf, n, fd, purpose);
  */

  /* Read was successful */

  *account += (uint64_t) n;

  buf->_offset += (size_t) n;

  return 1;
}

int lwroc_net_client_rw_write(lwroc_net_client_rw *buf,
			      int fd, int ispipe,
			      const char *purpose, uint64_t *account)
{
  size_t left = buf->_size - buf->_offset;
  ssize_t n;

  assert (left != 0);
  /*
  printf ("%p (%p %" MYPRIzd " %" MYPRIzd " %" MYPRIzd ")\n",
	  buf, buf->_ptr, buf->_size, buf->_offset, left);
  */

  if (ispipe)
    n = write(fd, buf->_ptr + buf->_offset, left);
  else
    n = send(fd, buf->_ptr + buf->_offset, left, MSG_NOSIGNAL);

  if (n == -1)
    {
      if (errno == EINTR ||
	  errno == EAGAIN)
	return 1;

      if (errno == EPIPE) {
	LWROC_INFO_FMT("Client (%s) has disconnected, closed.", purpose);
      } else {
	LWROC_PERROR("write");
	LWROC_ERROR_FMT("Error while writing to client (%s).", purpose);
      }
      return 0;
    }

  /*
  printf ("%p: write %" MYPRIzd " (%d, %s)\n", buf, n, fd,purpose);
  */

  /* Write was successful */

  *account += (uint64_t) n;

  buf->_offset += (size_t) n;

  return 1;
}

/********************************************************************/

int lwroc_net_client_base_read(lwroc_net_client_base *conn,
			       const char *purpose, uint64_t *account)
{
  return lwroc_net_client_rw_read(&conn->_buf, conn->_fd,
				  purpose, account);
}

int lwroc_net_client_base_write(lwroc_net_client_base *conn,
				const char *purpose, uint64_t *account)
{
  return lwroc_net_client_rw_write(&conn->_buf, conn->_fd, 0,
				   purpose, account);
}

/********************************************************************/


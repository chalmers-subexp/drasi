/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_NET_CLIENT_BASE_H__
#define __LWROC_NET_CLIENT_BASE_H__

#include "pd_linked_list.h"
#include "lwroc_select_util.h"
#include "lwroc_net_proto.h"
#include "lwroc_message.h"
#include "lwroc_net_conn_monitor.h"

#include <stdlib.h>
#include "../dtc_arch/acc_def/netinet_in_include.h"
#include "../dtc_arch/acc_def/socket_include.h"
#include "../dtc_arch/acc_def/socket_types.h"

/********************************************************************/

typedef struct lwroc_net_client_rw_t
{
  size_t _size;
  size_t _offset;
  char  *_ptr;

} lwroc_net_client_rw;

/********************************************************************/

void lwroc_net_client_rw_set(lwroc_net_client_rw *buf,
			     char *ptr, size_t bufsize,
			     size_t size, const char *descr);

#define LWROC_NET_CLIENT_RW_SET(buf, destitem, size)		 \
  lwroc_net_client_rw_set(buf,					 \
			  (char *) &destitem, sizeof (destitem), \
			  size, #destitem);

/********************************************************************/

typedef struct lwroc_net_client_base_t
{
  pd_ll_item _clients;

  lwroc_select_item _select_item;

  /* Communicating socket */
  int _fd;

  /* Where we're reading / writing */
  lwroc_net_client_rw _buf;

  /* To whom are we (supposed to be) connected. */
  struct sockaddr_storage _addr;
  /* Easy formatting of our name and kind for messages. */
  lwroc_format_message_context _msg_context;

  /* Monitoring the connection. */
  lwroc_net_conn_monitor *_mon;

} lwroc_net_client_base;

/********************************************************************/

int lwroc_net_client_base_after_fail(lwroc_select_item *item,
				     lwroc_select_info *si);

void lwroc_net_client_base_delete(lwroc_select_item *item);

/********************************************************************/

typedef struct lwroc_msg_wire_t
{
  char  *_wire;
  size_t _size;
} lwroc_msg_wire;

/********************************************************************/

int lwroc_discard_read_check_eof(int fd,
				 lwroc_select_info *si,
				 size_t *addread);

int lwroc_fail_on_read_check_eof(int fd,
				 lwroc_select_info *si);

/********************************************************************/

#define LWROC_SOCKET_TCP  1
#define LWROC_SOCKET_UDP  2

int lwroc_net_client_base_create_socket(lwroc_net_client_base *conn,
					int socket_type);

int lwroc_net_client_base_connect_socket(lwroc_net_client_base *conn,
					 struct sockaddr_storage *serv_addr);

int lwroc_net_client_base_connect_get_status(lwroc_net_client_base *conn,
					     int *so_error);

/********************************************************************/

int lwroc_net_client_rw_read(lwroc_net_client_rw *buf,
			     int fd,
			     const char *purpose, uint64_t *account);

int lwroc_net_client_rw_write(lwroc_net_client_rw *buf,
			      int fd, int ispipe,
			      const char *purpose, uint64_t *account);

/********************************************************************/

int lwroc_net_client_base_read(lwroc_net_client_base *conn,
			       const char *purpose, uint64_t *account);

int lwroc_net_client_base_write(lwroc_net_client_base *conn,
				const char *purpose, uint64_t *account);

/********************************************************************/

extern lwroc_monitor_net_block _lwroc_mon_net;

/********************************************************************/

#endif/*__LWROC_NET_CLIENT_BASE_H__*/

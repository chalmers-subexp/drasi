/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_message.h"
#include "lwroc_net_proto.h"
#include "lwroc_mon_block.h"
#include "lwroc_net_conn_monitor.h"
#include "lwroc_hostname_util.h"
#include "lwroc_data_pipe.h"

#include "gen/lwroc_message_source_serializer.h"
#include "gen/lwroc_monitor_conn_source_serializer.h"
#include "gen/lwroc_monitor_conn_block_serializer.h"

#include <string.h>
#include <assert.h>
#include "../dtc_arch/acc_def/myinttypes.h"

char *lwroc_mon_conn_serialise(char *wire, void *block)
{
  lwroc_monitor_conn_block *conn_block =
    (lwroc_monitor_conn_block *) block;

  return lwroc_monitor_conn_block_serialize(wire, conn_block);
}

void lwroc_net_conn_monitor_fetch(void *ptr)
{
  lwroc_net_conn_monitor *conn_mon = (lwroc_net_conn_monitor *)
    (((char *) ptr) - offsetof(lwroc_net_conn_monitor, _block));

  if (conn_mon->_pipe_buf)
    {
      conn_mon->_block._buf_data_fill =
	lwroc_pipe_buffer_fill(conn_mon->_pipe_buf);
      if (lwroc_pipe_buffer_blocked(conn_mon->_pipe_buf))
	conn_mon->_block._buf_data_fill |= LWROC_CONN_DATA_FILL_FULL;
    }
  else if (conn_mon->_pipe_buf_consumer)
    {
      conn_mon->_block._buf_data_fill =
	lwroc_pipe_buffer_fill_due_to_consumer(conn_mon->_pipe_buf_consumer);
      if (lwroc_pipe_buffer_blocked_from_consumer(conn_mon->_pipe_buf_consumer))
	conn_mon->_block._buf_data_fill |= LWROC_CONN_DATA_FILL_FULL;
    }

  if (conn_mon->_pipe_extra)
    {
      conn_mon->_block._events =
	conn_mon->_pipe_extra->_events;
      conn_mon->_block._bytes =
	conn_mon->_pipe_extra->_bytes;
    }

  if (conn_mon->_timestamp_ptr)
    {
      conn_mon->_block._timestamp = *(conn_mon->_timestamp_ptr);
      conn_mon->_block._timestprv = *(conn_mon->_timestamp_ptr+1);
      /*printf ("get ts: %" PRIu64 "\n", conn_mon->_block._timestamp);*/
    }
}

lwroc_net_conn_monitor *
lwroc_net_conn_monitor_init(uint32_t type,
			    uint32_t direction,
			    const char *hostname,
			    const char *label,
			    struct sockaddr_storage *addr,
			    lwroc_pipe_buffer_control *pipe_buf,
			    lwroc_pipe_buffer_consumer *pipe_buf_consumer,
			    uint64_t *timestamp_ptr,
			    lwroc_data_pipe_extra *pipe_extra,
			    void *aux,
			    lwroc_mon_block_fetch fetch_fcn,
			    uint32_t kind, uint32_t kind_sub)
{
  lwroc_net_conn_monitor *conn_mon;

  conn_mon =
    (lwroc_net_conn_monitor *) malloc (sizeof (lwroc_net_conn_monitor));

  if (!conn_mon)
    LWROC_FATAL("Memory allocation failure (conn mon).");

  memset (conn_mon, 0, sizeof (*conn_mon));

  conn_mon->_pipe_buf = pipe_buf;
  conn_mon->_pipe_buf_consumer = pipe_buf_consumer;
  conn_mon->_timestamp_ptr = timestamp_ptr;
  conn_mon->_pipe_extra = pipe_extra;
  conn_mon->_aux = aux;

  {
    lwroc_monitor_conn_source conn_src;
    lwroc_monitor_conn_source_sersize sz_conn_src;
    char *wire;

    lwroc_mon_block *mon_conn_handle;

    memset(&conn_src, 0, sizeof (conn_src));

    conn_src._type      = type;
    conn_src._direction = direction;
    conn_src._hostname  = (hostname ? hostname : "");
    conn_src._label     = (label ? label : "");

    conn_src._kind      = kind;
    conn_src._kind_sub  = kind_sub;

    /* LWROC_WARNING_FMT("kind: %x %x", kind, kind_sub); */

    if (addr)
      {
	conn_src._ipv4_addr    = lwroc_get_ipv4_addr(addr);
	conn_src._port = lwroc_get_port(addr);
      }

    if (pipe_buf)
      {
	conn_src._buf_data_size =
	  lwroc_pipe_buffer_size(pipe_buf);
	if (!fetch_fcn)
	  fetch_fcn = lwroc_net_conn_monitor_fetch;
      }
    else if (pipe_buf_consumer)
      {
	conn_src._buf_data_size =
	  lwroc_pipe_buffer_size_from_consumer(pipe_buf_consumer);
	if (!fetch_fcn)
	  fetch_fcn = lwroc_net_conn_monitor_fetch;
      }

    mon_conn_handle =
      lwroc_reg_mon_block(1, &conn_mon->_block, sizeof (conn_mon->_block),
			  lwroc_monitor_conn_block_serialized_size(),
			  lwroc_mon_conn_serialise,
			  lwroc_monitor_conn_source_serialized_size(&conn_src,
								    &sz_conn_src),
			  fetch_fcn, NULL);

    wire = mon_conn_handle->_ser_source;

    wire = lwroc_monitor_conn_source_serialize(wire, &conn_src,
					       &sz_conn_src);

    assert (wire == (mon_conn_handle->_ser_source +
		     mon_conn_handle->_ser_source_size));

    conn_mon->_handle = mon_conn_handle;
  }

  /* TODO: Should this setting really be here, or better at each user? */
  /* If the connection has an address, it is a real (network) connection.
   * Obviously since at init: not yet attempted.
   */
  if (addr)
    {
      conn_mon->_block._status = LWROC_CONN_STATUS_NOT_ATTEMPTED;
      /* Typically: LWROC_MON_CHECK_COPY_CONN_MON_BLOCK(conn_mon, 1); */
      lwroc_two_copy_update(&conn_mon->_handle->_two_copy,
			    conn_mon->_handle->_data);
    }

  return conn_mon;
}

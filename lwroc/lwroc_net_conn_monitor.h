/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_NET_CONN_MONITOR_H__
#define __LWROC_NET_CONN_MONITOR_H__

#include "lwroc_net_proto.h"
#include "lwroc_mon_block.h"
#include "lwroc_pipe_buffer.h"
#include "lwroc_data_pipe.h"

#include "../dtc_arch/acc_def/netinet_in_include.h"
#include "../dtc_arch/acc_def/socket_types.h"

typedef struct lwroc_net_conn_monitor_t
{
  lwroc_monitor_conn_block   _block;
  lwroc_mon_block           *_handle;

  lwroc_pipe_buffer_control *_pipe_buf;
  lwroc_pipe_buffer_consumer *_pipe_buf_consumer;
  uint64_t                  *_timestamp_ptr;
  lwroc_data_pipe_extra     *_pipe_extra;

  void                      *_aux;

} lwroc_net_conn_monitor;

lwroc_net_conn_monitor *
lwroc_net_conn_monitor_init(uint32_t type,
			    uint32_t direction,
			    const char *hostname,
			    const char *label,
			    struct sockaddr_storage *addr,
			    lwroc_pipe_buffer_control *pipe_buf,
			    lwroc_pipe_buffer_consumer *pipe_buf_consumer,
			    uint64_t *timestamp_ptr,
			    lwroc_data_pipe_extra *pipe_extra,
			    void *aux,
			    lwroc_mon_block_fetch fetch_fcn,
			    uint32_t kind, uint32_t kind_sub);

void lwroc_net_conn_monitor_fetch(void *ptr);

#define LWROC_MON_CHECK_COPY_CONN_MON_BLOCK(conn_mon, force)		\
  LWROC_MON_CHECK_COPY_BLOCK_INT(conn_mon->_handle, &conn_mon->_block, force, \
				 NULL)

#endif/*__LWROC_NET_CONN_MONITOR_H__*/

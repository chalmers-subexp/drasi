/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_net_control.h"
#include "lwroc_net_io.h"
#include "lwroc_control.h"
#include "lwroc_message.h"
#include "lwroc_hostname_util.h"
#include "lwroc_net_message.h"

/* These files are in a non-standard place... */
#include "../lwrocmon/lwroc_ctrl_token.h"
#include "../lwrocmon/lwroc_ctrl_token.c"

#include "gen/lwroc_control_request_serializer.h"
#include "gen/lwroc_control_token_serializer.h"
#include "gen/lwroc_control_response_serializer.h"

#include <errno.h>
#include <string.h>
#include <assert.h>
#include <arpa/inet.h>
#include "../dtc_arch/acc_def/myinttypes.h"
#include "../dtc_arch/acc_def/myprizd.h"
#include "../dtc_arch/acc_def/fall_through.h"

/********************************************************************/

lwroc_ctrl_token *_lwroc_control_tokens = NULL;

void lwroc_net_control_setup(void)
{
  lwroc_ctrl_token_readall(1, &_lwroc_control_tokens);
}

/********************************************************************/

#define LWROC_SET_CTRL_STATE(client, new_state) do {	\
    if (0) {						\
      printf ("%d: state %d -> %d\n", __LINE__,		\
	      client->_ctrl._state,			\
	      new_state);				\
    }							\
    client->_ctrl._state = new_state;			\
  } while (0)

/********************************************************************/

void lwroc_net_control_setup_select(lwroc_select_item *item,
				    lwroc_select_info *si)
{
  lwroc_net_control *client =
    PD_LL_ITEM(item, lwroc_net_control, _base._select_item);

  /* TODO: timeout while reading (and writing), such that broken
   * clients cannot hang us.
   *
   * Alternatively, print warnings?  That we should at least do if
   * whatever internal process that should deal with the request does
   * not in a timely fashion.
   */

  /* Are we supposed to read a request? */

  if (client->_ctrl._state == LWROC_NET_CONTROL_STATE_READ ||
      client->_ctrl._state == LWROC_NET_CONTROL_STATE_READ_DATA ||
      client->_ctrl._state == LWROC_NET_CONTROL_STATE_WRITE_ERROR )
    LWROC_READ_FD_SET(client->_base._fd, si);

  /* Are we supposed to write a response? */

  if (client->_ctrl._state == LWROC_NET_CONTROL_STATE_WRITE_INIT ||
      client->_ctrl._state == LWROC_NET_CONTROL_STATE_WRITE ||
      client->_ctrl._state == LWROC_NET_CONTROL_STATE_WRITE_TOKEN ||
      client->_ctrl._state == LWROC_NET_CONTROL_STATE_WRITE_ACCEPT ||
      client->_ctrl._state == LWROC_NET_CONTROL_STATE_WRITE_DATA ||
      client->_ctrl._state == LWROC_NET_CONTROL_STATE_WRITE_ERROR)
    LWROC_WRITE_FD_SET(client->_base._fd, si);
}

void lwroc_net_control_response(lwroc_net_control *client,
				lwroc_control_response *ctrl_response,
				const char *err_msg,
				int next_state)
{
  /*lwroc_control_response_sersize sz_ctrl_response; */
  size_t size;

  size = lwroc_control_response_serialized_size(/*ctrl_response,
						  &sz_ctrl_response*/);

  assert(ctrl_response->_err_msg[0] == 0);
  if (err_msg)
    {
      if (strlen(err_msg) > sizeof (ctrl_response->_err_msg) - 1)
	LWROC_BUG("Error message too long for control response.");
      strcpy(ctrl_response->_err_msg, err_msg);
      /* ctrl_response._err_msg = (char *) (err_msg); */
    }

  assert(sizeof (client->_ctrl._raw) >= size);

  lwroc_control_response_serialize(client->_ctrl._raw,
				   ctrl_response/*, &sz_ctrl_response*/);

  LWROC_SET_CTRL_STATE(client, next_state);

  client->_base._buf._ptr  = client->_ctrl._raw;
  client->_base._buf._size = size;
  client->_base._buf._offset = 0;

  /* printf ("transmit response: %zd\n", client->_base._buf._size); */
}

void lwroc_net_control_status_response(lwroc_net_control *client,
				       uint32_t status,
				       uint32_t origin,
				       const char *err_msg,
				       uint32_t max_data_size,
				       int next_state)
{
  lwroc_control_response ctrl_response;

  (void) origin;
  (void) err_msg;

  memset (&ctrl_response, 0, sizeof(ctrl_response));

  ctrl_response._status = status;
  ctrl_response._origin = origin;
  ctrl_response._response_data_size = max_data_size;

  lwroc_net_control_response(client, &ctrl_response, err_msg, next_state);
}

void lwroc_net_control_notify_request(lwroc_net_control *client)
{
  lwroc_control_buffer *ctrl = client->_wait_ctrl;

  /* Set us up to be notified when it has been handled. */
  ctrl->_thread_notify_handled = _lwroc_net_io_thread->_block;

  /* Make sure data was written before we update state. */
  SFENCE;
  /* All filled! */
  ctrl->_state = LWROC_CONTROL_STATE_MESSAGE_READY;
  /* Request is filled, notify whoever shall deal with it. */
  LWROC_THREAD_BLOCK_NOTIFY(&ctrl->_thread_notify_ready);

  /* It makes no sense to send a short underway
   * message 'dispatched' response to the requestor.
   * Since we are already now going to handle the
   * message, non-reception of a 'dispatched' message
   * does not guarantee that whoever is handling the
   * request has not partially or fully handled the
   * request.
   */

  client->_ctrl._state = LWROC_NET_CONTROL_STATE_WAIT_HANDLE;
  /* printf ("control dispatched\n"); */
}

int lwroc_net_control_after_select(lwroc_select_item *item,
				   lwroc_select_info *si)
{
  lwroc_net_control *client =
    PD_LL_ITEM(item, lwroc_net_control, _base._select_item);

  if (client->_ctrl._state == LWROC_NET_CONTROL_STATE_WAIT_HANDLE)
    {
      if (client->_wait_ctrl->_state == LWROC_CONTROL_STATE_MESSAGE_HANDLED)
	{
	  lwroc_control_buffer *ctrl = client->_wait_ctrl;

	  /* Make sure we read values after the state. */
	  MFENCE;
	  /* printf ("control operation done\n"); */

	  /* Check that the responder did not write more data than it
	   * was allowed to!
	   *
	   * These checks should really (and are) be performed by the
	   * thread that performed the actual control action /
	   * writing.  We therefore do *not* bug out or stop here.  If
	   * the process is still alive, we will try to report to the
	   * client that someone screwed up - bigtime.
	   */

	  if (ctrl->_response._response_data_size >
	      ctrl->_request._response_data_maxsize)
	    {
	      LWROC_CERROR_FMT(&client->_base,
			       "Control performer (our side) has written "
			       "more data (%" PRIu32 ") "
			       "than allowed (%" PRIu32 ") - "
			       "buffer overflow.",
			       ctrl->_response._response_data_size,
			       ctrl->_request._response_data_maxsize);
	      LWROC_SET_CONTROL_STATUS(ctrl, PERFORMER_FAIL,
				       SRV_POST_PERF,
				       "Performer wrote too much data.");
	    }
	  /* We also check the canary values. */
	  else if (ctrl->_request._response_data_maxsize &&
		   (ctrl->_canaries[0][0] !=  ctrl->_canary ||
		    ctrl->_canaries[0][1] != ~ctrl->_canary ||
		    ctrl->_canaries[1][0] != ~ctrl->_canary ||
		    ctrl->_canaries[1][1] !=  ctrl->_canary))
	    {
	      LWROC_CERROR_FMT(&client->_base,
			       "Control performer (our side) has overwritten "
			       "the data canary values "
			       "(0x%" PRIx32 " != expect 0x%" PRIx32 "),"
			       "(0x%" PRIx32 " != expect 0x%" PRIx32 "),"
			       "(0x%" PRIx32 " != expect 0x%" PRIx32 "),"
			       "(0x%" PRIx32 " != expect 0x%" PRIx32 ") - "
			       "buffer overflow?",
			       ctrl->_canaries[0][0],  ctrl->_canary,
			       ctrl->_canaries[0][1], ~ctrl->_canary,
			       ctrl->_canaries[1][0], ~ctrl->_canary,
			       ctrl->_canaries[1][1],  ctrl->_canary);
	      LWROC_SET_CONTROL_STATUS(ctrl, PERFORMER_FAIL,
				       SRV_POST_PERF,
				       "Performer has overwritten "
				       "canary value.");
	    }

	  /* Check that is sends a 'valid' response marker. */

	  if (ctrl->_response._status != LWROC_CONTROL_STATUS_SUCCESS &&
	      ctrl->_response._status != LWROC_CONTROL_STATUS_INVALID &&
	      ctrl->_response._status != LWROC_CONTROL_STATUS_DENIED &&
	      ctrl->_response._status != LWROC_CONTROL_STATUS_FAILED &&
	      ctrl->_response._status != LWROC_CONTROL_STATUS_ALREADY &&
	      ctrl->_response._status != LWROC_CONTROL_STATUS_SHUTDOWN &&
	      ctrl->_response._status != LWROC_CONTROL_STATUS_RESP_TOO_LARGE)
	    {
	      LWROC_CERROR_FMT(&client->_base,
			       "Control performer (our side) returned invalid "
			       "success code (%08x).",
			       ctrl->_response._status);

	      LWROC_SET_CONTROL_STATUS(ctrl, PERFORMER_FAIL,
				       SRV_POST_PERF,
				       "Performer returned "
				       "an invalid response code.");
	    }

	  if (ctrl->_response_err_msg &&
	      strlen(ctrl->_response_err_msg) >
	      sizeof (ctrl->_response._err_msg) - 1)
	    {
	      LWROC_CERROR_FMT(&client->_base,
			       "Control performer (our side) provided "
			       "too long error message "
			       "(%" MYPRIzd " > %" MYPRIzd ").",
			       strlen(ctrl->_response_err_msg),
			       sizeof (ctrl->_response._err_msg) - 1);

	      LWROC_SET_CONTROL_STATUS(ctrl, PERFORMER_FAIL,
				       SRV_POST_PERF,
				       "Performer provided "
				       "too long error message.");
	    }

	  /* Write the response to the requestor. */
	  lwroc_net_control_response(client, &ctrl->_response,
				     ctrl->_response_err_msg,
				     LWROC_NET_CONTROL_STATE_WRITE);

	  /* It is this thread waiting, so no need to fence memory. */
	  ctrl->_state = LWROC_CONTROL_STATE_WAIT_MESSAGE;
	  ctrl = NULL;
	}
      return 1;
    }

  if (client->_ctrl._state == LWROC_NET_CONTROL_STATE_WRITE_ERROR)
    {
      /* The client may be writing some junk to us...  We still want
       * to deliver the error message, so try for a bit to discard
       * what it writes to us.
       */

      if (!lwroc_discard_read_check_eof(client->_base._fd, si, NULL))
	return 0;
    }

  if (client->_ctrl._state == LWROC_NET_CONTROL_STATE_READ)
    {
      if (!LWROC_READ_FD_ISSET(client->_base._fd, si))
	return 1;

      /* We first need to get the size. */

      if (client->_base._buf._offset >= sizeof (uint32_t) &&
	  client->_base._buf._size == sizeof (uint32_t))
	{
	  uint32_t *raw_u32 = (uint32_t *) client->_ctrl._raw;
	  size_t size;

	  size = ntohl(*raw_u32);
	  if (size > sizeof (client->_ctrl._raw) ||
	      size <= sizeof (uint32_t)) /* No space for magic... */
	    {
	      /* We should handle this more gracefully.  Like try to
	       * consume the incoming data and send an error response.
	       * Hmmm, better to teach clients to not expect that
	       * we will not read it all in case of error, and check
	       * for early error responses too.  Perhaps better to
	       * do both.  And in any case, time the connection out
	       * after some time - finally give up...
	       */

	      lwroc_net_control_status_response(client,
		LWROC_CONTROL_STATUS_PROTOCOL_ERROR,
		LWROC_CONTROL_STAT_ORIG_SERVER,
		"Protocol error (no space for magic).", 0,
		LWROC_NET_CONTROL_STATE_WRITE_ERROR);
	      return 1;
	    }

	  client->_base._buf._size = size;
	}

      if (!lwroc_net_client_base_read(&client->_base, "control",
				      &_lwroc_mon_net._recv_ctrl))
	return 0;

      /*
      printf ("got ctrl: %zd (%zd)\n",
	      client->_base._buf._offset,
	      client->_base._buf._size);
      */

      if (client->_base._buf._offset >= client->_base._buf._size &&
	  client->_base._buf._size > sizeof (uint32_t))
	{
	  lwroc_msg_header_wire *header;
	  uint32_t magic;

	  lwroc_control_request  ctrl_request;
	  lwroc_control_response ctrl_response;
	  lwroc_deserialize_error desererr;
	  const char *end;
	  const char *ctrl_response_err_msg;

	  header = (lwroc_msg_header_wire *) client->_base._buf._ptr;
	  magic = ntohl(header->_magic);

	  /* printf ("magic 0x%08x\n", magic); */

	  /* Handle control tokens. */
	  if (magic == LWROC_CONTROL_TOKEN_SERPROTO_MAGIC1)
	    {
	      lwroc_control_token token_request;
	      lwroc_control_token token_response;

	      end = lwroc_control_token_deserialize(&token_request,
						    client->_ctrl._raw,
						    client->_base._buf._offset,
						    &desererr);

	      if (end == NULL)
		goto token_protocol_failure;

	      /* printf ("parsed: %zd\n", client->_base._buf._offset); */

	      memset(&token_response, 0, sizeof (token_response));

	      token_response._op     = token_request._op;
	      token_response._token  = token_request._token;

	      switch (token_request._status)
		{
		case LWROC_CONTROL_TOKEN_STAT_TOKEN:
		  /* Do we know the token? */

		  if (lwroc_ctrl_token_check(_lwroc_control_tokens,
					     token_request._token,
					     token_request._op,
					     0, 0))
		    {
		      client->_token_challenge =
			(((uint32_t) si->now.tv_usec) ^
			 ((uint32_t) rand())) |
			1; /* Always non-zero. */

		      token_response._token_challenge_response =
			client->_token_challenge;
		      token_response._status =
			LWROC_CONTROL_TOKEN_STAT_CHALLENGE;
		    }
		  else
		    token_response._status =
		      LWROC_CONTROL_TOKEN_STAT_UNKNOWN;
		  break;

		case LWROC_CONTROL_TOKEN_STAT_RESPONSE:
		  if (client->_token_challenge &&
		      lwroc_ctrl_token_check(_lwroc_control_tokens,
					     token_request._token,
					     token_request._op,
					     client->_token_challenge,
					     token_request.
					     _token_challenge_response))
		    {
		      token_response._status = LWROC_CONTROL_TOKEN_STAT_OK;
		      /* And note that we are good to go for this op. */
		      client->_token_op_good = token_request._op;
		    }
		  else
		    token_response._status = LWROC_CONTROL_TOKEN_STAT_BAD;

		  /* Either way, the challenge has been used. */
		  client->_token_challenge = 0;
		  break;

		default:
		  goto token_protocol_failure;
		}
	      /*
	      printf ("status 0x%08x, token 0x%08x -> "
		      "status 0x%08x, token 0x%08x\n",
		      token_request._status,
		      token_request._token,
		      token_response._status,
		      token_response._token);
	      */
	      assert(sizeof (client->_ctrl._raw) >=
		     lwroc_control_token_serialized_size());

	      lwroc_control_token_serialize(client->_ctrl._raw,
					    &token_response);

	      LWROC_SET_CTRL_STATE(client,
				   LWROC_NET_CONTROL_STATE_WRITE_TOKEN);
	      client->_base._buf._ptr  = client->_ctrl._raw;
	      client->_base._buf._size = lwroc_control_token_serialized_size();
	      client->_base._buf._offset = 0;

	      return 1;

	    token_protocol_failure:

	      lwroc_net_control_status_response(client,
		LWROC_CONTROL_STATUS_PROTOCOL_ERROR,
		LWROC_CONTROL_STAT_ORIG_SERVER,
		"Protocol error (token).", 0,
		LWROC_NET_CONTROL_STATE_WRITE_ERROR);
	      return 1;
	    }

	  /* We have gotten the entire message.  Parse and deal with
	   * it.
	   */

	  end = lwroc_control_request_deserialize(&ctrl_request,
						  client->_ctrl._raw,
						  client->_base._buf._offset,
						  &desererr);

	  /* printf ("parse err: %s\n", desererr._msg); */

	  if (end == NULL)
	    {
	      lwroc_net_control_status_response(client,
		LWROC_CONTROL_STATUS_PROTOCOL_ERROR,
		LWROC_CONTROL_STAT_ORIG_SERVER,
		"Protocol error (malformed request).", 0,
		LWROC_NET_CONTROL_STATE_WRITE_ERROR);
	      return 1;
	    }

	  /* printf ("parsed: %zd\n", client->_base._buf._offset); */

	  client->_wait_ctrl = NULL;

	  /* So successfully parsed. */

	  /* printf ("ctrl_request_op: %x\n", ctrl_request._op); */

	  /* Token is not checked for the done message.
	   * And no response is generated.
	   */

	  if (ctrl_request._op == LWROC_CONTROL_OP_DONE)
	    {
	      LWROC_CLOG(&client->_base,
			 "Control client done.");
	      /* Client will close connection.  We do the same. */
	      return 0;
	    }

	  memset(&ctrl_response, 0, sizeof (ctrl_response));
	  ctrl_response_err_msg = NULL;

	  ctrl_response._tag    = ctrl_request._tag;
	  ctrl_response._status = LWROC_CONTROL_STATUS_UNKNOWN; /* default */
	  ctrl_response._origin = LWROC_CONTROL_STAT_ORIG_SERVER;
	  ctrl_response._op     = ctrl_request._op;
	  ctrl_response._value  = 0;

	  if (client->_token_op_good != ctrl_response._op)
	    {
	      /* Similar as LWROC_CONTROL_STATUS_TOO_BIG below. */

	      lwroc_net_control_status_response(client,
		client->_token_op_good ?
		  LWROC_CONTROL_STATUS_TOKEN_WRONG_OP :
		  LWROC_CONTROL_STATUS_TOKEN_NONE,
		LWROC_CONTROL_STAT_ORIG_SERVER,
		client->_token_op_good ?
		  "Negotiated token is for the wrong operation." :
		  "No token has been negotiated.",
		0,
		LWROC_NET_CONTROL_STATE_WRITE);

	      return 1;
	    }

	  switch (ctrl_request._op)
	    {
	    case LWROC_CONTROL_OP_DEBUG_MASK:
	      if (ctrl_request._value == 0)
		LWROC_CDEBUG_FMT(&client->_base,
				 "Setting debug mask (our side) to 0x%08x.",
				 ctrl_request._value);
	      _lwroc_debug_mask_enabled = ctrl_request._value;
	      if (ctrl_request._value != 0)
		LWROC_CDEBUG_FMT(&client->_base,
				 "Debug mask (our side) set to 0x%08x.",
				 ctrl_request._value);
	      ctrl_response._value = _lwroc_debug_mask_enabled;
	      ctrl_response._status = LWROC_CONTROL_STATUS_SUCCESS;
	      break;
	    case LWROC_CONTROL_OP_UNLIMIT_LOG:
	      if (ctrl_request._value < 3600)
		{
		  _lwroc_unlimited_log_quota_until =
		    si->now.tv_sec + (time_t) ctrl_request._value;
		  ctrl_response._value = ctrl_request._value;
		  ctrl_response._status = LWROC_CONTROL_STATUS_SUCCESS;
		}
	      else
		{
		  ctrl_response._value = 0;
		  ctrl_response._status = LWROC_CONTROL_STATUS_INVALID;
		}
	      break;
	    default:
	    {
	      lwroc_control_buffer *ctrl;

	      ctrl = lwroc_get_control_buffer(ctrl_request._op);

	      if (ctrl)
		{
		  /*
		  printf ("ctrl->_state: %d (notify %p)\n",
			  ctrl->_state,
			  ctrl->_thread_notify_ready);
		  */
		  if (ctrl->_state != LWROC_CONTROL_STATE_WAIT_MESSAGE ||
		      ctrl->_thread_notify_ready == NULL)
		    {
		      /* We have no idea when it will be done...
		       * So be sort of final...
		       */
		      /* printf ("control %08x busy\n", ctrl_request._op); */
		      ctrl_response._status = LWROC_CONTROL_STATUS_TMP_BUSY;
		      ctrl_response._origin = LWROC_CONTROL_STAT_ORIG_SERVER;
		      ctrl_response_err_msg = "Control handler is busy.";
		      break;
		    }
		  /* In case we do the reading of the free-format data
		   * directly into the buffer, we must lock it...
		   */
		  ctrl->_state = LWROC_CONTROL_STATE_FILLING;
		  /* Copy the request. */
		  ctrl->_request = ctrl_request;
		  /* Clear the response. */
		  memset (&ctrl->_response, 0, sizeof (ctrl->_response));
		  ctrl->_response_err_msg = NULL;

		  /* If we are to receive or send response data, we
		   * must check that the size is within bounds.
		   */

		  if (ctrl_request._request_data_size ||
		      ctrl_request._response_data_maxsize)
		    {
		      int accept_size = 1;

		      if (ctrl_request._request_data_size >
			  LWROC_CONTROL_CONTROL_LIMIT_DATA_SIZE ||
			  ctrl_request._response_data_maxsize >
			  LWROC_CONTROL_CONTROL_LIMIT_DATA_SIZE ||
			  ctrl_request._request_data_size +
			  ctrl_request._response_data_maxsize >
			  ctrl->_buffer_size)
			accept_size = 0;

		      /* We are to tell the client if it can proceed or
		       * not.
		       */

		      if (accept_size)
			{
			  uint32_t aligned_req_size =
			    LWROC_ALIGN_SZ(ctrl_request._request_data_size,
					   (uint32_t) sizeof (uint64_t));
			  uint32_t aligned_resp_size =
			    LWROC_ALIGN_SZ(ctrl_request._response_data_maxsize,
					   (uint32_t) sizeof (uint64_t));

			  ctrl->_canary = 0x55aa55aa ^
			    ((ctrl_request._request_data_size << 4) +
			     (ctrl_request._response_data_maxsize << 8) +
			     (ctrl_request._tag << 12) +
			     (ctrl_request._op << 16));
			  ctrl->_request_data = ctrl->_data_buffer;
			  ctrl->_canaries[0] =
			    (uint32_t *) ((char *) ctrl->_request_data +
					  aligned_req_size);
			  ctrl->_canaries[0][0] =  ctrl->_canary;
			  ctrl->_canaries[0][1] = ~ctrl->_canary;
			  ctrl->_response_data =
			    ctrl->_canaries[0] + 2;
			  ctrl->_canaries[1] =
			    (uint32_t *) ((char *) ctrl->_response_data +
					  aligned_resp_size);
			  ctrl->_canaries[1][0] = ~ctrl->_canary;
			  ctrl->_canaries[1][1] =  ctrl->_canary;

			  client->_wait_ctrl = ctrl;

			  lwroc_net_control_status_response(client,
			    LWROC_CONTROL_STATUS_DATA_SIZE, 0, NULL,
			    (uint32_t) ctrl->_buffer_size,
			    LWROC_NET_CONTROL_STATE_WRITE_ACCEPT);
			}
		      else
			{
			  lwroc_net_control_status_response(client,
			    LWROC_CONTROL_STATUS_TOO_BIG,
			    LWROC_CONTROL_STAT_ORIG_SERVER,
			    "Requested control data size too large.",
			    (uint32_t) ctrl->_buffer_size,
			    LWROC_NET_CONTROL_STATE_WRITE);
			  /* We will not be getting any more data for this
			   * request, which hereby is dead anyhow.
			   * Ready for the next!
			   */
			  ctrl->_state = LWROC_CONTROL_STATE_WAIT_MESSAGE;
			}
		    }
		  else
		    {
		      ctrl->_request_data = ctrl->_response_data = NULL;
		      ctrl->_canaries[0]  = ctrl->_canaries[1] = NULL;
		      client->_wait_ctrl = ctrl;
		      lwroc_net_control_notify_request(client);
		    }

		  return 1;
		}
	      else
		{
		  ctrl_response._status = LWROC_CONTROL_STATUS_NO_HANDLER;
		  ctrl_response._origin = LWROC_CONTROL_STAT_ORIG_SERVER;
		  switch (ctrl_request._op)
		    {
		    case LWROC_CONTROL_OP_FILE:
		      ctrl_response_err_msg = "No file writer.";
		      break;
		    case LWROC_CONTROL_OP_ACQ_READOUT:
		      ctrl_response_err_msg = "No acquisition (triva/mi).";
		      break;
		    case LWROC_CONTROL_OP_MERGE:
		      ctrl_response_err_msg = "No merger.";
		      break;
		    case LWROC_CONTROL_OP_USER:
		      ctrl_response_err_msg = "No user control handler.";
		      break;
		    default:
		      ctrl_response_err_msg = "No suitable control handler.";
		      break;
		    }
		}

	      /* Unknown control, status already set. */
	      break;
	    }
	    }

	  lwroc_net_control_response(client, &ctrl_response,
				     ctrl_response_err_msg,
				     LWROC_NET_CONTROL_STATE_WRITE);
	}
      return 1;
    }

  if (client->_ctrl._state == LWROC_NET_CONTROL_STATE_READ_DATA)
    {
      if (!LWROC_READ_FD_ISSET(client->_base._fd, si))
	return 1;

      if (!lwroc_net_client_base_read(&client->_base, "control data",
				      &_lwroc_mon_net._recv_ctrl))
	return 0;

      if (client->_base._buf._offset >= client->_base._buf._size)
	{
	  lwroc_net_control_notify_request(client);
	  return 1;
	}
      return 1;
    }

  if (client->_ctrl._state == LWROC_NET_CONTROL_STATE_WRITE_INIT ||
      client->_ctrl._state == LWROC_NET_CONTROL_STATE_WRITE ||
      client->_ctrl._state == LWROC_NET_CONTROL_STATE_WRITE_TOKEN ||
      client->_ctrl._state == LWROC_NET_CONTROL_STATE_WRITE_ACCEPT ||
      client->_ctrl._state == LWROC_NET_CONTROL_STATE_WRITE_DATA ||
      client->_ctrl._state == LWROC_NET_CONTROL_STATE_WRITE_ERROR)
    {
      if (!LWROC_WRITE_FD_ISSET(client->_base._fd, si))
	return 1;
      /*
      printf ("write [%d]: %zd, %zd  %p  %08x %08x %08x %08x\n",
	      client->_ctrl._state,
	      client->_base._buf._offset,
	      client->_base._buf._size,
	      client->_base._buf._ptr,
	      ntohl(((uint32_t *) client->_base._buf._ptr)[0]),
	      ntohl(((uint32_t *) client->_base._buf._ptr)[1]),
	      ntohl(((uint32_t *) client->_base._buf._ptr)[2]),
	      ntohl(((uint32_t *) client->_base._buf._ptr)[3]));
      */
      if (!lwroc_net_client_base_write(&client->_base, "control",
				       &_lwroc_mon_net._sent_ctrl))
	return 0;

      if (client->_base._buf._offset < client->_base._buf._size)
	return 1;

      switch (client->_ctrl._state)
	{
	case LWROC_NET_CONTROL_STATE_WRITE_ERROR:
	  /* If it was an error response, we kill the connection,
	   * since it may have been a response to an incomplete
	   * message.  Note: this does not include cases when the
	   * control itself is denied!  I.e. only pure network
	   * protocol errors.
	   */
	  return 0;
	case LWROC_NET_CONTROL_STATE_WRITE:
	  /* Is there data to be written? */
	  if (client->_wait_ctrl &&
	      client->_wait_ctrl->_response._response_data_size)
	    {
	      client->_base._buf._ptr  =
		client->_wait_ctrl->_response_data;
	      client->_base._buf._size =
		LWROC_ALIGN_SZ(client->_wait_ctrl->_response.
			       /**/_response_data_size,
			       sizeof (uint64_t));
	      /* Make sure extra alignment bytes are initialised before write.
	       */
	      memset(client->_base._buf._ptr +
		     client->_wait_ctrl->_response._response_data_size,
		     0,
		     client->_base._buf._size -
		     client->_wait_ctrl->_response._response_data_size);
	      client->_base._buf._offset = 0;
	      LWROC_SET_CTRL_STATE(client, LWROC_NET_CONTROL_STATE_WRITE_DATA);
	      break;
	    }
	  FALL_THROUGH; /* we are done with this response. */
	case LWROC_NET_CONTROL_STATE_WRITE_INIT:
	case LWROC_NET_CONTROL_STATE_WRITE_DATA:
	case LWROC_NET_CONTROL_STATE_WRITE_TOKEN:
	  /* Get ready to read the next request. */

	  client->_base._buf._ptr  = client->_ctrl._raw;
	  client->_base._buf._size = sizeof (uint32_t);
	  client->_base._buf._offset = 0;
	  LWROC_SET_CTRL_STATE(client, LWROC_NET_CONTROL_STATE_READ);
	  break;
	case LWROC_NET_CONTROL_STATE_WRITE_ACCEPT:
	  if (client->_wait_ctrl->_request._request_data_size)
	    {
	      /* Get ready to read the data. */
	      client->_base._buf._ptr  =
		client->_wait_ctrl->_request_data;
	      client->_base._buf._size =
		LWROC_ALIGN_SZ(client->_wait_ctrl->_request._request_data_size,
			       sizeof (uint64_t));
	      client->_base._buf._offset = 0;
	      LWROC_SET_CTRL_STATE(client, LWROC_NET_CONTROL_STATE_READ_DATA);
	      break;
	    }
	  /* There was no data to read (just will be to write). */
	  lwroc_net_control_notify_request(client);
	}
      return 1;
    }
  assert (0); /* Should never reach this point.  Hmm, reorganise code... */
  return 1;
}

/********************************************************************/

lwroc_select_item_info lwroc_net_control_select_item_info =
{
  lwroc_net_control_setup_select,
  lwroc_net_control_after_select,
  lwroc_net_client_base_after_fail,
  lwroc_net_client_base_delete,
  0,
};

/********************************************************************/

char *lwroc_net_control_fmt_msg_context(char *buf, size_t size,
					const void *ptr)
{
  const lwroc_net_control *client = (const lwroc_net_control *)
    (ptr - offsetof(lwroc_net_control, _base._msg_context));
  char dotted[INET6_ADDRSTRLEN];

  lwroc_inet_ntop(&client->_base._addr, dotted, sizeof (dotted));

  return lwroc_message_fmt_msg_context(buf, size,
				       "ctrlclnt: %s", dotted);
}

/********************************************************************/

void lwroc_net_become_control(lwroc_net_control *client)
{
  lwroc_net_control_status_response(client,
				    LWROC_CONTROL_STATUS_READY, 0, NULL, 0,
				    LWROC_NET_CONTROL_STATE_WRITE_INIT);
  /* printf("become ctrl\n"); */

  client->_base._msg_context = lwroc_net_control_fmt_msg_context;

  client->_token_op_good = 0;

  LWROC_CLOG(&client->_base,
	     "Control client connected.");
}

/********************************************************************/

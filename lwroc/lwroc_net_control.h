/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_NET_CONTROL_H__
#define __LWROC_NET_CONTROL_H__

#include "lwroc_net_client_base.h"
#include "lwroc_control.h"

/********************************************************************/

#define LWROC_NET_CONTROL_STATE_WRITE_INIT         1
#define LWROC_NET_CONTROL_STATE_READ               2
#define LWROC_NET_CONTROL_STATE_WRITE_TOKEN        3
#define LWROC_NET_CONTROL_STATE_WRITE_ACCEPT       4
#define LWROC_NET_CONTROL_STATE_READ_DATA          5
#define LWROC_NET_CONTROL_STATE_WAIT_HANDLE        6
#define LWROC_NET_CONTROL_STATE_WRITE              7
#define LWROC_NET_CONTROL_STATE_WRITE_DATA         8
#define LWROC_NET_CONTROL_STATE_WRITE_ERROR        9

typedef struct lwroc_net_control_t
{
  lwroc_net_client_base _base;

  uint32_t _token_op_good;
  uint32_t _token_challenge;

  /* Hold simple part of control request and response.
   * A bit exaggerated at the moment? - requires more thinking...
   */
  struct
  {
    int               _state;
    char              _raw[64+256];
  } _ctrl;

  /* Waiting for this item to complete handling. */
  lwroc_control_buffer *_wait_ctrl;

} lwroc_net_control;

/********************************************************************/

extern lwroc_select_item_info lwroc_net_control_select_item_info;

/********************************************************************/

void lwroc_net_become_control(lwroc_net_control *client);

/********************************************************************/

void lwroc_net_control_setup(void);

/********************************************************************/

#endif/*__LWROC_NET_CONTROL_H__*/

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_net_in_rev_link.h"
#include "lwroc_message.h"
#include "lwroc_net_outgoing.h"
#include "lwroc_hostname_util.h"

#include "gen/lwroc_reverse_link_ack_serializer.h"

#include <errno.h>
#include <assert.h>

#include "../dtc_arch/acc_def/myprizd.h"

lwroc_msg_wire _lwroc_in_rev_link_msg = { NULL, 0 };

/********************************************************************/

void lwroc_net_in_rev_link_setup_select(lwroc_select_item *item,
					lwroc_select_info *si)
{
  lwroc_net_in_rev_link *rev_link =
    PD_LL_ITEM(item, lwroc_net_in_rev_link, _base._select_item);

  /* We always want to receive. */
  /*
  printf ("%p fd=%d, in rev link, setup...\n",
	  &rev_link->_base._buf,
	  rev_link->_base._fd);
  */
  LWROC_READ_FD_SET(rev_link->_base._fd, si);
}

int lwroc_net_in_rev_link_after_select(lwroc_select_item *item,
				       lwroc_select_info *si)
{
  lwroc_net_in_rev_link *rev_link =
    PD_LL_ITEM(item, lwroc_net_in_rev_link, _base._select_item);

  if (!LWROC_READ_FD_ISSET(rev_link->_base._fd, si))
    return 1;

  /* printf ("in rev link, to read...\n"); */

  if (!lwroc_net_client_base_read(&rev_link->_base, "rev_link",
				  &_lwroc_mon_net._recv_misc))
    return 0;

  /*
  printf ("in rev link, read %" MYPRIzd "/%" MYPRIzd "\n",
	  rev_link->_base._buf._offset,
	  rev_link->_base._buf._size);
  */

  if (rev_link->_base._buf._offset >= rev_link->_base._buf._size)
    {
      lwroc_reverse_link_ack link_ack;
      lwroc_deserialize_error desererr;
      const char *end;

      /* We have received a full message.  Decode it, and pass it on
       * to anyone that may be waiting for it.  Then we just wait to
       * receive the next messages...
       */

      end = lwroc_reverse_link_ack_deserialize(&link_ack,
					       (const char *) rev_link->_raw,
					       rev_link->_base._buf._offset,
					       &desererr);

      if (end == NULL)
	{
	  /* Bad message.  We will simply kill the connection. */
	  LWROC_CWARNING(&rev_link->_base, "Bad link ack message received.");
	  return 0;
	}

      /* Successfully decoded. */

      lwroc_reverse_link_ack_received(&link_ack,
				      &rev_link->_base._addr,
				      rev_link->_orig_port);

      /* And receive another one. */

      rev_link->_base._buf._offset = 0;
    }

  return 1;
}

/********************************************************************/

lwroc_select_item_info lwroc_net_in_rev_link_select_item_info =
{
  lwroc_net_in_rev_link_setup_select,
  lwroc_net_in_rev_link_after_select,
  lwroc_net_client_base_after_fail,
  lwroc_net_client_base_delete,
  0,
};

/********************************************************************/

char *lwroc_in_rev_link_fmt_msg_context(char *buf, size_t size,
					const void *ptr)
{
  const lwroc_net_in_rev_link *rev_link = (const lwroc_net_in_rev_link *)
    (ptr - offsetof(lwroc_net_in_rev_link, _base._msg_context));
  char dotted[INET6_ADDRSTRLEN];

  lwroc_inet_ntop(&rev_link->_base._addr, dotted, sizeof (dotted));

  return lwroc_message_fmt_msg_context(buf, size,
				       "inrevlink: %s", dotted);
}

/********************************************************************/

void lwroc_net_become_in_rev_link(lwroc_net_in_rev_link *rev_link)
{
  assert(sizeof (rev_link->_raw) ==
	 lwroc_reverse_link_ack_serialized_size());

  /* printf ("%p: become in rev link\n", rev_link); */

  /*
  printf ("%p (buf %p): become, cbc-empty: %d fd=%d\n",
	  rev_link, &rev_link->_base._buf,
	  PD_LL_IS_EMPTY(&rev_link->_base._clients),
	  rev_link->_base._fd);
  */

  rev_link->_base._buf._size = lwroc_reverse_link_ack_serialized_size();
  rev_link->_base._buf._ptr  = (char *) rev_link->_raw;
  rev_link->_base._buf._offset = 0;

  rev_link->_base._msg_context = lwroc_in_rev_link_fmt_msg_context;
}

/********************************************************************/

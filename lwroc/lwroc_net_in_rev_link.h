/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_NET_IN_REV_LINK_H__
#define __LWROC_NET_IN_REV_LINK_H__

#include "lwroc_net_client_base.h"

/********************************************************************/

typedef struct lwroc_net_in_rev_link_t
{
  lwroc_net_client_base _base;

  /* What port did the request claim to come from? */
  uint16_t _orig_port;

  uint32_t _raw[7];

} lwroc_net_in_rev_link;

/********************************************************************/

extern lwroc_select_item_info lwroc_net_in_rev_link_select_item_info;

/********************************************************************/

void lwroc_net_become_in_rev_link(lwroc_net_in_rev_link *in_rev_link);

/********************************************************************/

#endif/*__LWROC_NET_IN_REV_LINK_H__*/

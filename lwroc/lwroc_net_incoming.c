/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_net_incoming.h"
#include "lwroc_message.h"
#include "lwroc_message_internal.h"
#include "lwroc_main_iface.h"
#include "lwroc_hostname_util.h"
#include "lwroc_udp_awaken_hints.h"

#include "lwroc_net_mon.h"
#include "lwroc_net_control.h"
#include "lwroc_net_message.h"
#include "lwroc_net_trigbus.h"
#include "lwroc_net_trans.h"
#include "lwroc_net_in_rev_link.h"
#include "lwroc_net_reverse_link.h"
#include "lwroc_net_badrequest.h"
#include "lwroc_timeouts.h"

#include "gen/lwroc_request_msg_serializer.h"
#include "gen/lwroc_badrequest_serializer.h"
#include "gen/lwroc_reverse_link_ack_serializer.h"

#include <errno.h>
#include <assert.h>
#include <arpa/inet.h>

#include "../dtc_arch/acc_def/myprizd.h"

/********************************************************************/

PD_LL_SENTINEL(_ext_net_badrequest);
PD_LL_SENTINEL(_ext_net_monitor);
PD_LL_SENTINEL(_ext_net_control);
PD_LL_SENTINEL(_ext_net_in_rev_link);

PD_LL_SENTINEL(_ext_net_messages); /* only one */

extern lwroc_net_trans_unit *_lwroc_drasi_server;
extern PD_LL_SENTINEL_ITEM(_lwroc_net_trigbus_slaves);
extern PD_LL_SENTINEL_ITEM(_lwroc_net_trigbus_ebs);

/********************************************************************/

void lwroc_net_io_bad_request(lwroc_net_incoming *client,
			      uint32_t failure, uint32_t hint,
			      uint32_t ipv4_addr)
{
  lwroc_badrequest  badreq;

  badreq._failure = failure;
  badreq._hint = hint;
  badreq._seen_ipv4_addr = ipv4_addr;

  assert(sizeof (client->_raw) >= lwroc_badrequest_serialized_size());
  lwroc_badrequest_serialize((char *) client->_raw, &badreq);
  client->_base._buf._size = lwroc_badrequest_serialized_size();
  client->_base._buf._ptr  = (char *) client->_raw;
  client->_base._buf._offset = 0;
  /*
  printf ("%p: add badrq: %p\n", client, client->_base._buf._ptr);
  */
  /* Add it to the list of clients */

  client->_base._select_item.item_info =
    &lwroc_net_badrequest_select_item_info;

  PD_LL_ADD_BEFORE(&_ext_net_badrequest, &client->_base._clients);

  lwroc_net_badrequest_report(&((&client->_base)->_msg_context),
			      "We responded",
			      &badreq,
			      "-");

  _lwroc_mon_net._ncon_refused++;
}

#define STATE_MACHINE_DEBUG 0

int lwroc_net_io_parse_request(lwroc_net_incoming *client,
			       lwroc_select_info *si)
{
  lwroc_deserialize_error desererr;

  const char *end;

#if STATE_MACHINE_DEBUG
  printf ("parse request...\n");
#endif

  end = lwroc_request_msg_deserialize(&client->_req_msg, (char *) client->_raw,
				      client->_base._buf._offset, &desererr);

  client->_base._buf._size = 0;
  client->_base._buf._ptr = NULL; /* for safety */
  client->_base._buf._offset = 0;

  if (end == NULL)
    {
      PD_LL_REMOVE(&client->_base._clients);
      lwroc_net_io_bad_request(client, LWROC_BADREQUEST_BAD_MAGIC, 0, 0);
      return 0;
    }

#if STATE_MACHINE_DEBUG
  printf ("request: %x\n", client->_req_msg._request);
#endif

  /* We should now find out if we at all like connections
   * from the given machine.  (This could actually be vetoed much earlier
   * for completely unknown machines.)
   *
   * If we like that type of connection from the given machine.
   *
   * If we need to verify that the connection is from a process that has
   * the stated port open.  If so, we need to send a token that it
   * shall reply with.
   */





  /* Possibly the process that made the connection to us has just
   * started.  We should therefore retry all outgoing connections that
   * would like to connect to this address:port.  But with some
   * hysteresis if they just did it, in case the process is somehow
   * making rapid connection attempts.
   */

  /* We do not waste time on checking for incoming control or monitor
   * connections (that supply a dummy port number anyhow.  (Messages
   * will in the future be handled by the main process itself, so will
   * require reverse links.)
   */

  if (client->_req_msg._request != LWROC_REQUEST_MONITOR &&
      client->_req_msg._request != LWROC_REQUEST_CONTROL &&
      client->_req_msg._orig_port != 0)
    lwroc_awaken_retry_outgoing(&client->_base._addr,
				client->_req_msg._orig_port,
				&si->now);

  /* For the time being, we accept everyone.  But request link ack
   * for data transmissions.
   */

  if (client->_req_msg._request == LWROC_REQUEST_DATA_TRANS ||
      client->_req_msg._request == LWROC_REQUEST_TRIVAMI_BUS ||
      client->_req_msg._request == LWROC_REQUEST_TRIVAMI_EB)
    {
      client->_link_ack_rid = 1;

      if (!lwroc_find_reverse_link(client, si))
	{
	  uint32_t ipv4_addr = 0;

	  ipv4_addr = lwroc_get_ipv4_addr(&client->_base._addr);

	  PD_LL_REMOVE(&client->_base._clients);
	  lwroc_net_io_bad_request(client,
				   LWROC_BADREQUEST_BAD_REVERSE_LINK,
				   LWROC_BADREVLNK_HINT_NOT_FOUND,
				   ipv4_addr);
	  return 0;
	}

      /* We want to read a link ack token message. */

      client->_base._buf._size = lwroc_reverse_link_ack_serialized_size();
      assert(sizeof (client->_raw) >= client->_base._buf._size);
      client->_base._buf._ptr = ((char*) client->_raw);
      client->_base._buf._offset = 0;

      /* printf ("want lack to come: %p\n", &client->_base._buf); */

      return 0;
    }

  /* Continue with multiplexing.  We will not be here any longer. */
  PD_LL_REMOVE(&client->_base._clients);
  return 1;
}

int lwroc_net_io_check_link_ack(lwroc_net_incoming *client)
{
  lwroc_reverse_link_ack link_ack;
  lwroc_deserialize_error desererr;
  const char *end;
  uint32_t fail_hint = 0;

  /* printf ("check lack: %p\n", &client->_base._buf); */

  end = lwroc_reverse_link_ack_deserialize(&link_ack,
					   (const char *) client->_raw,
					   client->_base._buf._offset,
					   &desererr);

  if (end == NULL)
    {
      fail_hint = 1;
      lwroc_net_io_bad_request(client, LWROC_BADREQUEST_BAD_REVERSE_LINK,
			       fail_hint, 0);
      return 0;
    }

  if (!PD_LL_IS_EMPTY(&client->_send_link_ack))
    {
      /* We are still in the queue waiting to send the link ack...
       * Then it cannot be good!
       */
      fail_hint = 2;
    }
  else if (client->_req_msg._link_ack_id1 != link_ack._link_ack_id1)
    fail_hint = 3;
  else if (client->_req_msg._link_ack_id2 != link_ack._link_ack_id2)
    fail_hint = 4;
  else if (client->_link_ack_rid          != link_ack._link_ack_rid)
    fail_hint = 5;
  else if (client->_link_ack_token        != link_ack._link_ack_token)
    fail_hint = 6;

  if (fail_hint)
    {
      lwroc_net_io_bad_request(client, LWROC_BADREQUEST_BAD_REVERSE_LINK,
			       fail_hint, 0);
      return 0;
    }

  /* printf ("Got good link ack!\n"); */

  /* Good! */

  return 1;
}

void lwroc_net_io_multiplex_incoming(lwroc_net_incoming *client)
{
  /*
  printf ("multplex, cbc-empty: %d\n",
	  PD_LL_IS_EMPTY(&client->_base._clients));
  */

  if (_lwroc_shutdown)
    {
      lwroc_net_io_bad_request(client, LWROC_BADREQUEST_SHUTDOWN, 0, 0);
      return;
    }

  if (client->_req_msg._request == LWROC_REQUEST_MESSAGES)
    {
      lwroc_net_message *msg_client = (lwroc_net_message *) client;

      /* Turn this into the message sender.
       */

      if (!PD_LL_IS_EMPTY(&_ext_net_messages))
	{
	  lwroc_net_io_bad_request(client,
				   LWROC_BADREQUEST_MULTIPLE_RECEIVER, 0, 0);
	  return;
	}

      lwroc_udp_awaken_hints_add(client);

      lwroc_net_become_message(msg_client);

      client->_base._select_item.item_info =
	&lwroc_net_message_select_item_info;

      PD_LL_ADD_BEFORE(&_ext_net_messages, &client->_base._clients);

      return;
    }

  /* We do not accept anything but log message connections until a log
   * destination has been set up.
   *
   * TODO: once we handle log proxying within the program, with
   * verification ofg the destination systems, we also need to allow
   * reverse connections.
   */
  if (!_lwroc_message_client_seen)
    {
      lwroc_net_io_bad_request(client, LWROC_BADREQUEST_WAIT_LOG_CLIENT, 0, 0);
      return;
    }

  if (client->_req_msg._request == LWROC_REQUEST_MONITOR)
    {
      lwroc_net_mon *mon_client = (lwroc_net_mon *) client;

      lwroc_udp_awaken_hints_add(client);

      /* Turn this into a monitor sender.
       */
      lwroc_net_become_mon(mon_client);

      client->_base._select_item.item_info =
	&lwroc_net_monitor_select_item_info;

      PD_LL_ADD_BEFORE(&_ext_net_monitor, &client->_base._clients);

      return;
    }

  if (client->_req_msg._request == LWROC_REQUEST_CONTROL)
    {
      lwroc_net_control *ctrl_client = (lwroc_net_control *) client;

      /* Turn this into a control connection.
       */
      lwroc_net_become_control(ctrl_client);

      client->_base._select_item.item_info =
	&lwroc_net_control_select_item_info;

      PD_LL_ADD_BEFORE(&_ext_net_control, &client->_base._clients);

      return;
    }

  if (client->_req_msg._request == LWROC_REQUEST_TRIVAMI_BUS ||
      client->_req_msg._request == LWROC_REQUEST_TRIVAMI_EB)
    {
      /* Turn this into a slave connection.
       */
      lwroc_net_trigbus *slave_client =
	lwroc_net_become_slave(client);

      if (!slave_client)
	{
	  lwroc_net_io_bad_request(client,
				   LWROC_BADREQUEST_MULTIPLE_RECEIVER, 0, 0);
	  return;
	}

      /* We may not touch the @client pointer any further!
       * lwroc_net_trigbus / lwroc_net_become_slave() will have done
       * surgery on the list and replaced us.
       */

      if (slave_client->_request == LWROC_REQUEST_TRIVAMI_BUS)
	PD_LL_ADD_BEFORE(&_lwroc_net_trigbus_slaves,
			 &slave_client->_out._base._clients);
      else /* LWROC_REQUEST_TRIVAMI_EB */
	PD_LL_ADD_BEFORE(&_lwroc_net_trigbus_ebs,
			 &slave_client->_out._base._clients);

      return;
    }

  if (client->_req_msg._request == LWROC_REQUEST_REVERSE_LINK)
    {
      lwroc_net_in_rev_link *link_client = (lwroc_net_in_rev_link *) client;

      link_client->_orig_port = client->_req_msg._orig_port;

      /* TODO: are we checking anywhere that we actually need a reverse
       * link connection from this host:port, i.e. that we want to make
       * a connection there which requires confirmation.
       */

      /* Turn this into a reverse link connection.
       */
      lwroc_net_become_in_rev_link(link_client);

      client->_base._select_item.item_info =
	&lwroc_net_in_rev_link_select_item_info;

      PD_LL_ADD_BEFORE(&_ext_net_in_rev_link, &client->_base._clients);

      return;
    }

  if (client->_req_msg._request == LWROC_REQUEST_DATA_TRANS)
    {
      lwroc_net_trans_client *data_client = (lwroc_net_trans_client *) client;

      /* Turn this into a data sender.
       */

      if (!_lwroc_drasi_server)
	{
	  lwroc_net_io_bad_request(client,
				   LWROC_BADREQUEST_NONCFG_REQUEST, 0, 0);
	  return;
	}

      /* To allow sanity checking of allocation. */
      data_client->_bufchunks = NULL;

      if (!lwroc_net_become_trans(_lwroc_drasi_server, data_client, 0, 0,
				  LWROC_GDF_BUF_VARBUFSIZE_TRIM))
	{
	  lwroc_net_io_bad_request(client,
				   LWROC_BADREQUEST_MULTIPLE_RECEIVER, 0, 0);
	  return;
	}

      client->_base._select_item.item_info =
	&lwroc_net_data_trans_select_item_info;

      _lwroc_mon_net._ncon_total++;

      return;
    }

  lwroc_net_io_bad_request(client, LWROC_BADREQUEST_BAD_REQUEST, 0, 0);
}

/********************************************************************/

int lwroc_net_incoming_after_fail(lwroc_select_item *item,
				  lwroc_select_info *si)
{
  lwroc_net_incoming *incoming =
    PD_LL_ITEM(item, lwroc_net_incoming, _base._select_item);

  /* In case we were queued up to have a link ack sent. */

  PD_LL_REMOVE(&incoming->_send_link_ack);

  return lwroc_net_client_base_after_fail(item, si);
}

/********************************************************************/

void lwroc_net_incoming_setup_select(lwroc_select_item *item,
				     lwroc_select_info *si)
{
  lwroc_net_incoming *client =
    PD_LL_ITEM(item, lwroc_net_incoming, _base._select_item);

  /*
  printf ("before_select %p (%d, %" MYPRIzd "/%" MYPRIzd ")\n",
	  &client->_base._buf,
	  client->_base._fd,
	  client->_base._buf._offset,
	  client->_base._buf._size);
  */

  /* By definition, an incoming client wants to read the request. */

  LWROC_READ_FD_SET(client->_base._fd, si);

  /* We have a time-out since we have not exchanged any information at
   * all with the other end.  If it has just randomly connected to our
   * port, it may be that it expects us to write something, which we
   * will never do...
   */

  /* printf ("to: %d %d\n", (int)si->now.tv_sec,(int)  client->_next_time.tv_sec); */

  lwroc_select_info_time_setup(si, &client->_next_time);
}

int lwroc_net_incoming_after_select(lwroc_select_item *item,
				    lwroc_select_info *si)
{
  lwroc_net_incoming *client =
    PD_LL_ITEM(item, lwroc_net_incoming, _base._select_item);

  /*
  printf ("after_select %p (%d, %" MYPRIzd "/%" MYPRIzd ")\n",
	  &client->_base._buf,
	  client->_base._fd,
	  client->_base._buf._offset,
	  client->_base._buf._size);
  */

  if (timercmp(&si->now, &client->_next_time, >))
    {
      /* Actually, better would be to send a badreq response?
       * Then the client gets to know why we killed it...
       * Naturally, with a timeout also in the sending.
       * (Although the thing to send is so small that it will just
       * go through.)
       */
      LWROC_CINFO(&client->_base,
		  "Timeout waiting for actual incoming request / link ack "
		  "on incoming connection.");
      return 0;
    }

  if (!LWROC_READ_FD_ISSET(client->_base._fd, si))
    {
      /* printf ("incoming %d no read\n", client->_base._fd); */
      return 1;
    }

  if (!lwroc_net_client_base_read(&client->_base, "incoming",
				  &_lwroc_mon_net._recv_misc))
    return 0;

  if (client->_base._buf._offset == client->_base._buf._size)
    {
      if (client->_link_ack_rid == 0)
	{
	  /* Parse the request. */

	  if (!lwroc_net_io_parse_request(client, si))
	    {
	      /* We wanted a link ack message, so wait for that...
	       * Or we did not like and prepared a badreq response.
	       * Either way, keep connection.
	       */
	      return 1;
	    }
	}
      else
	{
	  /* We will not be here any longer. */

	  PD_LL_REMOVE(&client->_base._clients);

	  /* Check the link ack message. */

	  if (!lwroc_net_io_check_link_ack(client))
	    {
	      /* Connection is to be kept, we have prepared a badrequest
	       * response.
	       */
	      return 1;
	    }
	}

      /* printf ("multiplex ... %p\n", &client->_base._buf); */

      /* Figure out who is responsible for this client now.
       */
      lwroc_net_io_multiplex_incoming(client);
      /* We may not touch the client pointer any further!
       * lwroc_net_trigbus / lwroc_net_become_slave() will have done
       * surgery on the list and replaced us.
       */
    }
  return 1;
}

/********************************************************************/

char *lwroc_net_incoming_fmt_msg_context(char *buf, size_t size,
					 const void *ptr)
{
  const lwroc_net_incoming *client = (const lwroc_net_incoming *)
    (ptr - offsetof(lwroc_net_incoming, _base._msg_context));
  char dotted[INET6_ADDRSTRLEN];

  lwroc_inet_ntop(&client->_base._addr, dotted, sizeof (dotted));

  return lwroc_message_fmt_msg_context(buf, size,
				       "in: %s", dotted);
}

/********************************************************************/

lwroc_select_item_info lwroc_net_incoming_select_item_info =
{
  lwroc_net_incoming_setup_select,
  lwroc_net_incoming_after_select,
  lwroc_net_incoming_after_fail,
  lwroc_net_client_base_delete,
  0,
};

/********************************************************************/

void lwroc_net_become_incoming(lwroc_net_incoming *client,
			       lwroc_select_info *si)
{
  client->_base._buf._size = lwroc_request_msg_serialized_size();
  assert(sizeof (client->_raw) >= client->_base._buf._size);
  client->_base._buf._ptr = ((char*) client->_raw);
  client->_base._buf._offset = 0;

  client->_base._msg_context = lwroc_net_incoming_fmt_msg_context;

  PD_LL_INIT(&client->_send_link_ack);
  client->_link_ack_rid = 0;

  client->_next_time = si->now;
  client->_next_time.tv_sec += LWROC_INCOMING_CLOSE_TIMEOUT;
}

/********************************************************************/

void lwroc_net_badrequest_setup_select(lwroc_select_item *item,
				       lwroc_select_info *si)
{
  lwroc_net_incoming *client =
    PD_LL_ITEM(item, lwroc_net_incoming, _base._select_item);

  /* By definition, we want to write the badrequest response. */

  LWROC_WRITE_FD_SET(client->_base._fd, si);
}

int lwroc_net_badrequest_after_select(lwroc_select_item *item,
				      lwroc_select_info *si)
{
  lwroc_net_incoming *client =
    PD_LL_ITEM(item, lwroc_net_incoming, _base._select_item);

  if (!LWROC_WRITE_FD_ISSET(client->_base._fd, si))
    return 1;
  /*
  printf ("%p: badrq write: %p (%" MYPRIzd " %" MYPRIzd ")\n",
	  client,
	  client->_base._buf._ptr,
	  client->_base._buf._size,
	  client->_base._buf._offset);
  */
  if (!lwroc_net_client_base_write(&client->_base, "badrequest",
				   &_lwroc_mon_net._sent_misc))
    return 0;

  if (client->_base._buf._offset >= lwroc_badrequest_serialized_size())
    return 0; /* We have sent the full message, close connection. */

  return 1;
}

/********************************************************************/

lwroc_select_item_info lwroc_net_badrequest_select_item_info =
{
  lwroc_net_badrequest_setup_select,
  lwroc_net_badrequest_after_select,
  lwroc_net_client_base_after_fail,
  lwroc_net_client_base_delete,
  0,
};

/********************************************************************/

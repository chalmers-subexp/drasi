/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_NET_INCOMING_H__
#define __LWROC_NET_INCOMING_H__

#include "lwroc_net_client_base.h"

/********************************************************************/

struct lwroc_net_reverse_link_t;

typedef struct lwroc_net_incoming_t
{
  lwroc_net_client_base _base;

  /* Timeout for incoming connection to be set up. */
  struct timeval _next_time;

  /* Decoded request while waiting for the link ack. */
  lwroc_request_msg _req_msg;
  /* In queue to send link ack token? */
  pd_ll_item _send_link_ack;
  /* Reverse link ack id and token we sent. */
  uint32_t _link_ack_rid; /* 0 = not doing link ack. */
  uint32_t _link_ack_token;

  /* Reverse link structure that approved us (if any). */
  struct lwroc_net_reverse_link_t *_rev_link;

  /* Array below is large enough to hold in incoming request,
   * link ack, and an outgoing badrequest response.
   */
  uint32_t          _raw[7];
} lwroc_net_incoming;

/********************************************************************/

extern lwroc_select_item_info lwroc_net_incoming_select_item_info;

/********************************************************************/

extern lwroc_select_item_info lwroc_net_badrequest_select_item_info;

/********************************************************************/

void lwroc_net_become_incoming(lwroc_net_incoming *client,
			       lwroc_select_info *si);

/********************************************************************/

void lwroc_udp_awaken_hints_add(lwroc_net_incoming *client);

/********************************************************************/

#endif/*__LWROC_NET_INCOMING_H__*/

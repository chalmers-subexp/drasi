/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_net_proto.h"
#include "lwroc_net_io.h"
#include "lwroc_net_io_util.h"
#include "lwroc_thread_block.h"
#include "lwroc_message.h"
#include "lwroc_select_util.h"
#include "lwroc_message_internal.h"
#include "lwroc_merge_struct.h"
#include "lwroc_triva_state.h"
#include "lwroc_main_iface.h"
#include "lwroc_udp_awaken_hints.h"
#include "lwroc_slow_async_loop.h"
#include "lwroc_merge_in.h"

#include "lwroc_net_client_base.h"
#include "lwroc_net_portmap.h"
#include "lwroc_net_incoming.h"
#include "lwroc_net_mon.h"
#include "lwroc_net_control.h"
#include "lwroc_net_message.h"
#include "lwroc_net_trigbus.h"
#include "lwroc_net_trans.h"
#include "lwroc_net_outgoing.h"
#include "lwroc_net_in_rev_link.h"
#include "lwroc_net_ntp.h"

#include "gen/lwroc_portmap_msg_serializer.h"
#include "gen/lwroc_request_msg_serializer.h"

#include "../dtc_arch/acc_def/myprizd.h"
#include "../dtc_arch/acc_def/has_msg_nosignal.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <signal.h>
#include <string.h>
#include <netdb.h>

/* This is used in lwroc_merge_in.c lwroc_merge_sort.c
 */
lwroc_merge_info _lwroc_merge_info;

/* For debugging of the state machine.  Low-level, not wanted in the
 * general case, and not via the usual network output, as we are the
 * network.  Thus plain printf.
 */
#define STATE_MACHINE_DEBUG 0

/* Very simple network communication.
 *
 * The server several connections, and handle them with a select()
 * statement.  (As usual.)
 *
 * When reading data, which can only be done by one source at a time,
 * also the source may be included in the select statement, making the
 * server operational also at times when waiting for more data.
 *
 * The communication is _unidirectional_.  I.e. whenever a client opens
 * a connection, data will be thrown at it, but we do not care about
 * any errors it may have.  Closed pipe is a closed pipe and that's
 * that.
 */

/* The monitor connections are a bit lazy.  If a client does not keep
 * ut, it will receive mixed (garbage) data.  This is however detected
 * by the fact that the message begins and ends with a sequence
 * number.  If they are not the same - do not use the data!  This
 * avoids us having to keep and handle a chunk of data to be sent for
 * each connection. Anyhow - monitors that are lagging are no monitors
 * - who wants to see old stale information?
 */

/* This union is not used directly.  It exist to easily get the
 * maximum size needed to allocate the memory structure for a data
 * client connection.
 */
union lwroc_data_client_alloc_t
{
  lwroc_net_incoming     _incoming;
  lwroc_net_message      _msg;
  lwroc_net_mon          _mon;
  lwroc_net_control      _ctrl;
  lwroc_net_trigbus      _master;
  lwroc_net_in_rev_link  _revlink;
  lwroc_net_trans_client _data;
};

PD_LL_SENTINEL(_ext_net_incoming);
PD_LL_SENTINEL(_ext_net_portmaps);

PD_LL_SENTINEL(_lwroc_net_trans_serv);

PD_LL_SENTINEL(_lwroc_merge_in_serv);

PD_LL_SENTINEL(_lwroc_net_trigbus_slaves);
PD_LL_SENTINEL(_lwroc_net_trigbus_ebs);

PD_LL_SENTINEL(_lwroc_net_clients);

PD_LL_SENTINEL(_ext_net_outgoing);

/* Later, once the event fragmentation has been fixed, the
 * zero-padding only happens when we have no more data to send for the
 * moment.  Therefore, it does not matter if we have to divide the
 * transmission into many small calls to write.
 */

char _lwroc_zero_block[4096];

lwroc_thread_instance *_lwroc_net_io_thread = NULL;

int _ext_net_portmap_fd = -1;
int _ext_net_data_serv_fd = -1;

void lwroc_net_io_server_bind(int port)
{
  uint16_t data_port = (uint16_t) -1;
  pd_ll_item *iter;

  if (port != -2)
    {
      /* Create the server socket.
       */

      _ext_net_portmap_fd =
	lwroc_net_io_socket_bind(port, 1, "portmap socket", 0);

      /* To avoid most of the problems with the server port getting stuck
       * with 'cannot bind: address in use', we'll open one more port, on
       * which all data traffic (persistent connections) goes.  The 'well
       * numbered' port is only used as a cheap portmapper, i.e. giving
       * the port number of the real data server port.
       */

      _ext_net_data_serv_fd =
	lwroc_net_io_socket_bind(-1, 1, "data socket", 0);

      /* The data server socket is not bound to any specific port.  That's
       * the entire gag - listen will assign a random port!
       */

      /* We need to know which port was chosen! */

      data_port =
	lwroc_net_io_socket_get_port(_ext_net_data_serv_fd,"data socket");

      /* Both sockets successfully connected. */

      LWROC_INFO_FMT("Started server on port %d (data port %d).",
		     port,data_port);
   }

  /* Transport protocol. */

  PD_LL_FOREACH(_lwroc_net_trans_serv, iter)
    {
      lwroc_net_trans_unit *unit =
	PD_LL_ITEM(iter, lwroc_net_trans_unit, _servers);

      lwroc_net_trans_unit_bind(unit);
    }

  lwroc_main_bind();

  /* Prepare the portmapper response message. */

  {
    lwroc_portmap_msg msg;

    msg._port = data_port;

    _lwroc_portmap_msg._size = lwroc_portmap_msg_serialized_size();
    _lwroc_portmap_msg._wire = (char *) malloc (_lwroc_portmap_msg._size);

    if (!_lwroc_portmap_msg._wire)
      LWROC_FATAL("Failure allocating memory for portmap wire message.");

    lwroc_portmap_msg_serialize(_lwroc_portmap_msg._wire, &msg);
  }

  /* Update: we generally want SIGPIPE signals to kill us.
   *
   * Locations that do not want to die by SIGPIPE, now write with
   * send(,,,MSG_NOSIGNAL) and (as already done) handle the EPIPE.
   *
   * The code here that blocked SIGPIPE has been removed, but it is
   * caught by the handler set up in the main function, to report why
   * we died and hold program for inspection.
   */
#if !HAS_MSG_NOSIGNAL
  {
    sigset_t sigmask;

    /* Warn about this undesirable way of working! */
    LWROC_INFO("MSG_NOSIGNAL not available, blocking SIGPIPE.  "
	       "(Should only happen on old OS!)");
    /* We don't want any SIGPIPE signals to kill us. */

    sigemptyset(&sigmask);
    sigaddset(&sigmask,SIGPIPE);

    sigprocmask(SIG_BLOCK,&sigmask,NULL);
  }
#endif

  /* Monitor information. */

  lwroc_net_monitor_setup();

  /* */

  memset (_lwroc_zero_block, 0, sizeof (_lwroc_zero_block));

  /* TODO: lwroc_net_trans_client is really large...
   * a) investigate why (file_header only explains 1k of 3.5k)
   * b) reallocate structure depending on connection...
   * Printer here as reminder (and not at usage) to only print once.
   */
  printf ("client union size: "
	  "%" MYPRIzd " %" MYPRIzd " %" MYPRIzd " %" MYPRIzd " "
	  "%" MYPRIzd " "
	  "%" MYPRIzd " "
	  "%" MYPRIzd " "
	  " => %" MYPRIzd "\n",
	  sizeof (lwroc_net_incoming    ),
	  sizeof (lwroc_net_message     ),
	  sizeof (lwroc_net_mon         ),
	  sizeof (lwroc_net_control     ),
	  sizeof (lwroc_net_trigbus     ),
	  sizeof (lwroc_net_in_rev_link ),
	  sizeof (lwroc_net_trans_client),
	  sizeof (union lwroc_data_client_alloc_t));
}

void lwroc_net_io_portmap_accept(lwroc_select_info *si)
{
  int client_fd = lwroc_net_io_accept(_ext_net_portmap_fd,
				      "pmap connection", NULL);
  lwroc_portmap_client *portmap;

  if (client_fd == -1)
    return;

  portmap = (lwroc_portmap_client *) malloc(sizeof(lwroc_portmap_client));

  if (!portmap)
    LWROC_FATAL("Failure allocating memory for portmap client state.");

  /* ok, so we got a connection... */

  portmap->_base._fd = client_fd;

  lwroc_net_become_portmap(portmap, si);

  portmap->_base._select_item.item_info = &lwroc_net_portmap_select_item_info;

  /* Add it to the list of clients. */

  PD_LL_ADD_BEFORE(&_ext_net_portmaps, &portmap->_base._clients);
  PD_LL_ADD_BEFORE(&_lwroc_net_clients, &portmap->_base._select_item._items);

  _lwroc_mon_net._ncon_pmap_total++;
}

void lwroc_net_io_data_serv_accept(lwroc_select_info *si)
{
  struct sockaddr_storage cli_addr;
  lwroc_net_incoming *client;

  int client_fd = lwroc_net_io_accept(_ext_net_data_serv_fd,
				      "data connection", &cli_addr);

  if (client_fd == -1)
    return;

  /* We allocate enough memory for any of the supported client types.
   */
  client =
    (lwroc_net_incoming *) malloc(sizeof(union lwroc_data_client_alloc_t));

  if (!client)
    LWROC_FATAL("Failure allocating memory for client state.");

  /* ok, so we got a connection... */

  client->_base._fd = client_fd;

  client->_base._addr = cli_addr;

  lwroc_net_become_incoming(client, si);

  client->_base._select_item.item_info = &lwroc_net_incoming_select_item_info;

  /* Add it to the list of clients */

  PD_LL_ADD_BEFORE(&_ext_net_incoming, &client->_base._clients);
  PD_LL_ADD_BEFORE(&_lwroc_net_clients, &client->_base._select_item._items);

  /*_cur_clients++;*/

  _lwroc_mon_net._ncon_total++;
}

/* For debug purposes. */

void lwroc_net_io_print_clients(void)
{
  pd_ll_item *iter;

  return;

  printf ("clients:\n");

  PD_LL_FOREACH(_lwroc_net_clients, iter)
    {
      lwroc_select_item *item =
	PD_LL_ITEM(iter, lwroc_select_item, _items);

      printf ("client: %p\n", item);
    }
}

void lwroc_net_io_loop_setup_service(void *arg,
				     lwroc_select_info *si)
{
  int shutdownx = *((int *) arg);
  pd_ll_item *iter;

  /* Regular monitor updates.  Unless shutdown. */

  if (!shutdownx)
    lwroc_net_monitor_setup_select(si);

  if (_ext_net_portmap_fd != -1)
    LWROC_READ_FD_SET(_ext_net_portmap_fd, si);

  if (_ext_net_data_serv_fd != -1)
    LWROC_READ_FD_SET(_ext_net_data_serv_fd, si);

  PD_LL_FOREACH(_lwroc_net_trans_serv, iter)
    {
      lwroc_net_trans_unit *unit =
	PD_LL_ITEM(iter, lwroc_net_trans_unit, _servers);

      lwroc_net_trans_unit_setup_select(unit, si);
    }

  {
    int to_thread = 0;
    lwroc_net_trans_loop_setup_service(&to_thread, si);
  }

  lwroc_net_ntp_choose(si);

  LWROC_ITEM_BUFFER_SET_NOTIFY(_lwroc_merge_info._discon_sources,
			       _lwroc_net_io_thread->_block);

  lwroc_net_io_print_clients();
}

void lwroc_net_io_loop_before_service(void *arg,
				      lwroc_select_info *si)
{
  int shutdownx = *((int *) arg);

  (void) shutdownx;
  (void) si;

  lwroc_net_io_print_clients();

  lwroc_net_skip_data();
}

void lwroc_net_io_loop_after_service(void *arg,
				     lwroc_select_info *si)
{
  int shutdownx = *((int *) arg);
  pd_ll_item *iter;

  if (_lwroc_triva_state)
    lwroc_triva_state_update(&si->now);

  /* Any outgoing connections that need to be re-established? */

  while (LWROC_ITEM_BUFFER_NONEMPTY(_lwroc_merge_info._discon_sources))
    {
      lwroc_merge_source *conn;

      /* printf ("discon source...\n"); */

      LWROC_ITEM_BUFFER_REMOVE(_lwroc_merge_info._discon_sources, &conn);

      assert(conn->_out._base._fd == -1);

      lwroc_net_become_outgoing(&conn->_out, si);
    }

#if 0
  send_client *diter;

  for (diter = _ext_net_clients._next; diter != &_ext_net_clients; )
    {
      client = diter;
      diter = diter->_next;

      if (client->_chunk)
	{
	  /* If nothing to do, or if write successful, continue */

	  if (!LWROC_WRITE_FD_ISSET(client->_fd, si) ||
	      lwroc_net_io_client_write(client))
	    continue;
	}
      else if (!shutdownx)
	continue; /* nothing to do */
      else
	{
	  uint32_t dummy;
	  ssize_t n;

	  /* We are in shutdown mode.  Continue until file handle is
	   * ready for reading.
	   */

	  if (!LWROC_READ_FD_ISSET(client->_fd, si))
	    continue;

	  /* It may be the magic coming from the client.  If so,
	   * consume that.
	   */

	  n = read(client->_fd,&dummy,sizeof(dummy));

	  /* Do not care too much about errors, as we will just close
	   * anyhow.  Only if there was data to consume, we do not
	   * close.
	   */

	  if (n == -1 && errno == EINTR)
	    continue;

	  if (n > 0)
	    {
	      /* Make sure we cannot get stuck here by the client
	       * sending copious amounts of data at us.
	       */

	      client->_offset += n;

	      if (client->_offset <= sizeof(uint32_t))
		continue;
	    }
	}

      /* Client has exited, at least got EPIPE
       * close and remove it
       */

      while (close(client->_fd) != 0)
	{
	  if (errno == EINTR)
	    continue;

	  LWROC_PERROR("close");
	  LWROC_ERROR("Failure closing client socket.");
	  break;
	}
      if (client->_chunk)
	client->_chunk->_consumers--;

      client->_prev->_next = client->_next;
      client->_next->_prev = client->_prev;

      free(client);

      _cur_clients--;
    }
#endif
  /* Accept new clients */
  if (_ext_net_portmap_fd != -1)
    {
      if (LWROC_READ_FD_ISSET(_ext_net_portmap_fd, si))
	lwroc_net_io_portmap_accept(si);
    }
  /* Accept new clients */
  if (_ext_net_data_serv_fd != -1)
    {
      if (LWROC_READ_FD_ISSET(_ext_net_data_serv_fd, si))
	lwroc_net_io_data_serv_accept(si);
    }
  /* Accept new clients */
  PD_LL_FOREACH(_lwroc_net_trans_serv, iter)
    {
      lwroc_net_trans_unit *unit =
	PD_LL_ITEM(iter, lwroc_net_trans_unit, _servers);

      lwroc_net_trans_unit_accept(unit, si);
    }
  /* Pick up any transport connections that failed in the loop thread. */
  {
    int to_thread = 0;
    lwroc_net_trans_loop_after_service(&to_thread, si);
  }

  /* Any reason to ping the slow thread for work? */
  if (_lwroc_udp_awaken_hints_write == 1)
    {
      /* Do not send again for this one. */
      _lwroc_udp_awaken_hints_write = 2;
      lwroc_thread_block_send_token(_lwroc_slow_async_thread->_block);
    }

  /* Discarding space in the message buffers is after handling the
   * (active) message buffer.  Such that we get called in case a
   * producer just woke us up to free space, but the message receiver
   * connection then decided to die on us.
   */
  if (PD_LL_IS_EMPTY(&_ext_net_messages))
    lwroc_msg_bufs_skip_oldest();

  if (_lwroc_shutdown == 1)
    {
      /* Tell first thread in termination sequence to end operations. */
      lwroc_thread_set_terminate_first();
      /* Only once. */
      _lwroc_shutdown = 2;
    }

  /* Monitor update last, such that we do not shoot in the middle
   * of a transmission more than necessary.
   */
  if (!shutdownx)
    lwroc_net_monitor_after_select(si);
}

#if 0
int lwroc_net_io_select_clients(int shutdownx)
{
  /* before select(): */

  if (shutdownx)
    {
      /* Hmm, this is not used, but needs thinking if used... */
      /* Do we really want to overwrite? */
      si.next_time.tv_sec = 2;
      si.next_time.tv_usec = 2;
      timeout = &si.next_time;
    }


  /* after select(): */

  if (ret == 0 && shutdownx) /* can only happen on timeout (i.e. if shutdown) */
    return 1;

  return 0;
}
#endif

void lwroc_net_io_server_close(void)
{
  /* Shutdown server more or less orderly.  Reason: if a server
   * (socket bind, listen & accept) shuts down before the clients
   * (socket connect()), then the port will be in use for some time
   * after the server is gone and a new server cannot connect.  By
   * making sure the clients go away orderly, i.e. they close the
   * connections first, we generally avoid this.
   */
#if 0
  if (_ext_net_clients._next != &_ext_net_clients)
    LWROC_INFO("Shutdown.  Waiting for clients to finish...");
#endif
  /* No further connections, please! */

  if (_ext_net_portmap_fd != -1)
    {
      close(_ext_net_portmap_fd);
      _ext_net_portmap_fd = -1;
    }

  if (_ext_net_data_serv_fd != -1)
    {
      close(_ext_net_data_serv_fd);
      _ext_net_data_serv_fd = -1;
    }

  /* We now serve the clients, as long as there is progress with a
   * timeout time.  As we have reached this point, they should have
   * gotten the shutdown message.  There is of course no guarantee
   * that they will honour it, so in shutdown mode, any client which
   * tells it's ready for writing with no more data to write, will be
   * closed as well.
   */
#if 0
  while (_ext_net_clients._next != &_ext_net_clients)
    {
      if (lwroc_net_io_select_clients(-1,-1,1))
	{
	  /* We had a time-out.  Simply give up. */
	  LWROC_WARNING("Timeout...");
	  break;
	}
    }
#endif
}

/********************************************************************/

void lwroc_net_io_thread_loop(lwroc_thread_instance *inst)
{
  int shutdownx = 0;

  /* TODO: it would make sense to be able to call the after_fail
   * routines also for lwroc_net_trigbus connections,
   * i.e. after_loop_cleanup = 1 (and then that parameter can be
   * removed).  Issue is that they complain about having failure even
   * though no message was to be received.
   */
  lwroc_select_loop(&_lwroc_net_clients, &inst->_terminate,
		    inst->_block,
		    lwroc_net_io_loop_setup_service, &shutdownx,
		    lwroc_net_io_loop_before_service, &shutdownx,
		    lwroc_net_io_loop_after_service, &shutdownx,
		    0);
  /*
  for ( ; !inst->_terminate; )
    {
      lwroc_net_io_select_clients(0);
    }
  */
  lwroc_net_io_server_close();
}

/********************************************************************/

lwroc_thread_info _net_io_thread_info =
{
  NULL,
  lwroc_net_io_thread_loop,
  LWROC_MSG_BUFS_NET_IO,
  "net io",
  LWROC_THREAD_NO_MSG_CLIENT_WAIT,
  LWROC_THREAD_TERM_NET_IO,
  LWROC_THREAD_CORE_PRIO_OTHER,
};

/********************************************************************/

void lwroc_prepare_net_io_thread(int port)
{
  lwroc_net_io_server_bind(port);

  _lwroc_net_io_thread =
    lwroc_thread_prepare(&_net_io_thread_info);

  lwroc_udp_awaken_hints_read();
  lwroc_udp_awaken_hints_send();
}

/********************************************************************/

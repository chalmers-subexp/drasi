/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_NET_IO_H__
#define __LWROC_NET_IO_H__

#include "lwroc_thread_block.h"
#include "lwroc_thread_util.h"

extern lwroc_thread_instance *_lwroc_net_io_thread;

void lwroc_net_io_server_bind(int port);

int lwroc_net_io_select_clients(int shutdownx);

void lwroc_net_io_server_close(void);

extern int _net_skips_data;

void lwroc_net_io_print_clients(void); /* for debug purposes */

/* TODO: remove this external again: */
extern PD_LL_SENTINEL_ITEM(_lwroc_net_clients);

void lwroc_prepare_net_io_thread(int port);

#endif/*__LWROC_NET_IO_H__*/

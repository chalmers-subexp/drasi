/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_net_io_util.h"
#include "lwroc_message.h"
#include "lwroc_hostname_util.h"

#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <sys/types.h>

void lwroc_net_io_nonblock(int fd, int nonblock, const char *descr)
{
  int flags;

  if ((flags = fcntl(fd,F_GETFL)) == -1)
    {
      LWROC_PERROR("fcntl");
      LWROC_FATAL_FMT("Failure getting %s flags.", descr);
    }

  if (fcntl(fd,F_SETFL,
	    (flags & ~O_NONBLOCK) | (nonblock ? O_NONBLOCK : 0)) == -1)
    {
      LWROC_PERROR("fcntl");
      LWROC_FATAL_FMT("Failure making %s %s.", descr,
		      nonblock ? "non-blocking" : "blocking");
    }
}

#define LWROC_MAX_BIND_ATTEMPTS  20

int lwroc_net_io_socket_bind(int port, int tcp, const char *descr,
			     int reuseaddr)
{
  struct sockaddr_in serv_addr;
  int fd;
  int type = (tcp ? SOCK_STREAM : SOCK_DGRAM);

  fd = socket(PF_INET, type, 0);

  if (fd < 0)
    {
      LWROC_PERROR("socket");
      LWROC_FATAL_FMT("Could not open %s.", descr);
    }

  /* On Linux at least, it looks like SO_REUSEADDR needs to be set
   * also for the earlier port if it is supposed to be effective.
   * I.e. no use to only try to set it when attempting connection a
   * second time.  So we set it always for connections that want it.
   */
  if (reuseaddr)
    {
      int flag;

      flag = 1;
      if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR,
		     (char *) &flag, sizeof(int)) != 0)
	{
	  LWROC_PERROR("bind");
	  LWROC_WARNING("Setting SO_REUSEADDR failed.");
	}
    }

  if (port != -1)
    {
      int attempts = 0;

      serv_addr.sin_family = AF_INET;
      serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
      serv_addr.sin_port = htons((uint16_t) port);

      for ( ; ; )
	{
	  if (bind (fd,
		    (struct sockaddr *) &serv_addr,sizeof(serv_addr)) == 0)
	    break;

	  if (errno == EADDRINUSE)
	    {
	      attempts++;
	      if (attempts <= LWROC_MAX_BIND_ATTEMPTS)
		{
		  LWROC_ERROR_FMT("Failure binding %s to port %d.",
				  descr, port);
		  LWROC_WARNING_FMT("Trying again in 10 s, attempt %d/%d.",
				    attempts, LWROC_MAX_BIND_ATTEMPTS);
		  sleep(10);
		  continue;
		}
	    }

	  LWROC_PERROR("bind");
	  LWROC_FATAL_FMT("Failure binding %s to port %d.", descr, port);
	}
    }

  if (tcp &&
      listen(fd,3) != 0)
    {
      LWROC_PERROR("listen");
      LWROC_FATAL_FMT("Failure to set %s listening on port %d.", descr, port);
    }

  /* Make the server socket non-blocking, such that we're not hit by
   * false selects (see Linux man page bug notes).
   */

  lwroc_net_io_nonblock(fd, 1, descr);

  return fd;
}

uint16_t lwroc_net_io_socket_get_port(int fd,const char *descr)
{
  struct sockaddr_storage addr;
  socklen_t len;

  len = sizeof(addr);

  if (getsockname(fd, (struct sockaddr *) &addr,&len) != 0)
    {
      LWROC_PERROR("getsockname");
      LWROC_FATAL_FMT("Failure getting %s port number.", descr);
    }

  return lwroc_get_port(&addr);
}

int lwroc_net_io_connect(int fd, const struct sockaddr_storage *addr)
{
  int ret;
  socklen_t addrlen = 0;

  if (addr->ss_family == AF_INET)
    addrlen = (socklen_t) sizeof (struct sockaddr_in);
#ifdef AF_INET6
  else if (addr->ss_family == AF_INET6)
    addrlen = (socklen_t) sizeof (struct sockaddr_in6);
#endif
  /* else good luck */

  ret = connect(fd, (const struct sockaddr *) addr, addrlen);

  return ret;
}

int lwroc_net_io_accept(int server_fd, const char *descr,
			struct sockaddr_storage *pcli_addr)
{
  int client_fd;

  struct sockaddr_storage cli_addr;
  socklen_t cli_len;
  char client_dotted[INET6_ADDRSTRLEN];

  cli_len = sizeof(cli_addr);

  client_fd = accept(server_fd,
		     (struct sockaddr *) &cli_addr, &cli_len);

  if (client_fd < 0)
    {
      if (errno == EINTR)
	return -1; /* we need to do it again, lets redo the select... */

      if (errno == EAGAIN || errno == EWOULDBLOCK)
	return -1; /* false select... */

      /* There are many errors of accept that may happen as
       * consequences of the network (ECONNABORTED, EPERM, EPROTO),
       * so we only deal with it as a warning
       */

      LWROC_PERROR("accept");
      LWROC_ERROR_FMT("Accepting %s failed...", descr);
      return -1;
    }

  /* Make the socket non-blocking, such that we're not hit by false
   * selects (see Linux man page bug notes).
   */

  lwroc_net_io_nonblock(client_fd, 1, descr);

  /* Tell from where the connection is. */

  lwroc_inet_ntop(&cli_addr, client_dotted, sizeof(client_dotted));

  LWROC_DEBUG_FMT("Accepted %s [%s]...",
		  descr, client_dotted);

  if (pcli_addr)
    {
      *pcli_addr = cli_addr;
      /* memcpy(pcli_addr, &cli_addr, sizeof(cli_addr)); */
    }

  return client_fd;
}

void lwroc_full_read(int fd, void *buf, size_t count,
		     const char *purpose, int fatal)
{
  for ( ; ; )
    {
      ssize_t n = read(fd, buf, count);

      if (n == -1)
	{
	  if (errno == EINTR ||
	      errno == EAGAIN)
	    continue;

	  LWROC_PERROR("read");
	  break;
	}
      if (n == 0)
	{
	  LWROC_ERROR("read: closed");
	  break;
	}

      count -= (size_t) n;
      buf += n;

      if (!count)
	return;
    }
  if (fatal)
    LWROC_FATAL_FMT("Error reading from '%s'.", purpose);
  else
    LWROC_ERROR_FMT("Error reading from '%s'.", purpose);
}

void lwroc_full_write(int fd, void *buf, size_t count,
		      const char *purpose, int fatal)
{
  for ( ; ; )
    {
      ssize_t n = write(fd, buf, count);

      if (n == -1)
	{
	  if (errno == EINTR ||
	      errno == EAGAIN)
	    continue;

	  LWROC_PERROR("write");
	  break;
	}
      if (n == 0)
	{
	  LWROC_ERROR("write: closed");
	  break;
	}

      count -= (size_t) n;
      buf += n;

      if (!count)
	return;
    }
  if (fatal)
    LWROC_FATAL_FMT("Error writing to '%s'.", purpose);
  else
    LWROC_ERROR_FMT("Error writing to '%s'.", purpose);
}

/********************************************************************/

void lwroc_safe_close(int fd)
{
  int ret;

 do_close:
  ret = close(fd);

  if (ret == -1)
    {
      if (errno == EINTR)
	goto do_close;

      LWROC_PERROR("close");
      return;
    }
}

/********************************************************************/

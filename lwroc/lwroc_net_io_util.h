/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_NET_IO_UTIL_H__
#define __LWROC_NET_IO_UTIL_H__

#include "../dtc_arch/acc_def/netinet_in_include.h"
#include "../dtc_arch/acc_def/socket_include.h"
#include "../dtc_arch/acc_def/socket_types.h"
#include "../dtc_arch/acc_def/mystdint.h"

void lwroc_net_io_nonblock(int fd, int nonblock, const char *descr);

int lwroc_net_io_socket_bind(int port, int tcp, const char *descr,
			     int reuseaddr);

uint16_t lwroc_net_io_socket_get_port(int fd,const char *descr);

int lwroc_net_io_connect(int fd, const struct sockaddr_storage *addr);

int lwroc_net_io_accept(int server_fd, const char *descr,
			struct sockaddr_storage *pcli_addr);

void lwroc_full_read(int fd, void *buf, size_t count,
		     const char *purpose, int fatal);

void lwroc_full_write(int fd, void *buf, size_t count,
		      const char *purpose, int fatal);

void lwroc_safe_close(int fd);

#endif/*__LWROC_NET_IO_UTIL_H__*/

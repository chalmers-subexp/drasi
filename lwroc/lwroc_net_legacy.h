/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_NET_LEGACY_H__
#define __LWROC_NET_LEGACY_H__

#include "../dtc_arch/acc_def/mystdint.h"

typedef struct ltcp_stream_trans_open_info_t
{
  uint32_t   testbit;
  uint32_t   bufsize;
  uint32_t   bufs_per_stream;
  uint32_t   streams;
} ltcp_stream_trans_open_info;

#define LMD_TCP_PORT_TRANS    6000
#define LMD_TCP_PORT_STREAM   6002
#define LMD_TCP_PORT_EVENT    6003

/* This does not really belong here, not legacy. */

#define LMD_TCP_PORT_FAKERNET              1
#define LMD_UDP_PORT_FAKERNET_IDEMPOTENT  15

/* These are not really legacy, but are used to 'fix' the legacy :) */

#define LMD_TCP_PORT_TRANS_MAP_ADD  1234

#define LMD_TCP_INFO_BUFSIZE_NODATA     -1
#define LMD_TCP_INFO_BUFSIZE_MAXCLIENTS -2

/* Info about the data port is put in the 'streams' member. */
#define LMD_TCP_INFO_STREAMS_PORT_MAP_MARK         0x50540000
#define LMD_TCP_INFO_STREAMS_PORT_MAP_MARK_MASK    0xffff0000
#define LMD_TCP_INFO_STREAMS_PORT_MAP_PORT_MASK    0x0000ffff

/* Info about number of connection attempts/s in the 'streams' member. */
#define LMD_TCP_INFO_STREAMS_NODATA_HOLDOFF_MARK       0x50550000
#define LMD_TCP_INFO_STREAMS_NODATA_HOLDOFF_MARK_MASK  0xffff0000
#define LMD_TCP_INFO_STREAMS_NODATA_HOLDOFF_RATE_MASK  0x0000ffff

/****************************************************************/

#define LMD_TCP_PORT_EBYE_XFER_PUSH  10305  /* Src connect(), sink accept(). */
/* A number picked from a hat: I.e. subject to change! */
#define LMD_TCP_PORT_EBYE_XFER_PULL  11305  /* Src accept(), sink connect(). */

/****************************************************************/

#endif/*__LWROC_NET_LEGACY_H__*/

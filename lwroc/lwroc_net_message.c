/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_net_message.h"
#include "lwroc_message.h"
#include "lwroc_message_internal.h"
#include "lwroc_net_io.h"
#include "lwroc_hostname_util.h"
#include "lwroc_timeouts.h"

#include "gen/lwroc_message_keepalive_serializer.h"
#include "gen/lwroc_message_item_ack_serializer.h"

#include <errno.h>
#include <assert.h>

/********************************************************************/

lwroc_msg_writing _lwroc_cur_messages_writing = { NULL, NULL, 0 };

time_t _lwroc_unlimited_log_quota_until = 0;

/********************************************************************/

void lwroc_msg_prepare_keepalive(lwroc_msg_writing *writing,
				 lwroc_net_message *client)
{
  lwroc_message_keepalive keep_alive;

  assert(sizeof (client->_raw) >= lwroc_message_keepalive_serialized_size());

  keep_alive._source = 0;

  lwroc_message_keepalive_serialize((char *) client->_raw,
				    &keep_alive);

  writing->_buf_consumer = NULL;
  writing->_data = (char *) client->_raw;
  writing->_size = lwroc_message_keepalive_serialized_size();
}

/********************************************************************/

int lwroc_net_message_after_fail(lwroc_select_item *item,
				 lwroc_select_info *si)
{
  lwroc_net_message *client =
    PD_LL_ITEM(item, lwroc_net_message, _base._select_item);

  LWROC_CWARNING(&client->_base,"Disconnected!");

  return lwroc_net_client_base_after_fail(item, si);
}

void lwroc_net_message_setup_select(lwroc_select_item *item,
				    lwroc_select_info *si)
{
  lwroc_net_message *client =
    PD_LL_ITEM(item, lwroc_net_message, _base._select_item);

  /* printf ("write client\n"); */

  /* Rate-limit quota is only added once per second.  That is assumed
   * better than a continuous quota-granting, since it allows bursts
   * of log messages to be produced, then likely with better internal
   * timing.  (I.e. speed of operation not impacted by log message
   * restrictions except once per second.)
   */

  if (si->now.tv_sec != client->_rate_limit_quota_updated)
    {
      /* Only add quota. */

      if (si->now.tv_sec > client->_rate_limit_quota_updated)
	{
	  double elapsed;

	  elapsed =
	    (double) si->now.tv_sec -
	    (double) client->_rate_limit_quota_updated;

	  client->_rate_limit_quota +=
	    elapsed * LWROC_LOG_MESSAGE_RATE_LIMIT;

	  if (client->_rate_limit_quota >
	      LWROC_LOG_MESSAGE_RATE_LIMIT_INIT_QUOTA)
	    client->_rate_limit_quota =
	      LWROC_LOG_MESSAGE_RATE_LIMIT_INIT_QUOTA;
	}

      client->_rate_limit_quota_updated = si->now.tv_sec;
    }

  /* Note that the keep-alive messages do not setup a timeout to wake
   * us up when needed.  Since the timeout is several s, it is enough
   * with the two wake-ups per second caused by the monitoring
   * updates.
   */

  /* 'New' messages are only sent if rate-limited allows it. */

  if (_lwroc_cur_messages_writing._data ||
      (si->now.tv_sec < client->_last_msg ||
       si->now.tv_sec > client->_last_msg +
       LWROC_CONNECTION_MSG_KEEPALIVE_INTERVAL) ||
      (lwroc_msg_bufs_read_avail(_lwroc_net_io_thread->_block) &&
       (client->_rate_limit_quota >= 0 || /* Have rate-limit quota. */
	(_lwroc_unlimited_log_quota_until != 0 && /* Unlimited? */
	 _lwroc_unlimited_log_quota_until >= si->now.tv_sec))))
    {
      LWROC_WRITE_FD_SET(client->_base._fd, si);
    }

  /* We need to check for reading, such that we are notified
   * if the connection goes down.
   *
   * We are also always willing to read ack messages.
   */

  LWROC_READ_FD_SET(client->_base._fd, si);
}

int lwroc_net_message_after_select(lwroc_select_item *item,
				   lwroc_select_info *si)
{
  lwroc_net_message *client =
    PD_LL_ITEM(item, lwroc_net_message, _base._select_item);

  /* printf ("write after\n"); */

  if (LWROC_READ_FD_ISSET(client->_base._fd, si))
    {
      if (!lwroc_net_client_rw_read(&client->_buf_read,
				    client->_base._fd,
				    "message ack",
				    &_lwroc_mon_net._recv_msg_ack))
	return 0;

      if (client->_buf_read._offset >= client->_buf_read._size)
	{
	  lwroc_message_item_ack ack_msg;
	  lwroc_deserialize_error desererr;
	  const char *end;

	  /* We have gotten ourselves what should be an ack message. */

	  end = lwroc_message_item_ack_deserialize(&ack_msg,
						   (const char *) client->
						   /**/ _raw_read,
						   client->_buf_read._offset,
						   &desererr);

	  if (end == NULL)
	    {
	      /* Bad message.  We will simply kill the connection. */
	      LWROC_CWARNING(&client->_base,
			     "Bad message ack received.");
	      return 0;
	    }
	  /*
	  printf ("ack: %2d %08x\n",
		  ack_msg._source,
		  ack_msg._ack_ident);
	  */

	  lwroc_msg_bufs_got_ack(ack_msg._source,
				 ack_msg._ack_ident);

	  /* We are ready to receive another one. */

	  client->_buf_read._offset = 0;
	}
    }

  if (!LWROC_WRITE_FD_ISSET(client->_base._fd, si))
    return 1;

  /* printf ("write after b\n"); */

  /* Some buffer had data. */
  /* Find out who. */

  if (!_lwroc_cur_messages_writing._data)
    {
      /* First write all source info. */
      if (!lwroc_msg_bufs_source_info_read_avail(&_lwroc_cur_messages_writing))
	{
	  lwroc_msg_bufs_oldest_read_avail(&_lwroc_cur_messages_writing);
	  if (!_lwroc_cur_messages_writing._data)
	    {
	      if (si->now.tv_sec < client->_last_msg ||
		  si->now.tv_sec > client->_last_msg +
		  LWROC_CONNECTION_MSG_KEEPALIVE_INTERVAL)
		lwroc_msg_prepare_keepalive(&_lwroc_cur_messages_writing,
					    client);
	      else
		{
		  /* printf ("consumed alignment\n"); */
		  return 1; /* consumed some alignment space */
		}
	    }
	  else
	    {
	      /* Account the use of rate-limit quota when the new
	       * message is prepared.
	       */
	      client->_rate_limit_quota -=
		(double) _lwroc_cur_messages_writing._size;
	      /* If we are unlimited, we would consume large amounts
	       * of 'future' quota.  Just let it get negative.
	       */
	      if (client->_rate_limit_quota < -LWROC_LOG_MESSAGE_RATE_LIMIT/10)
		client->_rate_limit_quota = -LWROC_LOG_MESSAGE_RATE_LIMIT/10;
	    }
	}
      client->_base._buf._offset = 0;
    }

  /* These should be set up elsewhere? (earlier) */
  client->_base._buf._ptr  = _lwroc_cur_messages_writing._data;
  client->_base._buf._size = _lwroc_cur_messages_writing._size;

  if (!lwroc_net_client_base_write(&client->_base, "message",
				   &_lwroc_mon_net._sent_msg))
      return 0;

  if (client->_base._buf._offset >= _lwroc_cur_messages_writing._size)
    {
      lwroc_msg_buf_consume(&_lwroc_cur_messages_writing);
      _lwroc_cur_messages_writing._data = NULL;

      client->_last_msg = si->now.tv_sec;
    }

  return 1;
}

/********************************************************************/

lwroc_select_item_info lwroc_net_message_select_item_info =
{
  lwroc_net_message_setup_select,
  lwroc_net_message_after_select,
  lwroc_net_message_after_fail,
  lwroc_net_client_base_delete,
  0,
};

/********************************************************************/

char *lwroc_net_message_fmt_msg_context(char *buf, size_t size,
					const void *ptr)
{
  const lwroc_net_message *client = (const lwroc_net_message *)
    (ptr - offsetof(lwroc_net_message, _base._msg_context));
  char dotted[INET6_ADDRSTRLEN];

  lwroc_inet_ntop(&client->_base._addr, dotted, sizeof (dotted));

  return lwroc_message_fmt_msg_context(buf, size,
				       "msgclnt: %s", dotted);
}

/********************************************************************/

void lwroc_net_become_message(lwroc_net_message *client)
{
  (void) client;

  lwroc_msg_bufs_new_client();

  client->_base._msg_context = lwroc_net_message_fmt_msg_context;

  /* Ensure we send a keepalive before any messages. */
  client->_last_msg = 0;

  client->_rate_limit_quota =
    LWROC_LOG_MESSAGE_RATE_LIMIT_INIT_QUOTA;
  client->_rate_limit_quota_updated = time(NULL);

  /* Reading end always read ack messages. */
  assert(sizeof (client->_raw_read) >=
	 lwroc_message_item_ack_serialized_size());

  client->_buf_read._ptr  = (char *) client->_raw_read;
  client->_buf_read._size = lwroc_message_item_ack_serialized_size();
  client->_buf_read._offset = 0;

  LWROC_CLOG(&client->_base,"Connected.");
}

/********************************************************************/

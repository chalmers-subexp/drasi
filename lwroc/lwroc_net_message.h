/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_NET_MESSAGE_H__
#define __LWROC_NET_MESSAGE_H__

#include "lwroc_net_client_base.h"
#include "lwroc_net_proto.h"

/********************************************************************/

typedef struct lwroc_net_message_t
{
  lwroc_net_client_base _base;

  time_t   _last_msg;

  uint32_t _raw[4];

  /* For reading.  Is parallel to writing. */
  lwroc_net_client_rw _buf_read;
  uint32_t _raw_read[5];

  double   _rate_limit_quota;
  time_t   _rate_limit_quota_updated;

} lwroc_net_message;

extern time_t _lwroc_unlimited_log_quota_until;

/********************************************************************/

extern lwroc_select_item_info lwroc_net_message_select_item_info;

/********************************************************************/

void lwroc_net_become_message(lwroc_net_message *client);

/********************************************************************/

extern PD_LL_SENTINEL_ITEM(_ext_net_messages);

/********************************************************************/

#endif/*__LWROC_NET_MESSAGE_H__*/

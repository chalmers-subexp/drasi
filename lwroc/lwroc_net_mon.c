/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_net_mon.h"
#include "lwroc_message.h"
#include "lwroc_mon_block.h"
#include "lwroc_pipe_buffer.h"
#include "lwroc_hostname_util.h"
#include "lwroc_message_internal.h"
#include "lwroc_data_pipe.h"
#include "lwroc_main_iface.h"

#include "gen/lwroc_monitor_source_header_serializer.h"
#include "gen/lwroc_message_source_serializer.h"
#include "gen/lwroc_monitor_main_source_serializer.h"
#include "gen/lwroc_monitor_net_source_serializer.h"

#include "gen/lwroc_monitor_block_header_serializer.h"
#include "gen/lwroc_monitor_main_block_serializer.h"
#include "gen/lwroc_monitor_net_block_serializer.h"
#include "gen/lwroc_monitor_file_block_serializer.h"
#include "gen/lwroc_monitor_in_block_serializer.h"

#include "gen/lwroc_monitor_dt_trace_serializer.h"

#include <errno.h>
#include <assert.h>
#include <string.h>
#include <arpa/inet.h>

#include "../dtc_arch/acc_def/has_msg_nosignal.h"

/********************************************************************/

typedef struct lwroc_mon_blocks_sending_t
{
  size_t    _size;
  char     *_wire;
  uint32_t  _sequence;
} lwroc_mon_blocks_sending;

lwroc_mon_blocks_sending _lwroc_mon_initial;
lwroc_mon_blocks_sending _lwroc_mon_current[LWROC_MON_SEND_BUFFER_BLOCK_NUM];

struct timeval _lwroc_mon_next_time_request;
struct timeval _lwroc_mon_next_time_update;

/********************************************************************/

extern lwroc_pipe_buffer_control *_lwroc_readout_data_mon;

/********************************************************************/

lwroc_monitor_net_block     _lwroc_mon_net;
lwroc_monitor_block_header  _lwroc_mon_header;
lwroc_monitor_block_header  _lwroc_mon_header_dt_trace;

/********************************************************************/

lwroc_pipe_buffer_control *_lwroc_readout_dt_trace_ctrl = NULL;
lwroc_pipe_buffer_consumer *_lwroc_readout_dt_trace_consumer = NULL;

lwroc_monitor_dt_trace *_lwroc_mon_dt_trace = NULL;

/********************************************************************/

void lwroc_update_monitor(struct timeval *now)
{
  char *wire;

  /* Update the data in the network monitor. */

  _lwroc_mon_net._time._sec  = (uint64_t) now->tv_sec;
  _lwroc_mon_net._time._nsec = (uint32_t) now->tv_usec * 1000;

  _lwroc_mon_net._buf_data_fill = _lwroc_readout_data_mon ?
    lwroc_pipe_buffer_fill(_lwroc_readout_data_mon) : 0;

  if (_lwroc_bug_fatal_reported)
    {
      /* LWROC_NET_SYS_HEALTH_SIGNAL,
       * LWROC_NET_SYS_HEALTH_FATAL,
       * LWROC_NET_SYS_HEALTH_BUG
       */
      _lwroc_mon_net._sys_health = _lwroc_bug_fatal_reported;
    }
  else if (!_lwroc_message_client_seen)
    _lwroc_mon_net._sys_health = LWROC_NET_SYS_HEALTH_LOGWAIT;
  else
    _lwroc_mon_net._sys_health = LWROC_NET_SYS_HEALTH_OK;

  _lwroc_mon_net._num_msg = (uint32_t) lwroc_msg_num_msgs();

  /* Ensure that we manage to copy to all staging areas before we
   * start to overwrite the data block.  (Which may be transmitting
   * to clients.)
   */

  if (lwroc_mon_blocks_stage(0) &&
      lwroc_mon_blocks_stage(1))
    {
      lwroc_mon_blocks_sending *mon_cur =
	&_lwroc_mon_current[LWROC_MON_SEND_BUFFER_BLOCK_NORMAL];

      /* Package all the data, surrounded by the sequence number.
       * that we get some fixed size and magic markers before / after
       * the sequence numbers does not matter.
       */

      _lwroc_mon_header._sequence++;

      wire = mon_cur->_wire;

      /* Leading sequence number. */

      /* The header size is also supposed to mark the entire
       * structure, and not just the header.  This since it is used by
       * the reader to know how much to read from the network before
       * unpacking. */

      wire = lwroc_monitor_block_header_serialize_envelope(wire,
							   &_lwroc_mon_header,
							   mon_cur->_size);

      /* Our monitor block. */

      wire = lwroc_monitor_net_block_serialize(wire,
					       &_lwroc_mon_net);

      /* The connection blocks. */

      wire = lwroc_mon_blocks_serialize(wire, 1);

      /* Blocks from all other sources. */

      wire = lwroc_mon_blocks_serialize(wire, 0);

      /* Trailing sequence number. */

      wire = lwroc_monitor_block_header_serialize(wire,
						  &_lwroc_mon_header);

      assert (wire == mon_cur->_wire + mon_cur->_size);

      mon_cur->_sequence++;

      /* Next update, when? */

      /* We try to schedule the updates a few ms after the top of the
       * second.
       */

      _lwroc_mon_next_time_update = *now;
      _lwroc_mon_next_time_update.tv_usec = 20000;
      if (timercmp(&_lwroc_mon_next_time_update,now,<)) {
	_lwroc_mon_next_time_update.tv_usec = 520000;
      }
      if (timercmp(&_lwroc_mon_next_time_update,now,<)) {
	_lwroc_mon_next_time_update.tv_sec++;
	_lwroc_mon_next_time_update.tv_usec = 20000;
      }
    }
  else
    {
      /* We try again, sort of soon. */

      _lwroc_mon_next_time_update = *now;
      _lwroc_mon_next_time_update.tv_usec += 100000;
      if (_lwroc_mon_next_time_update.tv_usec >= 1000000)
	{
	  _lwroc_mon_next_time_update.tv_usec -= 1000000;
	  _lwroc_mon_next_time_update.tv_sec++;
	}
    }
}

void lwroc_update_monitor_dt_trace(void)
{
  int i;
  size_t total = 0;
  size_t used = 0;

  uint32_t *dest = _lwroc_mon_dt_trace->_array;

  char *wire;

  /* Is there DT trace data to be sent? */

  /* Since the data may be wrapping in the buffer, we have to request
   * the pointer twice (second time with offset).  But we mark data as
   * read once.  The second request may actually get a second batch of
   * (fresh) adjacent data.  But in that case there was no wrapping
   * anyhow.
   *
   * All data consist of 2-word entries.  The first with markers, and
   * the second a (pure) timestamp.
   *
   * Since the buffer is not larger than the array in the transmit
   * message, we will not cut events apart.
   */

  for (i = 0; i < 2; i++)
    {
      size_t n;
      uint32_t *data_dt_trace;
      char *ptr;
      size_t nwaste;

      uint32_t *p, *end;

    again:
      n = lwroc_pipe_buffer_avail_read(_lwroc_readout_dt_trace_consumer,
				       NULL, &ptr, &nwaste,
				       used, 0);

      if (!n)
	{
	  if (nwaste)
	    {
	      used += nwaste;
	      goto again;
	    }
	  break;
	}

      data_dt_trace = (uint32_t *) ptr;

      used += n;

      p = data_dt_trace;
      end = (uint32_t *) (((char *) data_dt_trace) + n);

      while (p+1 < end)
	{
	  uint32_t word1, word2;

	  word1 = *(p++);
	  word2 = *(p++);

	  dest[total++] = word1;
	  dest[total++] = word2;
	}
    }

  if (!total)
    return; /* There was no data to send along at all. */

  _lwroc_mon_dt_trace->_array_used = (uint32_t) total;
  /* Snatch the ctime from the cumulative deadtime measurement. */
  _lwroc_mon_dt_trace->_ctime = _lwroc_mon_net._meas_dt_ctime;

  lwroc_pipe_buffer_did_read(_lwroc_readout_dt_trace_consumer, used);

  {
    lwroc_mon_blocks_sending *mon_dt_trace =
      &_lwroc_mon_current[LWROC_MON_SEND_BUFFER_BLOCK_DT_TRACE];

    _lwroc_mon_header_dt_trace._sequence++;

    wire = mon_dt_trace->_wire;

    /* Leading sequence number. */

    /* The header size is also supposed to mark the entire
     * structure, and not just the header.  This since it is used by
     * the reader to know how much to read from the network before
     * unpacking. */

    wire =
      lwroc_monitor_block_header_serialize_envelope(wire,
						    &_lwroc_mon_header_dt_trace,
						    mon_dt_trace->_size);

    /* The actual data. */

    wire = lwroc_monitor_dt_trace_serialize(wire,
					    _lwroc_mon_dt_trace);

   /* Trailing sequence number. */

    wire = lwroc_monitor_block_header_serialize(wire,
						&_lwroc_mon_header_dt_trace);

    assert (wire == mon_dt_trace->_wire + mon_dt_trace->_size);

    mon_dt_trace->_sequence++;
  }
}

/********************************************************************/

void lwroc_request_monitor(struct timeval *now)
{
  /* We update the request flag for the other threads, such that they
   * (hopefully) update their structures soon.
   */

  _lwroc_mon_update_seq++;
  MFENCE;
  /* If it was a top-of-a-second update, then ping all threads that
   * have registered to be notified.
   */
  if (_lwroc_mon_next_time_request.tv_usec % 500000 == 0)
    {
      lwroc_mon_blocks_notify();
    }

  /* Requests are done 10 times per second.  This ensures rather
   * recent data even if the event rate is low (~< 10 Hz).  In case it
   * is high, it would have been enough with one request sort of
   * before the actual update (copying) by this thread.  But in that
   * case the overhead of the additional requests are anyhow small
   * compared to all the accounting of the primary information (event
   * counts etc) that anyhow has to be done per event.
   */

  /* We try to schedule the updates at the top of the second. */

  _lwroc_mon_next_time_request = *now;
  _lwroc_mon_next_time_request.tv_usec -=
    _lwroc_mon_next_time_request.tv_usec % 100000;
  _lwroc_mon_next_time_request.tv_usec += 100000;
  if (_lwroc_mon_next_time_request.tv_usec >= 1000000) {
    _lwroc_mon_next_time_request.tv_usec -= 1000000;
    _lwroc_mon_next_time_request.tv_sec++;
  }
}

/********************************************************************/

extern lwroc_pipe_buffer_control *_lwroc_main_data;

void lwroc_net_monitor_setup(void)
{
  _lwroc_mon_header._sequence = 0;
  _lwroc_mon_header._type     = LWROC_MONITOR_BLOCK_TYPE_NORMAL;
  _lwroc_mon_header_dt_trace._sequence = 0;
  _lwroc_mon_header_dt_trace._type     = LWROC_MONITOR_BLOCK_TYPE_DT_TRACE;

  memset (&_lwroc_mon_net, 0, sizeof (_lwroc_mon_net));

  {
    lwroc_monitor_source_header initial;
    lwroc_message_source     net_src_source;
    lwroc_monitor_net_source net_src;
    lwroc_message_source_sersize sz_net_src_source;
    char *wire;

    initial._blocks = 1 + lwroc_mon_blocks_num(0);

    lwroc_mon_source_set(&net_src_source);

    net_src._buf_data_size = lwroc_pipe_buffer_size(_lwroc_readout_data_mon);
    net_src._conn_blocks = lwroc_mon_blocks_num(1);

    _lwroc_mon_initial._size  = lwroc_monitor_source_header_serialized_size();
    _lwroc_mon_initial._size +=
      lwroc_message_source_serialized_size(&net_src_source,
					   &sz_net_src_source);
    _lwroc_mon_initial._size += lwroc_monitor_net_source_serialized_size();
    _lwroc_mon_initial._size += lwroc_mon_blocks_source_serialized_size(1);
    _lwroc_mon_initial._size += lwroc_mon_blocks_source_serialized_size(0);
    _lwroc_mon_initial._wire = (char *) malloc (_lwroc_mon_initial._size);
    _lwroc_mon_initial._sequence = (uint32_t) -1;

    if (!_lwroc_mon_initial._wire)
      LWROC_FATAL("Failure allocating memory for initial monitor info.");

    wire = _lwroc_mon_initial._wire;

    /* Size is for entire structure, see comment in lwroc_update_monitor. */
    wire = lwroc_monitor_source_header_serialize_envelope(wire, &initial,
							  _lwroc_mon_initial.
							  /**/_size);
    wire = lwroc_message_source_serialize(wire, &net_src_source,
					  &sz_net_src_source);
    wire = lwroc_monitor_net_source_serialize(wire, &net_src);
    wire = lwroc_mon_blocks_source_serialized_copy(wire, 1);
    wire = lwroc_mon_blocks_source_serialized_copy(wire, 0);

    assert (wire == _lwroc_mon_initial._wire + _lwroc_mon_initial._size);
  }

  {
    lwroc_pipe_buffer_init(&_lwroc_readout_dt_trace_ctrl,
			   1, NULL, 4096,
			   2);

    _lwroc_readout_dt_trace_consumer =
      lwroc_pipe_buffer_get_consumer(_lwroc_readout_dt_trace_ctrl, 0);

    _lwroc_mon_dt_trace = (lwroc_monitor_dt_trace *)
      malloc (sizeof (lwroc_monitor_dt_trace));

    if (!_lwroc_mon_dt_trace)
      LWROC_FATAL("Failure allocating memory for monitor DT trace structure.");
  }

  {
    struct timeval now;

    lwroc_mon_blocks_sending *mon_cur =
      &_lwroc_mon_current[LWROC_MON_SEND_BUFFER_BLOCK_NORMAL];
    lwroc_mon_blocks_sending *mon_dt_trace =
      &_lwroc_mon_current[LWROC_MON_SEND_BUFFER_BLOCK_DT_TRACE];

    mon_cur->_size  = lwroc_monitor_block_header_serialized_size();
    mon_cur->_size += lwroc_monitor_net_block_serialized_size();
    mon_cur->_size += lwroc_mon_blocks_serialized_size(1);
    mon_cur->_size += lwroc_mon_blocks_serialized_size(0);
    mon_cur->_size += lwroc_monitor_block_header_serialized_size();
    mon_cur->_wire = (char *) malloc (mon_cur->_size);
    mon_cur->_sequence = (uint32_t) -1;

    if (!mon_cur->_wire)
      LWROC_FATAL("Failure allocating memory for monitor message.");

    mon_dt_trace->_size  = lwroc_monitor_block_header_serialized_size();
    mon_dt_trace->_size += lwroc_monitor_dt_trace_serialized_size();
    mon_dt_trace->_size += lwroc_monitor_block_header_serialized_size();
    mon_dt_trace->_wire = (char *) malloc (mon_dt_trace->_size);
    mon_dt_trace->_sequence = (uint32_t) -1;

    if (!mon_dt_trace->_wire)
      LWROC_FATAL("Failure allocating memory for monitor DT trace message.");

    gettimeofday(&now, NULL);

    lwroc_update_monitor(&now);  /* updates sequence number */
    lwroc_request_monitor(&now); /* sets timeout */
  }
}

/********************************************************************/

void lwroc_net_monitor_setup_select(lwroc_select_info *si)
{
  if (!timerisset(&si->next_time) ||
      timercmp(&_lwroc_mon_next_time_request,&si->next_time,<))
    si->next_time = _lwroc_mon_next_time_request;
  if (/* !timerisset(&next_time) || */
      timercmp(&_lwroc_mon_next_time_update,&si->next_time,<))
    si->next_time = _lwroc_mon_next_time_update;
}

time_t _lwroc_last_log_no_start_wait_warning = 0;

void lwroc_net_monitor_after_select(lwroc_select_info *si)
{
  if (timercmp(&_lwroc_mon_next_time_request,&si->now,<))
    {
      /* printf ("request...\n"); */
      lwroc_request_monitor(&si->now);
    }
  if (timercmp(&_lwroc_mon_next_time_update,&si->now,<))
    {
      /* printf ("update...\n"); */
      lwroc_update_monitor(&si->now);
      lwroc_update_monitor_dt_trace();

      /* We parasite on this timer to do the check that
       * data pipes are making progress.
       */
      lwroc_data_pipes_check_max_ev_interval(&si->now);

      /* And abuse it to remind the user to not work without logging.
       */
      if (_config._log_no_start_wait)
	{
	  time_t diff;

	  diff = si->now.tv_sec - _lwroc_last_log_no_start_wait_warning;

	  if (diff < 0 || diff > 300) /* Every 5 minutes. */
	    {
	      if (_lwroc_last_log_no_start_wait_warning)
		LWROC_ERROR("Running with --log-no-start-wait.  "
			    "Don't do that!");
	      _lwroc_last_log_no_start_wait_warning = si->now.tv_sec;
	    }
	}
    }
}

/********************************************************************/

void lwroc_net_mon_setup_select(lwroc_select_item *item,
				lwroc_select_info *si)
{
  lwroc_net_mon *client =
    PD_LL_ITEM(item, lwroc_net_mon, _base._select_item);

  /* We need to check for reading, such that we are notified
   * if the connection goes down.
   */

  LWROC_READ_FD_SET(client->_base._fd, si);

  /* Do we have data to write?
   * Either already sending something, or a buffer has new data.
   */
  /*
  printf ("sent? %d %d (cur %d)\n",
	  client->_data._mon._sent_initial, client->_data._mon._sent_sequence,
	  _lwroc_mon_current._sequence);
  */

  if (client->_mon._send_buffer == LWROC_MON_SEND_BUFFER_NONE)
    {
      if (client->_mon._sent_sequence[LWROC_MON_SEND_BUFFER_BLOCK_NORMAL] !=
	  _lwroc_mon_current[LWROC_MON_SEND_BUFFER_BLOCK_NORMAL]._sequence)
	client->_mon._send_buffer = LWROC_MON_SEND_BUFFER_BLOCK_NORMAL;
      else if (client->_mon.
	       _sent_sequence[LWROC_MON_SEND_BUFFER_BLOCK_DT_TRACE] !=
	  _lwroc_mon_current[LWROC_MON_SEND_BUFFER_BLOCK_DT_TRACE]._sequence)
	client->_mon._send_buffer = LWROC_MON_SEND_BUFFER_BLOCK_DT_TRACE;
    }

  if (client->_mon._send_buffer != LWROC_MON_SEND_BUFFER_NONE)
    LWROC_WRITE_FD_SET(client->_base._fd, si);
}

int lwroc_net_mon_after_select(lwroc_select_item *item,
			       lwroc_select_info *si)
{
  lwroc_net_mon *client =
    PD_LL_ITEM(item, lwroc_net_mon, _base._select_item);
  lwroc_mon_blocks_sending *mon_send;
  size_t left;
  ssize_t n;

  if (!lwroc_fail_on_read_check_eof(client->_base._fd, si))
    {
      /* printf ("read 0\n"); */
      return 0;
    }

  if (!LWROC_WRITE_FD_ISSET(client->_base._fd, si))
    return 1;

  /*
  printf ("send: %d (%d / %d) (%d / %d)\n",
	  client->_mon._send_buffer,
	  client->_mon._sent_sequence[0],
	  _lwroc_mon_current[0]._sequence,
	  client->_mon._sent_sequence[1],
	  _lwroc_mon_current[1]._sequence);
  */

  if (client->_mon._send_buffer == LWROC_MON_SEND_BUFFER_INITIAL)
    mon_send = &_lwroc_mon_initial;
  else
    {
      assert (client->_mon._send_buffer >= 0 &&
	      client->_mon._send_buffer < LWROC_MON_SEND_BUFFER_BLOCK_NUM);
      mon_send = &_lwroc_mon_current[client->_mon._send_buffer];
    }

  /* These should be set up elsewhere! (earlier) */
  /*
  client->_base._buf._ptr  = mon_send->_wire;
  client->_base._buf._size = mon_send->_size;

  if (!lwroc_net_client_base_write(&client->_base, "monitor",
				   _lwroc_mon_net._sent_mon)
      return 0;
  */

  left = mon_send->_size - client->_base._buf._offset;

  n = send (client->_base._fd,
	    mon_send->_wire + client->_base._buf._offset, left,
	    MSG_NOSIGNAL);

  if (n == -1)
    {
      if (errno == EINTR)
	return 1;
      if (errno == EPIPE) {
	LWROC_CINFO(&client->_base,
		    "Monitor client has disconnected, socket closed.");
      } else {
	LWROC_CPERROR(&client->_base,"write");
	LWROC_CERROR(&client->_base,
		     "Error while writing monitor info to client.");
      }
      return 0;
    }

  /* Write was successful. */

  _lwroc_mon_net._sent_mon += (uint64_t) n;

  if (n > 0 && client->_base._buf._offset == 0)
    {
      /* We just started to send a new chunk, so when we are done, we'll
       * claim to have sent at least this far.  (If it was broken in the
       * middle does not matter, as we'll then actually send again.)
       */

      client->_mon._sending_sequence = mon_send->_sequence;
    }

  client->_base._buf._offset += (size_t) n;

  if (client->_base._buf._offset >= mon_send->_size)
    {
      if (client->_mon._send_buffer < LWROC_MON_SEND_BUFFER_BLOCK_NUM)
	client->_mon._sent_sequence[client->_mon._send_buffer] =
	  client->_mon._sending_sequence;

      /* lwroc_net_mon_setup_select() will find out if there is new data. */
      client->_mon._send_buffer = LWROC_MON_SEND_BUFFER_NONE;

      client->_base._buf._offset = 0;
    }

  return 1;
}

/********************************************************************/

lwroc_select_item_info lwroc_net_monitor_select_item_info =
{
  lwroc_net_mon_setup_select,
  lwroc_net_mon_after_select,
  lwroc_net_client_base_after_fail,
  lwroc_net_client_base_delete,
  0,
};

/********************************************************************/

char *lwroc_net_mon_fmt_msg_context(char *buf, size_t size,
				    const void *ptr)
{
  const lwroc_net_mon *client = (const lwroc_net_mon *)
    (ptr - offsetof(lwroc_net_mon, _base._msg_context));
  char dotted[INET6_ADDRSTRLEN];

  lwroc_inet_ntop(&client->_base._addr, dotted, sizeof (dotted));

  return lwroc_message_fmt_msg_context(buf, size,
				       "monclnt: %s", dotted);
}

/********************************************************************/

void lwroc_net_become_mon(lwroc_net_mon *client)
{
  client->_mon._send_buffer = LWROC_MON_SEND_BUFFER_INITIAL;
  client->_mon._sent_sequence[LWROC_MON_SEND_BUFFER_BLOCK_NORMAL] =
    (uint32_t) -1;
  /* Since DT trace buffers may be old and very stale, we never send
   * old data.
   */
  client->_mon._sent_sequence[LWROC_MON_SEND_BUFFER_BLOCK_DT_TRACE] =
    _lwroc_mon_current[LWROC_MON_SEND_BUFFER_BLOCK_DT_TRACE]._sequence;

  client->_base._msg_context = lwroc_net_mon_fmt_msg_context;
}

/********************************************************************/

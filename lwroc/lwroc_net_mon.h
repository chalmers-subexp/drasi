/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_NET_MON_H__
#define __LWROC_NET_MON_H__

#include "lwroc_net_client_base.h"
#include "lwroc_net_proto.h"

/********************************************************************/

#define LWROC_MON_SEND_BUFFER_BLOCK_NORMAL    0
#define LWROC_MON_SEND_BUFFER_BLOCK_DT_TRACE  1
#define LWROC_MON_SEND_BUFFER_BLOCK_NUM       2

#define LWROC_MON_SEND_BUFFER_INITIAL         2
#define LWROC_MON_SEND_BUFFER_NONE            3

typedef struct lwroc_net_mon_t
{
  lwroc_net_client_base _base;

  /* Monitoring state. */

  struct
  {
    int               _send_buffer;
    uint32_t          _sent_sequence[LWROC_MON_SEND_BUFFER_BLOCK_NUM];
    uint32_t          _sending_sequence;
  } _mon;
} lwroc_net_mon;

/********************************************************************/

extern lwroc_select_item_info lwroc_net_monitor_select_item_info;

/********************************************************************/

void lwroc_net_become_mon(lwroc_net_mon *client);

/********************************************************************/

void lwroc_net_monitor_setup_select(lwroc_select_info *si);

void lwroc_net_monitor_after_select(lwroc_select_info *si);

/********************************************************************/

extern lwroc_monitor_net_block _lwroc_mon_net;

void lwroc_update_monitor(struct timeval *now);

void lwroc_request_monitor(struct timeval *now);

void lwroc_net_monitor_setup(void);

/********************************************************************/

#endif/*__LWROC_NET_MON_H__*/

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_net_ntp.h"
#include "lwroc_net_io.h"
#include "lwroc_message.h"
#include "lwroc_timeouts.h"
#include "lwroc_hostname_util.h"
#include "lwroc_parse_util.h"
#include "lwroc_main_iface.h"
#include "../dtc_arch/acc_def/myisfinite.h"

#include <string.h>
#include <errno.h>
#include <math.h>
#include <float.h>

#include "../dtc_arch/acc_def/socket_include.h"

#include "../dtc_arch/acc_def/myinttypes.h"

#define LWROC_NET_NTP_DEBUG 0

#define LWROC_UNIX_EPOCH_NTP 	(((70*365 + 17)*86400ul))

/********************************************************************/

#define LWROC_NTP_MAX_BACKOFF_NONRESPONSIVE  15   /* s */

#define LWROC_NTP_QUERY_INTERVAL_INITIAL     100000   /* us */

#define LWROC_NTP_QUERY_INTERVAL_MAX         5000000  /* us */

/********************************************************************/

PD_LL_SENTINEL(_lwroc_net_ntp_conns);

lwroc_ntp_global_sol *_lwroc_ntp_global_sol = NULL;

lwroc_ntp_client *_lwroc_ntp_conn_chosen = NULL;

struct timeval _lwroc_ntp_choose_next_time = { 0, 0 };

/********************************************************************/

/* Utility routine.
 */

#define timerhalf(r) do {	\
    if ((r)->tv_sec & 1) {	\
      (r)->tv_usec += 1000000;  \
    }                           \
    (r)->tv_sec /= 2;		\
    (r)->tv_usec /= 2;		\
  } while (0)

/********************************************************************/

/* This function is called when we have completed the bursted queries
 * of one round.  Either due to getting the response to the last
 * query, or because we timed out for the last query.
 */

void lwroc_net_ntp_sample(lwroc_ntp_client *conn)
{
  uint64_t stamp_avg;
  int64_t delay;
  int64_t delay_us;
  struct timeval diff;
  int i;
  struct timeval *a, *b;


  i = conn->_track._v_last;

  /* Take the time as the average. */

  /* time_t is not always signed, so we cannot easily handle
   * negative times.  Find out who is first.
   */
  a = &conn->_best_in._local_time;
  b = &conn->_best_out._local_time;

  if (timercmp(a, b, >))
    {
      b = &conn->_best_in._local_time;
      a = &conn->_best_out._local_time;
    }

  timersub(b, a, &diff);
  timerhalf(&diff);

  timeradd(a, &diff, &conn->_track._v[i]._time);

  /* The difference between the two stamps can be positive or
   * negative, depending on who is first.  We must therefore treat the
   * difference as a signed value when dividing by 2.
   */
  stamp_avg =
    conn->_best_out._server_stamp +
    (uint64_t) (((int64_t) (conn->_best_in._server_stamp -
			    conn->_best_out._server_stamp)) / 2);
  delay = (conn->_best_out._delay + conn->_best_in._delay);
  delay_us = ((delay * 1000000) >> 32);

  /* For testing purposes, we can add a saw-tooth distortion to the
   * local clock sample.
   */
  if (0)
    {
      int phase_us;
      int add_us;
      struct timeval add;

      /* Saw-tooth has period of 100 s */
      phase_us =
	(int) ((conn->_track._v[i]._time.tv_sec % 100) * 1000000) +
	(int) (conn->_track._v[i]._time.tv_usec);

      /* Up to 20 s, we add, then we fall back. */
      if (phase_us < 20 * 1000000)
	add_us = phase_us / ((20 * 1000000) / 2000);
      else
	add_us = (100 * 1000000 - phase_us) / ((80 * 1000000) / 2000);

      printf ("phase_us %10d add_us %10d\n",
	      phase_us, add_us);

      add.tv_sec  = 0;
      add.tv_usec = add_us;

      timeradd(&conn->_track._v[i]._time, &add, &conn->_track._v[i]._time);
    }

  conn->_track._v[i]._stamp = stamp_avg;
  conn->_track._v[i]._udiff = (int) delay_us;

#if LWROC_NET_NTP_DEBUG
  printf ("rawsmp: [%d.%06d]  %016" PRIu64"  %016" PRIu64"  [%d.%06d]\n",
	  (int) conn->_best_out._local_time.tv_sec,
	  (int) conn->_best_out._local_time.tv_usec,
	  conn->_best_out._server_stamp,
	  conn->_best_in._server_stamp,
	  (int) conn->_best_in._local_time.tv_sec,
	  (int) conn->_best_in._local_time.tv_usec);

  printf ("sample: %d.%06d  %016" PRIu64"  [%d.%06d]"
	  "  %016" PRId64"  %016" PRId64"\n",
	  (int) conn->_track._v[i]._time.tv_sec,
	  (int) conn->_track._v[i]._time.tv_usec,
	  stamp_avg,
	  (int) diff.tv_sec,
	  (int) diff.tv_usec,
	  conn->_best_out._delay,
	  conn->_best_in._delay);
#endif

  if (conn->_log)
    {
      struct timeval serv;
      struct timeval serv_off;
      char sign;

      /* We need to convert the server time to local timescale. */
      serv.tv_sec = (time_t) ((stamp_avg >> 32) - LWROC_UNIX_EPOCH_NTP);
      serv.tv_usec = (int) (((stamp_avg & 0xffffffff) * 1000000) >> 32);

      if (timercmp(&serv, &conn->_track._v[i]._time, <))
	{
	  sign = '-';
	  timersub(&conn->_track._v[i]._time, &serv, &serv_off);
	}
      else /* including 0 */
	{
	  sign = '+';
	  timersub(&serv, &conn->_track._v[i]._time, &serv_off);
	}

#define UNIX_EPOCH 2208988800UL /* 1970 - 1900 in seconds */
      LWROC_CLOG_FMT(&conn->_base,
		     "NTP sample: @lcl: %d.%06d srv: %c%d.%06d dly: %d.%06d",
		     (int) conn->_track._v[i]._time.tv_sec,
		     (int) conn->_track._v[i]._time.tv_usec,
		     sign,
		     (int) serv_off.tv_sec,
		     (int) serv_off.tv_usec,
		     (int) (delay_us / 1000000),
		     (int) (delay_us % 1000000));
    }

  conn->_track._v_last =
    (conn->_track._v_last + 1) % LWROC_TRACK_TIMESTAMP_HISTORY;

  if (conn->_track._v_last == 0)
    {
      lwroc_track_timestamp_filter_fit(&conn->_track);

      /* Status of last sample will be checked as next thing. */
    }

  if (conn->_track._ref.y_ref) /* We have a fit, so can compare. */
    {
      double d_diff;
      double d_sigma;
      double sigmas;
      double delay_reduced;
      double delay_sigmas;

      /* Calculate how far away the last sample is from the fitted
       * expectation.
       *
       * Value is compared to fit, but before comparison, we subtract
       * the delay time (roundtrip) of the measurement, minus the best
       * measurement.
       */
      /* Either if data was reduced or not, i is the last sample added. */

      d_diff = lwroc_track_timestamp_fit_diff(&conn->_track._ref,
					      &conn->_track._v[i]._time,
					      conn->_track._v[i]._stamp);

      d_sigma = sqrt(conn->_track._ref.lfs.var_y0);

      delay_reduced = (double) delay -
	((double) (((int64_t) conn->_track._ref.min_udiff) << 32)) / 1000000.;
      if (delay_reduced < 0)
	delay_reduced = 0;

      sigmas = d_diff / d_sigma;

      delay_sigmas = delay_reduced / d_sigma;

#if LWROC_NET_NTP_DEBUG
      printf ("d_diff: %.6f  %.6f (s: %.6f d: %" PRId64 " %d dr:%.6f) "
	      "[%.2f %.2f]\n",
	      d_diff, d_diff * (1 / (double) 0x100000000ll),
	      d_sigma, delay, conn->_track._ref.min_udiff,
	      delay_reduced,
	      sigmas, delay_sigmas);
#endif

      /* For the currently chosen source, we will do data recollection
       * if the new value mismatches the expectation.
       *
       * Avoid for other sources, in order to not cause undue server
       * load.  They will eventually make a new data collection anyhow.
       */
      if (_lwroc_ntp_conn_chosen == conn &&
	  (sigmas < -10. - delay_sigmas ||
	   sigmas >  10. + delay_sigmas))
	{
#if LWROC_NET_NTP_DEBUG
	  printf ("  XXX  %d", conn->_fit_status);
#endif

	  switch (conn->_fit_status)
	    {
	    case LWROC_NTP_FIT_STATUS_INVESTIGATE1:
	      /* Happened second time.  Print message. */
	      LWROC_CINFO_FMT(&conn->_base,
			      "NTP sample far off (%.0f us, sigma %.1f), "
			      "investigating.",
			      d_diff * (1000000. / (double) 0x100000000ll),
			      sigmas);
	      /* Verify a third time, before blowing our collection. */
	      conn->_fit_status = LWROC_NTP_FIT_STATUS_INVESTIGATE2;
	      break;
	    case LWROC_NTP_FIT_STATUS_INVESTIGATE2:
	      LWROC_CWARNING(&conn->_base,
			     "NTP samples thrice mismatching expectation, "
			     "making new collection.  "
			     "(Local clock rate jump?)");
	      /* We got a second confirming crazy value. */
	      conn->_fit_status = LWROC_NTP_FIT_STATUS_RECOLLECT;
	      /* Better make a new collection (quickly).
	       * Discard all samples.
	       */
	      conn->_track._v_last = 0;
	      conn->_wait_usec = LWROC_NTP_QUERY_INTERVAL_INITIAL;
	      break;
	    case LWROC_NTP_FIT_STATUS_RECOLLECT:
	      /* Just continue the collection. */
	      break;
	    default:
	      /* We need to verify that the situation is bad. */
	      /* Also avoid printing, happens too often. */
	      conn->_fit_status = LWROC_NTP_FIT_STATUS_INVESTIGATE1;
	      break;
	    }

#if LWROC_NET_NTP_DEBUG
	  printf (" ---> %d\n", conn->_fit_status);
#endif
	}
      else if (conn->_fit_status != LWROC_NTP_FIT_STATUS_RECOLLECT ||
	       conn->_track._v_last == 0)
	conn->_fit_status = LWROC_NTP_FIT_STATUS_OK;
    }

  /* Update the globally visible state.
   * Even if not a new fit, we may have changed the status.
   */
  conn->_track._ref.status = conn->_fit_status;
  lwroc_two_copy_update(&_lwroc_ntp_global_sol->_two_copy,
			&conn->_track._ref);

  /* If fit is good, and we have cycled a full turn, possible
   * increase the sampling interval.
   */
  if (conn->_fit_status == LWROC_NTP_FIT_STATUS_OK &&
      conn->_track._v_last == 0)
    {
      /* With 16 measurements, 5 s apart and 100 us precision, we
       * would have a chance to reach 100e-6 / (16*5) = 1.25 ppm.
       */

      /* While starting, we want to get better and better fits. */
      conn->_wait_usec *= 2;
      if (conn->_wait_usec > LWROC_NTP_QUERY_INTERVAL_MAX)
	conn->_wait_usec = LWROC_NTP_QUERY_INTERVAL_MAX;
      else
	{
	  /* When starting, we should use the earliest measurements too,
	   * to get a better baseline.  Cull half the samples.
	   */

	  for (i = 1; i < LWROC_TRACK_TIMESTAMP_HISTORY/2; i++)
	    conn->_track._v[i] = conn->_track._v[i*2];

	  conn->_track._v_last = i;

#if LWROC_NET_NTP_DEBUG
	  printf ("Retained %d samples, interval: %d usec\n",
		  conn->_track._v_last, conn->_wait_usec);
#endif
	}
    }
}

/********************************************************************/

void lwroc_net_ntp_setup_select(lwroc_select_item *item,
				lwroc_select_info *si)
{
  lwroc_ntp_client *conn =
    PD_LL_ITEM(item, lwroc_ntp_client, _base._select_item);

  /* Are we expecting a response? */
  if (conn->_queries_active >
      conn->_queries_received)
    LWROC_READ_FD_SET(conn->_base._fd, si);

  /* Timeout. */
  if (lwroc_select_info_time_passed_else_setup(si,
					       &conn->_next_time))
    LWROC_WRITE_FD_SET(conn->_base._fd, si);
}

typedef struct lwroc_ntpv4_packet_t
{
  uint32_t _header;
  uint32_t _root_delay;
  uint32_t _root_disp;
  uint32_t _ref_id;
  uint32_t _tstamp_ref_sec;
  uint32_t _tstamp_ref_frac;
  uint32_t _tstamp_orig_sec;
  uint32_t _tstamp_orig_frac;
  uint32_t _tstamp_recv_sec;
  uint32_t _tstamp_recv_frac;
  uint32_t _tstamp_tmit_sec;
  uint32_t _tstamp_tmit_frac;

} lwroc_ntp_packet_ntpv4;

#define LWROC_NTPv4_LEAP_SHIFT        30
#define LWROC_NTPv4_LEAP_MASK       0x03

#define LWROC_NTPv4_VERS_SHIFT        27
#define LWROC_NTPv4_VERS_MASK       0x07

#define LWROC_NTPv4_VERS_V4            4

#define LWROC_NTPv4_MODE_SHIFT        24
#define LWROC_NTPv4_MODE_MASK       0x07

#define LWROC_NTPv4_MODE_CLIENT        3
#define LWROC_NTPv4_MODE_SERVER        4

#define LWROC_NTPv4_STRATUM_SHIFT     16
#define LWROC_NTPv4_STRATUM_MASK    0xff

#define LWROC_NTPv4_POLL_SHIFT         8
#define LWROC_NTPv4_POLL_MASK       0xff

#define LWROC_NTPv4_PRECISION_SHIFT   16
#define LWROC_NTPv4_PRECISION_MASK  0xff

#define NTP_HDR(h,item) (((h) >> LWROC_NTPv4_##item##_SHIFT) & \
			 LWROC_NTPv4_##item##_MASK)

int lwroc_net_ntp_after_select(lwroc_select_item *item,
			       lwroc_select_info *si)
{
  lwroc_ntp_client *conn =
    PD_LL_ITEM(item, lwroc_ntp_client, _base._select_item);

  if (conn->_queries_active >
      conn->_queries_received &&
      LWROC_READ_FD_ISSET(conn->_base._fd, si))
    {
      /* Deal with the response. */

      lwroc_ntp_packet_ntpv4 packet;
      ssize_t n;
      struct sockaddr src_addr;
      socklen_t addrlen;
      int q;

      addrlen = sizeof (src_addr);

      n = recvfrom(conn->_base._fd, &packet, sizeof (packet),
		   0,
		   (struct sockaddr *) &src_addr,
		   &addrlen);

      packet._header = ntohl(packet._header);

      (void) n;

#if LWROC_NET_NTP_DEBUG
      printf ("response: %d  0x%08x (LI:%d V:%d M:%d ST:%d)  "
	      "%08x:%08x %08x:%08x %08x:%08x %08x:%08x\n",
	      (int) n,
	      packet._header,
	      NTP_HDR(packet._header,LEAP),
	      NTP_HDR(packet._header,VERS),
	      NTP_HDR(packet._header,MODE),
	      NTP_HDR(packet._header,STRATUM),
	      packet._tstamp_ref_sec,
	      packet._tstamp_ref_frac,
	      packet._tstamp_orig_sec,
	      packet._tstamp_orig_frac,
	      packet._tstamp_recv_sec,
	      packet._tstamp_recv_frac,
	      packet._tstamp_tmit_sec,
	      packet._tstamp_tmit_frac);
#endif

      for (q = 0; q < conn->_queries_active; q++)
      {
	uint64_t stamp_recv;
	uint64_t stamp_tmit;
	uint64_t us_tmit;
	uint64_t us_recv;

	lwroc_ntp_client_leg out_leg, in_leg;

	if (conn->_query[q]._orig_sec  != packet._tstamp_orig_sec ||
	    conn->_query[q]._orig_frac != packet._tstamp_orig_frac)
	  continue; /* Not any of our queries. */

	conn->_query[q]._orig_sec = 0;

	stamp_recv =
	  (((uint64_t) ntohl(packet._tstamp_recv_sec)) << 32) |
	  /**/         ntohl(packet._tstamp_recv_frac);
	stamp_tmit =
	  (((uint64_t) ntohl(packet._tstamp_tmit_sec)) << 32) |
	  /**/         ntohl(packet._tstamp_tmit_frac);

	us_tmit =
	  ((LWROC_UNIX_EPOCH_NTP +
	    (uint64_t) conn->_query[q]._send_time.tv_sec) << 32) |
	  (((uint64_t) conn->_query[q]._send_time.tv_usec) << 32) / 1000000;
	us_recv =
	  ((LWROC_UNIX_EPOCH_NTP +
	    (uint64_t) si->now.tv_sec) << 32) |
	  (((uint64_t) si->now.tv_usec) << 32) / 1000000;

	out_leg._server_stamp = stamp_recv;
	in_leg._server_stamp  = stamp_tmit;
	out_leg._local_time = conn->_query[q]._send_time;
	in_leg._local_time  = si->now;
	out_leg._delay = (int64_t) (stamp_recv - us_tmit);
	in_leg._delay  = (int64_t) (us_recv - stamp_tmit);

	{
	  struct timeval diff;

	  timersub(&si->now, &conn->_query[q]._send_time,
		   &diff);

#if LWROC_NET_NTP_DEBUG
	  printf ("%d.%06d  [%d] %d->%d  "
		  "%016" PRIx64"  %016" PRIx64 "  "
		  "%8" PRId64 " %8" PRId64 " %8" PRId64 "\n",
		  (int) diff.tv_sec,
		  (int) diff.tv_usec,
		  q,
		  conn->_queries_active,
		  conn->_queries_received,
		  us_tmit, us_recv,
		  out_leg._delay,
		  stamp_tmit - stamp_recv,
		  in_leg._delay);
#endif
	}

	if (conn->_queries_received == 0 ||
	    out_leg._delay < conn->_best_out._delay)
	  conn->_best_out = out_leg;
	if (conn->_queries_received == 0 ||
	    in_leg._delay < conn->_best_in._delay)
	  conn->_best_in = in_leg;

	conn->_queries_received++;
	/* We got a response, so good to go again (we assume). */
	conn->_backoff_sec = 0;

	if (conn->_queries_received == LWROC_NTP_NUM_BURST_QUERY)
	  {
	    lwroc_net_ntp_sample(conn);

	    /* Reset the series. */
	    conn->_queries_active   = 0;
	    conn->_queries_received = 0;

	    /* The next request shall not be too soon. */
	    {
	      /*
	      conn->_next_time = si->now;
	      conn->_next_time.tv_sec += 1;
	      */
	      struct timeval wait;

	      if (conn->_fit_status == LWROC_NTP_FIT_STATUS_INVESTIGATE1 ||
		  conn->_fit_status == LWROC_NTP_FIT_STATUS_INVESTIGATE2)
		{
		  wait.tv_sec  = 0;
		  wait.tv_usec = LWROC_NTP_QUERY_INTERVAL_INITIAL;
		}
	      else
		{
		  wait.tv_sec  = conn->_wait_usec / 1000000;
		  wait.tv_usec = conn->_wait_usec % 1000000;
		}

	      timeradd(&si->now,
		       &wait,
		       &conn->_next_time);

#if LWROC_NET_NTP_DEBUG && 0
	      printf ("now: %d.%06d  wait: %d.%06d  next: %d.%06d\n",
		      (int) si->now.tv_sec,
		      (int) si->now.tv_usec,
		      (int) wait.tv_sec,
		      (int) wait.tv_usec,
		      (int) conn->_next_time.tv_sec,
		      (int) conn->_next_time.tv_usec);
#endif
	    }
	  }

	if (conn->_queries_active == 1)
	  {
	    /* It is the response to the first query.
	     *
	     * Setup the interval between following queries.
	     */
	    /* Next interval same as this first response time. */
	    timersub(&si->now, &conn->_query[q]._send_time,
		     &conn->_query_interval);
	    /* The last timeout is twice the first response time. */
	    timeradd(&conn->_query_interval, &conn->_query_interval,
		     &conn->_query_timeout);
	    /* And fudge the timeout of the next query to be now. */
	    /* Must do this query via a timeout, in order to make sure
	     * that the file descriptor is writable. */
	    conn->_next_time = si->now;

	    /* Note: for all other queries, the timeouts are set
	     * as the queries are fired away.
	     */
	  }
      }
    }

  if (timercmp(&si->now, &conn->_next_time, >) &&
      LWROC_WRITE_FD_ISSET(conn->_base._fd, si))
    {
      /* Send a NTP time query. */

      lwroc_ntp_packet_ntpv4 packet;
      int q;

      /* Have we fired off the entire burst?  Then the timeout
       * is reached because we have not gotten the response to
       * the last/all queries.
       */
      if (conn->_queries_active == LWROC_NTP_NUM_BURST_QUERY)
	{
	  if (conn->_queries_received)
	    lwroc_net_ntp_sample(conn);

	  /* Reset the series. */
	  conn->_queries_active   = 0;
	  conn->_queries_received = 0;
	  /* Make another request burst again at some time. */
	  goto query_again;
	}

      /* Another query. */
      q = conn->_queries_active++;

      memset(&packet, 0, sizeof (packet));

      packet._header = htonl((LWROC_NTPv4_VERS_V4 <<
			      LWROC_NTPv4_VERS_SHIFT) |
			     (LWROC_NTPv4_MODE_CLIENT <<
			      LWROC_NTPv4_MODE_SHIFT) |
			     (6 << /* claim a poll of 2^6 */
			      LWROC_NTPv4_POLL_SHIFT) |
			     (32 <<
			      LWROC_NTPv4_PRECISION_SHIFT));

      /* Use the global counter of NTP queries to mark the request. */
      conn->_query[q]._orig_sec  = (_lwroc_mon_net._ntp_queries++);
      /* As well as some more junk, which should defeat most
       * spoofing-by-mistake.
       */
      conn->_query[q]._orig_frac = (((uint32_t) (si->now.tv_usec & 0xff)) ^
				    ((uint32_t) si->now.tv_sec));

      /* As these are pure identifiers, there is no need to adjust the
       * network order.  We just want to compare what we get back.
       */
      packet._tstamp_tmit_sec  = /*htonl*/(conn->_query[q]._orig_sec);
      packet._tstamp_tmit_frac = /*htonl*/(conn->_query[q]._orig_frac);

      /* Get as fresh time as possible, just before the query. */
      gettimeofday(&conn->_query[q]._send_time, NULL);

      sendto(conn->_base._fd, &packet, sizeof (packet),
	     0,
	     (struct sockaddr *) &conn->_base._addr,
	     (socklen_t) sizeof (struct sockaddr_in) /* TODO */);

#if LWROC_NET_NTP_DEBUG
      printf ("query  %d->%d  %08x:%08x  %d.%06d\n",
	      conn->_queries_active,
	      conn->_queries_received,
	      packet._tstamp_tmit_sec,
	      packet._tstamp_tmit_frac,
	      (int) conn->_query[q]._send_time.tv_sec,
	      (int) conn->_query[q]._send_time.tv_usec);
#endif

      /* If it is the first query of a burst, we setup the timeout
       * for the next burst (in case we get no response).
       *
       * If it is the last query of the burst, we setup the timeout for
       * when evaluation will be required.
       *
       * In between, setup the short query-time for the next query
       * within the burst.
       */
      if (conn->_queries_active == 1)
	{
	query_again:
	  /* Increase time period if polling non-responding server. */
	  conn->_backoff_sec++;
	  if (conn->_backoff_sec > LWROC_NTP_MAX_BACKOFF_NONRESPONSIVE)
	    conn->_backoff_sec = LWROC_NTP_MAX_BACKOFF_NONRESPONSIVE;
	  /* Make another request again, even if we get no response. */
	  conn->_next_time = si->now;
	  conn->_next_time.tv_sec += conn->_backoff_sec;

	  if (conn->_backoff_sec > 1 &&
	      conn->_log)
	    LWROC_LOG_FMT("NTP server no response (interval %d).",
			  conn->_backoff_sec);
	}
      else if (conn->_queries_active == LWROC_NTP_NUM_BURST_QUERY)
	{
	  /* We will not wait for the last query for longer than
	   * twice the original response time.
	   */
	  timeradd(&si->now,
		   &conn->_query_timeout,
		   &conn->_next_time);
	}
      else
	{
	  /* Next query is soon. */
	  timeradd(&si->now,
		   &conn->_query_interval,
		   &conn->_next_time);
	  /* And next one in half the time. */
	  timerhalf(&conn->_query_interval);
	}
    }

  return 1;
}

/********************************************************************/

void lwroc_net_ntp_choose(lwroc_select_info *si)
{
  pd_ll_item *iter;
  double best_compare = DBL_MAX;
  lwroc_ntp_client *best_conn = NULL;
  double mul_compare;

  if (!_lwroc_ntp_global_sol)
    return;

  if (!lwroc_select_info_time_passed_else_setup(si,
						&_lwroc_ntp_choose_next_time))
    return;

  /* Find the best source. */

  mul_compare = 1.0;

  PD_LL_FOREACH(_lwroc_net_ntp_conns, iter)
    {
      lwroc_ntp_client *conn =
	PD_LL_ITEM(iter, lwroc_ntp_client, _ntp_conns);
      double compare;

      /* Penalise later sources, to prefer the sources in the
       * user-given order.
       */
      mul_compare *= 1.5;

      /* If the backoff has passed the limit, the server is not responsive. */
      if (conn->_backoff_sec >= LWROC_NTP_MAX_BACKOFF_NONRESPONSIVE)
	continue;

      if (!isfinite(conn->_track._ref.lfs.var_y0) ||
	  conn->_track._ref.lfs.var_y0 == 0. ||
	  !isfinite(conn->_track._ref.lfs.var_dydx) ||
	  conn->_track._ref.lfs.var_dydx == 0.)
	continue;

      compare =
	sqrt(conn->_track._ref.lfs.var_y0) *
	sqrt(conn->_track._ref.lfs.var_dydx);
      compare *= mul_compare;

      /* printf("%.6f %.6f\n", compare, mul_compare); */

      if (_lwroc_ntp_conn_chosen != conn)
	compare *= 3; /* Fudge factor to avoid flipping between sources. */

      if (compare < best_compare)
	{
	  best_compare = compare;
	  best_conn    = conn;
	}
    }


  if (_config._log_track_timestamp ||
      best_conn != _lwroc_ntp_conn_chosen)
    {
      LWROC_INFO_FMT("%16s %14s F NTP servers",
		     "......off [us]", "...slope [ppm]");

      PD_LL_FOREACH(_lwroc_net_ntp_conns, iter)
	{
	  lwroc_ntp_client *conn =
	    PD_LL_ITEM(iter, lwroc_ntp_client, _ntp_conns);

	  (void) conn;

#define LWROC_DIV_2P32(x) (x / (double) 0x100000000ll)

	  LWROC_INFO_FMT("%8.2f(%6.2f) %6.3f(%6.3f) %c %s",
			 (LWROC_DIV_2P32(conn->_track._ref.lfs.y0)      ) *1.e6,
			 (sqrt(conn->_track._ref.lfs.var_y0)  /
			  conn->_track._ref.lfs.dydx) *1.e6,
			 (LWROC_DIV_2P32(conn->_track._ref.lfs.dydx) - 1) *1.e6,
			 (sqrt(conn->_track._ref.lfs.var_dydx)  /
			  conn->_track._ref.lfs.dydx) *1.e6,
			 best_conn == conn ? '*' : ' ',
			 conn->_hostname);
	}
    }

  _lwroc_ntp_conn_chosen = best_conn;

  _lwroc_ntp_choose_next_time = si->now;
  _lwroc_ntp_choose_next_time.tv_sec += 20 * 5;
}

/********************************************************************/

char *lwroc_net_ntp_fmt_msg_context(char *buf, size_t size,
				    const void *ptr)
{
  const lwroc_ntp_client *conn = (const lwroc_ntp_client*)
    (ptr - offsetof(lwroc_ntp_client, _base._msg_context));

  return lwroc_message_fmt_msg_context(buf, size,
				       "NTP: %s", conn->_hostname);
}

/********************************************************************/

lwroc_select_item_info lwroc_net_ntp_select_item_info =
{
  lwroc_net_ntp_setup_select,
  lwroc_net_ntp_after_select,
  lwroc_net_client_base_after_fail,
  lwroc_net_client_base_delete,
  0,
};

/********************************************************************/

void lwroc_ntp_client_usage(void)
{
  printf ("\n");
  printf ("--ntp=[<OPTIONS>,]HOST[:PORT]\n");
  printf ("\n");
  printf ("  log                      Log NTP samples.\n");
  printf ("  HOST[:PORT]              NTP server.\n");
  printf ("\n");
}

/********************************************************************/

#define NTP_DEFAULT_PORT  123

void lwroc_add_ntp_client(const char *cfg)
{
  lwroc_ntp_client *conn;
  const char *hostname = NULL;
  const char *request;
  lwroc_parse_split_list parse_info;

  conn = (lwroc_ntp_client *) malloc (sizeof (lwroc_ntp_client));

  if (!conn)
    LWROC_FATAL("Memory allocation failure (NTP client).");

  memset(conn, 0, sizeof (lwroc_ntp_client));

  conn->_track._purpose = "NTP";

  conn->_base._select_item.item_info =
    &lwroc_net_ntp_select_item_info;

  lwroc_parse_split_list_setup(&parse_info, cfg, ',');

  while ((request = lwroc_parse_split_list_next(&parse_info)) != NULL)
    {
      if (LWROC_MATCH_C_ARG("help"))
	{
	  lwroc_ntp_client_usage();
	  exit(0);
	}
      else if (lwroc_parse_split_list_final(&parse_info))
	{
	  /*CMDLINEARG:HOST*/
	  hostname = strdup_chk(request);
	}
      else if (LWROC_MATCH_C_ARG("log"))
	conn->_log = 1;
      else
	LWROC_BADCFG_FMT("Unrecognised NTP option: %s", request);
    }

  if (!lwroc_get_host_port(hostname, NTP_DEFAULT_PORT,
			   &conn->_base._addr))
    exit(1);

  /* Create the socket. */
  if (!lwroc_net_client_base_create_socket(&conn->_base,
					   LWROC_SOCKET_UDP))
    exit(1);

  LWROC_LOG_FMT("NTP server: %s.", hostname);

  /* conn->_next_time = si->now; */
  gettimeofday(&conn->_next_time, NULL);
  conn->_next_time.tv_usec += LWROC_NTP_QUERY_INTERVAL_INITIAL;
  conn->_wait_usec = LWROC_NTP_QUERY_INTERVAL_INITIAL;
  conn->_fit_status = LWROC_NTP_FIT_STATUS_NONE;

  conn->_base._msg_context = lwroc_net_ntp_fmt_msg_context;
  conn->_track._msg_context = &(conn->_base._msg_context);

  conn->_hostname = hostname;

  /* Add to the list of clients handled by the network thread. */
  PD_LL_ADD_BEFORE(&_lwroc_net_clients, &conn->_base._select_item._items);

  if (!_lwroc_ntp_global_sol)
    {
      _lwroc_ntp_global_sol = (lwroc_ntp_global_sol *)
	malloc (sizeof (lwroc_ntp_global_sol));

      if (!_lwroc_ntp_global_sol)
	LWROC_FATAL("Memory allocation failure (NTP global solution).");

      lwroc_two_copy_init(&_lwroc_ntp_global_sol->_two_copy,
			  _lwroc_ntp_global_sol->_copy01,
			  sizeof (_lwroc_ntp_global_sol->_copy01[0]),
			  &conn->_track._ref);
    }

  PD_LL_ADD_BEFORE(&_lwroc_net_ntp_conns, &conn->_ntp_conns);
}

/********************************************************************/

/* This force-include was in lwroc_readout.c, but since also needed
 * in the merger, moved here.
 */

#define LWROC_TRACK_TIMESTAMP_LOG_FMT       LWROC_LOG_FMT
#define LWROC_TRACK_TIMESTAMP_CCLOG_FMT     LWROC_CCLOG_FMT
#define LWROC_TRACK_TIMESTAMP_INFO_FMT      LWROC_INFO_FMT
#define LWROC_TRACK_TIMESTAMP_ERROR_FMT     LWROC_ERROR_FMT
#define LWROC_TRACK_TIMESTAMP_WARNING_FMT   LWROC_WARNING_FMT

#define LWROC_TRACK_TIMESTAMP_UPDATE_MAIN_MON  1

#define LWROC_TRACK_TIMESTAMP_LOG_NEW_FIT   _config._log_track_timestamp

#include <math.h>
#include "../dtc_arch/acc_def/mynan.h"
#include "lwroc_track_timestamp.c"

/********************************************************************/

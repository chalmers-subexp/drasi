/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_NET_NTP_H__
#define __LWROC_NET_NTP_H__

#include "lwroc_net_client_base.h"
#include "lwroc_track_timestamp_extra.h"

/********************************************************************/

#define LWROC_NTP_FIT_STATUS_NONE          0  /* No fit performed. */
#define LWROC_NTP_FIT_STATUS_OK            1  /* Fit ok, last sample as
					       * expected. */
#define LWROC_NTP_FIT_STATUS_INVESTIGATE1  2  /* Last sample strange,
					       * verifying.  (i.e. getting
					       * another sample to check.) */
#define LWROC_NTP_FIT_STATUS_INVESTIGATE2  3  /* Verify a second time. */
#define LWROC_NTP_FIT_STATUS_RECOLLECT     4  /* Collecting fresh data.
					       * (after likely jump). */

/********************************************************************/

typedef struct lwroc_ntp_client_query_t
{
  struct timeval _send_time;

  /* For verification. */
  uint32_t _orig_sec;
  uint32_t _orig_frac;

} lwroc_ntp_client_query;

typedef struct lwroc_ntp_client_leg_t
{
  struct timeval _local_time;
  uint64_t       _server_stamp;
  int64_t        _delay;
} lwroc_ntp_client_leg;

/* First query wakes thing up, then issue a burst of another 4
 * with decreasing intervals.  Running more queries does not
 * seem to improve things.
 *
 * General idea though is that more seldom queries, but in
 * burst-fashion is more effective use of queries than doing them more
 * often, but alone.  A burst will 'heat up' the network path
 * (switch/router caches, etc.).
 */
#define LWROC_NTP_NUM_BURST_QUERY  5

typedef struct lwroc_ntp_client_t
{
  lwroc_net_client_base _base;

  pd_ll_item _ntp_conns;

  struct timeval _next_time;

  lwroc_ntp_client_query _query[LWROC_NTP_NUM_BURST_QUERY];

  int _queries_active;
  int _queries_received;

  int _fit_status;

  int _wait_usec;
  int _backoff_sec;

  int _log;

  struct timeval _query_timeout;
  struct timeval _query_interval;

  /* The best values for the queries so far. */
  lwroc_ntp_client_leg _best_out;
  lwroc_ntp_client_leg _best_in;

  lwroc_timestamp_track_info _track;

  /* For error printing context: */
  /* Who are we talking to. */
  const char *_hostname;

} lwroc_ntp_client;

/********************************************************************/

extern lwroc_select_item_info lwroc_net_ntp_select_item_info;

/********************************************************************/

void lwroc_add_ntp_client(const char *cfg);

/********************************************************************/

typedef struct lwroc_ntp_sol_t
{
  lwroc_timestamp_track_sol _copy01;

} lwroc_ntp_sol;

typedef struct lwroc_ntp_global_sol_t
{
  lwroc_two_copy _two_copy;

  lwroc_ntp_sol _copy01[2];

} lwroc_ntp_global_sol;

extern lwroc_ntp_global_sol *_lwroc_ntp_global_sol;

/********************************************************************/

void lwroc_net_ntp_choose(lwroc_select_info *si);

/********************************************************************/

#endif/*__LWROC_NET_NTP_H__*/

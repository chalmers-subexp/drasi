/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_net_outgoing.h"
#include "lwroc_message.h"
#include "lwroc_message_internal.h"
#include "lwroc_main_iface.h"
#include "lwroc_hostname_util.h"
#include "lwroc_merge_struct.h"
#include "lwroc_net_reverse_link.h"
#include "lwroc_timeouts.h"
#include "lwroc_net_io_util.h"
#include "lwroc_net_badrequest.h"

#include "gen/lwroc_portmap_msg_serializer.h"
#include "gen/lwroc_request_msg_serializer.h"
#include "gen/lwroc_badrequest_serializer.h"
#include "gen/lwroc_reverse_link_ack_serializer.h"

#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <string.h>
#include <arpa/inet.h>
#include "../dtc_arch/acc_def/fall_through.h"

/********************************************************************/

PD_LL_SENTINEL(_lwroc_ext_net_out_wait_link_ack);

PD_LL_SENTINEL(_lwroc_ext_net_out_try_conn);

/********************************************************************/

void lwroc_net_outgoing_failed_setup_retry(lwroc_net_outgoing *conn,
					   struct timeval *now)
{
  conn->_state = LWROC_NET_OUTGOING_STATE_FAILED_SLEEP;
  conn->_next_time = *now;

  conn->_retry_timeout += 1;

  /* We let it take quite some time before we do slower attempts.
   * Since we want to be responsive when things actually are working.
   */

  if (conn->_retry_timeout > LWROC_CONNECTION_MAX_RETRY_INTERVAL * 10)
    conn->_retry_timeout = LWROC_CONNECTION_MAX_RETRY_INTERVAL * 10;

  conn->_next_time.tv_sec += 1 + conn->_retry_timeout / 10;
}

/********************************************************************/

int lwroc_net_outgoing_after_fail(lwroc_select_item *item,
				  lwroc_select_info *si)
{
  lwroc_net_outgoing *conn =
    PD_LL_ITEM(item, lwroc_net_outgoing, _base._select_item);

  /* Connection closed, remove it. */

  if (conn->_base._fd != -1)
    {
      lwroc_safe_close(conn->_base._fd);
      conn->_base._fd = -1;
    }

  lwroc_net_outgoing_failed_setup_retry(conn, &si->now);

  return 1;
}

/********************************************************************/

uint32_t _lwroc_next_link_ack_id = 1;

void lwroc_net_outgoing_prepare_request_msg(lwroc_net_outgoing *conn,
					    lwroc_select_info *si)
{
  lwroc_request_msg req_msg;

  /* Basing the ID on a mix of time that wraps (us) and counter is a
   * bad idea, since it may come up with the same value within a
   * second again...  Using two fields instead.  The counter being 32
   * bits it really is large enough to not wrap within any reasonable
   * time...
   */

  conn->_link_ack_id1 = _lwroc_next_link_ack_id++;
  conn->_link_ack_id2 = (uint32_t) (si->now.tv_usec + (si->now.tv_sec << 20));

  req_msg._request = conn->_connected_item_info->request;
  req_msg._orig_port = (uint16_t) _config._port;
  req_msg._link_ack_id1 = conn->_link_ack_id1;
  req_msg._link_ack_id2 = conn->_link_ack_id2;

  /*
  printf ("out request: %08x (port %d) %08x:%08x\n",
	  req_msg._request,
	  req_msg._orig_port,
	  req_msg._link_ack_id1,
	  req_msg._link_ack_id2);
  */

  conn->_base._buf._size = lwroc_request_msg_serialized_size();
  /* printf ("%zd %zd\n", conn->_buf._alloc, conn->_buf._size); */
  assert(sizeof (conn->_raw) >= conn->_base._buf._size);
  conn->_base._buf._ptr = (void *) conn->_raw;
  lwroc_request_msg_serialize(conn->_base._buf._ptr, &req_msg);
}

/********************************************************************/

int lwroc_net_outgoing_report(lwroc_net_outgoing *conn,
			      struct timeval *now,
			      uint32_t failure)
{
  if (conn->_last_failure != failure)
    {
      conn->_report_interval = 1;
      conn->_next_report = *now;
      conn->_next_report.tv_sec += conn->_report_interval;
      conn->_last_failure = failure;
      return 1;
    }

  if (timercmp(now, &conn->_next_report, <))
    return 0;

  conn->_report_interval =
    (conn->_report_interval < 5 ? 5 : conn->_report_interval * 2);
  conn->_next_report = *now;
  conn->_next_report.tv_sec += conn->_report_interval;

  return 1;
}

/********************************************************************/

void lwroc_net_outgoing_handle_badrequest(lwroc_net_outgoing *conn,
					  struct timeval *now)
{
  lwroc_badrequest badreq;
  lwroc_deserialize_error desererr;
  const char *end;

  end = lwroc_badrequest_deserialize(&badreq,
				     conn->_base._buf._ptr,
				     conn->_base._buf._size,
				     &desererr);

  if (end == NULL)
    {
      LWROC_ERROR_FMT("Bad request response malformed (%s, 0x%x, 0x%x).",
		      desererr._msg, desererr._val1, desererr._val2);
      return;
    }

  if (lwroc_net_outgoing_report(conn, now, badreq._failure))
    {
      lwroc_net_badrequest_report(&((&conn->_base)->_msg_context),
				  "Host response",
				  &badreq,
				  conn->_connected_item_info->rev_link_missing);

      conn->_last_failure = badreq._failure;
    }
}

/********************************************************************/

void lwroc_net_outgoing_connected(lwroc_net_outgoing *conn,
				  lwroc_select_info *si)
{
  /* We are successful, so clear last failure. */
  conn->_last_failure = 0;
  conn->_retry_timeout = 0;

  /* If we are handled by another thread, remove from our list of
   * connections.
   */
  if (!(conn->_connected_item_info->base.flags &
	LWROC_SELECT_ITEM_FLAG_OUTGOING_NET_THREAD))
    {
      PD_LL_REMOVE(&conn->_base._select_item._items);
    }
  if (conn->_state == LWROC_NET_OUTGOING_STATE_DATA_WAIT_LACK_OR_READ)
    {
      /* We are not an pending outgoing connection. */
      PD_LL_REMOVE(&conn->_wait_link_ack);
    }

  conn->_base._select_item.item_info = &conn->_connected_item_info->base;

  if (conn->_connected_item_info->connected)
    conn->_connected_item_info->connected(&conn->_base._select_item, si);
}

/********************************************************************/

void lwroc_net_outgoing_setup_select(lwroc_select_item *item,
				     lwroc_select_info *si)
{
  lwroc_net_outgoing *conn =
    PD_LL_ITEM(item, lwroc_net_outgoing, _base._select_item);

 switch_again:
  assert(conn->_state != 0);
  switch (conn->_state)
    {
    case LWROC_NET_OUTGOING_STATE_FAILED_SLEEP:
      if (conn->_last_immediate_time.tv_sec == (time_t) -1)
	{
	  /* We try connection again.  Now! */
	  conn->_last_immediate_time = si->now;
	}
      else if (!lwroc_select_info_time_passed_else_setup(si,
							 &conn->_next_time))
	break;
      /* Do not do outgoing connections until a log destination has
       * been set up.  Network thread wakes also itself up when flag
       * is set.  Thus another I/O round is done, which will start all
       * outgoing connections by coming here again.
       */
      LFENCE;
      if (!_lwroc_message_client_seen)
	break;
	/* Try connection again. */
      conn->_state = LWROC_NET_OUTGOING_STATE_PORTMAP_SETUP;
      /* Set the timeout up. */
      conn->_next_time = si->now;
      conn->_next_time.tv_sec += LWROC_OUTGOING_CLOSE_TIMEOUT;
      FALL_THROUGH;
    case LWROC_NET_OUTGOING_STATE_PORTMAP_SETUP:
    case LWROC_NET_OUTGOING_STATE_DATA_SETUP:
      if (!lwroc_net_client_base_create_socket(&conn->_base,
					       LWROC_SOCKET_TCP))
	{
	  /* Take some time, and then try again. */
	  lwroc_net_outgoing_failed_setup_retry(conn, &si->now);
	  goto switch_again;
	}
      conn->_state++; /* xxx_WAIT_CONNECTED */

      {
	struct sockaddr_storage *serv_addr;

	serv_addr = &conn->_base._addr;
	if (conn->_state == LWROC_NET_OUTGOING_STATE_DATA_WAIT_CONNECTED)
	  serv_addr = &conn->_data_addr;

	switch (lwroc_net_client_base_connect_socket (&conn->_base, serv_addr))
	  {
	  case 1:
	    conn->_state++; /* state after xxx_WAIT_CONNECTED */
	    break;
	  case 0:
	    break;
	  case -1:
	    /* Take some time, and then try again. */
	    lwroc_net_outgoing_failed_setup_retry(conn, &si->now);
	    break;
	  }
      }

      /* What to do depends on how far we got. */
      goto switch_again;

    case LWROC_NET_OUTGOING_STATE_PORTMAP_READ_PREPARE:
      conn->_base._buf._size = lwroc_portmap_msg_serialized_size();
      goto common_prepare_read;
#if 0
    case LWROC_NET_OUTGOING_STATE_DATA_CHUNK_READ_PREPARE:
      /* We have gotten the first packet OK, so restart any error
       * reporting.  Also, restart quick reconnect attempts.
       */
      conn->_last_failure = 0;
      conn->_retry_timeout = 0;
#endif
    case LWROC_NET_OUTGOING_STATE_DATA_FIRST_READ_PREPARE:
      conn->_base._buf._size = sizeof (lwroc_msg_header_wire);
      goto common_prepare_read;
    common_prepare_read:
      /* reset the receive pointer */
      conn->_base._buf._ptr = (char *) conn->_raw;
      conn->_base._buf._offset = 0;
      conn->_state++; /* xxx_READ */
      FALL_THROUGH;
    case LWROC_NET_OUTGOING_STATE_PORTMAP_READ:
    case LWROC_NET_OUTGOING_STATE_DATA_WAIT_LACK_OR_READ:
    case LWROC_NET_OUTGOING_STATE_DATA_FIRST_READ:
      LWROC_READ_FD_SET(conn->_base._fd, si);
      break;

    case LWROC_NET_OUTGOING_STATE_DATA_REQ_WRITE_PREPARE:
      lwroc_net_outgoing_prepare_request_msg(conn, si);
      goto common_prepare_write;
    case LWROC_NET_OUTGOING_STATE_DATA_LACK_WRITE_PREPARE:
      /* Message data already prepared. */
      goto common_prepare_write;
    common_prepare_write:
      /* reset the send pointer */
      conn->_base._buf._offset = 0;
      conn->_state++; /* xxx_WRITE */
      FALL_THROUGH;
    case LWROC_NET_OUTGOING_STATE_PORTMAP_WAIT_CONNECTED:
    case LWROC_NET_OUTGOING_STATE_DATA_WAIT_CONNECTED:
    case LWROC_NET_OUTGOING_STATE_DATA_REQ_WRITE:
    case LWROC_NET_OUTGOING_STATE_DATA_LACK_WRITE:
      LWROC_WRITE_FD_SET(conn->_base._fd, si);
      break;

    case LWROC_NET_OUTGOING_STATE_DATA_WAIT_LACK_PREP_READ:
      conn->_base._buf._size = sizeof (lwroc_msg_header_wire);
      goto common_prepare_read;

    case LWROC_NET_OUTGOING_STATE_SHUTDOWN:
      /* Make sure it gets handled immediately, set timeout to 0 */
      lwroc_select_info_set_timeout_now(si);
      break;
    }

  /* In all cases, we set up a time-out. */

  lwroc_select_info_time_setup(si, &conn->_next_time);
}

int lwroc_net_outgoing_after_select(lwroc_select_item *item,
				    lwroc_select_info *si)
{
  lwroc_net_outgoing *conn =
    PD_LL_ITEM(item, lwroc_net_outgoing, _base._select_item);

  /* printf ("out (%p), after_select: %d\n", conn, conn->_state); */

  if (timercmp(&si->now, &conn->_next_time, >))
    {
      if (conn->_state == LWROC_NET_OUTGOING_STATE_DATA_WAIT_LACK_OR_READ)
	{
	  LWROC_CINFO(&conn->_base,
		      "Timeout waiting for link ack message to send on "
		      "outgoing connection (should come via reverse link).");

	  /* If we are waiting for an LACK, we instead try to
	   * see if there perhaps was an error message.
	   *
	   * Might have been that it was sent long ago, but it
	   * complicates the code to handle input at the same time as
	   * output.  And for this process it does not matter.  An
	   * error just leads to us going for retry again.
	   */
	  conn->_state = LWROC_NET_OUTGOING_STATE_DATA_FIRST_READ_PREPARE;
	  /* We are no longer waiting for LANK messages. */
	  PD_LL_REMOVE(&conn->_wait_link_ack);

	  /* Move the timeout forward.  By one second only, as any
	   * message, if there, already should be present.
	   */
	  conn->_next_time = si->now;
	  conn->_next_time.tv_sec += 1;
	}
      else if (conn->_state != LWROC_NET_OUTGOING_STATE_FAILED_SLEEP &&
	       conn->_state != LWROC_NET_OUTGOING_STATE_SHUTDOWN)
	{
	  /* Give up on this connection. */
	  LWROC_CINFO(&conn->_base,
		      "Timeout waiting for outgoing link establishment.");
	  return 0;
	}
    }

 switch_again:
  assert(conn->_state != 0);
  switch (conn->_state)
    {
    case LWROC_NET_OUTGOING_STATE_PORTMAP_READ:
    case LWROC_NET_OUTGOING_STATE_DATA_FIRST_READ:
    case LWROC_NET_OUTGOING_STATE_DATA_WAIT_LACK_OR_READ:
      if (!LWROC_READ_FD_ISSET(conn->_base._fd, si))
	break;

      if (!lwroc_net_client_base_read(&conn->_base, "outgoing",
				      &_lwroc_mon_net._recv_misc))
	return 0;

      if (conn->_state == LWROC_NET_OUTGOING_STATE_DATA_WAIT_LACK_OR_READ &&
	  conn->_base._buf._offset != 0)
	{
	  /* We did read something.  Meaning that we cannot really
	   * hope to get an LACK, as that must come to us before the
	   * server responds after we have sent it (which we cannot
	   * have done yet...)
	   *
	   * So, give up on getting an LACK, and continue / finish
	   * reading the response message instead.
	   */
	  conn->_state = LWROC_NET_OUTGOING_STATE_DATA_FIRST_READ;
	  /* We are no longer waiting for LANK messages. */
	  PD_LL_REMOVE(&conn->_wait_link_ack);
	}

      if (conn->_base._buf._offset < conn->_base._buf._size)
	{
	  /* we've not reached end of buffer yet */
	  break;
	}
      conn->_state++;
      goto switch_again;

    case LWROC_NET_OUTGOING_STATE_PORTMAP_READ_CHECK:
      assert(conn->_base._buf._offset == lwroc_portmap_msg_serialized_size());
    /* So we have read the portmap message. */
    {
      lwroc_portmap_msg pmap_msg;
      lwroc_deserialize_error desererr;
      const char *end;

      end = lwroc_portmap_msg_deserialize(&pmap_msg,
					  conn->_base._buf._ptr,
					  conn->_base._buf._offset,
					  &desererr);

      if (end == 0)
	{
	  if (lwroc_net_outgoing_report(conn, &si->now,
					LWROC_NET_OUT_FAILURE_BAD_PORTMAP))
	    LWROC_CWARNING_FMT(&conn->_base,
			       "Malformed portmap response (%s, 0x%x, 0x%x).",
			       desererr._msg, desererr._val1, desererr._val2);
	  /*
	  LWROC_WARNING_FMT("Bad magic (0x%08x) in portmap response.",
		  ntohl(pmap_msg->_header._magic));
	  LWROC_WARNING_FMT("Bad size (%d) in portmap response.",
		  ntohl(pmap_msg->_header._msg_size));
	  */
	  return 0;
	}

      /* Close the portmap connection. */
      lwroc_safe_close(conn->_base._fd);
      conn->_base._fd = -1;

      lwroc_copy_sockaddr_port(&conn->_data_addr,
			       &conn->_base._addr,
			       pmap_msg._port);

      conn->_state = LWROC_NET_OUTGOING_STATE_DATA_SETUP;
      break;
    }
    case LWROC_NET_OUTGOING_STATE_DATA_FIRST_READ_CHECK:
    {
      lwroc_msg_header_wire *header =
	(lwroc_msg_header_wire *) conn->_base._buf._ptr;

      /* First, have we read the entire message? */
      uint32_t size;
      uint32_t magic;

      size = ntohl(header->_msg_size);

      if (size != conn->_base._buf._size)
	{
	  /* printf ("-- %d %d\n", size, (int) conn->_base._buf._size); */

	  if (size < conn->_base._buf._size) {
	    /* Can only happen first time. */
	    if (lwroc_net_outgoing_report(conn, &si->now,
					  LWROC_NET_OUT_FAILURE_BAD_SIZE))
	      LWROC_CWARNING_FMT(&conn->_base,
				 "Bad size (%d) in first response.",
				 size);
	    return 0;
	  }
	  if (size > 0x10000) {
	    if (lwroc_net_outgoing_report(conn, &si->now,
					  LWROC_NET_OUT_FAILURE_BAD_SIZE_LARGE))
	      LWROC_CWARNING_FMT(&conn->_base,
				 "Cowardly refusing "
				 "large size (%d) of response.",
				 size);
	    return 0;
	  }
	  if (size > sizeof (conn->_raw)) {
	    /* TODO: Hmmm, also lwroc_net_outgoing_report check?
	     * Why no return 0? */
	    LWROC_CWARNING_FMT(&conn->_base,
			       "Response from outgoing connection "
			       "larger (%d) than maximum.",
			       size);
	  }
#if 0
	  /* We need more(?) space. */
	  if (conn->_base._buf._alloc < size)
	    lwrocmon_realloc_buffer(&conn->_base._buf, size);
#endif
	  conn->_base._buf._size = size;
	  conn->_state--; /* go back to reading */
	  break;
	}

      /* printf (":: %d\n", size); */

      magic = ntohl(header->_magic);

#if 0
      /* If nothing goes bad, next state is to read another chunk. */
      conn->_state = LWROC_NET_OUTGOING_STATE_DATA_CHUNK_READ_PREPARE;
#endif

#if STATE_MACHINE_DEBUG
      printf ("%x\n", magic);
#endif

      switch (magic)
	{
	case LWROC_BADREQUEST_SERPROTO_MAGIC1:
	  /* bad request */
	  lwroc_net_outgoing_handle_badrequest(conn, &si->now);
	  return 0; /* connection shall die */

#if 0
	case LWROC_MESSAGE_SOURCE_SERPROTO_MAGIC1:
	  if (!_config._log_filename)
	    goto unexpected_magic;
	  return lwrocmon_handle_message_source(conn);

	case LWROC_MESSAGE_ITEM_SERPROTO_MAGIC1:
	  if (!_config._log_filename)
	    goto unexpected_magic;
	  return lwrocmon_handle_message_item(conn);

	case LWROC_MONITOR_SOURCE_HEADER_SERPROTO_MAGIC1:
	  if (_config._log_filename)
	    goto unexpected_magic;
	  return lwrocmon_handle_monitor_source(conn);

	case LWROC_MONITOR_BLOCK_HEADER_SERPROTO_MAGIC1:
	  if (_config._log_filename)
	    goto unexpected_magic;
	  return lwrocmon_handle_monitor_item(conn);
#endif

	default:
	  if (lwroc_net_outgoing_report(conn, &si->now,
					LWROC_NET_OUT_FAILURE_BAD_MAGIC))
	    LWROC_CWARNING_FMT(&conn->_base,
			       "Bad magic (0x%08x) in response.", magic);
	  return 0;

	  /*
	unexpected_magic:
	  LWROC_WARNING_FMT("Unexpected magic (0x%08x) in response.", magic);
	  return 0;
	  */
	}
      break;
    }
    case LWROC_NET_OUTGOING_STATE_PORTMAP_WAIT_CONNECTED:
    case LWROC_NET_OUTGOING_STATE_DATA_WAIT_CONNECTED:
    {
      int so_error;

      if (!LWROC_WRITE_FD_ISSET(conn->_base._fd, si))
	break;

      /* So the connect has finished, figure out if it was successful. */

      if (!lwroc_net_client_base_connect_get_status(&conn->_base, &so_error))
	{
	  if (lwroc_net_outgoing_report(conn, &si->now,
					LWROC_NET_OUT_FAILURE_DELAYED_CONNECT))
	    LWROC_CWARNING_FMT(&conn->_base,"Delayed connect: %s",
			       strerror(so_error));
	  return 0; /* connection failed */
	}

      conn->_state++; /* state after xxx_WAIT_CONNECTED */
      /* Next state shall either read or write. */

      /*conn->_mode = LWROC_NET_OUTGOING_STATE_SEND_ORDERS;*/
      /* must wait for allowance to write, break */
      break;
    }

    case LWROC_NET_OUTGOING_STATE_DATA_REQ_WRITE:
    case LWROC_NET_OUTGOING_STATE_DATA_LACK_WRITE:
      if (!LWROC_WRITE_FD_ISSET(conn->_base._fd, si))
	break;

      if (!lwroc_net_client_base_write(&conn->_base, "outgoing",
				       &_lwroc_mon_net._sent_misc))
	return 0;

      if (conn->_base._buf._offset < conn->_base._buf._size)
	{
	  /* we've not reached end of buffer yet */
	  break;
	}
      if (conn->_state == LWROC_NET_OUTGOING_STATE_DATA_REQ_WRITE &&
	  conn->_connected_item_info->request == LWROC_REQUEST_REVERSE_LINK)
	{
	  lwroc_net_outgoing_connected(conn, si);
	  break;
	}
      if (conn->_state == LWROC_NET_OUTGOING_STATE_DATA_LACK_WRITE &&
	  (conn->_connected_item_info->request == LWROC_REQUEST_DATA_TRANS ||
	   conn->_connected_item_info->request == LWROC_REQUEST_TRIVAMI_BUS ||
	   conn->_connected_item_info->request == LWROC_REQUEST_TRIVAMI_EB))
	{
	  lwroc_net_outgoing_connected(conn, si);
	  break;
	}
      conn->_state++;
      if (conn->_state == LWROC_NET_OUTGOING_STATE_DATA_WAIT_LACK_PREP_READ)
	{
	  /* Add to list of waiting connections. */
	  PD_LL_ADD_BEFORE(&_lwroc_ext_net_out_wait_link_ack,
			   &conn->_wait_link_ack);
	}
      break;
    }

  return 1;
}

/********************************************************************/

lwroc_select_item_info lwroc_net_outgoing_select_item_info =
{
  lwroc_net_outgoing_setup_select,
  lwroc_net_outgoing_after_select,
  lwroc_net_outgoing_after_fail,
  NULL,
  0,
};

/********************************************************************/

extern PD_LL_SENTINEL_ITEM(_lwroc_net_clients);

void lwroc_net_outgoing_init(lwroc_net_outgoing *conn,
			     lwroc_outgoing_select_item_info *connected_item_info)
{
  /* Timeout is set to 0 absolute time, so will immediately go to
   * LWROC_NET_OUTGOING_STATE_PORTMAP_SETUP on setup_select.
   */
  conn->_state = LWROC_NET_OUTGOING_STATE_FAILED_SLEEP;
  conn->_base._fd = -1;

  conn->_connected_item_info = connected_item_info;

  lwroc_net_become_outgoing(conn, NULL);
}

/********************************************************************/

void lwroc_net_become_outgoing(lwroc_net_outgoing *conn,
			       lwroc_select_info *si)
{
  /* This is called every time an outgoing connection is returned from
   * their production handling here to be re-established.
   */

  conn->_base._select_item.item_info =
    &lwroc_net_outgoing_select_item_info;

  /* Set it up for retry, unless initial init. */

  if (si != NULL)
    lwroc_net_outgoing_failed_setup_retry(conn, &si->now);

  /* Only insert into the list if normally handled by another thread,
   * or initial.
   */
  if (!(conn->_connected_item_info->base.flags &
	LWROC_SELECT_ITEM_FLAG_OUTGOING_NET_THREAD) ||
      si == NULL)
    {
      /* Insert into the list. */
      PD_LL_ADD_BEFORE(&_lwroc_net_clients, &conn->_base._select_item._items);
    }

  /* We are an outgoing, not yet connected... */
  PD_LL_ADD_BEFORE(&_lwroc_ext_net_out_try_conn, &conn->_try_conn);
}

/********************************************************************/

void lwroc_reverse_link_ack_received(const lwroc_reverse_link_ack *link_ack,
				     struct sockaddr_storage *from_addr,
				     uint16_t orig_port)
{
  pd_ll_item *iter, *tmp_iter;

  /* Go through the list of outgoing connections waiting for a link
   * acknowledge, and see if we find the matching item.  (If there had
   * been a list of outgoing connections, we could also have checked
   * them all, checking for state
   * LWROC_NET_OUTGOING_STATE_DATA_WAIT_LACK.)
   *
   * If there is no matching connection, it means that the outgoing
   * connection has already given up, or that it is an unsolicited
   * link acknowledge.  In any case, just ignore it.
   *
   * This is the critical function when it comes to mismatching bad
   * link acknowledges.  If we pick the wrong one and send that, the
   * other end of our outgoing connection will just drop us, due to a
   * bad link acknowledge.
   */

  /*
  printf ("reverse_link_ack_received: %08x:%08x : %08x:%08x\n",
	  link_ack->_link_ack_id1,
	  link_ack->_link_ack_id2,
	  link_ack->_link_ack_rid,
	  link_ack->_link_ack_token);
  */

  PD_LL_FOREACH_TMP(_lwroc_ext_net_out_wait_link_ack, iter, tmp_iter)
    {
      lwroc_net_outgoing *conn =
	PD_LL_ITEM(iter, lwroc_net_outgoing, _wait_link_ack);

      /* Cheaper to check the id first. */

      if (conn->_link_ack_id1 != link_ack->_link_ack_id1 ||
	  conn->_link_ack_id2 != link_ack->_link_ack_id2)
	continue;

      /* Check that it comes from the correct host. */

      if (!lwroc_compare_sockaddr_pb(&conn->_base._addr,
				     from_addr,
				     orig_port))
	continue;

      /*
      printf ("reverse_link_ack_received: match! (%d)\n",
	      conn->_state);
      */

      /* Found a match. */
      PD_LL_REMOVE(&conn->_wait_link_ack);
      conn->_state++; /* */

      /* Prepare the message. */
      conn->_base._buf._size = lwroc_reverse_link_ack_serialized_size();
      assert (sizeof (conn->_raw) >= conn->_base._buf._size);
      conn->_base._buf._ptr = (void *) conn->_raw;

      lwroc_reverse_link_ack_serialize(conn->_base._buf._ptr,
				       link_ack);

      /* In principle, the link ack should only apply to one
       * connection.  But as it does not complicate the code
       * in any way, we check all waiting connections.
       */
    }
}

/********************************************************************/

void lwroc_awaken_retry_outgoing(struct sockaddr_storage *from_addr,
				 uint16_t orig_port,
				 struct timeval *now)
{
  pd_ll_item *iter;
  struct timeval limit;

  /* printf ("check awaken (%d)\n", orig_port); */

  /* It is cheaper to compare the times before the network addresses,
   * so do that.
   */

  limit = *now;
  limit.tv_sec -= LWROC_CONNECTION_MIN_IMMEDIATE_RETRY_INTERVAL;

  PD_LL_FOREACH(_lwroc_ext_net_out_try_conn, iter)
    {
      lwroc_net_outgoing *conn =
	PD_LL_ITEM(iter, lwroc_net_outgoing, _try_conn);

      if (conn->_last_immediate_time.tv_sec == (time_t) -1)
	continue; /* Already pending. */

      /* We will not issue request if last immediate connection
       * was not too long ago.
       */

      if (timercmp(&limit, &conn->_last_immediate_time, <))
	continue;

      /* Is it the correct address? */

      if (!lwroc_compare_sockaddr_pb(&conn->_base._addr,
				     from_addr,
				     orig_port))
	continue;

      /* Please try again. */

      conn->_last_immediate_time.tv_sec = (time_t) -1;

      /* printf ("woke up!\n"); */
    }
}

/********************************************************************/

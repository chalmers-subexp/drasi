/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_NET_OUTGOING_H__
#define __LWROC_NET_OUTGOING_H__

#include "lwroc_net_client_base.h"

#include "../dtc_arch/acc_def/netinet_in_include.h"

/********************************************************************/

#define LWROC_NET_OUTGOING_STATE_PORTMAP_SETUP             1
#define LWROC_NET_OUTGOING_STATE_PORTMAP_WAIT_CONNECTED    2
#define LWROC_NET_OUTGOING_STATE_PORTMAP_READ_PREPARE      3
#define LWROC_NET_OUTGOING_STATE_PORTMAP_READ              4
#define LWROC_NET_OUTGOING_STATE_PORTMAP_READ_CHECK        5

#define LWROC_NET_OUTGOING_STATE_DATA_SETUP                6
#define LWROC_NET_OUTGOING_STATE_DATA_WAIT_CONNECTED       7
#define LWROC_NET_OUTGOING_STATE_DATA_REQ_WRITE_PREPARE    8
#define LWROC_NET_OUTGOING_STATE_DATA_REQ_WRITE            9
#define LWROC_NET_OUTGOING_STATE_DATA_WAIT_LACK_PREP_READ 10
#define LWROC_NET_OUTGOING_STATE_DATA_WAIT_LACK_OR_READ   11
#define LWROC_NET_OUTGOING_STATE_DATA_LACK_WRITE_PREPARE  12
#define LWROC_NET_OUTGOING_STATE_DATA_LACK_WRITE          13
#define LWROC_NET_OUTGOING_STATE_DATA_FIRST_READ_PREPARE  14
#define LWROC_NET_OUTGOING_STATE_DATA_FIRST_READ          15
#define LWROC_NET_OUTGOING_STATE_DATA_FIRST_READ_CHECK    16

#define LWROC_NET_OUTGOING_STATE_FAILED_SLEEP             17
#define LWROC_NET_OUTGOING_STATE_SHUTDOWN                 18

/* In addition to the LWROC_BADREQUEST_ constants: */
#define LWROC_NET_OUT_FAILURE_DELAYED_CONNECT              1
#define LWROC_NET_OUT_FAILURE_BAD_MAGIC                    2
#define LWROC_NET_OUT_FAILURE_BAD_PORTMAP                  3
#define LWROC_NET_OUT_FAILURE_BAD_SIZE                     4
#define LWROC_NET_OUT_FAILURE_BAD_SIZE_LARGE               5
#define LWROC_NET_OUT_FAILURE_BAD_SIZE_MAX_OVERFLOW        6


typedef struct lwroc_net_outgoing_t
{
  lwroc_net_client_base _base;

  int _state;

  /* Timeout to retry connection. */
  int _retry_timeout;

  /* Next attempt. */
  struct timeval _next_time;

  /* Reason for most recent failure. */
  uint32_t _last_failure;

  /* Last report of failure. */
  struct timeval _next_report;
  int _report_interval;

  /* Where to connect. */
  struct sockaddr_storage _data_addr;

  /* What to become when connected. */
  lwroc_outgoing_select_item_info *_connected_item_info;

  /* Link ack id that we sent and expect back. */
  uint32_t _link_ack_id1;
  uint32_t _link_ack_id2;
  /* List item as we are waiting for a link acknowledge. */
  pd_ll_item _wait_link_ack;

  /* List item as we are part of the outgoing attempts. */
  pd_ll_item _try_conn;
  /* Last time we made an immediate reconnection attempt. */
  /* tv_sec == -1, try immediately again on failure */
  struct timeval _last_immediate_time;

  /* For error printing context: */
  /* Who are we talking to. */
  const char *_hostname;

  /* Array below is large enough to hold an outgoing request,
   * and an outgoing badrequest response, and an lack confirmation.
   */
  uint32_t          _raw[7];
} lwroc_net_outgoing;

/********************************************************************/

extern lwroc_select_item_info lwroc_net_outgoing_select_item_info;

/********************************************************************/

void lwroc_net_outgoing_init(lwroc_net_outgoing *conn,
			     lwroc_outgoing_select_item_info *connected_item_info);

void lwroc_net_become_outgoing(lwroc_net_outgoing *conn,
			       lwroc_select_info *si);

/********************************************************************/

void lwroc_net_outgoing_failed_setup_retry(lwroc_net_outgoing *conn,
					   struct timeval *now);

/********************************************************************/

void lwroc_reverse_link_ack_received(const lwroc_reverse_link_ack *link_ack,
				     struct sockaddr_storage *from_addr,
				     uint16_t orig_port);

/********************************************************************/

void lwroc_awaken_retry_outgoing(struct sockaddr_storage *from_addr,
				 uint16_t orig_port,
				 struct timeval *now);

/********************************************************************/

#endif/*__LWROC_NET_OUTGOING_H__*/

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_net_portmap.h"
#include "lwroc_message.h"
#include "lwroc_timeouts.h"

#include <errno.h>

lwroc_msg_wire _lwroc_portmap_msg = { NULL, 0 };

/********************************************************************/

void lwroc_net_portmap_setup_select(lwroc_select_item *item,
				    lwroc_select_info *si)
{
  lwroc_portmap_client *portmap =
    PD_LL_ITEM(item, lwroc_portmap_client, _base._select_item);

  /* Timeout. */

  lwroc_select_info_time_setup(si, &portmap->_next_time);

  if (portmap->_base._buf._offset < portmap->_base._buf._size)
    {
      /* It would like to write. */

      LWROC_WRITE_FD_SET(portmap->_base._fd, si);
    }
  else
    {
      /* Wait for the client to do the disconnect. */

      LWROC_READ_FD_SET(portmap->_base._fd, si);
    }
}

int lwroc_net_portmap_after_select(lwroc_select_item *item,
				   lwroc_select_info *si)
{
  lwroc_portmap_client *portmap =
    PD_LL_ITEM(item, lwroc_portmap_client, _base._select_item);

  /* If we have waited for some small time, we give up!
   *
   * Like below, giving up means that we are the ones that cannot
   * bind to the port if restarted.  But what to do if whoever
   * connected to this port refuses to shut the connection down?
   */

  if (timercmp(&si->now, &portmap->_next_time, >))
    return 0;

  if (portmap->_base._buf._offset < portmap->_base._buf._size)
    {
      if (!LWROC_WRITE_FD_ISSET(portmap->_base._fd, si))
	return 1;

      if (!lwroc_net_client_base_write(&portmap->_base, "portmap",
				       &_lwroc_mon_net._sent_misc))
	return 0;
    }
  else
    {
      /* We have sent our info.  Continue until file handle is ready
       * for reading.  Such that the client does the shutdown, and the
       * client thus gets the timeout, and we don't.
       *
       * We consume a bit of (unexpected) data to not be the ones
       * closing first.  But after say a kB or so, we give up and
       * close forcefully.
       */

      if (!lwroc_discard_read_check_eof(portmap->_base._fd, si,
					&portmap->_base._buf._offset))
	return 0;

      if (portmap->_base._buf._offset >= portmap->_base._buf._size + 1000)
	return 0;
    }

  return 1;
}

/********************************************************************/

lwroc_select_item_info lwroc_net_portmap_select_item_info =
{
  lwroc_net_portmap_setup_select,
  lwroc_net_portmap_after_select,
  lwroc_net_client_base_after_fail,
  lwroc_net_client_base_delete,
  0,
};

/********************************************************************/

void lwroc_net_become_portmap(lwroc_portmap_client *portmap,
			      lwroc_select_info *si)
{
  portmap->_base._buf._size = _lwroc_portmap_msg._size;
  portmap->_base._buf._ptr  = _lwroc_portmap_msg._wire;
  portmap->_base._buf._offset = 0;

  portmap->_next_time = si->now;
  portmap->_next_time.tv_sec += LWROC_PORTMAP_CLOSE_TIMEOUT;
}

/********************************************************************/

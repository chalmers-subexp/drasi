/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_NET_PROTO_H__
#define __LWROC_NET_PROTO_H__

#include "../dtc_arch/acc_def/mystdint.h"

/* Hints for lwroc_make_proto_serialization.pl. */
#define LWROC_PRINT_CTRL_STATIC(x)        /* use double (( )) */
#define LWROC_PRINT_CTRL(x)               /* use single ( )   */
/* The use of double (( )) is to let the generation script find
 * the end as )), without having to place a ; which upsets
 * some compilers.  Not very beautiful.
 */

/********************************************************************/

/* Marker for generation script, eat. */

#define LWROC_SERIALIZE_ENVELOPE_SIZE(x)  /* use double (( )) */

/********************************************************************/

typedef struct lwroc_msg_header_wire_t
{
  uint32_t _msg_size;
  uint32_t _magic;
} lwroc_msg_header_wire;

typedef struct lwroc_msg_footer_wire_t
{
  uint32_t _chksum;
} lwroc_msg_footer_wire;

/********************************************************************/

#define LWROC_NET_DEFAULT_PORT               56583

typedef struct lwroc_portmap_msg_t
{
  uint16_t _port;
} lwroc_portmap_msg;

/********************************************************************/

#define LWROC_REQUEST_MESSAGES              0x4d455347  /* MESG */
#define LWROC_REQUEST_MONITOR               0x4d4f4e49  /* MONI */
#define LWROC_REQUEST_CONTROL               0x4354524c  /* CTRL */
#define LWROC_REQUEST_DATA_TRANS            0x44415441  /* DATA */
#define LWROC_REQUEST_TRIVAMI_BUS           0x424f5353  /* BOSS */
#define LWROC_REQUEST_TRIVAMI_EB            0x4f524348  /* ORCH */
#define LWROC_REQUEST_REVERSE_LINK          0x524e4c4b  /* RLNK */

typedef struct lwroc_request_msg_t
{
  uint32_t _request;
  uint16_t _orig_port;
  uint32_t _link_ack_id1;
  uint32_t _link_ack_id2;
} lwroc_request_msg;

/********************************************************************/

#define LWROC_BADREQUEST_BAD_MAGIC          0x424d4743  /* BMGC */
#define LWROC_BADREQUEST_BAD_SIZE           0x4244535a  /* BDSZ */
#define LWROC_BADREQUEST_BAD_REQUEST        0x42445251  /* BDRQ */
#define LWROC_BADREQUEST_NONCFG_REQUEST     0x4e434647  /* NCFG */
#define LWROC_BADREQUEST_MULTIPLE_RECEIVER  0x4d524356  /* MRCV */
#define LWROC_BADREQUEST_IN_USE             0x494e5553  /* INUS */
#define LWROC_BADREQUEST_NO_REVERSE_LINK    0x4e4f524c  /* NORL */
#define LWROC_BADREQUEST_BAD_REVERSE_LINK   0x4244524c  /* BDRL */
#define LWROC_BADREQUEST_WAIT_LOG_CLIENT    0x574e4c43  /* WNLC */
#define LWROC_BADREQUEST_SHUTDOWN           0x53485554  /* SHUT */

#define LWROC_BADREVLNK_HINT_NOT_FOUND      0x524c4e46  /* RLNF */

typedef struct lwroc_badrequest_t
{
  uint32_t _failure;
  uint32_t _hint;
  uint32_t _seen_ipv4_addr;
} lwroc_badrequest;

/********************************************************************/

typedef struct lwroc_reverse_link_ack_t
{
  uint32_t _link_ack_id1;
  uint32_t _link_ack_id2;
  uint32_t _link_ack_rid;
  uint32_t _link_ack_token;
} lwroc_reverse_link_ack;

/********************************************************************/

#define LWROC_DATA_TRANSPORT_FORMAT_LMD     0x4c4d442e  /* LMD. */
#define LWROC_DATA_TRANSPORT_FORMAT_EBYE    0x45425945  /* EBYE */
#define LWROC_DATA_TRANSPORT_FORMAT_XFER    0x58464552  /* XFER */

typedef struct lwroc_data_transport_setup_t
{
  uint32_t _max_buf_size;
  uint32_t _max_ev_len;
  uint32_t _max_event_interval;
  uint32_t _data_format;
} lwroc_data_transport_setup;

/********************************************************************/

typedef struct lwroc_item_time_t
{
  uint64_t _sec;
  uint32_t _nsec;
} lwroc_item_time;

/********************************************************************/

#define LWROC_CONTROL_TOKEN_STAT_TOKEN      0x544f4b4e  /* TOKN */
#define LWROC_CONTROL_TOKEN_STAT_UNKNOWN    0x554e4b4e  /* UNKN */
#define LWROC_CONTROL_TOKEN_STAT_CHALLENGE  0x43484c47  /* CHLG */
#define LWROC_CONTROL_TOKEN_STAT_RESPONSE   0x52455350  /* RESP */
#define LWROC_CONTROL_TOKEN_STAT_OK         0x2e4f4b2e  /* .OK. */
#define LWROC_CONTROL_TOKEN_STAT_BAD        0x2e424144  /* .BAD */

typedef struct lwroc_control_token_t
{
  uint32_t _status;
  uint32_t _op;
  uint32_t _token;
  uint32_t _token_challenge_response;
} lwroc_control_token;

#define LWROC_CONTROL_OP_DEBUG_MASK         0x4442474d  /* DBGM */
#define LWROC_CONTROL_OP_UNLIMIT_LOG        0x554e4c4c  /* UNLL */
#define LWROC_CONTROL_OP_FILE               0x46494c45  /* FILE */
#define LWROC_CONTROL_OP_ACQ_READOUT        0x41435152  /* ACQR */
#define LWROC_CONTROL_OP_MERGE              0x4d455247  /* MERG */
#define LWROC_CONTROL_OP_USER               0x55534552  /* USER */
#define LWROC_CONTROL_OP_DONE               0x444f4e45  /* DONE */

typedef struct lwroc_control_request_t
{
  uint32_t _tag;
  uint32_t _op;
  uint32_t _value;
  uint32_t _request_data_size;
  uint32_t _response_data_maxsize;
} lwroc_control_request;

/* Request data follows serialised message, as raw bytes.  Aligned to
 * 4 bytes.  The maxsize for the response tells how large the response
 * may be.
 */

/********************************************************************/

/* Error codes marked 'u' may be returned by the process being
 * controlled, in which case the response block still is valid (even
 * for RSSZ which may still not overflow!)
 */

/* Internal: ready to continue. */
#define LWROC_CONTROL_STATUS_READY          0x2e524459  /* .RDY   -        */
/* Internal: ready to continue with data (max size given). */
#define LWROC_CONTROL_STATUS_DATA_SIZE      0x2e44535a  /* .DSZ   -        */
/* No token negotiated */
#define LWROC_CONTROL_STATUS_TOKEN_NONE     0x4e544b4e  /* NTKN   EACCES   */
/* (Previous) negotiated token is for the wrong op */
#define LWROC_CONTROL_STATUS_TOKEN_WRONG_OP 0x57544b4e  /* WTKN   EACCES   */
/* Other control client in progress. */
#define LWROC_CONTROL_STATUS_TMP_BUSY       0x42555359  /* BUSY   EBUSY    */
/* Requested message size (+ response) too large. */
#define LWROC_CONTROL_STATUS_TOO_BIG        0x32424947  /* 2BIG   E2BIG    */
/* Unknown / bad control request. */
#define LWROC_CONTROL_STATUS_NO_HANDLER     0x4e4f4844  /* NOHD   ENOTSUP  */
/* Unknown status. */
#define LWROC_CONTROL_STATUS_UNKNOWN        0x554e4b4e  /* UNKN   EFAULT   */
/* Successful! */
#define LWROC_CONTROL_STATUS_SUCCESS        0x2e4f4b2e  /* .OK. u -        */
/* Invalid control request (performer side). */
#define LWROC_CONTROL_STATUS_INVALID        0x494e564c  /* INVL u EINVAL   */
/* Denied control request. */
#define LWROC_CONTROL_STATUS_DENIED         0x44454e59  /* DENY u EACCES   */
/* Failed control request. */
#define LWROC_CONTROL_STATUS_FAILED         0x4641494c  /* FAIL u EFAULT   */
/* Already done, cannot do. */
#define LWROC_CONTROL_STATUS_ALREADY        0x414c5259  /* ALRY u EALREADY */
/* Shutdonw in process. */
#define LWROC_CONTROL_STATUS_SHUTDOWN       0x53485444  /* SHTD u ESHUTDOWN */
/* Response too large (performer side) */
#define LWROC_CONTROL_STATUS_RESP_TOO_LARGE 0x5253535a  /* RSSZ u ENOMEM   */
/* Performer broke the control buffer protocol. */
#define LWROC_CONTROL_STATUS_PERFORMER_FAIL 0x5046464c  /* CLFL   EIO      */
/* The following unconditionally closes the socket connection. */
/* The client (i.e. other end of network) broke the protocol. */
#define LWROC_CONTROL_STATUS_PROTOCOL_ERROR 0x50524552  /* PRER   EPROTO   */


/* Specifier for from where the error message originates. */

#define LWROC_CONTROL_STAT_ORIG_SERVER         0x53525652  /* SRVR */
#define LWROC_CONTROL_STAT_ORIG_PERFORMER      0x50455246  /* PERF */
#define LWROC_CONTROL_STAT_ORIG_SRV_POST_PERF  0x53505046  /* SPPF */

typedef struct lwroc_control_response_t
{
  uint32_t _status;
  uint32_t _tag;
  uint32_t _op;
  uint32_t _value;
  uint32_t _response_data_size;
  uint32_t _origin;
  /* Much easier for control client if this member has static size. */
  char     _err_msg[256];
} lwroc_control_response;

/* Response data follows serialised message, as for the request
 * above.
 */

/********************************************************************/

/* This is used as the payload for a file open/close request (rq).
 * It is also used as the answer payload to tell the state (st).
 */

#define LWROC_FILE_OP_OPEN     /* rq,st */  0x4f50454e  /* OPEN */
#define LWROC_FILE_OP_CLOSE    /* rq,st */  0x434c4f53  /* CLOS */
#define LWROC_FILE_OP_HOLD     /* rq,st */  0x484f4c44  /* HOLD */
#define LWROC_FILE_OP_JAMMED   /*    st */  0x4a414d44  /* JAMD */
#define LWROC_FILE_OP_UNJAM    /* rq    */  0x554e4a4d  /* UNJM */
#define LWROC_FILE_OP_NEWFILE  /* rq    */  0x4e574649  /* NWFI */
#define LWROC_FILE_OP_NEWRUN   /* rq    */  0x4e57524e  /* NWRN */
#define LWROC_FILE_OP_STATUS   /* rq    */  0x53544154  /* STAT */

typedef struct lwroc_file_request_t
{
  uint32_t _op;
  uint64_t _size;
  uint32_t _time;
  uint64_t _events;
  const char *_filename;
} lwroc_file_request;

/********************************************************************/

/* This is used as the payload for an acquisition start/stop request.
 * It is also used as the answer payload.
 */

#define LWROC_ACQR_OP_STATUS                0x53544154  /* STAT */
#define LWROC_ACQR_OP_START                 0x53545254  /* STRT */
#define LWROC_ACQR_OP_STOP                  0x53544f50  /* STOP */
#define LWROC_ACQR_OP_MEAS_DT               0x4d534454  /* MSDT */

typedef struct lwroc_acqr_request_t
{
  uint32_t _op;
  /* As response / status, number of events read out. */
  uint64_t _events;
} lwroc_acqr_request;

/********************************************************************/

/* This is used as the payload for an merger request.
 * It is also used as the answer payload.
 */

#define LWROC_MERGE_OP_TRY_RECONNECT_DIS_SRC  0x52434449  /* RCDI */

typedef struct lwroc_merge_request_t
{
  uint32_t _op;
  /* As response / status, number of events read out. */
  /* uint64_t _events; */
  uint32_t _num_active_sources;
  uint32_t _num_sources;
} lwroc_merge_request;

/********************************************************************/

typedef struct lwroc_message_source_t
{
  uint32_t _source;
  const char *_hostname;
  uint16_t _port;
  uint16_t _mainthread;
  const char *_thread;
  uint32_t _mon_ident1;
  uint32_t _mon_ident2;
  uint32_t _mon_ident3;
  uint32_t _mon_ident4;
  /* Followed by _hostname_len characters plus '\0'. */
  /* Followed by _thread_len characters plus '\0'. */
} lwroc_message_source;

/********************************************************************/

/* Need a magic in each message, as the messages may be intermixed
 * with updates of new available (or removed) sources.
 */

typedef struct lwroc_message_item_t
{
  lwroc_item_time  _time;
  uint16_t _level; /* 'B', 'F', 'C', 'E', 'A', 'D', 'W', 'a', 'i', 'l', 'd' */
  uint16_t _lost;
  uint32_t _source;
  const char *_file;
  uint32_t _line;
  uint32_t _ack_ident;
  const char *_msg;
  /* Followed by _file_len characters plus '\0'. */
  /* Followed by _msg_len characters plus '\0'. */
} lwroc_message_item;

/********************************************************************/

typedef struct lwroc_message_keepalive_t
{
  uint32_t _source;
} lwroc_message_keepalive;

/********************************************************************/

typedef struct lwroc_message_item_ack_t
{
  uint32_t _source;
  uint32_t _ack_ident;
} lwroc_message_item_ack;

/********************************************************************/

/* The monitor block consist of a header, followed by a fixed number
 * of blocks.  Each block has been described in the first message
 * from the monitor source.  This makes it possible for the monitor
 * display to have a quite static picture of the setup.
 */

typedef struct lwroc_monitor_source_header_t
{
  LWROC_SERIALIZE_ENVELOPE_SIZE(())
  uint32_t         _blocks;
} lwroc_monitor_source_header;

/* */

#define LWROC_MONITOR_BLOCK_TYPE_NORMAL     0x4e4f524d /* NORM */
#define LWROC_MONITOR_BLOCK_TYPE_DT_TRACE   0x44545243 /* DTRC */

typedef struct lwroc_monitor_block_header_t
{
  LWROC_SERIALIZE_ENVELOPE_SIZE(())
  /* Sequence number must be first (normal) item to protect following
   * items in header (but not for copy used as footer).
   */
  uint32_t         _sequence;
  uint32_t         _type;
} lwroc_monitor_block_header;

/********************************************************************/

typedef struct lwroc_monitor_main_source_t
{
  /* uint32_t         _dummy; */
  char             _label[16];
  uint32_t _mon_ident1;
  uint32_t _mon_ident2;
  uint32_t _mon_ident3;
  uint32_t _mon_ident4;
} lwroc_monitor_main_source;

typedef struct lwroc_monitor_main_block_t
{
  lwroc_item_time  _time;
  uint64_t         _cputime_usec;
  uint64_t         _events;
  uint64_t         _multi_events;
  uint64_t         _bytes;

  uint64_t         _last_timestamp;

  /* Timestamp tracking info.  Scale to ps, to avoid transporting doubles. */

  lwroc_item_time  _ts_ref_time               LWROC_PRINT_CTRL(NOFREQ:0);
  uint64_t         _ts_ref_stamp              LWROC_PRINT_CTRL(NODIFF:0);
  uint64_t         _ts_ref_stamp_freq_k       LWROC_PRINT_CTRL(NODIFF:0);
  uint64_t         _ts_ref_stamp_y0_k         LWROC_PRINT_CTRL(NODIFF:0);
  uint64_t         _ts_ref_stamp_sig_freq_k   LWROC_PRINT_CTRL(NODIFF:0);
  uint64_t         _ts_ref_stamp_sig_y0_k     LWROC_PRINT_CTRL(NODIFF:0);

} lwroc_monitor_main_block;

/********************************************************************/

typedef struct lwroc_monitor_filter_source_t
{
  char             _label[16];
} lwroc_monitor_filter_source;

typedef struct lwroc_monitor_filter_block_t
{
  lwroc_item_time  _time;
  uint64_t         _cputime_usec;
  uint64_t         _events;
  uint64_t         _multi_events;
  uint64_t         _bytes;

  /*uint64_t         _last_timestamp;*/

} lwroc_monitor_filter_block;

/********************************************************************/

typedef struct lwroc_monitor_net_source_t
{
  /* uint32_t      _dummy; */
  uint64_t         _buf_data_size;
  uint32_t         _conn_blocks;
} lwroc_monitor_net_source;

#define LWROC_NET_SYS_HEALTH_OK             0x2e4f4b2e  /* .OK. */
#define LWROC_NET_SYS_HEALTH_LOGWAIT        0x4c4f4757  /* LOGW */
#define LWROC_NET_SYS_HEALTH_SIGNAL         0x5349474e  /* SIGN */
#define LWROC_NET_SYS_HEALTH_BUG            0x4255472e  /* BUG. */
#define LWROC_NET_SYS_HEALTH_FATAL          0x4641544c  /* FATL */

typedef struct lwroc_monitor_net_block_t
{
  LWROC_PRINT_CTRL_STATIC((lwroc_monitor_net_source))

  lwroc_item_time  _time;
  uint64_t         _cputime_usec;

  uint32_t         _sys_health;

  uint32_t         _ncon_pmap_total;
  uint32_t         _ncon_total;
  uint32_t         _ncon_refused;
  uint32_t         _ncon_mux_open;
  uint32_t         _ncon_data_open;
  uint32_t         _ncon_msg_open;
  uint32_t         _ncon_mon_open;
  uint32_t         _ncon_ctrl_open;

  uint32_t         _ntp_queries;

  uint32_t         _num_msg;

  uint64_t         _recv_misc;
  uint64_t         _sent_misc;
  uint64_t         _recv_mssync;
  uint64_t         _sent_mssync;
  uint64_t         _sent_data;
  uint64_t         _sent_msg;
  uint64_t         _recv_msg_ack;
  uint64_t         _sent_mon;
  uint64_t         _recv_ctrl;
  uint64_t         _sent_ctrl;

  uint64_t         _buf_data_fill  LWROC_PRINT_CTRL(FILLFRAC:_buf_data_size);

  /* Deadtime measurement information.  In this block, as the
   * triva state machine is here.
   */

  uint32_t         _meas_dt_2events;
  uint32_t         _meas_dt_us_poll;
  uint32_t         _meas_dt_us;
  uint32_t         _meas_dt_us_postproc;
  uint32_t         _meas_dt_ctime;

} lwroc_monitor_net_block;

/********************************************************************/

/* #define LWROC_DT_TRACE_WASTE                0x80000000 */

#define LWROC_DT_TRACE_TIMESCALE_MASK       0x70000000
#define LWROC_DT_TRACE_TIMESCALE_LOCAL_NS   0x10000000
#define LWROC_DT_TRACE_TIMESCALE_LOCAL_US   0x20000000
#define LWROC_DT_TRACE_TIMESCALE_STAMP      0x30000000
#define LWROC_DT_TRACE_TIMESCALE_EVENT      0x40000000 /* Not a time. */

#define LWROC_DT_TRACE_T_MASK               0x0f000000
#define LWROC_DT_TRACE_T_TRIGGER            0x05000000
#define LWROC_DT_TRACE_T_LAST_POLL          0x01000000
#define LWROC_DT_TRACE_T_AFTER_POLL         0x02000000
#define LWROC_DT_TRACE_T_AFTER_DT_RELEASE   0x03000000
#define LWROC_DT_TRACE_T_AFTER_POSTPROC     0x04000000

#define LWROC_DT_TRACE_TV_SEC_MASK          0x000000ff

#define LWROC_DT_TRACE_SPILL_SHIFT          8

typedef struct lwroc_monitor_dt_trace_t
{
  uint32_t         _ctime;
  uint32_t         _array_used;
  uint32_t         _array[1024];
} lwroc_monitor_dt_trace;

/********************************************************************/

typedef struct lwroc_monitor_file_source_t
{
  /* uint32_t      _dummy; */
  uint32_t         _filewr_blocks;
} lwroc_monitor_file_source;

typedef struct lwroc_monitor_file_block_t
{
  lwroc_item_time  _time;
  uint64_t         _cputime_usec;
  uint64_t         _events;
  uint64_t         _bytes;
  uint64_t         _buffers;
  uint32_t         _files;
  uint32_t         _active;
} lwroc_monitor_file_block;

/********************************************************************/

typedef struct lwroc_monitor_filewr_source_t
{
  uint32_t         _dummy;
} lwroc_monitor_filewr_source;

#define LWROC_FILEWR_STATE_CLOSED           0x434c5344  /* CLSD */
#define LWROC_FILEWR_STATE_OPEN             0x4f50454e  /* OPEN */
#define LWROC_FILEWR_STATE_HOLD             0x484f4c44  /* HOLD */
#define LWROC_FILEWR_STATE_JAMMED           0x4a414d44  /* JAMD */

typedef struct lwroc_monitor_filewr_block_t
{
  uint32_t         _state;
  lwroc_item_time  _open_close_time;
  uint64_t         _bytes;
  /* Fixed size array due to being a monitor block.
   * (Transmission is using a fixed-size serialized array.)
   */
  char             _filename[256];
} lwroc_monitor_filewr_block;

/********************************************************************/

typedef struct lwroc_monitor_serv_source_t
{
  uint32_t         _dummy;
} lwroc_monitor_serv_source;

typedef struct lwroc_monitor_serv_block_t
{
  lwroc_item_time  _time;
  uint64_t         _cputime_usec;
} lwroc_monitor_serv_block;

/********************************************************************/

typedef struct lwroc_monitor_in_source_t
{
  uint32_t         _dummy;
} lwroc_monitor_in_source;

typedef struct lwroc_monitor_in_block_t
{
  LWROC_PRINT_CTRL_STATIC((lwroc_monitor_in_source))

  lwroc_item_time  _time;
  uint64_t         _cputime_usec;
  uint64_t         _bytes;
  uint64_t         _buffers;
  uint32_t         _active;
} lwroc_monitor_in_block;

/********************************************************************/

typedef struct lwroc_monitor_analyse_source_t
{
  /* uint32_t         _dummy; */
  uint32_t         _ana_ts_blocks;
} lwroc_monitor_analyse_source;

typedef struct lwroc_monitor_analyse_block_t
{
  lwroc_item_time  _time;
  uint64_t         _cputime_usec;
} lwroc_monitor_analyse_block;

/********************************************************************/

typedef struct lwroc_monitor_ana_ts_source_t
{
  uint32_t         _dummy;
} lwroc_monitor_ana_ts_source;

typedef struct lwroc_monitor_ana_ts_block_t
{
  lwroc_item_time  _time;
  uint64_t         _src_index_mask;
  uint32_t         _id;

  uint64_t         _ts_ref_offset_k          LWROC_PRINT_CTRL(NODIFF:0);
  uint64_t         _ts_ref_off_sig_k         LWROC_PRINT_CTRL(NODIFF:0);
  uint32_t         _within5sigma_k;
  uint32_t         _within20sigma_k;
  uint32_t         _outside_k;

  uint64_t         _ts_sync_ref_offset_k     LWROC_PRINT_CTRL(NODIFF:0);
  uint64_t         _ts_sync_ref_off_sig_k    LWROC_PRINT_CTRL(NODIFF:0);

  uint32_t         _sync_outside20sigma_k;

  uint64_t         _sync_check_no_checkval;
  uint64_t         _sync_check_no_ref;
  uint64_t         _sync_check_good;
  uint64_t         _sync_check_ambiguous;
  uint64_t         _sync_check_mismatch;
  uint64_t         _sync_check_missing;
  uint64_t         _sync_check_spurious;

  uint32_t         _sync_check_sigmas_k;

} lwroc_monitor_ana_ts_block;

/********************************************************************/

/* DAM = data acquisition module. */

typedef struct lwroc_monitor_dams_source_t
{
  /* uint32_t         _dummy; */
  uint32_t         _dam_blocks;
} lwroc_monitor_dams_source;

typedef struct lwroc_monitor_dams_block_t
{
  lwroc_item_time  _time;
  uint64_t         _cputime_usec; /* Just due to update macro. */
} lwroc_monitor_dams_block;

/********************************************************************/

typedef struct lwroc_monitor_dam_source_t
{
  uint32_t         _dummy;
} lwroc_monitor_dam_source;

typedef struct lwroc_monitor_dam_block_t
{
  /* lwroc_item_time  _time; */

  uint32_t         _id;          /* -1 = unused. */

  uint64_t         _events;
  uint64_t         _trig_sig;    /* Not included in hits. */
  uint64_t         _hits;        /* Include overflow and pileup here too. */
  uint64_t         _hits_ovfl;
  uint64_t         _hits_pileup;
  uint64_t         _hits_bad;    /* E.g. during bad timestamp tracking. */
  uint64_t         _hits_missed; /* Lost. */
  uint64_t         _hits_filt;   /* Filtered. */
  uint64_t         _hits_drop;   /* Dropped. */

} lwroc_monitor_dam_block;

/********************************************************************/

/* Kind of connection according to LWROC_REQUEST_ markers.
 * Additionally:
 */
#define LWROC_CONN_TYPE_TRANS               0x54524e53  /* TRNS */
#define LWROC_CONN_TYPE_STREAM              0x5354524d  /* STRM */
#define LWROC_CONN_TYPE_SYSTEM              0x53595354  /* SYST */
#define LWROC_CONN_TYPE_FILTER              0x46494c54  /* FILT */
#define LWROC_CONN_TYPE_TS_FILTER           0x5453464c  /* TSFL */
#define LWROC_CONN_TYPE_FILE                0x46494c45  /* FILE */
#define LWROC_CONN_TYPE_FNET                0x464e4554  /* FNET */
#define LWROC_CONN_TYPE_EBYE_PUSH           0x45425053  /* EBPS */
#define LWROC_CONN_TYPE_EBYE_PULL           0x45425055  /* EBPU */

#define LWROC_CONN_DIR_OUTGOING             0x4f555447  /* OUTG */
#define LWROC_CONN_DIR_INCOMING             0x494e434f  /* INCO */

#define LWROC_CONN_KIND_TRIVA               0x54524956  /* TRIV */
#define LWROC_CONN_KIND_TRIMI               0x54524d49  /* TRMI */
#define LWROC_CONN_KIND_UNTRIG              0x554e5452  /* UNTR */

#define LWROC_CONN_KIND_SUB_MASTER          0x4d415354  /* MAST */
#define LWROC_CONN_KIND_SUB_SLAVE           0x534c4156  /* SLAV */

#define LWROC_CONN_KIND_EB                  0x45564255  /* EVBU */
#define LWROC_CONN_KIND_TS                  0x5449534f  /* TISO */

#define LWROC_CONN_KIND_SUB_WR              0x57485242  /* WHRB */
#define LWROC_CONN_KIND_SUB_TITRIS          0x54495452  /* TITR */
#define LWROC_CONN_KIND_SUB_EBYE_HITS       0x45425948  /* EBYH */
#define LWROC_CONN_KIND_SUB_XFER_BLOCK      0x58465242  /* XFRB */

#define LWROC_CONN_KIND_SRCBUF_MAIN         0x534d4149  /* SMAI */
#define LWROC_CONN_KIND_SRCBUF_FILTER       0x53564c54  /* SFLT */
#define LWROC_CONN_KIND_SRCBUF_TS_FILTER    0x5354564c  /* STFL */

typedef struct lwroc_monitor_conn_source_t
{
  uint32_t         _type;
  uint32_t         _direction;
  const char      *_hostname;
  const char      *_label;

  uint32_t         _kind;
  uint32_t         _kind_sub;

  uint32_t         _ipv4_addr;
  uint16_t         _port;

  uint64_t         _buf_data_size;

} lwroc_monitor_conn_source;

/* For outgoing connections, we keep track of the last failure with
 * the LWROC_BADREQUEST_ markers.  Additionally:
 */
#define LWROC_CONN_STATUS_NOT_ATTEMPTED     0x4e545259  /* NTRY */
#define LWROC_CONN_STATUS_NOT_CONNECTED     0x4e434f4e  /* NCON */
#define LWROC_CONN_STATUS_CONNECTED         0x434f4e4e  /* CONN */

#define LWROC_CONN_DATA_FILL_FULL           UINT64_C(0x8000000000000000)

/* For triva/mi, we keep track of the general state. */

#define LWROC_TRIVA_STATUS_REINIT            0x5245494e  /* REIN */
#define LWROC_TRIVA_STATUS_WAIT_TERM         0x57545452  /* WTTR */
#define LWROC_TRIVA_STATUS_WAIT_CONN         0x5754434f  /* WTCO */
#define LWROC_TRIVA_STATUS_WAIT_EB_CLR       0x57544543  /* WTEC */
#define LWROC_TRIVA_STATUS_WAIT_MASTER_SELF  0x57544d41  /* WTMA */
#define LWROC_TRIVA_STATUS_WAIT_SLAVE        0x5754534c  /* WTSL */
#define LWROC_TRIVA_STATUS_WAIT_IDENT        0x57544944  /* WTID */
#define LWROC_TRIVA_STATUS_TEST              0x54455354  /* TEST */
#define LWROC_TRIVA_STATUS_WAIT_TEST         0x57545453  /* WTTS */
#define LWROC_TRIVA_STATUS_RUN               0x52554e2e  /* RUN. */
#define LWROC_TRIVA_STATUS_WAIT_RUN          0x5754524e  /* WTRN */
#define LWROC_TRIVA_STATUS_INSPILL           0x494e5350  /* INSP */
#define LWROC_TRIVA_STATUS_STUCK_INSPILL     0x53544d49  /* STKI */
#define LWROC_TRIVA_STATUS_OFFSPILL          0x4f465350  /* OFSP */
#define LWROC_TRIVA_STATUS_STOP              0x53544f50  /* STOP */
#define LWROC_TRIVA_STATUS_WAIT_NET          0x57544e54  /* WTNT */
#define LWROC_TRIVA_STATUS_WAIT_DT           0x57544454  /* WTDT */
#define LWROC_TRIVA_STATUS_WAIT_BUF          0x57544246  /* WTBF */
#define LWROC_TRIVA_STATUS_WAIT_READ         0x57545244  /* WTRD */
#define LWROC_TRIVA_STATUS_NODIAGNOSE        0x4e4f4449  /* NODI */
#define LWROC_TRIVA_STATUS_WAIT_ABORT        0x57544142  /* WTAB */
#define LWROC_TRIVA_STATUS_SLAVE             0x534c4156  /* SLAV */
#define LWROC_TRIVA_STATUS_SLAVE_WAIT_SELF   0x53575453  /* SWTS */
#define LWROC_TRIVA_STATUS_SLAVE_WAIT_ABORT  0x53575441  /* SWTA */
#define LWROC_TRIVA_STATUS_HEADACHE          0x48454144  /* HEAD */

#define LWROC_TRIVA_DEADTIME_MEASURE         0x44544d53  /* DTMS */

/* Failure codes may not overlap with status codes above.
 * Common decoding in lwroc_get_status_format().
 */

#define LWROC_TRIVA_FAIL_NONE               0x4e4f4e45  /* NONE */
#define LWROC_TRIVA_FAIL_LOCAL              0x4c4f434c  /* LOCL */
#define LWROC_TRIVA_FAIL_NET                0x4e45542e  /* NET. */
#define LWROC_TRIVA_FAIL_EB_ID              0x45424944  /* EBID */
#define LWROC_TRIVA_FAIL_TEST               0x54535446  /* TSTF */
#define LWROC_TRIVA_FAIL_MISMATCH           0x4d49534d  /* MISM */
#define LWROC_TRIVA_FAIL_EC_MISMATCH        0x45434d49  /* ECMI */
#define LWROC_TRIVA_FAIL_BAD_EC_SYNC        0x45435359  /* ECSY */
#define LWROC_TRIVA_FAIL_TRIG_UNEXPECT      0x55455452  /* UETR */
#define LWROC_TRIVA_FAIL_SEQ_ERROR          0x53514552  /* SQER */
#define LWROC_TRIVA_FAIL_SEQ_UNEXPECT       0x53515545  /* SQUE */
#define LWROC_TRIVA_FAIL_SEQ_MALFORM        0x53514d46  /* SQMF */
#define LWROC_TRIVA_FAIL_EB_DATA_CONN       0x45424441  /* EBDA */
#define LWROC_TRIVA_FAIL_STHR_BUG_FATAL     0x42474654  /* BGFT */

/* For triva/mi bus connections, we keep track of hold-ups.
 * Failure codes may not overlap with codes above.
 */

#define LWROC_TRIVA_BUS_STATUS_NONE         0x4e4f4e2e  /* NON. */
#define LWROC_TRIVA_BUS_STATUS_WAIT         0x57414954  /* WAIT */
#define LWROC_TRIVA_BUS_STATUS_DEADTIME     0x44454144  /* DEAD */
#define LWROC_TRIVA_BUS_STATUS_FAIL         0x4641494c  /* FAIL */

typedef struct lwroc_monitor_conn_block_t
{
  LWROC_PRINT_CTRL_STATIC((lwroc_monitor_conn_source))

  lwroc_item_time  _time;
  uint64_t         _events;
  uint64_t         _bytes;

  uint64_t         _timestamp;
  uint64_t         _timestprv;

  uint32_t         _ipv4_addr;
  uint16_t         _port;

  lwroc_item_time  _time_status;
  uint32_t         _status;

  uint64_t         _buf_data_fill  LWROC_PRINT_CTRL(FILLFRAC:_buf_data_size);

  uint32_t         _aux_status;
  uint32_t         _aux_last_error;
  lwroc_item_time  _aux_time_error;

} lwroc_monitor_conn_block;

/********************************************************************/

#if 0
#define LWROC_NET_PROTO_MAGIC \
  (0x95310919u + 1) /* Change with protocol version. */
#endif

/********************************************************************/

typedef struct lwroc_trigbus_msg_t
{
  uint32_t         _type;
  uint32_t         _magic;

  /* Used by internal control for init: */
  uint32_t  bus_enable;
  uint32_t  master;
  uint32_t  fctime;
  uint32_t  ctime;

  /* In response to a (test) trigger: */
  /* And trigger to set for next event in test mode: */
  uint32_t  _trig;

  /* In status report, master ident response and ident to slave. */
  uint64_t  event_counter;

  /* Tell if delayed eb by spill1213 is in use, and report from eb
   * if forced event building is required. */
  uint32_t  delayed_eb_status;

  /* Status report. */
  uint32_t  deadtime;
  uint32_t  status;

  /* Ident message (for event build acceptance) */
  /* Abused for DT measurement reporting. */
  uint32_t  _eb_ident0;
  uint32_t  _eb_ident1;
  uint32_t  _eb_ident2;
  uint32_t  _eb_ident3; /* Only abused. */

  /* For exchange with an event builder. */
  uint32_t  _eb_srcs_waiting;

} lwroc_trigbus_msg;

/********************************************************************/

#endif/*__LWROC_NET_PROTO_H__*/

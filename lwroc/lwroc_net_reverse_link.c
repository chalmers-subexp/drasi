/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_net_reverse_link.h"
#include "lwroc_message.h"
#include "lwroc_hostname_util.h"
#include "lwroc_net_incoming.h"
#include "lwroc_net_io.h" /* for debug */
#include "lwroc_net_io_util.h"

#include "gen/lwroc_reverse_link_ack_serializer.h"

#include <string.h>
#include <assert.h>
#include "../dtc_arch/acc_def/myprizd.h"

/********************************************************************/

PD_LL_SENTINEL(_lwroc_reverse_links);

/********************************************************************/

int lwroc_net_reverse_link_after_fail(lwroc_select_item *item,
				      lwroc_select_info *si)
{
  lwroc_net_reverse_link *conn =
    PD_LL_ITEM(item, lwroc_net_reverse_link, _out._base._select_item);

  (void) conn;
  (void) si;

  lwroc_safe_close(conn->_out._base._fd);
  conn->_out._base._fd = -1;

  assert(conn->_out._base._fd == -1);

  lwroc_net_become_outgoing(&conn->_out,si);

  return 1;
}

void lwroc_net_reverse_link_setup_select(lwroc_select_item *item,
					 lwroc_select_info *si)
{
  lwroc_net_reverse_link *conn =
    PD_LL_ITEM(item, lwroc_net_reverse_link, _out._base._select_item);

  /* Do we have data to send? */

  if (conn->_out._base._buf._size)
    {
      /* printf ("rev_link: to_send!\n"); */
      LWROC_WRITE_FD_SET(conn->_out._base._fd, si);
    }

  /* We always check for reading; we should never have anything to
   * read, but check for broken connection even when not sending.
   */

  LWROC_READ_FD_SET(conn->_out._base._fd, si);
}

/********************************************************************/

uint32_t _lwroc_next_link_ack_rid = 1;

void lwroc_net_reverse_prepare_link_ack(lwroc_net_reverse_link *conn,
					lwroc_net_incoming *incoming,
					lwroc_select_info *si)
{
  lwroc_reverse_link_ack link_ack;

  /* rid is always non-zero when in use, so shift counter up one. */

  incoming->_link_ack_rid   = (_lwroc_next_link_ack_rid++ << 1) | 1;

  /* This is not supposed to be secure or unbreakable, just to catch
   * somehow faulty/wrong connections.  We mix in the time also
   * just for the fun of it.
   */

  incoming->_link_ack_token = ((uint32_t) si->now.tv_usec) << 8;
  incoming->_link_ack_token ^= incoming->_link_ack_rid;
  incoming->_link_ack_token ^= incoming->_req_msg._link_ack_id1 << 3;
  incoming->_link_ack_token ^= incoming->_req_msg._link_ack_id2 << 5;

  /* Prepare what to send.  The basic point is that we send something
   * specific (rid, token) to a client at an IP _and_ port that we
   * connected to (the same as the client claims to come from).  If
   * the client then cannot send us back this very same information
   * over the original link, it likely is not really holding the port
   * it claims.
   */

  link_ack._link_ack_id1   = incoming->_req_msg._link_ack_id1;
  link_ack._link_ack_id2   = incoming->_req_msg._link_ack_id2;
  link_ack._link_ack_rid   = incoming->_link_ack_rid;
  link_ack._link_ack_token = incoming->_link_ack_token;

  /* Prepare the message. */
  conn->_out._base._buf._size = lwroc_reverse_link_ack_serialized_size();
  assert (sizeof (conn->_out._raw) >= conn->_out._base._buf._size);
  conn->_out._base._buf._ptr = (void *) conn->_out._raw;
  conn->_out._base._buf._offset = 0;

  /*
  printf ("prepared lack send (req %08x, orig_port: %d): "
	  "%08x:%08x : %08x:%08x\n",
	  incoming->_req_msg._request,
	  incoming->_req_msg._orig_port,
	  link_ack._link_ack_id1,
	  link_ack._link_ack_id2,
	  link_ack._link_ack_rid,
	  link_ack._link_ack_token);
  */

  lwroc_reverse_link_ack_serialize(conn->_out._base._buf._ptr,
				   &link_ack);
}

/********************************************************************/

void lwroc_net_reverse_link_check_queue_link_ack(lwroc_net_reverse_link *conn,
						 lwroc_select_info *si)
{
  if (PD_LL_IS_EMPTY(&conn->_send_link_ack))
    {
      /* Found nothing, we are done sending for the moment. */

      conn->_out._base._buf._size = 0;
    }
  else
    {
      lwroc_net_incoming *incoming =
	PD_LL_ITEM(PD_LL_FIRST(conn->_send_link_ack),
		   lwroc_net_incoming, _send_link_ack);

      lwroc_net_io_print_clients();

      /* printf ("dequeued incoming: %p\n", incoming); */

      lwroc_net_io_print_clients();

      PD_LL_REMOVE(&incoming->_send_link_ack);

      lwroc_net_io_print_clients();

      lwroc_net_reverse_prepare_link_ack(conn, incoming, si);

      lwroc_net_io_print_clients();
    }
}

/********************************************************************/

int lwroc_net_reverse_link_after_select(lwroc_select_item *item,
					lwroc_select_info *si)
{
  lwroc_net_reverse_link *conn =
    PD_LL_ITEM(item, lwroc_net_reverse_link, _out._base._select_item);

  if (!lwroc_fail_on_read_check_eof(conn->_out._base._fd, si))
    return 0;

  if (!LWROC_WRITE_FD_ISSET(conn->_out._base._fd, si))
    return 1;

  if (!lwroc_net_client_base_write(&conn->_out._base, "link ack",
				   &_lwroc_mon_net._sent_misc))
    return 0;

  /*
  printf ("rev_link (%p): sent %" MYPRIzd "/%" MYPRIzd "!\n",
	  &conn->_out._base._buf,
	  conn->_out._base._buf._offset, conn->_out._base._buf._size);
  */

  if (conn->_out._base._buf._offset == conn->_out._base._buf._size)
    {
      lwroc_net_reverse_link_check_queue_link_ack(conn, si);
    }

  return 1;
}

/********************************************************************/

extern PD_LL_SENTINEL_ITEM(_lwroc_net_clients);

void lwroc_net_reverse_link_connected(lwroc_select_item *item,
				      lwroc_select_info *si)
{
  lwroc_net_reverse_link *conn =
    PD_LL_ITEM(item, lwroc_net_reverse_link, _out._base._select_item);

  lwroc_net_io_print_clients();

  /* printf ("rev_link: connected!\n"); */

  lwroc_net_io_print_clients();

  /* Item in queue to be sent? */

  lwroc_net_io_print_clients();

  lwroc_net_reverse_link_check_queue_link_ack(conn, si);

  lwroc_net_io_print_clients();
}

/********************************************************************/

lwroc_outgoing_select_item_info lwroc_net_reverse_link_select_item_info =
{
  {
    lwroc_net_reverse_link_setup_select,
    lwroc_net_reverse_link_after_select,
    lwroc_net_reverse_link_after_fail,
    NULL,
    LWROC_SELECT_ITEM_FLAG_OUTGOING_NET_THREAD,
  },
  lwroc_net_reverse_link_connected,
  LWROC_REQUEST_REVERSE_LINK,
  NULL,
};

/********************************************************************/

char *lwroc_reverse_link_fmt_msg_context(char *buf, size_t size,
					 const  void *ptr)
{
  const lwroc_net_reverse_link *conn = (const lwroc_net_reverse_link *)
    (ptr - offsetof(lwroc_net_reverse_link, _out._base._msg_context));

  return lwroc_message_fmt_msg_context(buf, size,
				       "revlink: %s", conn->_out._hostname);
}

/********************************************************************/

lwroc_net_reverse_link *
lwroc_add_reverse_link(const char *hostname, uint32_t request,
		      int type)
{
  lwroc_net_reverse_link *conn;

  (void) type;

  /* TODO: if we want to accept several kinds of request(type)
   * connections from the same instance, we should add them to the
   * same reverse link!
   */

  /* Create the connection structure. */

  conn = (lwroc_net_reverse_link *) malloc (sizeof (lwroc_net_reverse_link));

  if (!conn)
    LWROC_FATAL("Memory allocation failure (reverse link conn info).");

  memset (conn, 0, sizeof (lwroc_net_reverse_link));

  conn->_out._hostname = strdup_chk(hostname);
  conn->_out._base._msg_context = lwroc_reverse_link_fmt_msg_context;

  if (!lwroc_get_host_port(hostname,
			   LWROC_NET_DEFAULT_PORT,
			   &conn->_out._base._addr))
    exit(1);

  conn->_request = request;

  PD_LL_ADD_BEFORE(&_lwroc_reverse_links, &conn->_reverse_links);

  PD_LL_INIT(&conn->_send_link_ack);

  /* printf ("adding rev_link -> out (%p)\n", &conn->_out); */

  lwroc_net_outgoing_init(&conn->_out,
			  &lwroc_net_reverse_link_select_item_info);

  return conn;
}

/********************************************************************/

int lwroc_find_reverse_link(lwroc_net_incoming *incoming,
			    lwroc_select_info *si)
{
  pd_ll_item *iter;

  /* Find a reverse link for the incoming connection,
   * i.e. that has or would connect to the host/port.
   */

  /*
  printf ("lwroc_find_reverse_link... "
	  "(req %08x, orig_port: %d): (%08x,%08x)\n",
	  incoming->_req_msg._request,
	  incoming->_req_msg._orig_port,
	  incoming->_req_msg._link_ack_id1,
	  incoming->_req_msg._link_ack_id2);
  */

  PD_LL_FOREACH(_lwroc_reverse_links, iter)
    {
      lwroc_net_reverse_link *conn =
	PD_LL_ITEM(iter, lwroc_net_reverse_link, _reverse_links);

      if (incoming->_req_msg._request != conn->_request)
	continue;

      if (!lwroc_compare_sockaddr_pb(&conn->_out._base._addr,
				     &incoming->_base._addr,
				     incoming->_req_msg._orig_port))
	continue;

      /* So we have a matching connection. */

      /* If the connection is live and currently not sending, then
       * immediately prepare the packet and send.  Otherwise queue it.
       *
       * Note that quick re-establishment of the connection is handled
       * already when we see the incoming connection as such.
       */

      if (conn->_out._base._select_item.item_info ==
	  &lwroc_net_reverse_link_select_item_info.base &&
	  conn->_out._base._buf._size == 0)
	{
	  /* printf ("lwroc_find_reverse_link found - send\n"); */
	  lwroc_net_reverse_prepare_link_ack(conn, incoming, si);
	}
      else
	{
	  /* printf ("lwroc_find_reverse_link found - queued\n"); */
	  PD_LL_ADD_BEFORE(&conn->_send_link_ack,
			   &incoming->_send_link_ack);
	  /* printf ("enqueued incoming: %p\n", incoming); */
	}

      /* Hmm, we actually here could just pass the reverse link pointer. */
      incoming->_rev_link = conn;

      return 1;
    }

  return 0;
}

/********************************************************************/

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_NET_REVERSE_LINK_H__
#define __LWROC_NET_REVERSE_LINK_H__

#include "lwroc_net_outgoing.h"

/********************************************************************/

typedef struct lwroc_net_reverse_link_t
{
  lwroc_net_outgoing _out;

  /* For which kind of request are we sending confirmation messages. */
  uint32_t _request;

  /* Additional information to be passed to the successfully multiplexed
   * handler.
   */
  void *_extra;

  /* List of all reverse links. */
  pd_ll_item _reverse_links;

  /* Queue for this reverse link. */
  pd_ll_item _send_link_ack;

} lwroc_net_reverse_link;

/********************************************************************/

extern lwroc_outgoing_select_item_info lwroc_net_reverse_link_select_item_info;

/********************************************************************/

void lwroc_net_become_reverse_link(lwroc_net_reverse_link *conn);

/********************************************************************/

lwroc_net_reverse_link *
lwroc_add_reverse_link(const char *hostname, uint32_t request,
		       int type);

/********************************************************************/

struct lwroc_net_incoming_t;

int lwroc_find_reverse_link(struct lwroc_net_incoming_t *incoming,
			    lwroc_select_info *si);

/********************************************************************/

#endif/*__LWROC_NET_REVERSE_LINK_H__*/

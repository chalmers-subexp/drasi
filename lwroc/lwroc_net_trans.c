/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_net_trans.h"
#include "lwroc_message.h"

#include "lwroc_net_io.h"
#include "lwroc_net_io_util.h"
#include "lwroc_pipe_buffer.h"
#include "lwroc_main_iface.h"
#include "lwroc_parse_util.h"
#include "lwroc_timeouts.h"
#include "lwroc_hostname_util.h"
#include "lwroc_delayed_eb.h"
#include "lwroc_cpu_affinity.h"

#include <errno.h>
#include <string.h>
#include <assert.h>

#include "../dtc_arch/acc_def/myinttypes.h"
#include "../dtc_arch/acc_def/myprizd.h"

/********************************************************************/

lwroc_net_trans_unit *_lwroc_drasi_server = NULL;

/********************************************************************/

/* Minimum buffer size that we accept. */
#define LWROC_MIN_TRANS_BUFFER_BUF_SIZE           0x400 /* 1024 */
/* Minimum default buffer size. */
#define LWROC_SMALL_TRANS_BUFFER_BUF_SIZE        0x8000 /* 32 ki */
/* Buffer size above which we do not insist on a factor 2 larger
 * buffer than event-size.
 */
#define LWROC_TIGHT_TRANS_BUFFER_BUF_SIZE      0x800000
/* Buffer size above which a non-variable buffer will have a
 * larger timeout value. */
#define LWROC_LARGE_TRANS_BUFFER_BUF_SIZE      0x400000
/* Maximum buffer size to automatically grow internal protocol to when
 * having large pipe. */
#define LWROC_GROW_PIPE_DRASI_BUFFER_BUF_SIZE  0x200000 /* 2 Mi */
/* Protocol is 32 bits, but let's play it safe, 1 G transfer sizes are
 * crazy anyhow.
 */
#define LWROC_MAX_TRANS_BUFFER_BUF_SIZE      0x40000000 /* 1 Gi */

/********************************************************************/

void lwroc_skip_data(lwroc_pipe_buffer_consumer *consumer,
		     lwroc_thread_block *block)
{
  size_t avail;
  char *ptr;

  /* We just discard any data available in the buffer. */

  /* Used for dummy consumers (when pipe really has no consumers), or
   * for (multi-)client slots that are not connected.
   */

  for ( ; ; )
    {
      avail = lwroc_pipe_buffer_avail_read(consumer,
					   block,
					   &ptr, NULL, 0, 0);

      if (!avail)
	break;

      lwroc_pipe_buffer_did_read(consumer, avail);

      /* If there was data, we must try again for more.  Otherwise we
       * will not be properly notified that we should read further,
       * since we have no natural cause to try again.  (If we would be
       * transmitting, we would have the cause when the transmission
       * is done.)
       */
    }
}

void lwroc_skip_nonsticky_data_full(lwroc_net_trans_unit *unit)
{
  /* Scan buffer for sticky events, while discarding all data. */

  /* Needed to be able to replay sticky events. */

  /* Consume while there is data. */

  for ( ; ; )
    {
      if (!unit->_fmt->chunks_collect(unit->_bufchunks_skip,
				      unit->source_full,
				      _lwroc_net_io_thread->_block,
				      NULL,
				      unit->sticky_store_full,
				      0, 1))
	break;

      /* printf("release (skip-full)\n"); */
      lwroc_gdf_buffer_chunks_release(unit->_bufchunks_skip,
				      unit->source_full,
				      NULL);
    }
}

void lwroc_skip_nonsticky_data(lwroc_net_trans_item *item,
			       ssize_t eat)
{
  /* First, we must ensure that whatever data is left to be sent is
   * copied to a local buffer.
   */

  assert(item->_client);
  assert(item->_client->_bufchunks);
  if (lwroc_gdf_buffer_chunks_pending(item->_client->_bufchunks))
    {
      size_t n;

      n = lwroc_gdf_buffer_chunks_wr_buf(item->_client->_bufchunks,
					 item->_localbuf,
					 item->_unit->_bufsize);

      if (n > 0)
	{
	  item->_client->_base._buf._size = n;
	  item->_client->_base._buf._offset = 0;
	  item->_client->_base._buf._ptr = item->_localbuf;

	  /* printf ("local write: %zd\n", n); */
	}

      /* We are done with whatever was in this buffer.
       * (Also if there was nothing.)
       */
      eat -= (ssize_t) item->_client->_bufchunks->fully_used;
    }

  /* We must release in any case.  There may have been partially
   * collected data (for which we have not collected stickies).
   * Release, so that we do that collection again, and get the
   * stickes.
   */
  /*printf ("Skipped(1): %zd\n", item->_client->bufchunks.fully_used);*/

  /* printf("release (skip/store-to-local)\n"); */
  lwroc_gdf_buffer_chunks_release(item->_client->_bufchunks,
				  item->_client->_item->source,
				  NULL);

  while (eat > 0)
    {
      /* Since we have to look for sticky events, we have to go
       * through the data.  Instead of having a dedicated routine for
       * that, use the normal routine.  One could think of fiddling
       * with the buffer size used by it, but...
       */

      /* All sticky events that are missed are piped through the
       * sticky store.
       */

      if (!item->_unit->_fmt->chunks_collect(item->_client->_bufchunks,
					     item->_client->_item->source,
					     _lwroc_net_io_thread->_block,
					     item->sticky_store,
					     NULL,
					     0, 1))
	{
	  /* Since we got here, there should be at least the data to eat
	   * that we have been requested to chew...
	   */
	  /* TODO: FIX: Hmm, might be just a waste buffer at the end,
	   * so ok....
	   */
	  LWROC_CBUG(&item->_client->_base,
		     "Trying to skip events in buffer, but none found.");
	}

      /*printf ("Skipped: %zd\n", item->_client->bufchunks.fully_used);*/

      eat -= (ssize_t) item->_client->_bufchunks->fully_used;

      /* TODO: the eat will not properly account when we release waste
       * buffers.  So we'd better have an actual target in terms of
       * pipe buffer location.
       */

      /* printf("release (skip-eat)\n"); */
      lwroc_gdf_buffer_chunks_release(item->_client->_bufchunks,
				      item->_client->_item->source,
				      NULL);
    }
}


void lwroc_net_trans_skip_data(lwroc_net_trans_unit *unit)
{
  pd_ll_item *iter;

  if (unit->_hold == LWROC_NET_SERVER_HOLD_FULL)
    {
      /* We never skip data. */
      /* Except if we are terminating, in which case new clients will
       * not be accepted, so no progress would be made to clear buffer
       * for a producer that may not have finished yet.
       */
      if (!_lwroc_shutdown ||
	  !PD_LL_IS_EMPTY(&unit->_items_used[LWROC_NET_TRANS_CLIENTS_NORMAL]))
	return;
    }
  if (unit->_hold == LWROC_NET_SERVER_HOLD_WEAK)
    {
      /* We skip data if no client is connected. */
      if (!PD_LL_IS_EMPTY(&unit->_items_used[LWROC_NET_TRANS_CLIENTS_NORMAL]))
	return;
    }

  /* With multiple clients, we need to track the stickies for full
   * replay separately.
   *
   * With only one client, we do the full replay tracking together
   * with the normal skipping.
   */
  lwroc_skip_nonsticky_data_full(unit);

  if (unit->_hold == LWROC_NET_SERVER_HOLD_WEAK)
    return; /* In weak hold mode, we only have one source. */

  /* Skip data in all free buffers. */

  PD_LL_FOREACH(unit->_items_free[LWROC_NET_TRANS_CLIENTS_NORMAL], iter)
    {
      lwroc_net_trans_item *item;

      item = PD_LL_ITEM(iter, lwroc_net_trans_item, _items);

      lwroc_skip_data(item->source, _lwroc_net_io_thread->_block);
    }

  /* Then skip data for used client slots that are lagging too much. */

  PD_LL_FOREACH(unit->_items_used[LWROC_NET_TRANS_CLIENTS_NORMAL], iter)
    {
      lwroc_net_trans_item *item;
      size_t buf_sz, buf_fill;

      item = PD_LL_ITEM(iter, lwroc_net_trans_item, _items);

      buf_sz = lwroc_pipe_buffer_size_from_consumer(item->source);
      buf_fill = lwroc_pipe_buffer_fill_due_to_consumer(item->source);

      /*printf ("%zd %zd\n", buf_fill, buf_sz);*/

      if (buf_fill > 9 * buf_sz / 10)
	{
	  /* printf ("Client is lagging...\n"); */
	  /* lwroc_skip_data(item->source, _lwroc_net_io_thread->_block); */
	  /*
	  printf ("%zd %zd : %zd\n",
		  buf_fill, buf_sz, (ssize_t) (buf_fill - 4 * buf_sz / 5));
	  */
	  /* We eat to be twice as free as the trigger point.  Otherwise,
	   * we get a ping-pong behaviour.
	   */
	  /* TODO: make sure the pipe_buf sets a wakeup point for us
	   * also at this (9/10) location.  in case we are not
	   * transmitting data, we should get awoken when being almost
	   * full, and not when completely full and thus ultra-urgent.
	   */

	  lwroc_skip_nonsticky_data(item,
				    (ssize_t) (buf_fill - 4 * buf_sz / 5));
	}
    }
}

/* Called from network thread to skip data. */

void lwroc_net_skip_data(void)
{
  pd_ll_item *iter;

  /* printf ("lwroc_net_skip_data()...\n"); fflush(stdout); */

  PD_LL_FOREACH(_lwroc_net_trans_serv, iter)
    {
      lwroc_net_trans_unit *unit =
	PD_LL_ITEM(iter, lwroc_net_trans_unit, _servers);

      lwroc_net_trans_skip_data(unit);
    }

  if (_lwroc_readout_data_cons_dummy)
    lwroc_skip_data(_lwroc_readout_data_cons_dummy,
		    _lwroc_net_io_thread->_block);

  if (_lwroc_filter_data_cons_dummy)
    lwroc_skip_data(_lwroc_filter_data_cons_dummy,
		    _lwroc_net_io_thread->_block);
}

/********************************************************************/

/* When a client is operated by the server thread, we react to failure
 * by putting the source back in the generic network thread, which
 * will deal with the failure properly (being able to access all unit
 * structures etc).
 */

int lwroc_net_trans_after_fail_thread(lwroc_select_item *item,
				      lwroc_select_info *si)
{
  lwroc_net_trans_client *client =
    PD_LL_ITEM(item, lwroc_net_trans_client, _base._select_item);

  client->_thread_block = NULL;
  /* Set a timeout, which will trigger the 'disconnect' handling
   * early in net_trans_after_select(), which returns 0, thus making sure
   * net_trans_after_fail() is called.
   */
  client->_next_time = si->now;
  /* Give it the normal select_item_info. */
  client->_base._select_item.item_info =
    &lwroc_net_data_trans_select_item_info;

  /* If we were operated in blocking mode, disable that. */
  lwroc_net_io_nonblock(client->_base._fd, 1, "data dest");

  /* We are no longer processed by this thread. */
  PD_LL_REMOVE(&client->_base._select_item._items);

  /* printf ("network client fail, give back to generic network thread\n"); */

  /* And at last give it to the generic thread. */
  LWROC_ITEM_BUFFER_INSERT(_lwroc_net_trans_info._move[0], client);

  return 1; /* The client shall not be deleted. */
}

void lwroc_net_trans_to_serv_thread(lwroc_net_trans_client *client)
{
  client->_thread_block = _lwroc_net_trans_thread->_block;

  /* Give it the special select_item_info. */
  client->_base._select_item.item_info =
    &lwroc_net_data_trans_loop_select_item_info;

  /* We are no longer processed by this thread. */
  PD_LL_REMOVE(&client->_base._select_item._items);

  /* printf ("network client -> data network thread\n"); */

  /* If there is only one client, we can operate the i/o in a blocking
   * manner.  This reduces the number of calls to write()
   * considerably.
   */
  if (_lwroc_net_trans_info._num_clients == 1)
    {
      /* printf ("using blocking writes\n"); */
      lwroc_net_io_nonblock(client->_base._fd, 0, "data dest");
    }

  /* And at last give it to the server thread. */
  LWROC_ITEM_BUFFER_INSERT(_lwroc_net_trans_info._move[1], client);
}

/********************************************************************/

void lwroc_net_trans_client_free_bufchunks(lwroc_net_trans_client *client)
{
  if (client->_bufchunks)
    free(client->_bufchunks);
  client->_bufchunks = NULL;
}

int lwroc_net_trans_after_fail(lwroc_select_item *item,
			       lwroc_select_info *si)
{
  lwroc_net_trans_client *client =
    PD_LL_ITEM(item, lwroc_net_trans_client, _base._select_item);
  lwroc_net_trans_unit *unit;

  /* printf ("network client fail in generic network thread\n"); */

  /* In case we were working on transmitting a buffer, we do not know
   * how much was actually received.  Since we do *not* want to send
   * any data twice, mark it as used.  See similar behaviour in the
   * file writer.
   */

  if (client->_item->_unit->_hold != LWROC_NET_SERVER_HOLD_NONE)
    {
      /* If we are in hold mode, and have collected a partial buffer,
       * but not sent it yet, then the full replay sticky store will
       * have advanced through the collected data.  Make sure it is
       * not used for collection again.
       *
       * This does mean that these events are permanently lost.  But
       * since we just lost a connection, we have also lost whatever
       * was sent but not handled too...
       */
      client->_bufchunks->fully_used = client->_bufchunks->collect_used;
    }
  /*
  printf("release (after-fail) %zd  %zd\n",
	 client->bufchunks.collect_used,
	 client->bufchunks.fully_used);
  */
  lwroc_gdf_buffer_chunks_release(client->_bufchunks,
				  client->_item->source,
				  NULL);

  unit = client->_item->_unit;

  if (client->_base._mon)
    {
      client->_base._mon->_block._status = LWROC_CONN_STATUS_NOT_CONNECTED;
      LWROC_MON_CHECK_COPY_CONN_MON_BLOCK(client->_base._mon, 1);
    }

  if (unit->_hold == LWROC_NET_SERVER_HOLD_FULL)
    lwroc_pipe_buffer_set_hold(unit->source_full, &unit->_msg_context);

  PD_LL_REMOVE(&client->_item->_items);
  PD_LL_ADD_BEFORE(&unit->_items_free[client->_item->_only_info],
		   &client->_item->_items);

  if (unit->_push_hostname)
    {
      /* For a push client (which also handles the reconnection):
       *
       * Do not delete the item.  Give it to the reconnect handling.
       * Also do not clear the item connection as done just below
       * this if-statement.
       *
       * Do delete the bufchunks, as they are reallocated for a new
       * connection.
       */
      lwroc_net_trans_client_free_bufchunks(client);

      client->_base._select_item.item_info =
	&lwroc_net_data_trans_conn_select_item_info;

      return lwroc_net_trans_conn_after_fail(item, si);
    }

  client->_item->_client = NULL;
  client->_item = NULL;

  return lwroc_net_client_base_after_fail(item, si);
}

void lwroc_net_trans_delete(lwroc_select_item *item)
{
  lwroc_net_trans_client *client =
    PD_LL_ITEM(item, lwroc_net_trans_client, _base._select_item);

  lwroc_net_trans_client_free_bufchunks(client);

  lwroc_net_client_base_delete(item);
}

/********************************************************************/

/* Note! the ..._setup_select function may be called either from the
 * general server thread, or a dedicated network thread.  Take care
 * with blocking and other structure members.
 */

int lwroc_net_trans_delay_send(lwroc_net_trans_client *client)
{
  size_t fill =
    lwroc_pipe_buffer_fill_due_to_consumer(client->_item->source);
  size_t hi_mark =
    (lwroc_pipe_buffer_size_from_consumer(client->_item->source) * 9) / 10;
  size_t lo_mark =
    (lwroc_pipe_buffer_size_from_consumer(client->_item->source) * 5) / 10;
  /*
  printf ("%zd  %zd  %zd  : %d\n",
	  fill, hi_mark, lo_mark, client->_lcl_force_eb);
  */
  if (client->_lcl_force_eb)
    {
      /* We were over the mark before.  So we continue building
       * until we get below the lower mark.
       */
      if (fill < lo_mark)
	client->_lcl_force_eb = 0;
    }
  else
    {
      /* We are approaching from below.  Hold building until we
       * reach the mark.
       */
      if (fill >= hi_mark)
	client->_lcl_force_eb = 1;
    }
  /*
  printf (" : %d\n",
	  client->_lcl_force_eb);
  */

  if (client->_lcl_force_eb)
    return 0;

  /* Since we are holding transmissions, we need the buffer to notify
   * us in case it passes the high mark, such that we start
   * transmitting immediately, and do not wait until we have the next
   * timeout.
   */

  /* We cannot use lwroc_pipe_buffer_avail_read() since the return
   * value has been cut down to the linear space, and thus would not
   * tell us the fill level.
   */
  /*
  printf ("req notify\n");
  */
  lwroc_pipe_buffer_notify_consumer(client->_item->source,
				    client->_thread_block ?
				    client->_thread_block :
				    _lwroc_net_io_thread->_block,
				    hi_mark);

  /* Did it fill up while we requested notification? */

  fill =
    lwroc_pipe_buffer_fill_due_to_consumer(client->_item->source);

  if (fill >= hi_mark)
    {
      client->_lcl_force_eb = 1;
      return 0;
    }

  /* We do not care about resetting the force_eb in case the spill
   * goes off.  In that case, we should basically be clearing out
   * the buffer during the off-spill period, at least below the low
   * mark, which will then stop the building.
   */
  return 1;
}

void lwroc_net_trans_setup_select(lwroc_select_item *item,
				  lwroc_select_info *si)
{
  lwroc_net_trans_client *client =
    PD_LL_ITEM(item, lwroc_net_trans_client, _base._select_item);

  /* Timeout for other end to close the connection? */
  if (client->_next_time.tv_sec)
    lwroc_select_info_time_setup(si, &client->_next_time);

  /* We never read anything (discard), but we check it for errors. */

  LWROC_READ_FD_SET(client->_base._fd, si);

  /* Should we hold the transmission for delayed event building. */

  if (/* We should want to do it. */
      (client->_item->_unit->_spilldelay & LWROC_NET_SERVER_DELAYED_ON) &&
      /* Delayed EB shall be enabled from triva state, but no force request. */
      (_lwroc_delayed_eb_status & (LWROC_DELAYED_EB_STATUS_ENABLED |
				   LWROC_DELAYED_EB_STATUS_FORCED)) ==
      LWROC_DELAYED_EB_STATUS_ENABLED &&
      /* Global spill-is-on marker. */
      (_lwroc_spill_state & LWROC_SPILL_STATE_ON) &&
      /* And we are not 'stuck' in-spill (likely missing off-spill marks). */
      !_lwroc_spill_state_stuck &&
      /* We should not delay the initial info buffer. */
      client->_last_write.tv_sec != (time_t) -1 &&
      /* Finally the logic to force ourselves before buffer becomes full. */
      lwroc_net_trans_delay_send(client))
    return;

  /* Do we have data to write? */

  if (!client->_base._buf._ptr &&
      !lwroc_gdf_buffer_chunks_pending(client->_bufchunks))
    {
      int partial_buffer;
      struct timeval timeout;
      struct timeval force_time;

      /* If we are a portmap connection, or if we already had used all
       * client connections, then do not find data, just wait to die.
       */

      if (client->_item->_only_info)
	return;

      /* Before considering more real data, clean up the sticky store.
       */

      if (client->_item->_unit->_fmt->_buffer_format ==
	  LWROC_DATA_TRANSPORT_FORMAT_LMD &&
	  lwroc_lmd_sticky_store_has_chunks(client->_item->sticky_store))
	{
	  /* printf ("May have sticky!\n"); */

	  /* The if-statement above checks that it is LMD format.
	   * TODO: Not really clean to be format-dependent and do
	   * casts like this.
	   */
	  lwroc_lmd_buffer_chunks *lmd_bufchunks =
	    (lwroc_lmd_buffer_chunks *) client->_bufchunks;

	  /* May be just empty meta-data, check by compacting. */
	  lwroc_lmd_sticky_store_compact_meta(client->_item->sticky_store);
	  lwroc_lmd_sticky_store_populate_hash(client->_item->sticky_store);

	  if (lwroc_lmd_sticky_store_has_chunks(client->_item->sticky_store))
	    {
	      size_t sticky_iter_ev_offset = 0;
	      /* There really is data! */
	      /* Since we will be using the sticky store as a source, we
	       * need to compact the data as well.
	       */
	      /* TODO: this is not really good when we do it due to
	       * having a lot of sticky stuff, and dumping it in
	       * multiple buffers.  We then recompact a lot of times.
	       * We should have some kind of flag that we have not
	       * added things to the sticky store.  If we have not, then any
	       * compacting is not necessary!  In that case we need to
	       * retain sticky_iter_ev_offset!
	       */
	      /* printf ("Have sticky!\n"); */
	      lwroc_lmd_sticky_store_compact_data(client->_item->sticky_store);

	      lwroc_lmd_sticky_store_write_chunks(client->_item->sticky_store,
						  /*client->_*/lmd_bufchunks,
						  &sticky_iter_ev_offset, 1);

	      /* printf ("Sticky! %zd\n", sticky_iter_ev_offset); */
	      /* Since lbc->fully_used is not set (i.e. still at 0),
	       * is is harmless to call lwroc_lmd_buffer_chunks_release().
	       * (No real data will be released).
	       */
	      /*
	      printf ("st buffer no: %d  (%d)\n",
		      ((uint32_t *) client->bufchunks.chunks[LWROC_LMD_BUF_CHUNK_BUFHE].ptr)[3],
		      ((uint32_t *) client->bufchunks.chunks[LWROC_LMD_BUF_CHUNK_DATA_1].ptr)[3]);
	      */
	      goto data_found;
	    }
	}

      partial_buffer = 0;

      if (client->_item->_unit->_flush_timeout)
	{
	  timeout.tv_sec = client->_item->_unit->_flush_timeout;
	  timeout.tv_usec = 0;
	}
      else if (client->_bufchunks->var_buffer_size !=
	       LWROC_GDF_BUF_VARBUFSIZE_NO ||
	       client->_bufchunks->buffer_size <=
	       2 * LWROC_SMALL_TRANS_BUFFER_BUF_SIZE)
	{
	  /* For protocols that do trim buffer size,
	   * or have really small buffers.
	   */
	  timeout.tv_sec = 0;
	  timeout.tv_usec = LWROC_VARBUF_PARTIAL_BUFFER_TIMEOUT_US;
	}
      else
	{
	  timeout.tv_sec = LWROC_FULLBUF_PARTIAL_BUFFER_TIMEOUT;
	  timeout.tv_usec = 0;

	  /* For really large (and likely wasteful) buffer sizes,
	   * we increase the timeout further...
	   */
	  if (client->_bufchunks->buffer_size >=
	      LWROC_LARGE_TRANS_BUFFER_BUF_SIZE)
	    timeout.tv_sec *= 2;
	}

      timeradd(&client->_last_write, &timeout, &force_time);

      if (timercmp(&si->now, &client->_last_write, <) ||
	  timercmp(&si->now, &force_time, >))
	partial_buffer = 1;

      /* This will not necessarily provide data to write. */

      if (client->_item->_unit->_hold != LWROC_NET_SERVER_HOLD_NONE)
	{
	  /* Hold mode, we only have one source to track.  And we get
	   * the full-replay stickes from that one.
	   */
	  client->_item->_unit->_fmt->
	    chunks_collect(client->_bufchunks,
			   client->_item->source,
			   client->_thread_block ?
			   client->_thread_block :
			   _lwroc_net_io_thread->_block,
			   NULL,
			   client->_item->_unit->
			   /* */ sticky_store_full,
			   partial_buffer, 0);
	}
      else
	{
	  /* Normal clients. */
	  client->_item->_unit->_fmt->
	    chunks_collect(client->_bufchunks,
			   client->_item->source,
			   client->_thread_block ?
			   client->_thread_block :
			   _lwroc_net_io_thread->_block,
			   NULL,
			   NULL,
			   partial_buffer, 0);
	}

      if (!lwroc_gdf_buffer_chunks_pending(client->_bufchunks))
	{
	  timeout.tv_sec = LWROC_KEEPALIVE_BUFFER_TIMEOUT;
	  timeout.tv_usec = 0;

	  timeradd(&client->_last_write, &timeout, &force_time);

	  if (timercmp(&si->now, &client->_last_write, <) ||
	      timercmp(&si->now, &force_time, >))
	    {
	      /* Prepare an empty buffer. */
	      client->_item->_unit->_fmt->chunks_empty(client->_bufchunks);
	      /* printf ("keep-alive\n"); */
	    }
	  else
	    return;
	}
      /*
      printf ("buffer no: %d  (%d)\n",
	      ((uint32_t *) client->bufchunks.chunks[LWROC_LMD_BUF_CHUNK_BUFHE].ptr)[3],
	      ((uint32_t *) client->bufchunks.chunks[LWROC_LMD_BUF_CHUNK_DATA_1].ptr)[3]);
      */
    }

 data_found:
  /* We have data to write. */

  /* printf ("have data %p\n", client->_thread_block); */

  LWROC_WRITE_FD_SET(client->_base._fd, si);
}

void lwroc_net_trans_wrote_buffer(lwroc_net_trans_client *client)
{
  if (client->_last_write.tv_sec != (time_t) -1)
    return;

  /* We have sent the first buffer = info header. */
  client->_last_write.tv_sec = 0;
  /* Remove hold context.  For a transport server this does not
   * guarantee that the destination is listening.  But at least better
   * here than to remove already in become_trans().
   */
  if (client->_item->_unit->_hold == LWROC_NET_SERVER_HOLD_FULL)
    lwroc_pipe_buffer_set_hold(client->_item->_unit->source_full, NULL);
}

/* Note! the ..._after_select function may be called either from the
 * general server thread, or a dedicated network thread.  Take care
 * with blocking and other structure members.
 */

int lwroc_net_trans_after_select(lwroc_select_item *item,
				 lwroc_select_info *si)
{
  lwroc_net_trans_client *client =
    PD_LL_ITEM(item, lwroc_net_trans_client, _base._select_item);
  ssize_t n;

  /* TODO: this should really be after all the processing, if we keep
   * track of amount of data sent/received.
   */
  if (client->_base._mon)
    LWROC_MON_CHECK_COPY_CONN_MON_BLOCK(client->_base._mon, 0);

  if (client->_next_time.tv_sec &&
      timercmp(&si->now, &client->_next_time, >))
    {
      /* printf("timer\n"); */
      return 0;
    }

  /* TODO: for now a very dirty implementation of the stream server.
   * We just discard whatever the client writes to us...
   *
   *
   * This has to be fixed, because right now, we'll also write data to
   * the client directly, which it never reads in case it uses the
   * port info and connects to the data port...  (only matters in hold
   * mode, though...)
   */

  /* TODO: when operating in the data server thread, and with blocking
   * i/o, we need to set the i/o as unblocking around doing any reads.
   */

  if (client->_item->_unit->_kind == LWROC_NET_SERVER_KIND_STREAM)
    {
      if (!lwroc_discard_read_check_eof(client->_base._fd, si, NULL))
	{
	  /* printf ("read 0\n"); */
	  return 0;
	}
    }
  else
    {
      /* For trans and drasi, we expect exactly no data, so die if we
       * ever get anything.
       */
      if (!lwroc_fail_on_read_check_eof(client->_base._fd, si))
	{
	  /* printf ("read 0\n"); */
	  return 0;
	}
     }

  /* If we are using blocking writes, we may actually have just filled
   * up the buffer and then select() will not report the file
   * descriptor as ready for writing.  Which means that we cycle
   * around here multiple times.  No major problem, as we then still
   * only come here once per timeout.
   */
  if (!LWROC_WRITE_FD_ISSET(client->_base._fd, si))
    return 1;

  if (client->_base._buf._ptr)
    {
      uint64_t account = 0;

      /* Write from local buffer. */

      int ret = lwroc_net_client_base_write(&client->_base,
					    "transport",
					    &account);

      _lwroc_mon_net._sent_data += account;
      if (client->_base._mon)
	client->_base._mon->_block._bytes += account;

      if (client->_base._buf._offset ==
	  client->_base._buf._size)
	{
	  client->_base._buf._ptr = NULL;

	  lwroc_net_trans_wrote_buffer(client);
	}

      return ret;
    }

  n = lwroc_gdf_buffer_chunks_write(client->_bufchunks,
				    lwroc_write_fd_no_sigpipe,
				    &client->_base._fd, NULL);

  if (n == -1)
    {
      if (errno == EINTR ||
	  errno == EAGAIN)
	return 1;
      if (client->_item->_unit->_kind == LWROC_NET_SERVER_KIND_DRASI)
	{
	  /* For drasi kind client we use a higher severity, since
	   * they are internal.
	   */
	  if (errno == EPIPE) {
	    LWROC_CERROR(&client->_base,
			 "Client has disconnected, socket closed.");
	  } else {
	    LWROC_CPERROR(&client->_base ,"write");
	    LWROC_CERROR(&client->_base,
			 "Error while writing to client (disconnect?).");
	  }
	}
      else
	{
	  if (errno == EPIPE) {
	    LWROC_CINFO(&client->_base,
			"Client has disconnected, socket closed.");
	  } else {
	    /* This error also happens when clients disconnect. */
	    /* LWROC_CPERROR(&client->_base ,"write"); */
	    LWROC_CINFO(&client->_base,
			"Error while writing to client (disconnect?).");
	  }
	}
      return 0;
    }

  /* Write was successful. */

  _lwroc_mon_net._sent_data += (uint64_t) n;
  if (client->_base._mon)
    client->_base._mon->_block._bytes += (uint64_t) n;

  if (!lwroc_gdf_buffer_chunks_pending(client->_bufchunks))
    {
      /* printf("release (%d)\n", client->bufchunks.fixed.bufhe.l_buf); */
      /* We are done with this buffer. */
      lwroc_gdf_buffer_chunks_release(client->_bufchunks,
				      client->_item->source,
				      NULL);

      lwroc_net_trans_wrote_buffer(client);

      if (client->_item->_only_info) /* Timeout to tear connection down. */
	{
	  client->_next_time = si->now;
	  client->_next_time.tv_sec += LWROC_TRANS_CLOSE_TIMEOUT;
	}
      else if (!client->_thread_block &&
	       client->_item->_unit->_hold != LWROC_NET_SERVER_HOLD_NONE)
	{
	  /* This thread is now processing data, move it to the
	   * dedicated server thread, to avoid the overhead of
	   * studying all clients of the generic network thread for
	   * each write() call.
	   *
	   * TODO: This cannot be used if we are not in hold mode,
	   * since the emergency skip of data when the buffer is about
	   * to become full is still handled by the main thread.
	   * (Move that handling?)
	   */
	  lwroc_net_trans_to_serv_thread(client);
	}

      client->_last_write = si->now;
    }

  return 1;
}

/********************************************************************/

const char *lwroc_net_trans_unit_kind_str(const lwroc_net_trans_unit *unit,
					  int *port)
{
  switch (unit->_kind)
    {
    case LWROC_NET_SERVER_KIND_DRASI:
      *port = -1;
      return "drasi";

    case LWROC_NET_SERVER_KIND_TRANS:
      *port =
	(unit->_server_port == LMD_TCP_PORT_TRANS) ? -1 : unit->_server_port;
      return "trans";

    case LWROC_NET_SERVER_KIND_STREAM:
      *port =
	(unit->_server_port == LMD_TCP_PORT_STREAM) ? -1 : unit->_server_port;
      return "stream";

    case LWROC_NET_SERVER_KIND_EBYE_PUSH:
      *port =
	(unit->_server_port ==
	 LMD_TCP_PORT_EBYE_XFER_PUSH) ? -1 : unit->_server_port;
      return "ebye-push";

    case LWROC_NET_SERVER_KIND_EBYE_PULL:
      *port =
	(unit->_server_port ==
	 LMD_TCP_PORT_EBYE_XFER_PULL) ? -1 : unit->_server_port;
      return "ebye-pull";

    default:
      return "(unknown)";
    }
}

char *lwroc_net_trans_unit_fmt_msg_context(char *buf, size_t size,
					   const  void *ptr)
{
  const lwroc_net_trans_unit *unit = (const lwroc_net_trans_unit *)
    (ptr - offsetof(lwroc_net_trans_unit, _msg_context));
  int port = -1;
  const char *kind_str;

  kind_str = lwroc_net_trans_unit_kind_str(unit, &port);

  if (port == -1)
    return lwroc_message_fmt_msg_context(buf, size,
					 "%s", kind_str);
  else
    return lwroc_message_fmt_msg_context(buf, size,
					 "%s:%d", kind_str, port);
}

/********************************************************************/

char *lwroc_net_trans_client_fmt_msg_context(char *buf, size_t size,
					     const  void *ptr)
{
  const lwroc_net_trans_client *client = (const lwroc_net_trans_client *)
    (ptr - offsetof(lwroc_net_trans_client, _base._msg_context));
  char dotted[INET6_ADDRSTRLEN];
  lwroc_net_trans_unit *unit;
  char *p;

  if (client->_item)
    {
      unit = client->_item->_unit;

      p = lwroc_net_trans_unit_fmt_msg_context(buf, size, &unit->_msg_context);
      size -= (size_t) (p - buf);
    }
  else
    p = buf;

  lwroc_inet_ntop(&client->_base._addr, dotted, sizeof (dotted));

  p = lwroc_message_fmt_msg_context(p, size,
				    ": %s", dotted);

  return p;
}

/********************************************************************/

lwroc_select_item_info lwroc_net_data_trans_select_item_info =
{
  lwroc_net_trans_setup_select,
  lwroc_net_trans_after_select,
  lwroc_net_trans_after_fail,
  lwroc_net_trans_delete,
  0,
};

/********************************************************************/

/* This is used when a client is in operated by the server thread. */
lwroc_select_item_info lwroc_net_data_trans_loop_select_item_info =
{
  lwroc_net_trans_setup_select,
  lwroc_net_trans_after_select,
  lwroc_net_trans_after_fail_thread,
  NULL,
  0,
};

/********************************************************************/

int lwroc_net_trans_assign_client_item(lwroc_net_trans_unit *unit,
				       lwroc_net_trans_client *client,
				       int only_info,
				       int change_to_used_list)
{
  pd_ll_item *iter;
  lwroc_net_trans_item *item;

  if (PD_LL_IS_EMPTY(&unit->_items_free[only_info]))
    return 0;

  /* Use a free slot. */

  iter = PD_LL_FIRST(unit->_items_free[only_info]);
  item = PD_LL_ITEM(iter, lwroc_net_trans_item, _items);

  if (change_to_used_list)
    {
      PD_LL_REMOVE(&item->_items);
      PD_LL_ADD_BEFORE(&unit->_items_used[only_info], &item->_items);
    }

  assert(item->_only_info == only_info);

  client->_item = item;
  client->_item->_client = client;

  return 1;
}

int lwroc_net_become_trans(lwroc_net_trans_unit *unit,
			   lwroc_net_trans_client *client,
			   int info_hint,
			   uint32_t info_hint2,
			   int var_buffer_size)
{
  lwroc_net_trans_item *item;
  int only_info = (info_hint != 0);
  char dotted[INET6_ADDRSTRLEN];

  /* printf ("become trans: %d\n",only_info); */

  if (!lwroc_net_trans_assign_client_item(unit, client, only_info, 1))
    return 0;

  item = client->_item;

  assert(client->_bufchunks == NULL);
  client->_bufchunks = unit->_fmt->chunks_alloc(LWROC_GDF_FLAGS_NET);

  client->_thread_block = NULL;

  client->_next_time.tv_sec = 0;
  client->_next_time.tv_usec = 0;
  client->_last_write.tv_sec = (time_t) -1;
  client->_last_write.tv_usec = 0;

  client->_base._buf._size = client->_base._buf._offset = 0;
  client->_base._buf._ptr = NULL;

  client->_base._msg_context = lwroc_net_trans_client_fmt_msg_context;

  if (!only_info)
    {
      /* If we are not in hold mode, synchronise to the full recovery
       * source, and also copy the full recovery sticky store (which
       * then will be sent).
       */
      if (unit->_hold != LWROC_NET_SERVER_HOLD_FULL)
	{
	  /* First we have to ensure that the full buffer tracking is at
	   * the end (such that it is later than the item source...
	   *
	   * In weak hold mode, we also skip to the end before
	   * transmitting anything.
	   */
	  lwroc_skip_nonsticky_data_full(unit);
	}

      if (unit->_hold == LWROC_NET_SERVER_HOLD_NONE)
	{
	  lwroc_pipe_buffer_did_read_fast(item->source, unit->source_full);
	}

      /* Compacting the general sticky store is quite harmless. */
      lwroc_lmd_sticky_store_compact_data(unit->sticky_store_full);
      lwroc_lmd_sticky_store_compact_meta(unit->sticky_store_full);
      lwroc_lmd_sticky_store_populate_hash(unit->sticky_store_full);

      lwroc_lmd_sticky_store_copy(item->sticky_store,
				  unit->sticky_store_full);
    }

  /* Setup to send the transport server info block. */

  unit->_fmt->chunks_init(client->_bufchunks, (uint32_t) unit->_bufsize,
			  unit->_kind != LWROC_NET_SERVER_KIND_DRASI);

  /* 1 for drasi clients, 2 for data-port clients. */
  client->_bufchunks->var_buffer_size = var_buffer_size;

  if (unit->_kind == LWROC_NET_SERVER_KIND_DRASI)
    {
      uint32_t max_ev_len;
      int max_event_interval;

      max_ev_len =
	lwroc_data_pipe_get_max_ev_len(unit->data_handle);
      max_event_interval =
	lwroc_data_pipe_get_max_ev_interval(unit->data_handle);

      if (max_event_interval)
	{
	  /* Add the flush time. */
	  if (unit->_flush_timeout)
	    max_event_interval += unit->_flush_timeout;
	  else
	    max_event_interval += 2 * LWROC_FULLBUF_PARTIAL_BUFFER_TIMEOUT;
	}

      lwroc_gdf_buffer_chunks_data_trans_setup(client->_bufchunks,
					       unit->_fmt,
					       max_ev_len,
					       (uint32_t) max_event_interval);
    }
  /* Checks that the cast below to (lwroc_lmd_buffer_chunks *) is ok. */
  else if (unit->_fmt->_buffer_format == LWROC_DATA_TRANSPORT_FORMAT_LMD)
    lwroc_lmd_buffer_chunks_trans_info(client->_bufchunks,
				       info_hint,
				       info_hint2);
  else if (unit->_fmt->_buffer_format == LWROC_DATA_TRANSPORT_FORMAT_EBYE ||
	   unit->_fmt->_buffer_format == LWROC_DATA_TRANSPORT_FORMAT_XFER)
    {
      if (unit->_kind != LWROC_NET_SERVER_KIND_EBYE_PULL)
	info_hint2 = 0;

      /* Send a (start) xfer buffer with data-length -1. */
      lwroc_ebye_buffer_chunks_empty_xfer(client->_bufchunks, 1,
					  info_hint2);
    }
  else
    LWROC_FATAL("lwroc_net_become_trans() called for unit with "
		"unhandled connection setup for buffer data format.");

  if (item->_mon)
    {
      client->_base._mon = item->_mon;
      client->_base._mon->_block._status = LWROC_CONN_STATUS_CONNECTED;
      client->_base._mon->_block._ipv4_addr =
	lwroc_get_ipv4_addr(&client->_base._addr);
      client->_base._mon->_block._port =
	lwroc_get_port(&client->_base._addr);
      LWROC_MON_CHECK_COPY_CONN_MON_BLOCK(client->_base._mon, 1);
    }
  else
    client->_base._mon = NULL;

  /*assert(_lwroc_main_data_cons_trans);
  client->source = _lwroc_main_data_cons_trans;
  _lwroc_main_data_cons_trans = NULL;*/

  lwroc_inet_ntop(&client->_base._addr, dotted, sizeof (dotted));

  LWROC_CINFO_FMT(unit, "Transport %s connected (%s) [%s].",
		  unit->_push_hostname ? "push-server" : "client",
		  !only_info ? "data" :
		  (info_hint == LMD_TCP_INFO_BUFSIZE_MAXCLIENTS ?
		   "reject-maxclients" : "pmap"),
		  dotted);
  /*
  printf ("varbuf: %d  unit->_flush_timeout: %d\n",
	  client->bufchunks.var_buffer_size,
	  unit->_flush_timeout);
  */
  return 1;
}

/********************************************************************/

void lwroc_net_trans_item_init(lwroc_net_trans_unit *unit,
			       int only_info,
			       lwroc_net_reverse_link *rev_link)
{
  lwroc_net_trans_item *item;

  item = (lwroc_net_trans_item *) malloc (sizeof (lwroc_net_trans_item));

  if (!item)
    LWROC_FATAL("Memory allocation failure (trans item).");

  memset (item, 0, sizeof (*item));

  item->_unit = unit;

  /* With multiple clients in non-hold mode, each client needs a
   * source to track.
   */

  item->source = NULL;
  item->sticky_store = NULL;
  item->_localbuf = NULL;

  item->_only_info = only_info;

  item->_rev_link = rev_link;

  PD_LL_ADD_BEFORE(&unit->_items_free[item->_only_info],
		   &item->_items);
}

/********************************************************************/

void lwroc_trans_server_usage(void)
{
  printf ("\n");
  printf ("--server=<OPTIONS>:\n");
  printf ("\n");
  printf ("  drasi                    Native protocol data server [hold].\n");
  printf ("  trans[:PORT]             Transport protocol data server [weakhold].\n");
  printf ("  stream[:PORT]            Stream protocol data server [nohold].\n");
  printf ("  ebye-push-relay=HOST[:[+]PORT]  Network data source (we connect) [hold].\n");
  printf ("  ebye-pull[:PORT]         Network data source (we accept conns.) [hold].\n");
  printf ("  [no]hold                 All data must be sent to one client.\n");
  printf ("  weakhold                 All data must be sent if some client connected.\n");
  printf ("  [no]spilldelay           Force delayed data transmission off/on.\n");
  printf ("                           (Default is on when only one CPU core available.)\n");
  printf ("  flush=N                  Flush data every N seconds.\n");
  printf ("  forcemap                 Enforce use of portmapping.\n");
  printf ("                           (Do not use legacy ports.)\n");
  printf ("  clients=N                Allow N clients (default 1).\n");
  printf ("  bufsize=N                Size of output protocol buffers (Mi|raw hex).\n");
  printf ("  dest=HOST[:[+]PORT]      Allowed destination instance.\n");
  printf ("  readout                  Use readout/merge buffer as source.\n");
  printf ("  filter                   Use filter buffer as source (only when available).\n");
  printf ("  final                    Use filter buffer as source (when available,\n");
  printf ("                           otherwise readout buffer).  (Default.)\n");
  printf ("  ts-filter                Use buffer of timestamps as source.\n");
  printf ("\n");
}

void lwroc_add_trans_server_cfg(const char *cfg)
{
  lwroc_net_trans_unit *unit;
  int drasi_clients = 0;
  int clients = 0;
  int hold = -1;
  int port = -1;
  int kind = 0;
  int forcemap = 0;
  int spilldelay = 0;
  int flush_timeout = 0;
  size_t bufsize = 0;
  const char *push_hostname = NULL;
  int i;
  const char *request;
  lwroc_parse_split_list parse_info;
  int srcbuf = 0;

  unit = (lwroc_net_trans_unit *) malloc (sizeof (lwroc_net_trans_unit));

  if (!unit)
    LWROC_FATAL("Memory allocation failure (trans unit).");

  memset(unit, 0, sizeof (*unit));

  for (i = 0; i < LWROC_NET_TRANS_CLIENTS_NUM; i++)
    {
      PD_LL_INIT(&unit->_items_free[i]);
      PD_LL_INIT(&unit->_items_used[i]);
    }
  /* Initialisition continued below. */

  lwroc_parse_split_list_setup(&parse_info, cfg, ',');

  while ((request = lwroc_parse_split_list_next(&parse_info)) != NULL)
    {
      const char *post;

      if (LWROC_MATCH_C_ARG("help"))
	{
	  lwroc_trans_server_usage();
	  exit(0);
	}
      else if (LWROC_MATCH_C_ARG("drasi"))
	kind = LWROC_NET_SERVER_KIND_DRASI;
      else if (LWROC_MATCH_C_ARG("trans"))
	kind = LWROC_NET_SERVER_KIND_TRANS;
      else if (LWROC_MATCH_C_PREFIX("trans:",post))
	{
	  kind = LWROC_NET_SERVER_KIND_TRANS;
	  port = lwroc_parse_hex_u16(post, "trans port number");
	}
      else if (LWROC_MATCH_C_ARG("stream"))
	kind = LWROC_NET_SERVER_KIND_STREAM;
      else if (LWROC_MATCH_C_PREFIX("stream:",post))
	{
	  kind = LWROC_NET_SERVER_KIND_STREAM;
	  port = lwroc_parse_hex_u16(post, "stream port number");
	}
      else if (LWROC_MATCH_C_PREFIX/*NODOC*/("ebye-push=",post) ||
	       LWROC_MATCH_C_PREFIX("ebye-push-relay=",post) ||
	       LWROC_MATCH_C_PREFIX/*NODOC*/("xfer-push-relay=",post)/*remove*/)
	{
	  /* ebye-push is unified/old name for ebye-push-relay */
	  kind = LWROC_NET_SERVER_KIND_EBYE_PUSH;
	  push_hostname = strdup_chk(post);
	}
      else if (LWROC_MATCH_C_ARG("ebye-pull") ||
	       LWROC_MATCH_C_ARG/*NODOC*/("xfer-pull")/*remove*/)
	kind = LWROC_NET_SERVER_KIND_EBYE_PULL;
      else if (LWROC_MATCH_C_PREFIX("ebye-pull:",post) ||
	       LWROC_MATCH_C_PREFIX/*NODOC*/("xfer-pull:",post)/*remove*/)
	{
	  kind = LWROC_NET_SERVER_KIND_EBYE_PULL;
	  port = lwroc_parse_hex_u16(post, "ebye-pull port number");
	}
      else if (LWROC_MATCH_C_ARG("hold"))
	hold = LWROC_NET_SERVER_HOLD_FULL;
      else if (LWROC_MATCH_C_ARG("nohold"))
	hold = LWROC_NET_SERVER_HOLD_NONE;
      else if (LWROC_MATCH_C_ARG("weakhold"))
	hold = LWROC_NET_SERVER_HOLD_WEAK;
      else if (LWROC_MATCH_C_ARG("spilldelay"))
	spilldelay =
	  LWROC_NET_SERVER_DELAYED_ON | LWROC_NET_SERVER_DELAYED_USER;
      else if (LWROC_MATCH_C_ARG("nospilldelay"))
	spilldelay =
	  LWROC_NET_SERVER_DELAYED_OFF | LWROC_NET_SERVER_DELAYED_USER;
      else if (LWROC_MATCH_C_ARG("forcemap"))
	forcemap = 1;
      else if (LWROC_MATCH_C_PREFIX("flush=",post))
	{
	  flush_timeout = lwroc_parse_time(post, "flush timeout");
	  if (flush_timeout < 1 ||
	      flush_timeout > 30)
	    LWROC_BADCFG_FMT("Refusing flush timeout %d s, "
			     "outside [1,30] s.", flush_timeout);
	}
      else if (LWROC_MATCH_C_PREFIX("clients=",post))
	{
	  clients = (int) lwroc_parse_hex_limit(post, "clients",
						1, 1000 /* arb. limit */);
	}
      else if (LWROC_MATCH_C_PREFIX("bufsize=",post))
	{
	  bufsize = (size_t) lwroc_parse_hex(post, "protocol buffer size");
	}
      else if (LWROC_MATCH_C_PREFIX("dest=",post))
	{
	  lwroc_net_reverse_link *rev_link =
	    lwroc_add_reverse_link(post, LWROC_REQUEST_DATA_TRANS,
				   0 /* LWROC_MERGE_IN_TYPE_DRASI */);

	  lwroc_net_trans_item_init(unit, 0 /* only_info */, rev_link);
	  drasi_clients++;
	}
      else if (LWROC_MATCH_C_ARG("readout"))
	srcbuf = LWROC_NET_SERVER_SRCBUF_READOUT;
      else if (LWROC_MATCH_C_ARG("filter"))
	srcbuf = LWROC_NET_SERVER_SRCBUF_FILTER;
      else if (LWROC_MATCH_C_ARG("final"))
	srcbuf = LWROC_NET_SERVER_SRCBUF_FINAL;
      else if (LWROC_MATCH_C_ARG("ts-filter"))
	srcbuf = LWROC_NET_SERVER_SRCBUF_TS_FILTER;
      else
	LWROC_BADCFG_FMT("Unrecognised server option: %s", request);
    }

  if (hold == -1)
    {
      /* TODO: introduce a 'weak' hold for transport connections;
       * weak = skip data when no client connected.
       */

      switch (kind)
	{
	case LWROC_NET_SERVER_KIND_DRASI:
	  hold = LWROC_NET_SERVER_HOLD_FULL;
	  break;
	case LWROC_NET_SERVER_KIND_TRANS:
	  hold = LWROC_NET_SERVER_HOLD_WEAK;
	  break;
	default:
	case LWROC_NET_SERVER_KIND_STREAM:
	  hold = LWROC_NET_SERVER_HOLD_NONE;
	  break;
	case LWROC_NET_SERVER_KIND_EBYE_PUSH:
	case LWROC_NET_SERVER_KIND_EBYE_PULL:
	  hold = LWROC_NET_SERVER_HOLD_FULL;
	  break;
	}
    }

  if (hold != LWROC_NET_SERVER_HOLD_NONE && clients > 1)
    {
      LWROC_BADCFG("Server with multiple clients in hold mode "
		   "not supported yet.");
      /* The way they are to work is that each piece of data is only
       * delivered to one client.  But all others need to get the
       * sticky replay data that was skipped...
       */
    }

  if (!kind)
    LWROC_BADCFG_FMT("Server kind (drasi/trans/stream) not specified: %s",
		     cfg);

  if (!srcbuf)
    srcbuf = LWROC_NET_SERVER_SRCBUF_FINAL;
  if (srcbuf == LWROC_NET_SERVER_SRCBUF_FINAL)
    {
      /* Normalise to an existing buffer. */
      if (_config._use_filter[LWROC_FILTER_USER])
	srcbuf = LWROC_NET_SERVER_SRCBUF_FILTER;
      else
	srcbuf = LWROC_NET_SERVER_SRCBUF_READOUT;
    }

  if (srcbuf == LWROC_NET_SERVER_SRCBUF_FILTER &&
      !_config._use_filter[LWROC_FILTER_USER])
    LWROC_BADCFG_FMT("Server cannot source user filter buffer "
		     "without filter: %s",
		     cfg);

  {
    int num_cpus = lwroc_num_cpus();
    int default_spilldelay = LWROC_NET_SERVER_DELAYED_ON;

    printf ("CPUS: %d\n", num_cpus);

    /* On a machine with multiple cores, the readout can hog one while
     * another does network traffic.  Only second order effects affect
     * the readout: memory access contention, and network interrupts
     * hitting the readout core.
     *
     * This would be fooled by a single-core SMT machine.  But if you
     * run your readout with that, the spilldelay option is for you...  :-)
     */
    if (num_cpus > 1)
      default_spilldelay = LWROC_NET_SERVER_DELAYED_OFF;

    if (!spilldelay)
      spilldelay = default_spilldelay;
    else if ((spilldelay & (LWROC_NET_SERVER_DELAYED_ON |
			    LWROC_NET_SERVER_DELAYED_OFF)) !=
	     default_spilldelay)
      LWROC_WARNING_FMT("[no]spilldelay has no effect, "
			"same as default with %d cores.",
			num_cpus);
  }

  if (port == -1)
    {
      if (kind == LWROC_NET_SERVER_KIND_TRANS)
	port = LMD_TCP_PORT_TRANS;
      else if (kind == LWROC_NET_SERVER_KIND_STREAM)
	port = LMD_TCP_PORT_STREAM;
      else if (kind == LWROC_NET_SERVER_KIND_EBYE_PUSH)
	port = LMD_TCP_PORT_EBYE_XFER_PUSH;
      else if (kind == LWROC_NET_SERVER_KIND_EBYE_PULL)
	port = LMD_TCP_PORT_EBYE_XFER_PULL;
    }

  if (push_hostname &&
      clients)
    LWROC_BADCFG("Number of clients cannot be specified with "
		 "push hostname, only one handled.");

  if (kind == LWROC_NET_SERVER_KIND_DRASI)
    {
      if (clients)
	LWROC_BADCFG("Number of clients cannot be specified with "
		     "drasi server, use explicit dest option.");
      if (!drasi_clients)
	LWROC_BADCFG("Use dest option to specify drasi clients.");
    }
  else
    {
      if (!clients)
	clients = 1;
      if (drasi_clients)
	LWROC_BADCFG("Explicit dest option can only be used with "
		     "drasi server.");
    }

  if (push_hostname &&
      kind != LWROC_NET_SERVER_KIND_EBYE_PUSH)
    LWROC_BADCFG("Hostname only allowed for ebye/xfer-push servers.");

  for (i = 0; i < LWROC_NET_TRANS_SERVER_NUM; i++)
    unit->_server_fd[i] = -1;
  unit->_kind = kind;
  unit->_server_port = port;
  unit->_data_port = 0;
  unit->_hold = hold;
  unit->_spilldelay = spilldelay;
  unit->_forcemap = forcemap;
  unit->_flush_timeout = flush_timeout;
  unit->_clients = clients + drasi_clients;
  unit->_bufsize = bufsize;
  unit->_srcbuf = srcbuf;

  printf ("delay: %d\n", unit->_spilldelay);

  unit->_msg_context = lwroc_net_trans_unit_fmt_msg_context;

  if (kind == LWROC_NET_SERVER_KIND_DRASI)
    {
      if (_lwroc_drasi_server)
	LWROC_BADCFG("Cannot start two drasi servers."); /*For separate pipes?*/
      _lwroc_drasi_server = unit;
    }

  unit->_push_hostname = push_hostname;

  PD_LL_ADD_BEFORE(&_lwroc_net_trans_serv, &unit->_servers);
}

int lwroc_net_trans_servers_clients(int *readout_clients,
				    int *filter_clients,
				    int *ts_filter_clients)
{
  pd_ll_item *iter;
  int tot_clients;

  _lwroc_net_trans_info._num_clients = 0;

  PD_LL_FOREACH(_lwroc_net_trans_serv, iter)
    {
      lwroc_net_trans_unit *unit =
	PD_LL_ITEM(iter, lwroc_net_trans_unit, _servers);
      int *pclients = NULL;

      if (unit->_srcbuf == LWROC_NET_SERVER_SRCBUF_READOUT)
	pclients = readout_clients;
      if (unit->_srcbuf == LWROC_NET_SERVER_SRCBUF_FILTER)
	pclients = filter_clients;
      if (unit->_srcbuf == LWROC_NET_SERVER_SRCBUF_TS_FILTER)
	pclients = ts_filter_clients;
      assert(pclients != NULL);

      /* If we are in hold mode, we track the data exactly, so no need for
       * multiple sources.
       */
      (*pclients)++;
      if (unit->_hold == LWROC_NET_SERVER_HOLD_NONE)
	(*pclients) += unit->_clients;

      _lwroc_net_trans_info._num_clients += (size_t) unit->_clients;
    }

  tot_clients = *readout_clients + *filter_clients;

  return tot_clients;
}

void lwroc_net_trans_item_init_pipe(lwroc_net_trans_item *item,
				    lwroc_pipe_buffer_control *pipe_ctrl,
				    int *pipe_cons_no)
{
  lwroc_net_trans_unit *unit = item->_unit;
  uint32_t conn_type = 0;

  if (!item->_only_info)
    {
      if (unit->_hold != LWROC_NET_SERVER_HOLD_NONE)
	item->source = unit->source_full;
      else
	item->source =
	  lwroc_pipe_buffer_get_consumer(pipe_ctrl, (*pipe_cons_no)++);

      item->sticky_store = lwroc_lmd_sticky_store_init();

      if (unit->_hold == LWROC_NET_SERVER_HOLD_NONE)
	{
	  item->_localbuf = malloc (unit->_bufsize);
	  if (!item->_localbuf)
	    LWROC_FATAL("Memory allocation failure "
			"(trans item local buf).");
	  /* Make sure the buffer is touched, such that we do
	   * not get delayed OOM failures.
	   */
	  memset(item->_localbuf, 0, unit->_bufsize);
	}
    }

  if (!item->_only_info)
    {
      struct sockaddr_storage dummy_addr;
      uint32_t kind = 0;

      switch (unit->_kind)
	{
	case LWROC_NET_SERVER_KIND_DRASI:
	  conn_type = LWROC_REQUEST_DATA_TRANS;
	  break;
	case LWROC_NET_SERVER_KIND_TRANS:
	  conn_type = LWROC_CONN_TYPE_TRANS;
	  break;
	case LWROC_NET_SERVER_KIND_STREAM:
	  conn_type = LWROC_CONN_TYPE_STREAM;
	  break;
	case LWROC_NET_SERVER_KIND_EBYE_PUSH:
	  conn_type = LWROC_CONN_TYPE_EBYE_PUSH;
	  break;
	case LWROC_NET_SERVER_KIND_EBYE_PULL:
	  conn_type = LWROC_CONN_TYPE_EBYE_PULL;
	  break;
	default:
	  assert(0);
	  break;
	}

      if (unit->_srcbuf == LWROC_NET_SERVER_SRCBUF_READOUT)
	kind = LWROC_CONN_KIND_SRCBUF_MAIN;
      if (unit->_srcbuf == LWROC_NET_SERVER_SRCBUF_FILTER)
	kind = LWROC_CONN_KIND_SRCBUF_FILTER;
      if (unit->_srcbuf == LWROC_NET_SERVER_SRCBUF_TS_FILTER)
	kind = LWROC_CONN_KIND_SRCBUF_TS_FILTER;
      assert(kind != 0);

      /* Port number (local) if no reverse link. */
      /* Need to set AF in order to store port number. */
      lwroc_set_ipv4_addr(&dummy_addr, 0);
      lwroc_set_port(&dummy_addr, (uint16_t) unit->_server_port);

      item->_mon =
	lwroc_net_conn_monitor_init(conn_type,
				    LWROC_CONN_DIR_INCOMING, NULL, NULL,
				    item->_rev_link ?
				    &item->_rev_link->_out._base._addr :
				    &dummy_addr,
				    NULL, item->source,
				    NULL, NULL, NULL, NULL, kind, 0);

      if (unit->_kind == LWROC_NET_SERVER_KIND_TRANS ||
	  unit->_kind == LWROC_NET_SERVER_KIND_STREAM ||
	  unit->_kind == LWROC_NET_SERVER_KIND_EBYE_PUSH ||
	  unit->_kind == LWROC_NET_SERVER_KIND_EBYE_PULL)
	{
	  item->_mon->_block._status = LWROC_CONN_STATUS_NOT_CONNECTED;
	  LWROC_MON_CHECK_COPY_CONN_MON_BLOCK(item->_mon, 1);
	}
    }
}

void lwroc_net_check_bufsize_limits(size_t bufsize,
				    size_t pipe_size, uint32_t max_ev_len,
				    lwroc_format_message_context *context,
				    const char *purpose)
{
  if (bufsize < LWROC_MIN_TRANS_BUFFER_BUF_SIZE)
    LWROC_CCBADCFG_FMT(context,
		       "Refusing %s buffer size "
		       "%" MYPRIzd " < %d.",
		       purpose, bufsize, LWROC_MIN_TRANS_BUFFER_BUF_SIZE);

  if (bufsize > LWROC_MAX_TRANS_BUFFER_BUF_SIZE)
    LWROC_CCBADCFG_FMT(context,
		       "Refusing %s buffer size "
		       "%" MYPRIzd " > %d.",
		       purpose, bufsize, LWROC_MAX_TRANS_BUFFER_BUF_SIZE);

  if (bufsize % 1024 != 0)
    LWROC_CCBADCFG_FMT(context,
		       "Refusing %s buffer size "
		       "%" MYPRIzd ", not a multiple of 1024.",
		       purpose, bufsize);

  if (bufsize > pipe_size / 4)
    LWROC_CCBADCFG_FMT(context,
		       "Refusing %s buffer size %" MYPRIzd ", "
		       "larger than a quarter of data pipe size "
		       "%" MYPRIzd "/4=%" MYPRIzd ".",
		       purpose, bufsize, pipe_size,
		       pipe_size / 4);

  if (bufsize > LWROC_TIGHT_TRANS_BUFFER_BUF_SIZE)
    {
      if (bufsize < max_ev_len + LWROC_SMALL_TRANS_BUFFER_BUF_SIZE)
	LWROC_CCBADCFG_FMT(context,
			   "Refusing %s buffer size %" MYPRIzd ", "
			   "smaller than %d + maximum event length, "
			   "%" PRIu32 "=%" PRIu32 ".",
			   purpose, bufsize,
			   LWROC_SMALL_TRANS_BUFFER_BUF_SIZE,
			   max_ev_len,
			   max_ev_len + LWROC_SMALL_TRANS_BUFFER_BUF_SIZE);

      if (bufsize < 2 * max_ev_len)
	LWROC_CCWARNING_FMT(context,
			   "Note: %s buffer size %" MYPRIzd ", "
			   "is smaller than twice maximum event length, "
			   "2*%" PRIu32 "=%" PRIu32 ".",
			   purpose, bufsize, max_ev_len, 2 * max_ev_len);
    }
  else
    {
      if (bufsize < 2 * max_ev_len)
	LWROC_CCBADCFG_FMT(context,
			   "Refusing %s buffer size %" MYPRIzd ", "
			   "smaller than twice maximum event length, "
			   "2*%" PRIu32 "=%" PRIu32 ".",
			   purpose, bufsize, max_ev_len, 2 * max_ev_len);
    }
}

void lwroc_net_trans_unit_init(lwroc_net_trans_unit *unit,
			       lwroc_data_pipe_handle *readout_data_handle,
			       int *readout_pipe_cons_no,
			       lwroc_data_pipe_handle *filter_data_handle,
			       int *filter_pipe_cons_no,
			       lwroc_data_pipe_handle *ts_filter_data_handle,
			       int *ts_filter_pipe_cons_no)
{
  pd_ll_item *iter;
  int i;
  size_t pipe_size;
  lwroc_pipe_buffer_control *pipe_ctrl;
  uint32_t max_ev_len;

  lwroc_data_pipe_handle *data_handle = NULL;
  int *pipe_cons_no = NULL;

  if (unit->_srcbuf == LWROC_NET_SERVER_SRCBUF_READOUT)
    {
      data_handle = readout_data_handle;
      pipe_cons_no = readout_pipe_cons_no;
    }
  if (unit->_srcbuf == LWROC_NET_SERVER_SRCBUF_FILTER)
    {
      data_handle = filter_data_handle;
      pipe_cons_no = filter_pipe_cons_no;
    }
  if (unit->_srcbuf == LWROC_NET_SERVER_SRCBUF_TS_FILTER)
    {
      data_handle = ts_filter_data_handle;
      pipe_cons_no = ts_filter_pipe_cons_no;
    }
  assert(data_handle != NULL);

  pipe_ctrl = lwroc_data_pipe_get_pipe_ctrl(data_handle);

  lwroc_net_trans_item_init(unit, 1 /* only_info */, NULL);

  if (unit->_kind != LWROC_NET_SERVER_KIND_DRASI)
    for (i = 0; i < unit->_clients; i++)
      lwroc_net_trans_item_init(unit, 0 /* only_info */, NULL);

  unit->data_handle = data_handle;

  unit->_fmt = lwroc_data_pipe_get_fmt(data_handle);
  unit->_bufchunks_skip = unit->_fmt->chunks_alloc(LWROC_GDF_FLAGS_NET);

  max_ev_len = lwroc_data_pipe_get_max_ev_len(unit->data_handle);

  pipe_size = lwroc_pipe_buffer_size(pipe_ctrl);

  if (!unit->_bufsize)
    {
      unit->_bufsize = 2 * max_ev_len + 512; /* 512 for headers */
      unit->_bufsize = (uint32_t) lwroc_power_2_round_up(unit->_bufsize);

      if (unit->_bufsize < unit->_fmt->_default_net_buffer_size)
	unit->_bufsize = unit->_fmt->_default_net_buffer_size;

      if (unit->_kind == LWROC_NET_SERVER_KIND_DRASI)
	{
	  size_t pipe_suggest = pipe_size / 16;
	  pipe_suggest = lwroc_power_2_round_up(pipe_suggest);

	  if (pipe_suggest > LWROC_GROW_PIPE_DRASI_BUFFER_BUF_SIZE)
	    pipe_suggest = LWROC_GROW_PIPE_DRASI_BUFFER_BUF_SIZE;

	  if (unit->_bufsize < pipe_suggest)
	    unit->_bufsize = pipe_suggest;
	}
    }

  lwroc_net_check_bufsize_limits(unit->_bufsize,
				 pipe_size, max_ev_len,
				 &(unit->_msg_context),
				 "server protocol");

  /* In hold mode we have only one source.
   */
  unit->source_full =
    lwroc_pipe_buffer_get_consumer(pipe_ctrl, (*pipe_cons_no)++);

  if (unit->_hold == LWROC_NET_SERVER_HOLD_FULL)
    lwroc_pipe_buffer_set_hold(unit->source_full, &unit->_msg_context);

  PD_LL_FOREACH(unit->_items_free[LWROC_NET_TRANS_CLIENTS_NORMAL], iter)
    {
      lwroc_net_trans_item *item;

      item = PD_LL_ITEM(iter, lwroc_net_trans_item, _items);

      lwroc_net_trans_item_init_pipe(item,
				     pipe_ctrl, pipe_cons_no);
    }

  unit->sticky_store_full = lwroc_lmd_sticky_store_init();

  /* For pure skipping, we eat in large chunks (256 MiB). */
  unit->_fmt->chunks_init(unit->_bufchunks_skip, 0x10000000, 0);

  if (unit->_push_hostname)
    lwroc_net_trans_add_push_conn(unit, unit->_push_hostname);
}

void lwroc_net_trans_servers_init(lwroc_data_pipe_handle *readout_data_handle,
				  int *readout_pipe_cons_no,
				  lwroc_data_pipe_handle *filter_data_handle,
				  int *filter_pipe_cons_no,
				  lwroc_data_pipe_handle *ts_filter_data_handle,
				  int *ts_filter_pipe_cons_no)
{
  pd_ll_item *iter;

  PD_LL_FOREACH(_lwroc_net_trans_serv, iter)
    {
      lwroc_net_trans_unit *unit =
	PD_LL_ITEM(iter, lwroc_net_trans_unit, _servers);

      lwroc_net_trans_unit_init(unit,
				readout_data_handle, readout_pipe_cons_no,
				filter_data_handle, filter_pipe_cons_no,
				ts_filter_data_handle, ts_filter_pipe_cons_no);
    }
}

/********************************************************************/

void lwroc_net_trans_unit_bind(lwroc_net_trans_unit *unit)
{
  if (unit->_kind == LWROC_NET_SERVER_KIND_TRANS)
    {
      int pmap_port = unit->_server_port + LMD_TCP_PORT_TRANS_MAP_ADD;

      /* Open the portmap port before the normal port, such that
       * clients do not accidentally get the normal (legacy) port while
       * we are trying to bind the portmap port.
       */

      unit->_server_fd[LWROC_NET_TRANS_SERVER_PMAP] =
	lwroc_net_io_socket_bind(pmap_port, 1, "trans pmap socket", 0);
      unit->_server_fd[LWROC_NET_TRANS_SERVER_DATA] =
	lwroc_net_io_socket_bind(-1, 1, "trans datasocket", 0);
      unit->_data_port =
	lwroc_net_io_socket_get_port(unit->
				     _server_fd[LWROC_NET_TRANS_SERVER_DATA],
				     "trans datasocket");

      if (!unit->_forcemap)
	{
	  unit->_server_fd[LWROC_NET_TRANS_SERVER_STD] =
	    lwroc_net_io_socket_bind(unit->_server_port, 1, "trans socket", 0);
	  LWROC_CINFO_FMT(unit,
			  "Started transport server on port %d.",
			  unit->_server_port);
	}

      LWROC_CINFO_FMT(unit,
		      "Started transport server on port %d, data %d.",
		      pmap_port, unit->_data_port);
    }
  else if (unit->_kind == LWROC_NET_SERVER_KIND_STREAM)
    {
      unit->_server_fd[LWROC_NET_TRANS_SERVER_DATA] =
	lwroc_net_io_socket_bind(-1, 1, "stream datasocket", 0);
      unit->_data_port =
	lwroc_net_io_socket_get_port(unit->
				     _server_fd[LWROC_NET_TRANS_SERVER_DATA],
				     "stream datasocket");
      unit->_server_fd[LWROC_NET_TRANS_SERVER_STD] =
	lwroc_net_io_socket_bind(unit->_server_port, 1, "stream socket", 0);

      LWROC_CINFO_FMT(unit,
		      "Started stream server on port %d, data %d.",
		      unit->_server_port, unit->_data_port);
    }
  else if (unit->_kind == LWROC_NET_SERVER_KIND_EBYE_PULL)
    {
      unit->_server_fd[LWROC_NET_TRANS_SERVER_DATA] =
	lwroc_net_io_socket_bind(-1, 1, "ebye-pull datasocket", 0);
      unit->_data_port =
	lwroc_net_io_socket_get_port(unit->
				     _server_fd[LWROC_NET_TRANS_SERVER_DATA],
				     "ebye-pull datasocket");

      unit->_server_fd[LWROC_NET_TRANS_SERVER_STD] =
	lwroc_net_io_socket_bind(unit->_server_port, 1, "ebye-pull socket", 0);

      LWROC_CINFO_FMT(unit,
		      "Started EBYE (pull) server on port %d, data %d.",
		      unit->_server_port, unit->_data_port);
    }
}


/********************************************************************/

void lwroc_net_trans_unit_setup_select(lwroc_net_trans_unit *unit,
				       lwroc_select_info *si)
{
  int i;

  for (i = 0; i < LWROC_NET_TRANS_SERVER_NUM; i++)
    if (unit->_server_fd[i] != -1)
      {
	if (i == LWROC_NET_TRANS_SERVER_STD &&
	    PD_LL_IS_EMPTY(&unit->_items_free
			   [LWROC_NET_TRANS_CLIENTS_NORMAL]))
	  {
	    /* We have just allowed the last data port connection.
	     * The client may be taking the portmap info from it, in
	     * which case it will try to reconnect for data.  But if
	     * we have not yet realised that the first connection is
	     * dead (which the client cannot signal to us), then we'd
	     * quickly report that all data connections are in use,
	     * and the client will give up.
	     *
	     * Allow some seconds timeout.  (As soon as we realise the
	     * connection is gone, we'll allow the connects here.)
	     */
	    if (!lwroc_select_info_time_passed_else_setup(si,
		   &unit->_time_report_maxclients))
	      {
		/*
		  printf ("Grace period for recently opened "
		  "last avail data client.\n");
		*/
		continue;
	      }
	  }

	if (i == LWROC_NET_TRANS_SERVER_PMAP ||
	    PD_LL_IS_EMPTY(&unit->_items_free
			   [LWROC_NET_TRANS_CLIENTS_NORMAL]))
	  {
	    if (PD_LL_IS_EMPTY(&unit->_items_free
			       [LWROC_NET_TRANS_CLIENTS_ONLY_INFO]))
	      continue;
	  }
	LWROC_READ_FD_SET(unit->_server_fd[i], si);
      }
  /*
  printf("empty: %d %d\n",
	 PD_LL_IS_EMPTY(&unit->_clients_free[LWROC_NET_TRANS_CLIENTS_NORMAL]),
	 PD_LL_IS_EMPTY(&unit->_clients_free[LWROC_NET_TRANS_CLIENTS_ONLY_INFO]));
  */
}

void lwroc_net_trans_unit_accept(lwroc_net_trans_unit *unit,
				 lwroc_select_info *si)
{
  struct sockaddr_storage cli_addr;
  int client_fd;
  lwroc_net_trans_client *client;
  int i;

  /* Since we have multiple ports that can accept connections,
   * and for the moment hold connections if there already is a client
   * connected, we only try one per round.
   *
   * See comment at end - even only accept one in total per round.
   */

  for (i = 0; i < LWROC_NET_TRANS_SERVER_NUM; i++)
    {
      int info_hint;
      uint32_t info_hint2;
      int var_buffer_size = LWROC_GDF_BUF_VARBUFSIZE_NO;

      if (unit->_server_fd[i] == -1)
	continue;
      if (!LWROC_READ_FD_ISSET(unit->_server_fd[i], si))
	continue;

      /* There is no race with drasi connections, since they do not
       * work with the same server instance.  Otherwise, a drasi
       * connection may just have snatched the last free server slot,
       * and then lwroc_net_become_trans() would fail.
       */

      client_fd = lwroc_net_io_accept(unit->_server_fd[i],
				      "trans connection", &cli_addr);

      if (client_fd == -1)
	return;

      client =
	(lwroc_net_trans_client *) malloc(sizeof(lwroc_net_trans_client));

      if (!client)
	LWROC_CFATAL(unit,
		     "Failure allocating memory for trans client state.");

      client->_base._fd = client_fd;
      client->_base._addr = cli_addr;

      client->_base._buf._offset = 0;

      if (i == LWROC_NET_TRANS_SERVER_PMAP)
	{
	  info_hint = LMD_TCP_INFO_BUFSIZE_NODATA;
	  info_hint2 =
	    ((uint32_t) LMD_TCP_INFO_STREAMS_PORT_MAP_MARK) |
	    (unit->_data_port);
	}
      else if (PD_LL_IS_EMPTY(&unit->_items_free
			      [LWROC_NET_TRANS_CLIENTS_NORMAL]))
	{
	  uint32_t rejects_this_s = /* And include previous. */
	    (uint32_t) (unit->_rejects_this_s + unit->_rejects_prev_s);

	  if (rejects_this_s > LMD_TCP_INFO_STREAMS_NODATA_HOLDOFF_RATE_MASK)
	    rejects_this_s = LMD_TCP_INFO_STREAMS_NODATA_HOLDOFF_RATE_MASK;

	  info_hint = LMD_TCP_INFO_BUFSIZE_MAXCLIENTS;
	  info_hint2 =
	    ((uint32_t) LMD_TCP_INFO_STREAMS_NODATA_HOLDOFF_MARK) |
	    rejects_this_s;

	  unit->_rejects_this_s++;
	  if (unit->_reject_last_time != si->now.tv_sec)
	    {
	      unit->_reject_last_time = si->now.tv_sec;
	      unit->_rejects_prev_s = unit->_rejects_this_s;
	      unit->_rejects_this_s = 0;
	    }
	}
      else
	{
	  info_hint = 0;
	  info_hint2 = 0;
	}

      client->_bufchunks = NULL; /* Allocated in lwroc_net_become_trans(). */

      /* Clients that do port-map are also required to handle variable
       * buffer sizes.
       */
      if (i == LWROC_NET_TRANS_SERVER_DATA)
	var_buffer_size = LWROC_GDF_BUF_VARBUFSIZE_TRIM_1024;

      if (!lwroc_net_become_trans(unit, client, info_hint, info_hint2,
				  var_buffer_size))
	{
	  /* We shall only try to accept connections if there are free
	   * slots.
	   */
	  LWROC_CBUG(unit, "lwroc_net_become_trans unexpectedly failed.");
	}

      /* We just got the last data connection.
       * Hold off a bit with reporting out-of-slots to next one.
       */
      if (info_hint == 0 &&
	  PD_LL_IS_EMPTY(&unit->_items_free
			 [LWROC_NET_TRANS_CLIENTS_NORMAL]))
	{
	  unit->_time_report_maxclients = si->now;
	  /* Wait this time until we report maxclients. */
	  unit->_time_report_maxclients.tv_sec += LWROC_TRANS_CLOSE_TIMEOUT;
	}

      client->_base._select_item.item_info =
	&lwroc_net_data_trans_select_item_info;

      /* dummy init, so we can be removed. */
      PD_LL_INIT(&client->_base._clients);
      PD_LL_ADD_BEFORE(&_lwroc_net_clients,
		       &client->_base._select_item._items);

      _lwroc_mon_net._ncon_total++;

      /* Since we in lwroc_net_trans_unit_setup_select may have marked
       * several of our server ports read-ready (since we do not know
       * which one may get a connection), but have a limited number of
       * slots, we return this round as soon as one client has been
       * accepted.  If we indeed still have free slots, then we'll be
       * read-ready again immediately next cycle.
       */
      return;
    }
}

/********************************************************************/

/* This routine is used to setup a connection when we as data source
 * are to connect to the data destination (sink).
 *
 * As soon as the connection is established, we restore the normal
 * handling routine.
 *
 * It is analogous to LWROC_MERGE_IN_STATE_DATA_CREATE and
 * _WAIT_CONNECTED.
 */

int lwroc_net_trans_conn_after_fail(lwroc_select_item *item,
				    lwroc_select_info *si)
{
  lwroc_net_trans_client *client =
    PD_LL_ITEM(item, lwroc_net_trans_client, _base._select_item);

  /* Normally, we would call lwroc_net_client_base_after_fail(item, si),
   * but that would remove us from the client processing list.
   */

  if (client->_base._fd != -1)
    lwroc_safe_close(client->_base._fd);
  client->_base._fd = -1;

  /* Setup a timeout for the reconnection. */

  client->_next_time = si->now;
  client->_next_time.tv_sec += 1; /* TODO: should be increasing. */

  return 1;
}

void lwroc_net_trans_conn_setup_select(lwroc_select_item *item,
				       lwroc_select_info *si)
{
  lwroc_net_trans_client *client =
    PD_LL_ITEM(item, lwroc_net_trans_client, _base._select_item);

  (void) si;

  /* Timeout for reconnect. */
  if (client->_next_time.tv_sec &&
      !lwroc_select_info_time_passed_else_setup(si,
						&client->_next_time))
    return;

  if (client->_base._fd == -1)
    {
      int socket_type;
      struct sockaddr_storage socket_addr;

      /* Reverse connection, we connect to the data destination. */
      socket_type = LWROC_SOCKET_TCP;
      socket_addr = client->_base._addr;

      if (!lwroc_net_client_base_create_socket(&client->_base,
					       socket_type))
	{
	  /* Take some time, and then try again. */
	  lwroc_net_trans_conn_after_fail(&client->_base._select_item, si);
	  return;
	}

      switch (lwroc_net_client_base_connect_socket(&client->_base,
						   &socket_addr))
	{
	case 1:
	  /* Handled by write becoming possible for connected socket. */
	  break;
	case 0:
	  break;
	case -1:
	  /* Take some time, and then try again. */
	  lwroc_net_trans_conn_after_fail(&client->_base._select_item, si);
	  return;
	}
    }

  LWROC_WRITE_FD_SET(client->_base._fd, si);
}

int lwroc_net_trans_conn_after_select(lwroc_select_item *item,
				      lwroc_select_info *si)
{
  lwroc_net_trans_client *client =
    PD_LL_ITEM(item, lwroc_net_trans_client, _base._select_item);
  int so_error;

  /* See lwroc_net_trans_after_select(). */
  if (client->_base._mon)
    LWROC_MON_CHECK_COPY_CONN_MON_BLOCK(client->_base._mon, 0);

  if (client->_base._fd == -1 ||
      !LWROC_WRITE_FD_ISSET(client->_base._fd, si))
    return 1;

  if (!lwroc_net_client_base_connect_get_status(&client->_base,
						&so_error))
    {
      /* It is not very interesting to continuously report to the user
       * that there is no one at the other end to connect to.
       */
      /*
      LWROC_CWARNING_FMT(&client->_base,
			 "Delayed connect: %s",
			 strerror(so_error));
      */
      return 0; /* connection failed */
    }

  /* We have been connected.  Restore normal handling. */
  client->_base._select_item.item_info =
    &lwroc_net_data_trans_select_item_info;

  /* There should be an free item. */
  assert(!PD_LL_IS_EMPTY(&client->_item->_unit->_items_free
			 [LWROC_NET_TRANS_CLIENTS_NORMAL]));
  /* And not a used one. */
  assert(PD_LL_IS_EMPTY(&client->_item->_unit->_items_used
			[LWROC_NET_TRANS_CLIENTS_NORMAL]));

  if (!lwroc_net_become_trans(client->_item->_unit, client, 0, 0,
			      0))
    {
      /* Should never fail, since the error return path should never
       * happen for us (there is always a slot free).
       */
      LWROC_CBUG(&client->_base,
		 "lwroc_net_become_trans unexpectedly failed "
		 "for push client.");
    }

  return 1;
}

/* This is used when a client is in push mode and trying to reconnect. */
lwroc_select_item_info lwroc_net_data_trans_conn_select_item_info =
{
  lwroc_net_trans_conn_setup_select,
  lwroc_net_trans_conn_after_select,
  lwroc_net_trans_conn_after_fail,
  NULL,
  0,
};

/********************************************************************/

/* For a push connection, the client item is (ab)used to make the
 * connection attempts also, so needs to be allocated (always).
 */
lwroc_net_trans_client *
lwroc_net_trans_add_push_conn(lwroc_net_trans_unit *unit,
			      const char *push_hostname)
{
  lwroc_net_trans_client *client;

  client =
    (lwroc_net_trans_client *) malloc(sizeof(lwroc_net_trans_client));

  if (!client)
    LWROC_CFATAL(unit,
		 "Failure allocating memory for trans client state.");

  memset(client, 0, sizeof (lwroc_net_trans_client));

  /* Just get the item.  Do not make it 'active' (_items_used). */
  if (!lwroc_net_trans_assign_client_item(unit, client, 0 /* only_info */,
					  0 /* no switch from free to used */))
    LWROC_BUG("Internal error.");

  client->_base._fd = -1;

  client->_base._select_item.item_info =
    &lwroc_net_data_trans_conn_select_item_info;

  client->_base._msg_context = lwroc_net_trans_client_fmt_msg_context;

  client->_next_time.tv_sec = 0;
  client->_next_time.tv_usec = 0;

  client->_bufchunks = NULL;

  if (!lwroc_get_host_port(push_hostname,
			   LWROC_NET_DEFAULT_PORT,
			   &client->_base._addr))
    exit(1);

  /* dummy init, so we can be removed. */
  PD_LL_INIT(&client->_base._clients);
  PD_LL_ADD_BEFORE(&_lwroc_net_clients,
		   &client->_base._select_item._items);

  return client;
}

/********************************************************************/

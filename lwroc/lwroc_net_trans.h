/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_NET_TRANS_H__
#define __LWROC_NET_TRANS_H__

#include "lwroc_net_client_base.h"
#include "lwroc_message.h"
#include "gdf/lwroc_gdf_util.h"
#include "lmd/lwroc_lmd_util.h"
#include "ebye/lwroc_ebye_util.h"
#include "lwroc_net_reverse_link.h"
#include "lwroc_data_pipe.h"
#include "lwroc_item_buffer.h"
#include "lwroc_thread_util.h"

/********************************************************************/

struct lwroc_net_trans_unit_t;
typedef struct lwroc_net_trans_unit_t lwroc_net_trans_unit;

struct lwroc_net_trans_client_t;
typedef struct lwroc_net_trans_client_t lwroc_net_trans_client;

typedef struct lwroc_net_trans_item_t
{
  lwroc_pipe_buffer_consumer *source;

  /* Part of this structure as we want to keep the memory. */
  lwroc_lmd_sticky_store *sticky_store; /* Needed when skipping. */

  pd_ll_item _items;

  lwroc_net_trans_unit *_unit;

  lwroc_net_trans_client *_client; /* TODO: These two structs should join! */

  char *_localbuf; /* Used when client lags too much. */

  int _only_info; /* Only send header, no data. */

  lwroc_net_reverse_link *_rev_link;

  /* Monitoring the connection. */
  lwroc_net_conn_monitor *_mon;

} lwroc_net_trans_item;

#define LWROC_NET_SERVER_KIND_DRASI      1
#define LWROC_NET_SERVER_KIND_TRANS      2
#define LWROC_NET_SERVER_KIND_STREAM     3
#define LWROC_NET_SERVER_KIND_EBYE_PUSH  4
#define LWROC_NET_SERVER_KIND_EBYE_PULL  5

#define LWROC_NET_SERVER_HOLD_NONE    0
#define LWROC_NET_SERVER_HOLD_WEAK    1  /* hold when client connected */
#define LWROC_NET_SERVER_HOLD_FULL    2  /* hold always */

#define LWROC_NET_SERVER_SRCBUF_READOUT    1
#define LWROC_NET_SERVER_SRCBUF_FILTER     2
#define LWROC_NET_SERVER_SRCBUF_FINAL      3
#define LWROC_NET_SERVER_SRCBUF_TS_FILTER  4

#define LWROC_NET_SERVER_DELAYED_ON    0x01
#define LWROC_NET_SERVER_DELAYED_OFF   0x02
#define LWROC_NET_SERVER_DELAYED_USER  0x04

#define LWROC_NET_TRANS_SERVER_STD    0
#define LWROC_NET_TRANS_SERVER_PMAP   1
#define LWROC_NET_TRANS_SERVER_DATA   2
#define LWROC_NET_TRANS_SERVER_NUM    3

#define LWROC_NET_TRANS_CLIENTS_NORMAL     0
#define LWROC_NET_TRANS_CLIENTS_ONLY_INFO  1
#define LWROC_NET_TRANS_CLIENTS_NUM        2

struct lwroc_net_trans_unit_t
{
  lwroc_gdf_format_functions *_fmt;

  lwroc_lmd_sticky_store *sticky_store_full; /* Needed on restart. */

  /* This source (consumer) is used to forward in the data to pick up
   * all sticky events along the way.  They are needed for replay whenever
   * a new client connects.  Either it is in hold mode or not.
   */
  lwroc_pipe_buffer_consumer *source_full;

  /* To know max event size. */
  lwroc_data_pipe_handle *data_handle;

  lwroc_gdf_buffer_chunks *_bufchunks_skip;

  int _server_fd[LWROC_NET_TRANS_SERVER_NUM];

  /* For cases where we connect (data push). */
  const char *_push_hostname;

  size_t _bufsize;
  int _kind;
  int _server_port;
  uint16_t _data_port;
  int _srcbuf;

  int _spilldelay;

  int _hold;
  int _clients;
  int _forcemap;

  int _flush_timeout;

  PD_LL_SENTINEL_ITEM(_items_free[LWROC_NET_TRANS_CLIENTS_NUM]);
  PD_LL_SENTINEL_ITEM(_items_used[LWROC_NET_TRANS_CLIENTS_NUM]);

  struct timeval _time_report_maxclients;

  int _rejects_this_s;
  int _rejects_prev_s;
  time_t _reject_last_time;

  pd_ll_item _servers;

  /* Easy formatting of our name and kind for messages. */
  lwroc_format_message_context _msg_context;
};

extern PD_LL_SENTINEL_ITEM(_lwroc_net_trans_serv);

extern lwroc_net_trans_unit *_lwroc_drasi_server;

/********************************************************************/

struct lwroc_net_trans_client_t
{
  lwroc_net_client_base _base;

  /* Keep track. */

  lwroc_gdf_buffer_chunks *_bufchunks;

  lwroc_net_trans_item *_item;

  lwroc_thread_block *_thread_block;

  struct timeval _next_time;

  int _lcl_force_eb;

  struct timeval _last_write;
};

/********************************************************************/

extern lwroc_select_item_info lwroc_net_data_trans_select_item_info;

extern lwroc_select_item_info lwroc_net_data_trans_loop_select_item_info;

extern lwroc_select_item_info lwroc_net_data_trans_conn_select_item_info;

/********************************************************************/

void lwroc_net_skip_data(void);

/********************************************************************/

int lwroc_net_become_trans(lwroc_net_trans_unit *unit,
			   lwroc_net_trans_client *client,
			   int info_hint,
			   uint32_t info_hint2,
			   int var_buffer_size);

/********************************************************************/

int lwroc_net_trans_conn_after_fail(lwroc_select_item *item,
				    lwroc_select_info *si);

/********************************************************************/

extern char _lwroc_zero_block[4096];

/********************************************************************/

void lwroc_add_trans_server_cfg(const char *cfg);

int lwroc_net_trans_servers_clients(int *readout_clients,
				    int *filter_clients,
				    int *ts_filter_clients);

void lwroc_net_trans_servers_init(lwroc_data_pipe_handle *readout_data_handle,
				  int *readout_pipe_cons_no,
				  lwroc_data_pipe_handle *filter_data_handle,
				  int *filter_pipe_cons_no,
				  lwroc_data_pipe_handle *ts_filter_data_handle,
				  int *ts_filter_pipe_cons_no);

void lwroc_net_trans_unit_bind(lwroc_net_trans_unit *unit);

void lwroc_net_trans_unit_setup_select(lwroc_net_trans_unit *unit,
				       lwroc_select_info *si);

void lwroc_net_trans_unit_accept(lwroc_net_trans_unit *unit,
				 lwroc_select_info *si);

/********************************************************************/

typedef LWROC_STRUCT_ITEM_BUFFER(lwroc_buffer_net_trans_clients_t,
				 lwroc_net_trans_client *)
  lwroc_buffer_net_trans_clients;

typedef struct lwroc_net_trans_info_t
{
  lwroc_buffer_net_trans_clients _move[2]; /* 0: -> generic, 1: -> thread. */

  size_t _num_clients;

  PD_LL_SENTINEL_ITEM(_clients);

} lwroc_net_trans_info;

extern lwroc_net_trans_info _lwroc_net_trans_info;

/********************************************************************/

extern lwroc_thread_instance *_lwroc_net_trans_thread;

void lwroc_prepare_net_trans_thread(void);

/********************************************************************/

void lwroc_net_trans_loop_after_service(void *arg,
					lwroc_select_info *si);

void lwroc_net_trans_loop_setup_service(void *arg,
					lwroc_select_info *si);

/********************************************************************/

void lwroc_net_check_bufsize_limits(size_t bufsize,
				    size_t pipe_size, uint32_t max_ev_len,
				    lwroc_format_message_context *context,
				    const char *purpose);

/********************************************************************/

lwroc_net_trans_client *
lwroc_net_trans_add_push_conn(lwroc_net_trans_unit *unit,
			      const char *hostname);

/********************************************************************/

#endif/*__LWROC_NET_TRANS_H__*/

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_net_trans.h"
#include "lwroc_net_proto.h"
#include "lwroc_message_internal.h"
#include "lwroc_thread_util.h"
#include "lwroc_mon_block.h"

#include "gen/lwroc_message_source_serializer.h"
#include "gen/lwroc_monitor_serv_source_serializer.h"
#include "gen/lwroc_monitor_serv_block_serializer.h"

#include "lwroc_net_trans.h"
#include "lwroc_net_io.h"

#include <string.h>
#include <assert.h>

lwroc_net_trans_info _lwroc_net_trans_info;

lwroc_monitor_serv_block  _lwroc_mon_serv;
lwroc_mon_block          *_lwroc_mon_serv_handle = NULL;

char *lwroc_mon_serv_serialise(char *wire, void *block)
{
  lwroc_monitor_serv_block *serv_block =
    (lwroc_monitor_serv_block *) block;

  return lwroc_monitor_serv_block_serialize(wire, serv_block);
}

void lwroc_net_trans_thread_at_prepare(lwroc_thread_instance *inst)
{
  int i;

  (void) inst;

  /* Monitor information. */

  {
    lwroc_message_source serv_src_source;
    lwroc_message_source_sersize sz_serv_src_source;
    lwroc_monitor_serv_source serv_src;
    char *wire;

    serv_src._dummy = 0;

    lwroc_mon_source_set(&serv_src_source);

    memset (&_lwroc_mon_serv, 0, sizeof (_lwroc_mon_serv));
    _lwroc_mon_serv_handle =
      lwroc_reg_mon_block(0, &_lwroc_mon_serv, sizeof (_lwroc_mon_serv),
			  lwroc_monitor_serv_block_serialized_size(),
			  lwroc_mon_serv_serialise,
			  lwroc_message_source_serialized_size(&serv_src_source,
							       &sz_serv_src_source) +
			  lwroc_monitor_serv_source_serialized_size(), NULL,
			  NULL);

    wire = _lwroc_mon_serv_handle->_ser_source;

    wire = lwroc_message_source_serialize(wire, &serv_src_source,
					  &sz_serv_src_source);
    wire = lwroc_monitor_serv_source_serialize(wire, &serv_src);

    assert (wire == (_lwroc_mon_serv_handle->_ser_source +
		     _lwroc_mon_serv_handle->_ser_source_size));
  }

  for (i = 0; i < 2; i++)
    LWROC_ITEM_BUFFER_INIT_ALLOC(&_lwroc_net_trans_info._move[i],
				 _lwroc_net_trans_info._num_clients);

  PD_LL_INIT(&_lwroc_net_trans_info._clients);
}

/********************************************************************/

void lwroc_net_trans_loop_setup_service(void *arg,
					lwroc_select_info *si)
{
  int to_thread = *((int *) arg);
  int nonempty = 0;

  LWROC_ITEM_BUFFER_NONEMPTY_OR_SET_NOTIFY(_lwroc_net_trans_info.
					   _move[to_thread],
					   to_thread ?
					   _lwroc_net_trans_thread->_block :
					   _lwroc_net_io_thread->_block,
					   nonempty);

  if (nonempty)
    lwroc_select_info_time_setup(si, &si->now);

  if (to_thread)
  {
    /* TODO: this timeout is what makes us send incomplete buffers.
     * It also is what gets us started if we were notified that data
     * is available, but it was not enough to fill a buffer.  Hmmm,
     * for that case we should actually help the system by moving the
     * hysteresis mark to the location where a full buffer would be
     * achieved.  However, this timeout is still needed, such that we
     * get kicked in if an important event was injected.  Hmmm, do
     * important events perhaps do automatic wakeup??  (of clients
     * that are sleeping waiting for data?)
     */

    struct timeval soon;

    soon = si->now;
    soon.tv_usec += 100000;
    if (soon.tv_usec >= 1000000)
      {
	soon.tv_usec -= 1000000;
	soon.tv_sec++;
      }

    lwroc_select_info_time_setup(si, &soon);
  }
  /*
  printf ("setup_service... empty: %d : %p\n",
	  PD_LL_IS_EMPTY(&_lwroc_net_trans_info._clients),
	  _lwroc_net_trans_thread_block);
  */
}

void lwroc_net_trans_loop_after_service(void *arg,
					lwroc_select_info *si)
{
  int to_thread = *((int *) arg);
  (void) si;

  /* Add any sources to handle by the data server thread. */

  while (LWROC_ITEM_BUFFER_NONEMPTY(_lwroc_net_trans_info._move[to_thread]))
    {
      lwroc_net_trans_client *client;

      /*
      printf ("move to %s thread...\n",
	      to_thread ? "data server" : "generic");
      */

      LWROC_ITEM_BUFFER_REMOVE(_lwroc_net_trans_info._move[to_thread],
			       &client);

      /* Insert into the list. */

      if (to_thread)
	PD_LL_ADD_BEFORE(&_lwroc_net_trans_info._clients,
			 &client->_base._select_item._items);
      else
	PD_LL_ADD_BEFORE(&_lwroc_net_clients,
			 &client->_base._select_item._items);
    }

  if (to_thread)
    LWROC_MON_CHECK_COPY_BLOCK(_lwroc_mon_serv_handle,&_lwroc_mon_serv, 0);
}

extern lwroc_thread_block *_tell_notify_block;

void lwroc_net_trans_thread_loop(lwroc_thread_instance *inst)
{
  int to_thread = 1;

  /* TODO: transport thread (and/or network thread) shall not quit
   * until drasi servers have emptied their source buffers.
   */
  lwroc_select_loop(&_lwroc_net_trans_info._clients, &inst->_terminate,
		    _lwroc_net_trans_thread->_block,
		    lwroc_net_trans_loop_setup_service, &to_thread,
		    NULL, NULL,
		    lwroc_net_trans_loop_after_service, &to_thread,
		    1);
}

/********************************************************************/

lwroc_thread_info _net_trans_thread_info =
{
  lwroc_net_trans_thread_at_prepare,
  lwroc_net_trans_thread_loop,
  LWROC_MSG_BUFS_NET_TRANS,
  "data server",
  0,
  LWROC_THREAD_TERM_NET_TRANS,
  LWROC_THREAD_CORE_PRIO_SERVER,
};

/********************************************************************/

lwroc_thread_instance *_lwroc_net_trans_thread = NULL;

void lwroc_prepare_net_trans_thread(void)
{
  _lwroc_net_trans_thread =
    lwroc_thread_prepare(&_net_trans_thread_info);
}

/********************************************************************/

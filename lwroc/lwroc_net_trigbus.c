/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_net_trigbus.h"
#include "lwroc_net_reverse_link.h"
#include "lwroc_triva_control.h"
#include "lwroc_triva_state.h"
#include "lwroc_message.h"
#include "lwroc_hostname_util.h"
#include "lwroc_net_io.h" /* for debug */
#include "lwroc_net_io_util.h"

#include <string.h>
#include <assert.h>
#include "../dtc_arch/acc_def/fall_through.h"

/* As the names may be confusing (who is who):
 *
 * We call a connection that *goes to* a slave, well, a slave connection,
 * i.e. our process is master.
 *
 * Thus, a connection that *goes to* a master is a master connection.
 *
 * And the connection that goes to the triva control thread
 * (in the readout process) is a control connection.
 */

/* A slave connection is quite simple:
 *
 * The communication with the actual slave (in the readout process)
 * goes via two pipes (read + write).  When we get a message on the
 * network from the master, it shall be written to the pipe.  When we
 * get a response, it shall be written to the network.  Since these
 * processes are on the same machine, we are responsible for byte
 * swapping.  (To keep the readout process absolutely minimal.)  If
 * the pipe communicating with the readout process dies, we shall
 * report that mishap.  (And probably restart the readout process...
 * with some delay.  We are likely not directly responsible for the
 * restart though...)
 *
 * Note however: when we have several connections at the same time, we
 * have to take care that only one uses each pipe at the same time.
 * I.e. writes or reads an entire message.
 */

/********************************************************************/

/* The interaction with lwroc_triva_state is simple, since we are
 * in the same thread (and thus cannot be concurrent).
 *
 * When sending and receiving on behalf of the merger (validation)
 * thread, we have concurrent operation.  Synchronisation is handled
 * by us setting status after an MFENCE after setting other variables.
 * The merger thread reads status before reading other variables.  The
 * merger thread writes state after an MFENCE after writing other
 * variables.  We read state before other variables.  In addition to
 * that, both only progress in a ping-pong fashion.
 */

/********************************************************************/

PD_LL_SENTINEL(_lwroc_net_masters);
PD_LL_SENTINEL(_lwroc_net_slaves);
PD_LL_SENTINEL(_lwroc_net_eb_masters);
PD_LL_SENTINEL(_lwroc_net_ebs);

/********************************************************************/

int lwroc_net_trigbus_master_after_fail(lwroc_select_item *item,
					lwroc_select_info *si)
{
  lwroc_net_trigbus *conn =
    PD_LL_ITEM(item, lwroc_net_trigbus, _out._base._select_item);

  lwroc_safe_close(conn->_out._base._fd);
  conn->_out._base._fd = -1;

  if (conn->_status != LWROC_NET_TRIGBUS_STATUS_MASTER)
    LWROC_CBUG_FMT(&conn->_out._base,
		   "Bad call of failure routine for master connection (%d).",
		   conn->_status);
  conn->_status = LWROC_NET_TRIGBUS_STATUS_OUTGOING;

  conn->_out._base._mon->_block._status = LWROC_CONN_STATUS_NOT_CONNECTED;
  LWROC_MON_CHECK_COPY_CONN_MON_BLOCK(conn->_out._base._mon, 1);

  lwroc_net_become_outgoing(&conn->_out,si);

  return 1;
}

void lwroc_net_trigbus_setup_select(lwroc_select_item *item,
				    lwroc_select_info *si)
{
  lwroc_net_trigbus *conn =
    PD_LL_ITEM(item, lwroc_net_trigbus, _out._base._select_item);
  int state;

  /* printf ("%p trigbus_setup_select %d\n",conn,conn->_state); */

  state = conn->_state;
  MFENCE; /* Interaction with merger thread (see
	   * lwroc_net_trigbus_send_msg()), read state to temporary
	   * variable before other members, since merger sets state
	   * after other members.  As things are now, only matters in
	   * ...setup_select().
	   */
  switch (state)
    {
    case LWROC_NET_TRIGBUS_STATE_EXT_READ_SETUP:
      LWROC_NET_CLIENT_RW_SET(&conn->_out._base._buf, conn->_raw,
			      lwroc_trigbus_msg_serialized_size());
      conn->_state = LWROC_NET_TRIGBUS_STATE_EXT_READ_RAW;
      FALL_THROUGH;
    case LWROC_NET_TRIGBUS_STATE_EXT_READ_RAW:
      /* If we are waiting for a message, we may switch to send a LAM. */
      if (conn->_send_lam &&
	  conn->_out._base._buf._offset == 0)
	{
	  conn->_send_lam = 0;
	  /* printf("%p: send LAM\n", conn); */
	  /* Prepare the LAM message. */
	  LWROC_TRIVA_CTRL_MSG_PREPARE(&conn->_msg,
				       LWROC_TRIVA_CONTROL_LOOK_AT_ME);
	  /* Setup to send it. */
	  lwroc_trigbus_msg_serialize((char*) &conn->_raw,
				      &conn->_msg);
	  LWROC_NET_CLIENT_RW_SET(&conn->_out._base._buf, conn->_raw,
				  lwroc_trigbus_msg_serialized_size());
	  MFENCE; /* When we are merger thread, set state last.  But
		   * does not matter here, as we come out and go into
		   * a state which merger does not care about.
		   */
	  conn->_state = LWROC_NET_TRIGBUS_STATE_EXT_WRITE_RAW;
	  goto ext_write_raw;
	}

      /* printf ("%p: ext_read\n", conn); */
      LWROC_READ_FD_SET(conn->_out._base._fd, si);
      /* TODO: No timeout for reading messages over the network
       * (for master connection after receiving the first bytes)?
       * (Or does triva_state do the timeout?  (Then note that here.))
       */
      break;

    case LWROC_NET_TRIGBUS_STATE_INT_READ_SETUP:
      LWROC_NET_CLIENT_RW_SET(&conn->_out._base._buf, conn->_msg,
			      sizeof (conn->_msg));
      conn->_state = LWROC_NET_TRIGBUS_STATE_INT_READ;
      FALL_THROUGH;
    case LWROC_NET_TRIGBUS_STATE_INT_READ:
      LWROC_READ_FD_SET(_lwroc_triva_control_pipe
			[TRIVA_CONTROL_PIPE_RESPONSE][0],si);
      break;

    case LWROC_NET_TRIGBUS_STATE_FRESH_MASTER:
    case LWROC_NET_TRIGBUS_STATE_FRESH_SLAVE:
    case LWROC_NET_TRIGBUS_STATE_MSG_RECEIVED:
      break;

    case LWROC_NET_TRIGBUS_STATE_DELAY_MSG_SEND:
      lwroc_select_info_time_setup(si, &conn->_out._next_time);
      FALL_THROUGH;
    case LWROC_NET_TRIGBUS_STATE_WAIT_MSG_SEND:
      /* Check for an request to do a status query by seeing if there
       * is a message pending from the client.
       */
      if (!conn->_got_lam)
	{
	  if (conn->_status == LWROC_NET_TRIGBUS_STATUS_CONTROL)
	    LWROC_READ_FD_SET(_lwroc_triva_control_pipe
			      [TRIVA_CONTROL_PIPE_RESPONSE][0],si);
	  else
	    LWROC_READ_FD_SET(conn->_out._base._fd, si);
	}
      break;

    ext_write_raw:
    case LWROC_NET_TRIGBUS_STATE_EXT_WRITE_RAW:
    case LWROC_NET_TRIGBUS_STATE_EXT_WRITE_RAW_DISCON:
      LWROC_WRITE_FD_SET(conn->_out._base._fd, si);
      break;

    case LWROC_NET_TRIGBUS_STATE_DISCON:
      /* We want to get killed now, set 0 timeout. */
      si->next_time = si->now;
      break;

    case LWROC_NET_TRIGBUS_STATE_INT_WRITE:
      LWROC_WRITE_FD_SET(_lwroc_triva_control_pipe
			 [TRIVA_CONTROL_PIPE_MESSAGE][1],si);
      break;
    }
}

/********************************************************************/

int lwroc_net_trigbus_after_select(lwroc_select_item *item,
				   lwroc_select_info *si)
{
  lwroc_net_trigbus *conn =
    PD_LL_ITEM(item, lwroc_net_trigbus, _out._base._select_item);
  int state;

  /* printf ("trigbus_after_select %d\n",conn->_state); */

  /* TODO: remove if after making sure every conn has a mon object */
  if (conn->_out._base._mon)
    LWROC_MON_CHECK_COPY_CONN_MON_BLOCK(conn->_out._base._mon, 0);

  state = conn->_state;
  MFENCE; /* Interaction with merger thread
	   * (lwroc_net_trigbus_send_msg), read state before other members.
	   */
  switch (state)
    {
    case LWROC_NET_TRIGBUS_STATE_EXT_READ_RAW:
      if (!LWROC_READ_FD_ISSET(conn->_out._base._fd, si))
	break;
      if (!lwroc_net_client_base_read(&conn->_out._base, "from peer",
				      &_lwroc_mon_net._recv_mssync))
	return 0;
      break;

    case LWROC_NET_TRIGBUS_STATE_INT_READ:
      if (!LWROC_READ_FD_ISSET(_lwroc_triva_control_pipe
			       [TRIVA_CONTROL_PIPE_RESPONSE][0], si))
	break;
      if (!lwroc_net_client_rw_read(&conn->_out._base._buf,
				    _lwroc_triva_control_pipe
				    [TRIVA_CONTROL_PIPE_RESPONSE][0],
				    "from control",
				    &_lwroc_mon_net._recv_mssync))
	LWROC_FATAL("Error reading from internal pipe.");
      break;

    case LWROC_NET_TRIGBUS_STATE_FRESH_MASTER:
    case LWROC_NET_TRIGBUS_STATE_FRESH_SLAVE:
    case LWROC_NET_TRIGBUS_STATE_MSG_RECEIVED:
      return 1;

    case LWROC_NET_TRIGBUS_STATE_DELAY_MSG_SEND:
      if (!timercmp(&si->now,&conn->_out._next_time,<))
	conn->_state = LWROC_NET_TRIGBUS_STATE_WAIT_MSG_SEND;
      FALL_THROUGH;
    case LWROC_NET_TRIGBUS_STATE_WAIT_MSG_SEND:
      if (!conn->_got_lam &&
	  (conn->_status == LWROC_NET_TRIGBUS_STATUS_CONTROL ?
	   LWROC_READ_FD_ISSET(_lwroc_triva_control_pipe
			       [TRIVA_CONTROL_PIPE_RESPONSE][0], si) :
	   LWROC_READ_FD_ISSET(conn->_out._base._fd, si)))
	{
	  /* There is something to read.  There would normally not be
	   * anything to read from the client at this stage.  It is
	   * thus likely (unless a spurious mark) a request to tell
	   * master to do a status query asap.  We will not read the
	   * message as such - it will be discarded when read as a
	   * message.
	   */
	  /*
	  printf("read-ready - assume LAM message... (%p: %s %x %d)\n",
		 conn, conn->_out._hostname, conn->_request, conn->_status);
	  */
	  /* Tell triva state that something has happened. */
	  _lwroc_triva_state->_got_look_at_me = 1;
	  /* Do not trigger on this again.  Need to protect, since we
	   * do not actually read the message.
	   */
	  conn->_got_lam = 1;
	}
      return 1;

    case LWROC_NET_TRIGBUS_STATE_EXT_WRITE_RAW:
    case LWROC_NET_TRIGBUS_STATE_EXT_WRITE_RAW_DISCON:
      if (!LWROC_WRITE_FD_ISSET(conn->_out._base._fd, si))
	break;
      if (!lwroc_net_client_base_write(&conn->_out._base, "to peer",
				       &_lwroc_mon_net._recv_mssync))
	return 0;
      break;

    case LWROC_NET_TRIGBUS_STATE_DISCON:
      return 0;

    case LWROC_NET_TRIGBUS_STATE_INT_WRITE:
      if (!LWROC_WRITE_FD_ISSET(_lwroc_triva_control_pipe
				[TRIVA_CONTROL_PIPE_MESSAGE][1], si))
	break;
      if (!lwroc_net_client_rw_write(&conn->_out._base._buf,
				     _lwroc_triva_control_pipe
				     [TRIVA_CONTROL_PIPE_MESSAGE][1], 1,
				     "to control",
				     &_lwroc_mon_net._recv_mssync))
	LWROC_FATAL("Error writing to internal pipe.");
      break;
    }

  if (conn->_out._base._buf._offset < conn->_out._base._buf._size)
    return 1; /* We are not done reading/writing. */

  /* We are done reading/writing this message, process it and
   * go to next state.
   */

  {
    lwroc_deserialize_error desererr;
    const char *end;

    switch (conn->_state)
      {
      case LWROC_NET_TRIGBUS_STATE_EXT_READ_RAW:
	end = lwroc_trigbus_msg_deserialize(&conn->_msg,
					    (const char*) conn->_raw,
					    conn->_out._base._buf._size,
					    &desererr);

	if (end == NULL)
	  {
	    LWROC_CERROR_FMT(&conn->_out._base,
			     "Bad message from master (%s, 0x%x, 0x%x).",
			     desererr._msg, desererr._val1, desererr._val2);
	    return 0;
	  }

	if (conn->_msg._magic != LWROC_TRIVA_CONTROL_MAGIC)
	  {
	    /* Version mismatch. */
	    LWROC_CERROR_FMT(&conn->_out._base,
			     "Got message with bad magic (%u, expected %u) "
			     "from master/slave/control/eb.",
			     conn->_msg._magic, LWROC_TRIVA_CONTROL_MAGIC);
	    /* Hmm, TODO: we should rather check this when
	     * establishing the connection.  (The check is against
	     * things not protected by the net_proto checksums).
	     */
	    return 0;
	  }

	if (conn->_status == LWROC_NET_TRIGBUS_STATUS_MASTER)
	  {
	    /* printf ("---> MSG_RECEIVED\n"); */
	    conn->_state = LWROC_NET_TRIGBUS_STATE_MSG_RECEIVED;
	    MFENCE;
	    if (conn->_thread_notify)
	      lwroc_thread_block_send_token(conn->_thread_notify);
	    break;
	  }
	FALL_THROUGH; /* slave */
      case LWROC_NET_TRIGBUS_STATE_INT_READ:
	/* We check the message here and now. */

	if (conn->_got_lam)
	  {
	    if (conn->_msg._type != LWROC_TRIVA_CONTROL_LOOK_AT_ME)
	      {
		LWROC_CWARNING(&conn->_out._base,
			       "Spurious read-ready, "
			       "LAM suspected without LAM message.");
	      }
	    /*
	    printf ("got_lam with msg: %08x\n",
		    conn->_msg._type);
	    */
	  }
	/* Termination requested? */
	if (conn->_msg.status & LWROC_READOUT_STATUS_TERM_REQ)
	  {
	    /* printf ("termination request seen.\n"); */
	    conn->_term_request = 1;
	    _lwroc_triva_state->_stop_reason |=
	      LWROC_ACQ_STOP_REASON_TERMINATE;
	  }
	/* This reset just to happen away from ..._WAIT_MSG_SEND. */
	conn->_got_lam = 0;
	/* Consume any look-at-me dummy wake-up message. */
	if (conn->_msg._type == LWROC_TRIVA_CONTROL_LOOK_AT_ME)
	  {
	    /* printf("Consumed LAM message.\n"); */
	    /* Read real message. */
	    if (conn->_state == LWROC_NET_TRIGBUS_STATE_EXT_READ_RAW)
	      conn->_state = LWROC_NET_TRIGBUS_STATE_EXT_READ_SETUP;
	    else
	      conn->_state = LWROC_NET_TRIGBUS_STATE_INT_READ_SETUP;
	    break;
	  }
	/*
	LWROC_INFO_FMT("trigbus_recv(slave/ctrl): 0x%04x (expect 0x%04x)",
		       conn->_msg._type, conn->_expect_msg_type);
	*/
	if (!LWROC_HAS_ONE_EXPECTED_BIT(conn->_msg._type,
					conn->_expect_msg_type))
	  {
	    if (conn->_status == LWROC_NET_TRIGBUS_STATUS_CONTROL)
	      {
		/* Since the triva control will be in another
		 * process, it is not necessarily an internal bug
		 * - may be out-of-version programs.  But!  we are
		 * supposed to have checks against that...
		 */
		LWROC_CBUG_FMT(&conn->_out._base,
			       "Got desynchronised message "
			       "(%08x, expect mask %08x) "
			       "from triva control.",
			       conn->_msg._type, conn->_expect_msg_type);
	      }
	    else
	      {
		LWROC_CERROR_FMT(&conn->_out._base,
				 "Got desyncronised message "
				 "(%08x, expect mask %08x) "
				 "from slave.",
				 conn->_msg._type, conn->_expect_msg_type);
		/* The failure routine will notice the expected
		 * message, remove us from the list, and set the
		 * error mark.
		 */
		return 0;
	      }
	  }
	/* So message was good! */
	conn->_expect_msg_type = 0;

	if (conn->_msg._type & (LWROC_TRIVA_CONTROL_EB_NO_IDENT |
				LWROC_TRIVA_CONTROL_EB_ASK_IDENT))
	  {
	    conn->_state = LWROC_NET_TRIGBUS_STATE_MSG_RECEIVED;
	  }
	else
	  {
	    /* Remove us from list of pending msg recipients. */
	    PD_LL_REMOVE(&conn->_wait_slaves);
	    /* Next thing is to send a message! */
	    conn->_state = LWROC_NET_TRIGBUS_STATE_WAIT_MSG_SEND;
	  }
	break;

      case LWROC_NET_TRIGBUS_STATE_EXT_WRITE_RAW:
	conn->_state = LWROC_NET_TRIGBUS_STATE_EXT_READ_SETUP;
	break;

      case LWROC_NET_TRIGBUS_STATE_EXT_WRITE_RAW_DISCON:
	/* We have sent a failure response, that was bad
	 * enough for us to tear the connection down.
	 * (Basically: master misbehaved in its communication.)
	 */
	return 0;

      case LWROC_NET_TRIGBUS_STATE_INT_WRITE:
	conn->_state = LWROC_NET_TRIGBUS_STATE_INT_READ_SETUP;
	break;
      }
  }
  return 1;
}

/********************************************************************/

extern PD_LL_SENTINEL_ITEM(_lwroc_net_clients);

void lwroc_net_trigbus_master_connected(lwroc_select_item *item,
					lwroc_select_info *si)
{
  lwroc_net_trigbus *conn =
    PD_LL_ITEM(item, lwroc_net_trigbus, _out._base._select_item);

  (void) si;

  lwroc_net_io_print_clients();

  /* printf ("slave: connected! (to master) (%p)\n", conn); */

  if (conn->_request == LWROC_REQUEST_TRIVAMI_EB)
    conn->_state = LWROC_NET_TRIGBUS_STATE_EXT_READ_SETUP;
  else
    {
      /* Using code updates to LWROC_NET_TRIGBUS_STATE_EXT_READ_SETUP
       * when the payload protocol reaches an init point.
       */
      conn->_state = LWROC_NET_TRIGBUS_STATE_FRESH_MASTER;
    }

  if (conn->_status != LWROC_NET_TRIGBUS_STATUS_OUTGOING)
    LWROC_CBUG_FMT(&conn->_out._base,
		   "Becoming master from non-outgoing (%d).",
		   conn->_status);
  conn->_status = LWROC_NET_TRIGBUS_STATUS_MASTER;

  conn->_out._base._mon->_block._status = LWROC_CONN_STATUS_CONNECTED;
  conn->_out._base._mon->_block._ipv4_addr =
    lwroc_get_ipv4_addr(&conn->_out._base._addr);
  conn->_out._base._mon->_block._port =
    lwroc_get_port(&conn->_out._base._addr);
  LWROC_MON_CHECK_COPY_CONN_MON_BLOCK(conn->_out._base._mon, 1);
}

/********************************************************************/

lwroc_outgoing_select_item_info lwroc_net_master_select_item_info =
{
  {
    lwroc_net_trigbus_setup_select,
    lwroc_net_trigbus_after_select,
    lwroc_net_trigbus_master_after_fail,
    NULL,
    LWROC_SELECT_ITEM_FLAG_OUTGOING_NET_THREAD,
  },
  lwroc_net_trigbus_master_connected,
  LWROC_REQUEST_TRIVAMI_BUS,
  "--slave option",
};

lwroc_outgoing_select_item_info lwroc_net_eb_master_select_item_info =
{
  {
    lwroc_net_trigbus_setup_select,
    lwroc_net_trigbus_after_select,
    lwroc_net_trigbus_master_after_fail,
    NULL,
    LWROC_SELECT_ITEM_FLAG_OUTGOING_NET_THREAD,
  },
  lwroc_net_trigbus_master_connected,
  LWROC_REQUEST_TRIVAMI_EB,
  "--eb option",
};

/********************************************************************/

char *lwroc_net_trigbus_fmt_msg_context(char *buf, size_t size,
					const  void *ptr)
{
  const lwroc_net_trigbus *conn = (const lwroc_net_trigbus *)
    (ptr - offsetof(lwroc_net_trigbus, _out._base._msg_context));

  if (conn->_request == LWROC_REQUEST_TRIVAMI_EB)
    return lwroc_message_fmt_msg_context(buf, size,
					 "EB %s", conn->_out._hostname);
  else if (conn->_request == LWROC_REQUEST_TRIVAMI_BUS)
    return lwroc_message_fmt_msg_context(buf, size,
					 "bus %s", conn->_out._hostname);
  else
    return lwroc_message_fmt_msg_context(buf, size,
					 "??conn %s", conn->_out._hostname);
}

/********************************************************************/

lwroc_net_trigbus *lwroc_net_trigbus_alloc(const char *hostname,
					   uint32_t request)
{
  lwroc_net_trigbus *conn;

  /* Create the connection structure. */

  conn = (lwroc_net_trigbus *) malloc (sizeof (lwroc_net_trigbus));

  if (!conn)
    LWROC_FATAL("Memory allocation failure (net_trigbus).");

  memset (conn, 0, sizeof (lwroc_net_trigbus));

  conn->_out._base._fd = -1;

  conn->_out._hostname = hostname;
  conn->_request = request;
  conn->_out._base._msg_context = lwroc_net_trigbus_fmt_msg_context;

  return conn;
}

/********************************************************************/

void lwroc_add_master(const char *hostname, uint32_t request)
{
  lwroc_net_trigbus *conn =
    lwroc_net_trigbus_alloc(strdup_chk(hostname), request);

  if (!lwroc_get_host_port(hostname,
			   LWROC_NET_DEFAULT_PORT,
			   &conn->_out._base._addr))
    exit(1);

  conn->_out._base._mon =
    lwroc_net_conn_monitor_init(request,
				LWROC_CONN_DIR_OUTGOING,
				hostname, NULL,
				&conn->_out._base._addr,
				NULL, NULL, NULL, NULL, NULL, NULL, 0, 0);

  conn->_status = LWROC_NET_TRIGBUS_STATUS_OUTGOING;

  if (request == LWROC_REQUEST_TRIVAMI_BUS)
    {
      PD_LL_ADD_BEFORE(&_lwroc_net_masters, &conn->_masters);

      /* printf ("adding master -> out (%p)\n", &conn->_out); */

      lwroc_net_outgoing_init(&conn->_out,
			      &lwroc_net_master_select_item_info);
    }
  else /* LWROC_REQUEST_TRIVAMI_EB */
    {
      assert(request == LWROC_REQUEST_TRIVAMI_EB);

      PD_LL_ADD_BEFORE(&_lwroc_net_eb_masters, &conn->_masters);

      /* printf ("adding eb master -> out (%p)\n", &conn->_out); */

      lwroc_net_outgoing_init(&conn->_out,
			      &lwroc_net_eb_master_select_item_info);
    }
}

/********************************************************************/

int lwroc_net_trigbus_slave_after_fail(lwroc_select_item *item,
				       lwroc_select_info *si)
{
  lwroc_net_trigbus *conn =
    PD_LL_ITEM(item, lwroc_net_trigbus, _out._base._select_item);

  /* Since we *only* can fail after having requested a message
   * to be sent, we *shall* be expecting a message.
   *
   * TODO: what about write error??
   */
  if (!conn->_expect_msg_type)
    LWROC_CBUG(&conn->_out._base,
	       "Slave failed but is not expecting a message.");

  /* Remove it from the list of clients that triva_state is
   * waiting for.
   */
  PD_LL_REMOVE(&conn->_wait_slaves);
  /* Monitor info that we had issue. */
  conn->_aux_status = LWROC_TRIVA_BUS_STATUS_FAIL;
  /* And so that it detects that something went bad. */
  _lwroc_triva_state->_had_issue = 1;
  /* Slave is gone, so termination request is not longer. */
  conn->_term_request = 0;

  conn->_status = LWROC_NET_TRIGBUS_STATUS_NO_SLAVE;

  conn->_out._base._mon->_block._status = LWROC_CONN_STATUS_NOT_CONNECTED;
  LWROC_MON_CHECK_COPY_CONN_MON_BLOCK(conn->_out._base._mon, 1);

  lwroc_net_client_base_after_fail(item, si);

  /* printf ("%p: lwroc_net_trigbus_slave_after_fail\n", conn); */

  /* Since we have no delete callback, and set
   * LWROC_SELECT_ITEM_FLAG_NO_DELETE_INTENTIONAL,
   * we will be kept (not free'd).  But we request to be removed from the
   * list of active network connections, to not get a select on the closed
   * file descriptor.
   */
  return 0;
}

void lwroc_net_trigbus_conn_monitor_fetch(void *ptr)
{
  lwroc_net_conn_monitor *conn_mon = (lwroc_net_conn_monitor *)
    (((char *) ptr) - offsetof(lwroc_net_conn_monitor, _block));

  lwroc_net_trigbus *conn = (lwroc_net_trigbus *) conn_mon->_aux;

  if (conn->_aux_status)
    conn->_out._base._mon->_block._aux_status =
      conn->_aux_status;
  else if (!PD_LL_IS_EMPTY(&conn->_wait_slaves))
    conn->_out._base._mon->_block._aux_status =
      LWROC_TRIVA_BUS_STATUS_WAIT;
  else
    conn->_out._base._mon->_block._aux_status =
      LWROC_TRIVA_BUS_STATUS_NONE;
}

/********************************************************************/

lwroc_select_item_info lwroc_net_slave_select_item_info =
  {
    lwroc_net_trigbus_setup_select,
    lwroc_net_trigbus_after_select,
    lwroc_net_trigbus_slave_after_fail,
    NULL,
    LWROC_SELECT_ITEM_FLAG_NO_DELETE_INTENTIONAL,
  };

/********************************************************************/

lwroc_net_trigbus *lwroc_add_slave(const char *hostname, uint32_t request,
				   lwroc_net_reverse_link *revlink)
{
  lwroc_net_trigbus *conn =
    lwroc_net_trigbus_alloc(strdup_chk(hostname), request);

  conn->_out._base._mon =
    lwroc_net_conn_monitor_init(request,
				LWROC_CONN_DIR_INCOMING,
				hostname, NULL,
				&revlink->_out._base._addr,
				NULL, NULL, NULL, NULL,
				conn,
				lwroc_net_trigbus_conn_monitor_fetch,
				0, 0);

  conn->_aux_status = 0;

  /* Hostname lookup is done by the reverse link. */

  conn->_status = LWROC_NET_TRIGBUS_STATUS_NO_SLAVE;

  /* PD_LL_ADD_BEFORE(&_lwroc_triva_state._slaves, &conn->_masters); */

  if (request == LWROC_REQUEST_TRIVAMI_BUS)
    {
      PD_LL_ADD_BEFORE(&_lwroc_net_slaves, &conn->_slaves_lst);
    }
  else /* LWROC_REQUEST_TRIVAMI_EB */
    {
      assert (request == LWROC_REQUEST_TRIVAMI_EB);
      PD_LL_ADD_BEFORE(&_lwroc_net_ebs, &conn->_slaves_lst);
    }

  /* printf ("adding slave -> \n"); */

  /* Whenever we are actively connected, we act as a slave connection. */

  conn->_out._base._select_item.item_info =
    &lwroc_net_slave_select_item_info;

  revlink->_extra = conn;

  return conn;
}

/********************************************************************/

lwroc_net_trigbus *lwroc_net_become_slave(lwroc_net_incoming *incoming)
{
  /* So here we will be a bit dirty.  Normally, the
   * lwroc_net_become_XXX() routines take over the structure
   * memory/instance, and 'repurpose' it (keeping the fd etc as they
   * are in the same place).
   *
   * However, since the triva_state want to at all times keep track of
   * all known slaves, we actually rather would like to use that
   * instance for the communication.  So, if we accept this connection
   * (i.e. slave is not already spoken for) we copy the relevant
   * information.  Then we replace the original structure in the list
   * of select clients, and also free the memory associated with it.
   */

  lwroc_net_reverse_link *rev_link = incoming->_rev_link;

  void *extra = rev_link->_extra;

  lwroc_net_trigbus *conn = (void *) extra;

  if (conn->_status != LWROC_NET_TRIGBUS_STATUS_NO_SLAVE)
    {
      if (conn->_status != LWROC_NET_TRIGBUS_STATUS_SLAVE)
	LWROC_CBUG(&conn->_out._base,
		   "Trying to attach as slave, but inconsistent.");

      /* Slave is already connected!  (Should not really happen,
       * as this means that the same slave connected twice...  perhaps
       * there is an old lingering connection that has not died
       * properly?)
       */

      /* Use the original incoming connection to send a nasty response!
       * this we actually leave for the caller to do...
       */
      return NULL;
    }

  /* Ok, so our slave instance takes over. */

  /* Using code updates to LWROC_NET_TRIGBUS_STATE_WAIT_MSG_SEND
   * when the payload protocol reaches an init point.
   */
  conn->_state = LWROC_NET_TRIGBUS_STATE_FRESH_SLAVE;
  MFENCE; /* For interaction with merger thread, set status after state. */
  conn->_status = LWROC_NET_TRIGBUS_STATUS_SLAVE;

  conn->_expect_msg_type = 0;
  conn->_term_request = 0;

  /* We add ourselves before the incoming item, and then remove
   * that one.  The same as a replace.
   */
  PD_LL_REPLACE(&incoming->_base._select_item._items,
		&conn->_out._base._select_item._items);

  assert(conn->_out._base._fd == -1);
  /* conn->_out._base._clients = ; */                /* done by caller */
  /* conn->_out._base._select_item._item = ; */      /* above */
  /* conn->_out._base._select_item._item_info = ; */ /* done by caller */
  conn->_out._base._fd = incoming->_base._fd;
  /* conn->_out._base._buf = ; */                    /* no need */
  conn->_out._base._addr = incoming->_base._addr;
  /* conn->_out._base._msg_context = ; */            /* no need */

  free(incoming);

  conn->_out._base._mon->_block._status = LWROC_CONN_STATUS_CONNECTED;
  conn->_out._base._mon->_block._ipv4_addr =
    lwroc_get_ipv4_addr(&conn->_out._base._addr);
  conn->_out._base._mon->_block._port =
    lwroc_get_port(&conn->_out._base._addr);
  LWROC_MON_CHECK_COPY_CONN_MON_BLOCK(conn->_out._base._mon, 1);

  return conn;
}

/********************************************************************/

lwroc_select_item_info lwroc_net_trigctrl_select_item_info =
  {
    lwroc_net_trigbus_setup_select,
    lwroc_net_trigbus_after_select,
    NULL,
    NULL,
    0,
  };

/********************************************************************/

lwroc_net_trigbus *lwroc_trigctrl_conn(lwroc_net_conn_monitor *conn_mon)
{
  lwroc_net_trigbus *conn = lwroc_net_trigbus_alloc("(this)", 0);

  conn->_out._base._mon = conn_mon;

  conn->_status = LWROC_NET_TRIGBUS_STATUS_CONTROL;

  /* printf ("adding trigctrl conn\n"); */

  conn->_state = LWROC_NET_TRIGBUS_STATE_WAIT_MSG_SEND;

  conn->_expect_msg_type = 0;

  /* Add it to the list of clients to be processed by the net I/O loop.
   * That is the entire reason why we exist...
   */
  PD_LL_ADD_BEFORE(&_lwroc_net_clients,
		   &conn->_out._base._select_item._items);

  conn->_out._base._select_item.item_info =
    &lwroc_net_trigctrl_select_item_info;

  return conn;
}

/********************************************************************/

void lwroc_net_trigbus_discon(lwroc_net_trigbus *conn)
{
  conn->_state = LWROC_NET_TRIGBUS_STATE_DISCON;

  /* Send wakeup token to network thread if we are called from the
   * merger thread.
   */
  if (conn->_thread_notify)
    {
      MFENCE;
      lwroc_thread_block_send_token(_lwroc_net_io_thread->_block);
    }
}

int lwroc_net_trigbus_recv_msg(lwroc_net_trigbus *conn,
			       uint32_t expect_type_mask,
			       int *had_issue)
{
  /* printf ("1\n"); */

  if (conn->_status == LWROC_NET_TRIGBUS_STATUS_OUTGOING)
    {
      LWROC_CERROR(&conn->_out._base,
		   "Connection lost.");
      /* Connection has been lost. */
      if (had_issue)
	*had_issue = 1;
      /* We need to return 1 to get the state machine out of the wait
       * loop.  It shall then consult the error flag!
       */
      goto done_wait_for_this;
    }

  /* printf ("2\n"); */

  if (conn->_status != LWROC_NET_TRIGBUS_STATUS_MASTER)
    LWROC_CBUG_FMT(&conn->_out._base,
		   "Trying to recv trigbus message in non-master (%d).",
		   conn->_status);

  MFENCE; /* When _we_ are merger thread, read status before state. */

  /* printf ("3\n"); */

  /* Have we gotten a message yet. */

  if (conn->_state != LWROC_NET_TRIGBUS_STATE_MSG_RECEIVED)
    return 0;

  /* printf ("4\n"); */

  /* The message has already been wire decoded.
   * (Control connection needs no decoding.)
   */

  /* Next we are to send a message. */
  conn->_state = LWROC_NET_TRIGBUS_STATE_WAIT_MSG_SEND;

  /* So we have gotten a message.  Is it of an expected type?
   */

  /*
  LWROC_INFO_FMT("trigbus_recv: 0x%04x (expect 0x%04x)",
		 conn->_msg._type, expect_type_mask);
  */

  (void) expect_type_mask;

  /* printf ("5\n"); */

  if (!LWROC_HAS_ONE_EXPECTED_BIT(conn->_msg._type,expect_type_mask))
    {
      /* If we are a master, there is nothing to send. */
      /* If we are a slave, we could send some kind of error report. */

      /* After this has been sent, tear the connection down, to force
       * a reset of the state of whoever screwed up.  It might be a
       * broken master, but that is no reason for us to die or go
       * debug hang.  We just let them try again.
       */

      LWROC_CERROR_FMT(&conn->_out._base,
		       "TRIVA state got unexpected message from master "
		       "(got %x, expect mask %x).",
		       conn->_msg._type, expect_type_mask);

      lwroc_net_trigbus_discon(conn);

      if (had_issue)
	*had_issue = 1;
      goto done_wait_for_this;
    }

  /* printf ("6\n"); */

  /* Ok, message looks good! */

 done_wait_for_this:
  /* Either it worked or not: if we are a connection to a slave, we
   * ask to be removed from the list of slaves to be waited for.
   */
  /* printf ("ret 1\n"); fflush(stdout); */
  /* printf ("7\n"); */
  return 1;
}

/********************************************************************/

void lwroc_net_trigbus_send_msg(lwroc_net_trigbus *conn,
				uint32_t expect_msg_type)
{
  if ((conn->_state != LWROC_NET_TRIGBUS_STATE_WAIT_MSG_SEND &&
       conn->_state != LWROC_NET_TRIGBUS_STATE_DELAY_MSG_SEND) ||
      conn->_status == LWROC_NET_TRIGBUS_STATUS_CONTROL)
    LWROC_CBUG_FMT(&conn->_out._base,
		   "Trying to send trigbus message in wrong state (%d,%d).",
		   conn->_state, conn->_status);

  /* LWROC_INFO_FMT("trigbus_send: 0x%04x", conn->_msg._type); */

  if (conn->_status == LWROC_NET_TRIGBUS_STATUS_SLAVE)
    {
      if (expect_msg_type)
	conn->_expect_msg_type = expect_msg_type;
      else
	{
	  /* We expect the response to have our message type! */
	  conn->_expect_msg_type = conn->_msg._type;
	}
    }

  lwroc_trigbus_msg_serialize((char*) &conn->_raw,
			      &conn->_msg);
  LWROC_NET_CLIENT_RW_SET(&conn->_out._base._buf, conn->_raw,
			  lwroc_trigbus_msg_serialized_size());
  MFENCE; /* When we are merger thread, set state last. */
  conn->_state = LWROC_NET_TRIGBUS_STATE_EXT_WRITE_RAW;
  if (conn->_thread_notify)
    {
      MFENCE;
      lwroc_thread_block_send_token(_lwroc_net_io_thread->_block);
    }
}

/********************************************************************/

void lwroc_net_trigctrl_send_msg(lwroc_net_trigbus *conn)
{
  if (conn->_state != LWROC_NET_TRIGBUS_STATE_WAIT_MSG_SEND ||
      conn->_status != LWROC_NET_TRIGBUS_STATUS_CONTROL)
    LWROC_CBUG_FMT(&conn->_out._base,
		   "Trying to send trigctrl message in wrong state (%d,%d).",
		   conn->_state, conn->_status);

  /* LWROC_INFO_FMT("trigctrl_send: 0x%04x", conn->_msg._type); */

  /* We expect the response to have our message type! */
  conn->_expect_msg_type = conn->_msg._type;

  LWROC_NET_CLIENT_RW_SET(&conn->_out._base._buf, conn->_msg,
			  sizeof (conn->_msg));
  conn->_state = LWROC_NET_TRIGBUS_STATE_INT_WRITE;
}

/********************************************************************/

void lwroc_net_trigbus_send_look_at_me(lwroc_net_trigbus *conn)
{
  /*
  printf("%p: forward LAM message (was %d)... (%d)\n",
	 conn, conn->_send_lam, conn->_state);
  */

  /* We can forward the LAM if we are actually just waiting to receive
   * a message.  If we are already receiving a message, it means we
   * are being queried right now.  But we might be in the state where
   * we are sending a response, in which case we should not discard
   * this.
   */

  conn->_send_lam = 1;

  /* If we are the merger we are in a different thread, so need to
   * wake the network thread up.
   */
  if (conn->_thread_notify)
    {
      MFENCE;
      lwroc_thread_block_send_token(_lwroc_net_io_thread->_block);
    }
}

/********************************************************************/

/* This is for communication with lwroc_triva_control.  Declared here
 * for programs that do not include the actual lwroc_triva_control
 * functions, but that have spurious references to the pipe
 * descriptors.
 */

/* These are pipes between processes on the same machine.  No
 * byteswapping etc needed.
 */

int _lwroc_triva_control_pipe[2][2] = { { -1, -1 }, { -1, -1 } };

/********************************************************************/

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_NET_TRIGBUS_H__
#define __LWROC_NET_TRIGBUS_H__

#include "lwroc_net_outgoing.h"
#include "lwroc_net_incoming.h"
#include "lwroc_net_reverse_link.h"
#include "lwroc_message.h"
#include "gen/lwroc_trigbus_msg_serializer.h"

/********************************************************************/

#define LWROC_NET_TRIGBUS_STATE_FRESH_MASTER             1
#define LWROC_NET_TRIGBUS_STATE_EXT_READ_SETUP           2
#define LWROC_NET_TRIGBUS_STATE_EXT_READ_RAW             3
#define LWROC_NET_TRIGBUS_STATE_MSG_RECEIVED             4
#define LWROC_NET_TRIGBUS_STATE_DELAY_MSG_SEND           5
#define LWROC_NET_TRIGBUS_STATE_FRESH_SLAVE              6
#define LWROC_NET_TRIGBUS_STATE_WAIT_MSG_SEND            7
#define LWROC_NET_TRIGBUS_STATE_EXT_WRITE_RAW            8
#define LWROC_NET_TRIGBUS_STATE_EXT_WRITE_RAW_DISCON     9

#define LWROC_NET_TRIGBUS_STATE_DISCON                  10

#define LWROC_NET_TRIGBUS_STATE_INT_WRITE               11
#define LWROC_NET_TRIGBUS_STATE_INT_READ_SETUP          12
#define LWROC_NET_TRIGBUS_STATE_INT_READ                13

/* */

#define LWROC_NET_TRIGBUS_STATUS_OUTGOING     1 /* probing... */
#define LWROC_NET_TRIGBUS_STATUS_MASTER       2 /* master connected */

#define LWROC_NET_TRIGBUS_STATUS_NO_SLAVE     3 /* waiting for conn... */
#define LWROC_NET_TRIGBUS_STATUS_SLAVE        4 /* slave connected */

#define LWROC_NET_TRIGBUS_STATUS_CONTROL      5 /* internal pipe */

/* The only time when a slave connection can fail is when reading or
 * writing a message, since that is when it dips its feet into the
 * outside world.  I.e. after having gotten a message without issue,
 * we can never fail with a request to enqueue a new message.  That
 * will (if at all) fail somewhere until the next message reception.
 */

#define LWROC_EB_IDENT_STATE_NOT_SENT         0
#define LWROC_EB_IDENT_STATE_SENT             1
#define LWROC_EB_IDENT_STATE_USED             2

typedef struct lwroc_net_trigbus_t
{
  lwroc_net_outgoing _out;

  /* Volatile since msg recv and send routines which check this are
   * called from a different thread when used by an EB.  To be able to
   * mark this volatile is also why we do not use conn->_out.state.
   */
  volatile int _state;
  volatile int _status;

  /* Used to notify event builder validation thread. */
  const lwroc_thread_block *_thread_notify;

  /* For error printing context: */
  /* With what protocol. */
  uint32_t _request;

  /* List of all masters.  With TRIVA - only one, but we may also have
   * other parties interested in our deadtime?
   *
   * This is actually just the list of us and our sibling connections
   * to masters.  (Hmm, in principle, masters are those that want to
   * listen to our deadtime.  Either we are in the same deadtime
   * domain (real master-slave), or just time-stamped).  In the latter
   * case, we'd better be master of the local dead-time domain.
   *
   * TODO: latter case: check that we _are_ local DT master.
   */
  pd_ll_item _masters;

  /* List of slaves, before added to triva state machine. */
  /* Could have used _slaves member below? */
  pd_ll_item _slaves_lst;

  /* List of all slave connections, including ours. */
  pd_ll_item _slaves;
  /* List of slaves that the triva state machine is expecting
   * responses from. */
  pd_ll_item _wait_slaves;

  /* The ident message we have sent to identify the stream in the EB. */
  uint32_t _eb_ident[3];
  /* 0 - not sent yet, 1 - sent, 2 - used */
  int _eb_ident_state;

  /* Should we send a LAM at next opportunity? */
  int _send_lam;
  /* Have we gotten a LAM this message cycle? */
  int _got_lam;
  /* Does this node want to terminate? */
  int _term_request;

  /* For a slave or control connection, we know what
   * message is acceptable as a response.
   */
  uint32_t _expect_msg_type;

  /* What kind of issue is master having with this slave, if any. */
  uint32_t _aux_status;

  /* Two separate structures, as (de)serialisation cannot copy
   * to the same memory area.  Raw buffer large enough to hold
   * any of the two messages.
   */
  uint32_t _raw[20];
  /* The plain message. */
  lwroc_trigbus_msg _msg;

} lwroc_net_trigbus;

/********************************************************************/

/* Unused prototype... (function is only used in declaring file.) */
char *lwroc_net_trigbus_fmt_msg_context(char *buf, size_t size,
					const  void *ptr);

/********************************************************************/

extern PD_LL_SENTINEL_ITEM(_lwroc_net_masters);
extern PD_LL_SENTINEL_ITEM(_lwroc_net_slaves);
extern PD_LL_SENTINEL_ITEM(_lwroc_net_eb_masters);
extern PD_LL_SENTINEL_ITEM(_lwroc_net_ebs);

/********************************************************************/

extern lwroc_outgoing_select_item_info lwroc_net_master_select_item_info;

extern lwroc_select_item_info lwroc_net_slave_select_item_info;

/********************************************************************/

/* Adds a slave connection, i.e. we are a master. */
lwroc_net_trigbus *lwroc_add_slave(const char *hostname, uint32_t request,
				   lwroc_net_reverse_link *revlink);

/* After successful connection from a slave. */
lwroc_net_trigbus *lwroc_net_become_slave(lwroc_net_incoming *incoming);

/********************************************************************/

/* Adds a master connection, i.e. we are a slave. */
void lwroc_add_master(const char *hostname, uint32_t request);

/********************************************************************/

lwroc_net_trigbus *lwroc_trigctrl_conn(lwroc_net_conn_monitor *conn_mon);

/********************************************************************/

void lwroc_net_trigbus_discon(lwroc_net_trigbus *conn);

int lwroc_net_trigbus_recv_msg(lwroc_net_trigbus *conn,
			       uint32_t expect_type_mask,
			       int *had_issue);

void lwroc_net_trigbus_send_msg(lwroc_net_trigbus *conn,
				uint32_t expect_msg_type);

void lwroc_net_trigctrl_send_msg(lwroc_net_trigbus *conn);

void lwroc_net_trigbus_send_look_at_me(lwroc_net_trigbus *conn);

/********************************************************************/

#endif/*__LWROC_NET_TRIGBUS_H__*/

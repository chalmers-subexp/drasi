/* This file is part of UCESB - a tool for data unpacking and processing.
 *
 * Copyright (C) 2016  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

/* Code taken from ucesb... */

#ifndef __LWROC_OPTIMISE_H__
#define __LWROC_OPTIMISE_H__


#if defined(__GNUC__) && __GNUC__ >= 3
#define   LIKELY(expr) __builtin_expect(expr,1)
#define UNLIKELY(expr) __builtin_expect(expr,0)
#else
#define   LIKELY(expr) expr
#define UNLIKELY(expr) expr
#endif

#include "../dtc_arch/acc_def/mfence.h"

#define LWROC_ALIGN_SZ(x,align) (((x) + ((align)-1)) & ~((align)-1))

#define LWROC_IS_ALIGNED(ptr,align) \
  (((uintptr_t) (const void *) (ptr)) % (align) == 0)

#endif/*__LWROC_OPTIMISE_H__*/

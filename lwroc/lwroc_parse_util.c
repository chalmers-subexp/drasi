/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_parse_util.h"
#include <string.h>
#include <stdlib.h>
#include "lwroc_message.h"
#include "../dtc_arch/acc_def/mystrndup.h"
#include "../dtc_arch/acc_def/myinttypes.h"

#include <ctype.h>

uint64_t lwroc_parse_hex_internal(const char *str)
{
  const char *p = str;
  char *end;
  int base = 10;
  uint64_t val;

  /* This check is done here instead of giving base 0 in order to
   * force base 10 otherwise - we do not want octal interpretation.
   */

  if (strncmp(str,"0x",2) == 0)
    {
      p += 2;
      base = 16;
    }

  val = strtoul (p, &end, base);

  if (end != str)
    {
      if      (strcmp(end,"k") == 0)  { val *= 1000;       return val; }
      else if (strcmp(end,"ki") == 0) { val <<= 10;        return val; }
      else if (strcmp(end,"M") == 0)  { val *= 1000000;    return val; }
      else if (strcmp(end,"Mi") == 0) { val <<= 20;        return val; }
      else if (strcmp(end,"G") == 0)  { val *= 1000000000; return val; }
      else if (strcmp(end,"Gi") == 0) { val <<= 30;        return val; }
    }

  if (end == str || *end)
    return (uint64_t) -1;

  return val;
}

uint64_t lwroc_parse_hex_double(const char *str, const char *purpose,
				double *fraction, int allow_normal)
{
  uint64_t val = (uint64_t) -1;

  if (fraction &&
      (strchr(str, '%') || strchr(str, '.')))
    {
      char *end;

      *fraction = strtod(str, &end);

      if (end != str)
	{
	  if (strcmp(end,"%") == 0)     { *fraction *= 0.01;
	    /* */                         return (uint64_t) -2; }
	  else if (strcmp(end,"") == 0) { return (uint64_t) -2; }
	}
    }
  else if (allow_normal)
    val = lwroc_parse_hex_internal(str);

  if (val == (uint64_t) -1)
    {
      LWROC_BADCFG_FMT("Malformed %s: %s", purpose, str);
    }

  return val;
}

uint64_t lwroc_parse_hex(const char *str, const char *purpose)
{
  return lwroc_parse_hex_double(str, purpose, NULL, 1);
}

uint64_t lwroc_parse_hex_limit(const char *str, const char *purpose,
			       uint64_t min, uint64_t max)
{
  uint64_t val = lwroc_parse_hex(str, purpose);

  if (val < min || val > max)
    {
      LWROC_BADCFG_FMT("Bad %s: (%" PRIu64 ") out of range "
		       "[%" PRIu64 ",%" PRIu64 "].",
		       purpose, val, min, max);
    }

  return val;
}

uint16_t lwroc_parse_hex_u16(const char *str, const char *purpose)
{
  return (uint16_t) lwroc_parse_hex_limit(str, purpose, 0, 0xffff);
}

uint32_t lwroc_parse_hex_u32(const char *str, const char *purpose)
{
  return (uint32_t) lwroc_parse_hex_limit(str, purpose, 0, 0xffffffff);
}

int lwroc_parse_time(const char *str, const char *purpose)
{
  char *end;
  uint64_t val;

  val = strtoul (str, &end, 10);

  if (end != str)
    {
      if      (strcmp(end,"s") == 0)   {              return (int) val; }
      else if (strcmp(end,"min") == 0) { val *= 60;   return (int) val; }
      else if (strcmp(end,"h") == 0)   { val *= 2600; return (int) val; }
      else if (*end == 0)
	{
	  LWROC_WARNING_FMT("Time parameter for %s is missing unit: %s",
			    purpose, str);
	  return (int) val;
	}
    }

  LWROC_BADCFG_FMT("Malformed time for %s: %s", purpose, str);
  return -1;
}

uint64_t lwroc_parse_time_ns(const char *str, const char *purpose)
{
  char *end;
  uint64_t val;

  val = strtoul (str, &end, 10);

  if (end != str)
    {
      if      (strcmp(end,"ns") == 0)   {                    return val; }
      else if (strcmp(end,"us") == 0)   { val *= 1000;       return val; }
      else if (strcmp(end,"ms") == 0)   { val *= 1000000;    return val; }
      else if (strcmp(end,"s") == 0)    { val *= 1000000000; return val; }
      else if (*end == 0)
	LWROC_BADCFG_FMT("Time parameter for %s is missing unit: %s",
			 purpose, str);
    }
  else
    LWROC_BADCFG_FMT("Malformed time for %s: %s", purpose, str);

  return (uint64_t) -1;
}

void lwroc_parse_hex_list_mask(uint64_t *mask,
			       const char *str, const char *purpose,
			       uint64_t max)
{
  const char *request;
  lwroc_parse_split_list parse_info;

  lwroc_parse_split_list_setup(&parse_info, str, ',');

  while ((request = lwroc_parse_split_list_next(&parse_info)) != NULL)
    {
      uint64_t val = lwroc_parse_hex_limit(request, purpose, 0, max);

      *mask |= ((uint64_t) 1) << val;
    }
}

void lwroc_parse_split_list_setup(lwroc_parse_split_list *info,
				  const char *str,
				  char separator)
{
  info->_str = str;
  info->_next_cmd = info->_str;
  info->_request = NULL;
  info->_separator = separator;
}

const char *lwroc_parse_split_list_next(lwroc_parse_split_list *info)
{
  const char *req_end;

  free(info->_request);
  if (info->_next_cmd == NULL)
    return NULL;
  info->_str = info->_next_cmd;

  /* Is there a separator? */
  req_end = strchr(info->_str,info->_separator);
  if (!req_end)
    {
      /* No separator; item ends at end of string. */
      req_end = info->_str + strlen(info->_str);
      info->_next_cmd = NULL;
    }
  else
    {
      /* Next item is after separator. */
      info->_next_cmd = req_end + 1;
    }

  /* Make a copy of the string, to have it null-terminated. */
  info->_request = strndup_chk(info->_str,(size_t) (req_end - info->_str));
  /* printf ("parse: '%s'\n", info->_request); */
  return info->_request;
}

int lwroc_parse_split_list_final(lwroc_parse_split_list *info)
{
  return info->_next_cmd == NULL;
}

/* Defined here since this lib is used by many... */

char *strdup_chk(const char *s)
{
  char *ret = strdup(s);
  if (!ret)
    LWROC_FATAL("Memory allocation error (strdup).");
  return ret;
}

char *strndup_chk(const char *s, size_t n)
{
  char *ret = strndup(s, n);
  if (!ret)
    LWROC_FATAL("Memory allocation error (strndup).");
  return ret;
}

void lwroc_string_to_print(char *dest, const void *src, size_t n)
{
  size_t i;

  memcpy(dest, src, n);
  dest[n] = 0;

  for (i = 0; i < n; i++)
    if (!isprint((unsigned char) dest[i]))
      dest[i] = '.';
}

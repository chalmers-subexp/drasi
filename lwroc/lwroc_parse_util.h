/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_PARSE_UTIL_H__
#define __LWROC_PARSE_UTIL_H__

#include "../dtc_arch/acc_def/mystdint.h"

#include <string.h>

/********************************************************************/

uint64_t lwroc_parse_hex(const char *str, const char *purpose);

uint64_t lwroc_parse_hex_limit(const char *str, const char *purpose,
			       uint64_t min, uint64_t max);
uint16_t lwroc_parse_hex_u16(const char *str, const char *purpose);
uint32_t lwroc_parse_hex_u32(const char *str, const char *purpose);

uint64_t lwroc_parse_hex_double(const char *str, const char *purpose,
				double *fraction, int allow_normal);

uint64_t lwroc_parse_hex_internal(const char *str); /* Avoid using. */

int lwroc_parse_time(const char *str, const char *purpose);

uint64_t lwroc_parse_time_ns(const char *str, const char *purpose);

void lwroc_parse_hex_list_mask(uint64_t *mask,
			       const char *str, const char *purpose,
			       uint64_t max);

/********************************************************************/

typedef struct lwroc_parse_split_list_t
{
  const char *_str;
  const char *_next_cmd;
  char *_request;
  char _separator;
} lwroc_parse_split_list;

void lwroc_parse_split_list_setup(lwroc_parse_split_list *info,
				  const char *str,
				  char separator);
const char *lwroc_parse_split_list_next(lwroc_parse_split_list *info);
int lwroc_parse_split_list_final(lwroc_parse_split_list *info);

#define LWROC_MATCH_C_PREFIX(prefix,post)		\
  (strncmp(request,prefix,strlen(prefix)) == 0 &&	\
   *(post = request + strlen(prefix)) != '\0')
#define LWROC_MATCH_C_ARG(name) (strcmp(request,name) == 0)

/********************************************************************/

void lwroc_string_to_print(char *dest, const void *src, size_t n);

/********************************************************************/

#endif/*__LWROC_PARSE_UTIL_H__*/

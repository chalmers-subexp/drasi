/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_pipe_buffer.h"
#include "lwroc_optimise.h"
#include "lwroc_thread_block.h"

#include "lwroc_message.h"

#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>

#include "../dtc_arch/acc_def/myprizd.h"

struct lwroc_pipe_buffer_consumer_t
{
  uint32_t _canary1;

  lwroc_pipe_buffer_control *_ctrl;

  int _this_i;

  volatile ssize_t _done;

  volatile ssize_t _wakeup_avail;

  const lwroc_thread_block *volatile _notify_consumer;

  void *volatile _held_context;

  uint32_t _canary2;

  /* 64-bit: 4 +(4)+ 8 + 4 +(4)+ 8 + 8 + 8 + 8 + 4 +(4) = 64 */
  /* 32-bit: 4 +     4 + 4 +     4 + 4 + 4 + 4 + 4      = 32 */
};

/* TODO: alignment as a constant.
 * Also, alignment could probably be 32 bytes on 32-bit platforms.
 * Reason would be that that is their typical cache line size.
 */

typedef union lwroc_pipe_buffer_consumer_union_t
{
  lwroc_pipe_buffer_consumer u;
  char dummy_align[64];
} lwroc_pipe_buffer_consumer_union;

struct lwroc_pipe_buffer_control_t
{
  union /* Common data, only written on setup. */
  {
    struct
    {
      uint32_t _canary1;

      /* Buffer. */
      char *_buf;

      /* Size of the (virtual) buffer, must be power of two. */
      ssize_t _wrap_size;
      /* Actual size of the buffer. */
      ssize_t _real_size;

      /* Amount to fill before waking consumer up. */
      ssize_t  _hysteresis_avail;

      int _consumers;

      uint32_t _canary2;
    } u;
    char dummy_align[64];
  } header;

  union /* Members written by producer, only read by consumers. */
  {
    struct
    {
      uint32_t _canary1;

      /* How far has the producer and consumer reached?  May wrap. */
      volatile ssize_t _avail;

      /* Please send a wake-up signal when passing these points. */
      volatile ssize_t _wakeup_done;

      /* Amount of space wasted at linear wrap (this round!). */
      volatile ssize_t _avail_waste;

      /* But only one of the consumers in this mask, other had cleared
       * far enough...
       */
      int _consumer_wait_i;

      /* Wake-up pipes to write to. */
      const lwroc_thread_block *volatile _notify_producer;

      uint32_t _canary2;

      /* Some additional words that are used by the producer however
       * they want, to be able to pass some other info to the
       * consumers.  Useful when operating in different processes, and
       * not wanting to set up another shared memory segment.
       *
       * The words are for free, given the padding size and the number
       * of ordinary members.
       */
      uint64_t _extra[LWROC_PIPE_BUFFER_PROD_EXTRA_UINT64_WORDS];
    } u;
    char dummy_align[64];
  } producer;

  /* Members written by consumer, only read by producer. */
  lwroc_pipe_buffer_consumer_union consumers[1];
};

size_t lwroc_power_2_round_up(size_t x)
{
  size_t y = 1;

  while (y < x)
    y <<= 1;

  return y;
}

void lwroc_pipe_buffer_init(lwroc_pipe_buffer_control **ctrl_ptr,
			    int consumers,
			    void *bufptr,
			    size_t size,
			    int hysteresis_factor)
{
  lwroc_pipe_buffer_control *ctrl;
  int i;

  /* TODO: ensure that ctrl is 64-bit aligned, or rather: aligned
   * for the platform cache-size.  Applies to the alignement-unions too...
   */
  ctrl = (lwroc_pipe_buffer_control *)
    malloc (sizeof (lwroc_pipe_buffer_control) +
	    sizeof (lwroc_pipe_buffer_consumer_union) *
	    (size_t) (consumers - 1));

  if (ctrl == NULL)
    LWROC_FATAL_FMT("Failed to allocate %" MYPRIzd " bytes for buffer.",
		    size);

#define LWROC_PIPE_BUF_CACHE_LINE 64 /* Must be power of 2. */

#define LWROC_PIPE_BUF_UNALIGN_MASK (((ssize_t) LWROC_PIPE_BUF_CACHE_LINE) - 1)

  if (bufptr)
    {
      size_t addr;
      size_t unalign = 0;

      addr = (size_t) bufptr;

      unalign = addr & LWROC_PIPE_BUF_UNALIGN_MASK;

      if (unalign)
	unalign = (-unalign) & LWROC_PIPE_BUF_UNALIGN_MASK;

      ctrl->header.u._buf = (void *) (addr + unalign);
      size -= unalign;
    }
  else
    {
      ctrl->header.u._buf = (char *) malloc (size);

      if (ctrl->header.u._buf == NULL)
	LWROC_FATAL_FMT("Failed to allocate %" MYPRIzd " bytes for buffer.",
			size);
    }

  ctrl->header.u._real_size = (ssize_t) size;
  /* Round to cache line. */
  ctrl->header.u._real_size &= ~LWROC_PIPE_BUF_UNALIGN_MASK;
  ctrl->header.u._wrap_size =
    (ssize_t) lwroc_power_2_round_up((size_t) ctrl->header.u._real_size);
  /*
  if (ctrl->header.u._real_size > 1000000)
    ctrl->header.u._wrap_size = 0x200000000ll;
  printf ("(%p) : %" MYPRIzd " %" MYPRIzd "\n",
	  ctrl,
	  ctrl->header.u._real_size,
	  ctrl->header.u._wrap_size);
  */
  ctrl->header.u._hysteresis_avail = 0;
  if (hysteresis_factor)
    ctrl->header.u._hysteresis_avail =
      ctrl->header.u._real_size / hysteresis_factor;

  ctrl->header.u._canary1 = 0x3456789a ^
    ((uint32_t) (ctrl->header.u._real_size +
		 ctrl->header.u._hysteresis_avail));
  ctrl->header.u._canary2 = 0x56789abc ^
    ((uint32_t) (ctrl->header.u._real_size -
		 ctrl->header.u._hysteresis_avail));

  ctrl->producer.u._canary1 = ctrl->header.u._canary1;
  ctrl->producer.u._canary2 = ctrl->header.u._canary2;

  ctrl->header.u._consumers = consumers;

  for (i = 0; i < ctrl->header.u._consumers; i++)
    {
      ctrl->consumers[i].u._canary1 = ctrl->header.u._canary1;
      ctrl->consumers[i].u._canary2 = ctrl->header.u._canary2;

      ctrl->consumers[i].u._ctrl = ctrl;
      ctrl->consumers[i].u._this_i = i;

      ctrl->consumers[i].u._held_context = NULL;
    }

  lwroc_pipe_buffer_clear(ctrl);

  *ctrl_ptr = ctrl;

  /*
  printf ("Alloc pipe buffer (%p), data %p -- %p\n",
	  ctrl, ctrl->header.u._buf, ctrl->header.u._buf + size);
  */
}

lwroc_pipe_buffer_consumer*
lwroc_pipe_buffer_get_consumer(lwroc_pipe_buffer_control *ctrl,
			       int i)
{
  if (i < 0 || i >= ctrl->header.u._consumers)
    LWROC_BUG_FMT("Request for consumer outside range [0,%d);",
		  ctrl->header.u._consumers);

  return &ctrl->consumers[i].u;
}

void lwroc_pipe_buffer_clear(lwroc_pipe_buffer_control *ctrl)
{
  int i;

  /* This function should only be called when it is known that the
   * other thread is not possibly using the buffer.
   */

  ctrl->producer.u._avail = 0;
  ctrl->producer.u._wakeup_done = 0;

  ctrl->producer.u._notify_producer = NULL;

  for (i = 0; i < ctrl->header.u._consumers; i++)
    {
      ctrl->consumers[i].u._done = 0;
      ctrl->consumers[i].u._wakeup_avail = 0;

      ctrl->consumers[i].u._notify_consumer = NULL;
    }
}

void lwroc_pipe_buffer_canary_check(lwroc_pipe_buffer_control *ctrl,
				    lwroc_pipe_buffer_consumer *consumer)
{
  if (consumer == NULL)
    consumer = &ctrl->consumers[0].u;

  if (ctrl->header.u._canary1   != consumer->_canary1 ||
      ctrl->producer.u._canary1 != consumer->_canary1 ||
      ctrl->header.u._canary2   != consumer->_canary2 ||
      ctrl->producer.u._canary2 != consumer->_canary2)
    {
      LWROC_BUG("Canary values of pipe buffer damaged.");
    }
}

char *lwroc_pipe_buffer_alloc_write(lwroc_pipe_buffer_control *ctrl,
				    size_t n, size_t *nwaste,
				    const lwroc_thread_block *thread_notify,
				    int wait)
{
  ssize_t offset;
  ssize_t segment;
  void *held_reported = NULL;
  size_t until_wrap;
  size_t need;

  lwroc_pipe_buffer_canary_check(ctrl, NULL);

  if ((ssize_t) n > ctrl->header.u._real_size)
    {
      LWROC_BUG_FMT("Requested more space (%" MYPRIzd ") "
		    "than buffer size (%" MYPRIszd "), impossible.",
		    n, ctrl->header.u._real_size);
      /* Hmmm...  not reached. */
      /* Need not set nwaste. */
      return NULL;
    }

 again:
  offset = ctrl->producer.u._avail & (ctrl->header.u._wrap_size - 1);
  segment = ctrl->header.u._real_size - offset;

  until_wrap = 0;
  need = n;

  /* We want the data in one chunk.  In case linear space is not
   * enough, we will pick it in pieces.
   */

  if (segment < (ssize_t) (n + 1))
    {
      /* The requested space is not continuous.  We need to waste the
       * remaining linear size.  As it complicates these loops
       * considerably, we return the smaller size to the caller, and
       * make it the responsibility of the reader to ignore these fill
       * records.
       *
       * The + 1 in the test is to ensure that we never exactly just
       * fit.  If we would fit exactly, and all space used, there
       * would be no waste reported next time.  But the waste report
       * is also used to handle _real_size < _wrap_size.  We need to
       * waste this entire space.
       *
       * Note that even if it would be a perfect match size-wise, the
       * caller may choose to not use all the requested space, which
       * could lead to the remaining space not being enough for the
       * marker.  Always make sure a marker fits in the remaining
       * linear space.
       */
      until_wrap = need = (size_t)
	(segment +
	 (ctrl->header.u._wrap_size - ctrl->header.u._real_size));
    }

  /* And wait until the space is ready. */

  for ( ; ; )
    {
      int consumer_wait_i = -1;
      ssize_t least_space = (ssize_t) need;
      int space_available;
      int i;
      void *hold_report = NULL;

      /* We have to ensure that space is available from all consumers.
       */

      for (i = 0; i < ctrl->header.u._consumers; i++)
	{
	  ssize_t space = ctrl->header.u._wrap_size -
	    (ctrl->producer.u._avail - ctrl->consumers[i].u._done);

	  if (space < least_space) /* implies space < n */
	    {
	      /* It only makes sense to have one client wake us up.
	       * Begin by the client with the most to consume.  Will
	       * presumably take the longest time.
	       */
	      least_space = space;
	      consumer_wait_i = i;
	      hold_report = ctrl->consumers[i].u._held_context;
	    }
	}

      if (consumer_wait_i == -1)
	break;

      /*{ int ii;
      for (ii = 0; ii < ctrl->header.u._consumers; ii++)
	{
	  printf ("%p: WQ1  %" MYPRIzd "  [%d] %" MYPRIzd "\n", ctrl,
		  ctrl->producer.u._avail, ii, ctrl->consumers[ii].u._done);
	}
	fflush(stdout); }*/

      /* We will have to sleep here. */

      /* First set how much space we need. */
      /* And which consumers should think about waking us up. */
      MFENCE;
      ctrl->producer.u._wakeup_done =
	ctrl->producer.u._avail - ctrl->header.u._wrap_size + (ssize_t) need;
      ctrl->producer.u._consumer_wait_i = consumer_wait_i;
      MFENCE;
      ctrl->producer.u._notify_producer = thread_notify;
      MFENCE;

      space_available = 1;

      /* Did the space become available in the meantime? */
      for (i = 0; i < ctrl->header.u._consumers; i++)
	{
	  ssize_t space = ctrl->header.u._wrap_size -
	    (ctrl->producer.u._avail - ctrl->consumers[i].u._done);

	  if (space < (ssize_t) need)
	    {
	      space_available = 0;
	      if (i == consumer_wait_i)
		consumer_wait_i = -1;
	    }
	}

      if (space_available)
	break;
      if (consumer_wait_i != -1)
	{
	  /* The consumer that we told to wake us up became ready.
	   * May (or may not be) that it sent us a token.
	   * In the case when it did not, we have to get someone else
	   * to send us a token.
	   */
	  continue;
	}

      /* Not available.  Wait for a token. */

      if (hold_report && hold_report != held_reported)
	{
	  LWROC_CCWARNING(hold_report,
			  "Output buffer full, "
			  "with hold destination missing client.");
	  held_reported = hold_report;
	}

      if (wait)
	{
	  /* We are in the thread that writes.
	   * We want the requested space, so will wait until it is available.
	   */

	  lwroc_thread_block_get_token(thread_notify);
	}
      else
	{
	  if (nwaste)
	    *nwaste = 0; /* For easier tests outside when wait = 1. */
	  return NULL;
	}

      /* Note that token may be returned for other reason.  Must check
       * success again.
       */
    }

  /*{ int ii;
  for (ii = 0; ii < ctrl->header.u._consumers; ii++)
    {
      printf ("%p: WG  %" MYPRIzd "  [%d] %" MYPRIzd " -> %d\n", ctrl,
	      ctrl->producer.u._avail, ii, ctrl->consumers[ii].u._done, n);
    }
    fflush(stdout); }*/

  if (nwaste)
    *nwaste = until_wrap;
  else if (until_wrap)
    {
      lwroc_pipe_buffer_waste_write(ctrl, until_wrap);
      goto again;
    }

  return ctrl->header.u._buf + offset;
}

void lwroc_pipe_buffer_did_write(lwroc_pipe_buffer_control *ctrl,
				 size_t n, int no_hysteresis)
{
  int i;

  for (i = 0; i < ctrl->header.u._consumers; i++)
    {
      lwroc_pipe_buffer_consumer *consumer = &ctrl->consumers[i].u;

      if (ctrl->producer.u._avail + (ssize_t) n - consumer->_done >
	  ctrl->header.u._wrap_size)
	LWROC_BUG_FMT("Wrote more than space in buffer (consumer %d) "
		      "(%" MYPRIszd " + %" MYPRIzd " - %" MYPRIszd " > "
		      "%" MYPRIszd ").",
		      i,
		      ctrl->producer.u._avail, n, consumer->_done,
		      ctrl->header.u._wrap_size);
    }

  SFENCE; /* Data written before reporting available. */
  ctrl->producer.u._avail += (ssize_t) n;

  /*{ int ii;
  for (ii = 0; ii < ctrl->header.u._consumers; ii++)
    {
    printf ("%p: Wrote  %" MYPRIzd "  [%d] %" MYPRIzd "\n", ctrl,
	    ctrl->producer.u._avail, ii, ctrl->consumers[ii].u._done);
    }
    fflush(stdout); }*/


  for (i = 0; i < ctrl->header.u._consumers; i++)
    {
      lwroc_pipe_buffer_consumer *consumer = &ctrl->consumers[i].u;

      if (consumer->_notify_consumer)
	{
	  MFENCE;
	  if (no_hysteresis ||
	      (ctrl->producer.u._avail - consumer->_wakeup_avail) >= 0)
	    {
	      /* The consumer was waiting for us. */
	      /*
		printf("notify consumer [%d] %" MYPRIzd " %" MYPRIzd " [%" MYPRIzd "] %d : %p\n",
		       i,
		       ((ctrl->producer.u._avail) - (consumer->_done)),
		       ((ctrl->producer.u._avail) - (consumer->_wakeup_avail)),
		       ctrl->header.u._hysteresis_avail,
		       no_hysteresis,
		       consumer->_notify_consumer);
	      */
	      LWROC_THREAD_BLOCK_NOTIFY(&consumer->_notify_consumer);
	    }
	}
    }
}

void lwroc_pipe_buffer_waste_write(lwroc_pipe_buffer_control *ctrl,
				   size_t nwaste)
{
  char *p;

  /*
  printf ("write waste: %zd / %zd\n",
	  nwaste, ctrl->header.u._wrap_size);
  fflush(stdout);
  */

  /* We first write the waste information.
   *
   * When we do this, all consumers must already have passed the end
   * of the buffer, such that it is unique to which wrap the waste
   * belongs.
   *
   * After writing the waste member, we use the normal routine to add
   * the write amount.
   */

  ctrl->producer.u._avail_waste =
    ctrl->producer.u._avail & (ctrl->header.u._wrap_size - 1);

  /* Write some garbage at the location, to hopefully provoke errors
   * in case someone reads it.
   */

  p = ctrl->header.u._buf + ctrl->producer.u._avail_waste;

  *p = (char) 0xff;

  SFENCE; /* Waste info visible before producer marks available. */

  lwroc_pipe_buffer_did_write(ctrl, nwaste, 0);
}

void lwroc_pipe_buffer_notify_consumer(lwroc_pipe_buffer_consumer *consumer,
				       const lwroc_thread_block *thread_notify,
				       size_t hysteresis)
{
  lwroc_pipe_buffer_control *ctrl = consumer->_ctrl;

  ssize_t wakeup_avail = consumer->_done + (ssize_t) hysteresis;
  /* Does the wakeup_avail go into the virtual area. */
  ssize_t wakeup_segment = wakeup_avail & (ctrl->header.u._wrap_size - 1);
  ssize_t wakeup_wrapped =
    (wakeup_avail ^ consumer->_done) & ~(ctrl->header.u._wrap_size - 1);

  if (wakeup_segment > ctrl->header.u._real_size ||
      wakeup_wrapped)
    wakeup_avail += (ctrl->header.u._wrap_size -
		     ctrl->header.u._real_size);

  /*
  if (ctrl->header.u._real_size > 10000)
    {
      printf ("notify: %" MYPRIzd " seg %" MYPRIzd " / %" MYPRIzd "\n",
	      wakeup_avail - consumer->_done,
	      wakeup_segment,
	      ctrl->header.u._real_size);
    }
  */

  MFENCE;
  consumer->_wakeup_avail = wakeup_avail;
  MFENCE;
  consumer->_notify_consumer = thread_notify;
  MFENCE;
}

size_t lwroc_pipe_buffer_avail_read(lwroc_pipe_buffer_consumer *consumer,
				    const lwroc_thread_block *thread_notify,
				    char **data, size_t *nwaste,
				    size_t noffset, size_t hysteresis)
{
  lwroc_pipe_buffer_control *ctrl = consumer->_ctrl;
  ssize_t fill;
  ssize_t offset;
  ssize_t full_segment;

  /* The consumer (that writes data to the network) needs to know if
   * there is any data available.
   */

  lwroc_pipe_buffer_canary_check(ctrl, consumer);

 again:
  fill = ctrl->producer.u._avail - consumer->_done;

  /* noffset tells how much we already know exists.
   * that much actually must exist, or we have a bug.
   */

  if ((ssize_t) noffset > fill)
    LWROC_BUG_FMT("Requesting with beyond available buffer fill "
		  "(%" MYPRIzd " > %" MYPRIszd ")",
		  noffset, fill);

  /* If there is not more data than known, we should go wait. */
  fill -= (ssize_t) noffset;

  if (fill == 0)
    {
      if (!hysteresis)
	hysteresis = (size_t) ctrl->header.u._hysteresis_avail;

      hysteresis += noffset;

      /* We should request to be woken when data is available.  In
       * order to avoid ping-pong between the two threads, put a
       * hysteresis of a quarter of the buffer size.
       *
       * To ensure timely delivery of events and messages even in
       * low-rate situations, the sending thread also has a timeout of
       * say .1 s, ensuring that we never stall for long times.
       */

      /*{ int ii;
	for (ii = 0; ii < ctrl->header.u._consumers; ii++)
	  {
	    printf ("%p: Q1[%d]  %" MYPRIzd "  [%d] %" MYPRIzd "\n", ctrl,
		    consumer->_this_i,
		    ctrl->producer.u._avail, ii, ctrl->consumers[ii].u._done);
	  }
	  fflush(stdout); }*/

      if (thread_notify == NULL)
	{
	  if (nwaste)
	    *nwaste = 0;
	  return 0;
	}

      lwroc_pipe_buffer_notify_consumer(consumer, thread_notify,
					hysteresis);

      /* It may happen that data became available in the meantime,
       * recheck.
       */

      fill = ctrl->producer.u._avail - consumer->_done;

      fill -= (ssize_t) noffset;

      if (fill == 0)
	{
	  if (nwaste)
	    *nwaste = 0;
	  return 0;
	}
    }

  offset =
    (consumer->_done + (ssize_t) noffset) & (ctrl->header.u._wrap_size - 1);
  /* When reading, we consider the entire virtual buffer part of the
   * segment.  I.e. to wrap_size and not only to real_size.  It is up
   * to the user to have included waste items such that the overshoot
   * area is just skipped (and never read).
   */
  full_segment = ctrl->header.u._wrap_size - offset;

  /* We then need to check if we are about to hit the end of the
   * linear space.
   */

  if (full_segment <= fill)
    {
      /* Only if we hit the linear limit, we have wrapped.  (May be an
       * exact wrap.)
       *
       * If so, we may be at the waste point.
       */

      LFENCE; /* Make sure we load _avail_waste after _avail. */

      if (offset >= ctrl->producer.u._avail_waste)
	{
	  size_t until_wrap;

	  assert (offset == ctrl->producer.u._avail_waste);

	  until_wrap =
	    (size_t) (ctrl->header.u._wrap_size -
		      ctrl->producer.u._avail_waste);
	  /*
	  printf ("read report waste: %zd / %zd\n",
		  until_wrap, ctrl->header.u._wrap_size);
	  fflush(stdout);
	  */
	  if (nwaste)
	    {
	      *nwaste = until_wrap;
	      return 0;
	    }
	  else
	    {
	      lwroc_pipe_buffer_waste_read(consumer, until_wrap);
	      goto again;
	    }
	}

      /* The segment available is only to the waste location. */
      full_segment = ctrl->producer.u._avail_waste - offset;

      fill = full_segment;
    }

  if (data)
    *data = ctrl->header.u._buf + offset;

  /*{ int ii;
  for (ii = 0; ii < ctrl->header.u._consumers; ii++)
    {
      printf ("%p: G[%d]  %" MYPRIzd "  [%d] %" MYPRIzd " -> %" MYPRIzd "\n", ctrl,
	      consumer->_this_i,
	      ctrl->producer.u._avail, ii, ctrl->consumers[ii].u._done, fill);
    }
    fflush(stdout); }*/

  /* Data reads (later in function that called us) after reading the
   * reports from producer about data availability above.
   */
  LFENCE;
  return (size_t) fill;
}

size_t
lwroc_pipe_buffer_fill_consumer_internal(lwroc_pipe_buffer_consumer *consumer,
					 lwroc_pipe_buffer_control *ctrl)
{
  /* Make copy of the volatile variables, so both calculations are
   * based on the same values.
   */
  ssize_t avail = ctrl->producer.u._avail;
  ssize_t done  = consumer->_done;
  size_t  fill = (size_t) (avail - done);
  ssize_t wrapped = (avail ^ done) & ~(ctrl->header.u._wrap_size - 1);
  /*
  printf("%14" MYPRIzd " %14" MYPRIzd " : f %14" MYPRIzd " : "
	 "%d : f %10" MYPRIzd "\n",
	 avail, done, fill, wrapped ? 1 : 0,
	 fill - (wrapped ? ((size_t) (ctrl->header.u._wrap_size -
				      ctrl->header.u._real_size)) : 0));
  */
  if (wrapped)
    {
      /* TODO: remove this assert.  And all asserts in this file. */
      assert (fill >=
	      (size_t) (ctrl->header.u._wrap_size -
			ctrl->header.u._real_size));
      fill -= (size_t) (ctrl->header.u._wrap_size -
			ctrl->header.u._real_size);
    }
  return fill;
}

size_t lwroc_pipe_buffer_fill(lwroc_pipe_buffer_control *ctrl)
{
  int i;
  size_t max_fill = 0;

  /* Find out the maximum fill. */

  for (i = 0; i < ctrl->header.u._consumers; i++)
    {
      size_t fill =
	lwroc_pipe_buffer_fill_consumer_internal(&ctrl->consumers[i].u, ctrl);

      if (fill > max_fill)
	max_fill = fill;
    }
  return max_fill;
}

size_t
lwroc_pipe_buffer_fill_due_to_consumer(lwroc_pipe_buffer_consumer *consumer)
{
  lwroc_pipe_buffer_control *ctrl = consumer->_ctrl;
  return lwroc_pipe_buffer_fill_consumer_internal(consumer, ctrl);
}

int lwroc_pipe_buffer_blocked(lwroc_pipe_buffer_control *ctrl)
{
  return ctrl->producer.u._notify_producer != NULL;
}

int
lwroc_pipe_buffer_blocked_from_consumer(lwroc_pipe_buffer_consumer *consumer)
{
  lwroc_pipe_buffer_control *ctrl = consumer->_ctrl;
  return ctrl->producer.u._notify_producer != NULL;
}

char *lwroc_pipe_buffer_get_read(lwroc_pipe_buffer_consumer *consumer)
{
  lwroc_pipe_buffer_control *ctrl = consumer->_ctrl;
  ssize_t offset = consumer->_done & (ctrl->header.u._wrap_size - 1);

  return ctrl->header.u._buf + offset;
}

void lwroc_pipe_buffer_did_read(lwroc_pipe_buffer_consumer *consumer,
				size_t n)
{
  lwroc_pipe_buffer_control *ctrl = consumer->_ctrl;

  if (consumer->_done + (ssize_t) n - ctrl->producer.u._avail > 0)
    LWROC_BUG_FMT("Read more than available in buffer "
		  "(%" MYPRIszd " + %" MYPRIzd " > %" MYPRIszd ").",
		  consumer->_done, n, ctrl->producer.u._avail);

  MFENCE; /* Data consumed before reporting done. */
  consumer->_done += (ssize_t) n;

  /* {
    ssize_t offset =
      (consumer->_done) & (ctrl->header.u._wrap_size - 1);

    if (offset >= ctrl->header.u._real_size)
      LWROC_BUG("Read to end up beyond end of buffer.");
      } */

  /*{ int ii;
    for (ii = 0; ii < ctrl->header.u._consumers; ii++)
      {
	printf ("%p: Read[%d] %" MYPRIzd "  [%d] %" MYPRIzd "\n", ctrl,
		consumer->_this_i,
		ctrl->producer.u._avail, ii, ctrl->consumers[ii].u._done);
      }
      fflush(stdout); }*/

  if (ctrl->producer.u._notify_producer &&
      ctrl->producer.u._consumer_wait_i == consumer->_this_i)
    {
      MFENCE;
      if ((consumer->_done - ctrl->producer.u._wakeup_done) >= 0)
	{
	  /* The producer was waiting for us. */
	  LWROC_THREAD_BLOCK_NOTIFY(&ctrl->producer.u._notify_producer);
	}
    }
}

void lwroc_pipe_buffer_waste_read(lwroc_pipe_buffer_consumer *consumer,
				  size_t nwaste)
{
  lwroc_pipe_buffer_control *ctrl = consumer->_ctrl;
  char *p;

  /* Check that the waste marker is intact. */

  p = ctrl->header.u._buf + ctrl->producer.u._avail_waste;

  if (*p != (char) 0xff)
    LWROC_BUG("Waste marker of pipe buffer damaged.");

  /* Do the actual update using the normal function. */
  lwroc_pipe_buffer_did_read(consumer, nwaste);
}

void lwroc_pipe_buffer_did_read_fast(lwroc_pipe_buffer_consumer *consumer,
				     lwroc_pipe_buffer_consumer *target)
{
  ssize_t ahead;

  if (target->_ctrl != consumer->_ctrl)
    LWROC_BUG("Attempt to fast-forward in different buffer.");

  ahead = target->_done - consumer->_done;

  if ((ssize_t) ahead < 0)
    LWROC_BUG("Attempt to fast-forward backwards.");

  lwroc_pipe_buffer_did_read(consumer, (size_t) ahead);
}

size_t lwroc_pipe_buffer_size(lwroc_pipe_buffer_control *ctrl)
{
  return (size_t) ctrl->header.u._real_size;
}

size_t
lwroc_pipe_buffer_size_from_consumer(lwroc_pipe_buffer_consumer *consumer)
{
  lwroc_pipe_buffer_control *ctrl = consumer->_ctrl;
  return (size_t) ctrl->header.u._real_size;
}

size_t lwroc_pipe_buffer_need_by_write(lwroc_pipe_buffer_consumer *consumer)
{
  lwroc_pipe_buffer_control *ctrl = consumer->_ctrl;
  /* Return non-zero number of bytes required by the producer, if the
   * producer is waiting for the consumer to free up some space.
   */
  if (ctrl->producer.u._notify_producer)
    {
      ssize_t needed;

      MFENCE;
      /* If _notify_producer is set, _wakeup_done has also been set and
       * should not be changed (to smaller).  Let's play it safe...
       */
      needed =
	ctrl->producer.u._wakeup_done - consumer->_done;

      return needed >= 0 ? (size_t) needed : 0;
    }
  return 0;
}

int lwroc_pipe_buffer_num_consumers(lwroc_pipe_buffer_control *ctrl)
{
  return ctrl->header.u._consumers;
}

int lwroc_pipe_buffer_num_consumers_fr_cons(lwroc_pipe_buffer_consumer *
					    consumer)
{
  return consumer->_ctrl->header.u._consumers;
}

void *lwroc_pipe_buffer_get_extra(lwroc_pipe_buffer_control *ctrl)
{
  return ctrl->producer.u._extra;
}

void lwroc_pipe_buffer_set_hold(lwroc_pipe_buffer_consumer *consumer,
				void *hold_context)
{
  consumer->_held_context = hold_context;
}

int lwroc_pipe_buffer_has_hold(lwroc_pipe_buffer_control *ctrl)
{
  int i;

  for (i = 0; i < ctrl->header.u._consumers; i++)
    if (ctrl->consumers[i].u._held_context)
      {
	return 1;
      }

  return 0;
}

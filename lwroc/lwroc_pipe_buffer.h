/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_PIPE_BUFFER_H__
#define __LWROC_PIPE_BUFFER_H__

#include "lwroc_thread_block.h"

#include <stdlib.h>
#include "../dtc_arch/acc_def/mystdint.h"

struct lwroc_pipe_buffer_control_t;
typedef struct lwroc_pipe_buffer_control_t lwroc_pipe_buffer_control;

struct lwroc_pipe_buffer_consumer_t;
typedef struct lwroc_pipe_buffer_consumer_t lwroc_pipe_buffer_consumer;

#define LWROC_PIPE_BUFFER_PROD_EXTRA_UINT64_WORDS  3

void lwroc_pipe_buffer_init(lwroc_pipe_buffer_control **ctrl_ptr,
			    int consumers,
			    void *bufptr,
			    size_t size,
			    int hysteresis_factor);

lwroc_pipe_buffer_consumer*
lwroc_pipe_buffer_get_consumer(lwroc_pipe_buffer_control *ctrl,
			       int i);

void lwroc_pipe_buffer_clear(lwroc_pipe_buffer_control *ctrl);

char *lwroc_pipe_buffer_alloc_write(lwroc_pipe_buffer_control *ctrl,
				    size_t n, size_t *nwaste,
				    const lwroc_thread_block *thread_notify,
				    int wait);

void lwroc_pipe_buffer_did_write(lwroc_pipe_buffer_control *ctrl,
				 size_t n, int no_hysteresis);

void lwroc_pipe_buffer_waste_write(lwroc_pipe_buffer_control *ctrl,
				   size_t nwaste);

size_t lwroc_pipe_buffer_fill(lwroc_pipe_buffer_control *ctrl);

int lwroc_pipe_buffer_blocked(lwroc_pipe_buffer_control *ctrl);

int
lwroc_pipe_buffer_blocked_from_consumer(lwroc_pipe_buffer_consumer *consumer);

size_t
lwroc_pipe_buffer_fill_due_to_consumer(lwroc_pipe_buffer_consumer *consumer);

size_t lwroc_pipe_buffer_need_by_write(lwroc_pipe_buffer_consumer *consumer);

void lwroc_pipe_buffer_notify_consumer(lwroc_pipe_buffer_consumer *consumer,
				       const lwroc_thread_block *thread_notify,
				       size_t hysteresis);

size_t lwroc_pipe_buffer_avail_read(lwroc_pipe_buffer_consumer *consumer,
				    const lwroc_thread_block *thread_notify,
				    char **data, size_t *nwaste,
				    size_t noffset, size_t hysteresis);

char *lwroc_pipe_buffer_get_read(lwroc_pipe_buffer_consumer *consumer);

void lwroc_pipe_buffer_did_read(lwroc_pipe_buffer_consumer *consumer,
				size_t nwaste);

void lwroc_pipe_buffer_waste_read(lwroc_pipe_buffer_consumer *consumer,
				  size_t n);

void lwroc_pipe_buffer_did_read_fast(lwroc_pipe_buffer_consumer *consumer,
				     lwroc_pipe_buffer_consumer *target);

size_t lwroc_pipe_buffer_size(lwroc_pipe_buffer_control *ctrl);

size_t
lwroc_pipe_buffer_size_from_consumer(lwroc_pipe_buffer_consumer *consumer);

int lwroc_pipe_buffer_num_consumers(lwroc_pipe_buffer_control *ctrl);

int lwroc_pipe_buffer_num_consumers_fr_cons(lwroc_pipe_buffer_consumer*
					    consumer);

void *lwroc_pipe_buffer_get_extra(lwroc_pipe_buffer_control *ctrl);

void lwroc_pipe_buffer_set_hold(lwroc_pipe_buffer_consumer *consumer,
				void *hold_context);

int lwroc_pipe_buffer_has_hold(lwroc_pipe_buffer_control *ctrl);

size_t lwroc_power_2_round_up(size_t x);

#endif/*__LWROC_PIPE_BUFFER_H__*/


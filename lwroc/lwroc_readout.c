/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Bastian Loeher  <b.loeher@gsi.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_main_iface.h"
#include "lwroc_message.h"
#include "lwroc_message_internal.h"
#include "lwroc_mon_block.h"

#include <string.h>
#include "../dtc_arch/acc_def/myinttypes.h"

#include "lmd/lwroc_lmd_event.h"

#include "lwroc_readout.h"
#include "lwroc_triva_readout.h"
#include "lwroc_triva_control.h"
#include "lwroc_triva.h"
#include "lwroc_data_pipe.h"

void lwroc_lmd_dump_event_header(lmd_event_10_1_host *);
void lwroc_lmd_dump_subevent_header(lmd_subevent_10_1_host *);

extern lwroc_monitor_main_block  _lwroc_mon_main;
extern lwroc_mon_block          *_lwroc_mon_main_handle;

struct lwroc_readout_functions _lwroc_readout_functions;

void lwroc_main_pre_parse_setup(void)
{
  /* This function may only initialise structures. */

  /* Initialise readout function pointers */
  memset(&_lwroc_readout_functions, 0, sizeof(struct lwroc_readout_functions));

  lwroc_readout_pre_parse_functions();
}

lwroc_gdf_format_functions *lwroc_main_get_fmt(void)
{
  if (_lwroc_readout_functions.fmt == NULL)
    LWROC_FATAL("_lwroc_readout_functions.fmt is NULL.  "
		"(Want &_lwroc_lmd_format_functions ?)");

  return _lwroc_readout_functions.fmt;
}

/* Different names to let chkcmdlineoptsdoc.pl differentiate. */
#define lwroc_main_readout_cmdline_usage      lwroc_main_cmdline_usage
#define lwroc_main_readout_parse_cmdline_arg  lwroc_main_parse_cmdline_arg

void lwroc_main_readout_cmdline_usage(void)
{
  if (_lwroc_readout_functions.cmdline_fcns.usage)
    return _lwroc_readout_functions.cmdline_fcns.usage();
}

int lwroc_main_readout_parse_cmdline_arg(const char *request)
{
  /*NOHELPCALL*/ /* We are a option subset, --help handled elsewhere. */

  if (_lwroc_readout_functions.cmdline_fcns.parse_arg)
    return _lwroc_readout_functions.cmdline_fcns.parse_arg(request);
  return 0;
}

void lwroc_main_parse_setup(lwroc_data_pipe_cfg *pipe_cfg)
{
  (void) pipe_cfg;

  if (_config._triva)
    lwroc_triva_parse_setup();
}

void lwroc_main_setup(void)
{
  /* Until we have a separate process, the readout thread is in the
   * main thread. */
  _lwroc_readout_thread = _lwroc_main_thread;

  if (_config._triva)
    lwroc_triva_setup();
  else
    lwroc_triva_no_setup();

  lwroc_readout_setup_functions();

  if (_config._triva)
    lwroc_triva_control_prepare_thread();
}

void lwroc_main_bind(void)
{
}

void lwroc_main_loop(void)
{
  int start_no_stop = 0;

  LWROC_INFO("call readout_init...");

  /* This is before the readout_functions.init() on purpose.
   *
   * Those init functions have a tendency to generate a lot of log
   * messages, and therefore must happen after we have a serious log
   * destination.
   */
  lwroc_msg_wait_any_msg_client(_lwroc_main_thread->_block);

  if (_lwroc_readout_functions.init)
    _lwroc_readout_functions.init();

  if (_config._triva)
    {
      lwroc_triva_readout(&start_no_stop);
    }
  else
    {
      LWROC_INFO("Untriggered loop (no --triva option) - GO!");

      if (_lwroc_readout_functions.untriggered_loop)
	{
	  /* Untriggered loop has no start/stop, so we set it as active
	   * here.
	   */
	  lwroc_data_pipes_readout_active(1);

	  _lwroc_readout_functions.untriggered_loop(&start_no_stop);
	}
      else
	{
	  LWROC_FATAL("No untriggered_loop callback defined.");
	}
    }

  /* Readout certainly stopped. */
  lwroc_data_pipes_readout_active(0);

  if (_lwroc_readout_functions.uninit)
    _lwroc_readout_functions.uninit(start_no_stop);
}

void lwroc_mon_main_fetch(void *ptr)
{
  (void) ptr;
}

void lwroc_lmd_dump_event_header(lmd_event_10_1_host *ev)
{
  printf("event = {_header = {l_dlen = %"PRIu32", i_type = %d, i_subtype = %d},\n"
	 "         _info = {i_dummy = %d, i_trigger = %d, l_count = %"PRIu32"}}\n",
	 ev->_header.l_dlen,
	 ev->_header.i_type,
	 ev->_header.i_subtype,
	 ev->_info.i_dummy,
	 ev->_info.i_trigger,
	 ev->_info.l_count);
}

void lwroc_lmd_dump_subevent_header(lmd_subevent_10_1_host *ev)
{
  printf("subevent = {_header = {l_dlen = %"PRIu32", i_type = %d, i_subtype = %d},\n"
	 "            _info = {h_control = %d, h_subcrate = %d, i_procid = %"PRIu32"}}\n",
	 ev->_header.l_dlen,
	 ev->_header.i_type,
	 ev->_header.i_subtype,
	 ev->h_control,
	 ev->h_subcrate,
	 ev->i_procid);
}

/********************************************************************/

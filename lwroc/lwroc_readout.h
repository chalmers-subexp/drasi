/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Bastian Loeher  <b.loeher@gsi.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_READOUT_H__
#define __LWROC_READOUT_H__

#include "lwroc_thread_util.h"
#include "lwroc_cmdline_fcns.h"
#include "gdf/lwroc_gdf_util.h"

struct cmvlc_stackcmdbuf;

typedef void  (*lwroc_init_func)(void);
typedef void  (*lwroc_uninit_func)(int start_no_stop);
typedef void  (*lwroc_start_stop_loop_func)(int start);
typedef void  (*lwroc_idle_read_func)(void);
typedef void  (*lwroc_read_event_func)(uint64_t cycle, uint16_t trig);
typedef void  (*lwroc_format_event_func)(uint64_t cycle, uint16_t trig,
					 const void *input, size_t input_len);
typedef void  (*lwroc_triva_event_loop_func)(int *start_no_stop);
typedef void  (*lwroc_untriggered_loop_func)(int *start_no_stop);

#if HAS_CMVLC
typedef int   (*lwroc_prepare_cmvlc_func)(unsigned char readout_no,
					  struct cmvlc_stackcmdbuf *stack);
#endif/*HAS_CMVLC*/

/* Interface for user code */
struct lwroc_readout_functions
{
  lwroc_init_func              init;
  lwroc_uninit_func            uninit;
  lwroc_start_stop_loop_func   start_stop_loop;
  lwroc_idle_read_func         idle_read;
  lwroc_read_event_func        read_event;
  lwroc_format_event_func      format_event;
  lwroc_triva_event_loop_func  triva_event_loop;
  lwroc_untriggered_loop_func  untriggered_loop;
  struct lwroc_cmdline_functions  cmdline_fcns;
  lwroc_gdf_format_functions  *fmt;   /* The data format generated. */

#if HAS_CMVLC
  lwroc_prepare_cmvlc_func     prepare_cmvlc;
  unsigned char                cmvlc_readout_for_trig[16];
#endif/*HAS_CMVLC*/
};

/* This function needs to be called to setup the user functions. */
void lwroc_readout_pre_parse_functions(void);
void lwroc_readout_setup_functions(void); /* This was lwroc_readout__setup_functions(), i.e. double __ for a while (5 years), to cause compile failures:  Note that old lwroc_readout_setup_functions() is now lwroc_readout_pre_parse_functions(). */

#include "lwroc_track_timestamp.h"

extern int _readout_pipe_wr; /* For messages to the readout thread. */

extern lwroc_thread_instance *_lwroc_readout_thread;

#endif/*__LWROC_READOUT_H__*/

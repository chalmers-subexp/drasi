/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_select_util.h"
#include "lwroc_thread_block.h"
#include "lwroc_message.h"

#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/********************************************************************/

void lwroc_select_info_clear(lwroc_select_info *si)
{
  si->nfd = -1;

  FD_ZERO(&si->readfds);
  FD_ZERO(&si->writefds);

  timerclear(&si->next_time);
}

/********************************************************************/

struct timeval *lwroc_select_info_time_left(lwroc_select_info *si)
{
  if (timerisset(&si->next_time))
    {
      if (timercmp(&si->next_time, &si->now, <))
	{
	  si->next_time.tv_sec  = 0;
	  si->next_time.tv_usec = 0;
	}
      else
	{
	  si->next_time.tv_sec  -= si->now.tv_sec;
	  si->next_time.tv_usec -= si->now.tv_usec;

	  if (si->next_time.tv_usec < 0)
	    {
	      si->next_time.tv_sec--;
	      si->next_time.tv_usec += 1000000;
	    }
	}
      return &si->next_time;
    }
  return NULL;
}

/********************************************************************/

void lwroc_select_info_time_setup(lwroc_select_info *si,
				  struct timeval *t)
{
  if (!timerisset(&si->next_time) ||
      timercmp(t, &si->next_time, <))
    {
      /* No timeout set at all, or t is earlier. */
      si->next_time = *t;
    }
}

/********************************************************************/

int lwroc_select_info_time_passed_else_setup(lwroc_select_info *si,
					     struct timeval *t)
{
  if (timercmp(&si->now, t, <))
    {
      /* We have not reached time t yet... */
      lwroc_select_info_time_setup(si, t);
      return 0;
    }
  /* Have passed time t. */
  return 1;
}

/********************************************************************/

void lwroc_select_info_set_timeout_now(lwroc_select_info *si)
{
  si->next_time = si->now;
}

/********************************************************************/

void lwroc_select_loop(pd_ll_item *items, volatile int *quit,
		       lwroc_thread_block *thread_block,
		       lwroc_select_loop_callback setup_service,
		       void *setup_service_arg,
		       lwroc_select_loop_callback before_service,
		       void *before_service_arg,
		       lwroc_select_loop_callback after_service,
		       void *after_service_arg,
		       int after_loop_cleanup)
{
  pd_ll_item *iter, *tmp_iter;

  lwroc_select_info si;

  gettimeofday(&si.now,NULL);

  while (!*quit)
    {
      struct timeval *time_left;
      int ret;

#if STATE_MACHINE_DEBUG
      printf ("-- setup --\n");
#endif

      lwroc_select_info_clear(&si);

      if (thread_block)
	lwroc_thread_block_setup_select(thread_block, &si);

      /* Add all connections to the respective sets. */

      PD_LL_FOREACH(*items, iter)
	{
	  lwroc_select_item *item =
	    PD_LL_ITEM(iter, lwroc_select_item, _items);

	  if (item->item_info->setup_select)
	    item->item_info->setup_select(item, &si);
	}

      if (setup_service)
	setup_service(setup_service_arg, &si);

      /* Do we need to set up a timeout? */

      time_left = lwroc_select_info_time_left(&si);

      /* Then wait for someone needing servicing. */

#if STATE_MACHINE_DEBUG
      printf ("nfd: %d\n", si.nfd);
#endif

      ret = select(si.nfd+1,&si.readfds,&si.writefds,NULL,time_left);

      if (ret == -1)
	{
	  if (errno == EINTR)
	    continue;

	  LWROC_PERROR("select");
	  LWROC_FATAL("Unexpected error in I/O multiplexer.");
	}

      gettimeofday(&si.now,NULL);

#if STATE_MACHINE_DEBUG
      printf ("ret: -> %d\n", ret);
      printf ("-- after --\n");
#endif

      if (thread_block)
	lwroc_thread_block_has_token_eat(thread_block, &si);

      /* Some servicing needed? */

      if (before_service)
	before_service(before_service_arg, &si);

      PD_LL_FOREACH_TMP(*items, iter, tmp_iter)
	{
	  lwroc_select_item *item =
	    PD_LL_ITEM(iter, lwroc_select_item, _items);
	  int ok = 1;

	  if (item->item_info->after_select)
	    ok = item->item_info->after_select(item, &si);

	  /* if @ok, we may not touch @iter or @item again, as
	   * lwroc_net_trigbus / lwroc_net_become_slave will have done
	   * surgery on the list and replaced us.
	   */

	  if (!ok)
	    {
#if STATE_MACHINE_DEBUG
	      printf (" -> fail\n");
#endif

	      if (item->item_info->after_fail)
		{
		  int keep;

		  keep = item->item_info->after_fail(item, &si);

		  if (!keep)
		    {
		      PD_LL_REMOVE(iter);

		      if (!item->item_info->delete_item)
			{
			  if (!(item->item_info->flags &
				LWROC_SELECT_ITEM_FLAG_NO_DELETE_INTENTIONAL))
			    LWROC_BUG("lwroc_select_item without "
				      "delete callback to be deleted.");
			}
		      else
			item->item_info->delete_item(item);
		    }
		}
	    }
	}

      if (after_service)
	after_service(after_service_arg, &si);

    }

  if (after_loop_cleanup)
    {
      /* We are about to close.  Try to clean the file descriptors up.
       *
       * Some fail routines move the item to another thread and modify the
       * list while doing that.
       */

      PD_LL_FOREACH_TMP(*items, iter, tmp_iter)
	{
	  lwroc_select_item *item =
	    PD_LL_ITEM(iter, lwroc_select_item, _items);

	  if (item->item_info->after_fail)
	    /*keep =*/ item->item_info->after_fail(item, &si);
	}
    }
}

/********************************************************************/

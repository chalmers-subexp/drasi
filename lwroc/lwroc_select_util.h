/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_SELECT_UTIL_H__
#define __LWROC_SELECT_UTIL_H__

#include "pd_linked_list.h"

#include "../dtc_arch/acc_def/time_include.h"

#include <unistd.h>
#include "../dtc_arch/acc_def/mystdint.h"
#include "../dtc_arch/acc_def/select_include.h"

/********************************************************************/

typedef struct lwroc_select_info_t
{
  fd_set readfds;
  fd_set writefds;
  int    nfd;

  struct timeval next_time;
  struct timeval now; /* time before select, or when select returned. */
} lwroc_select_info;

/********************************************************************/

void lwroc_select_info_clear(lwroc_select_info *si);

/********************************************************************/

#define LWROC_READ_FD_SET(fd, si) do { \
    FD_SET((fd),&(si)->readfds);       \
    if ((fd) > (si)->nfd)	       \
      (si)->nfd = (fd);		       \
  } while (0)

#define LWROC_WRITE_FD_SET(fd, si) do { \
    FD_SET((fd),&(si)->writefds);	\
    if ((fd) > (si)->nfd)		\
      (si)->nfd = (fd);			\
  } while (0)

/********************************************************************/

void lwroc_select_info_time_setup(lwroc_select_info *si,
				  struct timeval *t);

int lwroc_select_info_time_passed_else_setup(lwroc_select_info *si,
					     struct timeval *t);

void lwroc_select_info_set_timeout_now(lwroc_select_info *si);

/********************************************************************/

struct timeval *lwroc_select_info_time_left(lwroc_select_info *si);

#define LWROC_READ_FD_ISSET(fd, si)   FD_ISSET((fd),&(si)->readfds)
#define LWROC_WRITE_FD_ISSET(fd, si)  FD_ISSET((fd),&(si)->writefds)

/********************************************************************/

struct lwroc_select_item_t;
typedef struct lwroc_select_item_t lwroc_select_item;

/* Mark file descriptors item wants to listen to. */
typedef void (*lwroc_select_item_setup_select)(lwroc_select_item *item,
					       lwroc_select_info *si);
/* Handle file descriptors that are ready.  Return 0 on failure. */
typedef int (*lwroc_select_item_after_select)(lwroc_select_item *item,
					      lwroc_select_info *si);
/* Called after failure.  0 indicates that item shall be removed. */
typedef int (*lwroc_select_item_after_fail)(lwroc_select_item *item,
					    lwroc_select_info *si);
/* Called for items that requested removal. */
typedef void (*lwroc_select_item_delete)(lwroc_select_item *item);

/* Called after an outgoing item has been connected. */
typedef void (*lwroc_select_item_connected)(lwroc_select_item *item,
					    lwroc_select_info *si);

/********************************************************************/

#define LWROC_SELECT_ITEM_FLAG_OUTGOING_NET_THREAD    0x0002
#define LWROC_SELECT_ITEM_FLAG_NO_DELETE_INTENTIONAL  0x0004

typedef struct lwroc_select_item_info_t
{
  lwroc_select_item_setup_select setup_select;
  lwroc_select_item_after_select after_select;

  lwroc_select_item_after_fail   after_fail;

  lwroc_select_item_delete       delete_item;

  int   flags;

} lwroc_select_item_info;

typedef struct lwroc_outgoing_select_item_info_t
{
  lwroc_select_item_info base;

  lwroc_select_item_connected    connected;
  uint32_t request;

  const char *rev_link_missing;

} lwroc_outgoing_select_item_info;

/********************************************************************/

struct lwroc_select_item_t
{
  pd_ll_item _items;

  lwroc_select_item_info *item_info;
};

/********************************************************************/

struct lwroc_thread_block_t;
/* See lwroc_thread_block.h */
#define LWROC_TYPEDEF_LWROC_THREAD_BLOCK
typedef struct lwroc_thread_block_t lwroc_thread_block;

typedef void (*lwroc_select_loop_callback)(void *arg, lwroc_select_info *si);

/********************************************************************/

void lwroc_select_loop(pd_ll_item *items, volatile int *quit,
		       lwroc_thread_block *thread_block,
		       lwroc_select_loop_callback setup_service,
		       void *setup_service_arg,
		       lwroc_select_loop_callback before_service,
		       void *before_service_arg,
		       lwroc_select_loop_callback after_service,
		       void *after_service_arg,
		       int after_loop_cleanup);

/********************************************************************/

#endif/*__LWROC_SELECT_UTIL_H__*/

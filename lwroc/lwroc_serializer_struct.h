/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_SERIALIZER_STRUCT_H__
#define __LWROC_SERIALIZER_STRUCT_H__

#include "lwroc_format_prefix.h"

/********************************************************************/

typedef struct lwroc_deserialize_error_t
{
  const char *_msg;
  uint32_t    _val1, _val2;
} lwroc_deserialize_error;

/********************************************************************/

#define LWROC_SERSIZE_EXTRA_ITEM_char_ptr(name)  size_t sz_##name

/********************************************************************/

#define LWROC_PRINT_DIFF_CONFIG_USE_PREFIX  0x0001

typedef struct lwroc_print_diff_config_t
{
  int     _flags;
} lwroc_print_diff_config;

/********************************************************************/

#define LWROC_PRINT_DIFF_ITEM_INFO_char_ptr(name)
#define LWROC_PRINT_DIFF_ITEM_INFO_char_array(name)
#define LWROC_PRINT_DIFF_ITEM_INFO_uint16_t(name)
#define LWROC_PRINT_DIFF_ITEM_INFO_uint32_t(name)
#define LWROC_PRINT_DIFF_ITEM_INFO_uint32_t_array(name)
#define LWROC_PRINT_DIFF_ITEM_INFO_uint64_t(name) \
  lwroc_format_diff_info name##_val_info, name##_diff_info
#define LWROC_PRINT_NODIFF_ITEM_INFO_uint64_t(name) \
  LWROC_PRINT_DIFF_ITEM_INFO_uint64_t(name)
#define LWROC_PRINT_FILLFRAC_ITEM_INFO_uint64_t(name) \
  lwroc_format_diff_info name##_val_info
#define LWROC_PRINT_DIFF_ITEM_INFO_lwroc_item_time(name)
#define LWROC_PRINT_NOFREQ_ITEM_INFO_lwroc_item_time(name)

/********************************************************************/

#endif/*__LWROC_SERIALIZER_STRUCT_H__*/

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

/* Must be early, or stdio may miss vs(n)printf. */
#include <stdarg.h>

#include "lwroc_net_proto.h"
#include "lwroc_serializer_util.h"
#include "lwroc_message.h"

#include "../dtc_arch/acc_def/has_curses.h"
#if HAS_CURSES
#define USE_CURSES 1
#endif
#include "../lu_common/colourtext.h"
#include "../lu_common/colourtext.c"

#include <ctype.h>
#include <stdio.h>
#include "../dtc_arch/acc_def/time_include.h"
#include "../dtc_arch/acc_def/myinttypes.h"
#include "../dtc_arch/acc_def/has_snprintf.h"

#if !HAS_SNPRINTF
int snprintf(char *str, size_t size, const char *fmt, ...)
{
  va_list ap;
  int ret;

  /* snprintf does not exist.  assume also vsnprintf is missing.  do
   * the formatting by vsprintf, and check that it did not overrun the
   * buffer.
   */

  va_start(ap, fmt);
  ret = vsprintf(str, fmt, ap);
  va_end(ap);

  str[size-1] = 0;
  if (strlen(str) >= size-1)
    {
      /* Use a debugger to figure out where the call came from! */
      LWROC_BUG("Internal trouble - buffer overflow due to lack "
		"of snprintf on platform.");
    }

  return ret;
}
#endif

void lwroc_print_diff_item_lwroc_item_time(const char *name,
					   lwroc_item_time *this_t,
					   lwroc_item_time *prev_t,
					   double *frequency)
{
  time_t mt;
  struct tm *mt_tm;
  char mt_date[64];
  char mt_tz[64];
  uint32_t mt_ns;
  double elapsed;
  mt = (time_t) this_t->_sec;
  mt_tm = localtime(&mt);
  strftime(mt_date,sizeof(mt_date),"%Y-%m-%d %H:%M:%S",mt_tm);
  strftime(mt_tz,sizeof(mt_tz),"%z %Z",mt_tm);
  mt_ns = this_t->_nsec;
  if (frequency)
    {
      elapsed = (double) (this_t->_sec - prev_t->_sec) +
	0.000000001 * ((int) this_t->_nsec - (int) prev_t->_nsec);
      if (this_t->_sec == 0 || prev_t->_sec == 0)
	*frequency = 0;
      else
	*frequency = 1. / elapsed;
    }
  if (name)
    printf ("%-25s:", name);
  else
    printf ("Time ....................:");
  printf (" %s.%03d\n",
	  mt_date, mt_ns / 1000000);
}

void lwroc_print_diff_item_uint64_t(const char *name,
				    uint64_t val, uint64_t prev,
				    lwroc_format_diff_info *val_info,
				    lwroc_format_diff_info *diff_info,
				    lwroc_print_diff_config *config,
				    double *frequency)
{
  char str_val[64];
  char str_diff[64];

  double diff;

  if (config->_flags & LWROC_PRINT_DIFF_CONFIG_USE_PREFIX)
    lwroc_format_prefix(str_val,  sizeof (str_val),
			(double) val, 8, val_info);
  else
    snprintf (str_val,  sizeof (str_val),  "%" PRIu64 "", val);

  if (frequency)
    {
      diff = *frequency * (double) (val - prev);

      if (*frequency == 0.0)
	snprintf (str_diff, sizeof (str_diff), "-");
      else if (config->_flags & LWROC_PRINT_DIFF_CONFIG_USE_PREFIX)
	lwroc_format_prefix(str_diff, sizeof (str_diff),
			    diff, 8, diff_info);
      else
	snprintf (str_diff, sizeof (str_diff), "%.1f",        diff);
    }

  printf ("%-25s: %s%-20s%s", name,
	  CT_OUT(BOLD), str_val,  CT_OUT(NORM));
  if (frequency)
    printf (" %s%s%s",
	    CT_OUT(BOLD), str_diff, CT_OUT(NORM));
  printf ("\n");

  (void) val_info;
  (void) diff_info;
}

void lwroc_uint32_to_hollerith(char *dest, uint32_t hollerith)
{
  int i;

  for (i = 0; i < 4; i++)
    {
      dest[i] = (char) ((hollerith >> ((3-i)*8)) & 0xff);
      if (!isprint((unsigned char) dest[i]))
	dest[i] = '-';
    }
  dest[4] = 0;
}

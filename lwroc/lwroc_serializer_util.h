/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_SERIALIZER_UTIL_H__
#define __LWROC_SERIALIZER_UTIL_H__

#include "lwroc_serializer_struct.h"

#include "../dtc_arch/acc_def/myinttypes.h"

#include <stdlib.h>

/**********************************************************************/

#define LWROC_DESERIALIZE_ERROR(msg,v1,v2)	\
  do { error->_msg = msg;			\
    error->_val1 = v1;				\
    error->_val2 = v2;				\
    return NULL;				\
  } while (0)

/**********************************************************************/

/* size + magic + checksum (at the end) */
#define LWROC_SERBASICSIZE_HEADER				\
  sizeof (uint32_t) + sizeof (uint32_t) + sizeof (uint32_t)

#define LWROC_SERBASICSIZE_ITEM_char_ptr(name)  sizeof (uint32_t)
#define LWROC_SERBASICSIZE_ITEM_char_array(name,len)  (len)*sizeof (char)
#define LWROC_SERBASICSIZE_ITEM_uint16_t(name)  sizeof (uint32_t)
#define LWROC_SERBASICSIZE_ITEM_uint32_t(name)  sizeof (uint32_t)
#define LWROC_SERBASICSIZE_ITEM_uint32_t_array(name,len) (len)*sizeof (uint32_t)
#define LWROC_SERBASICSIZE_ITEM_uint64_t(name)  2 * sizeof (uint32_t)
#define LWROC_SERBASICSIZE_ITEM_lwroc_item_time(name)  3 * sizeof (uint32_t)

/**********************************************************************/

#define LWROC_CALC_SERSIZE_EXTRA_ITEM_char_ptr(sz,s,name)	\
  sz->sz_##name = strlen((s)->name);

#define LWROC_ADD_SERSIZE_EXTRA_ITEM_char_ptr(sz,name)	\
  (sz)->sz_##name + 1

#define LWROC_SERSIZE_ALIGN(x,y,alignment)		\
  x = ((y) + (alignment) - 1) & ~((alignment) - 1);

/**********************************************************************/

#define LWROC_SERIALIZE_SIZE(d32,chk,size)	\
  { chk = (uint32_t) size;			\
    *(d32++) = htonl((uint32_t) size); }
#define LWROC_SERIALIZE_MAGIC(d32,magic)	\
  *(d32++) = htonl(magic);
#define LWROC_SERIALIZE_CHKSUM(d32,chk,magic)	\
  *(d32++) = htonl(magic - chk);

/**********************************************************************/

#define LWROC_SERIALIZE_BLOCK_CALC_CHECKSUM(start,end,chk)	\
  { const uint32_t *p = (const uint32_t *) start;		\
    for (p = (const uint32_t *) start;				\
	 p < (const uint32_t *) end; p++)			\
      chk += ntohl(*p);						\
  }

#define LWROC_SERIALIZE_COPY_BLOCK_CHECKSUM(d32,s32,n,chk)	\
  { size_t i;							\
    for ( i = n; i; i--) {					\
      uint32_t v = *(s32++);					\
      *(d32++) = ntohl(v);					\
      chk += v;							\
    }								\
  }

#define LWROC_DESERIALIZE_COPY_BLOCK_CHECKSUM(d32,s32,n,chk)	\
  { size_t i;							\
    for (i = n; i; i--) {					\
      chk += *(d32++) = htonl(*(s32++));			\
    }								\
  }

/**********************************************************************/

#define LWROC_SERIALIZE_ITEM_uint32_t_internal(d32,chk,value) \
  { uint32_t v = (value);				      \
    chk += v;						      \
    *(d32++) = htonl(v); }
#define LWROC_SERIALIZE_ITEM_char_ptr(d32,chk,s,sz,name)		\
  LWROC_SERIALIZE_ITEM_uint32_t_internal(d32,chk,(uint32_t) sz->sz_##name);
#define LWROC_SERIALIZE_ITEM_char_array(d32,chk,s,sz,name)	\
  { char *d8 = (char *) d32;					\
    char *start = d8;						\
    memcpy (d8, s->name, sizeof (s->name));			\
    d8 += sizeof (s->name);					\
    d32 = (uint32_t *) d8;					\
    LWROC_SERIALIZE_BLOCK_CALC_CHECKSUM(start,d8,chk);		\
  }
#define LWROC_SERIALIZE_ITEM_uint16_t(d32,chk,s,sz,name)	\
  LWROC_SERIALIZE_ITEM_uint32_t_internal(d32,chk,s->name);
#define LWROC_SERIALIZE_ITEM_uint32_t(d32,chk,s,sz,name)	\
  LWROC_SERIALIZE_ITEM_uint32_t_internal(d32,chk,s->name);
#define LWROC_SERIALIZE_ITEM_uint32_t_array(d32,chk,s,sz,name)		\
  { const uint32_t *s32 = s->name;					\
    LWROC_SERIALIZE_COPY_BLOCK_CHECKSUM(d32,s32,			\
					(sizeof (s->name) /		\
					 sizeof (s->name[0])),chk);	\
  }
#define LWROC_SERIALIZE_ITEM_uint64_t_internal(d32,chk,value)		\
  LWROC_SERIALIZE_ITEM_uint32_t_internal(d32,chk,(uint32_t) ((value) >> 32)); \
  LWROC_SERIALIZE_ITEM_uint32_t_internal(d32,chk,(uint32_t) (value));
#define LWROC_SERIALIZE_ITEM_uint64_t(d32,chk,s,sz,name)	\
  LWROC_SERIALIZE_ITEM_uint64_t_internal(d32,chk,s->name)
#define LWROC_SERIALIZE_ITEM_lwroc_item_time(d32,chk,s,sz,name)	\
  LWROC_SERIALIZE_ITEM_uint64_t_internal(d32,chk,s->name._sec);	\
  LWROC_SERIALIZE_ITEM_uint32_t_internal(d32,chk,s->name._nsec);

/**********************************************************************/

#define LWROC_SERIALIZE_EXTRA_ITEM_char_ptr(d8,s,sz,name)	\
  { memcpy (d8, s->name, sz->sz_##name);			\
    d8[sz->sz_##name] = 0; d8 += sz->sz_##name + 1; }

/**********************************************************************/

#define LWROC_DESERIALIZE_CHECK_MIN_FIXED_SIZE(src_size,fixed_size)	\
  if (src_size < fixed_size)						\
    LWROC_DESERIALIZE_ERROR("src size too small for fixed size",	\
			    (uint32_t) src_size,(uint32_t) fixed_size);

#define LWROC_DESERIALIZE_SIZE(dsize,chk,s32)	\
  dsize = chk = ntohl(*(s32++));

#define LWROC_DESERIALIZE_CHECK_MIN_SIZE(size,fixed_size)		\
  if (size < fixed_size)						\
    LWROC_DESERIALIZE_ERROR("src size too small for actual size",	\
			    (uint32_t) size,(uint32_t) fixed_size);

#define LWROC_DESERIALIZE_CHECK_EXACT_SIZE(size,fixed_size)		\
  if (size != fixed_size)						\
    LWROC_DESERIALIZE_ERROR("wrong fixed size",	\
			    (uint32_t) size,(uint32_t) fixed_size);

#define LWROC_DESERIALIZE_CHECK_CALC_SIZE(size,calc_size)		\
  if (size < calc_size)							\
    LWROC_DESERIALIZE_ERROR("size smaller than calculated",		\
			    (uint32_t) size,(uint32_t) calc_size);

#define LWROC_DESERIALIZE_CHECK_MAGIC(s32,magic)		\
  { uint32_t src_magic = ntohl(*(s32++));			\
    if (src_magic != magic)					\
      LWROC_DESERIALIZE_ERROR("bad magic", src_magic, magic);	\
  }

#define LWROC_DESERIALIZE_CHECK_CHKSUM(s32,chk,magic)			\
  { uint32_t src_chksum = ntohl(*(s32++));				\
    uint32_t expect_chk = magic - chk;					\
    if (src_chksum != expect_chk)					\
      LWROC_DESERIALIZE_ERROR("bad checksum", src_chksum, expect_chk);	\
  }

/**********************************************************************/

#define LWROC_DESERIALIZE_ITEM_uint32_t_internal(dest,s32,chk)	\
  { uint32_t v = ntohl(*(s32++));				\
    chk += v;							\
    dest = v; }

#define LWROC_DESERIALIZE_ITEM_char_ptr(d,s32,chk,sz,name)		\
  LWROC_DESERIALIZE_ITEM_uint32_t_internal((sz)->sz_##name,s32,chk)
#define LWROC_DESERIALIZE_ITEM_char_array(d,s32,chk,sz,name)	\
  { const char *s8 = (const char *) s32;			\
    const char *start = s8;					\
    memcpy (d->name, s8, sizeof (d->name));			\
    d->name[sizeof (d->name) - 1] = 0; /* terminate */		\
    s8 += sizeof (d->name);					\
    s32 = (const uint32_t *) s8;				\
    LWROC_SERIALIZE_BLOCK_CALC_CHECKSUM(start,s8,chk);		\
  }
#define LWROC_DESERIALIZE_ITEM_uint16_t(d,s32,chk,sz,name)		\
  { uint32_t vv;							\
    LWROC_DESERIALIZE_ITEM_uint32_t_internal(vv,s32,chk);		\
    if (vv & 0xffff0000)						\
      LWROC_DESERIALIZE_ERROR("Unexpected bits in 16-bit item.",vv,0);	\
    d->name = (uint16_t) vv; }
#define LWROC_DESERIALIZE_ITEM_uint32_t(d,s32,chk,sz,name)		\
  LWROC_DESERIALIZE_ITEM_uint32_t_internal(d->name,s32,chk)
#define LWROC_DESERIALIZE_ITEM_uint32_t_array(d,s32,chk,sz,name)	\
  { uint32_t *d32 = d->name;						\
    LWROC_DESERIALIZE_COPY_BLOCK_CHECKSUM(d32,s32,			\
					  (sizeof (d->name) /		\
					   sizeof (d->name[0])),chk);	\
  }
#define LWROC_DESERIALIZE_ITEM_uint64_t_internal(dest,s32,chk)	\
  { uint64_t hi, lo;						\
    LWROC_DESERIALIZE_ITEM_uint32_t_internal(hi,s32,chk);	\
    LWROC_DESERIALIZE_ITEM_uint32_t_internal(lo,s32,chk);	\
    dest = lo | (hi << 32); }
#define LWROC_DESERIALIZE_ITEM_uint64_t(d,s32,chk,sz,name)	\
  LWROC_DESERIALIZE_ITEM_uint64_t_internal(d->name, s32, chk)
#define LWROC_DESERIALIZE_ITEM_lwroc_item_time(d,s32,chk,sz,name)	\
  LWROC_DESERIALIZE_ITEM_uint64_t_internal(d->name._sec, s32, chk);	\
  LWROC_DESERIALIZE_ITEM_uint32_t_internal(d->name._nsec, s32, chk)

/**********************************************************************/

#define LWROC_DESERIALIZE_EXTRA_CHECK_ITEM_char_ptr(s8,sz,name)	\
  { if (s8[(sz)->sz_##name] != 0)					\
      LWROC_DESERIALIZE_ERROR("string '" #name "' not properly terminated", \
			      0,0);					\
  }

#define LWROC_DESERIALIZE_EXTRA_ITEM_char_ptr_inplace(d,s8,sz,name)	\
  { LWROC_DESERIALIZE_EXTRA_CHECK_ITEM_char_ptr(s8,sz,name);		\
    d->name = (const char *) s8; /* In place ptr, user shall not free. */ \
    s8 += (sz)->sz_##name + 1;						\
  }
#define LWROC_DESERIALIZE_EXTRA_ITEM_char_ptr_strdup(d,s8,sz,name)	\
  { LWROC_DESERIALIZE_EXTRA_CHECK_ITEM_char_ptr(s8,sz,name);		\
    /* In case the item is a const char * we need to cast. */		\
    /* In principle item should not be const when we can free it here,	\
     * but the const works as indicator in the writing side.		\
     * Take care. */							\
    free (LWROC_UNCONST(char *, d->name)); /* Free previous item. */	\
    d->name = strdup(s8);						\
    if (d->name == NULL)						\
      LWROC_DESERIALIZE_ERROR("failed to allocate (strdup) string '"	\
			      #name "'",				\
			      (uint32_t) (sz)->sz_##name, 0);		\
    s8 += (sz)->sz_##name + 1;						\
  }

/**********************************************************************/

#define LWROC_PRINT_DIFF_ITEM_char_ptr(this,prev,name)	\
  printf ("%-25s: %s%s%s\n", #name,			\
	  CT_OUT(BOLD), this->name, CT_OUT(NORM));
#define LWROC_PRINT_DIFF_ITEM_char_array(this,prev,name)	\
  printf ("%-25s: %s%s%s\n", #name,				\
	  CT_OUT(BOLD), this->name, CT_OUT(NORM));
#define LWROC_PRINT_DIFF_ITEM_uint16_t(this,prev,name)	\
  printf ("%-25s: %s%" PRIu16 "%s\n", #name,		\
	  CT_OUT(BOLD), this->name, CT_OUT(NORM));
#define LWROC_PRINT_DIFF_ITEM_uint32_t(this,prev,name)	\
  printf ("%-25s: %s%" PRIu32 "%s\n", #name,		\
	  CT_OUT(BOLD), this->name, CT_OUT(NORM));
#define LWROC_PRINT_DIFF_ITEM_uint32_t_array(this,prev,name)	\
  printf ("%-25s: %s%" PRIu32 "%s...\n", #name,			\
	  CT_OUT(BOLD), this->name[0], CT_OUT(NORM));
#define LWROC_PRINT_DIFF_ITEM_uint64_t(this,prev,name)			\
  lwroc_print_diff_item_uint64_t(#name, this->name, prev->name,		\
				 &info->name##_val_info,		\
				 &info->name##_diff_info,		\
				 config, &frequency);
#define LWROC_PRINT_NODIFF_ITEM_uint64_t(this,prev,name)		\
  lwroc_print_diff_item_uint64_t(#name, this->name, prev->name,		\
				 &info->name##_val_info,		\
				 &info->name##_diff_info,		\
				 config, NULL);
#define LWROC_PRINT_FILLFRAC_ITEM_uint64_t(this,prev,staticinfo,	\
					   name,staticname)		\
  printf ("%-25s: %s%-20" PRIu64 "%s %s%.1f %%%s\n", #name,		\
	  CT_OUT(BOLD), this->name, CT_OUT(NORM),			\
	  CT_OUT(BOLD), 100.0 * (double) this->name /			\
	  (double) (staticinfo->staticname), CT_OUT(NORM));
#define LWROC_PRINT_DIFF_ITEM_lwroc_item_time(this,prev,name)		\
  {									\
    lwroc_print_diff_item_lwroc_item_time(NULL,				\
					  &this->name, &prev->name,	\
					  &frequency);			\
  }
#define LWROC_PRINT_NOFREQ_ITEM_lwroc_item_time(this,prev,name)		\
  {									\
    lwroc_print_diff_item_lwroc_item_time(#name,			\
					  &this->name, &prev->name,	\
					  NULL);			\
  }

/**********************************************************************/

void lwroc_print_diff_item_uint64_t(const char *name,
				    uint64_t val, uint64_t prev,
				    lwroc_format_diff_info *val_info,
				    lwroc_format_diff_info *diff_info,
				    lwroc_print_diff_config *config,
				    double *frequency);

void lwroc_print_diff_item_lwroc_item_time(const char *name,
					   lwroc_item_time *this_t,
					   lwroc_item_time *prev_t,
					   double *frequency);

/**********************************************************************/

void lwroc_uint32_to_hollerith(char *dest, uint32_t hollerith);

/**********************************************************************/

#endif/*__LWROC_SERIALIZER_UTIL_H__*/

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_net_proto.h"
#include "lwroc_macros.h"

#include "../dtc_arch/acc_def/has_curses.h"
#if HAS_CURSES
#define USE_CURSES 1
#endif
#include "../lu_common/colourtext.h"

#include <stdio.h>
#include "../dtc_arch/acc_def/myinttypes.h"
#include "../dtc_arch/acc_def/netinet_in_include.h"

#include "gen/lwroc_portmap_msg_serializer.c"

#include "gen/lwroc_request_msg_serializer.c"
#include "gen/lwroc_badrequest_serializer.c"
#include "gen/lwroc_reverse_link_ack_serializer.c"

#include "gen/lwroc_data_transport_setup_serializer.c"

#include "gen/lwroc_control_token_serializer.c"
#include "gen/lwroc_control_request_serializer.c"
#include "gen/lwroc_control_response_serializer.c"
#include "gen/lwroc_file_request_serializer.c"
#include "gen/lwroc_acqr_request_serializer.c"
#include "gen/lwroc_merge_request_serializer.c"

#include "gen/lwroc_message_source_serializer.c"
#include "gen/lwroc_message_item_serializer.c"
#include "gen/lwroc_message_keepalive_serializer.c"
#include "gen/lwroc_message_item_ack_serializer.c"

#include "gen/lwroc_monitor_source_header_serializer.c"
#include "gen/lwroc_monitor_main_source_serializer.c"
#include "gen/lwroc_monitor_net_source_serializer.c"
#include "gen/lwroc_monitor_file_source_serializer.c"
#include "gen/lwroc_monitor_filewr_source_serializer.c"
#include "gen/lwroc_monitor_filter_source_serializer.c"
#include "gen/lwroc_monitor_serv_source_serializer.c"
#include "gen/lwroc_monitor_in_source_serializer.c"
#include "gen/lwroc_monitor_conn_source_serializer.c"
#include "gen/lwroc_monitor_analyse_source_serializer.c"
#include "gen/lwroc_monitor_ana_ts_source_serializer.c"
#include "gen/lwroc_monitor_dams_source_serializer.c"
#include "gen/lwroc_monitor_dam_source_serializer.c"

#include "gen/lwroc_monitor_block_header_serializer.c"
#include "gen/lwroc_monitor_main_block_serializer.c"
#include "gen/lwroc_monitor_net_block_serializer.c"
#include "gen/lwroc_monitor_file_block_serializer.c"
#include "gen/lwroc_monitor_filewr_block_serializer.c"
#include "gen/lwroc_monitor_filter_block_serializer.c"
#include "gen/lwroc_monitor_serv_block_serializer.c"
#include "gen/lwroc_monitor_in_block_serializer.c"
#include "gen/lwroc_monitor_conn_block_serializer.c"
#include "gen/lwroc_monitor_analyse_block_serializer.c"
#include "gen/lwroc_monitor_ana_ts_block_serializer.c"
#include "gen/lwroc_monitor_dams_block_serializer.c"
#include "gen/lwroc_monitor_dam_block_serializer.c"

#include "gen/lwroc_monitor_dt_trace_serializer.c"

#include "gen/lwroc_trigbus_msg_serializer.c"

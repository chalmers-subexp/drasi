/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2023  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_slow_async_loop.h"
#include "lwroc_thread_block.h"
#include "lwroc_message_internal.h"
#include "lwroc_udp_awaken_hints.h"

#include <string.h>
#include <assert.h>

#include "../dtc_arch/acc_def/myinttypes.h"

/********************************************************************/

/* This thread is for doing things which could lock up, like writes to
 * files on network file systems.
 *
 * We want the other threads to continue to function also in this case
 * and therefore relegate such work to this thread.  Typically, such
 * tasks would be for the network thread.  In particular we do not want
 * the network thread to not be able to send error reports about the
 * situation.
 */

void lwroc_slow_async_thread_loop(lwroc_thread_instance *inst)
{
  (void) inst;

  for ( ; !inst->_terminate; )
    {
      if (_lwroc_udp_awaken_hints_write)
	lwroc_udp_awaken_hints_write();

      lwroc_thread_block_get_token(_lwroc_slow_async_thread->_block);
    }
}

/********************************************************************/

lwroc_thread_info _lwroc_slow_async_thread_info =
{
  NULL,
  lwroc_slow_async_thread_loop,
  LWROC_MSG_BUFS_SLOW_ASYNC,
  "slow_async",
  LWROC_THREAD_NO_MSG_CLIENT_WAIT,
  LWROC_THREAD_TERM_SLOW_ASYNC,
  LWROC_THREAD_CORE_PRIO_OTHER,
};

/********************************************************************/

lwroc_thread_instance *_lwroc_slow_async_thread = NULL;

void lwroc_prepare_slow_async_thread(void)
{
  _lwroc_slow_async_thread =
    lwroc_thread_prepare(&_lwroc_slow_async_thread_info);
}

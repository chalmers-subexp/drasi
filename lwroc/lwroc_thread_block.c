/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_thread_block.h"
#include "lwroc_optimise.h"

#include "lwroc_message.h"

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/socket.h>

struct lwroc_thread_block_t
{
  int _fd_wakeup[2];
  lwroc_thread_block_after_token _call_after_token;
};

lwroc_thread_block *lwroc_thread_block_init(void)
{
  lwroc_thread_block *block;

  block = (lwroc_thread_block *) malloc (sizeof (lwroc_thread_block));

  if (!block)
    {
      LWROC_FATAL("Failure allocation thread block memory.");
    }

  if (pipe(block->_fd_wakeup) != 0)
    {
      LWROC_PERROR("pipe");
      LWROC_FATAL("Failed to create notification pipe.");
    }

  block->_call_after_token = NULL;

  return block;
}

void lwroc_thread_block_set_after_token(lwroc_thread_block *block,
					lwroc_thread_block_after_token call)
{
  block->_call_after_token = call;
}

void lwroc_thread_block_setup_select(const lwroc_thread_block *block,
				     lwroc_select_info *si)
{
  LWROC_READ_FD_SET(block->_fd_wakeup[0], si);
}

int lwroc_thread_block_has_token_eat(const lwroc_thread_block *block,
				     lwroc_select_info *si)
{
  if (!LWROC_READ_FD_ISSET(block->_fd_wakeup[0], si))
    return 0;

  lwroc_thread_block_get_token(block);

  return 1;
}

void lwroc_thread_block_get_token(const lwroc_thread_block *block)
{
  if (block->_call_after_token)
    block->_call_after_token(block, 0);

  for ( ; ; )
    {
      char token;

      ssize_t n = read(block->_fd_wakeup[0], &token, sizeof(token));

      if (n >= 1)
	{
	  if (block->_call_after_token)
	    block->_call_after_token(block, 1);
	  return;
	}

      if (n == -1)
	{
	  if (errno == EINTR)
	    continue; /* try again */
	  /* We failed to read, for no good reason! */
	  LWROC_PERROR("read");
	}

      LWROC_ERROR("Failed to read from wakeup pipe.  strange");
      /* The error reports may not make it to the output.  Bypass... */
      fprintf (stderr,"Failed to write to wakeup pipe.  Strange...\n");
      exit(1);
    }
}

int lwroc_thread_block_has_token_timeout(const lwroc_thread_block *block,
					 struct timeval *timeout)
{
  lwroc_select_info si;
  int ret;

  lwroc_select_info_clear(&si);

  LWROC_READ_FD_SET(block->_fd_wakeup[0], &si);

  ret = select(si.nfd+1,&si.readfds,NULL,NULL,timeout);

  if (ret == -1)
    {
      if (errno == EINTR)
	return 0;

      LWROC_PERROR("select");
      LWROC_FATAL("Unexpected error checking/waiting for token.");
    }

  return !!(LWROC_READ_FD_ISSET(block->_fd_wakeup[0], &si));
}

int lwroc_thread_block_get_token_timeout(const lwroc_thread_block *block,
					 struct timeval *timeout)
{
  /* We will wait for the timeout once.  It is up to the caller to
   * ensure that the wanted time has passed before whatever additional
   * action is taken.
   */

  if (!lwroc_thread_block_has_token_timeout(block, timeout))
    return 0;

  lwroc_thread_block_get_token(block);

  return 1;
}

void lwroc_thread_block_send_token(const lwroc_thread_block *block)
{
  char token = 0;

  /* If there already exist a token, there is no need to write a
   * second one.
   *
   * Since several threads might write tokens, but the one waiting for
   * a token only will read one, tokens could potentially pile up in
   * the pipe, leading to block of the token sender.
   *
   * Trying to keep a separate variable that counts tokens does not
   * solve the underlying problem that the consumer does not read more
   * than one token each time it wakes up.
   *
   * Also doing reads of more than one token at a time would not solve
   * the problem if the consumer never gets the idea of consuming
   * tokens, e.g. due to being 100% CPU busy without waiting.
   *
   * Note: token consumers that wait for tokens with select() are not
   * problems, as they will concume a token per select cycle.
   */

  /* The test with select should be enough here.  If select returns
   * that there is a token, then there is one, at least at the time of
   * query.  If the token consumer eats it just the moment after, that
   * is fine!  The only purpose of send_token is that the consumer
   * should do just that!
   */

  struct timeval timeout;

  timeout.tv_sec = 0;
  timeout.tv_usec = 0;

  if (lwroc_thread_block_has_token_timeout(block, &timeout))
    {
      /* There is already a token. */
      return;
    }

  for ( ; ; )
    {
      ssize_t n = write(block->_fd_wakeup[1], &token, sizeof(token));

      if (n == sizeof(token))
	break;

      if (n == -1)
	{
	  if (errno == EINTR)
	    continue; /* try again */
	  /* We failed to write, for no good reason!
	   * EPIPE could happen, but we never close the reading end
	   * before the threads are done, so cannot happen!!
	   */
	  LWROC_PERROR("write");
	}
      /* The error reports may not make it to the output.  Bypass... */
      fprintf (stderr,"Failed to write to wakeup pipe.  DEADLOCK!\n");
      abort();
    }
}


/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_THREAD_BLOCK_H__
#define __LWROC_THREAD_BLOCK_H__

#include "lwroc_select_util.h"
#include "lwroc_optimise.h"

#include "../dtc_arch/acc_def/select_include.h"

#include <sys/types.h>
#include <unistd.h>

struct lwroc_thread_block_t;
/* To handle circular usage in lwroc_select_util.h */
#ifndef LWROC_TYPEDEF_LWROC_THREAD_BLOCK
typedef struct lwroc_thread_block_t lwroc_thread_block;
#endif

lwroc_thread_block *lwroc_thread_block_init(void);

typedef void (*lwroc_thread_block_after_token)(const lwroc_thread_block *block,
					       int after_token/*else setup*/);

void lwroc_thread_block_set_after_token(lwroc_thread_block *block,
					lwroc_thread_block_after_token call);

void lwroc_thread_block_get_token(const lwroc_thread_block *block);
int lwroc_thread_block_get_token_timeout(const lwroc_thread_block *block,
					 struct timeval *timeout);
void lwroc_thread_block_send_token(const lwroc_thread_block *block);

void lwroc_thread_block_setup_select(const lwroc_thread_block *block,
				     lwroc_select_info *si);

int lwroc_thread_block_has_token_eat(const lwroc_thread_block *block,
				     lwroc_select_info *si);

#define LWROC_THREAD_BLOCK_NOTIFY(notify_ptr) do {			\
    const lwroc_thread_block *volatile*__tmp_notify_ptr = (notify_ptr);	\
    const lwroc_thread_block *__thread_notify = *__tmp_notify_ptr;	\
    *__tmp_notify_ptr = NULL;						\
    SFENCE;								\
    lwroc_thread_block_send_token(__thread_notify);			\
  } while (0)

/* __tmr to not clash with __tmp above */
#define LWROC_THREAD_BLOCK_TEST_AND_NOTIFY(notify_ptr) do {		\
    const lwroc_thread_block *volatile*__tmr_notify_ptr = (notify_ptr);	\
    if (*__tmr_notify_ptr != NULL)  {					\
      LWROC_THREAD_BLOCK_NOTIFY(__tmr_notify_ptr);			\
    }									\
  } while (0)

#endif/*__LWROC_THREAD_BLOCK_H__*/

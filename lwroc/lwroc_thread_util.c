/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_thread_util.h"
#include "lwroc_message.h"
#include "lwroc_message_internal.h"
#include "lwroc_cpu_affinity.h"
#include "lwroc_main_iface.h"

#include <string.h>
#include <assert.h>
#include <signal.h>

PD_LL_SENTINEL(_lwroc_threads);

void lwroc_thread_set_terminate(lwroc_thread_instance *inst)
{
  /* We may be called multiple times from the signal handler.
   * only set if not already set.
   */
  /*
  printf ("set terminate %p (%p: %d) %s\n",
	  inst, &inst->_terminate, inst->_terminate,
	  inst->_info->_purpose);
  */
  if (inst->_terminate)
    return;

  inst->_terminate = 1;
  MFENCE;
  lwroc_thread_block_send_token(inst->_block);
  if (inst->_also_wakeup_on_term)
    lwroc_thread_block_send_token(inst->_also_wakeup_on_term->_block);
}

void lwroc_thread_set_terminate_first(void)
{
  pd_ll_item *next_iter = PD_LL_NEXT(&_lwroc_threads,&_lwroc_threads);
  lwroc_thread_instance *next;

  assert(next_iter);
  next = PD_LL_ITEM(next_iter, lwroc_thread_instance, _threads);

  LWROC_INFO_FMT("Set terminate first!  (%s)",
		 next->_info->_purpose);

  /* printf ("set terminate first...  %p\n", next->_also_wakeup_on_term); */
  lwroc_thread_set_terminate(next);
}

void lwroc_thread_done(lwroc_thread_instance *inst)
{
  /* When the thread is done, mark the next in the termination
   * sequence for termination.
   */

  pd_ll_item *next_iter = PD_LL_NEXT(&_lwroc_threads,&inst->_threads);
  lwroc_thread_instance *next = NULL;

  if (next_iter)
    next = PD_LL_ITEM(next_iter, lwroc_thread_instance, _threads);

  LWROC_INFO_FMT("%s thread done!  (Next term: %s)",
		 inst->_info->_purpose,
		 next ? next->_info->_purpose : "-");

  if (next)
    lwroc_thread_set_terminate(next);
}

void lwroc_thread_apply_cpu_set(lwroc_thread_instance *inst)
{
  if (inst->_cpu_set._setsize)
    {
#if LWROC_DEBUG_THREAD_AFFINITY
      usleep(100*(unsigned int) inst->_info->_term_order);
      printf ("thread level: %d  ", inst->_info->_core_prio);
#endif
      lwroc_cpu_set_apply_this_thread(&inst->_cpu_set);
#if LWROC_DEBUG_THREAD_AFFINITY
      fflush(stdout);
#endif
    }
}

void *lwroc_thread_generic_loop(void *arg)
{
  sigset_t set;
  lwroc_thread_instance *inst;

  sigemptyset(&set);
  sigaddset(&set, SIGINT);
  sigaddset(&set, SIGTERM);
  pthread_sigmask(SIG_BLOCK, &set, NULL);

  inst = (lwroc_thread_instance *) arg;

  lwroc_msg_thread_set_buf(inst->_info->_msg_buf_source,
			   inst->_block);

  LWROC_INFO_FMT("This is the %s thread!", inst->_info->_purpose);

  if (!(inst->_info->_flags & LWROC_THREAD_NO_MSG_CLIENT_WAIT))
    lwroc_msg_wait_any_msg_client(inst->_block);

  lwroc_thread_apply_cpu_set(inst);

  /* And do the actual work. */

  inst->_info->_loop(inst);

  lwroc_thread_done(inst);

  return NULL;
}

lwroc_thread_instance *
lwroc_thread_prepare(lwroc_thread_info *info)
{
  lwroc_thread_instance *inst;
  pd_ll_item *iter;

  inst = (lwroc_thread_instance *) malloc (sizeof (lwroc_thread_instance));

  if (!inst)
    LWROC_FATAL("Failure allocation thread instance memory.");

  memset(inst, 0, sizeof (*inst));

  inst->_info = info;
  inst->_block = lwroc_thread_block_init();
  inst->_terminate = 0;

  inst->_cpu_set._setsize = 0;
  inst->_cpu_set._mask = NULL;

  /****************************************************/

  if (inst->_info->_at_prepare)
    inst->_info->_at_prepare(inst);

  /****************************************************/

  if (inst->_info->_flags & LWROC_THREAD_MAIN)
    {
      inst->_thread = pthread_self();
      lwroc_msg_thread_set_buf(inst->_info->_msg_buf_source,
			       inst->_block);
    }
  else
    {
      /* The actual threads are created in lwroc_all_threads_create(),
       * after all thread structures have been set up.  This is
       * necessary since some threads (e.g. net io thread)
       * cross-references itself using the pointer returned by this
       * function.
       */
    }

  /****************************************************/

  /* Insert in the correct position according to termination order. */

  /* If none match, iter will end by pointing to the sentinel,
   * _lwroc_threads.
   */

  PD_LL_FOREACH(_lwroc_threads, iter)
    {
      lwroc_thread_instance *cmp =
	PD_LL_ITEM(iter, lwroc_thread_instance, _threads);

      if (cmp->_info->_term_order > info->_term_order)
	break;
    }

  PD_LL_ADD_BEFORE(iter, &inst->_threads);

  return inst;
}

void lwroc_all_threads_create(void)
{
  pd_ll_item *iter;
  int ret;

  /* Before we create all threads, figure out what cores to run them on
   * (if we have access to more than one core.
   */
  if (lwroc_num_cpus() > 1)
    {
      int level, num_remain, num_threads;
      lwroc_cpu_set remain, remain_before_level;
      size_t last_index;
      int assign_single = 0, assign_remain = 0;

      /* Rationale for this is that some threads need more CPU than
       * others.  Sometimes the linux scheduler does not manage to
       * organise things optimally for us.  In particular: it lets
       * threads that source and sink data through a pipe run on the
       * same core, which we do not want.
       *
       * Number one priority is that the readout thread should have a
       * core of its own.  It should preferably also run on a core
       * which is *not* dealing with network (or other) interrupts.
       * It seems that the latter are generally sent to core 0, so we
       * will assign cores from the other direction.
       *
       * Similarly, the merger thread is a critical part.  This should
       * if possible also have its own core.
       *
       * Following that we have the network server threads.
       *
       * Next in line are the network input threads.
       *
       * And finally remaining low-job threads, e.g. the network read.
       *
       * If there are more than four cores available, avoid core 0
       * altogether.
       */

      /* Strategy: work in order of priority.  For each level: if there
       * are remaining cores enough for all threads of that priority:
       * assign one thread per core.
       *
       * After assignment, if there is no core left, give all cores
       * that were available before this step as a full mask to all
       * remaining threads.
       *
       * If after assignment there is at least one core left, give the
       * remaining cores to the next level.
       *
       * For machines with more cores than threads to assign: allow the
       * use of several cores
       */

      lwroc_cpu_set_alloc(&remain_before_level);
      lwroc_cpu_set_alloc(&remain);
      lwroc_cpu_set_copy(&remain, &_lwroc_cpu_set);

      num_remain = lwroc_cpu_set_count(&remain);

      if (num_remain >= 4)
	lwroc_cpu_set_clear(&remain, 0);

      last_index = remain._setsize * 8 - 1;

      /* We need to assign cores to LWROC_THREAD_CORE_PRIO_OTHER threads. */
      for (level = 0; level < LWROC_THREAD_CORE_PRIO_OTHER; level++)
	{
	  lwroc_cpu_set_copy(&remain_before_level, &remain);

	  num_threads = 0;

	  PD_LL_FOREACH(_lwroc_threads, iter)
	    {
	      lwroc_thread_instance *inst =
		PD_LL_ITEM(iter, lwroc_thread_instance, _threads);

	      if (inst->_info->_core_prio == level)
		num_threads++;
	    }

	  num_remain = lwroc_cpu_set_count(&remain);

#if LWROC_DEBUG_THREAD_AFFINITY
	  printf ("thread core_prio level: %d  threads: %d  cores remain: %d\n",
		  level, num_threads, num_remain);
#endif

	  if (num_threads > num_remain)
	    {
	      /* More threads at this level than cores remaining.
	       * Let the kernel scheduler deal with all remaining threads.
	       */
	      break;
	    }

	  /* For each thread, peel of one core... */

	  PD_LL_FOREACH(_lwroc_threads, iter)
	    {
	      lwroc_thread_instance *inst =
		PD_LL_ITEM(iter, lwroc_thread_instance, _threads);

	      if (inst->_info->_core_prio == level)
		{
		  lwroc_cpu_set_alloc(&inst->_cpu_set);
		  lwroc_cpu_set_peel_last(&inst->_cpu_set, &remain,
					  &last_index);
		  assign_single++;
		}
	    }

	  if (last_index == (size_t) -1)
	    {
	      lwroc_cpu_set_copy(&remain, &remain_before_level);
	      break;
	    }
	}

      /* Copy of remaining threads. */
      lwroc_cpu_set_copy(&remain_before_level, &remain);

#if LWROC_DEBUG_THREAD_AFFINITY
      printf ("thread core_prio level: %d  cores remain: %d\n",
	      level, num_remain);
#endif

      /* All remaining threads run at whatever core is available in
       * the remaining set, governed by the kernel scheduler.
       */

      PD_LL_FOREACH(_lwroc_threads, iter)
	{
	  lwroc_thread_instance *inst =
	    PD_LL_ITEM(iter, lwroc_thread_instance, _threads);

	  if (inst->_info->_core_prio >= level)
	    {
	      lwroc_cpu_set_alloc(&inst->_cpu_set);
	      lwroc_cpu_set_copy(&inst->_cpu_set, &remain);
	      assign_remain++;
	    }
	}

#if CPU_AFFINITY_PTHREAD_AFFINITY
      LWROC_INFO_FMT("%d threads direct affinity (%08lx), "
		     "%d threads remain (%08lx).",
		     assign_single,
		     (long) *(__cpu_mask*) _lwroc_cpu_set._mask,
		     assign_remain,
		     (long) *(__cpu_mask*) remain_before_level._mask);
#else
      (void)assign_single;
      (void)assign_remain;
#endif

      lwroc_cpu_set_free(&remain);
      lwroc_cpu_set_free(&remain_before_level);
    }

  /* Threads will be created as next step (with no further error
   * messages generated in-between.  So message logging shall not drop
   * messages but wait for network thread to become available.
   */
  _lwroc_threads_created = 1;
  MFENCE;

  PD_LL_FOREACH(_lwroc_threads, iter)
    {
      lwroc_thread_instance *inst =
	PD_LL_ITEM(iter, lwroc_thread_instance, _threads);

      if (inst->_info->_flags & LWROC_THREAD_MAIN)
	continue;

      ret = pthread_create(&inst->_thread, NULL,
			   lwroc_thread_generic_loop, inst);

      if (ret)
	{
	  /* Just in case buffer (almost) full.  Do not get stuck. */
	  _lwroc_threads_created = 0;
	  MFENCE;
	  LWROC_PERROR("pthread_create");
	  exit(1);
	}
    }

  /* Now they are really created. */
  _lwroc_threads_created = 2;
  MFENCE;
}

void lwroc_thread_also_wakeup_on_term(lwroc_thread_instance *inst,
				      lwroc_thread_instance *also_wakeup)
{
  assert(inst->_also_wakeup_on_term == NULL);
  inst->_also_wakeup_on_term = also_wakeup;
}

void lwroc_thread_wait(lwroc_thread_instance *inst)
{
  void *retval;
  int ret;

  /* Wake the thread up. */

  lwroc_thread_block_send_token(inst->_block);

  /* Wait for it. */

  ret = pthread_join(inst->_thread, &retval);

  if (ret)
    {
      LWROC_PERROR("pthread_join");
      exit(1);
    }

  /* One should now free whatever resources are associated with the
   * thread...
   */

  /* printf ("Waited for %s thread.\n", inst->_info->_purpose); */
}

void lwroc_all_threads_wait(void)
{
  pd_ll_item *iter;

  /* printf ("Wait for all threads.\n"); */

  PD_LL_FOREACH(_lwroc_threads, iter)
    {
      lwroc_thread_instance *inst =
	PD_LL_ITEM(iter, lwroc_thread_instance, _threads);

      if (inst->_info->_flags & (LWROC_THREAD_NO_RETURN |
				 LWROC_THREAD_MAIN))
	continue; /* No return, so no one to wait for. */

      lwroc_thread_wait(inst);
    }

  /* printf ("Waited for all threads.\n"); */
}

/*
void lwroc_all_threads_send_token()
{
  pd_ll_item *iter;

  PD_LL_FOREACH(_lwroc_threads, iter)
    {
      lwroc_thread_instance *inst =
	PD_LL_ITEM(iter, lwroc_thread_instance, _threads);

      lwroc_thread_block_send_token(inst->_block);
    }
}
*/


/* Call by user-created thread (in readout/filter) to at least set up
 * message logging.
 */
void lwroc_thread_user_init(int user_thread_no, const char *purpose)
{
  lwroc_thread_block *block;
  int msg_source;

  if (user_thread_no <= 0 ||
      user_thread_no > LWROC_MSG_BUFS_NUM_USER)
    {
      /* Need to increase LWROC_MSG_BUFS_NUM_USER ? */
      LWROC_FATAL_FMT("User thread init slot number %d outside range [1,%d].",
		      user_thread_no, LWROC_MSG_BUFS_NUM_USER);
    }

  msg_source = LWROC_MSG_BUFS_USER_1 + user_thread_no - 1;

  /* A thread_block (notification pipe) is needed. */
  block = lwroc_thread_block_init();

  lwroc_msg_thread_set_buf(msg_source, block);

  /* Generate a log greeting. */
  LWROC_INFO_FMT("This is user thread #%d: %s!",
		 user_thread_no, purpose ? purpose : "(purpose not known)");
}

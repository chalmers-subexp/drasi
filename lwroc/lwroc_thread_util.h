/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_THREAD_UTIL_H__
#define __LWROC_THREAD_UTIL_H__

#include "lwroc_thread_block.h"
#include "lwroc_cpu_affinity.h"
#include "pd_linked_list.h"

#include <pthread.h>

struct lwroc_thread_instance_t;
typedef struct lwroc_thread_instance_t lwroc_thread_instance;

/* Called before thread is created.  But inst already populated. */
typedef void (*lwroc_thread_at_prepare)(lwroc_thread_instance *inst);
/* The actual thread work. */
typedef void (*lwroc_thread_loop)(lwroc_thread_instance *inst);

#define LWROC_THREAD_MAIN                 0x01
#define LWROC_THREAD_NO_MSG_CLIENT_WAIT   0x02
#define LWROC_THREAD_NO_RETURN            0x04

/* Upon receipt of SIGINT, we attempt to terminate the program in an
 * orderly fashion.  The termination sequence is given by the data
 * flow - threads are terminated in that order to give them a chance
 * to complete the handling of outstanding data.
 */
/* Main thread is either readout or merger, terminate that first. */
#define LWROC_THREAD_TERM_MAIN        1
/* Then the validation thread of the merger. */
#define LWROC_THREAD_TERM_MERGE_VAL   2
/* The merge analyse thread can be stopped early. */
#define LWROC_THREAD_TERM_MERGE_ANALYSE  3
/* The merger input is data-flow wise before the merging, but either
 * EB merging stops processing data with consent from the master, in
 * which case no data is flowing at the time of termination.  When
 * time-sorting, there is no such concept, and some read data will get
 * lost anyhow.
 */
#define LWROC_THREAD_TERM_MERGE_IN    4
/* The filter is after the readout or merger.  It is usually before
 * network servers, but may also be a sibling to network servers of
 * the unfiltered data.  It is before file writing.
 */
#define LWROC_THREAD_TERM_FILTER      5
/* The timestamp filter is after the readout or merger or user filter.
 * It is usually a sibling of network servers, but before its own
 * network server.  It is before file writing.
 */
#define LWROC_THREAD_TERM_TS_FILTER   6
/* Network output is logically after the readout, merging or filter. */
#define LWROC_THREAD_TERM_NET_TRANS   7
/* File-writing is likely to have more latency in completing writes,
 * so after network servers.
 */
#define LWROC_THREAD_TERM_FILE        8
/* Slow async thread. */
#define LWROC_THREAD_TERM_SLOW_ASYNC  9
/* Close the generic network i/o thread last. */
#define LWROC_THREAD_TERM_NET_IO      10
/* The triva control never terminates, so put last in queue (for now). */
#define LWROC_THREAD_TERM_TRIVA_CTRL  11


#define LWROC_THREAD_CORE_PRIO_READOUT     0
#define LWROC_THREAD_CORE_PRIO_MERGER      1 /* Basically same as readout. */
#define LWROC_THREAD_CORE_PRIO_MERGE_IN    2
#define LWROC_THREAD_CORE_PRIO_FILTER      3
#define LWROC_THREAD_CORE_PRIO_FILE_WRITE  4
#define LWROC_THREAD_CORE_PRIO_SERVER      5
#define LWROC_THREAD_CORE_PRIO_TS_FILTER   6
#define LWROC_THREAD_CORE_PRIO_ANALYSE     7 /* Low CPU needs. */
#define LWROC_THREAD_CORE_PRIO_OTHER       8

typedef struct lwroc_thread_info_t
{
  lwroc_thread_at_prepare _at_prepare;
  lwroc_thread_loop       _loop;

  int                     _msg_buf_source;
  const char             *_purpose;

  int                     _flags;

  int                     _term_order;

  int                     _core_prio;

} lwroc_thread_info;

struct lwroc_thread_instance_t
{
  lwroc_thread_info  *_info;

  pthread_t           _thread;
  lwroc_thread_block *_block;

  pd_ll_item          _threads;

  lwroc_thread_instance *_also_wakeup_on_term;

  /* This member is written by another thread, and marks that this
   * thread should complete its business.
   */
  volatile int        _terminate;

  lwroc_cpu_set       _cpu_set;
};

lwroc_thread_instance *
lwroc_thread_prepare(lwroc_thread_info *info);

void lwroc_thread_also_wakeup_on_term(lwroc_thread_instance *inst,
				      lwroc_thread_instance *also_wakeup);

/* void lwroc_thread_wait(lwroc_thread_instance *inst); */

void lwroc_thread_apply_cpu_set(lwroc_thread_instance *inst);

void lwroc_all_threads_create(void);

void lwroc_all_threads_wait(void);

/* void lwroc_all_threads_send_token(void); */

void lwroc_thread_done(lwroc_thread_instance *inst);

void lwroc_thread_set_terminate(lwroc_thread_instance *inst);

void lwroc_thread_set_terminate_first(void);

/* This function should be called by a user thread early (before
 * generating any log messages).
 *
 * user_thread_no shall be unique, and start to number at 1,
 * (current maximum value is 3).
 *
 * purpose is just printed in a log greeting message.
 */
void lwroc_thread_user_init(int user_thread_no, const char *purpose);

#endif/*__LWROC_THREAD_UTIL_H__*/

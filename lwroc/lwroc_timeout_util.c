/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2021  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_timeout_util.h"

#include <math.h>
#include <stdio.h>

void lwroc_rate_limit_timeout(lwroc_timeout_state *state,
			      double Tmin, double Tmax,
			      struct timeval *now,
			      struct timeval *next)
{
  double maxfactor;
  double elapsed;
  double timeout;
  struct timeval add;

  /* We at most give a timeout of time Tmax,
   * and at minimum of time Tmin.
   * Quota is added proportionally to the time spent since
   * last timeout.
   *
   * When quota is 1, the maximum timeout will be calculated.
   * When quota is maxfactor, the minimum timeout will be calculated.
   *
   * quota = 1          => timeout = Tmax / 1 = Tmax
   * quota = Tmax/Tmin  => timeout = Tmax / (Tmax/Tmin) = Tmin
   */

  maxfactor = Tmax / Tmin;

  elapsed = (double) (now->tv_sec - state->_prev.tv_sec) +
    0.000001 * (double) (now->tv_usec - state->_prev.tv_usec);

  if (elapsed < 0.)          /* Backwards time does not impress. */
    elapsed = 0.;

  /* Note: first update will give max quota if prev time is
   * initialised to 0.
   */

  state->_quota -= 1.;               /* For this attempt. */
  state->_quota += (elapsed / Tmax); /* How much we gained since last
				      * time?
				      *
				      * This also means that if we
				      * worked for a long time, we
				      * will be willing to do more
				      * rapid attempts for a while.
				      */

  if (state->_quota < 1.)
    state->_quota = 1.;

  if (state->_quota > maxfactor)
    state->_quota = maxfactor;

  /* If progress was better than last time, add lots of quota (minimum
   * half).
   *
   * This handles the case where a system comes back after being lost
   * for a long time, to allow rapid restarts after that.  Without
   * giving lots of quota when a system continuously fails at the same
   * point, e.g. due to version mismatch.
   */
  if (state->_progress > state->_progress_last &&
      state->_quota < 0.5 * maxfactor)
    state->_quota = 0.5 * maxfactor;

  timeout = Tmax / state->_quota;

  add.tv_sec = (time_t) timeout;
  add.tv_usec = (int) (1000000. * (timeout - (double) add.tv_sec));

  timeradd(now, &add, next);

  state->_prev = *now;

  /*
  printf ("quota: %.1f  timeout: %.2f  elapsed: %.2f (%d,%d)\n",
	  state->_quota, timeout, elapsed,
	  state->_progress, state->_progress_last);
  */

  state->_progress_last = state->_progress;
  state->_progress = 0;
}

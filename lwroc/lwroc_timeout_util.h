/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2021  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_TIMEOUT_UTIL_H__
#define __LWROC_TIMEOUT_UTIL_H__

#include "../dtc_arch/acc_def/time_include.h"

typedef struct lwroc_timeout_state_t
{
  double          _quota;
  struct timeval  _prev;  /* Init to 0, first update will give max quota! */
  char            _progress;
  char            _progress_last;
} lwroc_timeout_state;

void lwroc_rate_limit_timeout(lwroc_timeout_state *state,
			      double Tmin, double Tmax,
			      struct timeval *now,
			      struct timeval *next);

#endif/*__LWROC_TIMEOUT_UTIL_H__*/

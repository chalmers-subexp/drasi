/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_TIMEOUTS_H__
#define __LWROC_TIMEOUTS_H__

/* Time we wait for incoming connections (portmap). */

#define LWROC_PORTMAP_CLOSE_TIMEOUT                     2 /* s */

/* Time we wait for incoming connections (data port). */

#define LWROC_INCOMING_CLOSE_TIMEOUT                    5 /* s */

/* Time an outgoing connection waits for link to be established. */

#define LWROC_OUTGOING_CLOSE_TIMEOUT                    5 /* s */

/* Maximum time between re-connection attempts. */

#define LWROC_CONNECTION_MAX_RETRY_INTERVAL            20 /* s */

/* Minimum time between re-connection attempts. */

#define LWROC_CONNECTION_MIN_RETRY_INTERVAL             1 /* s */

/* Minimum time between immediate re-connection attempts. */

#define LWROC_CONNECTION_MIN_IMMEDIATE_RETRY_INTERVAL   3 /* s */

/* Maximum time between log keep-alive messages. */

#define LWROC_CONNECTION_MSG_KEEPALIVE_INTERVAL         5 /* s */

/* Fixed timeout between transport re-connection attempts. */
/* Why fixed? */

#define LWROC_CONNECTION_TRANS_RETRY_TIMEOUT            5 /* s */

/* Time we wait for transport info only connections to close. */

#define LWROC_TRANS_CLOSE_TIMEOUT                       2 /* s */

/* Time we wait before diagnosing a stuck timesorter. */

#define LWROC_MERGE_SORT_NODATA_STUCK_TIMEOUT          10 /* s */

/* Time we wait before we start to semi-ignore a timesorter source
 * with broken timestamps. */

#define LWROC_MERGE_BROKEN_SOURCE_SEMI_IGNORE_TIMEOUT   1 /* s */

/* Time we wait until we send partial buffers. */
/* Less than a second such that monitoring does not 'pump'. */

#define LWROC_VARBUF_PARTIAL_BUFFER_TIMEOUT_US     500000 /* us */

/* For protocols which do not adapt buffer sizes, we have
 * to be more careful, so in the region of seconds. */

#define LWROC_FULLBUF_PARTIAL_BUFFER_TIMEOUT            2 /* s */

/* Time until keep-alive data buffer is sent. */

#define LWROC_KEEPALIVE_BUFFER_TIMEOUT                  5 /* s */

/* Time until file-writer force data buffer write. */

#define LWROC_FILEWRITE_PARTIAL_BUFFER_TIMEOUT         30 /* s */

/* Maximum time to wait for input data before timeout. */

#define LWROC_MERGE_KEEPALIVE_TIMEOUT                  30 /* s */

/* Time we allow before we declare a file writing to be jammed. */

#define LWROC_FILE_WRITE_FAIL_JAM_TIMEOUT               5 /* s */

/* (Default) time before we declare an in-spill marked DAQ to be
 * stuck in-spill (i.e. likely missing the off-spill trigger. */

#define LWROC_INSPILL_STUCK_TIMEOUT                    30 /* s */

/* Additional margin time between source max event interval,
 * and the actual disable timeout. */

#define LWROC_MERGE_SOURCE_DISABLE_INPUT_MARGIN         5 /* s */

/* Not really a timeout, but close enough.
 * Rate-limit of log messages. */
/* (50 kB/s = 180 MB/h) */

#define LWROC_LOG_MESSAGE_RATE_LIMIT                50000 /* B/s */

/* Initial log message quota on connection to new logger,
 * e.g. on startup.
 */

#define LWROC_LOG_MESSAGE_RATE_LIMIT_INIT_QUOTA  10000000 /* B */

#endif/*__LWROC_TIMEOUTS_H__*/

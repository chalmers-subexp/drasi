
/* This file is intentionally written such that it does not
 * directly depend on being compiled with the rest of lwroc.
 *
 * The reason is such that it can be included in other DAQ systems, in
 * particular the code being responsible for reading timestamps.
 *
 * Its purpose is to take care of reported timestamps and together
 * with the system clock try to estimate the timestamp rate.  By that,
 * it can then warn if timestamps are suddenly jumping, or if they
 * drastically change rate.
 */

/* This file intentionally does not include headers!
 *
 * Whoever includes this file is responsible to first include the
 * necessary headers.
 *
 * (The reason is that header inclusion sometimes is different on
 * different platforms.)
 *
 * Candidates:

#include <stdint.h>    // uint64_t
#include <inttypes.h>  // PRIu64
#include <sys/time.h>  // gettimeofday(), struct timeval
#include <stdlib.h>    // qsort()
#include <math.h>      // fabs(), NAN
#include <string.h>    // memset()

 * It is also necessary to ensure that these macros define something
 * useful:

#define LWROC_TRACK_TIMESTAMP_LOG_FMT
#define LWROC_TRACK_TIMESTAMP_CCLOG_FMT
#define LWROC_TRACK_TIMESTAMP_INFO_FMT
#define LWROC_TRACK_TIMESTAMP_WARNING_FMT
#define LWROC_TRACK_TIMESTAMP_ERROR_FMT

#define LWROC_TRACK_TIMESTAMP_LOG_NEW_FIT

 * If NAN is missing, #define'ing it as sqrtf(-1.f) may work.

 */

#include "lwroc_track_timestamp.h"
#include "lwroc_track_timestamp_extra.h"

/* Since the names are too long to nicely format in the code: */

#define __LTT_LOG_FMT      LWROC_TRACK_TIMESTAMP_LOG_FMT
#define __LTT_CCLOG_FMT    LWROC_TRACK_TIMESTAMP_CCLOG_FMT
#define __LTT_INFO_FMT     LWROC_TRACK_TIMESTAMP_INFO_FMT
#define __LTT_WARNING_FMT  LWROC_TRACK_TIMESTAMP_WARNING_FMT
#define __LTT_ERROR_FMT    LWROC_TRACK_TIMESTAMP_ERROR_FMT

/* Debug the facility? */

#define LWROC_TRACK_TIMESTAMP_DEBUG 0

#define LWROC_TRACK_TIMESTAMP_MEAS_CPU_TIME 0

#ifdef LWROC_TRACK_TIMESTAMP_DEBUG
# include "../dtc_arch/acc_def/myinttypes.h"
# include "../dtc_arch/acc_def/myprizd.h"
#endif

/* */

lwroc_timestamp_track_info _lwroc_track_timestamp;

int lwroc_compare_int(const void *p1, const void *p2)
{
  const int *i1 = (const int *) p1;
  const int *i2 = (const int *) p2;

  if (*i1 < *i2)
    return -1;
  return *i1 > *i2;
}

int lwroc_compare_double(const void *p1, const void *p2)
{
  const double *d1 = (const double *) p1;
  const double *d2 = (const double *) p2;

  if (*d1 < *d2)
    return -1;
  return *d1 > *d2;
}

void lwroc_linear_fit_fit(lwroc_linear_fit_sol *sol,
			  lwroc_linear_fit_data *input,
			  size_t n,
			  int include,
			  double est_slope,
			  double est_y0)
{
  double sum    = 0.;
  double sum_x  = 0.;
  double sum_y  = 0.;
  double sum_x2 = 0.;
  double sum_y2 = 0.;
  double sum_xy = 0.;
  double Sy_xx;
  double S_xy;
  double S_xx;
  double S_yy;
  double s2_eps;
  double s2_dxdy;
  double s2_y0;
  size_t i;

#if LWROC_TRACK_TIMESTAMP_DEBUG
  printf ("c %d\n", include);
#endif

  for (i = 0; i < n; i++)
    {
#if LWROC_TRACK_TIMESTAMP_DEBUG
      printf ("d %2d: %d\n",
	      i, track->_v[i]._flags);
#endif
    if (input[i]._flags & include)
      {
	double x, y;

	x = input[i]._x;
	y = input[i]._y;

#if LWROC_TRACK_TIMESTAMP_DEBUG /* && 0 */
	printf ("%2d: %.9f %.3f (%d %d)\n",
		i, x, y,
		(int) ref_time->tv_sec,
		(int) track->_v[i]._time.tv_sec);
#endif

	/* Bias the slope, such that the determined one is centered
	 * around 0.  Gives less numerical loss.
	 */
	y -= est_y0 + est_slope * x;

	sum    += 1;
	sum_x  += x;
	sum_y  += y;
	sum_x2 += x*x;
	sum_y2 += y*y;
	sum_xy += x*y;
      }
    }

#if LWROC_TRACK_TIMESTAMP_DEBUG
  printf ("sums: 1:%.2f x:%.2f y:%.2f xx:%.2f yy:%.2f xy:%.2f\n",
	  sum, sum_x, sum_y, sum_x2, sum_y2, sum_xy);
#endif

  /* See https://en.wikipedia.org/wiki/Simple_linear_regression,
   * section Numerical example.
   */

  Sy_xx = sum_y * sum_x2 - sum_x * sum_xy;
  S_xy = sum   * sum_xy - sum_x * sum_y ;
  S_xx = sum   * sum_x2 - sum_x * sum_x ;
  S_yy = sum   * sum_y2 - sum_y * sum_y ;
  /*
  double a     = sum_y * sum_x2 - sum_x * sum_xy;
  double b     = sum   * sum_xy - sum_x * sum_y ;
  double denom = sum   * sum_x2 - sum_x * sum_x ;
  */
  sol->dydx = S_xy  / S_xx;
  sol->y0   = Sy_xx / S_xx;

  /* To get the variance estimates, we need to figure out the
   * variance of the values.
   */

  s2_eps = 1/sum/(sum-2) * (S_yy - sol->dydx * sol->dydx * S_xx);

  /* If the data is 'too good', then s2_eps may end up being negative.
   *
   * Since the timestamps come as integers, we do not believe values
   * below 0.5^2.
   */
  if (s2_eps < 0.25)
    s2_eps = 0.25;

  s2_dxdy = sum * s2_eps / S_xx;
  s2_y0   = s2_dxdy / sum * sum_x2;

  sol->var_dydx    = s2_dxdy;
  sol->var_y0      = s2_y0;
  sol->cov_dydx_y0 = NAN;

#if LWROC_TRACK_TIMESTAMP_DEBUG
  printf ("sol: %.2f (%.2f) + t * %.2f (%.2f)  (cov: %.2f)  "
	  "s2_eps: %.2f (%.2f) s2_dydx: %.2f (%.2f) s2_y0: %.2f (%.2f) "
	  "ppm:%.2f\n",
	  sol->y0,   sqrt(sol->var_y0),
	  sol->dydx, sqrt(sol->var_dydx),
	  sol->cov_dydx_y0,
	  s2_eps,  sqrt(s2_eps),
	  s2_dxdy, sqrt(s2_dxdy),
	  s2_y0,   sqrt(s2_y0),
	  sqrt(s2_dxdy) / (sol->dydx + est_slope) * 1.e6);
#endif

  sol->dydx += est_slope;
  sol->y0   += est_y0;
}

void lwroc_linear_fit_filter_fit(lwroc_linear_fit_sol *sol,
				 lwroc_linear_fit_data *input,
				 size_t n,
				 double *tmp_2array, /* 2*n elements. */
				 int include,
				 size_t *p_num_used)
{
  size_t i;
  size_t num_used;

  double *y_diffs      = tmp_2array;
  double *y_diffs_sort = tmp_2array + n;

  /* Get rough dydx. */
  lwroc_linear_fit_fit(sol, input, n,
		       include, 0.0, 0.0);
  /* Avoid precision loss in dydx. */
  /* TODO: is this game really needed? */
  lwroc_linear_fit_fit(sol, input, n,
		       include, sol->dydx, sol->y0);

  /* We now do a further cleanup of the data by removing the worst
   * outliers...
   *
   * If some values are crazy off, they will differ compared to the
   * average values, so will be detected.  Once those are gone, the
   * outlier removal will start to detect semi-off values.
   */

  for ( ; ; )
    {
      size_t num_sorted = 0;
      double y_diffs_sum;
      size_t num_cut;
      double y_diffs_cut;

      /* For each data-point, calculate its distance squared to the
       * expected value with the fit so far.
       */

      for (i = 0; i < n; i++)
	if (input[i]._flags & include)
	  {
	    double x, y;
	    double y_expect;
	    double y_diff;
	    double y_diff_sqr;

	    x = input[i]._x;
	    y = input[i]._y;

	    y_expect = sol->y0 + x * sol->dydx;

	    /* Add 0.5 to all differences.  Fudge to deal with the y
	     * values being digitised to integers.  We do not want to
	     * punish measurements just because the fit might be 'far'
	     * away due to integer rounding.
	     */
	    y_diff = fabs(y_expect - y) + 1.0;

	    y_diff_sqr = y_diff * y_diff;
	    y_diffs[i] = y_diff_sqr;
	    y_diffs_sort[num_sorted++] = y_diff_sqr;

#if LWROC_TRACK_TIMESTAMP_DEBUG /* && 0 */
	    printf ("%2" MYPRIzd ": %.9f %.3f (%d %d) %.3f %.3f %.3f\n",
		    i, x, y,
		    (int) ref_time.tv_sec,
		    (int) track->_v[i]._time.tv_sec,
		    y_expect,
		    y_diff_sqr,
		    y_diffs[i]);
#endif
	  }

      /* Sort the squared differences. */
      qsort (y_diffs_sort, num_sorted, sizeof (y_diffs_sort[0]),
	     lwroc_compare_double);

      /* Sum up the differences until 3/4 of the samples, outliers
       * would be last.  At most cut down to half the samples.
       */
      num_cut = (3*num_sorted)/4;
      if (num_cut < n/2)
	num_cut = n/2;

      y_diffs_sum = 0;
      for (i = 0; i < num_cut; i++)
	y_diffs_sum += y_diffs_sort[i];
      y_diffs_sum /= (double) num_cut;

      /* Allowance factor of 5. */
      y_diffs_cut = y_diffs_sum * 3;
      /* But do not go lower than the actual value at the cut. */
      if (y_diffs_cut < y_diffs_sort[num_cut-1])
	y_diffs_cut = y_diffs_sort[num_cut-1];

      /* Which samples survive the cut? */
      num_used = 0;
      for (i = 0; i < n; i++)
	if (input[i]._flags & include)
	  {
	    input[i]._flags &= ~include;
	    if (y_diffs[i] <= y_diffs_cut)
	      {
		input[i]._flags |=
		  LWROC_TIMESTAMP_FIT_FIT2;
		num_used++;
	      }
	  }

#if LWROC_TRACK_TIMESTAMP_DEBUG /* || 1 */
      printf ("sum: %.3f cutoff: %.3f  "
	      "%" MYPRIzd "/%" MYPRIzd "/%" MYPRIzd "\n",
	      y_diffs_sum, y_diffs_cut,
	      num_used, num_sorted, n);
#endif

      /* If no further samples were removed, we are done. */
      if (num_used == num_sorted)
	break;

      /* Redo the fit. */

      include = LWROC_TIMESTAMP_FIT_FIT2;

      /* Get rough dydx. */
      lwroc_linear_fit_fit(sol, input, n,
			   include, 0.0, 0.0);
      /* Avoid precision loss in dydx. */
      /* TODO: is this game really needed? */
      lwroc_linear_fit_fit(sol, input, n,
			   include, sol->dydx, sol->y0);
    }

  if (p_num_used)
    *p_num_used = num_used;
}

void lwroc_track_timestamp_filter_fit(lwroc_timestamp_track_info *track)
{
  size_t i;
  int udiffs[LWROC_TRACK_TIMESTAMP_HISTORY];
  int udiff_cut;
  size_t num_udiffs;
  size_t last_i_consider;
  size_t num_used;
  int include;

  lwroc_linear_fit_data lf_data[LWROC_TRACK_TIMESTAMP_HISTORY];
  double tmp_2array[2 * LWROC_TRACK_TIMESTAMP_HISTORY];

  lwroc_timestamp_track_sol sol;

  struct timeval ref_time;
  uint64_t ref_stamp;

#if LWROC_TRACK_TIMESTAMP_MEAS_CPU_TIME
  struct timeval a, b;

  gettimeofday(&a, NULL);
#endif

  /* Try to figure out the rate of the time stamps vs the system clock.
   * (We will likely just rather figure out the drift of the system
   * clock...)
   *
   * A multi-pass operation:
   *
   * - If the timestamps have boxed values, then first remove those
   *   with the worst box values.
   * - Then do a preliminary fit.
   * - Remove the worst outliers.
   * - Redo the fit.
   *
   * With boxed values, all these tricks should not be needed.  But we
   * do it in order to obtain accurate values.  Which should improve
   * the usefulness of the values for debugging.
   *
   * Even if the user does not supply fresh values upon request for
   * tight boxing, we can do the boxing by checking the time after an
   * event, and then take the stamp and time of the next-next event.
   * Next-next since the deadtime might have been released already
   * when we measure the first time, and thus the next timestamp may
   * be from a time before that.
   */

  num_udiffs = 0;
  last_i_consider = 0;

  for (i = 0; i < LWROC_TRACK_TIMESTAMP_HISTORY; i++)
    {
      lf_data[i]._flags = 0;

      if (track->_v[i]._time.tv_sec != 0)
	{
	  udiffs[num_udiffs++] = track->_v[i]._udiff;
	  lf_data[i]._flags |= LWROC_TIMESTAMP_FIT_CONSIDER;
	  last_i_consider = i;
	}
#if LWROC_TRACK_TIMESTAMP_DEBUG
      printf ("a %2zd: %d (%d.%06d %" PRIu64 " %d)\n",
	      i,
	      track->_v[i]._flags,
	      (int) track->_v[i]._time.tv_sec,
	      (int) track->_v[i]._time.tv_usec,
	      track->_v[i]._stamp,
	      track->_v[i]._udiff);
#endif
    }

  /* Use the last time for reference.  Makes values smaller. */
  ref_time =
    track->_v[last_i_consider]._time;
  /* ref_time.tv_sec -= 3; */ /* To provoke monitor offsets. */
  ref_stamp =
    track->_v[last_i_consider]._stamp;

  qsort (udiffs, num_udiffs, sizeof (udiffs[0]),
	 lwroc_compare_int);

  /* We place the cutoff at two times the quarter best value. */

  udiff_cut = 2 * udiffs[num_udiffs / 4];

  /* But do not cut more than half the values. */

  if (udiff_cut < udiffs[num_udiffs / 2])
    udiff_cut = udiffs[num_udiffs / 2];

  include = LWROC_TIMESTAMP_FIT_FIT1;

  num_used = 0;
  for (i = 0; i < LWROC_TRACK_TIMESTAMP_HISTORY; i++)
    {
      if ((lf_data[i]._flags &
	   LWROC_TIMESTAMP_FIT_CONSIDER) &&
	  track->_v[i]._udiff <= udiff_cut)
	{
	  struct timeval x_diff;
	  int64_t y_diff;
	  double x, y;

	  timersub(&ref_time, &track->_v[i]._time, &x_diff);
	  y_diff =
	    (int64_t) ref_stamp - (int64_t) track->_v[i]._stamp;

	  x = -((double) x_diff.tv_sec + 1.e-6 * (double) x_diff.tv_usec);
	  y = -(double) y_diff;

	  lf_data[i]._x = x;
	  lf_data[i]._y = y;
	  lf_data[i]._flags |= include;
	  num_used++;
	}
#if LWROC_TRACK_TIMESTAMP_DEBUG
      printf ("b %2zd: %d\n",
	      i, track->_v[i]._flags);
#endif
    }

#if LWROC_TRACK_TIMESTAMP_DEBUG
  printf ("udiff_cut: %d  used: %" MYPRIzd "\n",
	  udiff_cut, num_used);
#endif

  /* Crazy outliers could be removed by calculating the difference
   * between all pairs of timestamps, and of that calculate a geometric
   * average.  The same is done for all differences associated with
   * each timestamp.  And then compare to the same for the times.
   *
   * Not implemented (see test code however).  We get the same effect
   * from the outlier removal performed by this routine.
   */

  lwroc_linear_fit_filter_fit(&sol.lfs,
			      lf_data, LWROC_TRACK_TIMESTAMP_HISTORY,
			      tmp_2array, include, &num_used);

  /* Outlier removal done. */

  if (LWROC_TRACK_TIMESTAMP_LOG_NEW_FIT)
    __LTT_CCLOG_FMT(track->_msg_context,
		    "New %s tracking fit, "
		    "ref_t=%ld.%06d ref_s=%" PRIu64 ", "
		    "s = ref_s + %.1f(%.1f) + %.1f(%.1f) * (t-ref_t), "
		    "sigma_off: %.2f us, sigma_slope: %.3f ppm.  "
		    "Used %d/%d samples.",
		    track->_purpose ? track->_purpose : "??",
		    (long int) ref_time.tv_sec,
		    (int) ref_time.tv_usec,
		    ref_stamp,
		    sol.lfs.y0,   sqrt(sol.lfs.var_y0),
		    sol.lfs.dydx, sqrt(sol.lfs.var_dydx),
		    /* var_y0 also scaled with dydx, since x is in s. */
		    sqrt(sol.lfs.var_y0)   / (sol.lfs.dydx) * 1.e6,
		    sqrt(sol.lfs.var_dydx) / (sol.lfs.dydx) * 1.e6,
		    (int) num_used, LWROC_TRACK_TIMESTAMP_HISTORY);

  /* Compare the fit result to the previous fit.
   * If there was a previous fit...
   */

  if (track->_ref.y_ref)
  {
    double change =
      (sol.lfs.dydx - track->_ref.lfs.dydx) /
      (0.5 * (track->_ref.lfs.dydx + sol.lfs.dydx));

    if (fabs(change) > 10.e-6)
      {
	__LTT_WARNING_FMT("Rate of %s changed by %.1f ppm, "
			  "from %.1f ticks/s to %.1f ticks/s.",
			  track->_purpose ? track->_purpose : "??",
			  change * 1.e6,
			  track->_ref.lfs.dydx,
			  sol.lfs.dydx);
      }
    else
      {
	/* Calculate what the new reference time should have been
	 * using the previously estimated frequency.
	 */

	uint64_t diff_stamp;
	double d_diff_stamp;
	double d_diff_time_expect;
	struct timeval diff_time;
	double d_diff_time;

	double d_diff_diff_time;
	double relerror;

	diff_stamp = ref_stamp - track->_ref.y_ref;
	d_diff_stamp = (double) diff_stamp;
	d_diff_time_expect =
	  (d_diff_stamp - track->_ref.lfs.y0) /
	  track->_ref.lfs.dydx;

	/* */

	timersub(&ref_time, &track->_ref.x_ref, &diff_time);

	d_diff_time =
	  (double) diff_time.tv_sec + 1.0e-6 * (double) diff_time.tv_usec;

	/* */

	d_diff_diff_time = d_diff_time_expect - d_diff_time;

	relerror = d_diff_diff_time / d_diff_time;

#if LWROC_TRACK_TIMESTAMP_DEBUG
	printf ("diff_stamp: %.0f diff_time: %.6f s, diff_diff_time: %.6f s, "
		"relerr: %.2f ppm\n",
		d_diff_stamp, d_diff_time,
		d_diff_diff_time, relerror * 1.e6);
#endif

	if (fabs(relerror) > 100e-6)
	  {
	    __LTT_WARNING_FMT("Projection of %s from previous fit "
			      "mismatches current time by %.6f over %.2f s, "
			      "%.1f ppm.",
			      track->_purpose ? track->_purpose : "??",
			      d_diff_diff_time, d_diff_time,
			      relerror * 1.e6);

	  }
      }
  }

  /* Record the fit result. */

  sol.x_ref = ref_time;
  sol.y_ref = ref_stamp;

  sol.min_udiff = udiffs[0];

  track->_ref = sol;


#if LWROC_TRACK_TIMESTAMP_MEAS_CPU_TIME
  gettimeofday(&b, NULL);

  printf ("time for fit: %d us\n",
	  (int) (b.tv_usec - a.tv_usec) +
	  1000000 * (int) (b.tv_sec - a.tv_sec));
#endif
}

void lwroc_track_timestamp_eval(lwroc_timestamp_track_info *track)
{
  lwroc_timestamp_track_sol *sol;
  lwroc_timestamp_track_sol combine;

  (void) combine;
  /* If we have an NTP tracker, then factor that into the solution. */

  sol = &(track->_ref);

#if LWROC_TRACK_TIMESTAMP_UPDATE_MAIN_MON
  if (_lwroc_ntp_global_sol)
    {
      lwroc_timestamp_track_sol ntp_sol;
      double ntp_sol_x_ref_minus_sol_x_ref;
      double ntp_sol_dydx;
      double ntp_sol_y0;
      double ntp_sol_var_dydx;
      double ntp_sol_var_y0;

      if (!lwroc_two_copy_fetch(&_lwroc_ntp_global_sol->_two_copy, &ntp_sol))
	{
	  /* Fetching failed, this should be an intermittent unlucky
	   * chance happening, so just do not update this cycle.
	   */
	  return;
	}

      /* No NTP timescale is presently available.
       *
       * Either because we have recently detected a (large) clock
       * rate change (which also affect the timestamp measurements.
       *
       * Or because NTP is not responding.
       */

      if (!ntp_sol.y_ref)
	goto no_ntp; /* There is no NTP fit, use local time. */

      if (ntp_sol.status == LWROC_NTP_FIT_STATUS_INVESTIGATE1 ||
	  ntp_sol.status == LWROC_NTP_FIT_STATUS_INVESTIGATE2 ||
	  ntp_sol.status == LWROC_NTP_FIT_STATUS_RECOLLECT)
	{
	  /* NTP is currently being investigated for detection
	   * of a possible local clock jump.  This is rather quick,
	   * and if we indeed have a jump, local time is bad,
	   * so do not report the evaluation.
	   */
	  return;
	}

      if (ntp_sol.status != LWROC_NTP_FIT_STATUS_OK)
	goto no_ntp; /* Should not happen, but use local time. */

      memset(&combine, 0, sizeof (combine));

      /* We want to recalculate the timestamp tracking fit into the frame
       * of the NTP time tracking.
       *
       * s = timestamp    s_r      = reference timestamp
       * t = local time   t_s, t_q = reference times
       * q = NTP time     q_r      = reference time
       *
       * c, d, e, f: coefficients
       *
       * The fits that have been performed are:
       *
       * s - s_r = d + c * (t - t_s)
       * q - q_r = f + e * (t - t_q)
       *
       * We want to calculate
       *
       * s = function of q (removing t)
       *
       * The reason we cannot fit immediately is because both sets
       * of measurements are versus t, but not at the same times.
       *
       * s - s_r = d - c/e*f + c * (t_q - t_s) + c/e * (q - q_r)
       *
       */

#define FRAC_1div2pow32 (1 / (double) (0x100000000ll))

      /* The NTP timescale operate with the low 32 bits as the fractional
       * part.  Recalculate the slope and intercept.
       */
      ntp_sol_dydx = ntp_sol.lfs.dydx * FRAC_1div2pow32;
      ntp_sol_y0   = ntp_sol.lfs.y0   * FRAC_1div2pow32;

      ntp_sol_var_dydx =
	ntp_sol.lfs.var_dydx * FRAC_1div2pow32 * FRAC_1div2pow32;
      ntp_sol_var_y0   =
	ntp_sol.lfs.var_y0   * FRAC_1div2pow32 * FRAC_1div2pow32;

      /* The timestamp reference is unchanged. */
      combine.y_ref = sol->y_ref;
      /* The real time reference is the NTP time reference. */
      combine.x_ref.tv_sec  =
	(time_t) ((ntp_sol.y_ref >> 32) - LWROC_UNIX_EPOCH_NTP);;
      combine.x_ref.tv_usec =
	(int)    (((ntp_sol.y_ref & 0xffffffff) * 1000000) >> 32);
      /* The offset is given by the three terms. */
      ntp_sol_x_ref_minus_sol_x_ref =
	(( double) ( ntp_sol.x_ref.tv_sec  -
		     /**/sol->x_ref.tv_sec ) +
	 ((double) ( ntp_sol.x_ref.tv_usec -
		     /**/sol->x_ref.tv_usec)) * 0.000001);
      combine.lfs.y0 =
	sol->lfs.y0 -
	sol->lfs.dydx / ntp_sol_dydx * ntp_sol_y0 +
	sol->lfs.dydx * ntp_sol_x_ref_minus_sol_x_ref;
      /* The slope is the combined slope. */
      combine.lfs.dydx = sol->lfs.dydx / ntp_sol_dydx;

#if LWROC_TRACK_TIMESTAMP_DEBUG
      printf ("ntp     y0: %.9f     dydx: %.9f\n",
	      ntp_sol_y0, ntp_sol_dydx);
      printf ("ntp var_y0: %.9f var_dydx: %.9f\n",
	      ntp_sol_var_y0, ntp_sol_var_dydx);
      printf ("ntp sig_y0: %.9f sig_dydx: %.9f\n",
	      sqrt(ntp_sol_var_y0), sqrt(ntp_sol_var_dydx));
#endif

#define SQR(x) ((x)*(x))

      /* For the uncertainties, should they be added quadratically? */
      combine.lfs.var_dydx =
	sol->lfs.var_dydx / SQR(ntp_sol_dydx) +
	SQR(sol->lfs.dydx) * ntp_sol_var_dydx / SQR(SQR(ntp_sol_dydx));

#if LWROC_TRACK_TIMESTAMP_DEBUG
      printf ("var_dydx %.6f = %.6f + %.6f\n",
	      sqrt(combine.var_dydx),
	      sqrt(sol->var_dydx / SQR(ntp_sol_dydx)),
	      sqrt(SQR(sol->dydx) * ntp_sol_var_dydx / SQR(SQR(ntp_sol_dydx))));
#endif

      combine.lfs.var_y0 =
	/* 1st term */
	sol->lfs.var_y0 +
	/* 2nd term */
	sol->lfs.var_dydx  / SQR(ntp_sol_dydx) * SQR(ntp_sol_y0) +
	SQR(sol->lfs.dydx) * ntp_sol_var_dydx / SQR(SQR(ntp_sol_dydx)) *
	/* */                                   SQR(ntp_sol_y0) +
	SQR(sol->lfs.dydx) / SQR(ntp_sol_dydx) * ntp_sol_var_y0 +
	/* 3rd term */
	sol->lfs.var_dydx * SQR(ntp_sol_x_ref_minus_sol_x_ref);

#if LWROC_TRACK_TIMESTAMP_DEBUG
      printf ("var_y0 %.6f = %.6f + (%.6f + %.6f + %.6f) + %.6f\n",
	      sqrt(combine.var_y0),
	      sqrt(sol->var_y0),
	      sqrt(sol->var_dydx  / SQR(ntp_sol_dydx) * SQR(ntp_sol_y0)),
	      sqrt(SQR(sol->dydx) * ntp_sol_var_dydx / SQR(SQR(ntp_sol_dydx)) *
		   /* */                                SQR(ntp_sol_y0)),
	      sqrt(SQR(sol->dydx) / SQR(ntp_sol_dydx) * ntp_sol_var_y0),
	      sqrt(sol->var_dydx * SQR(ntp_sol_x_ref_minus_sol_x_ref)));
#endif

      combine.lfs.cov_dydx_y0 = NAN;

      if (LWROC_TRACK_TIMESTAMP_LOG_NEW_FIT)
	__LTT_CCLOG_FMT(track->_msg_context,
			"New combined %s tracking fit, "
			"ref_t=%ld.%06d ref_s=%" PRIu64 ", "
			"s = ref_s + %.1f(%.1f) + %.1f(%.1f) * (t-ref_t), "
			"sigma_off: %.2f us, sigma_slope: %.3f ppm.",
			track->_purpose ? track->_purpose : "??",
			(long int) combine.x_ref.tv_sec,
			(int)      combine.x_ref.tv_usec,
			combine.y_ref,
			combine.lfs.y0,   sqrt(combine.lfs.var_y0),
			combine.lfs.dydx, sqrt(combine.lfs.var_dydx),
			sqrt(combine.lfs.var_y0)   / (combine.lfs.dydx) *1.e6,
			sqrt(combine.lfs.var_dydx) / (combine.lfs.dydx) *1.e6);

      /* Take the solution to report as the combined solution. */
      sol = &combine;

    no_ntp:
      ;
    }
#endif

  (void) sol;

#if LWROC_TRACK_TIMESTAMP_UPDATE_MAIN_MON
  _lwroc_mon_main._ts_ref_stamp = sol->y_ref;
  _lwroc_mon_main._ts_ref_time._sec = (uint64_t) sol->x_ref.tv_sec;
  _lwroc_mon_main._ts_ref_time._nsec = (uint32_t) sol->x_ref.tv_usec * 1000;
  _lwroc_mon_main._ts_ref_stamp_freq_k = (uint64_t) (sol->lfs.dydx * 1.e3);
  _lwroc_mon_main._ts_ref_stamp_y0_k = (uint64_t) (int64_t) (sol->lfs.y0 *1.e3);
  _lwroc_mon_main._ts_ref_stamp_sig_freq_k =
    (uint64_t) (sqrt(sol->lfs.var_dydx) * 1.e3);
  _lwroc_mon_main._ts_ref_stamp_sig_y0_k =
    (uint64_t) (sqrt(sol->lfs.var_y0) * 1.e3);
#endif
}

/* Calculate the difference between the actual stamp, and what the fit
 * would have expected.
 */
double lwroc_track_timestamp_fit_diff(lwroc_timestamp_track_sol *sol,
				      struct timeval *t,
				      uint64_t stamp)
{
  struct timeval diff_time;
  double d_diff_time;
  double d_diff_stamp;
  int64_t diff_stamp;

  timersub(t,  &sol->x_ref, &diff_time);

  d_diff_time =
    (double) diff_time.tv_sec + 1.0e-6 * (double) diff_time.tv_usec;

  diff_stamp =
    (int64_t) stamp -
    (int64_t) sol->y_ref;

  d_diff_stamp  =
    ((double) diff_stamp) - (sol->lfs.y0 + sol->lfs.dydx * d_diff_time);

  return d_diff_stamp;
}

#define LWROC_TRACK_TIMESTAMP_FORWARD_MARGIN 0.2 /* s */
#if 0
#undef LWROC_TRACK_TIMESTAMP_FORWARD_MARGIN
#define LWROC_TRACK_TIMESTAMP_FORWARD_MARGIN 0.002 /* s */
#endif

void lwroc_track_timestamp_calc_allowance(lwroc_timestamp_track_info *track,
					  struct timeval *now,
					  double *d_diff_now_ref)
{
  struct timeval diff_time;
  double d_diff_time;
  double d_diff_stamp;

  if (timercmp(now, &track->_ref.x_ref, < ))
    {
      __LTT_ERROR_FMT("System clock moved backwards!  (%ld.%06d < %ld.%06d)  "
		      "Cannot reliably extrapolate allowed timestamp range.",
		      (long) now->tv_sec,
		      (int)  now->tv_usec,
		      (long) track->_ref.x_ref.tv_sec,
		      (int)  track->_ref.x_ref.tv_usec);
      track->_allow_stamp = 0;
      *d_diff_now_ref = 0;
      return;
    }

  timersub(now,  &track->_ref.x_ref, &diff_time);

  d_diff_time =
    (double) diff_time.tv_sec + 1.0e-6 * (double) diff_time.tv_usec;

  *d_diff_now_ref = d_diff_time;

  /* Make allowance for 0.2 s into the future (before we check system
   * clock again.
   */

  d_diff_time += LWROC_TRACK_TIMESTAMP_FORWARD_MARGIN;

  d_diff_stamp = track->_ref.lfs.y0 +
    track->_ref.lfs.dydx * d_diff_time;

  track->_allow_stamp =
    track->_ref.y_ref +
    (uint64_t) d_diff_stamp;
}

volatile int _lwroc_timestamp_periodic = 0;

void lwroc_init_timestamp_track(void)
{
  memset (&_lwroc_track_timestamp, 0, sizeof (_lwroc_track_timestamp));

  _lwroc_track_timestamp._purpose = "timestamp";
}

int lwroc_report_event_timestamp(uint64_t stamp, int flags)
{
  struct timeval now;
  int retval;
  uint64_t prev_stamp;
  lwroc_timestamp_track_info *track = &_lwroc_track_timestamp;

#if LWROC_TRACK_TIMESTAMP_UPDATE_MAIN_MON
  _lwroc_mon_main._last_timestamp = stamp;
#endif

  if (flags & LWROC_TRACK_TIMESTAMP_INVALID)
    {
      /* Since this badness comes form the outside, and is known
       * there, we do not produce any log message.
       */
      return LWROC_TRACK_TIMEST_RET_INVALID;
    }

  /* We are very quick in case the timestamp is in the allowed range,
   * i.e. after the previous, and within the allowance range.
   *
   * In particular, we only perform the expensive gettimeofday() call
   * when moving outside the allowance range.
   */

  /* Check that we did not go backwards. */
  if (stamp <= track->_prev_stamp)
    {
      if (track->_ref.y_ref)
	{
	  /* We have a fit, so tell how bad we are... */

	  uint64_t diff_stamp;
	  double d_diff_stamp;
	  double d_diff_time;

	  diff_stamp = track->_prev_stamp - stamp;
	  d_diff_stamp = -(double) diff_stamp;
	  d_diff_time =
	    (d_diff_stamp) / track->_ref.lfs.dydx;

	  __LTT_ERROR_FMT("Reported timestamp (%" PRIu64 ") "
			  "is before or equal to previous (%" PRIu64 "), "
			  "with %.3f s according to fit.",
			  stamp, track->_prev_stamp,
			  d_diff_time);
	}
      else
	{
	  __LTT_ERROR_FMT("Reported timestamp (%" PRIu64 ") "
			  "is before or equal to previous (%" PRIu64 ").",
			  stamp, track->_prev_stamp);
	}

      track->_prev_stamp = stamp;
      /* Catch us on next stamp again. */
      track->_allow_stamp = stamp;
      /* Also set some internal markers... */
      return LWROC_TRACK_TIMEST_RET_BACKWARDS;
    }

  prev_stamp = track->_prev_stamp;
  /* Do not allow any following stamps to go backwards. */
  track->_prev_stamp = stamp;

  /* Check if we are beyond the 'safe' (short forward) point. */
  if (stamp < track->_allow_stamp)
    {
      if (flags & LWROC_TRACK_TIMESTAMP_FRESH)
	{
	  struct timeval diff;
	  int i = track->_v_last;

	  /* The timestamp was extra made on our request, to get a
	   * better (boxed) value.
	   */

	  gettimeofday(&now, NULL);

	  timersub(&now, &track->_v[i]._time, &diff);

	  if (diff.tv_sec == 0 &&
	      track->_v[i]._udiff == 0)
	    {
	      /* Boxed difference less than a second. */

	      track->_v[i]._time = now;
	      track->_v[i]._stamp = stamp;
	      track->_v[i]._udiff = (int) diff.tv_usec;
	    }
	}

      /* Stamp is good (it seems). */
      retval = LWROC_TRACK_TIMEST_RET_SEEMS_OK;

      if (_lwroc_timestamp_periodic < 10)
	return retval;
      /* Once in a while we go the full way to check the local clock,
       * in case we should collect another value to make a new fit.
       */
      _lwroc_timestamp_periodic = 0;
      /* printf ("Force time check.\n"); */
    }

  /* So we are beyond the currently allowed range.
   * Perhaps it was just a long time since the previous event, and
   * we should extend the allowance?
   */

  gettimeofday(&now, NULL);

  /* We can only check if we are too crazily forward if we have a fit. */
  if (track->_ref.y_ref)
    {
      double d_diff_now_ref;

      lwroc_track_timestamp_calc_allowance(track, &now, &d_diff_now_ref);

#if LWROC_TRACK_TIMESTAMP_DEBUG && 0
      printf ("allow_stamp: %" PRIu64 "\n",
	      track->_allow_stamp);
#endif

      if (stamp > track->_allow_stamp)
	{
	  uint64_t diff_stamp;
	  double d_diff_stamp;
	  double d_diff_time;

	  diff_stamp = stamp - track->_allow_stamp;
	  d_diff_stamp = (double) diff_stamp;
	  d_diff_time =
	    (d_diff_stamp - track->_ref.lfs.y0) /
	    track->_ref.lfs.dydx;

	  __LTT_ERROR_FMT("Reported timestamp (%" PRIu64 ") "
			  "is %.3f s in the future beyond extrapolation "
			  "(%.3f s after fit ref-t; margin of %.1f s).",
			  stamp,
			  d_diff_time + LWROC_TRACK_TIMESTAMP_FORWARD_MARGIN,
			  d_diff_now_ref,
			  LWROC_TRACK_TIMESTAMP_FORWARD_MARGIN);
	  retval = LWROC_TRACK_TIMEST_RET_FUTURE;

	  /* Being too far in the future should not prevent us from
	   * accumulating into the fit...  Otherwise we may never fit
	   * again.
	   */

	  /* In principle, we should _not_ update the value of the
	   * previous timestamp, since we then may (likely) report the
	   * next as being backwards...  So next backward check should
	   * still be vs. the previous.
	   */
	  track->_prev_stamp = prev_stamp;
	}
      else
	retval = LWROC_TRACK_TIMEST_RET_SEEMS_OK;
    }
  else
    retval = LWROC_TRACK_TIMEST_RET_NO_FIT;

  /* Check if we also want to record this for the next fit. */
  if (timercmp(&now, &track->_next_record, >) ||
      timercmp(&now, &track->
	       /*      */ _v[track->_v_last]._time, <))
    {
      int i;

      track->_v_last =
	(track->_v_last + 1) % LWROC_TRACK_TIMESTAMP_HISTORY;

      if (track->_v_last == 0)
	{
	  lwroc_track_timestamp_filter_fit(track);

	  lwroc_track_timestamp_eval(track);
	}

      i = track->_v_last;

#if LWROC_TRACK_TIMESTAMP_DEBUG && 0
      printf ("accumulate %d\n", i);
#endif

      track->_v[i]._time = now;
      track->_v[i]._stamp = stamp;
      track->_v[i]._udiff = 0;

      if (track->_ref.y_ref)
	{
	  /* We have a fit, get another measurement in a second. */

	  track->_next_record = now;
	  track->_next_record.tv_sec++;
	}
      else
	{
	  /* No fit yet.
	   * But enforce some baseline, or the fit will just be crap.
	   */
	  struct timeval add = { 0, 10000 }; /* 10 ms */
	  timeradd(&now, &add, &track->_next_record);
	}

      /* We would like to get a boxed value if possible. */
      retval |= LWROC_TRACK_TIMEST_RET_REQ_FRESH;
    }

  return retval;
}

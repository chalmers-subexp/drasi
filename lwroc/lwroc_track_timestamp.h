#ifndef __LWROC_TRACK_TIMESTAMP_H__
#define __LWROC_TRACK_TIMESTAMP_H__

/* This file intentionally does not include headers!
 *
 * Whoever includes this file is responsible to first include the
 * necessary headers.
 *
 * Candidates:

#include <stdint.h>

 */

#define LWROC_TRACK_TIMESTAMP_INVALID     0x0001
#define LWROC_TRACK_TIMESTAMP_FRESH       0x0002

#define LWROC_TRACK_TIMEST_RET_NO_FIT     0x0001 /* no idea yet */
#define LWROC_TRACK_TIMEST_RET_SEEMS_OK   0x0002 /* likely ok */
#define LWROC_TRACK_TIMEST_RET_REQ_FRESH  0x0004 /* additional sample req. */
#define LWROC_TRACK_TIMEST_RET_INVALID    0x0010 /* bad */
#define LWROC_TRACK_TIMEST_RET_BACKWARDS  0x0020 /* bad */
#define LWROC_TRACK_TIMEST_RET_FUTURE     0x0040 /* bad */

#define LWROC_TRACK_TIMEST_RET_BAD (LWROC_TRACK_TIMEST_RET_INVALID | \
				    LWROC_TRACK_TIMEST_RET_BACKWARDS | \
				    LWROC_TRACK_TIMEST_RET_FUTURE)

/* Call the init function before using the report routine. */

void lwroc_init_timestamp_track(void);

/* Call the report routine with each new timestamp.
 *
 * Only a few timestamps are actually used to track and fit.
 *
 * The routine is designed to most often return very quickly, if the
 * new timestamp is within a short time window since the previously
 * calculated window.
 *
 * The flags can be:
 *
 * LWROC_TRACK_TIMESTAMP_INVALID - Do not use this timestamp for
 *                                 time tracking.
 * LWROC_TRACK_TIMESTAMP_FRESH   - Timestamp is from a fresh
 *                                 immediately new sample.
 *
 * Return value is a combination of flags:
 *
 * LWROC_TRACK_TIMEST_RET_INVALID
 *   Just given when LWROC_TRACK_TIMESTAMP_INVALID was provided.
 *
 * LWROC_TRACK_TIMEST_RET_BACKWARDS
 *   Timestamp smaller than previous, this (or previous) broken.
 *
 * LWROC_TRACK_TIMEST_RET_FUTURE
 *   Timestamp much larger than expected from fit.  Likely broken!
 *
 * LWROC_TRACK_TIMEST_RET_NO_FIT
 *   No fit is available, so the quality of the timestamp
 *   cannot be determined.
 *
 * LWROC_TRACK_TIMEST_RET_SEEMS_OK
 *   The timestamp *seems* to be good (no guarantee).
 *   Note that this is from a rough window estimate only.
 *
 * LWROC_TRACK_TIMEST_RET_REQ_FRESH
 *   If possible, sample a new timestamp immediately and
 *   report it with the LWROC_TRACK_TIMESTAMP_FRESH flag.
 *   (This is not required, but allows a more constrained fit.)
 *
 * In summary:
 *
 * If any bit of LWROC_TRACK_TIMEST_RET_BAD is returned, then the
 * timestamp is very likely garbage.
 *
 * If the bit LWROC_TRACK_TIMEST_RET_REQ_FRESH is returned, you may
 * provide another immediately sampled timestamp with the flag
 * LWROC_TRACK_TIMESTAMP_FRESH to improve the next fit.  However,
 * the original timestamp is what should be used for the event!
 *
 * Normally, new samples for a fit are not accepted more than once per
 * second.  If no fit has been determined yet, this limit is 10 ms.
 * 16 values are used for fitting.  If the fit is to be forcefully
 * initialised on startup, it is suggested to report 20 samples, each
 * at least 20 ms apart (to avoid the filter ignoring them).
 */

int lwroc_report_event_timestamp(uint64_t stamp, int flags);

/* Please add 1 to the periodic value about once per second.  This
 * ensures new data for fits are collected even if a crazy fit result
 * was obtained earlier.
 */

extern volatile int _lwroc_timestamp_periodic;

#endif/*__LWROC_TRACK_TIMESTAMP_H__*/

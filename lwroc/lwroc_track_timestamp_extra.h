#ifndef __LWROC_TRACK_TIMESTAMP_EXTRA_H__
#define __LWROC_TRACK_TIMESTAMP_EXTRA_H__

/* This file intentionally does not include headers!
 *
 * Whoever includes this file is responsible to first include the
 * necessary headers.
 *
 * Candidates:

#include <stdint.h>
#include <sys/time.h>  // struct timeval

 */

/* It contains some additional structures that are needed by the
 * track/fit routines, but that need not be considered by the user,
 * therefore in a separate header.
 */

typedef struct lwroc_linear_fit_sol_t
{
  double y0;
  double dydx;

  double var_y0;
  double var_dydx;
  double cov_dydx_y0;
} lwroc_linear_fit_sol;

typedef struct lwroc_linear_fit_data_t
{
  double _x;
  double _y;
  int    _flags;
} lwroc_linear_fit_data;

#define LWROC_TIMESTAMP_FIT_CONSIDER   0x01
#define LWROC_TIMESTAMP_FIT_FIT1       0x02
#define LWROC_TIMESTAMP_FIT_FIT2       0x04

void lwroc_linear_fit_filter_fit(lwroc_linear_fit_sol *sol,
				 lwroc_linear_fit_data *input,
				 size_t n,
				 double *tmp_2array, /* 2*n elements. */
				 int include,
				 size_t *p_num_used);

typedef struct lwroc_timestamp_track_sol_t
{
  struct timeval x_ref;
  uint64_t       y_ref;

  lwroc_linear_fit_sol lfs;

  int min_udiff;

  int status; /* Set by user, not fit routine. */

} lwroc_timestamp_track_sol;

#define LWROC_TRACK_TIMESTAMP_HISTORY  16

struct lwroc_format_message_context_t;

typedef struct lwroc_timestamp_track_info_t
{
  uint64_t _prev_stamp;  /* Last time-stamp seen. */
  uint64_t _allow_stamp; /* We cheaply allow new stamps up to this. */

  /* Record a stamp-time pair for fitting after this time. */
  struct timeval _next_record;

  /* The currently used fit. */

  lwroc_timestamp_track_sol _ref;

  /* Current history, used for next fit. */

  struct
  {
    uint64_t       _stamp;
    struct timeval _time;
    int            _udiff; /* Boxed value by these many us before stamp. */
  } _v[LWROC_TRACK_TIMESTAMP_HISTORY];

  int _v_last; /* Last index added to history. */

  const char *_purpose; /* String describing us, e.g. "timestamp" or "NTP". */

  lwroc_format_message_context *_msg_context;

} lwroc_timestamp_track_info;

void lwroc_track_timestamp_filter_fit(lwroc_timestamp_track_info *track);

double lwroc_track_timestamp_fit_diff(lwroc_timestamp_track_sol *sol,
				      struct timeval *t,
				      uint64_t stamp);

int lwroc_compare_int(const void *p1, const void *p2);

int lwroc_compare_double(const void *p1, const void *p2);

#endif/*__LWROC_TRACK_TIMESTAMP_EXTRA_H__*/

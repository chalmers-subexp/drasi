/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Bastian Loeher  <b.loeher@gsi.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_triva.h"
#include "lwroc_triva_access.h"
#include "lwroc_triva_control.h"
#include "lwroc_message.h"

#include "../dtc_arch/acc_def/time_include.h"

#include <unistd.h>

char * lwroc_triva_decode_status(char *dest, uint32_t status)
{
  sprintf (dest,
	   "%s%sEC=%d %s%strig=%d", /* Spaces after non-empty strings! */
	   (status & TRIVA_STATUS_EON)      ? "EON " : "",
	   (status & TRIVA_STATUS_DI)       ? "DI " : "",
	   (status & TRIVA_STATUS_EC_MASK) >> TRIVA_STATUS_EC_SHIFT,
	   (status & TRIVA_STATUS_MISMATCH) ? "MISM " : "",
	   (status & TRIVA_STATUS_TDT)      ? "DT " : "",
	   (status & TRIVA_STATUS_TRIG_MASK));

  return dest;
}

void lwroc_triva_do_start_acq(void)
{
  int use_irq = 0;

  LWROC_INFO_FMT("START ACQ: HALT, CLEAR=RESET, MT=14, %s",
		 /*info->_*/use_irq ? "GO | EN_IRQ" : "GO");

  LWROC_TRIVA_WRITE(control, TRIVA_CONTROL_HALT);
  LWROC_TRIVA_WRITE(control, TRIVA_CONTROL_RESET);
  LWROC_TRIVA_WRITE(status, 14); /*info->_mt = 14;*/
  LWROC_TRIVA_WRITE(control,(uint32_t) (TRIVA_CONTROL_GO |
					(/*info->_*/use_irq ?
					 TRIVA_CONTROL_IRQ_ENABLE : 0)));
}

void lwroc_triva_do_halt_reset(void)
{
  if (!lwroc_triva_access)
    return;

  /* Do local halt. */
  LWROC_TRIVA_WRITE(control, TRIVA_CONTROL_HALT);
  LWROC_TRIVA_WRITE(control, TRIVA_CONTROL_RESET);
}

uint16_t lwroc_triva_try_take_trigger(uint32_t expect_ec)
{
  uint32_t status;
  uint32_t control;
  uint32_t ec;

  uint16_t got_trig = 0;

  if (1) /* not use irq */
    {
      status = LWROC_TRIVA_READ(status);
      /*control = LWROC_TRIVA_READ(control);*/

      if (status & TRIVA_STATUS_EON)
	{
	  /* Reset badbit counters.  Should only be done after issue found. */
	  /* triva->link_control = 0x8000; */
	  /*
	  LWROC_INFO_FMT("take trigger: %d [%d] (%04x) [%04x]",
			 status & 0x0f,(status >> 8) & 0x1f,
			 status & 0xffff,
			 control & 0xffff);
	  */
	  (void) control;

	  got_trig = (uint16_t) (status & TRIVA_STATUS_TRIG_MASK);
	}
      else
	{
	  /*
	  LWROC_INFO_FMT("(no trigger) (%04x) [%04x]",
			 status & 0xffff,control & 0xffff);
	  sleep(1);
	  */

	  /* sched_yield(); */

	  return 0;
	}
    }

  if (status & TRIVA_STATUS_EON)
    {
      if (status & TRIVA_STATUS_MISMATCH)
	{
	  uint32_t status2;
	  char tmp[128];

	  LWROC_ERROR_FMT("*** MISMATCH REPORTED *** "
			  "by TRIVA (status = 0x%04x: %s) "
			  "(expect EC=%d).",
			  status & 0xffff,
			  lwroc_triva_decode_status(tmp, status),
			  expect_ec);
	  got_trig = 0xffff;

	  /* Tell the user what a second read gives. */
	  usleep(100);

	  status2 = LWROC_TRIVA_READ(status);

	  LWROC_ERROR_FMT("Re-read status after mismatch "
			  "(status = 0x%04x: %s).",
			  status2 & 0xffff,
			  lwroc_triva_decode_status(tmp, status2));
	}
    }

  ec = (status & TRIVA_STATUS_EC_MASK) >> TRIVA_STATUS_EC_SHIFT;

  if (ec != expect_ec)
    {
      /* Double layer of if-statements to not have the check for
       * trigger 15 every event.
       */
      if (got_trig == 15)
	expect_ec = 0;
      if (ec != expect_ec &&
	  got_trig != 0xffff) /* Do not report EC mismatch on other error. */
	{
	  LWROC_ERROR_FMT("*** Trig = %d, "
			  "module EC = %d != expect EC = %d ***",
			  got_trig, ec, expect_ec);
	  got_trig = 0xffff;
	}
    }

  return got_trig;
}

int _lwroc_multi_word_trig_clear = 0;

void lwroc_triva_release_dt(void)
{
  if (!_lwroc_multi_word_trig_clear)
    {
      /* We by default set all the bits with one write access. */
      LWROC_TRIVA_WRITE(status,
			TRIVA_STATUS_EV_IRQ_CLEAR |
			TRIVA_STATUS_IRQ_CLEAR |
			TRIVA_STATUS_FC_PULSE |
			TRIVA_STATUS_DT_CLEAR);
    }
  else
    {
      /*LWROC_INFO_FMT("[%08x] (EV_)IRQ_CLEAR",triva->stat);*/
      LWROC_TRIVA_WRITE(status,
			TRIVA_STATUS_EV_IRQ_CLEAR |
			TRIVA_STATUS_IRQ_CLEAR);
      /*LWROC_INFO_FMT("[%08x] FC_PULSE",triva->stat);*/
      LWROC_TRIVA_WRITE(status,
			TRIVA_STATUS_FC_PULSE);
      /*LWROC_INFO_FMT("[%08x] DT_CLEAR",triva->stat);*/
      LWROC_TRIVA_WRITE(status,
			TRIVA_STATUS_DT_CLEAR);
      /*LWROC_INFO_FMT("[%08x] ready",triva->stat);*/
    }

  /* Do not set _lwroc_readout_meas_dt_after_dt_release,
   * as caller knows it is just releasing dt by itself.
   */
}

volatile uint16_t _lwroc_readout_trig;
volatile long     _lwroc_readout_released_dt;


void lwroc_triva_user_release_dt(void)
{
  if (_lwroc_readout_released_dt)
    {
      if (_lwroc_readout_trig == 15)
	return; /* No deadtime release. */

      /* We regard this as fatal as it is a bug in the user code,
       * which really should be fixed.  (If we would allow it through,
       * deadtime would be released twice, leading to all kinds of fun.)
       */
      LWROC_FATAL("Deadtime already released, "
		  "called by user function twice?");
    }

  /* HTJ: It is not clear to me why a f_user_trig_clear() shall not do
   * TRIVA_STATUS_FC_PULSE, while the clear after an event (if user
   * did not do it), does.
   */

  if (!_lwroc_multi_word_trig_clear)
    {
      LWROC_TRIVA_WRITE(status,
			TRIVA_STATUS_EV_IRQ_CLEAR |
			TRIVA_STATUS_IRQ_CLEAR |
			/*TRIVA_STATUS_FC_PULSE | */
			TRIVA_STATUS_DT_CLEAR);
    }
  else
    {
      LWROC_TRIVA_WRITE(status,
			TRIVA_STATUS_EV_IRQ_CLEAR |
			TRIVA_STATUS_IRQ_CLEAR);
      /* LWROC_TRIVA_WRITE(status, TRIVA_STATUS_FC_PULSE); */
      LWROC_TRIVA_WRITE(status,
			TRIVA_STATUS_DT_CLEAR);
    }

  _lwroc_readout_released_dt = LWROC_READOUT_DT_RELEASED;

  if (_lwroc_readout_measure_dt)
    gettimeofday(&_lwroc_readout_meas_dt_after_dt_release, NULL);
}

uint32_t lwroc_trimi_user_get_master_start_count(void)
{
  uint32_t link_status;
  uint32_t mscnt;

  link_status = LWROC_TRIVA_READ(link_status);

  mscnt = (link_status & TRIMI_LINK_STATUS_MSCNT) >>
    TRIMI_LINK_STATUS_MSCNT_SHIFT;

  return mscnt;
}

void lwroc_triva_user_headache(void)
{
  struct timeval now;

  /* This function is called from some user readout to report that
   * there are problems with the system in some way.  Although there
   * is nothing that we can do about it, we can at least flag the
   * status as something having a headache.  It is also likely that
   * the user routines are doing lengthy reset manoeuvres.  It is then
   * better that we report headache status, than some unexplained
   * lengthy deadtime.
   */

  gettimeofday(&now, NULL);

  if (now.tv_sec != _lwroc_readout_headache_time.tv_sec)
    {
      /* TODO:It would be a good idea to wake the control thread up,
       * to in turn inform the readout control (we may be a slave),
       * which informs the master that we have an situation.  That is
       * however quite a bunch of steps...  For the time being, we
       * rely on the regular status polling...
       */
    }

  /* We record when it happened, and for which event.
   * Then it is up to the status query routines to interpret this info.
   */
  _lwroc_readout_headache_event = _lwroc_readout_event_count;
  /* The update (and reading) of the time (it consists of multiple
   * words) is not atomic or ordered with anything.  It is anyhow only
   * used as a hint.
   */
  _lwroc_readout_headache_time = now;

  LWROC_WARNING("User called routine to report system headache.");
}

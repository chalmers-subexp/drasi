/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Bastian Loeher  <b.loeher@gsi.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_TRIVA_H__
#define __LWROC_TRIVA_H__

#include "triva_registers.h"

/********************************************************************/

/* From trig_cam.h */

#define WAIT_SEM        12
#define POLL_SEM        16

/* End trig_cam.h */

/********************************************************************/

char *lwroc_triva_decode_status(char *dest, uint32_t status);

void lwroc_init_triva(int master,int busena);
void lwroc_triva_do_start_acq(void);
void lwroc_triva_do_halt_reset(void);
uint16_t lwroc_triva_try_take_trigger(uint32_t expect_ec);
void lwroc_triva_release_dt(void);
void lwroc_triva_user_release_dt(void);
uint32_t lwroc_trimi_user_get_master_start_count(void);
void lwroc_triva_user_headache(void);

void lwroc_triva_setup_sim_pipe(const char *simulator);

/********************************************************************/

#define TRIMI_LINK_STATUS_MSCNT_MASK (TRIMI_LINK_STATUS_MSCNT >> \
				      TRIMI_LINK_STATUS_MSCNT_SHIFT)

/********************************************************************/

extern int _lwroc_multi_word_trig_clear;

extern volatile uint16_t _lwroc_readout_trig;
extern volatile long     _lwroc_readout_released_dt;

#define LWROC_READOUT_DT_RELEASED 0x10 /* same as TRIG__CLEARED */

/********************************************************************/

extern uint32_t *_lwroc_readout_dt_trace_ptr;

/********************************************************************/

#endif/*__LWROC_TRIVA_H__*/

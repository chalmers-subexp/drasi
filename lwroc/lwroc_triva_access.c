/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_triva_access.h"
#include "lwroc_message.h"
#include "lwroc_net_io_util.h"
#include "hwmap_access.h"
#include "hwmap_mapvme.h"

#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>
#include <pthread.h>

lwroc_triva_access_functions *lwroc_triva_access = NULL;

/********************************************************************/

volatile hwmap_opaque *lwroc_triva_direct_base = NULL;

uint32_t lwroc_triva_read_direct(size_t offset)
{
  uint32_t value;

  value = HWMAP_R32_D32(lwroc_triva_direct_base, (uint32_t) offset);

  return value;
}

void lwroc_triva_write_direct(size_t offset, uint32_t value)
{
  HWMAP_W32_D32(lwroc_triva_direct_base, (uint32_t) offset, value);
}

void lwroc_triva_serialize_io_direct(void)
{
  /* SERIALIZE_IO; */ /* TODO */
}

/********************************************************************/

lwroc_triva_access_functions lwroc_triva_access_direct =
{
  lwroc_triva_read_direct,
  lwroc_triva_write_direct,
  lwroc_triva_serialize_io_direct,
};

void lwroc_triva_access_set_direct(volatile hwmap_opaque *base)
{
  lwroc_triva_direct_base = base;
  lwroc_triva_access = &lwroc_triva_access_direct;
};

/********************************************************************/

#if HAS_CMVLC
struct cmvlc_client *lwroc_triva_access_get_cmvlc(void)
{
  return hwmap_get_cmvlc(lwroc_triva_direct_base);
}
#endif

/********************************************************************/

int lwroc_triva_sim_pipe_fd = -1;

/* If two threads are playing with the simulator, we need to lock...
 *
 * TODO: do we really want to allow this?  May have to reconsider
 * if there are real modules where we have such issues.
 */
pthread_mutex_t lwroc_triva_sim_mutex = PTHREAD_MUTEX_INITIALIZER;

uint32_t lwroc_triva_read_sim_pipe(size_t offset)
{
  uint32_t msg[1];
  uint32_t value;

  msg[0] = htonl(0x40000000 | (uint32_t) offset);

  /* Lock to ensure no one else read our response. */
  pthread_mutex_lock(&lwroc_triva_sim_mutex);

  /* Write the address. */
  lwroc_full_write(lwroc_triva_sim_pipe_fd, &msg, sizeof (msg),
		   "TRIVA sim pipe", 1);

  /* Read the data. */
  lwroc_full_read(lwroc_triva_sim_pipe_fd, &msg, sizeof (msg),
		  "TRIVA sim pipe", 1);

  pthread_mutex_unlock(&lwroc_triva_sim_mutex);

  value = ntohl(msg[0]);

  return value;
}

void lwroc_triva_write_sim_pipe(size_t offset, uint32_t value)
{
  uint32_t msg[2];

  msg[0] = htonl(0x80000000 | (uint32_t) offset);
  msg[1] = htonl(value);

  /* The writes are atomic (< PIPE_BUF), so need no locking. */

  /* Write the address and data. */
  lwroc_full_write(lwroc_triva_sim_pipe_fd, &msg, sizeof (msg),
		   "TRIVA sim pipe", 1);
}

void lwroc_triva_serialize_io_sim_pipe(void)
{
  /* To ensure that any (write) operations before us have completed,
   * we issue a dummy read.
   */

  lwroc_triva_read_sim_pipe(0xff);
}

/********************************************************************/

lwroc_triva_access_functions lwroc_triva_access_sim_pipe =
{
  lwroc_triva_read_sim_pipe,
  lwroc_triva_write_sim_pipe,
  lwroc_triva_serialize_io_sim_pipe,
};

void lwroc_triva_access_set_sim_pipe(int fd)
{
  lwroc_triva_sim_pipe_fd = fd;
  lwroc_triva_access = &lwroc_triva_access_sim_pipe;
};

/********************************************************************/

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_TRIVA_ACCESS_H__
#define __LWROC_TRIVA_ACCESS_H__

#include "triva_registers.h"
#include <stdlib.h>
#include <stddef.h>
#include "hwmap_access.h"

/********************************************************************/

/* In order to unify access to a TRIVA module, either it is via direct
 * linear memory mapped VME, or via direct but non-linear memory
 * mapped CAMAC, yet another bus or even a triva simulator via a pipe,
 * the read and write register accesses are wrapped in macros.  The
 * macros in turn wrap any function calls needed to perform the access.
 *
 * Another questions is if one should have a global triva, or carry
 * the base pointer around all the time such that multiple trivas can
 * be handled.  Well, we have no reason to deal with multiple trivas,
 * so may as well use a global.  (Knowing that globals are not super
 * nice, this is however a conscious design choice.  Let's try that for
 * the moment.)
 *
 * Another issue is how to give the offsets.  The author prefers
 * names, that can be checked.  Therefore using a structure and the
 * offsetof in the macro.  For the TRIMI, which is rather dynamic, we
 * however will probably also need some raw offset access capability.
 */

typedef uint32_t (*lwroc_triva_read_func )(size_t offset);
typedef void     (*lwroc_triva_write_func)(size_t offset,uint32_t value);
typedef void     (*lwroc_triva_serialize_io_func)(void);

typedef struct lwroc_triva_access_functions_t
{
  lwroc_triva_read_func          read;
  lwroc_triva_write_func         write;
  lwroc_triva_serialize_io_func  serialize_io;

} lwroc_triva_access_functions;

extern lwroc_triva_access_functions *lwroc_triva_access;

/********************************************************************/

#define LWROC_TRIVA_READ(member)					\
  lwroc_triva_access->read(offsetof(lwroc_triva_reg_layout,member))
#define LWROC_TRIVA_WRITE(member,value)					\
  lwroc_triva_access->write(offsetof(lwroc_triva_reg_layout,member),value)
#define LWROC_TRIVA_SERIALIZE_IO		       			\
  lwroc_triva_access->serialize_io()

/********************************************************************/

void lwroc_triva_access_set_direct(volatile hwmap_opaque *base);

void lwroc_triva_access_set_sim_pipe(int fd);

/********************************************************************/

#if HAS_CMVLC
struct cmvlc_client *lwroc_triva_access_get_cmvlc(void);
#endif

/********************************************************************/

#endif/*__LWROC_TRIVA_ACCESS_H__*/

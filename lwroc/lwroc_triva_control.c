/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_triva_control.h"
#include "lwroc_readout.h"
#include "lwroc_message.h"
#include "lwroc_message_internal.h"
#include "lwroc_net_io_util.h"
#include "lwroc_select_util.h"
#include "lwroc_data_pipe.h"
#include "lwroc_delayed_eb.h"

#include "lwroc_triva.h"
#include "lwroc_triva_access.h"

#include <pthread.h>
#include <string.h>
#include <errno.h>

/* Should be in lwroc_readout.c */
lwroc_thread_instance *_lwroc_readout_thread = NULL;

/* We are the partner thread to the readout.  We are responsible for
 * the communication with the master process, as well as the readout.
 *
 * We perform the TRIVA initialisation, as well as trigger handling
 * during startup.  Since we are running in the readout process, that
 * we want to keep as simple as possible, we do not really do anything
 * fancy.  We just wait for requests from the master and carry them
 * out.
 *
 * In order to not handle any timeouts, we just respond to requests
 * from the master.  We can always (if at all possible) carry out the
 * request immediately and respond.  This also includes the machinery
 * to detect stuck system.  As we anyhow shall answer periodic
 * messages about the number of triggers taken, that will also serve
 * as a basis for the master to determine if the system might be
 * stuck, and if some action needs to be taken.
 */

lwroc_thread_instance *_lwroc_triva_control_thread = NULL;

volatile int _lwroc_readout_phase = TRIVA_READOUT_PHASE_SETUP;
volatile int _lwroc_readout_expect_trig = 0;
volatile uint64_t _lwroc_readout_event_count = 1; /* Start event count at 1. */
volatile uint32_t _lwroc_readout_status = 0;
volatile uint32_t _lwroc_eb_ident[3] = { 0, 0, 0 };

volatile uint64_t _lwroc_readout_headache_event = 0;
volatile struct timeval _lwroc_readout_headache_time = { 0, 0 };

volatile int      _lwroc_readout_measure_dt = 0;
volatile uint32_t _lwroc_readout_meas_dt_events = 0;
volatile uint32_t _lwroc_readout_meas_dt_us_poll = 0;
volatile uint32_t _lwroc_readout_meas_dt_us = 0;
volatile uint32_t _lwroc_readout_meas_dt_us_postproc;
struct timeval _lwroc_readout_meas_dt_after_dt_release;

void lwroc_triva_control_thread_loop(lwroc_thread_instance *inst);

void lwroc_triva_control_setup(void)
{
  int i;

  /* Pipes for communication with the readout process (control thread). */

  for (i = 0; i < 2; i++)
    {
      if (pipe(_lwroc_triva_control_pipe[i]) != 0)
	{
	  LWROC_PERROR("pipe");
	  LWROC_FATAL("Failed to create control <-> readout pipe.");
	}
    }
  /*
  printf ("triva control pipes: %d %d %d %d\n",
	  _lwroc_triva_control_pipe[0][0],
	  _lwroc_triva_control_pipe[0][1],
	  _lwroc_triva_control_pipe[1][0],
	  _lwroc_triva_control_pipe[1][1]);
  fflush(stdout);
  */
}

/********************************************************************/

lwroc_thread_info _triva_control_thread_info =
{
  NULL,
  lwroc_triva_control_thread_loop,
  LWROC_MSG_BUFS_TRIVA_CTRL,
  "triva control",
  LWROC_THREAD_NO_RETURN,
  LWROC_THREAD_TERM_TRIVA_CTRL,
  LWROC_THREAD_CORE_PRIO_OTHER,
};

/********************************************************************/

void lwroc_triva_control_prepare_thread(void)
{
  _lwroc_triva_control_thread =
    lwroc_thread_prepare(&_triva_control_thread_info);
  lwroc_thread_also_wakeup_on_term(_lwroc_main_thread,
				   _lwroc_triva_control_thread);
}

void lwroc_triva_control_send_message(lwroc_triva_control_response *response)
{
  /* Not much to do - just send message. */
  /*
  LWROC_INFO_FMT("TRIVA control send message 0x%04x", response->_type);
  */

  if (_lwroc_readout_thread->_terminate)
    {
      response->status |= LWROC_READOUT_STATUS_TERM_REQ;
      /* printf ("control response |= ..._TERM_REQ\n"); */
    }

  lwroc_full_write(_lwroc_triva_control_pipe[TRIVA_CONTROL_PIPE_RESPONSE][1],
		   response, sizeof (*response),
		   "triva ctrl pipe response", 1);
}

int lwroc_triva_readout_phase_wait(int wait_while,
				   int wait_for,
				   lwroc_thread_instance *thread)
{
  int phase;

  /*
  LWROC_LOG_FMT("Wait while: 0x%x, wait for 0x%x.",
		wait_while, wait_for);
  */

  for ( ; ; )
    {
      phase = _lwroc_readout_phase;

      if (phase != wait_while)
	break;

      if (thread->_terminate)
	return 0;
      /* Wait for token. */
      lwroc_thread_block_get_token(thread->_block);
      /* Must check again, token may have been gotten for other
       * reason.
       */
      MFENCE;
    }

  if (!(wait_for & phase))
    LWROC_BUG_FMT("Thread %s waiting for readout phase mask 0x%x, got 0x%x.",
		  thread->_info->_purpose, wait_for, phase);

  /*
  LWROC_LOG_FMT("Waited, got 0x%x (of 0x%x).", phase, wait_for);
  */

  return 1;
}

int lwroc_triva_control_get_message(lwroc_triva_control_msg *msg,
				    uint32_t expect_type_mask)
{
  /* Fetch a message from the main process.
   *
   * Messages are passed in a pipe, i.e. cannot get lost.
   *
   * If a message is unexpected, we are out of sync and have a bug.
   * We verify that each message is expected.
   *
   * We are also responsible for handling the ABORT_TEST message, in
   * which case we return 0 to indicate that things should start over
   * from the beginning.
   */

  /* While we wait for the main process to send some message / status
   * query, the readout may have detected a mismatch.  In that case,
   * i.e. if we are not currently having an exchange with the master,
   * send a small wake-up to the master, such that it can issue a
   * status request soon, to get to know about the problem.
   */

  for ( ; ; )
    {
      lwroc_select_info si;
      int ret;

      lwroc_select_info_clear(&si);

      LWROC_READ_FD_SET(_lwroc_triva_control_pipe
			[TRIVA_CONTROL_PIPE_MESSAGE][0], &si);
      lwroc_thread_block_setup_select(_lwroc_triva_control_thread->_block,
				      &si);

      ret = select(si.nfd+1,&si.readfds,&si.writefds,NULL,NULL);

      if (ret == -1)
	{
	  if (errno == EINTR)
	    continue;

	  LWROC_PERROR("select");
	  exit(1);
	}

      if (LWROC_READ_FD_ISSET(_lwroc_triva_control_pipe
			      [TRIVA_CONTROL_PIPE_MESSAGE][0], &si))
	break; /* There is data to read from main process. */

      /* Check if there was a token, and if so discard it. */
      lwroc_thread_block_has_token_eat(_lwroc_triva_control_thread->_block,
				       &si);

      if ((_lwroc_readout_status & LWROC_READOUT_STATUS_MISMATCH) ||
	  _lwroc_readout_thread->_terminate == 1)
	{
	  lwroc_triva_control_response response;

	  /* Send only once. */
	  if (_lwroc_readout_thread->_terminate)
	    _lwroc_readout_thread->_terminate = 2;

	  /* printf ("control send LAM.\n"); */

	  /* Send a dummy message (wake-up token) to main process. */
	  LWROC_TRIVA_CTRL_MSG_PREPARE(&response,
				       LWROC_TRIVA_CONTROL_LOOK_AT_ME);

	  lwroc_triva_control_send_message(&response);
	  /* One token is enough - go wait for message. */
	  break;
	}
    }

  lwroc_full_read(_lwroc_triva_control_pipe[TRIVA_CONTROL_PIPE_MESSAGE][0],
		  msg, sizeof (*msg),
		  "triva ctrl pipe message", 1);
  /*
  LWROC_INFO_FMT("TRIVA control get message 0x%04x (expect 0x%04x)",
		 msg->_type, expect_type_mask);
  */
  if (!LWROC_HAS_ONE_EXPECTED_BIT(msg->_type,expect_type_mask))
    {
      lwroc_triva_control_response response;

      /* We have gotten out of sync with the master process.
       * This is a bug.
       * Report the failure to the master, then bug out.
       */

      LWROC_TRIVA_CTRL_MSG_PREPARE(&response,
				   LWROC_TRIVA_CONTROL_DESYNCHRONIZED);

      lwroc_triva_control_send_message(&response);

      /* This will hang permanently!  (For debugging.) */
      LWROC_BUG_FMT("TRIVA control got unexpected message (0x%x) "
		    "from main process, expect mask 0x%x.",
		    msg->_type, expect_type_mask);
    }

  if (msg->_type == LWROC_TRIVA_CONTROL_ABORT_TEST)
    return 0;

  return 1;
}

void lwroc_timing_test_read_write(int *rd_ns, int *wr_ns)
{
  int num = 10;

#define CALC_ELAPSED(i)						\
  elapsed = (int) (b.tv_sec - a.tv_sec) * 1000000 +		\
    (int) (b.tv_usec - a.tv_usec);				\
  if (elapsed < min_elapsed[i])					\
    min_elapsed[i] = elapsed;

  for (num = 10; num < 1000; num *= 2)
    {
      struct timeval a, b;
      int i, j;
      int elapsed;
      int min_elapsed[3] = { 1000000, 1000000, 1000000 };
      uint32_t dummy_sum = 0;
      (void)dummy_sum;

      for (i = 0; i < 5; i++)
	{
	  gettimeofday(&a,NULL);
	  gettimeofday(&b,NULL);
	  CALC_ELAPSED(0);

	  gettimeofday(&a,NULL);
	  for (j = 0; j < num; j++)
	    {
	      LWROC_TRIVA_WRITE(ctime,0x8765);
	      LWROC_TRIVA_SERIALIZE_IO; /* Make sure write is issued. */
	    }
	  gettimeofday(&b,NULL);
	  CALC_ELAPSED(1);

	  gettimeofday(&a,NULL);
	  for (j = 0; j < num; j++)
	    {
	      dummy_sum += LWROC_TRIVA_READ(ctime);
	    }
	  gettimeofday(&b,NULL);
	  CALC_ELAPSED(2);
	}

      /* Subtract the time for gettimeofday() overhead.
       * Multiply from us to ns, divide by number of accesses.
       */
      *rd_ns = ((min_elapsed[2] - min_elapsed[0]) * 1000) / num;
      *wr_ns = ((min_elapsed[1] - min_elapsed[0]) * 1000) / num;

      /*
      LWROC_ACTION_FMT("Elapsed: %d %d %d (num=%d)\n",
		       min_elapsed[0], min_elapsed[1], min_elapsed[2], num);
      */

      /* Measure for at least 1 ms each. */
      if (min_elapsed[1] > 1000 && min_elapsed[2] > 1000)
	break;
      if (min_elapsed[1] < 100 && min_elapsed[2] < 100)
	num *= 5;
    }
}

extern lwroc_monitor_main_block  _lwroc_mon_main;

void lwroc_triva_control_thread_loop(lwroc_thread_instance *inst)
{
  lwroc_triva_control_msg msg;
  lwroc_triva_control_response response;
  uint32_t master;
  int test_triggers;
  uint32_t trig_check_delay_us;
  /* These are just for fun-facts. */
  int num_wr;
  int rd_ns, wr_ns;
  int min_event_time_ns;
  double max_rate;
  int measure_dt_cycle = 0;
  int run_use_irq = /*info->_*//*use_irq*/0;

#if HAS_CMVLC
  run_use_irq = 1;
#endif

  (void) inst;

  lwroc_msg_wait_any_msg_client(_lwroc_triva_control_thread->_block);

  {
    startup:
      /*************************************************************/
      /* We are in STARTUP state.  Wait for request to initialise. */

      LWROC_LOG("TRIVA control: startup (wait for init request).");

      if (!lwroc_triva_control_get_message(&msg,
					   LWROC_TRIVA_CONTROL_INITIALISE))
	goto startup;

      /* Wait for the readout to report ready. */

      lwroc_triva_readout_phase_wait(TRIVA_READOUT_PHASE_SETUP,
				     TRIVA_READOUT_PHASE_READY,
				     _lwroc_triva_control_thread);

      /* Initialise the TRIVA. */

      _lwroc_readout_expect_trig = -1; /* Readout should not do anything! */

      _lwroc_readout_measure_dt = 0;

      LWROC_INFO_FMT("Setup TRIVA  (%s, HALT, %s, RESET)",
		     msg./*_u._init.*/bus_enable ? "ENABUS" : "DISBUS",
		     msg./*_u._init.*/master     ? "MASTER" : "SLAVE");
      master = msg./*_u._init.*/master;

      LWROC_TRIVA_WRITE(control, (uint32_t) (msg./*_u._init.*/bus_enable ?
					     TRIVA_CONTROL_BUS_ENABLE :
					     TRIVA_CONTROL_BUS_DISABLE));
      LWROC_TRIVA_WRITE(control, TRIVA_CONTROL_HALT);
      LWROC_TRIVA_WRITE(control, (uint32_t) (msg./*_u._init.*/master ?
					     TRIVA_CONTROL_MASTER :
					     TRIVA_CONTROL_SLAVE));
      LWROC_TRIVA_WRITE(control, TRIVA_CONTROL_RESET);

      /* Do timing test of reading and writing, to be able to estimate
       * minimum readout time per event.
       */
      lwroc_timing_test_read_write(&rd_ns, &wr_ns);

      /* Sanitize values. */
      if (msg./*_u._init.*/fctime > 0xffff)
	msg./*_u._init.*/fctime = 0xffff;
      if (msg./*_u._init.*/fctime < 1)
	msg./*_u._init.*/fctime = 1;
      if (msg./*_u._init.*/ctime > 0xffff)
	msg./*_u._init.*/ctime = 0xffff;
      if (msg./*_u._init.*/ctime < 1)
	msg./*_u._init.*/ctime = 1;

      LWROC_LOG_FMT("Set FCAtime to %d and Ctime to %d...",
		    msg./*_u._init.*/fctime, msg./*_u._init.*/ctime);

      LWROC_TRIVA_WRITE(fcatime, 0x10000 - msg./*_u._init.*/fctime);
      LWROC_TRIVA_WRITE(ctime,   0x10000 - msg./*_u._init.*/ctime);

      trig_check_delay_us =
	(msg./*_u._init.*/fctime + msg./*_u._init.*/ctime) / 10 + 1;

      /* Calculate minimum event time / corresponding maximum rate. */

      num_wr = _lwroc_multi_word_trig_clear ? 3 : 1;

      min_event_time_ns =
	(int) msg./*_u._init.*/ctime * 100 +
	1 * rd_ns + num_wr * wr_ns +
	(int) msg./*_u._init.*/fctime * 100;
      max_rate = 1.e9 / (min_event_time_ns);

      LWROC_INFO_FMT("Minimum event time "
		     "ctime(%d)+1*rd(%d)+%d*wr(%d)+fctime(%d)=%d ns "
		     "(%.3f kHz).",
		     msg./*_u._init.*/ctime * 100,
		     rd_ns, num_wr, wr_ns,
		     msg./*_u._init.*/fctime * 100,
		     min_event_time_ns,
		     max_rate * 1.e-3);

      LWROC_LOG("Clearing TRIVA... (HALT, DIS_IRQ, CLEAR=RESET)");

      LWROC_TRIVA_WRITE(control, TRIVA_CONTROL_HALT);
      LWROC_TRIVA_WRITE(control, TRIVA_CONTROL_IRQ_DISABLE);
      LWROC_TRIVA_WRITE(control, TRIVA_CONTROL_RESET);

      LWROC_TRIVA_SERIALIZE_IO;

      /* Report. */

      LWROC_TRIVA_CTRL_MSG_PREPARE(&response,
				   LWROC_TRIVA_CONTROL_INITIALISE);

      if (master)
	response.event_counter = _lwroc_readout_event_count;
      else
	_lwroc_readout_event_count = msg.event_counter;

      _lwroc_readout_headache_event = 0;
      timerclear(&_lwroc_readout_headache_time);

      lwroc_triva_control_send_message(&response);

      /* Wait for the request to inject ID message. */

      if (!lwroc_triva_control_get_message(&msg,
					   LWROC_TRIVA_CONTROL_INJECT_IDENT |
					   LWROC_TRIVA_CONTROL_ABORT_TEST))
	goto abort_test;

    inject_ident:
      /* Tell the readout thread to inject the ID message. */

      _lwroc_eb_ident[0] = msg._eb_ident0;
      _lwroc_eb_ident[1] = msg._eb_ident1;
      _lwroc_eb_ident[2] = msg._eb_ident2;
      MFENCE;
      _lwroc_readout_phase = TRIVA_READOUT_PHASE_INJECT;
      MFENCE;
      /* Send a token to wake it up! */
      lwroc_thread_block_send_token(_lwroc_readout_thread->_block);

      /* Wait for the readout to report marked. */

      lwroc_triva_readout_phase_wait(TRIVA_READOUT_PHASE_INJECT,
				     TRIVA_READOUT_PHASE_INJECTED,
				     _lwroc_triva_control_thread);

      /* Report. */

      LWROC_TRIVA_CTRL_MSG_PREPARE(&response,
				   LWROC_TRIVA_CONTROL_INJECT_IDENT);

      lwroc_triva_control_send_message(&response);

      /*************************************************************/
      /* We are INITIALIZED.  Wait for request to go to test mode. */

      LWROC_LOG("TRIVA control: initialised (wait for test request).");

      if (!lwroc_triva_control_get_message(&msg,
					   LWROC_TRIVA_CONTROL_INJECT_IDENT |
					   LWROC_TRIVA_CONTROL_GO_TEST |
					   LWROC_TRIVA_CONTROL_ABORT_TEST))
	goto abort_test;

      if (msg._type == LWROC_TRIVA_CONTROL_INJECT_IDENT)
	goto inject_ident;

    do_test:
      test_triggers = 0;

      /* We are in HALT since before, but IRQs may be enabled for CMVLC.
       * Get rid of any IRQ mode before any test trigger.
       */
      LWROC_LOG("START TEST: DIS_IRQ");
      LWROC_TRIVA_WRITE(control, TRIVA_CONTROL_IRQ_DISABLE);

      /* Set to GO mode. */

      if (master)
	{
	  LWROC_INFO_FMT("START TEST ACQ: HALT, CLEAR=RESET, MT=%d", msg._trig);
	  /* This RESET is needed to reset any slaves.  They were
	   * initialised since we did our init above.
	   */
	  LWROC_TRIVA_WRITE(control, TRIVA_CONTROL_HALT);
	  LWROC_TRIVA_WRITE(control, TRIVA_CONTROL_RESET);
	  if (msg._trig)
	    LWROC_TRIVA_WRITE(status, msg._trig);
	}
      if (!master || msg._trig) /* Slave, or master issued test trigger. */
	{
	  LWROC_ACTION(/*info->_*//*use_irq*/0 ?
		       "TEST: GO | EN_IRQ" : "TEST: GO");
	  LWROC_TRIVA_WRITE(control,
			    (uint32_t) (TRIVA_CONTROL_GO |
					(/*info->_*//*use_irq*/0 ?
					 TRIVA_CONTROL_IRQ_ENABLE : 0)));
	}

      LWROC_TRIVA_SERIALIZE_IO;
      usleep(trig_check_delay_us); /* Wait such that trigger gets through. */

      {
	uint32_t status;
	char tmp[128];

	status = LWROC_TRIVA_READ(status);

	LWROC_LOG_FMT("status = 0x%08x: %s",
		      status, lwroc_triva_decode_status(tmp, status));
      }

      /* Report. */

      LWROC_TRIVA_CTRL_MSG_PREPARE(&response,
				   LWROC_TRIVA_CONTROL_GO_TEST);

      lwroc_triva_control_send_message(&response);

      for ( ; ; )
	{
	  uint32_t status;
	  uint32_t ec;
	  uint32_t resp_status = 0;
	  uint32_t accept_release_DT;

	  /*********************************************************/
	  /* We are in GO TEST.  Wait for request to check trigger.*
	   * alternatively: request to go to full RUN mode.        */

	  if (!lwroc_triva_control_get_message(&msg,
					       LWROC_TRIVA_CONTROL_GET_TRIG |
					       LWROC_TRIVA_CONTROL_GO_RUN |
					       LWROC_TRIVA_CONTROL_ABORT_TEST))
	    goto abort_test;

	  if (msg._type == LWROC_TRIVA_CONTROL_GO_RUN)
	    break;

	get_trig:
	  /* Get trigger. */

	  LWROC_TRIVA_CTRL_MSG_PREPARE(&response,
				       LWROC_TRIVA_CONTROL_GET_TRIG);

	  /* If we are a slave, we might have a longer CTIME than
	   * master.  Just to make sure, we wait again (with our
	   * delay).
	   */
	  usleep(trig_check_delay_us);

	  status = LWROC_TRIVA_READ(status);
	  /*
	  LWROC_LOG_FMT("(get_trig) status = 0x%08x, control = 0x%08x", status,
			LWROC_TRIVA_READ(control));
	  */
	  if (status & TRIVA_STATUS_MISMATCH)
	    {
	      uint32_t status2;
	      char tmp[128];

	      LWROC_ERROR_FMT("TRIVA reported mismatch "
			      "(status = 0x%04x: %s).",
			      status & 0xffff,
			      lwroc_triva_decode_status(tmp, status));

	      resp_status |= LWROC_READOUT_STATUS_MISMATCH;

	      /* Tell the user what a second read gives. */
	      usleep(100);

	      status2 = LWROC_TRIVA_READ(status);

	      LWROC_ERROR_FMT("Re-read status after mismatch "
			      "(status = 0x%04x: %s).",
			      status2 & 0xffff,
			      lwroc_triva_decode_status(tmp, status2));
	    }

	  if (!(status & TRIVA_STATUS_DT_CLEAR/*TDT*/))
	    {
	      char tmp[128];

	      LWROC_ERROR_FMT("TRIVA status DT not set "
			      "(status = 0x%04x: %s).",
			      status & 0xffff,
			      lwroc_triva_decode_status(tmp, status));

	      resp_status |= LWROC_READOUT_STATUS_MISSING_DT;
	    }

	  ec = (status & TRIVA_STATUS_EC_MASK) >> TRIVA_STATUS_EC_SHIFT;
	  /* We cannot check the event counter locally, since we do
	   * not know if we are before or after last DT release.  This
	   * check must be done by the triva state machine that knows
	   * the procedure.
	   */

	  response./*_u._trig.*/_trig =
	    (uint16_t) (status & TRIVA_STATUS_TRIG_MASK);
	  response.event_counter = ec;

	  response./*_u._trig.*/status = resp_status;

	  /* Report. */

	  lwroc_triva_control_send_message(&response);

	  /* If we reported some kind of mismatch, we will not accept
	   * a release DT message.  So master has to abort in this case.
	   */

	  accept_release_DT = (resp_status ? 0 :
			       LWROC_TRIVA_CONTROL_RELEASE_DT);

	  /*********************************************************/
	  /* Wait for request to release DT. */

	  if (!lwroc_triva_control_get_message(&msg,
					       accept_release_DT |
					       LWROC_TRIVA_CONTROL_GET_TRIG |
					       LWROC_TRIVA_CONTROL_GO_RUN |
					       LWROC_TRIVA_CONTROL_ABORT_TEST))
	    goto abort_test;

	  if (msg._type == LWROC_TRIVA_CONTROL_GET_TRIG)
	    goto get_trig;

	  if (msg._type == LWROC_TRIVA_CONTROL_GO_RUN)
	    break;

	  /* If we are master, we must set next trigger before
	   * releasing deadtime, such that front-panel triggers do not
	   * get through.
	   */

	  if (master && msg._trig)
	    {
	      /*LWROC_INFO_FMT("TEST: MT=%d", msg._trig);*/
	      LWROC_TRIVA_WRITE(status, msg._trig);
	    }

	  /* Release deadtime. */

	  /*LWROC_INFO("TEST: (EV_)IRQ_CLEAR, FC_PULSE");*/
	  LWROC_TRIVA_WRITE(status,
			    TRIVA_STATUS_EV_IRQ_CLEAR |
			    TRIVA_STATUS_IRQ_CLEAR);
	  LWROC_TRIVA_WRITE(status, TRIVA_STATUS_FC_PULSE);
	  if (master && !msg._trig)
	    {
	      /*LWROC_INFO("TEST: HALT");*/
	      LWROC_TRIVA_WRITE(control, TRIVA_CONTROL_HALT);
	    }
	  /*LWROC_INFO("TEST: DT_CLEAR");*/
	  LWROC_TRIVA_WRITE(status, TRIVA_STATUS_DT_CLEAR);

	  LWROC_TRIVA_SERIALIZE_IO;

	  /* Report. */

	  LWROC_TRIVA_CTRL_MSG_PREPARE(&response,
				       LWROC_TRIVA_CONTROL_RELEASE_DT);

	  lwroc_triva_control_send_message(&response);
	  /*
	  LWROC_LOG_FMT("(after rel DT) status = 0x%08x, control = 0x%08x",
			LWROC_TRIVA_READ(status),
			LWROC_TRIVA_READ(control));
	  */
	  test_triggers++;

	  /*********************************************************/
	  /* Loop back, wait for next trigger...                   */
	}

      /*************************************************************/
      /* Tell readout to do preparation (MVLC stack setup). */

      LWROC_LOG("TRIVA control: initialised (prepare readout).");

      _lwroc_readout_phase = TRIVA_READOUT_PHASE_GO_PREP;
      MFENCE;
      /* Send a token to wake it up! */
      lwroc_thread_block_send_token(_lwroc_readout_thread->_block);
      /* Wait for it... */
      lwroc_triva_readout_phase_wait(TRIVA_READOUT_PHASE_GO_PREP,
				     TRIVA_READOUT_PHASE_PREPARED,
				     _lwroc_triva_control_thread);

      /* Prepare done! */

      /*************************************************************/
      /* Set to GO mode. */

      LWROC_LOG("TRIVA control: initialised (going for run).");

      _lwroc_delayed_eb_status = msg.delayed_eb_status;

      {
	uint32_t status;
	char tmp[128];

	status = LWROC_TRIVA_READ(status);

	LWROC_LOG_FMT("status = 0x%08x: %s",
		      status,
		      lwroc_triva_decode_status(tmp, status));

	/* Full check of event will happen in readout.
	 * This additional check just prints an error to aid debugging
	 * if something is rotten.
	 * Neither master nor slave should have trigger at this point.
	 */

	if (status & TRIVA_STATUS_EON)
	  LWROC_ERROR("Module told to go run, but trigger is present.");
      }

      /* Both master and slave shall expect trigger 14. */
      _lwroc_readout_expect_trig = 14;
      MFENCE;

      if (master)
	{
	  /* TODO: This RESET is only need on TRIVAs, since the 1-trigger
	   * testing brings the EC out of sync.
	   */
	  LWROC_INFO("RUN: RESET");
	  LWROC_TRIVA_WRITE(control, TRIVA_CONTROL_RESET);
	  usleep(trig_check_delay_us);

	  LWROC_INFO_FMT("RUN: MT=%d", 14);
	  LWROC_TRIVA_WRITE(status, 14);

	  if (0)
	    {
	      uint32_t status, ctrl;

	      status = LWROC_TRIVA_READ(status);
	      ctrl   = LWROC_TRIVA_READ(control);

	      LWROC_INFO_FMT("Status: 0x%08x Ctrl: 0x%08x", status, ctrl);
	    }

	  LWROC_ACTION_FMT(run_use_irq ?
			   "RUN: GO | EN_IRQ (%d good test triggers done) "
			   "(max %.1f kHz)" :
			   "  GO (%d good test triggers done) "
			   "(max %.1f kHz)",
			   test_triggers, max_rate * 1.e-3);
	  LWROC_TRIVA_WRITE(control,
			    (uint32_t) (TRIVA_CONTROL_GO |
					(run_use_irq ?
					 TRIVA_CONTROL_IRQ_ENABLE : 0)));

	  LWROC_TRIVA_SERIALIZE_IO;

	  if (0)
	    {
	      uint32_t status, ctrl;

	      status = LWROC_TRIVA_READ(status);
	      ctrl   = LWROC_TRIVA_READ(control);

	      LWROC_INFO_FMT("Status: 0x%08x Ctrl: 0x%08x", status, ctrl);
	    }
	}
      else
	{
	  LWROC_ACTION(run_use_irq ?
		       "RUN:" :
		       "RUN: EN_IRQ");
	  LWROC_TRIVA_WRITE(control,
			    (uint32_t) ((run_use_irq ?
					 TRIVA_CONTROL_IRQ_ENABLE : 0)));
	}

      /* Tell the readout thread it is in control. */

      _lwroc_readout_phase = TRIVA_READOUT_PHASE_GO_READ;
      MFENCE;
      /* Send a token to wake it up! */
      lwroc_thread_block_send_token(_lwroc_readout_thread->_block);
      /* Wait for it... */
      lwroc_triva_readout_phase_wait(TRIVA_READOUT_PHASE_GO_READ,
				     TRIVA_READOUT_PHASE_READOUT,
				     _lwroc_triva_control_thread);

      /* Report. */

      LWROC_TRIVA_CTRL_MSG_PREPARE(&response,
				   LWROC_TRIVA_CONTROL_GO_RUN);

      lwroc_triva_control_send_message(&response);

      LWROC_LOG("TRIVA control: running...");

      {
	uint32_t accept_acq_change = LWROC_TRIVA_CONTROL_ACQ_STOP;

      for ( ; ; )
	{
	  /*************************************************************/
	  /* We are in GO RUN mode.                                    */
	  /* We can return statistics.                                 */

	  lwroc_triva_control_get_message(&msg,
					  LWROC_TRIVA_CONTROL_RUN_STATS |
					  accept_acq_change |
					  LWROC_TRIVA_CONTROL_ABORT_READOUT);

	  switch (msg._type)
	    {
	    case LWROC_TRIVA_CONTROL_ACQ_STOP:
	      LWROC_INFO("TRIVA control: acq stop request received.");
	      /* We must prevent further triggers from happening. */
	      LWROC_ACTION("RUN: HALT");
	      LWROC_TRIVA_WRITE(control,
				TRIVA_CONTROL_HALT);
	      LWROC_TRIVA_SERIALIZE_IO;
#if 0
	      /* Once readout has dealt with the current trigger, it
	       * shall go to hold mode in anticipation of next step.
	       */
#endif
#if 0
	      _lwroc_readout_phase = TRIVA_READOUT_PHASE_HOLD;
	      MFENCE;
	      /* Send a token to wake it up! */
	      lwroc_thread_block_send_token(_lwroc_readout_thread->_block);
#endif
	      /* We do *not* wait for it to enter held mode, since
	       * that can take a very long time (if stalled due to
	       * buffer full on current trigger).
	       */
#if 0
	      /* Before we can issue trigger 15, we must know that the
	       * readout has handled all stuff up to now.
	       */
	      _lwroc_readout_phase = TRIVA_READOUT_PHASE_HOLD;
	      MFENCE;
	      /* Send a token to wake it up! */
	      lwroc_thread_block_send_token(_lwroc_readout_thread->_block);
	      /* Wait for it... */
	      lwroc_triva_readout_phase_wait(TRIVA_READOUT_PHASE_HOLD,
					     TRIVA_READOUT_PHASE_HELD,
					     _lwroc_triva_control_thread->_block,
					     "control");
	      LWROC_INFO("TRIVA control: run held - readout done.");
#endif
	      /* It can take ages before the readout manages to deal with
	       * the last trigger.  Send a response that we at least have
	       * initiated the stop procedure.
	       */
	      LWROC_TRIVA_CTRL_MSG_PREPARE(&response,
					   LWROC_TRIVA_CONTROL_ACQ_STOP);
	      lwroc_triva_control_send_message(&response);

	      accept_acq_change = LWROC_TRIVA_CONTROL_TRIG15;
	      continue;

	    case LWROC_TRIVA_CONTROL_TRIG15:
	      LWROC_ACTION("RUN: Inject trigger 15");
	      LWROC_TRIVA_WRITE(control, TRIVA_CONTROL_RESET);

	      _lwroc_readout_expect_trig = 15;
	      MFENCE;
	      LWROC_TRIVA_WRITE(status, 15);
	      LWROC_TRIVA_WRITE(control,
				(uint32_t) (TRIVA_CONTROL_GO |
					    (/*info->_*//*use_irq*/0 ?
					     TRIVA_CONTROL_IRQ_ENABLE:0)));
	      /* We did our job, injected the final trigger 15. */
	      LWROC_TRIVA_CTRL_MSG_PREPARE(&response,
					   LWROC_TRIVA_CONTROL_TRIG15);
	      lwroc_triva_control_send_message(&response);

	      /* Do not accept GO_TEST until the trigger 15 has been
	       * reported handled (state is HELD).
	       */
	      accept_acq_change = 0;
	      continue;

	    case LWROC_TRIVA_CONTROL_GO_TEST:
	      LWROC_INFO("TRIVA control: acq start (test) request received.");
	      goto do_test;

	    case LWROC_TRIVA_CONTROL_ABORT_READOUT:
	      LWROC_INFO("TRIVA control: run abort request received.");
	      goto abort_readout;
	    }

#if 0
	  if (insert_trig15 == 2 &&
	      (_lwroc_readout_status & LWROC_READOUT_STATUS_HELD))
	    {
	      _lwroc_readout_phase = TRIVA_READOUT_PHASE_HOLD;
		MFENCE;
	      /* Send a token to wake it up! */
	      lwroc_thread_block_send_token(_lwroc_readout_thread->_block);
	      insert_trig15 = 0;
	    }
#endif

	  _lwroc_delayed_eb_status = msg.delayed_eb_status;

	  if (msg.deadtime != LWROC_TRIVA_DEADTIME_MEASURE)
	    _lwroc_readout_measure_dt = 0;
	  else if (!_lwroc_readout_measure_dt)
	    {
	      /* The 0x40000000 just to ensure non-zero. */
	      _lwroc_readout_measure_dt = (++measure_dt_cycle) | 0x40000000;
	    }

	  /* Get statistics. */

	  /* Report. */

	  LWROC_TRIVA_CTRL_MSG_PREPARE(&response,
				       LWROC_TRIVA_CONTROL_RUN_STATS);

	  {
	    uint32_t status;

	    status = LWROC_TRIVA_READ(status);

	    response.deadtime = (status & TRIVA_STATUS_DT_CLEAR ? 1 : 0);
	    /* Hmmm, should we really reduce the event count? */
	    response.event_counter = (uint32_t) _lwroc_readout_event_count;

	    response.status = _lwroc_readout_status;

	    if (_lwroc_readout_status & LWROC_READOUT_STATUS_HELD)
	      {
		accept_acq_change |= LWROC_TRIVA_CONTROL_GO_TEST;
	      }

#if 0
	    if (_lwroc_readout_phase == TRIVA_READOUT_PHASE_HELD)
	      {
		/* response.status |= LWROC_READOUT_STATUS_HELD; */
		accept_acq_change = LWROC_TRIVA_CONTROL_GO_TEST;
	      }
#endif

	    if (timerisset(&_lwroc_readout_headache_time))
	      {
		/* printf ("HAS HEADACHE\n"); */
		if (_lwroc_readout_headache_event ==
		    _lwroc_readout_event_count)
		  response.status |= LWROC_READOUT_STATUS_HEADACHE_CUR;
		else
		  {
		    struct timeval now, recent;
		    struct timeval headache_time;

		    gettimeofday(&now,NULL);
		    recent = now;
		    recent.tv_sec -= 10;

		    headache_time = _lwroc_readout_headache_time;

		    if (timercmp(&headache_time,&now,<) &&
			timercmp(&headache_time,&recent,>))
		      response.status |= LWROC_READOUT_STATUS_HEADACHE;
		  }
		/* printf ("HAS HEADACHE -> %d\n",response.status); */
	      }

	    if (_lwroc_bug_fatal_reported)
	      {
		/* Some thread has reported bug or fatal, and is
		 * sleeping, hoping for a debugger.
		 */
		response.status |= LWROC_READOUT_STATUS_STHR_BUG_FATAL;
	      }

	    {
	      uint32_t meas_dt_events;
	      uint32_t meas_dt_events2;
	      uint32_t meas_dt_us;
	      uint32_t meas_dt_us_poll;
	      uint32_t meas_dt_us_postproc;
	      uint32_t val_ctime;

	      val_ctime = 0x10000 - (LWROC_TRIVA_READ(ctime) & 0xffff);
	      meas_dt_events = _lwroc_readout_meas_dt_events;
	      LFENCE;
	      meas_dt_us_poll     = _lwroc_readout_meas_dt_us_poll;
	      meas_dt_us          = _lwroc_readout_meas_dt_us;
	      meas_dt_us_postproc = _lwroc_readout_meas_dt_us_postproc;
	      LFENCE;
	      meas_dt_events2 = _lwroc_readout_meas_dt_events;

	      /* Did we manage the read without update in between,
	       * and also not during update?
	       */
	      if (meas_dt_events == meas_dt_events2 &&
		  !(meas_dt_events & 1))
		{
		  response._eb_ident0 = meas_dt_events;
		  response._eb_ident1 = meas_dt_us_poll;
		  response._eb_ident2 = meas_dt_us;
		  response._eb_ident3 = meas_dt_us_postproc;
		  /* High bit of ctime marks valid value. */
		  response.ctime = 0x80000000 | val_ctime;
		}

	      /* The low bit marks if a measurement (in principle) is
	       * ongoing.
	       * TODO: change to use the second-high bit of ctime.
	       * i.e., in lwroc_draw_mon_tree_meas_dt() .
	       */
	      response._eb_ident0 &= ~(uint32_t) 1;
	      response._eb_ident0 |=
		  _lwroc_readout_measure_dt ? 1 : 0;
	      response.ctime |=
		_lwroc_readout_measure_dt ? 0x40000000 : 0;
	    }

	    /*
	    printf ("Get readout stats: DT: %d  cnt: %" PRIu64 "  "
		    "status: 0x%x (0x%x) force: %d\n",
		    response.deadtime,
		    response.event_counter,
		    response.status,
		    _lwroc_readout_status,
		    msg.delayed_eb_status);
	    */
	  }

	  lwroc_triva_control_send_message(&response);
	}
      }

    abort_readout:
      /*************************************************************/
      /* Something bad has happened.  We have been requested to    *
       * tell the readout it is no longer in control.              */

      _lwroc_readout_phase = TRIVA_READOUT_PHASE_BREAKOUT;
      MFENCE;
      /* Send a token to wake it up! */
      lwroc_thread_block_send_token(_lwroc_readout_thread->_block);

      /* TODO: If the readout is waiting for trigger interrupt, but
       * gets none, we somehow have to interrupt that call.  Send a
       * trigger 15?
       */

      /* Wait for the readout thread to stop processing.  This can
       * take an arbitrary amount of time, but that does not matter.
       * Until it has exited its processing loop, there is nothing we
       * can do.  Except killing our process.  But that is up to
       * the main thread to decide.
       */

      LWROC_ACTION("TRIVA control: run aborted - waiting for readout...");

#if HAS_CMVLC
      /* Execute a MVLC stack that generates some flush data, to get
       * readout out of waiting for nothing.
       */
      lwroc_triva_flush_seq_cmvlc();
#endif

      lwroc_triva_readout_phase_wait(TRIVA_READOUT_PHASE_BREAKOUT,
				     TRIVA_READOUT_PHASE_BROKEOUT,
				     _lwroc_triva_control_thread);

      LWROC_INFO("TRIVA control: run aborted - readout done.");

      /* Report. */

      LWROC_TRIVA_CTRL_MSG_PREPARE(&response,
				   LWROC_TRIVA_CONTROL_ABORT_READOUT);

      lwroc_triva_control_send_message(&response);

      /*************************************************************/
      /* We do not do anything with the module, for debug purposes.*/
      /* Loop back to top! */
      goto set_phase_setup;

    abort_test:
      /* We end up here when we have gotten some partial way into the
       * readout, but master state machine have decided that we need
       * to cycle back to init.  Before we can cycle back, we must
       * ensure that the readout also gets back to init state.
       */

      /* We do nothing with the trigger bus, in case it is needed for
       * debugging.
       */

      /* Get the readout out of its sweet dreams. */
      _lwroc_readout_phase = TRIVA_READOUT_PHASE_ABORT_T;
      MFENCE;
      /* Send a token to wake it up! */
      lwroc_thread_block_send_token(_lwroc_readout_thread->_block);

      LWROC_INFO("TRIVA control: abort test.");

      lwroc_triva_readout_phase_wait(TRIVA_READOUT_PHASE_ABORT_T,
				     TRIVA_READOUT_PHASE_ABORTED,
				     _lwroc_triva_control_thread);

      /* Report. */

      LWROC_TRIVA_CTRL_MSG_PREPARE(&response,
				   LWROC_TRIVA_CONTROL_ABORT_TEST);

      lwroc_triva_control_send_message(&response);

      goto set_phase_setup;

    set_phase_setup:
      /* We cannot do this on first loop, since readout updates phase
       * as first action.
       */
      _lwroc_readout_phase = TRIVA_READOUT_PHASE_SETUP;
      MFENCE;
      /* Send a token to wake it up! */
      lwroc_thread_block_send_token(_lwroc_readout_thread->_block);

      goto startup;
    }
}

/* More careful insertation of trigger 15 (at stop)?
 *
 * The current method of waiting for a fixed time, and then resetting
 * the TRIVA/MI and then injecting trigger 15 runs the risk of some
 * slaves having handled a trigger which others have not.  We then end
 * up out of sync.
 *
 * This causes a spurious error, which however is detected by the
 * event builder and then recovered using the normal logic.
 *
 * One could imagine to try to do the injection such that this cannot
 * happen.  However, this is very difficult, due to software triggers
 * which may already have been injected by user code.  And even a
 * software trigger injected and hidden under a normal trigger that
 * has not yet been processed when the module is put into halt mode
 * (before the trigger 15 injection).
 *
 * By doing software trigger injection (only) by a function which
 * takes into consideration the state of this control and readout
 * routine, it could possibly be achieved, but with much complication
 * of the code which does not add any value to normal processing.
 * Since stop acq / trigger 15 is not suggested to be used anyhow (DAQ
 * always runs), we do not do this.
 *
 * If the user code uses the pending trigger of the TRLO II/TRIMI,
 * then these issues do not arise due to user code, as it does not
 * inject software triggers.  (It would be completed (water-proof) by
 * trigger 15 being injected as a pending trigger, and not a software
 * trigger.)
 */

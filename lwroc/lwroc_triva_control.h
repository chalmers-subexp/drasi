/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_TRIVA_CONTROL_H__
#define __LWROC_TRIVA_CONTROL_H__

#include "../dtc_arch/acc_def/mystdint.h"
#include "lwroc_net_proto.h"
#include "lwroc_thread_block.h"
#include "lwroc_thread_util.h"

/* The messages are defined as bitmasks, such that we easily can tell
 * the fetch routine which ones are valid at each point.
 */

#define LWROC_TRIVA_CONTROL_INITIALISE           0x0001

#define LWROC_TRIVA_CONTROL_INJECT_IDENT         0x0002

#define LWROC_TRIVA_CONTROL_GO_TEST              0x0004

#define LWROC_TRIVA_CONTROL_GET_TRIG             0x0008

#define LWROC_TRIVA_CONTROL_RELEASE_DT           0x0010

#define LWROC_TRIVA_CONTROL_ABORT_TEST           0x0020

#define LWROC_TRIVA_CONTROL_GO_RUN               0x0040

#define LWROC_TRIVA_CONTROL_RUN_STATS            0x0080

#define LWROC_TRIVA_CONTROL_ACQ_STOP             0x0100

#define LWROC_TRIVA_CONTROL_TRIG15              0x80000

#define LWROC_TRIVA_CONTROL_ABORT_READOUT        0x0200

#define LWROC_TRIVA_CONTROL_EB_INVALIDATE_IDENT  0x0400 /* M->EB; -> INV..ED */

#define LWROC_TRIVA_CONTROL_EB_INVALIDATED_IDENT 0x0800 /* EB->M; -> INV/QUE */

#define LWROC_TRIVA_CONTROL_EB_QUERY_IDENT       0x1000 /* M->EB; -> NO/ASK */

#define LWROC_TRIVA_CONTROL_EB_NO_IDENT          0x2000 /* EB->M; -> ANY */

#define LWROC_TRIVA_CONTROL_EB_ASK_IDENT         0x4000 /* EB->M; -> B/G */

#define LWROC_TRIVA_CONTROL_EB_BAD_IDENT         0x8000 /* M->EB; -> NO/ASK */

#define LWROC_TRIVA_CONTROL_EB_GOOD_IDENT       0x10000 /* M->EB; -> NO/ASK */

#define LWROC_TRIVA_CONTROL_EB_STATUS_QUERY     0x20000 /* M->EB; -> ST..RS */

#define LWROC_TRIVA_CONTROL_EB_STATUS_RESPONSE  0x40000 /* EB->M; -> INV/QUE */

/* Only return: tell state machine that something went real sour:
 * State machine (triva) not in lock-step with control.
 */
#define LWROC_TRIVA_CONTROL_DESYNCHRONIZED   0x80000000

/* Only return: tell state machine that something needs attention. */
#define LWROC_TRIVA_CONTROL_LOOK_AT_ME       0x40000000

/* This message is internal between the main and the readout process,
 * so we need never handle different versions.  However, as someone
 * may try to run with mixed compilations, we still need to verify
 * that they use the same structure and set of operations.
 *
 * We introduce a magic number, which SHALL be changed if one changes
 * the messages above, introduce new ones, modify the exchanged
 * data/structures or in any way change how it is interpreted.
 * To come up with a new one is easy, just run: date "+%s"
 */

#define LWROC_TRIVA_CONTROL_MAGIC            1529332314u

/* Since the triva control is between systems in one deadtime domain,
 * you really do not want to run with mixed versions of these setup
 * routines anyhow.  So just bite the sour apple and recompile all
 * systems within the DT domain if you need to change this!
 */

#define lwroc_triva_control_msg       lwroc_trigbus_msg
#define lwroc_triva_control_response  lwroc_trigbus_msg

#define LWROC_TRIVA_CTRL_MSG_PREPARE(msg, type) do { \
    memset((msg), 0, sizeof (*msg));		     \
    (msg)->_type = (type);			     \
    (msg)->_magic = LWROC_TRIVA_CONTROL_MAGIC;	     \
  } while (0)

#define TRIVA_CONTROL_PIPE_MESSAGE   0
#define TRIVA_CONTROL_PIPE_RESPONSE  1

extern int _lwroc_triva_control_pipe[2][2];

#define TRIVA_READOUT_PHASE_SETUP    0x001 /* control in charge */
#define TRIVA_READOUT_PHASE_READY    0x002 /* control in charge, readout ok */
#define TRIVA_READOUT_PHASE_INJECT   0x004 /* ctrl in charge, inject id msg */
#define TRIVA_READOUT_PHASE_INJECTED 0x008 /* ctrl in charge, inject id msg */
#define TRIVA_READOUT_PHASE_GO_PREP  0x010 /* go back to readout */
#define TRIVA_READOUT_PHASE_PREPARED 0x020 /* readout in charge */
#define TRIVA_READOUT_PHASE_GO_READ  0x040 /* go back to readout */
#define TRIVA_READOUT_PHASE_READOUT  0x080 /* readout in charge */
#define TRIVA_READOUT_PHASE_BREAKOUT 0x100 /* wait for readout to break loop */
#define TRIVA_READOUT_PHASE_BROKEOUT 0x200 /* readout broke out of loop */
#define TRIVA_READOUT_PHASE_ABORT_T  0x400 /* wait for readout to break test */
#define TRIVA_READOUT_PHASE_ABORTED  0x800 /* readout broke out of test */

/* SETUP    -> [by readout] READY
 * READY    -> [by control] INJECT
 * INJECT   -> [by readout] INJECTED
 * INJECTED -> [by control] GO_PREP or
 *             [by control] ABORT_T (on failure)
 * GO_PREP  -> [by readout] PREPARED
 * PREPARED -> [by control] GO_READ
 * GO_READ  -> [by readout] READOUT
 * READOUT  -> [by control] BREAKOUT or
 *             [by control] GO_PREP (while status HELD)
 * BREAKOUT -> [by readout] BROKEOUT
 * BROKEOUT -> [by control] SETUP
 * ABORT_T  -> [by readout] ABORTED
 * ABORTED  -> [by control] SETUP
 */

extern volatile int _lwroc_readout_phase;
extern volatile int _lwroc_readout_expect_trig;

#define LWROC_READOUT_STATUS_MISMATCH        0x0001u
#define LWROC_READOUT_STATUS_EC_MISMATCH     0x0002u
#define LWROC_READOUT_STATUS_TRIG_UNEXPECT   0x0004u
#define LWROC_READOUT_STATUS_SEQ_ERROR       0x0008u
#define LWROC_READOUT_STATUS_SEQ_UNEXPECT    0x0010u
#define LWROC_READOUT_STATUS_SEQ_MALFORM     0x0020u
#define LWROC_READOUT_STATUS_MISSING_DT      0x0040u
#define LWROC_READOUT_STATUS_IN_READOUT      0x0080u
#define LWROC_READOUT_STATUS_HEADACHE_CUR    0x0100u
#define LWROC_READOUT_STATUS_HEADACHE        0x0200u
#define LWROC_READOUT_STATUS_WAIT_BUFSPACE   0x0400u
#define LWROC_READOUT_STATUS_HELD            0x0800u
#define LWROC_READOUT_STATUS_TERM_REQ        0x1000u
#define LWROC_READOUT_STATUS_STHR_BUG_FATAL  0x2000u

extern volatile uint64_t _lwroc_readout_event_count;
extern volatile uint32_t _lwroc_readout_status;

extern volatile uint32_t _lwroc_eb_ident[3];

extern volatile uint64_t _lwroc_readout_headache_event;
extern volatile struct timeval _lwroc_readout_headache_time;

extern volatile int      _lwroc_readout_measure_dt;
extern volatile uint32_t _lwroc_readout_meas_dt_events;
extern volatile uint32_t _lwroc_readout_meas_dt_us_poll;
extern volatile uint32_t _lwroc_readout_meas_dt_us;
extern volatile uint32_t _lwroc_readout_meas_dt_us_postproc;
extern struct timeval _lwroc_readout_meas_dt_after_dt_release;

extern lwroc_thread_instance *_lwroc_triva_control_thread;

void lwroc_triva_control_setup(void);

void lwroc_triva_control_prepare_thread(void);

int lwroc_triva_readout_phase_wait(int wait_while,
				   int wait_for,
				   lwroc_thread_instance *thread);

#define LWROC_HAS_ONE_BIT(x) (!((x) & ((x) - 1)))

#define LWROC_HAS_ONE_EXPECTED_BIT(x,expect) \
  ((x) & (expect) && LWROC_HAS_ONE_BIT(x))

void lwroc_triva_flush_seq_cmvlc(void);

#endif/*__LWROC_TRIVA_CONTROL_H__*/

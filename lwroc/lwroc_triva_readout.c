/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_readout.h"
#include "lwroc_triva_readout.h"

#include "lwroc_main_iface.h"
#include "lwroc_parse_util.h"
#include "lwroc_data_pipe.h"
#include "lwroc_message.h"
#include "lwroc_mon_block.h"
#include "lwroc_delayed_eb.h"

#include "hwmap_mapvme.h"
#include "lwroc_triva.h"
#include "lwroc_triva_access.h"
#include "lwroc_triva_control.h"
#include "lwroc_triva_state.h"
#include "lwroc_triva_kind.h"
#include "lwroc_map_pexor.h"

#include <sched.h>
#include <string.h>

#include "../dtc_arch/acc_def/myprizd.h"

#include "lmd/lwroc_lmd_event.h"

#if HAS_CMVLC
#include "cmvlc.h"
/*#include "../src/mvlc_const.h"*/
#include "cmvlc_stackcmd.h"
#include "cmvlc_supercmd.h"
#endif

extern lwroc_monitor_main_block  _lwroc_mon_main;
extern lwroc_mon_block          *_lwroc_mon_main_handle;

extern struct lwroc_readout_functions _lwroc_readout_functions;

#define TRIVA_MAP_LENGTH   0x10000

void lwroc_triva_setup(void)
{
  LWROC_LOG_FMT("TRIVA: %s", _config._triva);

  /* The TRIVA mapping must be arranged before we create the
   * control thread.
   */

  lwroc_triva_map();

  /* Take control of the TRIVA, by halting any operations.  This such
   * that any user function init operations have a well defined
   * (hopefully quiet) state.
   */

  LWROC_INFO("Silence TRIVA  (HALT)");
  LWROC_TRIVA_WRITE(control, TRIVA_CONTROL_HALT);

  lwroc_triva_state_setup();

  lwroc_triva_control_setup();
}

void lwroc_triva_no_setup(void)
{
  lwroc_triva_state_no_setup();
}

void *_unmapinfo = NULL;

void lwroc_triva_unmap_at_exit(void)
{
  if (_lwroc_hardware_cleanup)
    {
      printf ("Performing hardware cleanup (TRIVA HALT, RESET)...\n");
      _lwroc_hardware_cleanup();
    }
  if (_unmapinfo)
    hwmap_unmap_vme(_unmapinfo);
}

void lwroc_triva_usage(void)
{
  printf ("\n");
  printf ("--[triva|trimi|trixor]=<OPTIONS>:\n");
  printf ("\n");
  printf ("  master                   Trigger module is master.\n");
  printf ("  slave                    Trigger module is slave.\n");
  printf ("  bridge=HOST              Hostname of (VME) bridge (for MVLC).\n");
  printf ("  @0xADDR                  Hardware address (e.g. VME, bits 31-24) (def 0x2).\n");
  printf ("  spill1213                Spill on/off marked by trigger 12/13 for delayed EB.\n");
  printf ("  ctime=n                  Conversion time (in 100 ns steps) (def 10).\n");
  printf ("  fctime=n                 Fast clear time (in 100 ns steps) (def 10).\n");
  printf ("  sim[=MOD[@HOST]]         Simulated trigger bus (MODule number).\n");
  printf ("  multi-word-trig-clear    Do trigger (DT) clear with one word write per bit.\n");
  printf ("\n");
}

void lwroc_triva_parse_setup(void)
{
  const char *cfg = _config._triva;
  const char *request;
  lwroc_parse_split_list parse_info;

  memset (&_lwroc_triva_config, 0, sizeof (_lwroc_triva_config));
  _lwroc_triva_config._master = 1; /* default to master */
  _lwroc_triva_config._kind = _config._triva_kind;
  _lwroc_triva_config._mapkind = _lwroc_triva_config._kind;

  switch (_lwroc_triva_config._mapkind)
    {
    case LWROC_TRIVA_KIND_TRIVA:
    case LWROC_TRIVA_KIND_TRIXOR:
      /* TODO: these need more testing etc... */
      /* Need larger values in master/slave configurations? */

      _lwroc_triva_config._fctime = 10;
      _lwroc_triva_config._ctime = 10;
      break;
    case LWROC_TRIVA_KIND_TRIMI:
      _lwroc_triva_config._fctime = 1;
      _lwroc_triva_config._ctime = 1;
      break;
      /* Note: we do not know sim kind yet. */
    default:
      LWROC_BUG("Unknown TRIVA type.");
      break;
    }

  _lwroc_triva_config._bridge = NULL;
  _lwroc_triva_config._vmeaddr = 0x02000000;
  _lwroc_triva_config._vmelen = TRIVA_MAP_LENGTH;

  lwroc_parse_split_list_setup(&parse_info, cfg, ',');

  while ((request = lwroc_parse_split_list_next(&parse_info)) != NULL)
    {
      const char *post;

      if (LWROC_MATCH_C_ARG("help"))
	{
	  lwroc_triva_usage();
	  exit(0);
	}
      else if (LWROC_MATCH_C_ARG("master"))
	{
	  _lwroc_triva_config._master = 1;
	}
      else if (LWROC_MATCH_C_ARG("slave"))
	{
	  _lwroc_triva_config._master = 0;
	}
      else if (LWROC_MATCH_C_PREFIX("bridge=",post))
	{
	  _lwroc_triva_config._bridge = strdup_chk(post);
	}
      else if (LWROC_MATCH_C_PREFIX("@",post))
	{
	  uint32_t off;

	  off = (uint32_t) lwroc_parse_hex(post, "address offset");

	  _lwroc_triva_config._vmeaddr = 0x01000000 * off;
	  printf ("cfg: '%s' => %d\n", cfg, _lwroc_triva_config._vmeaddr);
	}
      else if (LWROC_MATCH_C_ARG("spill1213"))
	{
	  _lwroc_triva_config._delayed_eb_status =
	    LWROC_DELAYED_EB_STATUS_ENABLED;
	}
      else if (LWROC_MATCH_C_ARG("sim"))
	{
	  _lwroc_triva_config._mapkind = LWROC_TRIVA_KIND_SIM;
	}
      else if (LWROC_MATCH_C_PREFIX("sim=",post))
	{
	  _lwroc_triva_config._sim_cfg = strdup_chk(post);
	  _lwroc_triva_config._mapkind = LWROC_TRIVA_KIND_SIM;
	}
      else if (LWROC_MATCH_C_PREFIX("fctime=",post))
	{
	  _lwroc_triva_config._fctime = lwroc_parse_hex_u16(post, "fctime");
	}
      else if (LWROC_MATCH_C_PREFIX("ctime=",post))
	{
	  _lwroc_triva_config._ctime = lwroc_parse_hex_u16(post, "ctime");
	}
      else if (LWROC_MATCH_C_ARG("multi-word-trig-clear"))
	{
	  _lwroc_multi_word_trig_clear = 1;
	}
      else
	LWROC_BADCFG_FMT("Unrecognised triva option: %s", request);
    }

/* #if HAS_ZYNQ_MMAP */
/*   vmeaddr = 0x43c00000; */
/* #endif */

  LWROC_LOG_FMT("TRIVA mode: master=%d",
		_lwroc_triva_config._master);
}

void lwroc_triva_map(void)
{
  uint32_t triva_status;

  _lwroc_hardware_cleanup = lwroc_triva_do_halt_reset;
  atexit(lwroc_triva_unmap_at_exit);

  switch (_lwroc_triva_config._mapkind)
    {
    case LWROC_TRIVA_KIND_TRIVA:
    case LWROC_TRIVA_KIND_TRIMI:
      {
	volatile hwmap_opaque *triva;

	LWROC_LOG_FMT("TRIVA VME map (addr=0x%08x)",
		      _lwroc_triva_config._vmeaddr);

#if HAS_FINDCONTROLLER
	/* TODO: ugly - ugly - ugly
	 * Using direct mapping. */
	if (_lwroc_triva_config._vmeaddr & 0xf0000000)
	  LWROC_FATAL_FMT("Cannot do direct mapping with "
			  "address (0x%08x) & 0xf0000000.",
			  _lwroc_triva_config._vmeaddr);
	triva = (volatile hwmap_opaque *) (0xd0000000 |
					   _lwroc_triva_config._vmeaddr);
#else
	triva = (volatile hwmap_opaque *)
	  hwmap_map_vme(_lwroc_triva_config._bridge,
			_lwroc_triva_config._vmeaddr,
			_lwroc_triva_config._vmelen,
			"TRIVA", &_unmapinfo);
#endif

	lwroc_triva_access_set_direct(triva);
      }
      break;

    case LWROC_TRIVA_KIND_TRIXOR:
      {
	volatile void *trixor;

	trixor = lwroc_map_trixor(TRIVA_MAP_LENGTH);

	lwroc_triva_access_set_direct(trixor);
      }
      break;

    case LWROC_TRIVA_KIND_SIM:
      {
	LWROC_LOG_FMT("TRIVA SIM open (%s)", _lwroc_triva_config._sim_cfg);

	lwroc_triva_setup_sim_pipe(_lwroc_triva_config._sim_cfg);
      }
      break;

    default:
      LWROC_BUG("Unknown TRIVA type.");
      break;
    }

  triva_status = LWROC_TRIVA_READ(status);

  LWROC_LOG_FMT("TRIVA off 0x00 = %08x", triva_status);
}

lwroc_data_pipe_handle *_lwroc_readout_pipe_handle = NULL;
lwroc_pipe_buffer_control *_lwroc_readout_pipe_ctrl = NULL;

uint32_t _lwroc_spill_seen = 0;
int      _lwroc_spill_flipped = 0;

uint32_t *_lwroc_readout_dt_trace_start = NULL;
uint32_t *_lwroc_readout_dt_trace_ptr = NULL;

uint64_t _lwroc_dt_trace_last_timestamp = 0;

void lwroc_triva_event_loop(int *start_no_stop)
{
  struct timeval meas_dt_last_poll;
  struct timeval meas_dt_after_poll;
  struct timeval meas_dt_after_postproc;
  int meas_dt_cycle = 0;
  uint32_t cycle;
#if 0
  int hold_seen = 0;
#endif
  *start_no_stop = 0;

  /* Note that we do not look for the global termination request.  The
   * control loop does the looking, and requests the triva state
   * machine to stop the acquisition.  That will get us out of this
   * loop in an orderly fashion, and then the readout loop is
   * terminated.
   */
  for (cycle = 0; ; cycle++)
    {
      uint16_t trig = 0;
      int cnt = 1;

      for ( ; !_lwroc_readout_status; )
	{
	  uint32_t expect_ec = cycle & 0x01f;

	  if (_lwroc_readout_measure_dt)
	    {
	      meas_dt_cycle = _lwroc_readout_measure_dt;
	      _lwroc_readout_meas_dt_after_dt_release.tv_sec = 0;
	      gettimeofday(&meas_dt_last_poll, NULL);
	    }

	  trig = lwroc_triva_try_take_trigger(expect_ec);

	  if (trig)
	    break;

	  if ((cnt & 0x0f) == 0)
	    {
	      if (_lwroc_readout_functions.idle_read)
		_lwroc_readout_functions.idle_read();
	      sched_yield();
	    }
	  else
	    cnt++;

	  if (_lwroc_readout_phase != TRIVA_READOUT_PHASE_READOUT)
	    break;
	}

      if (_lwroc_readout_measure_dt)
	gettimeofday(&meas_dt_after_poll, NULL);

      if (trig == 0xffff)
	{
	mismatch_handling:
	  _lwroc_readout_status |= LWROC_READOUT_STATUS_MISMATCH;
	  MFENCE;
	  /* Notify the control thread, which will urge the main
	   * process to do an immediate status query.
	   */
	  lwroc_thread_block_send_token(_lwroc_triva_control_thread->_block);

	  /* TODO: need to wait/sleep until we are told to break out? */
	}
      else if (trig)
	{
	  uint64_t event_count = _lwroc_readout_event_count;

	  _lwroc_readout_released_dt = 0;
	  _lwroc_readout_trig = trig;
	  _lwroc_readout_status |= LWROC_READOUT_STATUS_IN_READOUT;

	  if (_lwroc_readout_expect_trig)
	    {
	      if (trig != _lwroc_readout_expect_trig)
		{
		  LWROC_ERROR_FMT("Got unexpected trigger %d (expected %d).  "
				  "Something setting "
				  "TRIVA/MI software triggers?",
				  trig, _lwroc_readout_expect_trig);
		  _lwroc_readout_status |= LWROC_READOUT_STATUS_TRIG_UNEXPECT;
		  goto mismatch_handling;
		}

	      if (_lwroc_readout_expect_trig == 14)
		_lwroc_readout_expect_trig = 0;
	      else if (_lwroc_readout_expect_trig == 15)
		_lwroc_readout_expect_trig = -1;
	    }

	  if (trig >= 12)
	    {
	      if (trig >= 14)
		{
		  LWROC_INFO_FMT("Trigger %d seen.", trig);
		  if (trig == 15)
		    {
		      /* Prevent user function deadtime release. */
		      _lwroc_readout_released_dt = LWROC_READOUT_DT_RELEASED;
		    }
		  else if (trig == 14)
		    {
		      *start_no_stop = 1;
		      if (_lwroc_readout_functions.start_stop_loop)
			_lwroc_readout_functions.start_stop_loop(1);

		      lwroc_data_pipes_readout_active(1);
		    }
		}
	      else if (_lwroc_delayed_eb_status &
		       LWROC_DELAYED_EB_STATUS_ENABLED)
		{
		  uint32_t new_state;

		  new_state =
		    (trig == 12) ?
		    LWROC_SPILL_STATE_ON : LWROC_SPILL_STATE_OFF;

		  /* _lwroc_spill_state starts at 0, so will not warn
		   * for first trigger.
		   */
		  if (new_state == _lwroc_spill_state)
		    {
		      LWROC_WARNING_FMT("Spill trigger %d seen more than "
					"once in a row, last duplicate "
					"seen %d spill triggers ago.",
					trig, _lwroc_spill_flipped);
		      _lwroc_spill_flipped = 1;
		    }
		  else
		    _lwroc_spill_flipped++;

		  _lwroc_spill_state = new_state;

		  _lwroc_spill_state_since = time(NULL);

		  if (!(_lwroc_spill_seen & _lwroc_spill_state))
		    LWROC_INFO_FMT("Spill trigger %d seen.", trig);

		  _lwroc_spill_seen |= _lwroc_spill_state;

		  if (!(_lwroc_spill_state & LWROC_SPILL_STATE_ON))
		    {
		      /* Wake the transmission thread up, so that it
		       * starts sending data ASAP.  Otherwise, it will
		       * wait until its next timeout.
		       *
		       * The easiest way is to notify any thread which
		       * is waiting on the data buffer.  Then they can
		       * evaluate if they have anything to send.
		       *
		       * no_hysteresis wakes all consumers that have
		       * marked blocking up.
		       */

		      lwroc_pipe_buffer_did_write(_lwroc_readout_pipe_ctrl,
						  0, 1);
		    }
		}
	    }

	  /* Do the accounting before the event is handled.  For
	   * normal lock-up detection this does not matter.  It is
	   * important when determining if the last event has been
	   * handled after control setting HALT, i.e. before trigger
	   * 15 is issued.  (As it happens before the EON is removed
	   * from the triva/mi status.)
	   */
	  _lwroc_readout_event_count++;
	  SFENCE;

	  if (_lwroc_readout_functions.read_event)
	    _lwroc_readout_functions.read_event(event_count, trig);

	  /* TODO: Check buffer integrity */

	  /*lwroc_lmd_dump_subevent_header(subevent);*/
	  /*lwroc_lmd_dump_event_header(ev);*/
	  /*lwroc_lmd_dump_event(ev, used_size);*/

	  if (trig == 15)
	    {
	      lwroc_data_pipes_readout_active(0);

	      *start_no_stop = 0;
	      if (_lwroc_readout_functions.start_stop_loop)
		_lwroc_readout_functions.start_stop_loop(0);

	      /* To expect nothing also for slaves. */
	      _lwroc_readout_expect_trig = -1;

	      _lwroc_readout_status |= LWROC_READOUT_STATUS_HELD;
	      MFENCE;
	      LWROC_ACTION("RUN: HALT (after 15)");
	      LWROC_TRIVA_WRITE(control,
				TRIVA_CONTROL_HALT);
	      /* This sleep seems to be required, since when we are a
	       * master, the RESET will also reset all slave modules.
	       * Possibly before they have dealt with the trigger 15.
	       *
	       * But, this is not water-tight - an arbitrary sleep is
	       * not enough...  They may take longer time, and then
	       * loose it.  So no reset.
	       */
#if 0
	      usleep(20000);
	      LWROC_TRIVA_WRITE(control,
				TRIVA_CONTROL_RESET);
#endif
	      LWROC_TRIVA_SERIALIZE_IO;

	      /* We break out of the in-readout path here, i.e. would
	       * miss this statement further below.
	       */
	      _lwroc_readout_status &= ~LWROC_READOUT_STATUS_IN_READOUT;
	      break;
	    }
	  else if (!_lwroc_readout_released_dt)
	    lwroc_triva_release_dt();

	  if (_lwroc_readout_measure_dt &&
	      meas_dt_cycle == _lwroc_readout_measure_dt)
	    {
	      /* In principle, there may be a trigger already again.
	       * But those only happen for pending triggers,
	       * so would not be common.
	       * And checking for that costs time.
	       * Let's not complicate ourselves with that.
	       */

	      gettimeofday(&meas_dt_after_postproc, NULL);

#define ELAPSED_us_32(a,b)						\
	      ((uint32_t) ((b).tv_usec - (a).tv_usec) +			\
	       ((uint32_t) ((b).tv_sec - (a).tv_sec)) * 1000000)

	      /* The event counter is updated twice, such that odd
	       * numbers means while calculating below.
	       */
	      _lwroc_readout_meas_dt_events++;
	      SFENCE;
	      _lwroc_readout_meas_dt_us_poll +=
		ELAPSED_us_32(meas_dt_last_poll,
			      meas_dt_after_poll);
	      if (_lwroc_readout_meas_dt_after_dt_release.tv_sec)
		{
		  _lwroc_readout_meas_dt_us +=
		    ELAPSED_us_32(meas_dt_after_poll,
				  _lwroc_readout_meas_dt_after_dt_release);
		  _lwroc_readout_meas_dt_us_postproc +=
		    ELAPSED_us_32(_lwroc_readout_meas_dt_after_dt_release,
				  meas_dt_after_postproc);
		}
	      else
		{
		  _lwroc_readout_meas_dt_us +=
		    ELAPSED_us_32(meas_dt_after_poll,
				  meas_dt_after_postproc);
		}
	      SFENCE;
	      _lwroc_readout_meas_dt_events++;

	      /* Do we have space (allocated last time, user-times
	       * possibly already filled), to store the actual times?
	       */

#define LWROC_DT_TRACE_T_LOCAL(flags, tv) do {			\
		*(_lwroc_readout_dt_trace_ptr++) =		\
		  LWROC_DT_TRACE_TIMESCALE_LOCAL_US | (flags) |	\
		  (((uint32_t) (tv)->tv_sec) &			\
		   LWROC_DT_TRACE_TV_SEC_MASK);			\
		*(_lwroc_readout_dt_trace_ptr++) =		\
		  (uint32_t) (tv)->tv_usec;			\
	      } while (0)

	      if (_lwroc_readout_dt_trace_ptr)
		{
		  ssize_t n;
		  /* printf ("#"); */

		  /* Store the trigger, spill state and event number.
		   * We only copy the low half (32 bits) of the event
		   * count.  This is stored as the first item of an
		   * event.
		   */
		  *(_lwroc_readout_dt_trace_ptr++) =
		    LWROC_DT_TRACE_TIMESCALE_EVENT |
		    ((_lwroc_spill_state) << LWROC_DT_TRACE_SPILL_SHIFT) |
		    (trig);
		  *(_lwroc_readout_dt_trace_ptr++) =
		    (uint32_t) _lwroc_readout_event_count;

		  /* If the last timestamp reported is new, we record it. */
		  if (_lwroc_mon_main._last_timestamp !=
		      _lwroc_dt_trace_last_timestamp)
		    {
		      /* Store from the local variable. */
		      _lwroc_dt_trace_last_timestamp =
			_lwroc_mon_main._last_timestamp;

		      *(_lwroc_readout_dt_trace_ptr++) =
			LWROC_DT_TRACE_TIMESCALE_STAMP |
			LWROC_DT_TRACE_T_TRIGGER |
			(uint32_t) ((_lwroc_dt_trace_last_timestamp >> 32) &
				    LWROC_DT_TRACE_TV_SEC_MASK);
		      *(_lwroc_readout_dt_trace_ptr++) =
			(uint32_t) _lwroc_dt_trace_last_timestamp;
		    }

		  LWROC_DT_TRACE_T_LOCAL(LWROC_DT_TRACE_T_LAST_POLL,
					 &meas_dt_last_poll);

		  LWROC_DT_TRACE_T_LOCAL(LWROC_DT_TRACE_T_AFTER_POLL,
					 &meas_dt_after_poll);

		  if (_lwroc_readout_meas_dt_after_dt_release.tv_sec)
		    LWROC_DT_TRACE_T_LOCAL(LWROC_DT_TRACE_T_AFTER_DT_RELEASE,
					   &_lwroc_readout_meas_dt_after_dt_release);

		  LWROC_DT_TRACE_T_LOCAL(LWROC_DT_TRACE_T_AFTER_POSTPROC,
					 &meas_dt_after_postproc);

		  n = ((char *) _lwroc_readout_dt_trace_ptr) -
		    ((char *) _lwroc_readout_dt_trace_start);

		  lwroc_pipe_buffer_did_write(_lwroc_readout_dt_trace_ctrl,
					      (size_t) n, 0);
		}

	      /* See if we can get space to store the measured times
	       * for the next event.
	       */

	      for ( ; ; )
		{
		  /* printf ("*"); */

		  _lwroc_readout_dt_trace_ptr = (uint32_t *)
		    lwroc_pipe_buffer_alloc_write(_lwroc_readout_dt_trace_ctrl,
						  64 * sizeof (uint32_t),
						  NULL,
						  NULL, 0);

		  _lwroc_readout_dt_trace_start =
		    _lwroc_readout_dt_trace_ptr;

		  /*
		    printf ("%p %zd\n", _lwroc_readout_dt_trace_start, nwaste);
		  */

		  break;
		}
	    }

	  _lwroc_readout_status &= ~LWROC_READOUT_STATUS_IN_READOUT;
	}

      LWROC_MON_CHECK_COPY_BLOCK(_lwroc_mon_main_handle,&_lwroc_mon_main,
				 trig >= 12);

      if (_lwroc_readout_phase != TRIVA_READOUT_PHASE_READOUT)
	{
#if 0
	  if (_lwroc_readout_phase != TRIVA_READOUT_PHASE_HOLD)
	    {
	      /* In case we have reached here but the HALT state was
	       * set after the last trigger and a trigger got through
	       * before that but after the last check for a trigger.
	       * We therefore allow for a last trigger check.
	       */
	      if (hold_seen)
		break;
	      hold_seen = 1;
	    }
	  else
#endif
	    break;
	}
    }

  /* We may have been aborted (as a slave), without taking the trig 15. */
  lwroc_data_pipes_readout_active(0);

  LWROC_MON_CHECK_COPY_BLOCK(_lwroc_mon_main_handle,&_lwroc_mon_main,1);
}

#if HAS_CMVLC
int lwroc_triva_cmvlc_skip(uint32_t *buf, size_t len,
			   struct cmvlc_event_info *info);

int lwroc_triva_cmvlc_event_check(uint32_t *buf, size_t len,
				  struct cmvlc_event_info *info);

int lwroc_triva_cmvlc_periodic(double elapsed);

void lwroc_triva_silence_cmvlc(void)
{
  struct cmvlc_client *cmvlc = NULL;

  cmvlc = lwroc_triva_access_get_cmvlc();

  LWROC_INFO("Silence MVLC readout.");

  /* Silence any readout if it is running. */
  if (cmvlc_set_daq_mode(cmvlc, 0, 0, NULL, 0, 0) < 0)
    LWROC_FATAL("Failed to clear MVLC DAQ mode.");

  /* TODO:
   *
   * Enable stack sequences for all IRQs and see that nothing triggers.
   *
   * If it does - we have a rogue module delivering interrupts, which
   * the user needs to silence!
   *
   */
}

void lwroc_triva_flush_seq_cmvlc(void)
{
  /* Execute stack seq 3. */
  struct cmvlc_client *cmvlc = NULL;

  cmvlc = lwroc_triva_access_get_cmvlc();

  LWROC_INFO("Execute MVLC flush sequence.");

  if (cmvlc_fire_stack(cmvlc, 3) < 0)
    LWROC_FATAL("Failed to fire MVLC flush sequence.");
}

/* Keep track of which readout stacks are in use. */
uint8_t _lwroc_cmvlc_readout_stacks[16];

/* Has the user preparation routine called DT release already? */
int _lwroc_triva_prepare_cmvlc_released_dt;

/* Skipping... */
uint32_t _lwroc_triva_cmvlc_skip_ident = 1;
uint32_t _lwroc_triva_cmvlc_skip_stacks;

void lwroc_triva_prepare_cmvlc(void)
{
  struct cmvlc_client *cmvlc = NULL;
  unsigned char readout_no;
  int trig;
  uint8_t signal_map[8][2];
  int map_no;

  uint32_t vme_base = 0x02000000; /* FIXME */

  cmvlc = lwroc_triva_access_get_cmvlc();

  LWROC_INFO("Prepare MVLC readout...");

  /* Silence any readout if it is running. */
  if (cmvlc_set_daq_mode(cmvlc, 0, 0, NULL, 0, 0) < 0)
    LWROC_FATAL("Failed to clear MVLC DAQ mode.");

  /* Clear library state of stacks. */
  cmvlc_reset_stacks(cmvlc);

  /* Prepare readout stack.  IRQ 4 - TRIVA IRQ. */
  {
    struct cmvlc_stackcmdbuf stack;

    cmvlc_stackcmd_init(&stack);
    cmvlc_stackcmd_start(&stack, stack_out_pipe_data);

    /* 40+460+32+40+40+480+40 */
    /* 40+480+40 */

    /* Pre-data marker. */
    cmvlc_stackcmd_marker(&stack, 0x5555aaaa);
    /* Read TRIVA status register. */
    cmvlc_stackcmd_vme_rw(&stack, vme_base + 0x0, 0, vme_rw_read_to_accu,
			  vme_user_A32, vme_D32);
    /* Write the event count and trigger number. */
    cmvlc_stackcmd_write_special(&stack, wrspec_accu);
    /* Extract the trigger number. */
    cmvlc_stackcmd_mask_rotate_accu(&stack, 0x000f, 0);
    /* Signal execution of another stack. */
    cmvlc_stackcmd_signal_accu(&stack);
    /* Silence the interrupt. */
    cmvlc_stackcmd_vme_rw(&stack, vme_base + 0x0,
			  TRIVA_STATUS_EV_IRQ_CLEAR |
			  TRIVA_STATUS_IRQ_CLEAR, vme_rw_write,
			  vme_user_A32, vme_D32);

    /* Post-data marker. */
    cmvlc_stackcmd_marker(&stack, 0xaaaa5555);

    cmvlc_stackcmd_end(&stack);

    /* Upload the stack itself.  Remember trigger. */
    cmvlc_setup_stack(cmvlc, &stack, 4, 0x0040 | (4-1));
  }

  /* Prepare abort/flush stack.  Use number 3. */
  {
    struct cmvlc_stackcmdbuf stack;

    cmvlc_stackcmd_init(&stack);
    cmvlc_stackcmd_start(&stack, stack_out_pipe_data);

    /* Marker. */
    cmvlc_stackcmd_marker(&stack, 0xdeadbeef);

    /* Random + incrementing marker. */
    _lwroc_triva_cmvlc_skip_ident++;
    _lwroc_triva_cmvlc_skip_ident ^= ((uint32_t) rand()) << 16;

    cmvlc_stackcmd_marker(&stack, _lwroc_triva_cmvlc_skip_ident);

    cmvlc_stackcmd_end(&stack);

    /* Upload the stack itself.  Remember trigger. */
    cmvlc_setup_stack(cmvlc, &stack, 3, 0x0040 | (3-1));
  }

  memset(_lwroc_cmvlc_readout_stacks, 0, sizeof (_lwroc_cmvlc_readout_stacks));

  _lwroc_readout_functions.cmvlc_readout_for_trig[14] = 14;
  _lwroc_readout_functions.cmvlc_readout_for_trig[15] = 15;

  memset(signal_map, 0, sizeof (signal_map));
  map_no = 0;

  for (trig = 1; trig <= 15; trig++)
    {
      readout_no = _lwroc_readout_functions.cmvlc_readout_for_trig[trig];

      /* 14 and 15 are hard-coded and may not be used.
       * 4 is also in use by the TRIVA itself.
       */
      if (readout_no > (trig < 14 ? 13 : 15) ||
	  readout_no == 4)
	LWROC_FATAL_FMT("MVLC readout #%d for trigger %d outside "
			"allowed range [1,13] or 4 (TRIVA IRQ itself).",
			readout_no, trig);

      if (readout_no)
	{
	  _lwroc_cmvlc_readout_stacks[readout_no]++;

	  if (map_no >= 8)
	    LWROC_FATAL("Too many MVLC trigger -> readout stack "
			"map entries.");

	  signal_map[map_no][0] = (uint8_t) trig;
	  signal_map[map_no][1] = readout_no;
	  map_no++;
	}
    }

  for (readout_no = 1; readout_no <= 15; readout_no++)
    if (_lwroc_cmvlc_readout_stacks[readout_no])
      {
	/* Prepare readout stack. */

	struct cmvlc_stackcmdbuf stack;

	cmvlc_stackcmd_init(&stack);
	cmvlc_stackcmd_start(&stack, stack_out_pipe_data);

	_lwroc_triva_prepare_cmvlc_released_dt = 0;

	/* Pre-data marker. */
	cmvlc_stackcmd_marker(&stack, 0x55affe00 | (uint32_t) readout_no);

	/* User needs to insert its part of the readout.
	 *
	 * That also includes releasing the deadtime!
	 */
	_lwroc_readout_functions.prepare_cmvlc(readout_no,
					       &stack);

	/* Release the deadtime. */
	if (readout_no != 15)
	  {
	    if (!_lwroc_triva_prepare_cmvlc_released_dt)
	      lwroc_triva_prepare_cmvlc_release_dt(&stack, readout_no);
	  }
	else
	  {
	    if (_lwroc_triva_prepare_cmvlc_released_dt)
	      LWROC_FATAL("MVLC preparation for readout #15 (stop acq) "
			  "released DT.");
	  }

	/* Post-data marker. */
	cmvlc_stackcmd_marker(&stack, 0x00112233);

	cmvlc_stackcmd_end(&stack);

	/* Upload the stack itself.  Remember trigger. */
	cmvlc_setup_stack(cmvlc, &stack, readout_no,
			  0x0040 | (uint8_t) ((readout_no)-1));
      }

  /* Tell MVLC where to send readout data. */
  if (cmvlc_readout_attach(cmvlc) < 0)
    LWROC_FATAL("Failed to attach MVLC data output.");

  {
    /* Setup pointers to stacks, their triggers. */
    if (cmvlc_set_daq_mode(cmvlc, 0, 1,
			   signal_map, sizeof (signal_map), 0) < 0)
      LWROC_FATAL("Failed to setup MVLC stacks and IRQ stack map.");
  }

  /* Execute the flush stack, to get that into the data stream. */
  lwroc_triva_flush_seq_cmvlc();

  /* Skip data until we see the flush sequence. */
  {
    _lwroc_triva_cmvlc_skip_stacks = 0;

    cmvlc_readout_loop(cmvlc, lwroc_triva_cmvlc_skip, NULL);
  }

  {
    /* Enable DAQ mode. */
    if (cmvlc_set_daq_mode(cmvlc, 1, 0, NULL, 0, 0) < 0)
      LWROC_FATAL("Failed to set MVLC DAQ mode.");
  }

  LWROC_INFO("Prepare MVLC readout done.");
}

void lwroc_triva_prepare_cmvlc_release_dt(struct cmvlc_stackcmdbuf *stack,
					  unsigned char readout_no)
{
  uint32_t vme_base = 0x02000000; /* FIXME */

  if (_lwroc_triva_prepare_cmvlc_released_dt)
    LWROC_FATAL_FMT("MVLC preparation for readout #%d already "
		    "released DT.",
		    readout_no);

  _lwroc_triva_prepare_cmvlc_released_dt = 1;

  /* Release deadtime. */
  cmvlc_stackcmd_vme_rw(stack, vme_base + 0x0,
			TRIVA_STATUS_EV_IRQ_CLEAR |
			TRIVA_STATUS_IRQ_CLEAR |
			TRIVA_STATUS_FC_PULSE |
			TRIVA_STATUS_DT_CLEAR, vme_rw_write,
			vme_user_A32, vme_D32);
}

uint8_t _expect_stack;
uint8_t _expect_ec;

void lwroc_triva_event_loop_cmvlc(int *start_no_stop)
{
  struct cmvlc_client *cmvlc = NULL;

  int ret;

  cmvlc = lwroc_triva_access_get_cmvlc();

  (void) start_no_stop;

  LWROC_INFO("CMVLC event loop!");

  _expect_stack = 4; /* The readout stack. */
  _expect_ec    = 0;

  /* Readout loop. */
  ret = cmvlc_readout_loop(cmvlc,
			   lwroc_triva_cmvlc_event_check,
			   lwroc_triva_cmvlc_periodic);

  ret = -ret;

  if (ret & 0x100000)
    goto final_return; /* Expected breakout. */

  /* Unexpected breakout, inform control and wait for request to get
   * out.
   *
   * Same handling as for mismatch detected in 'normal'
   * (lwroc_triva_event_loop) readout.
   */

  _lwroc_readout_status |= (uint32_t) ret;
  MFENCE;
  for ( ; ; )
    {
      /* Notify the control thread, which will urge the main
       * process to do an immediate status query.
       */
      lwroc_thread_block_send_token(_lwroc_triva_control_thread->_block);

      LWROC_MON_CHECK_COPY_BLOCK(_lwroc_mon_main_handle,&_lwroc_mon_main,1);

      if (_lwroc_readout_phase != TRIVA_READOUT_PHASE_READOUT)
	break;
    }

 final_return:
  /* Hmmm.... */
  lwroc_data_pipes_readout_active(0);

  LWROC_MON_CHECK_COPY_BLOCK(_lwroc_mon_main_handle,&_lwroc_mon_main,1);
}

/* Breaking out of cmvlc_readout_loop() is done with negative error
 * codes.
 *
 * We need to distinguish two cases:
 *
 * - Expected breakout, the canonical case is handling trigger 15.
 *   The loop shall in these cases just be aborted.
 *
 *   Indicated by -0x100000 and a number.
 *
 * - Unexpected breakout, e.g. due to malformed data from the
 *   sequencer.  Or a trigger mismatch / event counter failure.
 *
 *   In these cases, a error readout status is to be set, and then the
 *   control thread contacts the state machine that in turn will
 *   request the final abort for the readout thread.  (We have already
 *   exited the loop as such.)
 *
 *   Indicated by -(error) status code to set.
 */

#define DATA_VERIFY(x) do {                                             \
    if (!(x)) {                                                         \
      size_t __i;                                                       \
      fprintf(stderr, "Data fail '%s' at offset %zd=0x%zx: 0x%08x.\n",  \
	      #x, off, off, buf[off]);                                  \
      for (__i = 0; __i < len; __i++) {                                 \
	if (__i % 8 == 0)                                               \
	  fprintf (stderr, "\n%03zx:", __i);                            \
	fprintf (stderr, " %08x", buf[__i]);                            \
      }                                                                 \
      fprintf (stderr, "\n\n");                                         \
      LWROC_ERROR("Not nice");						\
      return -(int) LWROC_READOUT_STATUS_SEQ_MALFORM;			\
    }                                                                   \
  } while (0)

int lwroc_triva_cmvlc_skip(uint32_t *buf, size_t len,
			   struct cmvlc_event_info *info)
{
  /* We do not care about flags or anything.
   * Just skip util we get the flush buffer.
   */

  _lwroc_triva_cmvlc_skip_stacks++;

  if (info->_stacknum == 3)
    {
      /* TODO: We should check the payload also. */

      if (len < 2)
	{
	  LWROC_INFO_FMT("Found MVLC flush/abort stack #3, "
			 "but too short (%" MYPRIzd ").",
			 len);
	  return 0;
	}

      if(buf[1] != _lwroc_triva_cmvlc_skip_ident)
	{
	  LWROC_INFO_FMT("Found MVLC flush/abort stack #3, "
			 "got mark %08x, expected %08x.",
			 buf[1], _lwroc_triva_cmvlc_skip_ident);
	  return 0;
	}

      LWROC_INFO_FMT("Found MVLC flush/abort stack #3, "
		     "at skip %" PRIu32 ".",
		     _lwroc_triva_cmvlc_skip_stacks);
      /* Expected breakout. */
      return -(int) 0x100001;
    }

  return 0;
}

uint64_t _triggers_seen[16];
uint64_t _triggers_seen_last[16];

int lwroc_triva_cmvlc_event_check(uint32_t *buf, size_t len,
				  struct cmvlc_event_info *info)
{
  size_t   off = 0;
  uint32_t triva_status;
  uint8_t  ec;
  static uint8_t trig = 0; /* Keep from IRQ4 stack to readout stack. */

  /*
  printf ("event:  %zd %d %d\n", len, info->_flags, info->_stacknum);
  fflush(stdout);
  */

  if (info->_flags)
    {
      LWROC_ERROR_FMT("MVLC readout block #%d with "
		      "non-zero flags (error?): %d.\n",
		      info->_stacknum, info->_flags);
      return -(int) LWROC_READOUT_STATUS_SEQ_ERROR;
    }

  if (info->_stacknum != _expect_stack)
    {
      if (info->_stacknum == 3)
	{
	  /* This is the flush/abort stack.
	   * Issued by control thread to surely get us out of readout loop.
	   */
	  LWROC_INFO("Got MVLC flush/abort stack #3.");
	  /* Expected breakout. */
	  return -(int) 0x100001;
	}

      LWROC_ERROR_FMT("Unexpected MVLC readout stack #%d != expected #%d.  "
		      "(Rogue module issuing IRQ?)",
		      info->_stacknum, _expect_stack);

      return -(int) LWROC_READOUT_STATUS_SEQ_UNEXPECT;
    }

  if (info->_stacknum == 4)
    {
      /*****************************************************************
       *
       * First-stage readout stack - responding to the trigger as such.
       */

      /* Pre-data marker. */
      DATA_VERIFY(len > off);
      DATA_VERIFY(buf[off] == 0x5555aaaa);
      off++;

      DATA_VERIFY(len > off);
      triva_status = buf[off];
      /* DATA_VERIFY((buf[off] & 0x0000000f) == 14); */
      off++;

      /* Next stack expectation. */
      trig = triva_status & TRIVA_STATUS_TRIG_MASK;
      ec  = (triva_status & TRIVA_STATUS_EC_MASK) >> TRIVA_STATUS_EC_SHIFT;

      if (triva_status & TRIVA_STATUS_MISMATCH)
	{
	  char tmp[128];

	  LWROC_ERROR_FMT("*** MISMATCH REPORTED *** "
			  "by TRIVA (status = 0x%04x: %s) "
			  "(expect EC=%d).",
			  triva_status & 0xffff,
			  lwroc_triva_decode_status(tmp, triva_status),
			  _expect_ec);
	  return -(int) LWROC_READOUT_STATUS_MISMATCH;
	}

      if (ec != _expect_ec)
	{
	  uint8_t expect_ec = _expect_ec;
	  /* Double layer of if-statements to not have the check for
	   * trigger 15 every event.
	   */
	  if (trig == 15)
	    expect_ec = 0;
	  if (ec != expect_ec)
	    {
	      LWROC_ERROR_FMT("*** Trig = %d, "
			      "module EC = %d != expect EC = %d ***",
			      trig, ec, _expect_ec);
	      return -(int) LWROC_READOUT_STATUS_EC_MISMATCH;
	    }
	}
      _expect_ec = (_expect_ec + 1) &
	(TRIVA_STATUS_EC_MASK >> TRIVA_STATUS_EC_SHIFT);

      _triggers_seen[trig]++;

      /*
      printf ("irq 4: status 0x%08x\n", triva_status);
      fflush(stdout);
      */

      /* Post-data marker. */
      DATA_VERIFY(len > off);
      DATA_VERIFY(buf[off] == 0xaaaa5555);
      off++;

      _expect_stack = _lwroc_readout_functions.cmvlc_readout_for_trig[trig];

      /* No surplus data. */
      DATA_VERIFY(len == off);
    }
  else
    {
      uint32_t expect_pre_mark  = (0x55affe00 | (uint32_t) info->_stacknum);
      uint32_t expect_post_mark = 0x00112233;

      /*****************************************************************
       *
       * Second-stage readout stack - actual readout data.
       */

      /* Space for pre- and post-data markers. */
      if (len < 2)
	{
	  LWROC_ERROR_FMT("MVLC readout block #%d "
			  "too short (%" MYPRIzd ") for "
			  "pre- and post-data markers.",
			  info->_stacknum, len);
	  return -(int) LWROC_READOUT_STATUS_SEQ_MALFORM;
	}
      if (buf[0] != expect_pre_mark)
	{
	  LWROC_ERROR_FMT("MVLC readout block #%d "
			  "pre-data marker wrong (0x%08x != 0x%08x).",
			  info->_stacknum,
			  buf[0], expect_pre_mark);
	  return -(int) LWROC_READOUT_STATUS_SEQ_MALFORM;
	}
      if (buf[len-1] != expect_post_mark)
	{
	  LWROC_ERROR_FMT("MVLC readout block #%d "
			  "post-data marker wrong (0x%08x != 0x%08x).",
			  info->_stacknum,
			  buf[0], expect_post_mark);
	  return -(int) LWROC_READOUT_STATUS_SEQ_MALFORM;
	}

      if (info->_stacknum >= 14)
	{
	  /* For trigger 14 and 15, the readout stack number is the
	   * same as the trigger number.
	   */
	  LWROC_INFO_FMT("Trigger %d seen.", info->_stacknum);

	  if (trig == 14)
	    {
	      lwroc_data_pipes_readout_active(1);
	    }
	}

      {
	uint64_t event_count = _lwroc_readout_event_count;

	_lwroc_readout_event_count++;
	SFENCE;

	if (_lwroc_readout_functions.format_event)
	  _lwroc_readout_functions.format_event(event_count, trig,
						buf + 1,
						(len - 2) * sizeof (uint32_t));

	LWROC_MON_CHECK_COPY_BLOCK(_lwroc_mon_main_handle,&_lwroc_mon_main,
				   trig >= 12);
      }

      if (trig == 15)
	{
	  lwroc_data_pipes_readout_active(0);

	  _lwroc_readout_status |= LWROC_READOUT_STATUS_HELD;

	  /* TODO: we cannot interface the TRIVA - control thread
	   * might be doing it as well!  Should be in control thread?
	   */
#if 0
	  MFENCE;
	  LWROC_ACTION("RUN: HALT (after 15)");
	  LWROC_TRIVA_WRITE(control,
			    TRIVA_CONTROL_HALT);
#endif

	  /* Break out of the readout loop.  TODO: Should be done not
	   * with an error code! */
	  /* Expected breakout. */
	  return -(int) 0x100002;
	}

      _expect_stack = 4;
    }

  return 0;
}

int lwroc_triva_cmvlc_periodic(double elapsed)
{
  int trig;

  for (trig = 0; trig < 16; trig++)
    if (_triggers_seen[trig])
      {
	printf ("Trig %2d: %12" PRIu64 " %7.0f/s\n",
		trig,
		_triggers_seen[trig],
		(double) (_triggers_seen[trig] -
			  _triggers_seen_last[trig]) / elapsed);
      }

  memcpy (_triggers_seen_last, _triggers_seen,
	  sizeof (_triggers_seen_last));

  return 0;
}
#endif/*HAS_CMVLC*/

void lwroc_triva_inject_ident(int dummy)
{
  char *buf;
  lmd_event_header_host *header;
  uint32_t *p;
  size_t data_len = 3 * sizeof(uint32_t);
  size_t total_len = data_len + sizeof (lmd_event_header_host);

  lwroc_data_pipe_set_readout_notify(_lwroc_readout_pipe_handle,
				     &_lwroc_readout_status,
				     LWROC_READOUT_STATUS_WAIT_BUFSPACE);

  buf = lwroc_request_event_space(_lwroc_readout_pipe_handle,
				  (uint32_t) total_len);
  header = (lmd_event_header_host *) buf;
  header->i_type = LWROC_LMD_IDENT_TYPE;
  header->i_subtype = LWROC_LMD_IDENT_SUBTYPE;
  p = (uint32_t *) (header+1);
  if (dummy)
    {
      *(p++) = 0;
      *(p++) = 0;
      *(p++) = 0;
    }
  else
    {
      *(p++) = _lwroc_eb_ident[0];
      *(p++) = _lwroc_eb_ident[1];
      *(p++) = _lwroc_eb_ident[2];
    }
  header->l_dlen = (uint32_t) DLEN_FROM_EVENT_DATA_LENGTH(data_len);
  lwroc_used_event_space(_lwroc_readout_pipe_handle,
			 (uint32_t) total_len, 1);
  /*
  printf ("Injected IDENT %08x %08x %08x\n",
	  _lwroc_eb_ident[0],
	  _lwroc_eb_ident[1],
	  _lwroc_eb_ident[2]);
  */
}

void lwroc_triva_readout(int *start_no_stop)
{
  _lwroc_readout_pipe_handle = lwroc_get_data_pipe("READOUT_PIPE");

  if (!_lwroc_readout_pipe_handle)
    LWROC_FATAL("No handle for data pipe READOUT_PIPE.");

  _lwroc_readout_pipe_ctrl =
    lwroc_data_pipe_get_pipe_ctrl(_lwroc_readout_pipe_handle);

  /* init done by control thread. */
  /* lwroc_init_triva(master); */

 readout_phase_setup:
  _lwroc_readout_status = 0; /* all is fine */

#if HAS_CMVLC
  lwroc_triva_silence_cmvlc();
#endif

  _lwroc_readout_phase = TRIVA_READOUT_PHASE_READY;
  MFENCE;
  /* Send a token to wake control up! */
  lwroc_thread_block_send_token(_lwroc_triva_control_thread->_block);

  LWROC_LOG("Readout thread waiting for inject request...");

  if (!lwroc_triva_readout_phase_wait(TRIVA_READOUT_PHASE_READY,
				      TRIVA_READOUT_PHASE_INJECT |
				      TRIVA_READOUT_PHASE_ABORT_T,
				      _lwroc_readout_thread))
    return;

  if (_lwroc_readout_phase == TRIVA_READOUT_PHASE_ABORT_T)
    {
      /* TODO: Remove this message... Just to see if it worked... */
      LWROC_INFO("Caught ABORT_T while waiting for first INJECT.");
      goto abort_t;
    }

  /* We are to inject the ID message, such that our stream will
   * be accepted by the event builder.
   */

 inject_ident:
  if (_lwroc_eb_ident[0])
    {
      LWROC_LOG("Readout thread injecting ID message!");

      lwroc_triva_inject_ident(0);
    }

  /* We have injected the ID message.
   * Control thread actually does not care whether we send it or not...
   */

  _lwroc_readout_phase = TRIVA_READOUT_PHASE_INJECTED;
  MFENCE;
  /* Send a token to wake control up! */
  lwroc_thread_block_send_token(_lwroc_triva_control_thread->_block);

  if (!lwroc_triva_readout_phase_wait(TRIVA_READOUT_PHASE_INJECTED,
				      TRIVA_READOUT_PHASE_INJECT |
				      TRIVA_READOUT_PHASE_GO_PREP |
				      TRIVA_READOUT_PHASE_ABORT_T,
				      _lwroc_readout_thread))
    return;

  if (_lwroc_readout_phase == TRIVA_READOUT_PHASE_INJECT)
    goto inject_ident;

  /* Control may have cycled back to start all over... */

  if (_lwroc_readout_phase != TRIVA_READOUT_PHASE_GO_PREP)
    {
    abort_t:
      LWROC_INFO("Readout thread thrown back to top by test abort.");

      if (_lwroc_readout_phase != TRIVA_READOUT_PHASE_ABORT_T)
	LWROC_BUG_FMT("Bad readout abort phase (%d, expected %d).",
		      _lwroc_readout_phase, TRIVA_READOUT_PHASE_ABORT_T);

      _lwroc_readout_phase = TRIVA_READOUT_PHASE_ABORTED;
      MFENCE;
      /* Send a token to wake control up! */
      lwroc_thread_block_send_token(_lwroc_triva_control_thread->_block);

      /* Wait until we leave the brokeout state. */
      if (!lwroc_triva_readout_phase_wait(TRIVA_READOUT_PHASE_ABORTED,
					  TRIVA_READOUT_PHASE_SETUP,
					  _lwroc_readout_thread))
	return;

      goto readout_phase_setup;
    }

 readout_phase_prepare:
#if HAS_CMVLC
  lwroc_triva_prepare_cmvlc();
#endif

  _lwroc_readout_phase = TRIVA_READOUT_PHASE_PREPARED;
  MFENCE;
  /* Send a token to wake control up! */
  lwroc_thread_block_send_token(_lwroc_triva_control_thread->_block);

  if (!lwroc_triva_readout_phase_wait(TRIVA_READOUT_PHASE_PREPARED,
				      TRIVA_READOUT_PHASE_GO_READ,
				      _lwroc_readout_thread))
    return;

 readout_phase_readout:
  _lwroc_readout_phase = TRIVA_READOUT_PHASE_READOUT;
  MFENCE;
  /* Send a token to wake control up! */
  lwroc_thread_block_send_token(_lwroc_triva_control_thread->_block);

  LWROC_LOG("Readout thread loop active!");

  _lwroc_spill_seen = 0;

  /* done by control thread
     if (master)
       lwroc_triva_do_start_acq();
  */

  if (_lwroc_readout_functions.triva_event_loop)
    _lwroc_readout_functions.triva_event_loop(start_no_stop);
  else
    {
#if HAS_CMVLC
      lwroc_triva_event_loop_cmvlc(start_no_stop);
#else
      lwroc_triva_event_loop(start_no_stop);
#endif
    }

  /* Do not block further data transmissions. */
  _lwroc_spill_state = 0;

  LWROC_LOG("Readout thread loop ended!");

  if (_lwroc_readout_status & LWROC_READOUT_STATUS_HELD)
    {
      /* Either we get told to get back to readout, or we should break
       * out.  In order to distinguish the readout request, we are
       * told GO_PREP, and we set PREPARED (then GO_READ and READOUT)
       * after that.
       */
      if (!lwroc_triva_readout_phase_wait(TRIVA_READOUT_PHASE_READOUT,
					  TRIVA_READOUT_PHASE_GO_PREP |
					  TRIVA_READOUT_PHASE_BREAKOUT,
					  _lwroc_readout_thread))
	return;

      if (_lwroc_readout_phase == TRIVA_READOUT_PHASE_GO_PREP)
	{
	  _lwroc_readout_status = 0; /* No longer held. */
	  goto readout_phase_prepare;
	}
    }

#if 0
  if (_lwroc_readout_status & LWROC_READOUT_STATUS_HELD)
    {
      /* Control should tell us HOLD, and we respond with HELD.  We
       * need to have a different state, such that we can respond to
       * getting READOUT state again.
       */
      if (!lwroc_triva_readout_phase_wait(TRIVA_READOUT_PHASE_READOUT,
					 TRIVA_READOUT_PHASE_HOLD |
					 TRIVA_READOUT_PHASE_BREAKOUT,
					 _lwroc_readout_thread))
	return;
    }

  if (_lwroc_readout_phase == TRIVA_READOUT_PHASE_HOLD)
    {
      _lwroc_readout_phase = TRIVA_READOUT_PHASE_HELD;
      MFENCE;
      /* Send a token to wake control up! */
      lwroc_thread_block_send_token(_lwroc_triva_control_thread->_block);

      /* Wait until we leave the held state. */
      if (!lwroc_triva_readout_phase_wait(TRIVA_READOUT_PHASE_HELD,
					  TRIVA_READOUT_PHASE_READOUT |
					  TRIVA_READOUT_PHASE_BREAKOUT,
					  _lwroc_readout_thread->_block))
	return;

      if (_lwroc_readout_phase == TRIVA_READOUT_PHASE_READOUT)
	goto readout_phase_readout;
    }
#endif

  if (_lwroc_readout_phase == TRIVA_READOUT_PHASE_BREAKOUT)
    {
      _lwroc_readout_phase = TRIVA_READOUT_PHASE_BROKEOUT;
      MFENCE;
      /* Send a token to wake control up! */
      lwroc_thread_block_send_token(_lwroc_triva_control_thread->_block);

      /* Wait until we leave the brokeout state. */
      if (!lwroc_triva_readout_phase_wait(TRIVA_READOUT_PHASE_BROKEOUT,
					  TRIVA_READOUT_PHASE_SETUP,
					  _lwroc_readout_thread))
	return;
    }
  else
    {
      LWROC_BUG_FMT("Bad readout phase after readout "
		    "(0x%x, expected 0x%x).",
		    _lwroc_readout_phase,
		    TRIVA_READOUT_PHASE_BREAKOUT);
    }

  /* Inject a (bad) ident event.  Purpose: make event-builder realise
   * that we will not provide further events.
   *
   * Solves a lock-up when we are a slave and master has lost our
   * deadtime.  Master may then have continued and generated loads of
   * data, filling its buffers completely.  Triva state machine
   * realises things are toast and tells everyone to abort.  We abort.
   * But master fails aborting, since it is waiting for data space to
   * read event out before it can break out of the readout loop.  But
   * buffer is full and event-builder is happily waiting...
   *
   * Alternative would be in master to during abort set a flag that
   * buffer space will not be available, and thus fail that request...
   */

  lwroc_triva_inject_ident(1);

  goto readout_phase_setup;
}

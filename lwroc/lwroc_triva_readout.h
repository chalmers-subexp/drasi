/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_TRIVA_READOUT_H__
#define __LWROC_TRIVA_READOUT_H__

struct cmvlc_stackcmdbuf;

void lwroc_triva_parse_setup(void);

void lwroc_triva_setup(void);

void lwroc_triva_no_setup(void);

void lwroc_triva_unmap_at_exit(void);

void lwroc_triva_map(void);

void lwroc_triva_readout(int *start_no_stop);

void lwroc_triva_silence_cmvlc(void);

void lwroc_triva_prepare_cmvlc(void);

void lwroc_triva_prepare_cmvlc_release_dt(struct cmvlc_stackcmdbuf *stack,
					  unsigned char readout_no);

#endif/*__LWROC_TRIVA_READOUT_H__*/

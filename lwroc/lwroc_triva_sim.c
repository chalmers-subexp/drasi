/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_message.h"
#include "lwroc_hostname_util.h"
#include "lwroc_parse_util.h"
#include "lwroc_select_util.h"
#include "lwroc_net_io_util.h"
#include "lwroc_triva_access.h"
#include "../trigbussim/trigbussim.h"

#include <netinet/tcp.h>
#include <string.h>

#include "../dtc_arch/acc_def/mystrndup.h"

int lwroc_triva_sim_pipe_connect(struct sockaddr_storage *serv_addr,
				 const char *descr)
{
  int fd;
  int ret;
  int flag;

  fd = socket(PF_INET, SOCK_STREAM, 0);

  if (fd == -1)
    {
      LWROC_PERROR("socket");
      LWROC_FATAL_FMT("Failed to create socket for '%s'.",descr);
    }

  /* Disable Nagle's algorithm - otherwise we are very slow!
   * Since we do multiple short writes after another.
   */
  flag = 1;
  setsockopt(fd, IPPROTO_TCP, TCP_NODELAY,
	     (char *) &flag, sizeof(int));

  ret = lwroc_net_io_connect(fd, serv_addr);

  if (ret == -1)
    {
      LWROC_PERROR("connect");
      LWROC_FATAL_FMT("Failed to connect '%s'.",descr);
    }

  return fd;
}

void lwroc_triva_setup_sim_pipe(const char *simulator)
{
  /* The TRIVA simulation is only used for testing.  Therefore we do
   * all the connection steps serially.  We also treat all errors as
   * fatal.
   */

  uint32_t modno = 1;

  struct sockaddr_storage serv_addr;
  struct sockaddr_storage data_addr;
  int fd;
  trigsimbus_init_msg msg;
  const char *at;
  uint16_t port;

  if (simulator &&
      (at = strchr(simulator,'@')) != NULL)
    {
      char *str_simmodno = strndup_chk(simulator, (size_t) (at - simulator));

      modno = lwroc_parse_hex_u16(str_simmodno, "sim. mod. no");
      simulator = at + 1;

      free(str_simmodno);
    }

  if (simulator == NULL ||
      !*simulator)
    simulator = "localhost";

  if (!lwroc_get_host_port(simulator, TRIGBUSSIM_PORT,
			   &serv_addr))
    LWROC_BADCFG("Bad TRIVA sim specification.");

  memset(&msg, 0, sizeof (msg));

  /* Open TRIVA sim portmapper. */
  fd = lwroc_triva_sim_pipe_connect(&serv_addr, "TRIVA sim portmap");

  /* Read the real port number. */
  lwroc_full_read(fd, &msg, sizeof (msg),
		  "TRIVA sim portmap", 1);
  lwroc_safe_close(fd);

  /* Check magic and extract the port. */
  if (ntohl(msg.magic) != TRIGBUSSIM_MAGIC)
    LWROC_FATAL_FMT("Bad magic (0x%08x) from TRIVA sim portmapper.",
		    ntohl(msg.magic));

  port = (uint16_t) ntohl(msg.port_mod);

  /* Connect to the same machine, but the data port. */
  lwroc_copy_sockaddr_port(&data_addr, &serv_addr, port);

  /* Prepare connection message. */
  msg.magic = htonl((uint32_t) ~TRIGBUSSIM_MAGIC);
  msg.port_mod = htonl(modno);

  /* Open connection to the data port. */
  fd = lwroc_triva_sim_pipe_connect(&data_addr, "TRIVA sim");

  /* Write the connection message. */
  lwroc_full_write(fd, &msg, sizeof (msg),
		   "TRIVA sim pipe", 1);

  /* And set the TRIVA routines to use the acquired simulator descriptor. */
  lwroc_triva_access_set_sim_pipe(fd);
}

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_triva_state.h"
#include "lwroc_triva_control.h"
#include "lwroc_triva_readout.h"
#include "lwroc_triva_kind.h"
#include "lwroc_net_trigbus.h"
#include "lwroc_message.h"
#include "lwroc_delayed_eb.h"
#include "lwroc_track_timestamp.h"
#include "lwroc_main_iface.h"
#include "lwroc_timeouts.h"

#include "lwroc_control.h"
#include "lwroc_net_proto.h"
#include "lwroc_net_io.h"

#include "gen/lwroc_acqr_request_serializer.h"

#include <string.h>
#include <assert.h>
#include <math.h>
#include "../dtc_arch/acc_def/fall_through.h"
#include "../dtc_arch/acc_def/myinttypes.h"

lwroc_control_buffer *_acqr_ctrl = NULL;

lwroc_triva_config  _lwroc_triva_config;

/* */

void lwroc_net_trigctrl_conn_monitor_fetch(void *ptr)
{
  lwroc_net_conn_monitor *conn_mon = (lwroc_net_conn_monitor *)
    (((char *) ptr) - offsetof(lwroc_net_conn_monitor, _block));

  /* Fills conn_mon->_block._buf_data_fill. */
  lwroc_net_conn_monitor_fetch(ptr);

  {
    /* It is a bit hacky to do the in-spill-stuck monitoring and mark in
     * the monitor fetch routine.  But we do not want it in the readout
     * (both to not use CPU there, and to make it happen even if/when we
     * get no triggers).
     *
     * This monitoring happens here first, without consulting presumed
     * acquisition state, as it should also happen for slaves, such
     * that they send data after in-spill stuck timeout.
     */
    uint32_t spill_state = _lwroc_spill_state; /* Copy since volatile. */
    int stuck = 0;

    if (spill_state == LWROC_SPILL_STATE_ON)
      {
	time_t now;
	time_t since = _lwroc_spill_state_since;

	now = time(NULL);

	/* Have we been marked as in-spill for a suspiciously
	 * long time?
	 */
	if (now < since ||
	    now > since + _config._inspill_stuck_timeout)
	  {
	    stuck = 1;
	    if (!_lwroc_spill_state_stuck)
	      LWROC_WARNING_FMT("In-spill since %d seconds.  Assuming stuck; "
				"delayed event-building disabled.",
				(int) (now - since));
	  }
      }
    _lwroc_spill_state_stuck = stuck;

    /* If we know the spill state, use that instead of just run status. */
    if (conn_mon->_block._aux_status == LWROC_TRIVA_STATUS_RUN)
      {
	if (spill_state)
	  {
	    if (stuck)
	      conn_mon->_block._aux_status = LWROC_TRIVA_STATUS_STUCK_INSPILL;
	    else if (spill_state == LWROC_SPILL_STATE_ON)
	      conn_mon->_block._aux_status = LWROC_TRIVA_STATUS_INSPILL;
	    else
	      conn_mon->_block._aux_status = LWROC_TRIVA_STATUS_OFFSPILL;
	  }
      }
  }

  /* We use the fact that this routine is called once in a while, to
   * pulse the timestamp collector.  (Does perhaps not really belong
   * here.)
   */
  _lwroc_timestamp_periodic++;
}

void lwroc_net_triva_state_conn_monitor_fetch(void *ptr)
{
  lwroc_net_conn_monitor *conn_mon = (lwroc_net_conn_monitor *)
    (((char *) ptr) - offsetof(lwroc_net_conn_monitor, _block));

  /*
  conn_mon->_block._buf_data_fill =
    lwroc_pipe_buffer_fill(conn_mon->_pipe_buf);
  */
  conn_mon->_block._aux_status =
    _lwroc_triva_state->_mon_status;
  conn_mon->_block._aux_last_error =
    _lwroc_triva_state->_mon_last_error;
  conn_mon->_block._aux_time_error._sec  =
    (uint64_t) _lwroc_triva_state->_mon_time_error.tv_sec;
  conn_mon->_block._aux_time_error._nsec  =
    (uint32_t) _lwroc_triva_state->_mon_time_error.tv_usec * 1000;

  /* This may override the status, with in-spill stuck info. */

  lwroc_net_trigctrl_conn_monitor_fetch(ptr);
}

void lwroc_net_notriva_state_conn_monitor_fetch(void *ptr)
{
  /* TODO: we should get the status from somewhere...
   *
   * To fill (some) fields as in lwroc_net_triva_state_conn_monitor_fetch().
   */

  /* This may override the status, with in-spill stuck info. */

  lwroc_net_trigctrl_conn_monitor_fetch(ptr);
}


/* Testing is a bit tricky, as we do not want to be without deadtime.
 * Therefore, we must before the last system releases deadtime set the
 * next pending trigger...
 */

lwroc_triva_state *_lwroc_triva_state = NULL;

lwroc_net_conn_monitor *
lwroc_triva_state_setup_conn_mon(lwroc_mon_block_fetch fetch_fcn,
				 uint32_t kind, uint32_t kind_sub)
{
  lwroc_data_pipe_handle *handle;
  lwroc_pipe_buffer_control *pipe_buf;
  lwroc_data_pipe_extra *pipe_extra;
  lwroc_net_conn_monitor *conn_mon;

  handle = lwroc_get_data_pipe("READOUT_PIPE");

  if (!handle)
    LWROC_FATAL("No handle for data pipe READOUT_PIPE.");

  pipe_buf = lwroc_data_pipe_get_pipe_ctrl(handle);

  /* For updating info from the readout, and pass along to the
   * monitor.  TODO: Should we really have this one?
   */
  pipe_extra = lwroc_data_pipe_get_pipe_extra(handle);

  lwroc_data_pipe_extra_clear(pipe_extra);

  conn_mon =
    lwroc_net_conn_monitor_init(LWROC_CONN_TYPE_SYSTEM,
				LWROC_CONN_DIR_OUTGOING,
				NULL, NULL,
				NULL, pipe_buf, NULL, NULL, pipe_extra, NULL,
				fetch_fcn,
				kind, kind_sub);

  /* LWROC_WARNING_FMT("ismaster: %d\n", _lwroc_triva_config._master); */

  if (_lwroc_triva_state)
    _lwroc_triva_state->_pipe_extra = pipe_extra;

  /* If we have made a promise regarding the max event interval,
   * then the stuck timeout must be smaller than that.
   *
   * Note: the stuck timeout is always active (0 is not disabled)!
   */

  {
    int max_event_interval =
      lwroc_data_pipe_get_max_ev_interval(handle);

    if (max_event_interval &&
	(_config._inspill_stuck_timeout +
	 LWROC_MERGE_SOURCE_DISABLE_INPUT_MARGIN) >
	max_event_interval)
      {
	LWROC_BADCFG_FMT("Refusing inspill stuck timeout "
			 "(%d s + margin %d s) > "
			 "max event interval (max-ev-interval) (%d s).",
			 _config._inspill_stuck_timeout,
			 LWROC_MERGE_SOURCE_DISABLE_INPUT_MARGIN,
			 max_event_interval);
      }
  }

  return conn_mon;
}

void lwroc_triva_state_setup(void)
{
  pd_ll_item *iter;

  _lwroc_triva_state =
    (lwroc_triva_state *) malloc(sizeof (lwroc_triva_state));

  if (!_lwroc_triva_state)
    LWROC_FATAL("Memory allocation error (triva state).");

  if (_lwroc_triva_config._master)
    _lwroc_triva_state->_state = LWROC_TRIVA_STATE_MASTER_INITIAL;
  else
    _lwroc_triva_state->_state = LWROC_TRIVA_STATE_SLAVE_INIT;

  PD_LL_INIT(&_lwroc_triva_state->_slaves);
  PD_LL_INIT(&_lwroc_triva_state->_ebs);
  PD_LL_INIT(&_lwroc_triva_state->_wait_slaves);
  PD_LL_INIT(&_lwroc_triva_state->_wait_slaves2);

  _lwroc_triva_state->_acq_state = LWROC_ACQ_STATE_REQ_RUN;

  _lwroc_triva_state->_had_issue = 0;
  _lwroc_triva_state->_stop_reason = 0;

  _lwroc_triva_state->delayed_eb_status =
    _lwroc_triva_config._delayed_eb_status;

  _lwroc_triva_state->_next_report.tv_sec = 0;

  _lwroc_triva_state->_mon_status = LWROC_TRIVA_STATUS_REINIT;
  _lwroc_triva_state->_mon_last_error = LWROC_TRIVA_FAIL_NONE;
  timerclear(&_lwroc_triva_state->_mon_time_error);

  timerclear(&_lwroc_triva_state->_measure_dt_end);

  {
    lwroc_net_conn_monitor *conn_mon;

    conn_mon =
      lwroc_triva_state_setup_conn_mon(lwroc_net_triva_state_conn_monitor_fetch,
				       LWROC_CONN_KIND_TRIVA/* :
				       LWROC_CONN_KIND_TRIMI*/,
				       _lwroc_triva_config._master ?
				       LWROC_CONN_KIND_SUB_MASTER :
				       LWROC_CONN_KIND_SUB_SLAVE);

    _lwroc_triva_state->_triva_ctrl =
      lwroc_trigctrl_conn(conn_mon);
  }

  _lwroc_triva_state->_master = NULL;

  _lwroc_triva_state->_master_abort_type = 0;
  _lwroc_triva_state->_slave_abort_type = 0;

  timerclear(&_lwroc_triva_state->_init_allowance_reftime);
  _lwroc_triva_state->_init_allowance = 0.;
  timerclear(&_lwroc_triva_state->_next_init);

  if (_lwroc_triva_config._master &&
      !PD_LL_IS_EMPTY(&_lwroc_net_masters))
    LWROC_BADCFG("TRIVA/MI is master, "
		 "but (external) master drasi instance given.");

  PD_LL_FOREACH(_lwroc_net_masters, iter)
    {
      lwroc_net_trigbus *conn =
	PD_LL_ITEM(iter, lwroc_net_trigbus, _masters);

      /* TODO: we take first master.  We should check and only have
       * one master.  The one being a local triva/mi master.
       *
       * Do this by only allowing one and instead have a list of
       * time-stamp masters.
       */
      _lwroc_triva_state->_master = conn;
      break;
    }

  if (!_lwroc_triva_config._master &&
      !_lwroc_triva_state->_master)
    LWROC_BADCFG("TRIVA/MI is slave, but no master drasi instance given.");

  if (!_lwroc_triva_config._master &&
      !PD_LL_IS_EMPTY(&_lwroc_net_slaves))
    LWROC_BADCFG("TRIVA/MI is slave, "
		 "but (external) slave drasi instance given.");

  if (!_lwroc_triva_config._master &&
      !PD_LL_IS_EMPTY(&_lwroc_net_ebs))
    LWROC_BADCFG("TRIVA/MI is slave, "
		 "but (external) event builder drasi instance given.");

  /* printf ("add slaves to triva_state...\n"); */

  PD_LL_FOREACH(_lwroc_net_slaves, iter)
    {
      lwroc_net_trigbus *conn =
	PD_LL_ITEM(iter, lwroc_net_trigbus, _slaves_lst);

      /* printf ("slave!\n"); */

      PD_LL_ADD_BEFORE(&_lwroc_triva_state->_slaves, &conn->_slaves);
    }

  PD_LL_FOREACH(_lwroc_net_ebs, iter)
    {
      lwroc_net_trigbus *conn =
	PD_LL_ITEM(iter, lwroc_net_trigbus, _slaves_lst);

      /* printf ("e!\n"); */

      PD_LL_ADD_BEFORE(&_lwroc_triva_state->_ebs, &conn->_slaves);
    }

  if (_lwroc_triva_config._master)
    {
      _acqr_ctrl =
	lwroc_alloc_control_buffer(LWROC_CONTROL_OP_ACQ_READOUT, 0x400);

      /* We cannot set _thread_notify_ready here, due to blocker object
       * not existing yet.
       */
      _acqr_ctrl->_state = LWROC_CONTROL_STATE_INIT;

      _lwroc_triva_state->_acqr_req = malloc(sizeof (lwroc_acqr_request));

      if (!_lwroc_triva_state->_acqr_req)
	LWROC_FATAL("Memory allocation error (triva state acqr request).");
    }
}

/* TODO: should this global and following function really be here? */
lwroc_net_conn_monitor *_lwroc_mon_main_system_handle = NULL;

void lwroc_triva_state_no_setup(void)
{
  if (!PD_LL_IS_EMPTY(&_lwroc_net_masters))
    LWROC_BADCFG("No TRIVA/MI setup, "
		 "but (external) master drasi instance given.");
  if (!PD_LL_IS_EMPTY(&_lwroc_net_slaves))
    LWROC_BADCFG("No TRIVA/MI setup, "
		 "but (external) slave drasi instance given.");
  if (!PD_LL_IS_EMPTY(&_lwroc_net_ebs))
    LWROC_BADCFG("No TRIVA/MI setup, "
		 "but (external) event builder drasi instance given.");

  if (!PD_LL_IS_EMPTY(&_lwroc_net_eb_masters))
    LWROC_BADCFG("Not in merge mode, but event builder master given.");

  _lwroc_mon_main_system_handle =
    lwroc_triva_state_setup_conn_mon(lwroc_net_notriva_state_conn_monitor_fetch,
				     LWROC_CONN_KIND_UNTRIG, 0);
}

/* TODO: This is ugly and should be fixed some other way! */
const char *_lwroc_prev_report_reason = NULL;

void lwroc_report_wait_slaves(struct timeval *now, const char *reason,
			      uint32_t mon_status_wait)
{
  /* We get called when the state machine is waiting for some machines
   * before it can proceed.
   *
   * We take the approach that we start reporting the status after 1 s
   * of delay.  The interval of the reports as then doubled for each
   * report, with the second report after 5 s.  If there is a change
   * in which slaves are being waited for, we report that after 1 s
   * (in order to combine multiple slaves starting to respond).
   */

  lwroc_triva_state *ts = _lwroc_triva_state;
  pd_ll_item *iter;

  /* printf ("wait for %s\n", reason); */

  if (ts->_next_report.tv_sec == 0 ||
      reason != _lwroc_prev_report_reason)
    {
      ts->_report_interval = 1;
      ts->_next_report = *now;
      ts->_next_report.tv_sec += ts->_report_interval;
      _lwroc_prev_report_reason = reason;
      return;
    }

  if (timercmp(now, &ts->_next_report, <))
    return;

  if (mon_status_wait)
    ts->_mon_status = mon_status_wait;

  LWROC_WARNING_FMT("Waited %d seconds for %s:", ts->_report_interval, reason);

  PD_LL_FOREACH(ts->_wait_slaves, iter)
    {
      lwroc_net_trigbus *slave =
	PD_LL_ITEM(iter, lwroc_net_trigbus, _wait_slaves);

      if (slave->_out._hostname)
	LWROC_CWARNING_FMT(&slave->_out._base, "(state %d)", slave->_state);
      else
	LWROC_WARNING_FMT("Master: (state %d)", slave->_state);

      (void) slave;
    }

  ts->_report_interval =
    (ts->_report_interval < 5 ? 5 : ts->_report_interval * 2);
  ts->_next_report = *now;
  ts->_next_report.tv_sec += ts->_report_interval;
}

#define LWROC_TRIVA_TS_MSG_PREPARE(msg, ts, type) do {		\
    LWROC_TRIVA_CTRL_MSG_PREPARE(msg, type);			\
    (msg)->delayed_eb_status = (ts)->delayed_eb_status;		\
    if (type == LWROC_TRIVA_CONTROL_RUN_STATS) {		\
      if (timerisset(&_lwroc_triva_state->_measure_dt_end))	\
	(msg)->deadtime = LWROC_TRIVA_DEADTIME_MEASURE;		\
    }								\
  } while (0)

#define SET_MON_STATUS(mon_status) do {		\
    ts->_mon_status = mon_status;		\
    ts->_mon_status_nonwait = mon_status;	\
  } while (0)

#define SET_MON_STATUS_NONWAIT do {		\
    ts->_mon_status = ts->_mon_status_nonwait;	\
  } while (0)

#define GOTO_HAD_ISSUE(had_issue_label, mon_state_error) do { \
    ts->_mon_last_error = mon_state_error;		 \
    ts->_mon_time_error = *now;				 \
    goto had_issue_label;				 \
  } while (0)

#define WAIT_SLAVES_CHECK_ISSUE(reason, had_issue_label,		\
				mon_status_wait, mon_state_error) do {	\
    if (!PD_LL_IS_EMPTY(&ts->_wait_slaves))				\
      {									\
	lwroc_report_wait_slaves(now, reason, mon_status_wait);		\
	return;								\
      }									\
    if (ts->_had_issue) {						\
      GOTO_HAD_ISSUE(had_issue_label, mon_state_error);			\
    }									\
    SET_MON_STATUS_NONWAIT;						\
  } while (0)

uint32_t _lwroc_next_eb_ident_id = 0;

void lwroc_send_ident(lwroc_triva_state *ts,
		      lwroc_net_trigbus *conn,
		      struct timeval *now,
		      pd_ll_item *wait_slaves,
		      int master)
{
  LWROC_TRIVA_TS_MSG_PREPARE(&conn->_msg, ts,
			     LWROC_TRIVA_CONTROL_INJECT_IDENT);

  /* Using seconds as well as usecs, since these messages can
   * lurk around for a long time.
   */
  /* TODO: some stuff based on our IP number would be good too... */

  conn->_eb_ident[0] = _lwroc_next_eb_ident_id++ |
    0x80000000 /* ensure non-zero */;
  conn->_eb_ident[1] = (uint32_t) now->tv_sec;
  conn->_eb_ident[2] = (uint32_t) now->tv_usec;

  /* Do not actually inject a message if there is no event builder
   * listening...
   *
   * We however go through all state change motions as usual (easiest).
   */
  if (PD_LL_IS_EMPTY(&ts->_ebs))
    conn->_eb_ident[0] = 0;

  conn->_msg._eb_ident0 = conn->_eb_ident[0];
  conn->_msg._eb_ident1 = conn->_eb_ident[1];
  conn->_msg._eb_ident2 = conn->_eb_ident[2];
  /*
  printf ("Set %s IDENT %08x %08x %08x\n",
	  master ? "master" : "slave",
	  conn->_eb_ident[0],
	  conn->_eb_ident[1],
	  conn->_eb_ident[2]);
  */
  if (master)
    lwroc_net_trigctrl_send_msg(conn);
  else
    lwroc_net_trigbus_send_msg(conn, 0);

  conn->_eb_ident_state = LWROC_EB_IDENT_STATE_SENT;

  PD_LL_ADD_BEFORE(wait_slaves, &conn->_wait_slaves);
}

int lwroc_ident_match(lwroc_net_trigbus *eb,
		      lwroc_net_trigbus *conn)
{
  if (conn->_eb_ident_state != LWROC_EB_IDENT_STATE_SENT)
    return 0;

  if (eb->_msg._eb_ident0 == conn->_eb_ident[0] &&
      eb->_msg._eb_ident1 == conn->_eb_ident[1] &&
      eb->_msg._eb_ident2 == conn->_eb_ident[2])
    {
      conn->_eb_ident_state = LWROC_EB_IDENT_STATE_USED;
      return 1;
    }
  return 0;
}

#define LWROC_TEST_TRIG(test_no) ((((test_no) & ~0x80000000) % 15) + 1)
#define LWROC_TEST_EC(test_no)   ((test_no) & 0x1f)

void lwroc_ts_relase_dt_trig_msg(lwroc_triva_state *ts,
				 lwroc_net_trigbus *conn,
				 int master)
{
  /* printf ("lwroc_ts_relase_dt_trig_msg: %d %d\n", ts->_test_no, master); */
  if (ts->_test_no & 0x80000000)
    return;
  if (!master)
    return;
  /* Next trigger is with number incremented by 1. */
  conn->_msg._trig = LWROC_TEST_TRIG(ts->_test_no + 1);
  /* printf ("--> trig=%d\n",conn->_msg._trig); */
}

typedef void (*lwroc_ts_set_msg_func)(lwroc_triva_state *ts,
				      lwroc_net_trigbus *conn,
				      int master);

void lwroc_triva_state_send_msg(lwroc_triva_state *ts,
				lwroc_net_trigbus *conn,
				int master,
				uint32_t msg_type,
				pd_ll_item *wait_slaves,
				lwroc_ts_set_msg_func set_msg_func)
{
  (void) ts;

  LWROC_TRIVA_TS_MSG_PREPARE(&conn->_msg, ts, msg_type);

  if (set_msg_func)
    set_msg_func(ts, conn, master);

  if (master)
    lwroc_net_trigctrl_send_msg(conn);
  else
    lwroc_net_trigbus_send_msg(conn, 0);

  PD_LL_ADD_BEFORE(wait_slaves, &conn->_wait_slaves);
}

void lwroc_triva_state_send_msgs_except(lwroc_triva_state *ts,
					uint32_t msg_type,
					int except_slave,
					lwroc_net_trigbus **excepted_slave,
					lwroc_ts_set_msg_func set_msg_func)
{
  pd_ll_item *iter;
  int islave = 0;

  if (except_slave == 0)
    *excepted_slave = ts->_triva_ctrl;
  else
    lwroc_triva_state_send_msg(ts, ts->_triva_ctrl, 1, msg_type,
			       &ts->_wait_slaves,
			       set_msg_func);

  PD_LL_FOREACH(ts->_slaves, iter)
    {
      lwroc_net_trigbus *slave =
	PD_LL_ITEM(iter, lwroc_net_trigbus, _slaves);
      islave++;

      if (except_slave == islave)
	*excepted_slave = slave;
      else
	lwroc_triva_state_send_msg(ts, slave, 0, msg_type,
				   &ts->_wait_slaves,
				   set_msg_func);
    }
}

void lwroc_triva_state_send_msgs(lwroc_triva_state *ts,
				 uint32_t msg_type)
{
  lwroc_triva_state_send_msgs_except(ts, msg_type, -1, NULL, NULL);
}

int lwroc_triva_check_test_trig(lwroc_triva_state *ts,
				lwroc_net_trigbus *conn,
				int master)
{
  if (conn->_msg._trig == LWROC_TEST_TRIG(ts->_test_no) &&
      conn->_msg.event_counter == LWROC_TEST_EC(ts->_test_no) &&
      !(conn->_msg.status & ~(LWROC_READOUT_STATUS_TERM_REQ)))
    return 0;

  LWROC_CERROR_FMT(&conn->_out._base,
		  "Got bad test trigger in %s '%s' "
		  "(trig=%d, ec=%d, status=%d), "
		  "expected (trig=%d, ec=%d, status=%d).",
		  master ? "master" : "slave",
		  conn->_out._hostname,
		  conn->_msg._trig,
		  (uint32_t) conn->_msg.event_counter,
		  conn->_msg.status,
		  LWROC_TEST_TRIG(ts->_test_no),
		  LWROC_TEST_EC(ts->_test_no),
		  0);
  return 1;
}

int lwroc_triva_have_term_request(lwroc_triva_state *ts)
{
  pd_ll_item *iter;

  if (ts->_triva_ctrl->_term_request)
    return 1;

  PD_LL_FOREACH(ts->_slaves, iter)
    {
      lwroc_net_trigbus *slave =
	PD_LL_ITEM(iter, lwroc_net_trigbus, _slaves);

      if (slave->_term_request)
	return 1;
    }

  PD_LL_FOREACH(ts->_ebs, iter)
    {
      lwroc_net_trigbus *eb =
	PD_LL_ITEM(iter, lwroc_net_trigbus, _slaves);

      if (eb->_term_request)
	return 1;
    }

  return 0;
}

uint32_t lwroc_triva_state_status_2_failure(uint32_t status)
{
  if (status & LWROC_READOUT_STATUS_MISMATCH)
    return LWROC_TRIVA_FAIL_MISMATCH;
  if (status & LWROC_READOUT_STATUS_EC_MISMATCH)
    return LWROC_TRIVA_FAIL_EC_MISMATCH;
  if (status & LWROC_READOUT_STATUS_TRIG_UNEXPECT)
    return LWROC_TRIVA_FAIL_TRIG_UNEXPECT;
  if (status & LWROC_READOUT_STATUS_SEQ_ERROR)
    return LWROC_TRIVA_FAIL_SEQ_ERROR;
  if (status & LWROC_READOUT_STATUS_SEQ_UNEXPECT)
    return LWROC_TRIVA_FAIL_SEQ_UNEXPECT;
  if (status & LWROC_READOUT_STATUS_SEQ_MALFORM)
    return LWROC_TRIVA_FAIL_SEQ_MALFORM;
  if (status & LWROC_READOUT_STATUS_STHR_BUG_FATAL)
    return LWROC_TRIVA_FAIL_STHR_BUG_FATAL;
  LWROC_BUG_FMT("Unexpected failure status %x.", status);
  return 0;
}

#define LWROC_TRIVA_STR_ADD_READOUT_STATUS(x,y) do {	\
    if (status & LWROC_READOUT_STATUS_##x) {		\
      if (strlen(str) > 2) 				\
	strcat(str,", ");				\
      strcat(str,y);					\
      status &= ~LWROC_READOUT_STATUS_##x;		\
    }							\
  } while (0)

const char *lwroc_triva_str_readout_status(char *str,
					   uint32_t status)
{
  *str = 0;

  if (!status)
    return str;

  strcat(str, " (");
  LWROC_TRIVA_STR_ADD_READOUT_STATUS(MISMATCH      , "MISMATCH"      );
  LWROC_TRIVA_STR_ADD_READOUT_STATUS(EC_MISMATCH   , "EC_MISMATCH"   );
  LWROC_TRIVA_STR_ADD_READOUT_STATUS(TRIG_UNEXPECT , "TRIG_UNEXPECT" );
  LWROC_TRIVA_STR_ADD_READOUT_STATUS(MISSING_DT    , "MISSING_DT"    );
  LWROC_TRIVA_STR_ADD_READOUT_STATUS(IN_READOUT    , "IN_READOUT"    );
  LWROC_TRIVA_STR_ADD_READOUT_STATUS(HEADACHE_CUR  , "HEADACHE_CUR"  );
  LWROC_TRIVA_STR_ADD_READOUT_STATUS(HEADACHE      , "HEADACHE"      );
  LWROC_TRIVA_STR_ADD_READOUT_STATUS(WAIT_BUFSPACE , "WAIT_BUFSPACE" );
  LWROC_TRIVA_STR_ADD_READOUT_STATUS(HELD          , "HELD"          );
  LWROC_TRIVA_STR_ADD_READOUT_STATUS(TERM_REQ      , "TERM_REQ"      );
  LWROC_TRIVA_STR_ADD_READOUT_STATUS(STHR_BUG_FATAL, "BUG_FATAL"     );
  if (status)
    strcat(str," + *UNKNOWN*");
  strcat(str, ")");
  return str;
}

void lwroc_triva_fill_meas_dt(lwroc_trigbus_msg *msg)
{
  _lwroc_mon_net._meas_dt_2events =     msg->_eb_ident0;
  _lwroc_mon_net._meas_dt_us_poll =     msg->_eb_ident1;
  _lwroc_mon_net._meas_dt_us =          msg->_eb_ident2;
  _lwroc_mon_net._meas_dt_us_postproc = msg->_eb_ident3;
  _lwroc_mon_net._meas_dt_ctime =       msg->ctime;
}

void lwroc_triva_state_update(struct timeval *now)
{
  pd_ll_item *iter, *tmp_iter;
  pd_ll_item *iter2;

  lwroc_triva_state *ts = _lwroc_triva_state;

  /* Does some slave want to terminate.  Then we go for acq stop mode,
   * such that it can terminate in a controlled fashion.
   */

  /*
  if (_lwroc_triva_state->_stop_reason & LWROC_ACQ_STOP_REASON_TERMINATE)
    printf ("stop_reason & TERMINATE: ts->_acq_state = %08x\n",
	    ts->_acq_state);
  */

  if (_acqr_ctrl &&
      (ts->_stop_reason & LWROC_ACQ_STOP_REASON_TERMINATE) &&
      ts->_acq_state == LWROC_ACQ_STATE_REQ_RUN)
    {
      LWROC_ACTION("Someone wants to terminate - stop acq.");
      ts->_acq_state = LWROC_ACQ_STATE_REQ_STOP;
    }

  /* Do we have a control request? */

  if (_acqr_ctrl &&
      (_acqr_ctrl->_state & LWROC_CONTROL_STATE_USER_ACTION_MARK))
  switch (_acqr_ctrl->_state)
    {
      /* Note: there are other states too (while control requests are
       * being received by the network thread).  We do not react on
       * those!
       */
    case LWROC_CONTROL_STATE_MESSAGE_READY:
    {
      lwroc_deserialize_error desererr;
      const char *end;

      /* Prepare a response. */

      _acqr_ctrl->_response._tag    = _acqr_ctrl->_request._tag;
      _acqr_ctrl->_response._op     = _acqr_ctrl->_request._op;
      _acqr_ctrl->_response._value  = 0;
      _acqr_ctrl->_response._response_data_size = 0;
      LWROC_SET_CONTROL_STATUS(_acqr_ctrl, UNKNOWN,
			       PERFORMER, "Unknown control request.");

      /* Can we parse the request? */

      end =
	lwroc_acqr_request_deserialize(ts->_acqr_req,
				       _acqr_ctrl->_request_data,
				       _acqr_ctrl->_request.
				       /* */ _request_data_size,
				       &desererr);

      if (end == NULL)
	{
	  LWROC_SET_CONTROL_STATUS(_acqr_ctrl, INVALID,
				   PERFORMER,
				   "Invalid control message.");
	  goto message_send;
	}
      /*
      printf ("%d %x\n",
	      ts->_acq_state,
	      _acqr_ctrl->_response._status);
      */
      switch (ts->_acqr_req->_op)
	{
	case LWROC_ACQR_OP_STATUS:
	  /* Just return status. */
	  LWROC_SET_CONTROL_STATUS(_acqr_ctrl, SUCCESS,
				   PERFORMER, NULL);
	  break;
	case LWROC_ACQR_OP_START:
	  if (ts->_acq_state == LWROC_ACQ_STATE_REQ_HOLD)
	    {
	      ts->_acq_state = LWROC_ACQ_STATE_REQ_START;
	      /* We may also have a stop reason due to terminate, but
	       * that will clear itself as the slave disconnects and
	       * reconnects.  The start operation as such will likely
	       * fail due to timeout though.  Well, user did the
	       * termination, so deserves strange messages, no?
	       */
	      ts->_stop_reason &= ~LWROC_ACQ_STOP_REASON_CONTROL;
	    }
	  else
	    {
	      LWROC_SET_CONTROL_STATUS(_acqr_ctrl, ALREADY,
				       PERFORMER,
				       "Acquisition already started.");
	    }
	  break;
	case LWROC_ACQR_OP_STOP:
	  if (ts->_acq_state == LWROC_ACQ_STATE_REQ_RUN)
	    {
	      ts->_acq_state = LWROC_ACQ_STATE_REQ_STOP;
	      ts->_stop_reason |= LWROC_ACQ_STOP_REASON_CONTROL;
	    }
	  else
	    {
	      LWROC_SET_CONTROL_STATUS(_acqr_ctrl, ALREADY,
				       PERFORMER,
				       "Acquisition already stopped.");
	    }
	  break;
	case LWROC_ACQR_OP_MEAS_DT:
	  _lwroc_triva_state->_measure_dt_start = *now;
	  _lwroc_triva_state->_measure_dt_end   = *now;
	  _lwroc_triva_state->_measure_dt_end.tv_sec += 15;
	  LWROC_SET_CONTROL_STATUS(_acqr_ctrl, SUCCESS,
				   PERFORMER, NULL);
	  break;
	}
      /*
      printf ("%d %x\n",
	      ts->_acq_state,
	      _acqr_ctrl->_response._status);
      */
      /* If status is unknown, then we do not have a response yet. */
      if (_acqr_ctrl->_response._status != LWROC_CONTROL_STATUS_UNKNOWN)
	goto message_send;
      _acqr_ctrl->_state = LWROC_CONTROL_STATE_MESSAGE_WORKING;
      break;
    }
    case LWROC_CONTROL_STATE_MESSAGE_WORKING:

      switch (ts->_acqr_req->_op)
	{
	case LWROC_ACQR_OP_START:
	  if (ts->_acq_state == LWROC_ACQ_STATE_REQ_STARTED)
	    {
	      ts->_acq_state = LWROC_ACQ_STATE_REQ_RUN;
	      LWROC_SET_CONTROL_STATUS(_acqr_ctrl, SUCCESS,
				       PERFORMER, NULL);
	    }
	  if (ts->_acq_state == LWROC_ACQ_STATE_REQ_START_REINIT)
	    {
	      ts->_acq_state = LWROC_ACQ_STATE_REQ_HOLD;
	      LWROC_SET_CONTROL_STATUS(_acqr_ctrl, FAILED,
				       PERFORMER,
				       "Acquisition start failed.");
	    }
	  break;
	case LWROC_ACQR_OP_STOP:
	  if (ts->_acq_state == LWROC_ACQ_STATE_REQ_STOPPED)
	    {
	      ts->_acq_state = LWROC_ACQ_STATE_REQ_HOLD;
	      LWROC_SET_CONTROL_STATUS(_acqr_ctrl, SUCCESS,
				       PERFORMER, NULL);
	    }
	  if (ts->_acq_state == LWROC_ACQ_STATE_REQ_STOP_REINIT)
	    {
	      ts->_acq_state = LWROC_ACQ_STATE_REQ_HOLD;
	      LWROC_SET_CONTROL_STATUS(_acqr_ctrl, FAILED,
				       PERFORMER,
				       "Acquisition stop failed.");
	    }
	  break;
	}
      /* If status is unknown, then we do not have a response yet. */
      if (_acqr_ctrl->_response._status == LWROC_CONTROL_STATUS_UNKNOWN)
	break;

      FALL_THROUGH;

      /* We are supposed to send the control message response. */
    case LWROC_CONTROL_STATE_MESSAGE_SEND:
      ;
    message_send:
      /* Tell if we are (in principle) running as status value. */
      _acqr_ctrl->_response._value =
	(uint32_t) (ts->_acq_state == LWROC_ACQ_STATE_REQ_RUN);

      ts->_acqr_req->_op = 0;
      ts->_acqr_req->_events =
	_lwroc_triva_state->_pipe_extra->_events;

      {
	size_t sz_response;

	sz_response =
	  lwroc_acqr_request_serialized_size();

	if (sz_response > _acqr_ctrl->_request._response_data_maxsize)
	  {
	    LWROC_SET_CONTROL_STATUS(_acqr_ctrl, TOO_BIG,
				     PERFORMER,
				     "Response structure too large.");
	  }
	else
	  {
	    lwroc_acqr_request_serialize(_acqr_ctrl->_response_data,
					 ts->_acqr_req);
	    _acqr_ctrl->_response._response_data_size =
	      (uint32_t) sz_response;
	  }

	/*
	  printf ("response: %d\n", _acqr_ctrl->_response._response_data_size);
	*/
      }

      lwroc_control_done(_acqr_ctrl, _lwroc_net_io_thread->_block);
      break;

    case LWROC_CONTROL_STATE_INIT:
      _acqr_ctrl->_thread_notify_ready = _lwroc_net_io_thread->_block;
      _acqr_ctrl->_state = LWROC_CONTROL_STATE_WAIT_MESSAGE;
      break;

    default:
      /* If we are not currently waiting to finish a message, we may
       * need to bump the acquisition state, as would be done when we
       * finish handling a message.
       */
      if (ts->_acq_state == LWROC_ACQ_STATE_REQ_STARTED)
	ts->_acq_state = LWROC_ACQ_STATE_REQ_RUN;
      if (ts->_acq_state == LWROC_ACQ_STATE_REQ_START_REINIT)
	ts->_acq_state = LWROC_ACQ_STATE_REQ_HOLD;
      if (ts->_acq_state == LWROC_ACQ_STATE_REQ_STOPPED)
	ts->_acq_state = LWROC_ACQ_STATE_REQ_HOLD;
      if (ts->_acq_state == LWROC_ACQ_STATE_REQ_STOP_REINIT)
	ts->_acq_state = LWROC_ACQ_STATE_REQ_HOLD;

      break;
    }

  /*
  printf ("lwroc_triva_state_update: %d\n", ts->_state);
  fflush(stdout);
  */

  switch (ts->_state)
    {
    init_master_had_issue:
      LWROC_ERROR("TRIVA/TRIGGER master BUS failure to initialise, "
		  "start over.");

    init_master_restart:
      ts->_state = LWROC_TRIVA_STATE_MASTER_INITIAL;
      ts->_had_issue = 0;

      /* Print before the case, so we only report once. */
      if (ts->_stop_reason)
	LWROC_ACTION_FMT("Acquisition held by %s request.",
			 ts->_stop_reason & LWROC_ACQ_STOP_REASON_CONTROL ?
			 "control" :
			 ts->_stop_reason & LWROC_ACQ_STOP_REASON_TERMINATE ?
			 "slave/eb termination" : "unknown");

      /* Rate limitation.  We keep an allowance value.  After passing
       * each time, it is incremented by one.  It then decays
       * exponentially with time.  We are not allowed to pass until
       * the value has passed below 1.  This allows two rapid attempts
       * at startup, and then kicks in limiting.
       */

      {
	if (timerisset(&ts->_init_allowance_reftime) &&
	    !timercmp(now, &ts->_init_allowance_reftime, <))
	  {
	    struct timeval tdiff;
	    double elapsed;

	    /* Do the exponential decay until the current time. */

	    timersub(now, &ts->_init_allowance_reftime, &tdiff);
	    elapsed = (double) (tdiff.tv_sec) + (1.e-6 * (int) tdiff.tv_usec);

	    /* Wait a factor 0.35, we allow a reinit about every second. */
	    ts->_init_allowance *= exp(-elapsed * 0.35);
	    /*
	    printf ("Init allowance decay(%.6f s) -> %.3f.\n",
		    elapsed, ts->_init_allowance);
	    */
	  }
	/* In either case, update reftime. */
	ts->_init_allowance_reftime = *now;

	if (ts->_init_allowance > 1.)
	  {
	    struct timeval tdiff;
	    double wait;
	    int seconds;

	    /* How long until we reach the value 1 again? */

	    wait = log(ts->_init_allowance);
	    /*
	    printf ("Init allowance %.3f -> wait %.6f s.\n",
		    ts->_init_allowance, wait);
	    */
	    tdiff.tv_sec = seconds = (int) (floor(wait) + 0.5);
	    tdiff.tv_usec = (int) (1.e6 * (wait - seconds));

	    timeradd(now, &tdiff, &ts->_next_init);

	    LWROC_LOG_FMT("Rate limiting reinit, next attempt in %.3f s.",
			  wait);
	  }
	else
	  timerclear(&ts->_next_init);

	ts->_init_allowance += 1.;
	/*
	printf ("Init allowance +1 -> %.3f.\n", ts->_init_allowance);
	*/
      }
      FALL_THROUGH;

    case LWROC_TRIVA_STATE_MASTER_INITIAL:
      ;
    master_initial:
      if (ts->_stop_reason)
	{
	  if ((ts->_stop_reason & LWROC_ACQ_STOP_REASON_TERMINATE))
	    {
	      if (!lwroc_triva_have_term_request(ts))
		{
		  /* printf ("NO TERM REQUEST\n"); */
		  ts->_stop_reason &= ~LWROC_ACQ_STOP_REASON_TERMINATE;
		  if (!ts->_stop_reason)
		    {
		      ts->_acq_state = LWROC_ACQ_STATE_REQ_RUN;
		      goto master_initial;
		    }
		}
	      else
		ts->_mon_status = LWROC_TRIVA_STATUS_WAIT_TERM;
	    }
	  /* printf ("TERM REQUEST pending\n"); */
	  break;
	}
      if (timerisset(&ts->_next_init) &&
	  timercmp(now, &ts->_next_init, <))
	break;

      /* First make sure that the slave-wait list is empty. */

      PD_LL_INIT(&ts->_wait_slaves);
      PD_LL_INIT(&ts->_wait_slaves2);

      /* Setup wait for all slaves to be connected. */

      /* printf ("add slaves for check...\n"); */

      PD_LL_FOREACH(ts->_slaves, iter)
	{
	  lwroc_net_trigbus *slave =
	    PD_LL_ITEM(iter, lwroc_net_trigbus, _slaves);

	  PD_LL_ADD_BEFORE(&ts->_wait_slaves, &slave->_wait_slaves);
	}

      /* Also setup wait for all EBs to be connected. */

      /* printf ("add ebs for check...\n"); */

      PD_LL_FOREACH(ts->_ebs, iter)
	{
	  lwroc_net_trigbus *eb =
	    PD_LL_ITEM(iter, lwroc_net_trigbus, _slaves);

	  PD_LL_ADD_BEFORE(&ts->_wait_slaves, &eb->_wait_slaves);
	}

      SET_MON_STATUS(LWROC_TRIVA_STATUS_REINIT);
      ts->_state = LWROC_TRIVA_STATE_MASTER_WAIT_SLAVE_CONN;

      if (!PD_LL_IS_EMPTY(&ts->_wait_slaves))
	break;

      FALL_THROUGH; /* No slave? - then just continue. */

    case LWROC_TRIVA_STATE_MASTER_WAIT_SLAVE_CONN:
      /* Wait for all slaves and EBs to be connected. */

      /* printf ("check slaves...\n"); */

      PD_LL_FOREACH_TMP(ts->_wait_slaves, iter, tmp_iter)
	{
	  lwroc_net_trigbus *slave =
	    PD_LL_ITEM(iter, lwroc_net_trigbus, _wait_slaves);

	  /* A slave is good if it is connected, and waiting for a
	   * message to be sent.
	   *
	   * The same things applies to event builders.
	   */

	  /* printf ("slave!\n"); */

	  if (slave->_status == LWROC_NET_TRIGBUS_STATUS_SLAVE)
	    {
	      if (slave->_state == LWROC_NET_TRIGBUS_STATE_FRESH_SLAVE)
		slave->_state = LWROC_NET_TRIGBUS_STATE_WAIT_MSG_SEND;
	      if (slave->_state == LWROC_NET_TRIGBUS_STATE_WAIT_MSG_SEND)
		{
		  PD_LL_REMOVE(iter);
		  /* Right now, we have no known issues with this slave/EB. */
		  slave->_aux_status = 0;
		}
	    }
	}

      if (!PD_LL_IS_EMPTY(&ts->_wait_slaves))
	{
	  lwroc_report_wait_slaves(now, "initial slave and EB connection(s)",
				   LWROC_TRIVA_STATUS_WAIT_CONN);
	  break;
	}
      SET_MON_STATUS_NONWAIT;

      /* printf ("slaves checked...\n"); */

      ts->_state = LWROC_TRIVA_STATE_EB_CLEAR;
      FALL_THROUGH;

    case LWROC_TRIVA_STATE_EB_CLEAR:
      /* Send request to all EBs to clear all validated sources.
       * Since we have not sent our identifications yet, all
       * identifications received up to this point are invalid.
       */

      PD_LL_FOREACH(ts->_ebs, iter)
	{
	  lwroc_net_trigbus *eb =
	    PD_LL_ITEM(iter, lwroc_net_trigbus, _slaves);

	  LWROC_TRIVA_TS_MSG_PREPARE(&eb->_msg, ts,
				     LWROC_TRIVA_CONTROL_EB_INVALIDATE_IDENT);

	  lwroc_net_trigbus_send_msg(eb,
				     LWROC_TRIVA_CONTROL_EB_INVALIDATED_IDENT);

	  PD_LL_ADD_BEFORE(&ts->_wait_slaves, &eb->_wait_slaves);
	}

      ts->_state = LWROC_TRIVA_STATE_WAIT_EB_CLEAR;
      FALL_THROUGH;

    case LWROC_TRIVA_STATE_WAIT_EB_CLEAR:

      WAIT_SLAVES_CHECK_ISSUE("clearing EBs",
			      init_master_had_issue,
			      LWROC_TRIVA_STATUS_WAIT_EB_CLR,
			      LWROC_TRIVA_FAIL_NET);

      ts->_state = LWROC_TRIVA_STATE_MASTER_STARTUP;
      FALL_THROUGH;

    case LWROC_TRIVA_STATE_MASTER_STARTUP:
      /* Send request to initialize to module process. */

      LWROC_TRIVA_TS_MSG_PREPARE(&ts->_triva_ctrl->_msg, ts,
				 LWROC_TRIVA_CONTROL_INITIALISE);

      ts->_triva_ctrl->_msg.bus_enable = !PD_LL_IS_EMPTY(&ts->_slaves);
      ts->_triva_ctrl->_msg.master = 1;
      ts->_triva_ctrl->_msg.fctime = _lwroc_triva_config._fctime;
      ts->_triva_ctrl->_msg.ctime  = _lwroc_triva_config._ctime;

      lwroc_net_trigctrl_send_msg(ts->_triva_ctrl);
      PD_LL_ADD_BEFORE(&ts->_wait_slaves, &ts->_triva_ctrl->_wait_slaves);

      ts->_state = LWROC_TRIVA_STATE_MASTER_WAIT_INIT;
      break;

    case LWROC_TRIVA_STATE_MASTER_WAIT_INIT:
      /* Wait for the module process to report successful initialisation. */

      WAIT_SLAVES_CHECK_ISSUE("master initialisation",
			      init_master_had_issue,
			      LWROC_TRIVA_STATUS_WAIT_MASTER_SELF,
			      LWROC_TRIVA_FAIL_LOCAL);

      /* From now, master needs abort message before new init. */
      ts->_master_abort_type = LWROC_TRIVA_CONTROL_ABORT_TEST;

      ts->_state = LWROC_TRIVA_STATE_MASTER_INITIALIZED;
      FALL_THROUGH;

    case LWROC_TRIVA_STATE_MASTER_INITIALIZED:
      /* Send message to slaves that they should initialize */

      PD_LL_FOREACH(ts->_slaves, iter)
	{
	  lwroc_net_trigbus *slave =
	    PD_LL_ITEM(iter, lwroc_net_trigbus, _slaves);

	  LWROC_TRIVA_TS_MSG_PREPARE(&slave->_msg, ts,
				     LWROC_TRIVA_CONTROL_INITIALISE);

	  slave->_msg.event_counter = ts->_triva_ctrl->_msg.event_counter;

	  lwroc_net_trigbus_send_msg(slave, 0);

	  PD_LL_ADD_BEFORE(&ts->_wait_slaves, &slave->_wait_slaves);
	}
      /* Any connected slave that survives this will needs abort
       * message before new init. */
      ts->_slave_abort_type = LWROC_TRIVA_CONTROL_ABORT_TEST;

      ts->_state = LWROC_TRIVA_STATE_MASTER_WAIT_SLAVE_INIT;

      FALL_THROUGH; /* No slave? - then just continue. */

    case LWROC_TRIVA_STATE_MASTER_WAIT_SLAVE_INIT:
      /* Wait for all slaves processes to report successful init. */

      /* Note that we wait for all slaves to either report or timeout
       * (= connection torn down) before we possibly handle the error.
       * This ensures that all (still connected) slaves are ready to
       * accept another message the next round.
       */
      /* Master has already done init, and thus would need any reinit
       * to first abort the test phase.
       */
      WAIT_SLAVES_CHECK_ISSUE("slave initialisation",
			      master_had_issue,
			      LWROC_TRIVA_STATUS_WAIT_SLAVE,
			      LWROC_TRIVA_FAIL_NET);

      ts->_state = LWROC_TRIVA_STATE_MASTER_SLAVE_INITED;
      FALL_THROUGH;

    case LWROC_TRIVA_STATE_MASTER_SLAVE_INITED:
      /* LWROC_TRIVA_STATE_SETUP_EB_CHECK: */
      /* Add all EBs to the list for checking. */

      PD_LL_FOREACH(ts->_ebs, iter)
	{
	  lwroc_net_trigbus *eb =
	    PD_LL_ITEM(iter, lwroc_net_trigbus, _slaves);

	  PD_LL_ADD_BEFORE(&ts->_wait_slaves, &eb->_wait_slaves);
	}

      ts->_triva_ctrl->_eb_ident_state = LWROC_EB_IDENT_STATE_NOT_SENT;

      PD_LL_FOREACH(ts->_slaves, iter)
	{
	  lwroc_net_trigbus *slave =
	    PD_LL_ITEM(iter, lwroc_net_trigbus, _slaves);

	  slave->_eb_ident_state = LWROC_EB_IDENT_STATE_NOT_SENT;
	}

      /* Make sure we send the initial batch of ident marks. */
      ts->_next_ident_resend = *now;

      ts->_ident_interval = 10000; /* us */
      ts->_state = LWROC_TRIVA_STATE_EB_CHECK;
      FALL_THROUGH;

    case LWROC_TRIVA_STATE_EB_CHECK:
      /* Check that all EBs have received good ident event. */

      PD_LL_FOREACH_TMP(ts->_wait_slaves, iter, tmp_iter)
	{
	  lwroc_net_trigbus *eb =
	    PD_LL_ITEM(iter, lwroc_net_trigbus, _wait_slaves);

	  /* Any EB which still is on the list is as yet unverified:
	   *
	   * It is actually up to the EB to make sure it verifies
	   * all connections.  We do the following:
	   *
	   * Whenever it is ready for sending a message (after
	   * reception or delay/timeout), we query it for status.
	   *
	   * It can respond:
	   *
	   * - Connections remaining, but do not know magic numbers
	   *   yet.  = please query me again soon.
	   *
	   * - Need verification, in which case we send the result,
	   *   and it responds again.
	   *
	   * - All connections verified.  I.e. we then assume this EB
	   *   to be in good shape.  Further issues will be handled
	   *   using the usual failure handling mechanisms.
	   */

	  /* If it lost the connection, or a bad message, it will have
	   * been removed from the list by the network routine (with
	   * the had_issues set).
	   */

	  /* There are some additional issues here: originally, while
	   * checking the EBs for valid connections, there was no
	   * checking on slaves.  If a slave dies in the meanwhile,
	   * the EB may never get the validation message.  But we wait
	   * for it forever, and thus never get around to get the
	   * slave up again.  Actually, we need in this case to resend
	   * the validation.
	   *
	   * Also, even if a slave is alive, its outgoing data
	   * connection may have some interruptions, and thus loose
	   * the validation message.  This we cannot even test for, we
	   * have to periodically resend new validations for slaves
	   * which have not yet been validated.
	   *
	   * And thirdly, the EB validator must be monotonic.  As
	   * connections become validated, they may not be counted as
	   * unvalidated again (while we are in this mode).  Failures
	   * must be dealt with later.  (Or clearly reported as
	   * failures; on express failure we could cycle back.)
	   */

	  if (eb->_state == LWROC_NET_TRIGBUS_STATE_MSG_RECEIVED)
	    {
	      /* TODO: Hmm, should we call the recv_msg routine? */
	      eb->_state = LWROC_NET_TRIGBUS_STATE_WAIT_MSG_SEND;
	      /*
	      printf ("EB Master got message %x (%d)\n",
		      eb->_msg._type,
		      eb->_msg._eb_srcs_waiting);
	      */
	      switch (eb->_msg._type)
		{
		case LWROC_TRIVA_CONTROL_EB_NO_IDENT:
		  if (eb->_msg._eb_srcs_waiting == 0)
		    {
		      /* All connections verified.  We are done with this
		       * one.
		       */
		      PD_LL_REMOVE(iter);
		      continue;
		    }
		  /* No ID to verify, but also not done as sources
		   * remain.  Set a timeout, such that we soon again
		   * (but not immediately) send a query.
		   */
		  /* printf ("No ident - setup timeout...\n"); */
		  eb->_out._next_time = *now;
		  eb->_out._next_time.tv_sec += 3;
		  /*
		  eb->_out._next_time.tv_usec += ts->_ident_interval;
		  if (eb->_out._next_time.tv_usec > 1000000)
		    {
		      eb->_out._next_time.tv_sec++;
		      eb->_out._next_time.tv_usec -= 1000000;
		    }
		  ts->_ident_interval *= 2;
		  if (ts->_ident_interval > 1000000)
		    ts->_ident_interval = 1000000;
		  */
		  eb->_state = LWROC_NET_TRIGBUS_STATE_DELAY_MSG_SEND;
		  break;

		case LWROC_TRIVA_CONTROL_EB_ASK_IDENT:
		  /* The EB has found an ID event.  Have we sent it to
		   * master or any slave?
		   */
		  /*
		  printf ("Ask IDENT %08x %08x %08x ->",
			  eb->_msg._eb_ident0,
			  eb->_msg._eb_ident1,
			  eb->_msg._eb_ident2);
		  */
		  eb->_msg._type = LWROC_TRIVA_CONTROL_EB_BAD_IDENT;

		  if (lwroc_ident_match(eb, ts->_triva_ctrl))
		    {
		      /* This is a good IDENT message! */
		      eb->_msg._type = LWROC_TRIVA_CONTROL_EB_GOOD_IDENT;
		    }
		  else
		    PD_LL_FOREACH(ts->_slaves, iter2)
		      {
			lwroc_net_trigbus *slave =
			  PD_LL_ITEM(iter2, lwroc_net_trigbus, _slaves);

			if (lwroc_ident_match(eb, slave))
			  {
			    /* This is a good IDENT message! */
			    eb->_msg._type = LWROC_TRIVA_CONTROL_EB_GOOD_IDENT;
			    break;
			  }
		      }
		  /*
		  printf(" %s\n",
			 eb->_msg._type == LWROC_TRIVA_CONTROL_EB_BAD_IDENT ?
			 "BAD" : "GOOD");
		  */
		  /* Send the message back to the slave, with the
		   * ident information updated.  Note that we do not
		   * clear the message.
		   */
		  lwroc_net_trigbus_send_msg(eb,
					     LWROC_TRIVA_CONTROL_EB_NO_IDENT |
					     LWROC_TRIVA_CONTROL_EB_ASK_IDENT);
		  /* Reset the timeout. */
		  ts->_ident_interval = 10000; /* us */
		  break;

		default:
		  /* This should have been caught by the network routine. */
		  LWROC_BUG("Unexpected query response from EB.");
		  break;
		}
	    }

	  if (eb->_state == LWROC_NET_TRIGBUS_STATE_WAIT_MSG_SEND ||
	      (eb->_state == LWROC_NET_TRIGBUS_STATE_DELAY_MSG_SEND &&
	       eb->_got_lam))
	    {
	      /* The network machinery handles the timeout to not
	       * query too often ?
	       */
	      /* printf ("Send query...\n"); */

	      LWROC_TRIVA_TS_MSG_PREPARE(&eb->_msg, ts,
					 LWROC_TRIVA_CONTROL_EB_QUERY_IDENT);

	      lwroc_net_trigbus_send_msg(eb,
					 LWROC_TRIVA_CONTROL_EB_NO_IDENT |
					 LWROC_TRIVA_CONTROL_EB_ASK_IDENT);
	    }
	}

      /*
      printf ("resend: %d  %d\n",
	      !timercmp(now, &ts->_next_ident_resend, <),
	      PD_LL_IS_EMPTY(&ts->_wait_slaves2));
      */

      if (PD_LL_IS_EMPTY(&ts->_wait_slaves2) &&
	  !timercmp(now, &ts->_next_ident_resend, <))
	{
	  /* The danger of resending the ident messages often is that
	   * we then require the data chains to be this fast!  If the
	   * ident message does not circulate back to us for
	   * validation before the marker is changed...  Then we'll
	   * try forever.
	   */
	  ts->_next_ident_resend = *now;
	  ts->_next_ident_resend.tv_sec += 5; /* resend in 5 seconds. */

	  /* Resend validation marks for all slaves (or master) that
	   * are not yet validated.  This will make the previous
	   * validation marks for these slaves unusable.  But that is
	   * ok, since they if anything will be before.
	   *
	   * Since we want any error reports, we also need to wait
	   * with further handling of the EBs.  I.e. we need to check
	   * the delayed errors when we know all slaves are done with
	   * their messages.  Hmm, better approach: as we anyhow have
	   * to deal with any leftover EBs at that point, we check
	   * for errors occurring continuously.  And as soon as we notice
	   * any, we go into the catch-all phase of remaining connections,
	   * after which we cycle back to init...
	   */

	  LWROC_INFO("(Re)send ident messages...");

	  if (ts->_triva_ctrl->_eb_ident_state != LWROC_EB_IDENT_STATE_USED)
	    {
	      /* Send message to master to inject ident message. */

	      /* TODO: if we have not gotten the response from last
	       * time, then we will try to queue up two messages...
	       */
	      lwroc_send_ident(ts, ts->_triva_ctrl,
			       now, &ts->_wait_slaves2, 1);
	    }

	  /* Send message to slaves that they should inject ident message. */

	  PD_LL_FOREACH(ts->_slaves, iter)
	    {
	      lwroc_net_trigbus *slave =
		PD_LL_ITEM(iter, lwroc_net_trigbus, _slaves);

	      /* TODO: remove, just to test context. */
	      LWROC_CACTION(&slave->_out._base, "send ident?");

	      if (slave->_eb_ident_state != LWROC_EB_IDENT_STATE_USED)
		{
		  /* TODO: if we have not gotten the response from
		   * last time, then we will try to queue up two
		   * messages...
		   */
		  lwroc_send_ident(ts, slave, now, &ts->_wait_slaves2, 0);
		}
	    }

	  if (PD_LL_IS_EMPTY(&ts->_wait_slaves2))
	    {
	      LWROC_ERROR("Event builders waiting for validation, "
			  "but no slave left to validate.  "
			  "Bad data topology?");
	    }
	}

      /* As an exception, we do the issue check before making sure
       * that we have gotten all responses.  This is such that if some
       * slave has gone bad, we shall not wait forever for a (good) EB
       * association that will then never finish.  Cost is that we have
       * to go to another state to retire outstanding responses.
       */
      if (ts->_had_issue)
	GOTO_HAD_ISSUE(eb_check_had_issue, LWROC_TRIVA_FAIL_EB_ID);

      if (!PD_LL_IS_EMPTY(&ts->_wait_slaves) ||
	  !PD_LL_IS_EMPTY(&ts->_wait_slaves2))
	{
	  lwroc_report_wait_slaves(now, "validated readout->EB connection(s)",
				   LWROC_TRIVA_STATUS_WAIT_IDENT);
	  break;
	}
      SET_MON_STATUS_NONWAIT;

      /* printf ("ebs checked...\n"); */

      ts->_state = LWROC_TRIVA_STATE_MASTER_GO_SLAVE_TEST;
      FALL_THROUGH;

    case LWROC_TRIVA_STATE_MASTER_GO_SLAVE_TEST:
      ;
    run_master_go_test:

      /* Send message to slaves that they should go to test mode. */

      PD_LL_FOREACH(ts->_slaves, iter)
	{
	  lwroc_net_trigbus *slave =
	    PD_LL_ITEM(iter, lwroc_net_trigbus, _slaves);

	  LWROC_TRIVA_TS_MSG_PREPARE(&slave->_msg, ts,
				     LWROC_TRIVA_CONTROL_GO_TEST);

	  lwroc_net_trigbus_send_msg(slave, 0);

	  PD_LL_ADD_BEFORE(&ts->_wait_slaves, &slave->_wait_slaves);
	}

      /* Any connected slave that survives this will needs abort
       * message before new init. */
      /* We need test abort, in case we came from a start acq. */
      ts->_slave_abort_type = LWROC_TRIVA_CONTROL_ABORT_TEST;

      SET_MON_STATUS(LWROC_TRIVA_STATUS_TEST);
      ts->_state = LWROC_TRIVA_STATE_MASTER_WAIT_SLAVE_TEST;

      FALL_THROUGH; /* No slave? - then just continue. */

    case LWROC_TRIVA_STATE_MASTER_WAIT_SLAVE_TEST:
      /* Wait for slaves to enter test mode. */

      WAIT_SLAVES_CHECK_ISSUE("slave(s) to enter test mode",
			      master_had_issue,
			      LWROC_TRIVA_STATUS_WAIT_TEST,
			      LWROC_TRIVA_FAIL_TEST);

      ts->_state = LWROC_TRIVA_STATE_MASTER_GO_TEST;
      FALL_THROUGH;

    case LWROC_TRIVA_STATE_MASTER_GO_TEST:
      /* For the time being, send master directly to run mode. */

      LWROC_TRIVA_TS_MSG_PREPARE(&ts->_triva_ctrl->_msg, ts,
				 LWROC_TRIVA_CONTROL_GO_TEST);
      ts->_triva_ctrl->_msg.master = 1;
      if (1) /* If doing triva test. */
	{
	  ts->_test_no = 0;
	  ts->_triva_ctrl->_msg._trig = LWROC_TEST_TRIG(ts->_test_no);
	}

      lwroc_net_trigctrl_send_msg(ts->_triva_ctrl);
      PD_LL_ADD_BEFORE(&ts->_wait_slaves, &ts->_triva_ctrl->_wait_slaves);

      ts->_state = LWROC_TRIVA_STATE_MASTER_WAIT_TEST;
      break;

    case LWROC_TRIVA_STATE_MASTER_WAIT_TEST:
	/* Wait for the control process to report successful initialisation. */

      WAIT_SLAVES_CHECK_ISSUE("master to enter test mode",
			      master_had_issue,
			      LWROC_TRIVA_STATUS_WAIT_TEST,
			      LWROC_TRIVA_FAIL_TEST);

      /* From now, master needs abort message before new init. */
      /* We need test abort, in case we came from a start acq. */
      ts->_master_abort_type = LWROC_TRIVA_CONTROL_ABORT_TEST;

      ts->_state = LWROC_TRIVA_STATE_TEST_GET_TRIG;
      FALL_THROUGH;

    case LWROC_TRIVA_STATE_TEST_GET_TRIG:
      ;
    test_get_trig:
      /* Get trigger info from all systems. */

      lwroc_triva_state_send_msgs(ts, LWROC_TRIVA_CONTROL_GET_TRIG);

      ts->_state = LWROC_TRIVA_STATE_TEST_WAIT_GET_TRIG;
      break;

    case LWROC_TRIVA_STATE_TEST_WAIT_GET_TRIG:
      /* Wait for systems to delivere trigger info. */

      WAIT_SLAVES_CHECK_ISSUE("get test trigger info",
			      master_had_issue,
			      LWROC_TRIVA_STATUS_WAIT_TEST,
			      LWROC_TRIVA_FAIL_TEST);

      /* We must now verify that the triggers are good. */
      {
	int bad = 0;

	bad |= lwroc_triva_check_test_trig(ts, ts->_triva_ctrl, 1);

	ts->_nslaves = 0;

	PD_LL_FOREACH(ts->_slaves, iter)
	  {
	    lwroc_net_trigbus *slave =
	      PD_LL_ITEM(iter, lwroc_net_trigbus, _slaves);

	    bad |= lwroc_triva_check_test_trig(ts, slave, 0);

	    ts->_nslaves++;
	  }
	if (bad)
	  GOTO_HAD_ISSUE(master_had_issue, LWROC_TRIVA_FAIL_TEST);
      }

      ts->_state = LWROC_TRIVA_STATE_TEST_MOST_RELEASE_DT;
      FALL_THROUGH;

    case LWROC_TRIVA_STATE_TEST_MOST_RELEASE_DT:
      /* Do we want to do further tests after this trigger?  If so, we
       * have to provide the next trigger to the master already now,
       * before releasing the deadtime.
       *
       * We do an multiple of 32 triggers such that the event counter
       * in the TRIVA/MI wraps cleanly.
       *
       * We do at least 5 triggers per connected system, such that each
       * releases DT last a few times.
       */

      if ((ts->_test_no+1) % 32 == 0 &&
	  ts->_test_no >= 5 * (ts->_nslaves + 1) &&
	  ts->_test_no > 32)
	ts->_test_no |= 0x80000000;

      if (_lwroc_triva_config._kind == LWROC_TRIVA_KIND_TRIVA ||
	  _lwroc_triva_config._kind == LWROC_TRIVA_KIND_TRIXOR)
	{
	  /* It looks like TRIVAs do not support getting the next
	   * trigger set by software while they are still in deadtime.
	   * We therefore only perform one test cycle.
	   *
	   * Note: this behaviour contradicts earlier experiences.
	   */
	  ts->_test_no |= 0x80000000;
	}

      /* Release DT in all but one systems.
       * After this, we can see that DT is still set in master,
       * verifying that the remaining system has its DT connected.
       */

      lwroc_triva_state_send_msgs_except(ts, LWROC_TRIVA_CONTROL_RELEASE_DT,
					 (int) (ts->_test_no %
						(ts->_nslaves + 1)),
					 &ts->_conn_last_release,
					 lwroc_ts_relase_dt_trig_msg);

      ts->_state = LWROC_TRIVA_STATE_TEST_WAIT_MOST_REL_DT;
      FALL_THROUGH; /* No slave? - then just continue. */

    case LWROC_TRIVA_STATE_TEST_WAIT_MOST_REL_DT:
      /* Wait for systems to release DT. */

      WAIT_SLAVES_CHECK_ISSUE("test systems except one release DT",
			      master_had_issue,
			      LWROC_TRIVA_STATUS_WAIT_TEST,
			      LWROC_TRIVA_FAIL_TEST);

      ts->_state = LWROC_TRIVA_STATE_TEST_GET_TRIG2;
      FALL_THROUGH;

    case LWROC_TRIVA_STATE_TEST_GET_TRIG2:
      /* Get trigger info from master to verify DT and (old) trigger. */

      lwroc_triva_state_send_msg(ts, ts->_triva_ctrl, 1,
				 LWROC_TRIVA_CONTROL_GET_TRIG,
				 &ts->_wait_slaves, NULL);

      ts->_state = LWROC_TRIVA_STATE_TEST_WAIT_GET_TRIG2;
      break;

    case LWROC_TRIVA_STATE_TEST_WAIT_GET_TRIG2:
      /* Wait for master to delivere trigger info. */

      WAIT_SLAVES_CHECK_ISSUE("get test trigger info 2nd",
			      master_had_issue,
			      LWROC_TRIVA_STATUS_WAIT_TEST,
			      LWROC_TRIVA_FAIL_TEST);

      /* Master should still have the old trigger. */
      {
	int bad = 0;

	bad |= lwroc_triva_check_test_trig(ts, ts->_triva_ctrl, 1);

	if (bad)
	  {
	    LWROC_ERROR_FMT("Master has bad trigger (next)?, "
			    "after all systems except one (%s) released DT.",
			    ts->_conn_last_release->_out._hostname);
	    GOTO_HAD_ISSUE(master_had_issue, LWROC_TRIVA_FAIL_TEST);
	  }
      }

      ts->_state = LWROC_TRIVA_STATE_TEST_LAST_RELEASE_DT;
      FALL_THROUGH;

    case LWROC_TRIVA_STATE_TEST_LAST_RELEASE_DT:
      /* Release DT in last system. */

      lwroc_triva_state_send_msg(ts, ts->_conn_last_release,
				 !(ts->_test_no % (ts->_nslaves + 1)),
				 LWROC_TRIVA_CONTROL_RELEASE_DT,
				 &ts->_wait_slaves,
				 lwroc_ts_relase_dt_trig_msg);

      ts->_state = LWROC_TRIVA_STATE_TEST_WAIT_LAST_REL_DT;
      break;

    case LWROC_TRIVA_STATE_TEST_WAIT_LAST_REL_DT:
      /* Wait for the last system to release DT. */

      WAIT_SLAVES_CHECK_ISSUE("test systems last release DT",
			      master_had_issue,
			      LWROC_TRIVA_STATUS_WAIT_TEST,
			      LWROC_TRIVA_FAIL_TEST);

      if (!(ts->_test_no & 0x80000000))
	{
	  ts->_test_no++;
	  goto test_get_trig;
	}

      /* We are done testing. */
      ts->_state = LWROC_TRIVA_STATE_MASTER_GO_SLAVE_RUN;
      FALL_THROUGH;

    case LWROC_TRIVA_STATE_MASTER_GO_SLAVE_RUN:
      /* Send message to slaves that they should go to run mode. */

      PD_LL_FOREACH(ts->_slaves, iter)
	{
	  lwroc_net_trigbus *slave =
	    PD_LL_ITEM(iter, lwroc_net_trigbus, _slaves);

	  LWROC_TRIVA_TS_MSG_PREPARE(&slave->_msg, ts,
				     LWROC_TRIVA_CONTROL_GO_RUN);

	  lwroc_net_trigbus_send_msg(slave, 0);

	  PD_LL_ADD_BEFORE(&ts->_wait_slaves, &slave->_wait_slaves);
	}

      /* Any connected slave that survives this will needs abort
       * message before new init. */
      ts->_slave_abort_type = LWROC_TRIVA_CONTROL_ABORT_READOUT;

      ts->_state = LWROC_TRIVA_STATE_MASTER_WAIT_SLAVE_RUN;

      if (!timerisset(&ts->_mon_time_error))
	ts->_mon_time_error = *now;
      SET_MON_STATUS(LWROC_TRIVA_STATUS_RUN);
      FALL_THROUGH; /* No slave? - then just continue. */

    case LWROC_TRIVA_STATE_MASTER_WAIT_SLAVE_RUN:
      /* Wait for slaves to enter run mode. */

      WAIT_SLAVES_CHECK_ISSUE("slave(s) to enter run mode",
			      master_had_issue,
			      LWROC_TRIVA_STATUS_WAIT_RUN,
			      LWROC_TRIVA_FAIL_NET);

      ts->_state = LWROC_TRIVA_STATE_MASTER_GO_RUN;
      FALL_THROUGH;

    case LWROC_TRIVA_STATE_MASTER_GO_RUN:
      /* Send master to run mode. */

      LWROC_TRIVA_TS_MSG_PREPARE(&ts->_triva_ctrl->_msg, ts,
				 LWROC_TRIVA_CONTROL_GO_RUN);
      ts->_triva_ctrl->_msg.master = 1;

      lwroc_net_trigctrl_send_msg(ts->_triva_ctrl);
      PD_LL_ADD_BEFORE(&ts->_wait_slaves, &ts->_triva_ctrl->_wait_slaves);

      ts->_state = LWROC_TRIVA_STATE_MASTER_WAIT_RUN;
      break;

    case LWROC_TRIVA_STATE_MASTER_WAIT_RUN:
      /* Wait for the module process to report successful initialisation. */

      WAIT_SLAVES_CHECK_ISSUE("master to enter run mode",
			      master_had_issue,
			      LWROC_TRIVA_STATUS_WAIT_RUN,
			      LWROC_TRIVA_FAIL_LOCAL);

      /* From now, master needs abort message before new init. */
      ts->_master_abort_type = LWROC_TRIVA_CONTROL_ABORT_READOUT;

      /* Initialise the lock-up detection. */
      ts->prev_deadtime = 0;
      ts->prev_event_counter = 0; /* Does not matter, with dt = 0. */
      ts->_poll2_flags = 0;
      ts->_poll2_slave_counter = 0;

      if (ts->_acq_state == LWROC_ACQ_STATE_REQ_START)
	ts->_acq_state = LWROC_ACQ_STATE_REQ_STARTED;

      ts->_state = LWROC_TRIVA_STATE_MASTER_GO_RUNNING_POLL;
      FALL_THROUGH;

    case LWROC_TRIVA_STATE_MASTER_GO_RUNNING_POLL:
      ;
    run_master_poll:
      /* Request to stop? */
      if (ts->_acq_state == LWROC_ACQ_STATE_REQ_STOP)
	{
	  ts->_state = LWROC_TRIVA_STATE_MASTER_GO_STOP;
	  goto run_master_go_stop;
	}
      if (ts->_acq_state == LWROC_ACQ_STATE_REQ_START)
	{
	  ts->_state = LWROC_TRIVA_STATE_MASTER_GO_SLAVE_TEST;
	  goto run_master_go_test;
	}

      /* Wait for the module to report some issue... */

      /* Actually, they never really report issues.  We try to search
       * for them.  What we can have are connections that go broken
       * however.
       */

      /* There is no need to request status from slaves (in the same
       * DT domain), as when they get stuck, the master also gets
       * stuck.  However, we do need to know if the EB connection has
       * been terminated.  (Otherwise, a freshly connected
       * (i.e. restarted) EB may just eat/discard events from the
       * readouts, without ever getting the validation events in, and
       * thus events built.
       */

      PD_LL_FOREACH(ts->_ebs, iter)
	{
	  lwroc_net_trigbus *eb =
	    PD_LL_ITEM(iter, lwroc_net_trigbus, _slaves);

	  LWROC_TRIVA_TS_MSG_PREPARE(&eb->_msg, ts,
				     LWROC_TRIVA_CONTROL_EB_STATUS_QUERY);
	  lwroc_net_trigbus_send_msg(eb,
				     LWROC_TRIVA_CONTROL_EB_STATUS_RESPONSE);
	  PD_LL_ADD_BEFORE(&ts->_wait_slaves, &eb->_wait_slaves);
	}

      LWROC_TRIVA_TS_MSG_PREPARE(&ts->_triva_ctrl->_msg, ts,
				 LWROC_TRIVA_CONTROL_RUN_STATS);
      /* printf ("Send status poll.\n"); */
      lwroc_net_trigctrl_send_msg(ts->_triva_ctrl);
      PD_LL_ADD_BEFORE(&ts->_wait_slaves, &ts->_triva_ctrl->_wait_slaves);

      ts->_state = LWROC_TRIVA_STATE_MASTER_GO_RUNNING_PGET;
      ts->_next_report.tv_sec = 0;
      break;

    case LWROC_TRIVA_STATE_MASTER_GO_RUNNING_PGET:
    {
      int prev_poll2_flags = ts->_poll2_flags;

      /* Wait for the module process to report the status. */

      WAIT_SLAVES_CHECK_ISSUE("master and EB to report run status",
			      master_had_issue,
			      LWROC_TRIVA_STATUS_WAIT_NET,
			      LWROC_TRIVA_FAIL_NET);

      /* Clear the poll2 flags.  Since there are too many exit paths
       * from the polling, we 'shift' the DO_LAM flag into FOR_LAM.
       */
      ts->_poll2_flags = 0;
      if (prev_poll2_flags & LWROC_POLL2_DO_LAM)
	ts->_poll2_flags |= LWROC_POLL2_FOR_LAM;

      {
	uint32_t need_force = 0;

	/* If the EB has lost connection, we do have an issue.  Since
	 * events will not be properly built until a new validation
	 * comes through, better get to that as soon as possible.
	 */

	PD_LL_FOREACH(ts->_ebs, iter)
	  {
	    lwroc_net_trigbus *eb =
	      PD_LL_ITEM(iter, lwroc_net_trigbus, _slaves);

	    if (eb->_msg._eb_srcs_waiting != 0)
	      {
		LWROC_CERROR(&eb->_out._base, "EB had connection issue.");
		GOTO_HAD_ISSUE(master_had_issue,
			       LWROC_TRIVA_FAIL_EB_DATA_CONN);
	      }

	    need_force |=
	      (eb->_msg.delayed_eb_status &
	       LWROC_DELAYED_EB_STATUS_NEED_FORCE);
	  }

	if (need_force != !!(ts->delayed_eb_status &
			     LWROC_DELAYED_EB_STATUS_FORCED))
	  {
	    /* Need force status about to change.  Inform all slaves. */
	    ts->_poll2_flags |= LWROC_POLL2_FOR_FORCE_EB;
	  }
	ts->delayed_eb_status &= ~LWROC_DELAYED_EB_STATUS_FORCED;
	if (need_force)
	  ts->delayed_eb_status |= LWROC_DELAYED_EB_STATUS_FORCED;
      }

      /* We have received the status from the master.
       * Investigate it!
       */
      /*
      if ((ts->_triva_ctrl->_msg.status & LWROC_READOUT_STATUS_HELD) &&
	  ts->_acq_state == LWROC_ACQ_STATE_REQ_STOPPING)
	ts->_acq_state = LWROC_ACQ_STATE_REQ_STOPPED;
      */

      lwroc_triva_fill_meas_dt(&ts->_triva_ctrl->_msg);

      {
	uint32_t mask_held = 0;

	/* If we want to stop, HELD is not an error.  Otherwise it is. */

	/* Hmmm, what if we are in START state?  Need to be nudged out of
	 * hold then ?
	 */
	if (ts->_acq_state != LWROC_ACQ_STATE_REQ_RUN)
	  mask_held = LWROC_READOUT_STATUS_HELD;

      /* Need to reinit?  Being out of buffer space, or held (stopped)
       * is not an reason to reinit.
       */
      if (ts->_triva_ctrl->_msg.status & ~(LWROC_READOUT_STATUS_WAIT_BUFSPACE |
					   LWROC_READOUT_STATUS_IN_READOUT |
					   LWROC_READOUT_STATUS_HEADACHE_CUR |
					   LWROC_READOUT_STATUS_HEADACHE |
					   LWROC_READOUT_STATUS_TERM_REQ |
					   mask_held))
	{
	  LWROC_WARNING_FMT("Master TRIVA/MI has error (status = 0x%x).",
			    ts->_triva_ctrl->_msg.status);

	  ts->_poll2_flags |= LWROC_POLL2_PRINT_STATUS;
	  ts->_state = LWROC_TRIVA_STATE_MASTER_GO_RUNNING_SLAVE_POLL;
	  goto run_master_setup_slave_poll;
	}
      }

      /* Cannot do the lock-up test if we are doing an urgent poll.  Time
       * since last poll is unknown.
       */
      if (ts->_poll2_flags & (LWROC_POLL2_FOR_LAM |
			      LWROC_POLL2_FOR_FORCE_EB))
	{
	  /* Copy values. */
	  ts->prev_deadtime = ts->_triva_ctrl->_msg.deadtime;
	  ts->prev_event_counter = ts->_triva_ctrl->_msg.event_counter;
	  goto run_master_setup_slave_poll;
	}

      /* If we should be running, then check that we have not gotten stuck. */
      if (ts->_acq_state == LWROC_ACQ_STATE_REQ_RUN &&
	  ts->_triva_ctrl->_msg.event_counter == ts->prev_event_counter &&
	  ts->_triva_ctrl->_msg.deadtime &&
	  ts->prev_deadtime)
	{
	  /* Looks like we are stuck! */

	  LWROC_WARNING("Master TRIVA/MI no progress last second, "
			"and in deadtime.");

	  ts->_poll2_flags |= LWROC_POLL2_STUCK | LWROC_POLL2_PRINT_STATUS;
	  ts->_state = LWROC_TRIVA_STATE_MASTER_GO_RUNNING_SLAVE_POLL;
	  goto run_master_setup_slave_poll;
	}

      /* TODO: we should remember the time of these values (now).
       * Such that we can setup the timeout relative to this.  And not
       * be indefinitely delayed if some urgent LAMs are spamming us.
       * (Although they shouldn't.)  Note also copy of values above.
       */
      ts->prev_deadtime = ts->_triva_ctrl->_msg.deadtime;
      ts->prev_event_counter = ts->_triva_ctrl->_msg.event_counter;

      if (ts->_acq_state == LWROC_ACQ_STATE_REQ_STOPPING ||
	  ts->_acq_state == LWROC_ACQ_STATE_REQ_STOP15)
	{
	  /* We do not complete the STOPPING phase until also all slaves
	   * have processed the last trigger and are in HELD state.
	   */
	  printf("check slaves for HELD\n");
	  ts->_poll2_flags |= LWROC_POLL2_FOR_STOP;
	  ts->_state = LWROC_TRIVA_STATE_MASTER_GO_RUNNING_SLAVE_POLL;
	  goto run_master_setup_slave_poll;
	}

      /* If we got this far, all is well.  Except that we are not
       * polling the slaves.  We should do that once in a while, to
       * detect if one of them has lost the trigger and therefore is
       * lagging behind (waiting until all queues are full and EB
       * stalls takes too long.)
       */
      ts->_poll2_slave_counter++;
      if (!(ts->_poll2_slave_counter & 0x07) ||
	  (prev_poll2_flags & LWROC_POLL2_STUCK) ||
	  timerisset(&_lwroc_triva_state->_measure_dt_end) ||
	  ts->_mon_status == LWROC_TRIVA_STATUS_HEADACHE)
	{
	  if (timerisset(&_lwroc_triva_state->_measure_dt_end) &&
	      (timercmp(&_lwroc_triva_state->_measure_dt_start, now, >) ||
	       timercmp(&_lwroc_triva_state->_measure_dt_end, now, <)))
	    {
	      /* We should end the measurement.
	       * (Or clock moved backwards).
	       */
	      timerclear(&_lwroc_triva_state->_measure_dt_end);
	    }
	  ts->_poll2_flags |= LWROC_POLL2_REGULAR;
	  ts->_state = LWROC_TRIVA_STATE_MASTER_GO_RUNNING_SLAVE_POLL;
	  goto run_master_setup_slave_poll;
	}

      /* Things are looking good.  Again?  Set good run status. */
      if (ts->_mon_status != LWROC_TRIVA_STATUS_STOP)
	SET_MON_STATUS(LWROC_TRIVA_STATUS_RUN);
    }

    run_master_setup_pwait:
      /* Setup timeout to do next status poll. */
      {
	struct timeval interval = { 0, 0 };

	/* If we are measuring deadtime, we need more frequent updates
	 * to avoid that the monitor display flickers. (at least if
	 * there is some reasonable event rate.
	 */
	if (timerisset(&_lwroc_triva_state->_measure_dt_end))
	  interval.tv_usec = 250000;
	else
	  interval.tv_sec = 1;

	timeradd(now, &interval, &ts->_triva_ctrl->_out._next_time);
      }
      ts->_triva_ctrl->_state = LWROC_NET_TRIGBUS_STATE_DELAY_MSG_SEND;

      ts->_state = LWROC_TRIVA_STATE_MASTER_GO_RUNNING_PWAIT;
      break;

    case LWROC_TRIVA_STATE_MASTER_GO_RUNNING_PWAIT:

      if (ts->_acq_state == LWROC_ACQ_STATE_REQ_START ||
	  ts->_acq_state == LWROC_ACQ_STATE_REQ_STOP)
	{
	  if (ts->_triva_ctrl->_state !=
	      LWROC_NET_TRIGBUS_STATE_DELAY_MSG_SEND &&
	      ts->_triva_ctrl->_state !=
	      LWROC_NET_TRIGBUS_STATE_WAIT_MSG_SEND)
	    LWROC_BUG("Unexpected triva_ctrl state.");
	  /* Make sure we handle the STOP quickly. */
	  ts->_triva_ctrl->_state = LWROC_NET_TRIGBUS_STATE_WAIT_MSG_SEND;
	}

      if (ts->_got_look_at_me)
	{
	  ts->_got_look_at_me = 0;
	  /* Do a query now. */
	  ts->_triva_ctrl->_state = LWROC_NET_TRIGBUS_STATE_WAIT_MSG_SEND;
	  /* Make sure we go the full way, and query the slaves. */
	  ts->_poll2_flags |= LWROC_POLL2_DO_LAM;
	}

      if (ts->_triva_ctrl->_state == LWROC_NET_TRIGBUS_STATE_WAIT_MSG_SEND)
	{
	  ts->_state = LWROC_TRIVA_STATE_MASTER_GO_RUNNING_POLL;
	  goto run_master_poll;
	}
      break;

    run_master_setup_slave_poll:
    case LWROC_TRIVA_STATE_MASTER_GO_RUNNING_SLAVE_POLL:

      /* Master has detected some lock-up.  Also at regular interval.
       * Get the status from all slaves, to further diagnose the issue.
       */

      /* printf ("Send slave status poll. (%d)\n",
	 PD_LL_IS_EMPTY(&ts->_wait_slaves)); */
      PD_LL_FOREACH(ts->_slaves, iter)
	{
	  lwroc_net_trigbus *slave =
	    PD_LL_ITEM(iter, lwroc_net_trigbus, _slaves);

	  LWROC_TRIVA_TS_MSG_PREPARE(&slave->_msg, ts,
				     LWROC_TRIVA_CONTROL_RUN_STATS);
	  lwroc_net_trigbus_send_msg(slave, 0);
	  PD_LL_ADD_BEFORE(&ts->_wait_slaves, &slave->_wait_slaves);
	}
      PD_LL_FOREACH(ts->_ebs, iter)
	{
	  lwroc_net_trigbus *eb =
	    PD_LL_ITEM(iter, lwroc_net_trigbus, _slaves);

	  LWROC_TRIVA_TS_MSG_PREPARE(&eb->_msg, ts,
				     LWROC_TRIVA_CONTROL_EB_STATUS_QUERY);
	  lwroc_net_trigbus_send_msg(eb,
				     LWROC_TRIVA_CONTROL_EB_STATUS_RESPONSE);
	  PD_LL_ADD_BEFORE(&ts->_wait_slaves, &eb->_wait_slaves);
	}

      ts->_state = LWROC_TRIVA_STATE_MASTER_GO_RUNNING_SLAVE_PGET;

      FALL_THROUGH; /* No slave? - then just continue. */

    case LWROC_TRIVA_STATE_MASTER_GO_RUNNING_SLAVE_PGET:

      /* printf ("Slave poll, wait... (%d)\n",
	 PD_LL_IS_EMPTY(&ts->_wait_slaves)); */

      /* We wait for all slaves to either report or timeout (=
       * connection torn down) before we continue.  Actually, since
       * something is stuck, it is not unlikely that a slave has
       * gotten lost...
       */

      WAIT_SLAVES_CHECK_ISSUE("slaves and EB to report run status",
			      master_had_issue,
			      LWROC_TRIVA_STATUS_WAIT_NET,
			      LWROC_TRIVA_FAIL_NET);

      /* We have gotten reports from everyone.  But before we
       * investigate, the situation has to be still present at the
       * master.
       */

      ts->_state = LWROC_TRIVA_STATE_MASTER_GO_RUNNING_POLL2;
      FALL_THROUGH;

    case LWROC_TRIVA_STATE_MASTER_GO_RUNNING_POLL2:

      LWROC_TRIVA_TS_MSG_PREPARE(&ts->_triva_ctrl->_msg, ts,
				 LWROC_TRIVA_CONTROL_RUN_STATS);
      /* printf ("Send verifying status poll.\n"); */
      lwroc_net_trigctrl_send_msg(ts->_triva_ctrl);
      PD_LL_ADD_BEFORE(&ts->_wait_slaves, &ts->_triva_ctrl->_wait_slaves);

      ts->_state = LWROC_TRIVA_STATE_MASTER_GO_RUNNING_PGET2;
      break;

    case LWROC_TRIVA_STATE_MASTER_GO_RUNNING_PGET2:
      /* Wait for the module process to report the status. */

      WAIT_SLAVES_CHECK_ISSUE("master to report run status 2",
			      master_had_issue,
			      LWROC_TRIVA_STATUS_WAIT_NET /* not really, int */,
			      LWROC_TRIVA_FAIL_LOCAL);

      /* Being out of buffer space is not an reason to reinit. */
      if (ts->_triva_ctrl->_msg.status & ~(LWROC_READOUT_STATUS_WAIT_BUFSPACE |
					   LWROC_READOUT_STATUS_IN_READOUT |
					   LWROC_READOUT_STATUS_HEADACHE_CUR |
					   LWROC_READOUT_STATUS_HEADACHE |
					   LWROC_READOUT_STATUS_HELD |
					   LWROC_READOUT_STATUS_TERM_REQ))
	{
	  GOTO_HAD_ISSUE(master_had_issue,
	    lwroc_triva_state_status_2_failure(ts->_triva_ctrl->_msg.status));
	}

      /* We have received the status from the master.
       * Investigate it!
       */

      if (!(ts->_poll2_flags & (LWROC_POLL2_FOR_LAM |
				LWROC_POLL2_FOR_FORCE_EB |
				LWROC_POLL2_REGULAR)) &&
	  (ts->_triva_ctrl->_msg.event_counter != ts->prev_event_counter ||
	   !ts->_triva_ctrl->_msg.deadtime))
	{
	  printf("%d %d %d\n",
		 (int) ts->_triva_ctrl->_msg.event_counter,
		 (int) ts->prev_event_counter,
		 ts->_triva_ctrl->_msg.deadtime);
	  LWROC_INFO("Progress seen on master TRIVA/MI.");
	  /* Things are looking good.  Again! */
	  if (ts->_mon_status != LWROC_TRIVA_STATUS_STOP)
	    SET_MON_STATUS(LWROC_TRIVA_STATUS_RUN);
	  goto run_master_setup_pwait;
	}

      /* Situation still present.  Investigate slave responses! */

      {
	uint32_t wait_buf_space = 0;
	uint32_t in_readout = 0;
	uint32_t headache = 0;
	uint32_t other_issue = 0;
	uint32_t eb_lost_conn = 0;
	uint32_t all_held;
	uint32_t mask_held = 0;
	uint32_t bug_fatal = 0;
	int all_ec_same = 1;
	int bad_ec_sync = 0;
	uint32_t slave_dt = 0;

	if (ts->_acq_state != LWROC_ACQ_STATE_REQ_RUN)
	  mask_held = LWROC_READOUT_STATUS_HELD;

	/* We have to distinguish the case when we are stuck due to
	 * (some) node not having free buffer space to write data.
	 * Generally, this is not a reason to go for restart.  Except:
	 * if we also are in charge of the event builder, and that has
	 * lost connections.  Then we do need to restart.
	 * Being held (stopped) is not a reason for restart.
	 */

	wait_buf_space |=
	  ts->_triva_ctrl->_msg.status & LWROC_READOUT_STATUS_WAIT_BUFSPACE;
	in_readout |=
	  ts->_triva_ctrl->_msg.status & LWROC_READOUT_STATUS_IN_READOUT;
	headache |=
	  ts->_triva_ctrl->_msg.status & (LWROC_READOUT_STATUS_HEADACHE_CUR |
					  LWROC_READOUT_STATUS_HEADACHE);
	bug_fatal |=
	  ts->_triva_ctrl->_msg.status & LWROC_READOUT_STATUS_STHR_BUG_FATAL;
	other_issue |=
	  ts->_triva_ctrl->_msg.status & ~(LWROC_READOUT_STATUS_WAIT_BUFSPACE |
					   LWROC_READOUT_STATUS_IN_READOUT |
					   LWROC_READOUT_STATUS_HEADACHE_CUR |
					   LWROC_READOUT_STATUS_HEADACHE |
					   LWROC_READOUT_STATUS_TERM_REQ |
					   LWROC_READOUT_STATUS_STHR_BUG_FATAL |
					   mask_held);
	all_held =
	  ts->_triva_ctrl->_msg.status & LWROC_READOUT_STATUS_HELD;

	PD_LL_FOREACH(ts->_slaves, iter)
	  {
	    lwroc_net_trigbus *slave =
	      PD_LL_ITEM(iter, lwroc_net_trigbus, _slaves);

	    wait_buf_space |=
	      slave->_msg.status & LWROC_READOUT_STATUS_WAIT_BUFSPACE;
	    in_readout |=
	      slave->_msg.status & LWROC_READOUT_STATUS_IN_READOUT;
	    headache |=
	      slave->_msg.status & (LWROC_READOUT_STATUS_HEADACHE_CUR |
				    LWROC_READOUT_STATUS_HEADACHE);
	    bug_fatal |=
	      slave->_msg.status & LWROC_READOUT_STATUS_STHR_BUG_FATAL;
	    other_issue |=
	      slave->_msg.status & ~(LWROC_READOUT_STATUS_WAIT_BUFSPACE |
				     LWROC_READOUT_STATUS_IN_READOUT |
				     LWROC_READOUT_STATUS_HEADACHE_CUR |
				     LWROC_READOUT_STATUS_HEADACHE |
				     LWROC_READOUT_STATUS_TERM_REQ |
				     LWROC_READOUT_STATUS_STHR_BUG_FATAL |
				     LWROC_READOUT_STATUS_HELD);
	    all_held &=
	      slave->_msg.status & LWROC_READOUT_STATUS_HELD;

	    all_ec_same &=
	      (slave->_msg.event_counter ==
	       ts->_triva_ctrl->_msg.event_counter);

	    slave_dt |= slave->_msg.deadtime;

	    /* If master had long deadtime, we report any slaves that
	     * are in deadtime.
	     */
	    slave->_aux_status = 0;  /* TODO: FIXME!  This overwrites
				      * LWROC_TRIVA_BUS_STATUS_FAIL set in
				      * lwroc_net_trigbus_slave_after_fail().
				      *
				      * Hmmm, should this _aux_status be
				      * a flags set (perhaps the readout_status
				      * such that it can be masked?)
				      */

	    if ((ts->_poll2_flags & LWROC_POLL2_STUCK) &&
		slave->_msg.deadtime)
	      {
		if (!slave->_aux_status)
		  slave->_aux_status = LWROC_TRIVA_BUS_STATUS_DEADTIME;
	      }

	    /* If the slave event counter is less than one before the
	     * count master had before querying the slave, or more
	     * than one after the master after querying the slave,
	     * something is bad.
	     */
	    if (slave->_msg.event_counter + 1 <
		ts->prev_event_counter /* avoid -1 on 0 */ ||
		slave->_msg.event_counter >
		ts->_triva_ctrl->_msg.event_counter + 1)
	      {
		LWROC_CERROR_FMT(&slave->_out._base,
				 "Slave has desynchronized event count.  "
				 "EC: %" PRIu64 "  "
				 "Expect [%" PRIu64 "-1,%" PRIu64 "+1].",
				 slave->_msg.event_counter,
				 ts->prev_event_counter - 1,
				 ts->_triva_ctrl->_msg.event_counter + 1);
		bad_ec_sync = 1;
	      }
	  }
	PD_LL_FOREACH(ts->_ebs, iter)
	  {
	    lwroc_net_trigbus *eb =
	      PD_LL_ITEM(iter, lwroc_net_trigbus, _slaves);

	    bug_fatal |=
	      eb->_msg.status & LWROC_READOUT_STATUS_STHR_BUG_FATAL;

	    eb_lost_conn |= (eb->_msg._eb_srcs_waiting != 0);
	  }

	/* In case we are going to print any faults or warnings, also
	 * print the status of all systems.
	 */
	if (bad_ec_sync || other_issue || eb_lost_conn || bug_fatal ||
	    (!(ts->_poll2_flags & LWROC_POLL2_REGULAR) &&
	     (wait_buf_space || in_readout || headache)))
	  ts->_poll2_flags |= LWROC_POLL2_PRINT_STATUS;

	/* Now do any printing, before do any actual bailing. */
	if (ts->_poll2_flags & LWROC_POLL2_PRINT_STATUS)
	  {
	    char str[1024];

	    /* Master having deadtime is only really significant if slave has.
	     */
	    LWROC_INFO_WARNING_FMT((ts->_triva_ctrl->_msg.deadtime &&
				    !slave_dt) ||
				   ts->_triva_ctrl->_msg.status,
				   "Master: deadtime: %d.  Status: 0x%x%s.  "
				   "EC: %" PRIu64 "",
				   ts->_triva_ctrl->_msg.deadtime,
				   ts->_triva_ctrl->_msg.status,
				   lwroc_triva_str_readout_status/*(*/
				   (str,ts->_triva_ctrl->_msg.status),
				   ts->_triva_ctrl->_msg.event_counter);
	    PD_LL_FOREACH(ts->_slaves, iter)
	      {
		lwroc_net_trigbus *slave =
		  PD_LL_ITEM(iter, lwroc_net_trigbus, _slaves);

		LWROC_CINFO_WARNING_FMT(&slave->_out._base,
					slave->_msg.deadtime ||
					slave->_msg.status,
					"Slave: deadtime: %d.  Status: 0x%x%s.  "
					"EC: %" PRIu64 "",
					slave->_msg.deadtime,
					slave->_msg.status,
					lwroc_triva_str_readout_status/*(*/
					(str,slave->_msg.status),
					slave->_msg.event_counter);
	      }
	    PD_LL_FOREACH(ts->_ebs, iter)
	      {
		lwroc_net_trigbus *eb =
		  PD_LL_ITEM(iter, lwroc_net_trigbus, _slaves);

		LWROC_CINFO_WARNING_FMT(&eb->_out._base,
					eb->_msg.status,
					"EB: Status: 0x%x%s.  "
					/*"EC: %" PRIu64 ""*/,
					eb->_msg.status,
					lwroc_triva_str_readout_status/*(*/
					(str,eb->_msg.status)/*,*/
					/*eb->_msg.event_counter*/);
	      }
	  }

	/* We have given the user all info about things being bad.
	 * Can now take actions.
	 */

	if (bad_ec_sync) /* Already reported why. */
	  {
	    GOTO_HAD_ISSUE(master_had_issue, LWROC_TRIVA_FAIL_BAD_EC_SYNC);
	  }

	if (other_issue)
	  {
	    LWROC_ERROR("Slave reported issue.  Restart.");
	    GOTO_HAD_ISSUE(master_had_issue,
			   lwroc_triva_state_status_2_failure(other_issue));
	  }
	if (eb_lost_conn)
	  {
	    LWROC_ERROR("Event builder lost data connection.  Restart.");
	    GOTO_HAD_ISSUE(master_had_issue, LWROC_TRIVA_FAIL_EB_DATA_CONN);
	  }

	if (bug_fatal)
	  {
	    LWROC_ERROR("Some system has had a bug or fatal error.  "
			"Is sleeping waiting for debugger.  "
			"Manual intervention required.");
	    /* This is a serious condition with some system, which
	     * cannot be fixed by us trying to restart...
	     *
	     * Just continue diagnosis...?
	     */
	  }

	/* We have now dealt with the fatal issues that need restart.
	 * have we found some valid reasons to wait further?
	 */

	if (!(ts->_poll2_flags & LWROC_POLL2_REGULAR))
	  {
	    if (wait_buf_space)
	      {
		LWROC_WARNING("Node(s) waiting for free buffer space, "
			      "waiting...");
		SET_MON_STATUS(LWROC_TRIVA_STATUS_WAIT_BUF);
		goto run_master_setup_pwait;
	      }
	    if (in_readout)
	      {
		if (headache)
		  {
		    LWROC_WARNING("Node(s) busy in readout, "
				  "with headache, waiting...");
		    SET_MON_STATUS(LWROC_TRIVA_STATUS_HEADACHE);
		  }
		else
		  {
		    LWROC_WARNING("Node(s) busy in readout, waiting...");
		    SET_MON_STATUS(LWROC_TRIVA_STATUS_WAIT_READ);
		  }
		goto run_master_setup_pwait;
	      }
	  }

	/* Are we trying to stop? */

	if (ts->_poll2_flags & LWROC_POLL2_FOR_STOP)
	  {
	    if (ts->_acq_state == LWROC_ACQ_STATE_REQ_STOPPING &&
		all_ec_same)
	      {
		LWROC_INFO("Stopping, and all systems have seen "
			   "same event number.  Issue trig 15.");
		/* TODO: Hmm, we must also check that master does not have
		 * a pending trigger that it has not processed yet.  And
		 * if it has done that, that it also has gotten far enough
		 * to update its event counter.  Then all is fine.
		 */
		ts->_state = LWROC_TRIVA_STATE_MASTER_GO_TRIG15;
		goto run_master_go_trig15;
	      }

	    if (ts->_acq_state == LWROC_ACQ_STATE_REQ_STOP15)
	      {
		if (all_held)
		  {
		    LWROC_INFO("Stopping, and all systems held.");
		    ts->_acq_state = LWROC_ACQ_STATE_REQ_STOPPED;
		    SET_MON_STATUS(LWROC_TRIVA_STATUS_STOP);
		    /* Note: we are not resetting error time counter
		     * just because we stopped!
		     */
		  }
		else
		  LWROC_INFO("Some slave has not seen final trigger yet.");
	      }
	    /* Go back to normal polling. */
	    goto run_master_setup_pwait;
	  }

	/* Did we come here due to requested deep polling?
	 * If so, we are not in any trouble, so stop diagnosing.
	 */

	if (ts->_poll2_flags & (LWROC_POLL2_FOR_LAM |
				LWROC_POLL2_FOR_FORCE_EB |
				LWROC_POLL2_REGULAR))
	  {
	    /* Things are looking good.  Again?  Set good run status. */
	    if (ts->_mon_status != LWROC_TRIVA_STATUS_STOP)
	      {
		if (headache)
		  SET_MON_STATUS(LWROC_TRIVA_STATUS_HEADACHE);
		else
		  SET_MON_STATUS(LWROC_TRIVA_STATUS_RUN);
	      }
	    /* Go back to normal polling. */
	    goto run_master_setup_pwait;
	  }

	/* TODO: wherever we propagate HEADACHE info into the
	 * SET_MON_STATUS, we should also propagate BUG_FATAL.
	 */
	if (headache)
	  {
	    SET_MON_STATUS(LWROC_TRIVA_STATUS_HEADACHE);
	    LWROC_WARNING("No diagnose for global deadtime, "
			  "but user routine (recently) reported headache.  "
			  "Waiting...");
	    /* Go back to normal polling. */
	    goto run_master_setup_pwait;
	  }
      }

      /* At this point we have dealt with all issues that we can
       * diagnose.
       *
       * There should really be none left.  I.e., if we reach this
       * point, something strange has happened.  Assuming the user has
       * not willingly temporarily stopped ^Z a (thread!), we should
       * not even end up here.
       *
       * E.g. if a trimi back link is broken such that the deadtime
       * does not come back, then master is simply not in deadtime.
       * There will be mismatch, but we do not end up here.
       *
       * If a link is broken forward, the slave will also have no
       * trigger, and generate no data.  Eventually, the EB will fill
       * up the input buffers, and master or other system gets stuck.
       * While diagnosing that, we will notice the out-of-sync event
       * counters of the slave missing triggers.  This can be
       * diagnosed early by regularly checking slave systems.
       */

      /* Either a slave reported that it had issues or not, master is
       * still unhappy.  What to do?  Wait indefinitely, or restart?
       *
       * Possibly we should wait a bit longer until we pull the plug,
       * but eventually we need to get on track again by forcing a
       * restart.
       */

#if 0
      {
	/* If we should go this route, it should be timeout-based. */
	LWROC_WARNING("Enough waiting for progress, going for restart.");
	GOTO_HAD_ISSUE(master_had_issue, ? ? ?);
      }
#endif

      /* Things are looking strange. */
      SET_MON_STATUS(LWROC_TRIVA_STATUS_NODIAGNOSE);
      LWROC_WARNING("Unable to diagnose reason for global deadtime.  "
		    "Waiting...");
      /* Go back to normal polling. */
      goto run_master_setup_pwait;



    run_master_go_stop:
    case LWROC_TRIVA_STATE_MASTER_GO_STOP:
      /* Send master to stop mode. */

      LWROC_TRIVA_TS_MSG_PREPARE(&ts->_triva_ctrl->_msg, ts,
				 LWROC_TRIVA_CONTROL_ACQ_STOP);

      lwroc_net_trigctrl_send_msg(ts->_triva_ctrl);
      PD_LL_ADD_BEFORE(&ts->_wait_slaves, &ts->_triva_ctrl->_wait_slaves);

      ts->_state = LWROC_TRIVA_STATE_MASTER_WAIT_STOP;
      break;

    case LWROC_TRIVA_STATE_MASTER_WAIT_STOP:
      /* Wait for the module process to report successful stop. */

      WAIT_SLAVES_CHECK_ISSUE("master to stop acquisition",
			      master_had_issue,
			      0, LWROC_TRIVA_FAIL_LOCAL);

      ts->_acq_state = LWROC_ACQ_STATE_REQ_STOPPING;
      ts->_state = LWROC_TRIVA_STATE_MASTER_GO_RUNNING_POLL;
      goto run_master_poll;



    run_master_go_trig15:
    case LWROC_TRIVA_STATE_MASTER_GO_TRIG15:
      /* Send master to run mode. */

      LWROC_TRIVA_TS_MSG_PREPARE(&ts->_triva_ctrl->_msg, ts,
				 LWROC_TRIVA_CONTROL_TRIG15);

      lwroc_net_trigctrl_send_msg(ts->_triva_ctrl);
      PD_LL_ADD_BEFORE(&ts->_wait_slaves, &ts->_triva_ctrl->_wait_slaves);

      ts->_state = LWROC_TRIVA_STATE_MASTER_WAIT_TRIG15;
      break;

    case LWROC_TRIVA_STATE_MASTER_WAIT_TRIG15:
      /* Wait for the module process to report successful stop. */

      WAIT_SLAVES_CHECK_ISSUE("master to set trig15, final stop acquisition",
			      master_had_issue,
			      0, LWROC_TRIVA_FAIL_LOCAL);

      ts->_acq_state = LWROC_ACQ_STATE_REQ_STOP15;
      ts->_state = LWROC_TRIVA_STATE_MASTER_GO_RUNNING_POLL;
      goto run_master_poll;





    eb_check_had_issue:
      /* If we detect an issue during EB connection validation, we may
       * still have a number of outstanding messages to EBs and other
       * slaves.  The issue means that we have to cycle back, but
       * before that we must first consume any outstanding responses.
       */
      ts->_mon_status = LWROC_TRIVA_STATUS_REINIT;

      /* Any EB that is waiting for a timeout to send another message
       * can be removed from the list.
       */
      PD_LL_FOREACH_TMP(ts->_wait_slaves, iter, tmp_iter)
	{
	  lwroc_net_trigbus *eb =
	    PD_LL_ITEM(iter, lwroc_net_trigbus, _wait_slaves);

	  if (eb->_state == LWROC_NET_TRIGBUS_STATE_DELAY_MSG_SEND)
	    PD_LL_REMOVE(iter);
	}

      ts->_state = LWROC_TRIVA_STATE_EB_CHECK_CLEANUP;
      FALL_THROUGH;

    case LWROC_TRIVA_STATE_EB_CHECK_CLEANUP:
      if (!PD_LL_IS_EMPTY(&ts->_wait_slaves) ||
	  !PD_LL_IS_EMPTY(&ts->_wait_slaves2))
	{
	  lwroc_report_wait_slaves(now, "retire failed EB validation",
				   LWROC_TRIVA_STATUS_WAIT_ABORT);
	  break;
	}
      SET_MON_STATUS_NONWAIT;

      /* We certainly had an issue, as that is why we came here... */
      goto master_had_issue;




    master_had_issue:
      /* We come here when we have been in running mode, but detected
       * a stall and subsequent failure of at least one connection.
       */
      LWROC_WARNING_FMT("Issue during test/run (%d), "
			"tell master and slaves to abort.", ts->_state);

      ts->_mon_status = LWROC_TRIVA_STATUS_REINIT;

      /* Get master and all working slaves out of their test or
       * readout loops, such that we can do some orderly
       * reinitialisation.  If some slave has gotten lost, that's fine
       * too.
       */

      printf ("Send slave abort readout. (%d)\n",
	      PD_LL_IS_EMPTY(&ts->_wait_slaves));

      PD_LL_FOREACH(ts->_slaves, iter)
	{
	  lwroc_net_trigbus *slave =
	    PD_LL_ITEM(iter, lwroc_net_trigbus, _slaves);

	  /* If the slave has had an issue and cannot send messages
	   * any longer, no need to ignore it.  It will have to recover at
	   * the init stage instead.
	   */

	  if (slave->_status != LWROC_NET_TRIGBUS_STATUS_SLAVE ||
	      slave->_state != LWROC_NET_TRIGBUS_STATE_WAIT_MSG_SEND)
	    continue;

	  printf ("Send slave abort (0x%x)... (%d,%d)\n",
		  ts->_slave_abort_type,
		  slave->_status, slave->_state);

	  LWROC_TRIVA_TS_MSG_PREPARE(&slave->_msg, ts,
				     ts->_slave_abort_type);
	  lwroc_net_trigbus_send_msg(slave, 0);
	  PD_LL_ADD_BEFORE(&ts->_wait_slaves, &slave->_wait_slaves);
	}

      /* Also tell master to abort. */

      LWROC_TRIVA_TS_MSG_PREPARE(&ts->_triva_ctrl->_msg, ts,
				 ts->_master_abort_type);
      printf ("Send master abort (0x%x).\n",
	      ts->_master_abort_type);
      lwroc_net_trigctrl_send_msg(ts->_triva_ctrl);
      PD_LL_ADD_BEFORE(&ts->_wait_slaves, &ts->_triva_ctrl->_wait_slaves);

      ts->_master_abort_type = 0;
      ts->_slave_abort_type = 0;

      /* Any ongoing control action needs to be bumped to failure state,
       * such that it gets reported.
       */
      switch (ts->_acq_state)
	{
	case LWROC_ACQ_STATE_REQ_RUN:
	  break;
	case LWROC_ACQ_STATE_REQ_STOP:
	case LWROC_ACQ_STATE_REQ_STOPPING:
	case LWROC_ACQ_STATE_REQ_STOP15:
	  ts->_acq_state = LWROC_ACQ_STATE_REQ_STOP_REINIT;
	  break;
	case LWROC_ACQ_STATE_REQ_STOPPED:
	case LWROC_ACQ_STATE_REQ_STOP_REINIT:
	case LWROC_ACQ_STATE_REQ_HOLD:
	  break;
	case LWROC_ACQ_STATE_REQ_START:
	  ts->_acq_state = LWROC_ACQ_STATE_REQ_START_REINIT;
	  break;
	case LWROC_ACQ_STATE_REQ_STARTED:
	case LWROC_ACQ_STATE_REQ_START_REINIT:
	  break;
	default:
	  assert(0);
	}

      ts->_state = LWROC_TRIVA_STATE_MASTER_WAIT_SLAVE_ABORT_READOUT;
      break;

    case LWROC_TRIVA_STATE_MASTER_WAIT_SLAVE_ABORT_READOUT:

      if (!PD_LL_IS_EMPTY(&ts->_wait_slaves))
	{
	  lwroc_report_wait_slaves(now, "master/slaves to abort test/readout",
				   LWROC_TRIVA_STATUS_WAIT_ABORT);
	  break;
	}
      SET_MON_STATUS_NONWAIT;

      /* It is not much use to check if any slave had an issue here,
       * since we in any case will go back to the initialisation.
       */

      printf ("Error handling done, back to init!\n");

      goto init_master_restart;








      /*
       *   *************************
       *   *  SLAVE  SLAVE  SLAVE  *
       *   *************************
       */

    init_slave_restart:
    case LWROC_TRIVA_STATE_SLAVE_INIT:

      SET_MON_STATUS(LWROC_TRIVA_STATUS_SLAVE);

      ts->_state = LWROC_TRIVA_STATE_SLAVE_STARTUP;
      ts->_had_issue = 0;
      ts->_slave_abort_type = 0;
      ts->_got_look_at_me = 0;

      FALL_THROUGH;

    case LWROC_TRIVA_STATE_SLAVE_STARTUP:
      /* Wait for master to request initialisation. */
      /* If we came here due to an error, we shall NOT reinitialise
       * on our own, since that possibly destroys debug possibilities
       * on the bus.
       */

      /* If master is not even connected, we cannot do anything! */

      if (ts->_master->_status != LWROC_NET_TRIGBUS_STATUS_MASTER)
	return;
      if (ts->_master->_state == LWROC_NET_TRIGBUS_STATE_FRESH_MASTER)
	ts->_master->_state = LWROC_NET_TRIGBUS_STATE_EXT_READ_SETUP;

      ts->_master_msg_types = LWROC_TRIVA_CONTROL_INITIALISE;
      FALL_THROUGH;

    case LWROC_TRIVA_STATE_SLAVE_WAIT_MSG_FROM_MASTER:
      if (ts->_got_look_at_me)
	{
	  lwroc_net_trigbus_send_look_at_me(ts->_master);
	  ts->_got_look_at_me = 0;
	}
      /* Get next message from master. */
      if (!lwroc_net_trigbus_recv_msg(ts->_master,
				      ts->_master_msg_types,
				      &ts->_had_issue))
	return;

      if (ts->_had_issue)
	goto slave_had_issue;

      /* Get the delayed eb status before forwarding. */
      ts->delayed_eb_status = ts->_master->_msg.delayed_eb_status;

      /* We forward the message to the control/readout process. */
      LWROC_TRIVA_TS_MSG_PREPARE(&ts->_triva_ctrl->_msg, ts,
				 ts->_master->_msg._type);

      /* What to forward depends on the message. */
      switch (ts->_master->_msg._type)
	{
	case LWROC_TRIVA_CONTROL_INITIALISE:
	  ts->_triva_ctrl->_msg.bus_enable = 1;
	  ts->_triva_ctrl->_msg.master = 0;
	  ts->_triva_ctrl->_msg.fctime = _lwroc_triva_config._fctime;
	  ts->_triva_ctrl->_msg.ctime  = _lwroc_triva_config._ctime;
	  ts->_triva_ctrl->_msg.event_counter =
	    ts->_master->_msg.event_counter;

	  ts->_master_msg_types =
	    LWROC_TRIVA_CONTROL_INJECT_IDENT |
	    LWROC_TRIVA_CONTROL_ABORT_TEST;
	  break;

	case LWROC_TRIVA_CONTROL_INJECT_IDENT:
	  ts->_triva_ctrl->_msg._eb_ident0 = ts->_master->_msg._eb_ident0;
	  ts->_triva_ctrl->_msg._eb_ident1 = ts->_master->_msg._eb_ident1;
	  ts->_triva_ctrl->_msg._eb_ident2 = ts->_master->_msg._eb_ident2;
	  /*
	  printf ("Forward IDENT %08x %08x %08x\n",
		  ts->_triva_ctrl->_msg._eb_ident0,
		  ts->_triva_ctrl->_msg._eb_ident1,
		  ts->_triva_ctrl->_msg._eb_ident2);
	  */
	  ts->_master_msg_types =
	    LWROC_TRIVA_CONTROL_INJECT_IDENT |
	    LWROC_TRIVA_CONTROL_GO_TEST |
	    LWROC_TRIVA_CONTROL_ABORT_TEST;
	  break;
	case LWROC_TRIVA_CONTROL_GO_TEST:
	  ts->_master_msg_types =
	    LWROC_TRIVA_CONTROL_GET_TRIG |
	    LWROC_TRIVA_CONTROL_GO_RUN |
	    LWROC_TRIVA_CONTROL_ABORT_READOUT;
	  break;
	case LWROC_TRIVA_CONTROL_GET_TRIG:
	  ts->_master_msg_types =
	    LWROC_TRIVA_CONTROL_RELEASE_DT |
	    LWROC_TRIVA_CONTROL_ABORT_TEST;
	  break;
	case LWROC_TRIVA_CONTROL_RELEASE_DT:
	  ts->_master_msg_types =
	    LWROC_TRIVA_CONTROL_GET_TRIG |
	    LWROC_TRIVA_CONTROL_GO_RUN |
	    LWROC_TRIVA_CONTROL_ABORT_TEST;
	  break;
	case LWROC_TRIVA_CONTROL_GO_RUN:
	  ts->_master_msg_types =
	    LWROC_TRIVA_CONTROL_RUN_STATS |
	    LWROC_TRIVA_CONTROL_ABORT_READOUT;
	  break;
	case LWROC_TRIVA_CONTROL_RUN_STATS:
	  ts->_triva_ctrl->_msg.deadtime = ts->_master->_msg.deadtime;

	  ts->_master_msg_types =
	    LWROC_TRIVA_CONTROL_RUN_STATS |
	    LWROC_TRIVA_CONTROL_GO_TEST |
	    LWROC_TRIVA_CONTROL_ABORT_READOUT;
	  break;
	}

      lwroc_net_trigctrl_send_msg(ts->_triva_ctrl);
      PD_LL_ADD_BEFORE(&ts->_wait_slaves, &ts->_triva_ctrl->_wait_slaves);

      ts->_state = LWROC_TRIVA_STATE_SLAVE_WAIT_RESPONSE;
      break;

    case LWROC_TRIVA_STATE_SLAVE_WAIT_RESPONSE:
      /* Wait for the control/readout process to report status. */

      if (!PD_LL_IS_EMPTY(&ts->_wait_slaves))
	{
	  lwroc_report_wait_slaves(now, "our slave actual action",
				   LWROC_TRIVA_STATUS_SLAVE_WAIT_SELF);
	  break;
	}
      SET_MON_STATUS_NONWAIT;

      ts->_next_report.tv_sec = 0;

      if (ts->_had_issue)
	{
	  /* TODO: readout process failed, master has to be told. */
	  /* By breaking connection?  By reporting that we are toast? */
	  goto slave_had_issue;
	}

      /* Report to master. */

      LWROC_TRIVA_TS_MSG_PREPARE(&ts->_master->_msg, ts,
				 ts->_triva_ctrl->_msg._type);

      switch(ts->_triva_ctrl->_msg._type)
	{
	case LWROC_TRIVA_CONTROL_INITIALISE:
	  /* From now, slave would need abort before init. */
	  ts->_slave_abort_type = LWROC_TRIVA_CONTROL_ABORT_TEST;
	  break;

	case LWROC_TRIVA_CONTROL_GET_TRIG:
	  /* From now, slave would need abort before init. */
	  ts->_master->_msg._trig =
	    ts->_triva_ctrl->_msg._trig;
	  ts->_master->_msg.event_counter =
	    ts->_triva_ctrl->_msg.event_counter;
	  ts->_master->_msg.status =
	    ts->_triva_ctrl->_msg.status;
	  break;

	case LWROC_TRIVA_CONTROL_GO_RUN:
	  /* From now, slave would need abort before init. */
	  ts->_slave_abort_type = LWROC_TRIVA_CONTROL_ABORT_READOUT;
	  break;

	case LWROC_TRIVA_CONTROL_RUN_STATS:
	  /* Copy the status info. */
	  ts->_master->_msg.event_counter =
	    ts->_triva_ctrl->_msg.event_counter;
	  ts->_master->_msg.deadtime =
	    ts->_triva_ctrl->_msg.deadtime;
	  ts->_master->_msg.status =
	    ts->_triva_ctrl->_msg.status;

	  /* And the deadtime measurement info. */
	  lwroc_triva_fill_meas_dt(&ts->_triva_ctrl->_msg);
	  break;
	}

      /*
      if (ts->_triva_ctrl->_msg.status & LWROC_READOUT_STATUS_TERM_REQ)
	printf ("slave->master, status & ..._TERM_REQ, send %08x\n",
		ts->_master->_msg.status);
      */

      lwroc_net_trigbus_send_msg(ts->_master, 0);

      if (ts->_triva_ctrl->_msg._type == LWROC_TRIVA_CONTROL_ABORT_TEST ||
	  ts->_triva_ctrl->_msg._type == LWROC_TRIVA_CONTROL_ABORT_READOUT)
	{
	  /* We have on direct request from master aborted the readout.
	   * Go back to init...
	   */
	  LWROC_INFO("Slave aborted on request from master!");
	  goto init_slave_restart;
	}

      ts->_state = LWROC_TRIVA_STATE_SLAVE_WAIT_MSG_FROM_MASTER;
      break;


    slave_had_issue:
      /* We come here when we have been in running mode, but master
       * has detected a stall or disconnected, so we need to tell
       * the readout to get out of the loop.
       */
      /* TODO: we also come here when the readout has crapped up on us.
       * need to be handled elsewhere?  As the master connection still
       * thinks things are fine?
       */
      if (ts->_slave_abort_type == 0)
	{
	  LWROC_ERROR("TRIVA/TRIGGER slave BUS failure to initialise, "
		      "start over.");
	  goto init_slave_restart;
	}

      LWROC_WARNING("Issue during slave test/run, tell to abort.");

      /* Tell slave to abort. */

      LWROC_TRIVA_TS_MSG_PREPARE(&ts->_triva_ctrl->_msg, ts,
				 ts->_slave_abort_type);
      printf ("Send slave abort readout.\n");
      lwroc_net_trigctrl_send_msg(ts->_triva_ctrl);
      PD_LL_ADD_BEFORE(&ts->_wait_slaves, &ts->_triva_ctrl->_wait_slaves);

      ts->_state = LWROC_TRIVA_STATE_SLAVE_WAIT_ABORT;
      break;

    case LWROC_TRIVA_STATE_SLAVE_WAIT_ABORT:

      if (!PD_LL_IS_EMPTY(&ts->_wait_slaves))
	{
	  lwroc_report_wait_slaves(now, "our slave to abort test/readout",
				   LWROC_TRIVA_STATE_SLAVE_WAIT_ABORT);
	  break;
	}
      SET_MON_STATUS_NONWAIT;
      /* It is not much use to check if any slave had an issue here,
       * since we in any case will go back to the initialisation.
       */

      printf ("Error handling done, back to init!\n");

      /* TODO: should go back at least to a better named state. */
      goto init_slave_restart;
    }


}

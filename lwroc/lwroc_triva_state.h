/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_TRIVA_STATE_H__
#define __LWROC_TRIVA_STATE_H__

#include "lwroc_net_trigbus.h"
#include "../dtc_arch/acc_def/time_include.h"
#include "lwroc_data_pipe.h"

/* We are in charge of controlling the state of the trigger module
 * (TRIVA/TRIMI), and in particular its initialisation sequence as
 * well as subsystem interrogation in case of excessive deadtime.
 *
 * We are not in direct contact with the triva, and are neither the
 * readout process.  The readout is in a separate process (for now:
 * thread), and the immediate (behind-the-scenes) additional TRIVA
 * communication is handled by another thread in that process.  That
 * thread is also responsible for communicating with us.
 *
 * Communication with the different slaves (or master) in (remote)
 * computers go via the usual before/after select network update
 * functions for each open socket descriptor.  We may queue messages
 * to them, and are also 'woken' up when some message has arrived.
 *
 * For simplicity, each such connection holds any incoming message
 * until it has been handled.  Until it has been handled, it will not
 * work on receiving any next one.  (i.e. it makes no sense to send
 * two in a row).  It is also holding one output message at a time; a
 * second message cannot be queued until the first has been sent.
 *
 * Our state update function is called after all 'after-select'
 * functions have been processed, such that all incoming messages for
 * this select cycle has arrived.  We may then queue up messages.
 *
 * The network handling routines are also responsible for handling
 * timeouts.  When a message is enqueued that requires a response, a
 * timer may be set.  If it expires, we will note that it expired, and
 * also tear down the connection, and also inform this routine that it
 * failed.  The idea being that this has nothing to do with a system
 * being in DT or not - the CPU of it shall *always* be alert to respond
 * to network messages within some reasonable time.  This also means
 * that a slave system never sends messages on their own.  Everything
 * is a response.
 *
 *
 */

/* Test sequences:
 *
 * TRIVA:
 */


/* SL = slave, MS = master */

/* TRIVA module to be master.  Setup wait for all
 * slaves to be connected. */
#define LWROC_TRIVA_STATE_MASTER_INITIAL           1

/* Wait for all slaves to be connected. */
#define LWROC_TRIVA_STATE_MASTER_WAIT_SLAVE_CONN   2

#define LWROC_TRIVA_STATE_EB_CLEAR                 3

#define LWROC_TRIVA_STATE_WAIT_EB_CLEAR            4

/* TRIVA module to be master.  Is in unknown state, e.g. at startup, or
 * after some mismatch.  Initialisation needed - request it. */
#define LWROC_TRIVA_STATE_MASTER_STARTUP           5

/* Wait for module to finish initialisation. */
#define LWROC_TRIVA_STATE_MASTER_WAIT_INIT         6

/* TRIVA module to be master.  (Trigger bus enabled if slaves should
 * be present).  In HALT mode.  Tell slaves to initialise. */
#define LWROC_TRIVA_STATE_MASTER_INITIALIZED       7

/* Wait for slaves to initialise. */
#define LWROC_TRIVA_STATE_MASTER_WAIT_SLAVE_INIT   8

/* Everyone is initialised.
 * Setup to wait for all EBs to validate. */
#define LWROC_TRIVA_STATE_MASTER_SLAVE_INITED      9

/* Wait for all EBs to validate all connections. */
#define LWROC_TRIVA_STATE_EB_CHECK                10

/* All EBs have validated.
 * Get all slaves into test mode. */
#define LWROC_TRIVA_STATE_MASTER_GO_SLAVE_TEST    11

/* Wait for slaves to get into test mode. */
#define LWROC_TRIVA_STATE_MASTER_WAIT_SLAVE_TEST  12

/* TRIVA module to be master.
 * Trigger accept loop controlled by comm thread. */
#define LWROC_TRIVA_STATE_MASTER_GO_TEST          13

/* Wait for the master to get into test mode. */
#define LWROC_TRIVA_STATE_MASTER_WAIT_TEST        14

/* Check triggers in all systems. */
#define LWROC_TRIVA_STATE_TEST_GET_TRIG           15

/* Wait for all systems to report triggers. */
#define LWROC_TRIVA_STATE_TEST_WAIT_GET_TRIG      16

/* Release DT in all systems except one. */
#define LWROC_TRIVA_STATE_TEST_MOST_RELEASE_DT    17

/* Wait for all except one systems to release DT. */
#define LWROC_TRIVA_STATE_TEST_WAIT_MOST_REL_DT   18

/* Check that DT is still set in master system. */
#define LWROC_TRIVA_STATE_TEST_GET_TRIG2          19

/* Wait for master system to check DT. */
#define LWROC_TRIVA_STATE_TEST_WAIT_GET_TRIG2     20

/* Release DT in last system. */
#define LWROC_TRIVA_STATE_TEST_LAST_RELEASE_DT    21

/* Wait for last system to release DT. */
#define LWROC_TRIVA_STATE_TEST_WAIT_LAST_REL_DT   22



/* Get all slaves into run mode. */
#define LWROC_TRIVA_STATE_MASTER_GO_SLAVE_RUN     23

/* Wait for the slaves to get into run mode. */
#define LWROC_TRIVA_STATE_MASTER_WAIT_SLAVE_RUN   24

/* TRIVA module is master.  Get into run mode. */
#define LWROC_TRIVA_STATE_MASTER_GO_RUN           25

/* Wait for the master to get into run mode. */
#define LWROC_TRIVA_STATE_MASTER_WAIT_RUN         26

/* TRIVA module to be master, running (in GO mode).
 * Trigger accept loop is controlled by the readout thread. */
/* Poll status of master readout. */
#define LWROC_TRIVA_STATE_MASTER_GO_RUNNING_POLL  27

/* Get status of master readout. */
/* Investigate result. */
#define LWROC_TRIVA_STATE_MASTER_GO_RUNNING_PGET  28

/* Wait until doing next poll. */
#define LWROC_TRIVA_STATE_MASTER_GO_RUNNING_PWAIT 29


/* Poll status of all systems, first slaves and EBs. */
#define LWROC_TRIVA_STATE_MASTER_GO_RUNNING_SLAVE_POLL  30

/* Get status of all slaves and EBs. */
#define LWROC_TRIVA_STATE_MASTER_GO_RUNNING_SLAVE_PGET  31

/* Poll again status of master readout. */
#define LWROC_TRIVA_STATE_MASTER_GO_RUNNING_POLL2       32

/* Get again status of master readout. */
/* Investigate results. */
#define LWROC_TRIVA_STATE_MASTER_GO_RUNNING_PGET2       33


/* Make master stop acquisition. */
#define LWROC_TRIVA_STATE_MASTER_GO_STOP          34

/* Wait for master to stop acquisition. */
#define LWROC_TRIVA_STATE_MASTER_WAIT_STOP        35


/* Make master stop acquisition. */
#define LWROC_TRIVA_STATE_MASTER_GO_TRIG15        36

/* Wait for master to stop acquisition. */
#define LWROC_TRIVA_STATE_MASTER_WAIT_TRIG15      37





/* Wait for outstanding responses of all EBs and slaves. */
#define LWROC_TRIVA_STATE_EB_CHECK_CLEANUP              38

/* Wait until master and slaves have aborted test/readout. */
#define LWROC_TRIVA_STATE_MASTER_WAIT_SLAVE_ABORT_READOUT 39






/* TRIVA module to be slave. */
#define LWROC_TRIVA_STATE_SLAVE_INIT             40
/* Is in unknown state, e.g. at startup, or after some mismatch. */
#define LWROC_TRIVA_STATE_SLAVE_STARTUP          41

/* Wait for message from master to send to control/readout. */
#define LWROC_TRIVA_STATE_SLAVE_WAIT_MSG_FROM_MASTER  42

/* Wait for control(readout) to report response. */
#define LWROC_TRIVA_STATE_SLAVE_WAIT_RESPONSE    43

/* We have had to tell control(readout) to abort current action. */
#define LWROC_TRIVA_STATE_SLAVE_WAIT_ABORT       44



/* Usual state, either run, or try to get running. */
#define LWROC_ACQ_STATE_REQ_RUN            1
/* User requested stop, we are here until stop done. */
#define LWROC_ACQ_STATE_REQ_STOP           2
/* Stopping initiated (HALT). */
#define LWROC_ACQ_STATE_REQ_STOPPING       3
/* Stopping initiated (sent MT=15). */
#define LWROC_ACQ_STATE_REQ_STOP15         4
/* Stop performed, can inform user that stop happened. */
#define LWROC_ACQ_STATE_REQ_STOPPED        5
/* Stop ended up in reinit.  (We will be held.) */
#define LWROC_ACQ_STATE_REQ_STOP_REINIT    6
/* User is informed, so we try to not run again. */
#define LWROC_ACQ_STATE_REQ_HOLD           7
/* User requested start, so try to get up again. */
#define LWROC_ACQ_STATE_REQ_START          8
/* Start succeeded, inform user. */
#define LWROC_ACQ_STATE_REQ_STARTED        9
/* Start ended up in reinit.  (We will be held, as we report failure.) */
#define LWROC_ACQ_STATE_REQ_START_REINIT  10


/* We have requested to stop due to control request.
 * Can only be removed by start request. */
#define LWROC_ACQ_STOP_REASON_CONTROL    0x01
/* We have requested to stop since some slave wanted to terminate.
 * Removed once that slave has disconnected. */
#define LWROC_ACQ_STOP_REASON_TERMINATE  0x02



#define LWROC_POLL2_FOR_STOP      0x0001
#define LWROC_POLL2_FOR_LAM       0x0002
#define LWROC_POLL2_DO_LAM        0x0004
#define LWROC_POLL2_PRINT_STATUS  0x0008
#define LWROC_POLL2_STUCK         0x0010
#define LWROC_POLL2_FOR_FORCE_EB  0x0020
#define LWROC_POLL2_REGULAR       0x0040

typedef struct lwroc_triva_state_t
{
  int _state;

  PD_LL_SENTINEL_ITEM(_slaves);
  PD_LL_SENTINEL_ITEM(_ebs);
  PD_LL_SENTINEL_ITEM(_wait_slaves);
  PD_LL_SENTINEL_ITEM(_wait_slaves2);

  /* State of acquisition requested by control.  (Normally not used, so
   * usually run.)
   */
  int _acq_state;
  /* Current outstanding control request. */
  lwroc_acqr_request *_acqr_req;

  /* This is set by any message reception handler whenever we did not
   * like the response, and therefore will go back to basics, i.e.
   * start over.
   */
  int _had_issue;
  /* This keeps track of why we have stop acq request. */
  int _stop_reason;

  /* Progressive delay when waiting for ident messages. */
  int _ident_interval;

  /* Triva bus testing. */
  uint32_t _nslaves;
  uint32_t _test_no;
  lwroc_net_trigbus *_conn_last_release;

  /* For our lock-up detection: */
  uint32_t prev_deadtime;
  uint64_t prev_event_counter;
  int     _poll2_flags;
  int     _poll2_slave_counter;

  /* What is the delayed_eb status? */
  uint32_t delayed_eb_status;

  /* When we (possibly) have received a look-at-me from a slave/EB. */
  int     _got_look_at_me;

  /* For updating info from the readout, and pass along to the
   * monitor.  TODO: Should we really have this one?
   */
  lwroc_data_pipe_extra *_pipe_extra;

  lwroc_net_trigbus *_triva_ctrl;

  lwroc_net_trigbus *_master;

  /* Abort type needed for master. */
  uint32_t _master_abort_type;
  /* Abort type needed for slaves. */
  uint32_t _slave_abort_type;

  /* Bitmask of next possible messages from master. */
  uint32_t _master_msg_types;

  /* Time reference for init allowance value. */
  struct timeval _init_allowance_reftime;
  /* Re-init allowance. */
  double _init_allowance;
  /* Next init is allowed at this time. */
  struct timeval _next_init;

  /* Next time we resend ident messages. */
  struct timeval _next_ident_resend;

  /* Last report of why we are stuck. */
  struct timeval _next_report;
  int _report_interval;

  /* Status to report to monitor. */
  uint32_t _mon_status;
  uint32_t _mon_status_nonwait;
  /* Last failure to report to monitor. */
  uint32_t _mon_last_error;
  struct timeval _mon_time_error;

  /* Time until we run deadtime measurement. */
  struct timeval _measure_dt_end;
  struct timeval _measure_dt_start;

} lwroc_triva_state;

extern lwroc_triva_state *_lwroc_triva_state;

typedef struct lwroc_triva_config_t
{
  int       _kind;
  int       _master;

  uint32_t  _fctime;
  uint32_t  _ctime;

  char     *_bridge;
  uint32_t  _vmeaddr;
  uint32_t  _vmelen;

  int       _mapkind;
  char     *_sim_cfg;

  uint32_t  _delayed_eb_status;

} lwroc_triva_config;

extern lwroc_triva_config _lwroc_triva_config;

void lwroc_triva_state_setup(void);

void lwroc_triva_state_no_setup(void);

void lwroc_triva_state_update(struct timeval *now);


#endif/*__LWROC_TRIVA_STATE_H__*/

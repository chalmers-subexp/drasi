/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2019  Michael Munch  <mm.munk@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_tsm_writer.h"
#include "lwroc_message.h"
#include "lwroc_mon_block.h"
#include "lwroc_main_iface.h"
#include "lwroc_parse_util.h"
#include "lwroc_hostname_util.h"
#include "lwroc_parse_util.h"

#include <string.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <ctype.h>

#include "../dtc_arch/acc_def/has_tsmapi.h"
#include "../dtc_arch/acc_def/myinttypes.h"

#if HAS_TSMAPI

typedef struct lwroc_tsm_state_t {
  char *description;
  int is_connected;
  int is_opened;
  int verbose;
  struct session_t session;
  char* node;
  char* pw;
  char* pw_path;
  char* server;
  char* fs;
  char* filename_cur;

  lwroc_file_writer_info _info;
} lwroc_tsm_state;

void lwroc_tsm_writer_close(lwroc_file_writer_info* info);

void lwroc_tsm_disconnect(lwroc_tsm_state* state);
void lwroc_tsm_connect(lwroc_tsm_state* state);

void lwroc_tsm_writer_free(lwroc_file_writer_info* info)
{
  lwroc_tsm_state* state;
  state = CONTAINER_OF(info, lwroc_tsm_state, _info);

  lwroc_tsm_writer_close(info);
  lwroc_tsm_disconnect(state);
  tsm_cleanup(DSM_SINGLETHREAD);

  free(state->description);
  free(state->node);
  free(state->pw);
  free(state->pw_path);
  free(state->server);
  free(state->fs);
  free(state->filename_cur);

  free(state);
}

void lwroc_tsm_writer_init(lwroc_file_writer_info* info)
{
  lwroc_tsm_state* state;

  char const *username;
  char hostname[256];
  char *p;
  size_t bytes;

  state = CONTAINER_OF(info, lwroc_tsm_state, _info);

  if (tsm_init(DSM_SINGLETHREAD))
    LWROC_FATAL("tsm_init failed.");

  username = cuserid(NULL);
  if (!username)
    username = "nobody";
  lwroc_gethostname(hostname, sizeof hostname);
  bytes = strlen(username) + 1 + strlen(hostname) + 1;
  p = malloc(bytes);
  if (!p)
    LWROC_FATAL("Out of memory.");
  snprintf(p, bytes, "%s@%s", username, hostname);
  state->description = p;

  if (!state->node || !state->pw_path ||
      !state->server || !state->fs)
    LWROC_BADCFG("All four parameters for TSM must be set.");

  api_msg_set_level(state->verbose ? API_MSG_MAX : API_MSG_NORMAL);

  {
    FILE *file;
    char pw[256], *nl;

    file = fopen(state->pw_path, "r");
    if (!file)
      {
	LWROC_PERROR("fopen");
	LWROC_FATAL_FMT("Failure opening TSM password file '%s'.",
			state->pw_path);
      }
    if (!fgets(pw, sizeof pw, file))
      LWROC_FATAL("Failure reading TSM password file.");
    fclose(file);
    nl = strchr(pw, '\n');
    if (nl)
      *nl = '\0';
    state->pw = strdup_chk(pw);
  }
}

void lwroc_tsm_connect(lwroc_tsm_state* state)
{
  while (!state->is_connected)
    {
      struct login_t login;

      login_fill(&login,
		 state->server,
		 state->node,
		 state->pw,
		 "", /* Owner not needed? */
		 LINUX_PLATFORM,
		 state->fs,
		 DEFAULT_FSTYPE);
      memset(&state->session, 0, sizeof state->session);
      if (!tsm_fconnect(&login, &state->session))
	{
	  state->is_connected = 1;
	  break;
	}
      LWROC_ERROR("tsm_fconnect failed, will retry in 1 second.");
      sleep(1);
    }
}

void lwroc_tsm_disconnect(lwroc_tsm_state* state)
{
  if (!state->is_connected)
    return;

  tsm_fdisconnect(&state->session);
  state->is_connected = 0;
}

void lwroc_tsm_writer_close(lwroc_file_writer_info* info)
{
  lwroc_tsm_state* state;
  state = CONTAINER_OF(info, lwroc_tsm_state, _info);

  if (!state->is_opened)
    return;

  assert(state->is_connected);
  state->is_opened = 0;
  if (tsm_fclose(&state->session))
    {
      LWROC_ERROR("tsm_fclose failed.");
      /* Let us hope a new connection for the next file may help. */
      lwroc_tsm_disconnect(state);
    }
  free(state->filename_cur);
  state->filename_cur = NULL;
}

int lwroc_tsm_writer_open(char const* filename,
			  lwroc_file_writer_info* info)
{
  lwroc_tsm_state* state;
  state = CONTAINER_OF(info, lwroc_tsm_state, _info);

  assert(!state->is_opened);

  if (filename)
    {
      free(state->filename_cur);
      state->filename_cur = strdup_chk(filename);
    }

  lwroc_tsm_connect(state);

  if (tsm_fopen(state->fs,
		state->filename_cur,
		state->description,
		&state->session))
    {
      LWROC_ERROR_FMT("tsm_fopen(%s) failed.", state->filename_cur);
      lwroc_tsm_disconnect(state);
      return 0;
    }

  state->is_opened = 1;
  return 1;
}

ssize_t lwroc_tsm_writer_write(void* priv, const void* buf,
			       size_t count)
{
  lwroc_tsm_state* state;
  state = CONTAINER_OF(priv, lwroc_tsm_state, _info);

  return tsm_fwrite (buf, 1, count, &state->session);
}

int  lwroc_tsm_writer_handle_write_err(int err, int attempts,
				       lwroc_file_writer_info* info)
{
  lwroc_tsm_state* state;
  state = CONTAINER_OF(info, lwroc_tsm_state, _info);

  (void) err;

  if (attempts <= 5) return 1;
  else {
    lwroc_tsm_writer_close(info);
    lwroc_tsm_disconnect(state);
    return 0;
  }
}

static lwroc_file_writer_functions _lwroc_tsm_funcs = {
  lwroc_tsm_writer_init,
  lwroc_tsm_writer_free,
  lwroc_tsm_writer_write,
  lwroc_tsm_writer_handle_write_err,
  lwroc_tsm_writer_open,
  lwroc_tsm_writer_close,
  LWROC_FILE_WRITER_FLAGS_AUTO_UNJAM
};

void lwroc_tsm_writer_usage(void)
{
  printf ("\n");
  printf ("--ltsm-opt=<OPTIONS>:\n");
  printf ("\n");
  printf (" All fields except 'verbose' must be specified,\n"
	  " (values are provided by LTSM operators).\n");
  printf ("\n");
  printf ("  node=NODE                TSM node-name.\n");
  printf ("  pw-file=FILENAME         Path to file with node password.\n");
  printf ("  server=SERVER            Server hostname.\n");
  printf ("  fs=FS                    TSM fs path.\n");
  printf ("  verbose[=n]              LTSM verbosity, 0=off (default) .. 6=max.\n");
  printf ("\n");
}

void lwroc_tsm_writer_parse_cfg(const char *cfg,
				lwroc_tsm_state* state)
{
  const char *request;
  lwroc_parse_split_list parse_info;

  lwroc_parse_split_list_setup(&parse_info, cfg, ',');

  while ((request = lwroc_parse_split_list_next(&parse_info)) != NULL)
    {
      const char *post;

      if (LWROC_MATCH_C_ARG("help")) {
	lwroc_tsm_writer_usage();
	exit(0);
      } else if (LWROC_MATCH_C_PREFIX("node=",post)) {
	state->node = strdup_chk(post);
      } else if (LWROC_MATCH_C_PREFIX("pw-file=",post)) {
	state->pw_path = strdup_chk(post);
      } else if (LWROC_MATCH_C_PREFIX("server=",post)) {
	state->server = strdup_chk(post);
      } else if (LWROC_MATCH_C_PREFIX("fs=",post)) {
	state->fs = strdup_chk(post);
      } else if (LWROC_MATCH_C_ARG("verbose")) {
	state->verbose = 1;
      } else if (LWROC_MATCH_C_PREFIX("verbose=",post)) {
	state->verbose = (int) lwroc_parse_hex(post, "verbose");
      } else {
	LWROC_BADCFG_FMT("Unrecognised TSM option: %s", request);
      }
    }
}

lwroc_file_writer_info* lwroc_tsm_writer_do_alloc(void)
{
  lwroc_tsm_state* state;

  state = malloc(sizeof(lwroc_tsm_state));
  if (!state)
    LWROC_FATAL("Memory allocation failure (TSM writer internal struct).");

  memset(state, 0, sizeof(lwroc_tsm_state));

  lwroc_tsm_writer_parse_cfg(_config._ltsm_opt, state);

  state->_info._funcs = &_lwroc_tsm_funcs;
  return &state->_info;
}

#endif/* HAS_TSMAPI */

lwroc_file_writer_info* lwroc_tsm_writer_alloc(void)
{
  if (!_config._ltsm_opt)
    return NULL;

#if HAS_TSMAPI
  return lwroc_tsm_writer_do_alloc();
#else
  LWROC_BADCFG("Support for LTSM not compiled in.");
  return NULL;
#endif
}

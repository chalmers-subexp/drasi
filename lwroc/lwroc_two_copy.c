/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2020  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_message.h"
#include "lwroc_two_copy.h"
#include "lwroc_optimise.h"

#include <string.h>

void lwroc_two_copy_init(lwroc_two_copy *tc,
			 void *copy01, size_t size,
			 void *src_init)
{
  tc->_size = size;
  tc->_copy[0] = copy01;
  tc->_copy[1] = ((char *) copy01) + size;

  tc->_copy_seq_done = 0;
  tc->_copy_seq[1][0] = -1;
  tc->_copy_seq[1][1] = -2;
  /* We will place a copy of data on startup in first copy. */
  tc->_copy_seq[0][0] = tc->_copy_seq[0][1] = 0;

  /* This is all done suring initialisation, so no memory sync
   * statements needed.
   */

  memcpy (tc->_copy[0], src_init, tc->_size);
}

void lwroc_two_copy_update(lwroc_two_copy *tc, void *src)
{
  int copy;

  /* Block is now updated.  Decide which copy we place it in.
   */

  copy = (++tc->_copy_seq_done) & 1;

  /* Update the marker that we are now updating this copy.
   * (marker [0] does not match marker [1] during the copy.
   */

  tc->_copy_seq[copy][0] = tc->_copy_seq_done;

  SFENCE;

  memcpy (tc->_copy[copy], src, tc->_size);

  SFENCE;

  /* Update is done, let marker[1] match. */

  tc->_copy_seq[copy][1] = tc->_copy_seq_done;
}

int lwroc_two_copy_fetch(lwroc_two_copy *tc, void *dest)
{
  int attempt;

  for (attempt = 0; ; attempt++)
    {
      int copy0_good;
      int copy1_good;
      int copy;
      int value_before;

      /*
      printf ("i %d  %d %d %d %d\n", attempt,
	      tc->_copy_seq[0][0],
	      tc->_copy_seq[0][1],
	      tc->_copy_seq[1][0],
	      tc->_copy_seq[1][1]);
      */

      if (attempt >= 2)
	return 0; /* At most two attempts.  Failure is extremely
		   * unlikely.
		   */

      copy0_good = (tc->_copy_seq[0][0] ==
		    tc->_copy_seq[0][1]);
      copy1_good = (tc->_copy_seq[1][0] ==
		    tc->_copy_seq[1][1]);

      if (copy0_good && copy1_good)
	{
	  int diff = tc->_copy_seq[0][0] - tc->_copy_seq[1][0];

	  if (diff < 0) /* copy1 is newer */
	    copy0_good = 0;
	}
      if (!copy0_good && !copy1_good)
	continue; /* Strange... we somehow got interrupted
		   * in between initial checking above.  Highly
		   * unlikely, but can happen.  Try again.
		   */

      copy = !copy0_good;

      /* Note the reverse order: we check [1] first, while the
       * writer writes [0] first.
       */
      value_before = tc->_copy_seq[copy][1];

      MFENCE;

      memcpy (dest, tc->_copy[copy], tc->_size);

      MFENCE;

      if (value_before == tc->_copy_seq[copy][0])
	break; /* Good! */

      /* Data was changed while we were copying, try again... */
    }
  return 1;
}


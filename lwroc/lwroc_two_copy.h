/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2020  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_TWO_COPY_H__
#define __LWROC_TWO_COPY_H__

/* This holds a copy of data, updated by one thread.  Other thread
 * will copy from here.  Idea is that if the sequence number does not
 * change during the copy, the structure is internally consistent.
 */

typedef struct lwroc_two_copy_t
{
  void    *_copy[2];
  size_t   _size;
  int      volatile _copy_seq[2][2];
  int      _copy_seq_done;
} lwroc_two_copy;

void lwroc_two_copy_init(lwroc_two_copy *tc,
			 void *copy01, size_t size,
			 void *src_init);

void lwroc_two_copy_update(lwroc_two_copy *tc, void *src);

int lwroc_two_copy_fetch(lwroc_two_copy *tc, void *dest);

#endif/*__LWROC_TWO_COPY_H__*/

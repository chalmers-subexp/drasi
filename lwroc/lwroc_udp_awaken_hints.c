/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2023  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_udp_awaken_hints.h"
#include "lwroc_net_incoming.h"
#include "lwroc_main_iface.h"
#include "lwroc_message.h"
#include "lwroc_net_io_util.h"
#include "lwroc_hostname_util.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include "../dtc_arch/acc_def/myinttypes.h"

char        *_lwroc_udp_awaken_hints_filename = NULL;

volatile int _lwroc_udp_awaken_hints_write = 0;

/* Plan:
 *
 * The log recorder and monitor lwrocmon program does not have a fixed
 * port, so a newly started DAQ does not know where it is.  This leads
 * to two delays:
 *
 * - DAQ startup, since it waits for a log recorder connection.
 *
 * - Delays in monitoring after DAQ start.
 *
 * The delays are typically a few seconds at quick restarts, but up to
 * 20 s when the retry back-off has gotten further.  This is irritating.
 *
 * To somewhat work around this:
 *
 * The monitor program holds an UDP port open where it listens for
 * reconnection hints.  When a hint is received, it tries reconnection
 * immediately.
 *
 * During operation, the DAQ programs stores away the UDP port and a
 * random identifier for each monitor that connects in a file, whose name
 * is based on the user-id and port number where the DAQ is running.
 *
 * At DAQ startup, all hints are read and UDP packets are sent.
 *
 * Note I: the hints must not be sent before the local server port has
 * been created, as otherwise the connection will again fail, and
 * retry will be maximally late.
 *
 * Note II: it only works when the monitor program has not been
 * restarted.  (It always uses a random UDP port, and has a new random
 * identifier.)
 */

#define HOSTNAME_LEN  256

#define LWROC_AWAKEN_HINTS_NUM  32

/* This structure is stored on file.  Items which are malformed will
 * only lead us to send some garbage UDP hint packets.
 */
typedef struct lwroc_awaken_hint_t
{
  uint32_t _ipv4_addr;
  uint16_t _port;
  uint16_t _used;
  uint32_t _id1, _id2;
} lwroc_awaken_hint;

lwroc_awaken_hint _lwroc_awaken_hints[LWROC_AWAKEN_HINTS_NUM];

void lwroc_udp_awaken_hints_read(void)
{
  char hostname[HOSTNAME_LEN];
  uid_t uid;
  const char *tmpdir;
  size_t sz_filename;
  int fd;

  /* In case there is no file or reading fails. */
  memset(_lwroc_awaken_hints, 0, sizeof (_lwroc_awaken_hints));

  lwroc_gethostname(hostname, sizeof (hostname));

  tmpdir = getenv("TMP");
  if (!tmpdir)
    tmpdir = getenv("TMPDIR");
  if (!tmpdir)
    tmpdir = getenv("TEMP");
  if (!tmpdir)
    tmpdir = "/tmp";

  sz_filename = strlen(tmpdir) + strlen(hostname) + 128;

  _lwroc_udp_awaken_hints_filename = malloc (sz_filename);

  if (!_lwroc_udp_awaken_hints_filename)
    {
      fprintf (stderr, "Memory allocation error (awaken hints filename).\n");
      return;
    }

  uid = getuid();

  /* The port is what will be reused when we are restarted.
   *
   * The uid is included in the filename to avoid issues with file
   * access in case another user later uses the same machine (same
   * temp dir).
   *
   * The hostname is included to avoid issues in case the temp
   * directory is shared between machines (not likely).
   *
   * A parent directory is used in order to avoid producing very long
   * paths in the temp directory itself.
   */
  snprintf (_lwroc_udp_awaken_hints_filename,
	    sz_filename,
	    "/tmp/drasi.u%d",
	    uid);

  /* We do not care about the success of this mkdir() call.  Usually
   * the directory exist.  If it does not, we will simply not be able
   * to read the file below, which is not a problem.
   */
  mkdir(_lwroc_udp_awaken_hints_filename,
	S_IRUSR|S_IWUSR|S_IXUSR|
	S_IRGRP|S_IWGRP|S_IXGRP|
	S_IROTH|S_IWOTH|S_IXOTH);

  snprintf (_lwroc_udp_awaken_hints_filename,
	    sz_filename,
	    "/tmp/drasi.u%d/drasi.hints.u%d.%s:%d",
	    uid, uid, hostname, _config._port);

  LWROC_INFO_FMT("UDP awaken hints file: %s",
		 _lwroc_udp_awaken_hints_filename);

  fd = open(_lwroc_udp_awaken_hints_filename, O_RDONLY);

  if (fd == -1)
    {
      if (errno != ENOENT)
	{
	  LWROC_PERROR("open");
	  LWROC_WARNING_FMT("Failed to open persistent awaken hints "
			    "file for reading: %s.",
			    _lwroc_udp_awaken_hints_filename);
	}
      return;
    }

  lwroc_full_read(fd,
		  _lwroc_awaken_hints, sizeof (_lwroc_awaken_hints),
		  "awaken hints", 0);

  lwroc_safe_close(fd);
}

void lwroc_udp_awaken_hints_send(void)
{
  int slot;
  int fd;

  /* Get a socket so we can send the hints. */

  fd =
    lwroc_net_io_socket_bind(-1, 0 /* udp */, "udp reconnect hint", 0);

  for (slot = 0; slot < LWROC_AWAKEN_HINTS_NUM; slot++)
    {
      lwroc_awaken_hint *hint = &_lwroc_awaken_hints[slot];

      struct sockaddr_storage dest_addr;
      socklen_t addrlen;

      uint32_t packet[4];
      int i;
      /*
      printf ("Hint: x %08x %d (%d) %08x %08x\n",
	hint->_ipv4_addr, hint->_port,
	      hint->_used,
	      hint->_id1, hint->_id2);
      */
      if (!hint->_ipv4_addr)
	continue;

      /* We only use an item 10 times.
       *
       * Note: this is only effective if the data is written to
       * file again, since we only send once on startup.
       *
       * But it guards a bit against garbage data.
       */
      if (hint->_used > 10)
	continue;

      hint->_used++;

      lwroc_set_ipv4_addr(&dest_addr, hint->_ipv4_addr);
      lwroc_set_port(&dest_addr, hint->_port);

      packet[0] = hint->_id1;
      packet[1] = hint->_id2;
      packet[2] = (uint16_t) _config._port;
      packet[3] = 0;

      for (i = 0; i < 4; i++)
	packet[i] = htonl(packet[i]);

      addrlen = sizeof (dest_addr);

      sendto(fd, &packet, sizeof (packet),
	     0,
	     (struct sockaddr *) &dest_addr, addrlen);
    }

  lwroc_safe_close(fd);
}

void lwroc_udp_awaken_hints_add(lwroc_net_incoming *client)
{
  uint32_t ipv4_addr;
  uint16_t port;
  size_t slot;

  ipv4_addr = lwroc_get_ipv4_addr(&client->_base._addr);

  /* Note: the interesting port is not the port where this connection
   * came from, but the port that it reports itself (in hi word of
   * id2).  That is the UDP port where awaken messages should be sent.
   */
  port = (uint16_t) client->_req_msg._link_ack_id2;
  /*
  printf ("Add hints: %08x %d %08x %08x\n",
	  ipv4_addr, port,
	  client->_req_msg._link_ack_id1,
	  client->_req_msg._link_ack_id2);
  */
  /* Do we have the hint already.  We do not check the last slot, as
   * that is the default one to go.  This also ensures that the
   * memmove below do not write outside the array.
   */
  for (slot = 0; slot < LWROC_AWAKEN_HINTS_NUM - 1; slot++)
    {
      lwroc_awaken_hint *hint = &_lwroc_awaken_hints[slot];

      if (hint->_ipv4_addr == ipv4_addr &&
	  hint->_port      == port)
	break;
    }

  /* j will either point to the item, or it points to one beyond the
   * last item.  We want to insert at the front.  Move everything one
   * slot down, and in case the item existed, only so it is removed.
   */

  memmove(_lwroc_awaken_hints + 1,
	  _lwroc_awaken_hints,
	  (slot) * sizeof (_lwroc_awaken_hints[0]));

  {
    lwroc_awaken_hint *hint = &_lwroc_awaken_hints[0];

    hint->_ipv4_addr = ipv4_addr;
    hint->_port      = port;
    hint->_used      = 0;
    hint->_id1       = client->_req_msg._link_ack_id1;
    hint->_id2       = client->_req_msg._link_ack_id2;
  }

  /* The write to the file is done in the slow thread, since that
   * could potentially stall, and we do not want the network thread to
   * do that.
   *
   * Set the mark that the file should be written.
   */

  SFENCE;
  _lwroc_udp_awaken_hints_write = 1;
}

void lwroc_udp_awaken_hints_write(void)
{
  int fd;

  /* Remove the mark before we write.  That way, if another change is
   * taking place while we write, the mark will be set again, and
   * another write will happen.
   *
   * Since the file contents may be broken, we do not need to guard
   * against the contents being changed mid-write.
   */
  _lwroc_udp_awaken_hints_write = 0;
  MFENCE;

  fd = open(_lwroc_udp_awaken_hints_filename,
	    O_WRONLY | O_CREAT | O_TRUNC,
	    S_IRUSR|S_IWUSR|
	    S_IRGRP|S_IWGRP|
	    S_IROTH|S_IWOTH);

  if (fd == -1)
    {
      LWROC_PERROR("open");
      LWROC_ERROR_FMT("Failed to open persistent awaken hints "
		      "file '%s' for writing.",
		      _lwroc_udp_awaken_hints_filename);
      return;
    }

  lwroc_full_write(fd,
		   _lwroc_awaken_hints, sizeof (_lwroc_awaken_hints),
		   "awaken hints", 0);

  lwroc_safe_close(fd);
}

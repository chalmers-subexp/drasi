/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2021  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_crc32.h"

#include "lwroc_crc32.c"

#include <stdio.h>
#include <string.h>

#include "../dtc_arch/acc_def/myinttypes.h"
#include "../dtc_arch/acc_def/myprizd.h"
#include "../dtc_arch/acc_def/time_include.h"

void test(const char *buf, size_t ni, size_t n, uint32_t check)
{
  size_t i, j;

  if (ni == 0)
    ni = n;

  for (i = 0; i < ni; i++)
    for (j = i; j < n; j++)
      {
	uint32_t crc;

	lwroc_crc32_init(&crc);

	lwroc_crc32_calc(buf,     i,     &crc);
	lwroc_crc32_calc(buf + i, j - i, &crc);
	lwroc_crc32_calc(buf + j, n - j, &crc);

	lwroc_crc32_final(&crc);

	if (crc != check)
	  {
	    fprintf (stderr,
		     "Failure: %" MYPRIzd " %" MYPRIzd " %" MYPRIzd ", "
		    "%08" PRIx32 " != %08" PRIx32".\n",
		    i, j, n, crc, check);
	    exit(1);
	  }
      }
}

int main(int argc, char *argv[])
{
  size_t i;

  const char *buf1 = "This is a test string.\n";
  const char *buf2 = "This is a another test.\n";
  const char *buf3 = "123456789";
  char buf4[1567];
  char *buf = NULL;

  (void) argc;
  (void) argv;

  for (i = 0; i < 1567; i++)
    buf4[i] = (char) i;

  lwroc_crc32_table_init();

  for (_lwroc_crc32_chunk = 1;
       _lwroc_crc32_chunk <= 16;
       _lwroc_crc32_chunk *= 2)
    {
      test(buf1, 0, strlen(buf1), 0x5eec2fcf);
      test(buf2, 0, strlen(buf2), 0x922a5fa8);
      test(buf3, 0, strlen(buf3), 0xcbf43926);
      test(buf4, 50,  567,  0x449640c3);
      test(buf4, 32,  1567, 0x28b7900d);

      /* fwrite(buf4, 1, sizeof(buf4), stdout); */

      if (argc == 2 && strcmp(argv[1],"--speed") == 0)
	{
	  struct timeval a, b;
	  double elapsed;
	  size_t tot = 0;
	  uint32_t crc = 0;

#define TEST_CRC32_LARGEBUF_SIZE  256*1024*1024

	  if (!buf)
	    {
	      buf = (char *) malloc(TEST_CRC32_LARGEBUF_SIZE);

	      if (!buf)
		{
		  fprintf (stderr,
			   "Failed to allocate %d bytes for speed test.\n",
			   TEST_CRC32_LARGEBUF_SIZE);
		  exit(1);
		}
	    }

	  for (i = 0; i < TEST_CRC32_LARGEBUF_SIZE; i++)
	    buf[i] = (char) rand();

	  gettimeofday(&a, NULL);
	  for (i = 0; i < 2; i++)
	    {
	      lwroc_crc32_calc(buf, TEST_CRC32_LARGEBUF_SIZE, &crc);
	      tot += TEST_CRC32_LARGEBUF_SIZE;
	    }
	  gettimeofday(&b, NULL);

	  elapsed = (double) (b.tv_sec - a.tv_sec) +
	    0.000001 * (double) (b.tv_usec - a.tv_usec);

	  printf ("Loop-chunk %d: "
		  "%" MYPRIzd " , %.6f s, %.2f MB/s (%08" PRIx32")\n",
		  _lwroc_crc32_chunk,
		  tot, elapsed, ((double) tot) / elapsed * 1.e-6, crc);
	}
    }

  free(buf);

  return 0;
}

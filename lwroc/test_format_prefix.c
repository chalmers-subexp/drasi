/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2020  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_format_prefix.h"

#include "lwroc_format_prefix.c"

void test(int prev_prefix)
{
  char buf[64];
  double val, x, y;
  int width;

  lwroc_format_diff_info info;

  printf ("%25s ", "");
  for (width = 3; width < 10; width++)
    printf (" %*.*s", width, width,"............");
  printf ("\n");

  for (x = 0.01; x < 1.e20; x *= 10)
    {
      double yy[] = { 1, 1.001, 1.01, 1.1, 1.2, 1.3,
		      2., 3., 5., 8., 9., 9.9, 9.99 };
      size_t yi;

      for (yi = 0; yi < sizeof (yy) / sizeof (yy[0]); yi++)
	{
	  y = yy[yi];
	  val = x * y;

	  printf ("%25.3f ", val);

	  for (width = 3; width < 10; width++)
	    {
	      info._prefix_age = 0;
	      info._prefix = (uint8_t) prev_prefix;

	      lwroc_format_prefix(buf, sizeof (buf),
				  val, width,
				  &info);

	      printf (" %*s", width, buf);
	    }

	  printf ("\n");
	}
    }

  printf ("\n");
}

int main(int argc, char *argv[])
{
  (void) argc;
  (void) argv;

  /*
  {
    char buf[64];
    double val = 99900;
    int width = 5;

    lwroc_format_diff_info info;

    info._prefix_age = 0;
    info._prefix = (uint8_t) 9;

    lwroc_format_prefix(buf, sizeof (buf),
			val, width,
			&info);

    printf (" %*s\n", width, buf);
  }
  */

  test(0);
  test(9);

  return 0;
}

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2018  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

/* Program to test the timestamp tracking routines. */

/* For include only the headers needed for the tracking routines. */

#include "../dtc_arch/acc_def/mystdint.h"
#include "../dtc_arch/acc_def/myinttypes.h"
#include "../dtc_arch/acc_def/time_include.h"
#include "../dtc_arch/acc_def/mynan.h"
#include <math.h>
#include <stdlib.h>
#include <string.h>

#include "../trigbussim/message_internal.c"
#define LWROC_TRACK_TIMESTAMP_LOG_FMT      LWROC_INFO_FMT
#define LWROC_TRACK_TIMESTAMP_CCLOG_FMT    LWROC_CCINFO_FMT
#define LWROC_TRACK_TIMESTAMP_INFO_FMT     LWROC_INFO_FMT
#define LWROC_TRACK_TIMESTAMP_ERROR_FMT    LWROC_ERROR_FMT
#define LWROC_TRACK_TIMESTAMP_WARNING_FMT  LWROC_WARNING_FMT

#define LWROC_TRACK_TIMESTAMP_LOG_NEW_FIT  1

#include "lwroc_track_timestamp.c"

/* Then include what is needed for the test program itself. */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>



void crazy_outliers(uint64_t *x, uint64_t *y)
{
  int i, j;
  int64_t x_diff, y_diff;
  double d_x_diff, d_y_diff;
  double l2_x_diff, l2_y_diff;
  double xgeom[16];
  double ygeom[16];
  double xtotgeom = 0, ytotgeom = 0;
  double yscaled[16];

  for (i = 0; i < 16; i++)
    xgeom[i] = ygeom[i] = 0;

  for (i = 1; i < 16; i++)
    for (j = 0; j < i; j++)
      {
	x_diff = ((int64_t) x[i]) - ((int64_t) x[j]);
	y_diff = ((int64_t) y[i]) - ((int64_t) y[j]);

	d_x_diff = (double) x_diff;
	d_y_diff = (double) y_diff;

	l2_x_diff = log(d_x_diff * d_x_diff + 1);
	l2_y_diff = log(d_y_diff * d_y_diff + 1);

	xgeom[i] += l2_x_diff;
	xgeom[j] += l2_x_diff;
	xtotgeom += l2_x_diff;

	ygeom[i] += l2_y_diff;
	ygeom[j] += l2_y_diff;
	ytotgeom += l2_y_diff;

	if (i == 14 || j == 14)
	  printf ("%2d %2d  %17.2f  %17.2f  %7.2f  %7.2f\n",
		  i, j, d_x_diff, d_y_diff, l2_x_diff, l2_y_diff);
      }

  for (i = 0; i < 16; i++)
    {
      yscaled[i] = (ygeom[i] - xgeom[i]) - (ytotgeom - xtotgeom) * 2 / (16);

      xgeom[i] /= (16-1);
      ygeom[i] /= (16-1);
      yscaled[i] /= (16-1);

      printf("%2d  %12" PRIu64 "  %12" PRIu64 "  %7.2f %7.2f  %7.2f  %7.2f  %7.2f\n",
	     i, x[i], y[i], xgeom[i], ygeom[i], yscaled[i],
	     (ygeom[i] - xgeom[i]), (ytotgeom - xtotgeom));
    }

  printf ("                                %7.2f %7.2f\n",
	  xtotgeom / (16 * (16-1) / 2), ytotgeom / (16 * (16-1) / 2));
  printf ("                                %7.2f %7.2f\n",
	  xtotgeom, ytotgeom);


}

void test_outliers(void)
{
  uint64_t x[16];
  uint64_t y[16];
  int i;

  for (i = 0; i < 16; i++)
    x[i] = (uint64_t) i * 1000000000l;
  for (i = 0; i < 16; i++)
    y[i] = (uint64_t) i * 1000000000l;

  crazy_outliers(x, y);

  y[3] += UINT64_C(1000000000);
  crazy_outliers(x, y);

  y[3] += UINT64_C(10000000000);
  crazy_outliers(x, y);

  y[3] += UINT64_C(100000000000);
  crazy_outliers(x, y);


  for (i = 0; i < 16; i++)
    x[i] = (uint64_t) i * UINT64_C(1000000000);
  for (i = 0; i < 16; i++)
    y[i] = (uint64_t) i * UINT64_C(10000000);

  crazy_outliers(x, y);

  y[3] += UINT64_C(10000000);
  crazy_outliers(x, y);

  y[3] += UINT64_C(100000000);
  crazy_outliers(x, y);

  y[3] += UINT64_C(1000000000);
  crazy_outliers(x, y);

  y[5] += UINT64_C(1000000000);
  crazy_outliers(x, y);

  y[14] += UINT64_C(1000000000);
  crazy_outliers(x, y);

  y[3]  += UINT64_C(10000000000);
  y[5]  += UINT64_C(10000000000);
  y[13] += UINT64_C(10000000000);
  crazy_outliers(x, y);



}




uint64_t get_stamp_from_time(struct timeval *t0)
{
  struct timeval t, t_diff;
  uint64_t stamp;
  static int cnt = 0; /* to avoid getting the same stamp */

  gettimeofday(&t, NULL);

  timersub(&t, t0, &t_diff);

  t_diff.tv_sec += 10;

  stamp = 1000000 * (uint64_t) t_diff.tv_sec + (uint64_t) t_diff.tv_usec;

  /* Emulate a timestamp that ticks as 25 MHz */
  stamp *= 25;
  stamp += (uint64_t) (cnt++);

  /*
    printf ("%d: %" PRIu64 "\n", i, stamp);
  */

  return stamp;
}

int main(int argc, char *argv[])
{
  uint64_t stamp;
  int i;
  int retval;

  struct timeval t0;

  (void) argc;
  (void) argv;

  /* test_outliers(); */

  lwroc_init_timestamp_track();

  gettimeofday(&t0, NULL);

  for (i = 0; i < 300; i++)
    {
      usleep(9000);

      stamp = get_stamp_from_time(&t0);

      if (i == 10 || i == 110 || i == 210 || i == 215 || i == 216)
	stamp += 25000000;
      if (i == 11)
	stamp += 50000000;
      if (i == 12 || i == 112)
	stamp += 75000000;
      if (i == 20 || i == 120 || i == 220)
	stamp -= 25000000;
      if (i == 21)
	stamp -= 50000000;
      if (i == 22)
	stamp -= 75000000;

      retval = lwroc_report_event_timestamp(stamp, 0);

      printf ("%d ", retval);

      if (retval & LWROC_TRACK_TIMEST_RET_REQ_FRESH)
	{
	  stamp = get_stamp_from_time(&t0);
	  retval = lwroc_report_event_timestamp(stamp,
						LWROC_TRACK_TIMESTAMP_FRESH);

	  printf ("[%d] ", retval);
	}
    }

  printf ("\n");

  return 0;
}

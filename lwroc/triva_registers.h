/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __TRIVA_REGISTERS_H__
#define __TRIVA_REGISTERS_H__

#include "../dtc_arch/acc_def/myinttypes.h"
#include "../dtc_arch/acc_def/mystdint.h"

/********************************************************************/

/* TODO: put in common place?  Copied from
 * trloii/trloctrl/trlolib/trlo_trimi_test.c .  And from somewhere else
 * to there.
 *
 * Fix!  (common location)
 */

typedef struct lwroc_triva_reg_layout_t
{
  /* For a normal TRIVA: */

  /*    0 0x0000 */ uint32_t status;
  /*    1 0x0004 */ uint32_t control;
  /*    2 0x0008 */ uint32_t fcatime;
  /*    3 0x000c */ uint32_t ctime;

  /* These are only for a TRIMI: */

  /*             */ uint32_t dummy_0x0010[0x001c];
  /*   32 0x0080 */ uint32_t link_status;
  /*   33 0x0084 */ uint32_t link_control;
  /*   34 0x0088 */ uint32_t link_sendmsg;
  /*   35 0x008c */ uint32_t link_speed;
  /*   36 0x0090 */ uint32_t link_fast_dttime;

} lwroc_triva_reg_layout;

/********************************************************************/

#define TRIVA_STATUS_FC_PULSE      0x00000010  /* W */
#define TRIVA_STATUS_DT_CLEAR      0x00000020  /* W (R: TDT) */
#define TRIVA_STATUS_IRQ_CLEAR     0x00001000  /* W */
#define TRIVA_STATUS_DI_CLEAR      0x00002000  /* W (R: DI)  */
#define TRIVA_STATUS_EV_IRQ_CLEAR  0x00008000  /* W (R: EON) */

#define TRIVA_STATUS_TRIG_MASK     0x0000000f  /* R */
#define TRIVA_STATUS_TDT           0x00000020  /* R (W: DT_CLEAR) */
#define TRIVA_STATUS_MISMATCH      0x00000040  /* R */
#define TRIVA_STATUS_EC_MASK       0x00001f00  /* R */
#define TRIVA_STATUS_EC_SHIFT      8
#define TRIVA_STATUS_DI            0x00002000  /* R (W: DI_CLEAR)  */
#define TRIVA_STATUS_EON           0x00008000  /* R (W: EV_IRQ_CLEAR)*/

/********************************************************************/

#define TRIVA_CONTROL_IRQ_ENABLE   0x00000001  /* W/R */
#define TRIVA_CONTROL_IRQ_DISABLE  0x00000008  /* W   */

#define TRIVA_CONTROL_GO           0x00000002  /* W/R */
#define TRIVA_CONTROL_HALT         0x00000010  /* W   */

#define TRIVA_CONTROL_MASTER       0x00000004  /* W/R */
#define TRIVA_CONTROL_SLAVE        0x00000020  /* W   */

#define TRIVA_CONTROL_RESET        0x00000040  /* W   */

#define TRIVA_CONTROL_BUS_ENABLE   0x00000800  /* W/R */
#define TRIVA_CONTROL_BUS_DISABLE  0x00001000  /* W   */

/********************************************************************/

#define TRIMI_LINK_STATUS_MSCNT    0x0000c000  /* R   */
#define TRIMI_LINK_STATUS_MSCNT_SHIFT      14

/********************************************************************/

#endif/*__TRIVA_REGISTERS_H__*/

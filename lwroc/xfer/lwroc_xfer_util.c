/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Bastian Loeher  <b.loeher@gsi.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "xfer/lwroc_xfer_util.h"
#include "ebye/lwroc_ebye_util.h"
#include "gdf/lwroc_gdf_util.h"
#include "lwroc_message.h"
#include "lwroc_net_trans.h"
#include "lwroc_parse_util.h"

#include <assert.h>
#include <string.h>

#include "../dtc_arch/acc_def/myinttypes.h"
#include "../dtc_arch/acc_def/myprizd.h"

/* Collecting data for one xfer buffer/block at time from the circular
 * pipe.
 *
 * This mode is used to do pass-through of the EBYE XFER protocol
 * buffers, without caring about their contents.  They are stored
 * together with their headers fully in the circular pipe.  Including
 * padding!
 */

#define LBC (&(lbc->_gdf))

int lwroc_xfer_buffer_chunks_collect(/*lwroc_xfer_buffer_chunks *lbc,*/
				     lwroc_gdf_buffer_chunks *lbc_gdf,
				     lwroc_pipe_buffer_consumer *pipe_buf,
				     const lwroc_thread_block *thread_block,
				     void *v_sticky_store,
				     void *v_sticky_store_drvk,
				     int partial_buffer, int for_skipping)
{
  lwroc_xfer_buffer_chunks *lbc = (lwroc_xfer_buffer_chunks *) lbc_gdf;
  int buffer_full = 0;
  int flush_buffer = 0;
  size_t avail = 0;
  char *ptr;

  (void) v_sticky_store;
  (void) v_sticky_store_drvk;

 /* check_buffer_avail: */
  for ( ; ; )
    {
      /* If there is no data, next time to be woken up is when there
       * is additionally what is missing to get a complete buffer.
       */
      /* We always reserve space enough to allow the record header (24
       * bytes) be replaced by an xfer header (32 bytes).
       */
      size_t hysteresis = LBC->buffer_size;
      size_t waste_sz;

      avail = lwroc_pipe_buffer_avail_read(pipe_buf,
					   thread_block,
					   &ptr, &waste_sz, LBC->collect_used,
					   hysteresis);

      if (!avail)
	{
	  if (waste_sz)
	    {
	      /* We are always at offset 0, since we only take one block
	       * at a time.
	       */
	      assert (LBC->collect_used == 0);

	      lwroc_pipe_buffer_waste_read(pipe_buf, waste_sz);
	      continue; /* Try again. */
	    }
	}
      break;
    }

  if (avail)
    {
      ebye_xfer_header *xfer;
      size_t s;

      /* We always get exactly one buffer of data.  So get the header
       * of what is available, and take that amount of data.
       */
      if (avail < sizeof (ebye_xfer_header))
	LWROC_BUG_FMT("Incomplete XFER entry (block header) in data buffer "
		      "(%" MYPRIzd " bytes available < %" MYPRIzd ").",
		      avail, sizeof (ebye_xfer_header));

      xfer = (ebye_xfer_header *) ptr;

      /* As some kind of (internal) sanity check, verify the magic words. */
      if (!(xfer->_id1 == htonl(EBYE_XFER_HEADER_ID1) &&
	    xfer->_id2 == htonl(EBYE_XFER_HEADER_ID2)))
	{
	  /* Bug, since it was checked by input thread. */
	  LWROC_BUG_FMT("Bad XFER id1:2 "
			"(got %08x:%08x, expected %08x:%08x).",
			ntohl(xfer->_id1), ntohl(xfer->_id2),
			EBYE_XFER_HEADER_ID1, EBYE_XFER_HEADER_ID2);
	  return 0;
	}

      /* The actual block size. */
      s = ntohl(xfer->_block_length);

      if (avail < s)
	LWROC_BUG_FMT("Incomplete XFER entry (one block) in data buffer "
		      "(%" MYPRIzd " bytes available < %" MYPRIzd ").",
		      avail, s);

      /* Data is available here, set pointer.
       *
       * Always no pointer should be set when we come here, since we
       * only take one block at a time.
       */
      assert (LBC->chunks[LBC->collect_chunk].ptr == NULL);

      LBC->chunks[LBC->collect_chunk].ptr = ptr;

      if (s > LBC->buffer_size)
	{
	  LWROC_FATAL_FMT("Block longer (%" MYPRIzd " bytes) than "
			  "output xfer buffer (%" PRIu32 " bytes).",
			  s,
			  LBC->buffer_size);
	}
      buffer_full = 1;

      LBC->collect_used += s;

      LBC->collect_events++;
      LBC->chunks[LBC->collect_chunk].size += s;
    }

  if (!LBC->collect_used)
    return 0;

  if (for_skipping)
    {
      LBC->fully_used = LBC->collect_used;
      return 1;
    }

  if (!flush_buffer && !partial_buffer && !buffer_full)
    return 0;

  /* Ok, so we have found ourselves some data to write. */

  lwroc_xfer_buffer_chunks_prepare_write(lbc);

  return 1;
}

void lwroc_xfer_buffer_chunks_prepare_write(lwroc_xfer_buffer_chunks *lbc)
{
  uint32_t write_size;
  size_t data_use =
    LBC->chunks[LWROC_GDF_BUF_CHUNK_DATA_1].size +
    LBC->chunks[LWROC_GDF_BUF_CHUNK_DATA_2].size;
  size_t total_use;

  /* We will write the entire buffer/block. */
  total_use = LBC->collect_used;
  write_size = (uint32_t) total_use;

  if (LBC->flags & LWROC_GDF_FLAGS_FILE)
    {
      ebye_xfer_header *xfer;

      xfer = (ebye_xfer_header *)
	LBC->chunks[LWROC_GDF_BUF_CHUNK_DATA_1].ptr;

      /* Fill out the EBYE buffer header. */

      data_use = ntohl(xfer->_data_length);

      lwroc_ebye_buffer_chunks_record(lbc,
				      &lbc->fixed.record,
				      (uint32_t) data_use);

      LBC->chunks[LWROC_GDF_BUF_CHUNK_BUFHE].ptr  =
	&lbc->fixed.record;
      LBC->chunks[LWROC_GDF_BUF_CHUNK_BUFHE].size =
	sizeof (lbc->fixed.record);

      /* The actual payload data written does not include the
       * XFER header which is at the beginning.
       */

      LBC->chunks[LWROC_GDF_BUF_CHUNK_DATA_1].ptr  +=
	sizeof (ebye_xfer_header);
      LBC->chunks[LWROC_GDF_BUF_CHUNK_DATA_1].size -=
	sizeof (ebye_xfer_header);
      /* Note: total_use includes both the header, and the (kept) data. */
      total_use -=
	sizeof (ebye_xfer_header) - sizeof (ebye_record_header);
    }
  else
    {
      /* Sending with XFER header, i.e. what we already have. */

      LBC->chunks[LWROC_GDF_BUF_CHUNK_BUFHE].ptr  = NULL;
      LBC->chunks[LWROC_GDF_BUF_CHUNK_BUFHE].size = 0;
    }
  LBC->chunks[LWROC_GDF_BUF_CHUNK_FRAG_EV_HEADER].ptr  = NULL;
  LBC->chunks[LWROC_GDF_BUF_CHUNK_FRAG_EV_HEADER].size = 0;
  /* LWROC_GDF_BUF_CHUNK_DATA_1 and LWROC_GDF_BUF_CHUNK_DATA_2
   * already filled.
   */
  LBC->chunks[LWROC_GDF_BUF_CHUNK_ZERO_PAD].ptr  = _lwroc_zero_block;
  LBC->chunks[LWROC_GDF_BUF_CHUNK_ZERO_PAD].size =
    write_size - total_use;

  /*
  LWROC_INFO_FMT("data_use: %zd total_use: %zd write_size: %d pad: %zd\n",
		 data_use, total_use, write_size,
		 LBC->chunks[LWROC_GDF_BUF_CHUNK_ZERO_PAD].size);
  */

  LBC->fully_used = LBC->collect_used;
  LBC->done_events = LBC->collect_events;

  /*There is data to write! */

  LBC->chunk = LWROC_GDF_BUF_CHUNK_BUFHE;
}

/* */

/* Note: for most operations, the ebye_util functions are used. */

lwroc_gdf_format_functions _lwroc_xfer_format_functions =
{
  LWROC_DATA_TRANSPORT_FORMAT_XFER, "XFER",
  lwroc_ebye_buffer_chunks_alloc,
  lwroc_ebye_buffer_chunks_init,
  lwroc_ebye_buffer_chunks_get_fixed_raw,
  lwroc_xfer_buffer_chunks_collect,
  lwroc_ebye_buffer_chunks_file_header,
  lwroc_ebye_buffer_chunks_empty,
  LMD_EBYE_DEFAULT_SIZE,
  LMD_EBYE_DEFAULT_SIZE,
  1 /* runno-digits */, 1 /* fileno-digits */, 0 /* first-fileno-in-run */
};

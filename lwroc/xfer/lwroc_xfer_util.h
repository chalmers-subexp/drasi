/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_XFER_UTIL_H__
#define __LWROC_XFER_UTIL_H__

#include "gdf/lwroc_gdf_util.h"

#include "ebye/lwroc_ebye_event.h"
#include "ebye/lwroc_ebye_util.h"

extern lwroc_gdf_format_functions _lwroc_xfer_format_functions;

/* Use the same structure as ebye_util.  We use several functions from
 * there.
 */

typedef lwroc_ebye_buffer_chunks lwroc_xfer_buffer_chunks;

int lwroc_xfer_buffer_chunks_collect(/*lwroc_xfer_buffer_chunks *lbc,*/
				     lwroc_gdf_buffer_chunks *lbc_gdf,
				     lwroc_pipe_buffer_consumer *pipe_buf,
				     const lwroc_thread_block *thread_block,
				     void *sticky_store,
				     void *sticky_store_drvk,
				     int partial_buffer, int for_skipping);

void lwroc_xfer_buffer_chunks_prepare_write(lwroc_xfer_buffer_chunks *lbc);

/*****************************************************************************/

#endif/*__LWROC_XFER_UTIL_H__*/

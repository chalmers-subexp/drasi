/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_ctrl_client.h"
#include "lwroc_ctrl_token.h"
/* TODO: remove the following include.  Likely should move the
 * affected constants to some common file.
 */
#include "lwroc_net_proto.h"

#include <unistd.h>
#include <errno.h>
#include <string.h>
#include "../dtc_arch/acc_def/netinet_in_include.h"
#include <arpa/inet.h>
#include <netdb.h>
#include "../dtc_arch/acc_def/socket_include.h"
#include "../dtc_arch/acc_def/socket_types.h"
#include "../dtc_arch/acc_def/select_include.h"

#include <stdio.h>

/* For NetBSD, the error EPROTO does not exist, so use something else. */
#ifndef EPROTO
#define EPROTO EBADMSG
#endif

/* gcc 4.3 and later cannot figure out that code in glibc bits/byteswap.h
 * does _not_ get hit by 'conversion to short int from int may alter its
 * value', so we have a local copy:
 * looks like at least 4.6 copes, so disabling again
 */
#ifdef __GNUC_PREREQ
# if __GNUC_PREREQ (4, 3) && !__GNUC_PREREQ (4, 6)
#  undef bswap_16
#  define bswap_16(x)				\
  (unsigned short int) ((((x) >> 8) & 0x00ff) | \
			(((x) << 8) & 0xff00))
#  ifdef ntohs
#   undef ntohs
#   define ntohs(x)     bswap_16(x)
#  endif
#  ifdef htons
#   undef htons
#   define htons(x)     bswap_16(x)
#  endif
# endif
#endif

#define LWROC_CTRL_CLIENT_MIN_BUFFER_SIZE  64

struct lwroc_ctrl_client
{
  int _fd;

  char  *_buf;
  size_t _buf_alloc;
  size_t _buf_used;
  size_t _buf_filled;

  lwroc_ctrl_token *_tokens;

  /* After having had a protocol error, we cannot continue.
   * Message writing/reading is out of sync with the server.
   */
  int _protocol_error;

  const char *_last_error;
};

struct lwroc_ctrl_client *lwroc_ctrl_create_client(void)
{
  struct lwroc_ctrl_client *client;

  client = (struct lwroc_ctrl_client *)
    malloc (sizeof (struct lwroc_ctrl_client));

  if (!client)
    {
      errno = ENOMEM;
      return NULL;
    }

  client->_fd = -1;
  client->_buf = NULL;
  client->_buf_alloc = 0;
  client->_buf_used = 0;
  client->_buf_filled = 0;

  client->_tokens = NULL;

  client->_protocol_error = 0;

  client->_last_error = NULL;

  return client;
}

/* For internal use only. */

static int lwroc_ctrl_full_write(struct lwroc_ctrl_client *client,
				 void *buffer, size_t size)
{
  size_t sent;

  sent = 0;

  while (sent < size)
    {
      fd_set writefds;
      int nfd;
      struct timeval time_left;
      int ret;
      size_t left;
      ssize_t n;

      FD_ZERO(&writefds);
      FD_SET(client->_fd,&writefds);
      nfd = client->_fd;

      time_left.tv_sec = 5;
      time_left.tv_usec = 0;

      ret = select(nfd+1,NULL,&writefds,NULL,&time_left);

      if (ret == -1)
	{
	  if (errno == EINTR)
	    continue;
	  return 0;
	}

      if (ret == 0) /* Can only happen on timeout. */
	{
	  client->_last_error = "Timeout during write.";
	  errno = ETIMEDOUT;
	  return 0;
	}

      left = size - sent;

      n = write(client->_fd, ((char *) buffer) + sent, left);

      if (n == -1)
	{
	  if (errno == EINTR)
	    continue;
	  return 0;
	}
      if (n == 0)
	{
	  client->_last_error = "Failed to write.";
	  errno = EPROTO;
	  return 0;
	}

      sent += (size_t) n;
    }

  return 1;
}

static int lwroc_ctrl_full_read(struct lwroc_ctrl_client *client,
				void *buffer, size_t size)
{
  size_t rcvd;

  rcvd = 0;

  while (rcvd < size)
    {
      fd_set readfds;
      int nfd;
      struct timeval time_left;
      int ret;
      size_t left;
      ssize_t n;

      FD_ZERO(&readfds);
      FD_SET(client->_fd,&readfds);
      nfd = client->_fd;

      time_left.tv_sec = 5;
      time_left.tv_usec = 0;

      ret = select(nfd+1,&readfds,NULL,NULL,&time_left);

      if (ret == -1)
	{
	  if (errno == EINTR)
	    continue;
	  return 0;
	}

      if (ret == 0) /* Can only happen on timeout. */
	{
	  client->_last_error = "Timeout during read.";
	  errno = ETIMEDOUT;
	  return 0;
	}

      left = size - rcvd;

      n = read(client->_fd, ((char *) buffer) + rcvd, left);

      if (n == -1)
	{
	  if (errno == EINTR)
	    continue;
	  return 0;
	}
      if (n == 0)
	{
	  client->_last_error = "Failed to read.";
	  errno = EPROTO;
	  /* printf ("Got short: %zd\n", rcvd); */
	  return 0;
	}

      rcvd += (size_t) n;
    }

  /* printf ("Got: %zd\n", rcvd); */

  return 1;
}

#if 0
static int lwroc_ctrl_send_magic(struct lwroc_ctrl_client *client)
{
  size_t sent;
  uint32_t magic = htonl(~EXTERNAL_WRITER_MAGIC);

  /* Send a 32-bit magic, to distinguish us from a possible http
   * client.
   */

  sent = 0;

  while (sent < sizeof(magic))
    {
      fd_set writefds;
      int nfd;
      struct timeval timeout;
      int ret;
      size_t left;
      ssize_t n;

      FD_ZERO(&writefds);
      FD_SET(client->_fd,&writefds);
      nfd = client->_fd;

      timeout.tv_sec = 2;
      timeout.tv_usec = 0;

      ret = select(nfd+1,NULL,&writefds,NULL,&timeout);

      if (ret == -1)
	{
	  if (errno == EINTR)
	    continue;
	  return 0;
	}

      if (ret == 0) /* Can only happen on timeout. */
	{
	  client->_last_error = "Timeout during write.";
	  errno = ETIMEDOUT;
	  return 0;
	}

      left = sizeof(magic) - sent;

      n = write(client->_fd,((char*) &magic)+sent,left);

      if (n == -1)
	{
	  if (errno == EINTR)
	    continue;
	  return 0;
	}
      if (n == 0)
	{
	  client->_last_error = "Failed to write.";
	  errno = EPROTO;
	  return 0;
	}

      sent += (size_t) n;
    }

  return 1;
}
#endif

/* The following constants depend on / are in sync with the
 * definitions in lwroc_net_proto.h.  They are here as copies as we
 * want clients to be able to depend solely on this file and its header,
 * and not on the full build environment of lwroc.
 */

#define LWROC_PORTMAP_MSG_SERPROTO_MAGIC1  0x8a2492c5u
#define LWROC_PORTMAP_MSG_SERPROTO_MAGIC2  0x117c02aeu

#define LWROC_REQUEST_MSG_SERPROTO_MAGIC1  0x65675b89u
#define LWROC_REQUEST_MSG_SERPROTO_MAGIC2  0x76d39ca3u

#define LWROC_CONTROL_TOKEN_SERPROTO_MAGIC1  0xbd2d5104u
#define LWROC_CONTROL_TOKEN_SERPROTO_MAGIC2  0xa47f4139u

#define LWROC_CONTROL_REQUEST_SERPROTO_MAGIC1  0x629442f0u
#define LWROC_CONTROL_REQUEST_SERPROTO_MAGIC2  0xe17bd09du

#define LWROC_CONTROL_RESPONSE_SERPROTO_MAGIC1  0x91c6e15du
#define LWROC_CONTROL_RESPONSE_SERPROTO_MAGIC2  0x376dadaeu

#define HEADER_MSG_OFF_SIZE                     0
#define HEADER_MSG_OFF_MAGIC1                   1

#define PORTMAP_MSG_OFF_PORT                    2
#define PORTMAP_MSG_OFF_CHKSUM                  3

#define CONN_REQUEST_MSG_OFF_REQUEST            2
#define CONN_REQUEST_MSG_OFF_DUMMY_PORT         3
#define CONN_REQUEST_MSG_OFF_DUMMY_ID1          4
#define CONN_REQUEST_MSG_OFF_DUMMY_ID2          5
#define CONN_REQUEST_MSG_OFF_CHKSUM             6

#define CTRL_TOKEN_MSG_OFF_STATUS               2
#define CTRL_TOKEN_MSG_OFF_OP                   3
#define CTRL_TOKEN_MSG_OFF_TOKEN                4
#define CTRL_TOKEN_MSG_OFF_TOKEN_CHALLENGE_RESP 5
#define CTRL_TOKEN_MSG_OFF_CHKSUM               6

#define CTRL_REQUEST_MSG_OFF_TAG                2
#define CTRL_REQUEST_MSG_OFF_OP                 3
#define CTRL_REQUEST_MSG_OFF_VALUE              4
#define CTRL_REQUEST_MSG_OFF_REQ_DATA_SIZE      5
#define CTRL_REQUEST_MSG_OFF_RESP_DATA_MAXSIZE  6
#define CTRL_REQUEST_MSG_OFF_CHKSUM             7

#define CTRL_RESPONSE_MSG_OFF_STATUS            2
#define CTRL_RESPONSE_MSG_OFF_TAG               3
#define CTRL_RESPONSE_MSG_OFF_OP                4
#define CTRL_RESPONSE_MSG_OFF_VALUE             5
#define CTRL_RESPONSE_MSG_OFF_RESP_DATA_SIZE    6
#define CTRL_RESPONSE_MSG_OFF_ORIGIN            7
#define CTRL_RESPONSE_MSG_OFF_ERR_MSG           8
#define CTRL_RESPONSE_MSG_OFF_CHKSUM            (8+LWROC_CTRL_ERR_MSG_SIZE/4)

void lwroc_ctrl_set_checksum(uint32_t *msg,
			     size_t off_chksum,
			     uint32_t magic)
{
  size_t i;
  uint32_t chk;

  chk = ntohl(msg[0]);

  for (i = 2; i < off_chksum; i++)
    chk += ntohl(msg[i]);

  msg[off_chksum] = htonl(magic - chk);
}


int lwroc_ctrl_check_checksum(uint32_t *msg,
			      size_t off_chksum,
			      uint32_t magic)
{
  size_t i;
  uint32_t chk;
  uint32_t got;
  uint32_t expect;

  chk = ntohl(msg[0]);

  for (i = 2; i < off_chksum; i++)
    chk += ntohl(msg[i]);

  got = ntohl(msg[off_chksum]);

  expect = magic - chk;

  return expect == got;
}

int lwroc_ctrl_token_send_recv(struct lwroc_ctrl_client *client,
			       uint32_t status_send,
			       uint32_t *status_recv,
			       uint32_t status_expect1,
			       uint32_t status_expect2,
			       uint32_t op,
			       uint32_t token,
			       uint32_t *challenge,
			       uint32_t response)
{
  uint32_t ctrl_token_msg[7]; /* size, magic1, status, op, token, ch_resp,
			       * chksum
			       */
  int ret;

  ctrl_token_msg[HEADER_MSG_OFF_SIZE] = htonl(sizeof (ctrl_token_msg));
  ctrl_token_msg[HEADER_MSG_OFF_MAGIC1] =
    htonl(LWROC_CONTROL_TOKEN_SERPROTO_MAGIC1);
  ctrl_token_msg[CTRL_TOKEN_MSG_OFF_STATUS] =
    htonl(status_send);
  ctrl_token_msg[CTRL_TOKEN_MSG_OFF_OP] = htonl(op);
  ctrl_token_msg[CTRL_TOKEN_MSG_OFF_TOKEN] = htonl(token);
  ctrl_token_msg[CTRL_TOKEN_MSG_OFF_TOKEN_CHALLENGE_RESP] =
    htonl(response);
  lwroc_ctrl_set_checksum(ctrl_token_msg, CTRL_TOKEN_MSG_OFF_CHKSUM,
			  LWROC_CONTROL_TOKEN_SERPROTO_MAGIC2);
  /*
  printf ("Send status 0x%08x, op 0x%08x, token 0x%08x, response 0x%08x.\n",
	  status_send, op, token, response);
  */
  ret = lwroc_ctrl_full_write(client,
			      (char *) ctrl_token_msg,
			      sizeof (ctrl_token_msg));

  if (!ret)
    return 0;

  ret = lwroc_ctrl_full_read(client,
			     (char *) ctrl_token_msg,
			     sizeof (ctrl_token_msg));

  if (!ret)
    return 0;

  if (ctrl_token_msg[HEADER_MSG_OFF_SIZE] !=
      htonl(sizeof (ctrl_token_msg)))
    {
      client->_last_error = "Bad token response message size.";
      errno = EPROTO;
      return 0;
    }

  if (ctrl_token_msg[HEADER_MSG_OFF_MAGIC1] !=
      htonl(LWROC_CONTROL_TOKEN_SERPROTO_MAGIC1))
    {
      client->_last_error = "Bad token response message magic.";
      errno = EPROTO;
      return 0;
    }

  if (!lwroc_ctrl_check_checksum(ctrl_token_msg, CTRL_TOKEN_MSG_OFF_CHKSUM,
				 LWROC_CONTROL_TOKEN_SERPROTO_MAGIC2))
    {
      client->_last_error = "Bad token response message checksum.";
      errno = EPROTO;
      return 0;
    }
  /*
  printf ("Got status 0x%08x, op 0x%08x, token 0x%08x, challenge 0x%08x.\n",
	  ntohl(ctrl_token_msg[CTRL_TOKEN_MSG_OFF_STATUS]),
	  ntohl(ctrl_token_msg[CTRL_TOKEN_MSG_OFF_OP]),
	  ntohl(ctrl_token_msg[CTRL_TOKEN_MSG_OFF_TOKEN]),
	  ntohl(ctrl_token_msg[CTRL_TOKEN_MSG_OFF_TOKEN_CHALLENGE_RESP]));
  */
  if (ntohl(ctrl_token_msg[CTRL_TOKEN_MSG_OFF_OP]) != op)
    {
      client->_last_error = "Bad token response op.";
      errno = EPROTO;
      return 0;
    }

  if (ntohl(ctrl_token_msg[CTRL_TOKEN_MSG_OFF_TOKEN]) != token)
    {
      client->_last_error = "Bad token response token.";
      errno = EPROTO;
      return 0;
    }

  *status_recv =
    ntohl(ctrl_token_msg[CTRL_TOKEN_MSG_OFF_STATUS]);
  *challenge =
    ntohl(ctrl_token_msg[CTRL_TOKEN_MSG_OFF_TOKEN_CHALLENGE_RESP]);

  if (*status_recv != status_expect1 &&
      *status_recv != status_expect2)
    {
      client->_last_error = "Unexpected token response status.";
      errno = EPROTO;
      return 0;
    }

  return 1;
}

int lwroc_ctrl_token_validation(struct lwroc_ctrl_client *client,
				uint32_t op)
{
  int ret;
  lwroc_ctrl_token *token = client->_tokens;

  /* We in turn try all of our TOKN.  While server responds UNKN, we
   * must try new ones.  When it responds CHLG, we shall write a
   * response.  If .OK., then we are ready to go.  If .BAD, we have
   * to try further tokens.
   *
   * If we are to perform multiple operations, at next round, try same
   * token again first.
   */

  if (!token)
    {
      /* We have no token to negotiate with. */

      client->_last_error = "No token to offer server.";
      errno = EACCES;
      return 0;
    }

  for ( ; token; token = token->_next)
    {
      uint32_t status;
      uint32_t challenge;
      uint32_t response;

      ret = lwroc_ctrl_token_send_recv(client,
				       LWROC_CONTROL_TOKEN_STAT_TOKEN,
				       &status,
				       LWROC_CONTROL_TOKEN_STAT_CHALLENGE,
				       LWROC_CONTROL_TOKEN_STAT_UNKNOWN,
				       op, token->_token,
				       &challenge,
				       0);

      if (!ret)
	return 0;

      if (status != LWROC_CONTROL_TOKEN_STAT_CHALLENGE)
	{
	  /* Try next token, if we have one. */
	  continue;
	}

      /* Figure out the response. */
      response = lwroc_ctrl_token_response(token, challenge);

      lwroc_ctrl_token_send_recv(client,
				 LWROC_CONTROL_TOKEN_STAT_RESPONSE,
				 &status,
				 LWROC_CONTROL_TOKEN_STAT_OK,
				 LWROC_CONTROL_TOKEN_STAT_BAD,
				 op, token->_token,
				 &challenge /* dummy */,
				 response);

      if (!ret)
	return 0;

      if (status == LWROC_CONTROL_TOKEN_STAT_OK)
	{
	  return 1;
	}
    }

  /* We have no token which satisfies the server. */

  client->_last_error = "No available token accepted by server.";
  errno = EACCES;
  return 0;
}

static int lwroc_ctrl_send_conn_request(struct lwroc_ctrl_client *client)
{
  uint32_t conn_request_msg[7]; /* size, magic1, request,
			    * [dummy:port, dummy: id1, id2],
			    * chksum
			    */

  conn_request_msg[HEADER_MSG_OFF_SIZE]   = htonl(sizeof (conn_request_msg));
  conn_request_msg[HEADER_MSG_OFF_MAGIC1] =
    htonl(LWROC_REQUEST_MSG_SERPROTO_MAGIC1);
  conn_request_msg[CONN_REQUEST_MSG_OFF_REQUEST]    =
    htonl(LWROC_REQUEST_CONTROL);
  conn_request_msg[CONN_REQUEST_MSG_OFF_DUMMY_PORT] = htonl(0);
  conn_request_msg[CONN_REQUEST_MSG_OFF_DUMMY_ID1]  = htonl(0);
  conn_request_msg[CONN_REQUEST_MSG_OFF_DUMMY_ID2]  = htonl(0);
  lwroc_ctrl_set_checksum(conn_request_msg, CONN_REQUEST_MSG_OFF_CHKSUM,
			  LWROC_REQUEST_MSG_SERPROTO_MAGIC2);

  /* Send the connection type request.
   */

  return lwroc_ctrl_full_write(client,
			       (char *) conn_request_msg,
			       sizeof (conn_request_msg));
}

static int lwroc_ctrl_errno_from_status(struct lwroc_ctrl_client *client,
					uint32_t status,
					uint32_t expect_status)
{
  int user = -1; /* not user */

  if (status == expect_status)
    {
      return 0;
    }

  switch (status)
    {
    case LWROC_CONTROL_STATUS_TMP_BUSY:
      client->_last_error = "Control target (temporarily) busy.";
      errno = EBUSY;
      break;
    case LWROC_CONTROL_STATUS_TOO_BIG:
      client->_last_error = "Control message too large.";
      errno = E2BIG;
      break;
    case LWROC_CONTROL_STATUS_TOKEN_NONE:
      client->_last_error = "No token negotiated.";
      errno = EACCES;
      break;
    case LWROC_CONTROL_STATUS_TOKEN_WRONG_OP:
      client->_last_error = "(Prev.) negotiated token for wrong operation.";
      errno = EACCES;
      break;
    case LWROC_CONTROL_STATUS_NO_HANDLER:
      client->_last_error = "Control operation has no handler.";
      errno = ENOTSUP;
      break;
    case LWROC_CONTROL_STATUS_UNKNOWN:
      client->_last_error = "Control status unknown.";
      errno = EFAULT;
      break;

    case LWROC_CONTROL_STATUS_INVALID:
      client->_last_error = "Control request invalid.";
      errno = EINVAL;    user = -2;
      break;
    case LWROC_CONTROL_STATUS_DENIED:
      client->_last_error = "Control request denied.";
      errno = EACCES;    user = -2;
      break;
    case LWROC_CONTROL_STATUS_FAILED:
      client->_last_error = "Control request failed.";
      errno = EFAULT;    user = -2;
      break;
    case LWROC_CONTROL_STATUS_ALREADY:
      client->_last_error = "Control request is a no-op, already such.";
      errno = EALREADY;  user = -2;
      break;
    case LWROC_CONTROL_STATUS_SHUTDOWN:
      client->_last_error = "Shutdown in progress.";
      errno = ESHUTDOWN;  user = -2;
      break;
    case LWROC_CONTROL_STATUS_RESP_TOO_LARGE:
      client->_last_error = "Control request gives a too large response.";
      errno = ENOMEM;    user = -2;
      break;

    case LWROC_CONTROL_STATUS_PERFORMER_FAIL:
      client->_last_error = "Control request failed by performer.";
      errno = EIO;
      break;
    case LWROC_CONTROL_STATUS_PROTOCOL_ERROR:
      client->_last_error = "Control protocol error.";
      errno = EPROTO;
      break;
    default:
      /* Receiving anything else, including an unexpected success marker
       * is a protocol error.
       */
      client->_last_error = "Control operation returned unknown status.";
      errno = EPROTO;
      break;
    }
  return user;
}

static int lwroc_ctrl_read_response(struct lwroc_ctrl_client *client,
				    uint32_t expect_status,
				    uint32_t expect_tag,
				    uint32_t expect_op,
				    uint32_t *ret_status,
				    uint32_t *ret_value,
				    uint32_t *response_data_size,
				    char *err_msg, size_t sz_err_msg)
{
  uint32_t ctrl_response_msg[9+LWROC_CTRL_ERR_MSG_SIZE/4]; /* size, magic1,
					* status, tag, op, value, sz,
					* origin, msg[256 bytes],
					* chksum
					*/
  uint32_t status;
  uint32_t data_size;
  int ret;

  ret = lwroc_ctrl_full_read(client,
			     (char *) ctrl_response_msg,
			     sizeof (ctrl_response_msg));

  if (!ret)
    return 0;
  /*
  printf ("%08x %08x %08x\n",
	 (uint32_t) status_msg[HEADER_MSG_OFF_SIZE],
	 (uint32_t) htonl(sizeof (status_msg)),
	 (uint32_t) sizeof (status_msg));
  */
  if (ctrl_response_msg[HEADER_MSG_OFF_SIZE] !=
      htonl(sizeof (ctrl_response_msg)))
    {
      client->_last_error = "Bad response message size.";
      errno = EPROTO;
      return 0;
    }

  if (ctrl_response_msg[HEADER_MSG_OFF_MAGIC1] !=
      htonl(LWROC_CONTROL_RESPONSE_SERPROTO_MAGIC1))
    {
      client->_last_error = "Bad response message magic.";
      errno = EPROTO;
      return 0;
    }

  if (!lwroc_ctrl_check_checksum(ctrl_response_msg,
				 CTRL_RESPONSE_MSG_OFF_CHKSUM,
				 LWROC_CONTROL_RESPONSE_SERPROTO_MAGIC2))
    {
      client->_last_error = "Bad token response message checksum.";
      errno = EPROTO;
      return 0;
    }

  /* If the checksum worked, we'll try to copy the error message, if
   * it makes any sense.
   */
  if (err_msg)
    {
      size_t msg_len;

      msg_len = sz_err_msg;

      if (msg_len > LWROC_CTRL_ERR_MSG_SIZE)
	msg_len = LWROC_CTRL_ERR_MSG_SIZE;

      memcpy(err_msg, &ctrl_response_msg[CTRL_RESPONSE_MSG_OFF_ERR_MSG],
	     msg_len);

      err_msg[msg_len-1] = 0;
    }

  if (ret_status)
    {
      /* Only if we want the status back (i.e. a real response), the
       * tags and op values must match.
       */

      if (ntohl(ctrl_response_msg[CTRL_RESPONSE_MSG_OFF_TAG]) != expect_tag)
	{
	  client->_last_error = "Bad response tag.";
	  errno = EPROTO;
	  return 0;
	}

      if (ntohl(ctrl_response_msg[CTRL_RESPONSE_MSG_OFF_OP]) != expect_op)
	{
	  client->_last_error = "Bad response op.";
	  errno = EPROTO;
	  return 0;
	}
    }

  data_size = ntohl(ctrl_response_msg[CTRL_RESPONSE_MSG_OFF_RESP_DATA_SIZE]);

  if (data_size &&
      response_data_size == NULL)
    {
      client->_last_error = "Unexpected response data size non-zero.";
      return 0;
    }

  if (response_data_size)
    *response_data_size = data_size;

  status = ntohl(ctrl_response_msg[CTRL_RESPONSE_MSG_OFF_STATUS]);

  if (expect_status &&
      lwroc_ctrl_errno_from_status(client,
				   status,
				   expect_status) != 0)
    return 0;

  if (ret_status)
    *ret_status = status;

  if (ret_value)
    *ret_value = ntohl(ctrl_response_msg[CTRL_RESPONSE_MSG_OFF_VALUE]);

  return 1;
}


struct lwroc_ctrl_client *lwroc_ctrl_connect(const char *server)
{
  struct lwroc_ctrl_client *client;

  if (!(client = lwroc_ctrl_create_client()))
    return NULL; /* errno already set */

  /* Get us a buffer for reading. */

  client->_buf = (char *) malloc (LWROC_CTRL_CLIENT_MIN_BUFFER_SIZE);

  if (!client->_buf)
    {
      errno = ENOMEM;
      goto errno_return_NULL;
    }

  lwroc_ctrl_token_readall(1, &client->_tokens);

  client->_buf_alloc = LWROC_CTRL_CLIENT_MIN_BUFFER_SIZE;

    {
      int rc;
      struct sockaddr_in serv_addr;
      struct hostent *h;
      char *hostname, *colon;
      int port_parse = LWROC_NET_DEFAULT_PORT;
      unsigned short port;
      /* struct external_writer_portmap_msg portmap_msg; */
      uint32_t portmap_msg[4]; /* size, magic1, port, chksum */
      uint32_t port32;

      /* If there is a colon in the hostname, interpret what follows
       * as a port number, overriding the default port.
       */
      hostname = strdup(server);

      if (!hostname)
	{
	  errno = ENOMEM;
	  goto errno_return_NULL;
	}

      colon = strchr(hostname,':');

      if (colon)
	{
	  *colon = 0; /* cut the hostname */

	  if (*(colon+1) == '+')
	    port_parse += lwroc_parse_hex_u16(colon+2, "port offset");
	  else
	    port_parse = lwroc_parse_hex_u16(colon+1, "port number");
	}

      port = (unsigned short) port_parse;

      /* Get server IP address. */
      h = gethostbyname(hostname);
      free(hostname);

      if(h == NULL)
	{
	  /* EHOSTDOWN is not really correct, but the best I could find. */
	  errno = EHOSTDOWN;
	  goto errno_return_NULL;
	}

      /*
	INFO("Server '%s' known... (IP : %s).", h->h_name,
	inet_ntoa(*(struct in_addr *)h->h_addr_list[0]));
      */

      /* Create the socket. */
      client->_fd =
	socket(PF_INET,SOCK_STREAM,IPPROTO_TCP);
      if (client->_fd == -1)
	goto errno_return_NULL;

      /* Bind to the port. */
      serv_addr.sin_family = (sa_family_t) h->h_addrtype;
      memcpy((char *) &serv_addr.sin_addr.s_addr,
	     h->h_addr_list[0], (size_t) h->h_length);
      serv_addr.sin_port = htons(port);

      rc = connect(client->_fd,
		   (struct sockaddr *) &serv_addr, sizeof(serv_addr));
      if (rc == -1)
	goto errno_return_NULL;

      /* Send a 32-bit magic, to distinguish us from a possible http
       * client.
       */
#if 0 /* This would probably be a good idea... */
      if (!lwroc_ctrl_send_magic(client))
	goto errno_return_NULL;
#endif
      /* We've connected to the port server, get the address.  We
       * employ a time-out for the portmap message, to not get stuck
       * here.
       */

      lwroc_ctrl_full_read(client, portmap_msg, sizeof (portmap_msg));

      for ( ; ; )
	{
	  if (close(client->_fd) == 0)
	    break;
	  if (errno == EINTR)
	    continue;
	  goto errno_return_NULL;
	}

      client->_fd = -1;

      if (portmap_msg[HEADER_MSG_OFF_SIZE] != htonl(sizeof (portmap_msg)))
	{
	  errno = EPROTO;
	  goto errno_return_NULL;
	}

      if (portmap_msg[HEADER_MSG_OFF_MAGIC1] !=
	  htonl(LWROC_PORTMAP_MSG_SERPROTO_MAGIC1))
	{
	  errno = EPROTO;
	  goto errno_return_NULL;
	}

      if (!lwroc_ctrl_check_checksum(portmap_msg, PORTMAP_MSG_OFF_CHKSUM,
				     LWROC_PORTMAP_MSG_SERPROTO_MAGIC2))
	{
	  errno = EPROTO;
	  goto errno_return_NULL;
	}

      port32 = ntohl(portmap_msg[PORTMAP_MSG_OFF_PORT]);

      if (port32 & 0xffff0000)
	{
	  errno = EPROTO;
	  goto errno_return_NULL;
	}

      port = (unsigned short) port32;

      /* Now, connect to the data port! */

      /* Create the socket. */
      client->_fd =
	socket(PF_INET,SOCK_STREAM,IPPROTO_TCP);
      if (client->_fd == -1)
	goto errno_return_NULL;

      /* Connect to the port. */
      serv_addr.sin_family = (sa_family_t) h->h_addrtype;
      memcpy((char *) &serv_addr.sin_addr.s_addr,
	     h->h_addr_list[0], (size_t) h->h_length);
      serv_addr.sin_port = htons(port);

      rc = connect(client->_fd,
		   (struct sockaddr *) &serv_addr, sizeof(serv_addr));
      if (rc == -1)
	goto errno_return_NULL;

      /* We have successfully connected. */

      /* Send a 32-bit magic, to distinguish us from a possible http
       * client.
       */
#if 0
      if (!lwroc_ctrl_send_magic(client))
	goto errno_return_NULL;
#endif

      if (!lwroc_ctrl_send_conn_request(client))
	goto errno_return_NULL;

      if (!lwroc_ctrl_read_response(client,
				    LWROC_CONTROL_STATUS_READY,
				    0, 0,
				    NULL, NULL, NULL,
				    NULL, 0))
	goto errno_return_NULL;

      return client;
    }

  /* We never come this way. */

 errno_return_NULL:
  /* Free the allocated client buffer.
   * The data buffer also, if allocated.
   */
  {
    int errsv = errno;
    /* We do not care about close errors. */
    if (client->_fd != -1)
      close(client->_fd);
    /* Cannot fail, could possibly change errno. */
    free(client->_buf);
    free(client);
    errno = errsv;
  }
  return NULL;
}

#define LWROC_ALIGN_PAD(x,align) (((align) - (x)) & ((align)-1))

int lwroc_ctrl_request(struct lwroc_ctrl_client *client,
		       uint32_t tag,
		       uint32_t op, uint32_t value,
		       uint32_t *ret_value,
		       char *err_msg, size_t sz_err_msg,
		       void *request_data, size_t sz_request_data,
		       void *response_data, size_t sz_response_data_max,
		       size_t *sz_response_data)
{
  uint32_t ctrl_request_msg[8]; /* size, magic1, tag, op, value, sz, msz,
				 * chksum
				 */
  uint32_t response_status;
  uint32_t response_value;
  uint32_t response_data_size;
  int ret;

  /* In case we fail, fill with some garbage values. */
  if (ret_value)
    *ret_value = 0xdeadbeef;
  if (sz_response_data)
    *sz_response_data = (size_t) -1;

  if (err_msg && sz_err_msg > 0)
    err_msg[0] = 0;

  if (!client)
    {
      client->_last_error = "Client context NULL.";
      errno = EFAULT;
      return -1;
    }

  if (client->_protocol_error)
    {
      client->_last_error = "Client had earlier protocol error "
	"(communication out of sync).";
      errno = EPROTO;
      return -1;
    }

  /* These limits ensure (with margin) that we can store the size as
   * 32-bit integers.
   */

  if (sz_request_data > 0x01000000 ||
      sz_response_data_max > 0x01000000)
    {
      client->_last_error = "Request or response data insanely large.";
      errno = E2BIG;
      return -1;
    }

  /* Unless we manage to completely read and write the request, we
   * will be out of sync.  Mark the client as having had a protocol
   * error until the critical parts (last read) has been performed.
   */
  client->_protocol_error = 1;

  /* Before we can try to send the control request, we must negotiate
   * that we are in possession of a valid token.
   *
   * TODO: we do no need to revalidate token if previous request was
   * for same operation.
   */

  if (!lwroc_ctrl_token_validation(client, op))
    {
      /* Error message and errno already set. */
      return -1;
    }

  ctrl_request_msg[HEADER_MSG_OFF_SIZE] = htonl(sizeof (ctrl_request_msg));
  ctrl_request_msg[HEADER_MSG_OFF_MAGIC1] =
    htonl(LWROC_CONTROL_REQUEST_SERPROTO_MAGIC1);
  ctrl_request_msg[CTRL_REQUEST_MSG_OFF_TAG] = htonl(tag);
  ctrl_request_msg[CTRL_REQUEST_MSG_OFF_OP] = htonl(op);
  ctrl_request_msg[CTRL_REQUEST_MSG_OFF_VALUE] = htonl(value);
  ctrl_request_msg[CTRL_REQUEST_MSG_OFF_REQ_DATA_SIZE] =
    htonl((uint32_t) sz_request_data);
  ctrl_request_msg[CTRL_REQUEST_MSG_OFF_RESP_DATA_MAXSIZE] =
    htonl((uint32_t) sz_response_data_max);
  lwroc_ctrl_set_checksum(ctrl_request_msg, CTRL_REQUEST_MSG_OFF_CHKSUM,
			  LWROC_CONTROL_REQUEST_SERPROTO_MAGIC2);

  ret = lwroc_ctrl_full_write(client,
			      (char *) ctrl_request_msg,
			      sizeof (ctrl_request_msg));

  if (!ret)
    return -1;

  if (sz_request_data || sz_response_data_max)
    {
      uint32_t max_data_size = 0;

      /* We first expect a message from the server saying that it is
       * ready to take the data.  Otherwise, it will tell us the
       * maximum combined size of the request and response buffers.
       *
       * This adds a bit of latency, but is much easier than having to
       * recover from the case when too much data has been written.
       * (I.e. server has to skip data.)
       */

      if (!lwroc_ctrl_read_response(client,
				    LWROC_CONTROL_STATUS_DATA_SIZE,
				    0, 0,
				    NULL, NULL, &max_data_size,
				    err_msg, sz_err_msg))
	{
	  if (errno == E2BIG)
	    {
	      /* Not as we wanted, but also not a protocol issue. */
	      client->_protocol_error = 0;
	      *sz_response_data = max_data_size;
	    }
	  return -1;
	}

      /* Just for sanity, we check that the max_data_size is enough
       * for what we send and expect to receive.
       */

      if (max_data_size < sz_request_data + sz_response_data_max)
	{
	  client->_last_error = "Server confused (bug!) about data max size.";
	  errno = EPROTO;
	  return -1;
	}
    }

  if (sz_request_data)
    {
      char zeros[sizeof (uint64_t)] = { 0, 0, 0, 0, 0, 0, 0, 0 };
      size_t align64 = LWROC_ALIGN_PAD(sz_request_data, sizeof (uint64_t));

      ret = lwroc_ctrl_full_write(client, request_data, sz_request_data);

      if (!ret)
	return -1;

      ret = lwroc_ctrl_full_write(client, zeros, align64);

      if (!ret)
	return -1;
    }

  /* Control message has been sent; get the response... */

  /* We shall also be able to handle if we get a status message! */

  /* TODO: And, we should also be capable of reading while writing
   * above, in case the server refuses the message early on.
   */

  if (!lwroc_ctrl_read_response(client,
				0,
				tag, op,
				&response_status,
				&response_value,
				&response_data_size,
				err_msg, sz_err_msg))
    {
      return -1;
    }

  if (response_data_size > sz_response_data_max)
    {
      client->_last_error = "Too large response data size.";
      errno = EPROTO;
      return -1;
    }

  /* fprintf (stderr, "response data size: %zd\n", response_data_size); */

  if (sz_response_data)
    *sz_response_data = response_data_size;

  if (response_data_size)
    {
      char zeros[sizeof (uint64_t)];
      size_t align64 = LWROC_ALIGN_PAD(response_data_size, sizeof (uint64_t));

      /* fprintf (stderr, "response data size: %zd\n", align64); */

      ret = lwroc_ctrl_full_read(client, response_data, response_data_size);

      if (!ret)
	return -1;

      ret = lwroc_ctrl_full_read(client, zeros, align64);

      if (!ret)
	return -1;
    }

  /* All reading is done, we are in sync.  So whatever error is now
   * reported is due to failures reported from the server, not
   * self-inflicted out-of-sync status.
   */
  client->_protocol_error = 0;

  ret = lwroc_ctrl_errno_from_status(client,
				     response_status,
				     LWROC_CONTROL_STATUS_SUCCESS);

  if (ret != 0)
    return ret;

  *ret_value = response_value;

  return 0;
}


int lwroc_ctrl_close(struct lwroc_ctrl_client *client)
{
  if (!client)
    {
      client->_last_error = "Client context NULL.";
      errno = EFAULT;
      return -1;
    }

  /* printf ("close connection.\n"); */

  /* In order for the server to know that the lost connection is
   * intentional, send a close / dummy request. */

  {
    uint32_t ctrl_request_msg[8]; /* size, magic1, tag, op, value, sz, msz,
				   * chksum
				   */
    int ret;

    memset (ctrl_request_msg, 0, sizeof (ctrl_request_msg));

    ctrl_request_msg[HEADER_MSG_OFF_SIZE] = htonl(sizeof (ctrl_request_msg));
    ctrl_request_msg[HEADER_MSG_OFF_MAGIC1] =
      htonl(LWROC_CONTROL_REQUEST_SERPROTO_MAGIC1);
    ctrl_request_msg[CTRL_REQUEST_MSG_OFF_OP] = htonl(LWROC_CONTROL_OP_DONE);
    lwroc_ctrl_set_checksum(ctrl_request_msg, CTRL_REQUEST_MSG_OFF_CHKSUM,
			    LWROC_CONTROL_REQUEST_SERPROTO_MAGIC2);

    ret = lwroc_ctrl_full_write(client,
				(char *) ctrl_request_msg,
				sizeof (ctrl_request_msg));

    /* printf ("close wrote %zd, got %d\n", sizeof (ctrl_request_msg), ret); */

    /* This should not really fail, but we report errors :-) */

    if (!ret)
      return -1;
  }

  /* Only close the file handle if opened by us. */

  while (client->_fd != -1)
    {
      int errsave;

      if (close(client->_fd) == 0)
	break;

      if (errno == EINTR)
	continue;

      client->_last_error = "Failure closing socket.";
      errsave = errno;
      /* Cannot fail, could possibly change errno. */
      free(client->_buf);
      free(client);
      errno = errsave;
      return -1;
    }

  /* Cannot fail. */
  free(client->_buf);
  free(client);

  return 0;
}

const char *lwroc_ctrl_last_error(struct lwroc_ctrl_client *client)
{
  if (client == NULL)
    return NULL;

  return client->_last_error;
}
#if 0
/* Functions that 'handle' errors by printing messages to stderr.
 */

#include <stdio.h>

struct lwroc_ctrl_client *lwroc_ctrl_connect_stderr(const char *server)
{
  struct lwroc_ctrl_client *client =
    lwroc_ctrl_connect(server);

  if (client == NULL)
    {
      perror("lwroc_ctrl_connect");
      fprintf (stderr,"Failed to connect to server '%s'.\n",server);
      return NULL;
    }

  fprintf (stderr,"Connected to server '%s'.\n",server);

  return client;
}

int lwroc_ctrl_setup_stderr(struct lwroc_ctrl_client *client,
			  const void *struct_layout_info,
			  size_t size_info,
			  size_t size_buf)
{
  int ret = lwroc_ctrl_setup(client,struct_layout_info,size_info,size_buf);

  if (ret == -1)
    {
      perror("lwroc_ctrl_setup");
      fprintf (stderr,"Failed to setup connection: %s\n",
	       client->_last_error);
      /* Not more fatal than that we can disconnect. */
      return 0;
    }

  return 1;
}

int lwroc_ctrl_fetch_event_stderr(struct lwroc_ctrl_client *client,
				void *buf,size_t size)
{
  int ret = lwroc_ctrl_fetch_event(client,buf,size);

  if (ret == 0)
    {
      fprintf (stderr,"End from server.\n");
      return 0; /* Out of data. */
    }

  if (ret == -1)
    {
      perror("lwroc_ctrl_fetch_event");
      fprintf (stderr,"Failed to fetch event: %s\n",
	       client->_last_error);
      /* Not more fatal than that we can disconnect. */
      return 0;
    }

  return 1;
}

void lwroc_ctrl_close_stderr(struct lwroc_ctrl_client *client)
{
  int ret = lwroc_ctrl_close(client);

  if (ret != 0)
    {
      perror("lwroc_ctrl_close");
      fprintf (stderr,"Errors reported during disconnect: %s\n",
	       client->_last_error);
      return;
    }

  fprintf (stderr,"Disconnected from server.\n");
}
#endif

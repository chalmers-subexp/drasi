/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_CTRL_CLIENT_H__
#define __LWROC_CTRL_CLIENT_H__

#include "../dtc_arch/acc_def/mystdint.h"
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

/*************************************************************************/

struct lwroc_ctrl_client;

/* Connect to a TCP external data client.
 *
 * @server          Either a HOSTNAME or a HOSTNAME:PORT.
 *
 * Return value:
 *
 * Pointer to a context structure (use when calling other functions).
 * NULL on failure, in which case errno describes the error.
 *
 * In addition to various system (socket etc) errors, errno:
 *
 * EPROTO           Protocol error, version mismatch?
 * ENOMEM           Failure to allocate memory.
 * ETIMEDOUT        Timeout while trying to get data port number.
 *
 * EHOSTDOWN        Hostname not found (not really correct).
 */

struct lwroc_ctrl_client *lwroc_ctrl_connect(const char *server);

/*************************************************************************/

#define LWROC_CTRL_ERR_MSG_SIZE  256

/* Pack and write one one control request.
 *
 * @client          Connection context structure.
 * @tag             Identifier
 * @op              Operation to perform.
 * @value           Single value to send to performer.
 * @ret_value       Single return value from performer.
 * @err_msg         Pointer to array for error message.
 *                  (Suggested 256 bytes, LWROC_CTRL_ERR_MSG_SIZE)
 * @sz_err_msg      Size of error message array.
 * @request_data          Data array to send to performer (optional / NULL).
 * @sz_request_data       Size of data array (or 0).
 * @response_data         Data array received from performer (optional / NULL).
 * @sz_response_data_max  Maximum size of received data array.
 * @sz_response_data      Actual size of received data array.
 *
 * Return value:
 *
 *  0          success (one event written (buffered)).
 * -1          failure.  See errno.
 * -2          failure, by performer (not communication).
 *
 * In addition to various system (socket write) errors, errno:
 *
 * EINVAL           @size is wrong.
 * EBADMSG          Data offset outside structure.
 *                  Malformed message.  Bug?
 * EPROTO           Unexpected message.  Bug?
 * EFAULT           @client is NULL.
 *
 * 'errno' should be considered a second hand choice for reporting the
 * nature of an error to the user.  Rather use lwroc_ctrl_last_error().
 */

int lwroc_ctrl_request(struct lwroc_ctrl_client *client,
		       uint32_t tag,
		       uint32_t op, uint32_t value,
		       uint32_t *ret_value,
		       char *err_msg, size_t sz_err_msg,
		       void *request_data, size_t sz_request_data,
		       void *response_data, size_t sz_response_data_max,
		       size_t *sz_response_data);

/*************************************************************************/

/* Close a client connection, and free the context structure.
 *
 * @client          Connection context structure.
 *
 * Return value:
 *
 *  0  success.
 * -1  failure.  See errno.
 *
 * In addition to various system (socket close) errors, errno:
 *
 * EFAULT           @client is NULL, or not properly set up.
 */

int lwroc_ctrl_close(struct lwroc_ctrl_client *client);

/*************************************************************************/

/* Return a pointer to a (static) string with a more descriptive error
 * message.
 */

const char *lwroc_ctrl_last_error(struct lwroc_ctrl_client *client);

/*************************************************************************/

/* The following functions are the same as those above, except that
 * errors are caught and messages printed to stderr.
 *
 * Returns:
 *
 * lwroc_ctrl_connect_stderr        pointer or NULL
 * lwroc_ctrl_write_request         1 or 0
 * lwroc_ctrl_close_stderr          (void)
 */

struct lwroc_ctrl_client *lwroc_ctrl_connect_stderr(const char *server);

int lwroc_ctrl_request_stderr(struct lwroc_ctrl_client *client,
			      uint32_t tag,
			      uint32_t op, uint32_t value,
			      uint32_t *ret_value);

void lwroc_ctrl_close_stderr(struct lwroc_ctrl_client *client);

/*************************************************************************/

#ifdef __cplusplus
}
#endif

#endif/*__LWROC_CTRL_CLIENT_H__*/

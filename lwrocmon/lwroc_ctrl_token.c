/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2019  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_ctrl_token.h"

#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

/*
lwroc_ctrl_token *_lwroc_access_tokens = NULL;
int               _lwroc_num_access_tokens = 0;
*/

#define LCG_A3 2862933555777941757LL
#define LCG_C3          3037000493LL

uint32_t lwroc_ctrl_token_response(lwroc_ctrl_token *token,
				   uint32_t challenge)
{
  uint32_t response;
  uint64_t mix1;
  uint64_t mix2;

  /* It is probably also a bad idea to use the challenge twice. */

  /* But this routine is not expected/supposed to be cryptographically
   * sane.  It is just supposed to prevent mistakes.
   */

  response = token->_token;

  mix1 = challenge ^ token->_hash1;
  mix1 = mix1 * LCG_A3 + LCG_C3;

  response ^= (uint32_t) (mix1 >> 32);

  /* xorshift64star, see wikipedia */

  mix2 = challenge ^ token->_hash2;

  mix2 ^= mix2 >> 12;
  mix2 ^= mix2 << 25;
  mix2 ^= mix2 >> 27;

  mix2 = mix2 * 2685821657736338717ull;

  response ^= (uint32_t) (mix2 >> 32);

  return response | 1; /* Responses are always non-zero. */
}

int lwroc_ctrl_token_check(lwroc_ctrl_token *tokens,
			   uint32_t token,
			   uint32_t op,
			   uint32_t challenge,
			   uint32_t response)
{
  /* Loop over all tokens.
   *
   * For any token that matches the name (and is valid for the
   * requested operation), see if the response is good (if supplied
   * with a non-zero challenge).
   */

  for ( ; tokens; tokens = tokens->_next)
    {
      lwroc_ctrl_token *t = tokens;
      uint32_t good_response;

      if (t->_token != token)
	continue;

      /* TODO: Check op */
      (void) op;

      if (!challenge)
	return 1;

      good_response =
	lwroc_ctrl_token_response(t, challenge);

      if (response == good_response)
	return 1;
    }

  return 0;
}

int lwroc_ctrl_token_readall(int print_tokens,
			     lwroc_ctrl_token **tokens)
{
  struct dirent *dirent;
  DIR *dir;
  char *home;
  char *path = NULL, *file = NULL;
  struct stat statbuf;
  int ret;
  uid_t uid;
  FILE *fid;
  char line[256];
  lwroc_ctrl_token t;

  *tokens = NULL;

  /* All tokens are in $HOME/.drasi_tokens/ */

  uid = getuid();

  home = getenv("HOME");

  if (!home)
    {
      fprintf (stderr, "Could not get environment variable HOME.\n");
      goto error_return;
    }

  path = malloc (strlen(home) + 32);

  if (!path)
    {
      fprintf (stderr, "Memory allocation error.\n");
      goto error_return;
    }

  strcpy(path, home);
  strcat(path, "/.drasi_tokens");

  dir = opendir (path);

  if (dir == NULL)
    {
      perror ("opendir");
      fprintf (stderr, "Failed to open directory '%s'.\n", path);
      goto error_return;
    }

  while ((dirent = readdir(dir)) != NULL)
    {
      /* We should use openat, statat.
       * But those may not be available...
       */

      file = malloc (strlen(path) + 2 + strlen(dirent->d_name));

      if (!file)
	{
	  fprintf (stderr, "Memory allocation error.\n");
	  goto error_return;
	}

      strcpy(file, path);
      strcat(file, "/");
      strcat(file, dirent->d_name);

      ret = stat(file, &statbuf);

      if (ret != 0)
	{
	  perror("stat");
	  fprintf (stderr, "Failed to stat token file '%s'.\n", file);
	  goto error_return;
	}

      /* We only care about regular files. */

      if (S_ISREG(statbuf.st_mode))
	{
	  if (statbuf.st_uid != uid)
	    {
	      fprintf (stderr,
		       "Refusing to use token file '%s' "
		       "not owned by user.\n",
		       file);
	      goto error_return;
	    }

	  if (statbuf.st_mode & (S_IRWXO))
	    {
	      fprintf (stderr,
		       "Refusing to use token file '%s' "
		       "with access by others ('chmod go-rwx FILENAME').\n",
		       file);
	      goto error_return;
	    }

	  fid = fopen(file, "r");

	  if (fid == NULL)
	    {
	      perror("fopen");
	      fprintf (stderr, "Failed to open token file '%s'.\n", file);
	    }

	  t._token = 0;
	  t._hash1 = 0;
	  t._hash2 = 0;

	  while (!feof(fid))
	    {
	      if (fgets(line, sizeof (line), fid) != NULL)
		{
		  char *p;

		  /* printf ("got: \"%s\"\n", line); */

#define XORSHIFT(x,op1,op2,op3) do {	\
		    x ^= (x) << 13;	\
		    x ^= (x) >> 17;	\
		    x ^= (x) << 5;	\
		  } while (0)

		  for (p = line; *p; p++)
		    if (*p != '\n')
		      {
			t._token ^= (unsigned char) (*p);
			XORSHIFT(t._token,<<,>>,<<);
			t._hash1 ^= (unsigned char) (*p);
			XORSHIFT(t._hash1,>>,<<,<<);
			t._hash2 ^= (unsigned char) (*p);
			XORSHIFT(t._hash2,>>,<<,>>);
		      }
		}
	    }

	  fclose(fid);

	  if (t._token)
	    {
	      lwroc_ctrl_token *token;

	      token = malloc (sizeof (lwroc_ctrl_token));

	      if (!token)
		{
		  fprintf (stderr, "Memory allocation error.\n");
		  goto error_return;
		}

	      t._next = NULL;
	      *token = t;

	      *tokens = token;
	      tokens = &(token->_next);

	      if (print_tokens)
		{
		  /* Note! hashes are not to be dumped to log-file! */
		  printf ("Token: %08x (%08x:%08x) [%s]\n",
			  t._token,
			  t._hash1, t._hash2,
			  file);
		}
	    }
	  else
	    {
	      fprintf (stderr, "Ignoring empty token: [%s]\n",
		       file);
	    }
	}

      free (file);
      file = NULL;
    }
  closedir (dir);

  free (path);
  return 1;

 error_return:

  free (path);
  free (file);
  return 0;
}

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2019  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

/* This header is for internal use only. */

#ifndef __LWROC_CTRL_TOKEN_H__
#define __LWROC_CTRL_TOKEN_H__

#include "../dtc_arch/acc_def/mystdint.h"

typedef struct lwroc_ctrl_token_t
{
  uint32_t _token; /* Hash, as name. */

  uint32_t _hash1; /* For validation. */
  uint32_t _hash2;

  struct lwroc_ctrl_token_t *_next;

} lwroc_ctrl_token;

int lwroc_ctrl_token_readall(int print_tokens,
			     lwroc_ctrl_token **tokens);

uint32_t lwroc_ctrl_token_response(lwroc_ctrl_token *token,
				   uint32_t challenge);

int lwroc_ctrl_token_check(lwroc_ctrl_token *tokens,
			   uint32_t token,
			   uint32_t op,
			   uint32_t challenge,
			   uint32_t response);

#endif

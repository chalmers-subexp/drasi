/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROC_MON_H__
#define __LWROC_MON_H__

#include "lwrocmon_struct.h"

#include <stdio.h>

#include "../dtc_arch/acc_def/netinet_in_include.h"
#include "../dtc_arch/acc_def/socket_types.h"

/********************************************************************/

extern int _lwrocmon_source_update;
extern int _lwrocmon_block_update;

extern int _lwrocmon_print_header_ago;
extern int _lwrocmon_event_incr_nonzero;

typedef struct lwrocmon_rate_state_t
{
  lwroc_format_diff_info _events_diff_info;
  lwroc_format_diff_info _incr_diff_info;
  lwroc_format_diff_info _coll_diff_info;
  lwroc_format_diff_info _filter_events_diff_info;
  lwroc_format_diff_info _filter_incr_diff_info;
  lwroc_format_diff_info _filter_coll_diff_info;
  lwroc_format_diff_info _sent_diff_info;
  lwroc_format_diff_info _file_wr_diff_info;
  lwroc_format_diff_info _file_sz_diff_info;
} lwrocmon_rate_state;

extern lwrocmon_rate_state _lwrocmon_rate_state;

/********************************************************************/

#define MONITOR_STRUCT_PTRS(struct_base_name)				\
      struct_base_name##_block *ptr_block =				\
	(struct_base_name##_block *) item->_data[conn->_good_data];     \
      struct_base_name##_block *ptr_prev =				\
	(struct_base_name##_block *) item->_data[conn->_prev_data];	\
      struct_base_name##_block_format_diff_info *ptr_info =		\
	(struct_base_name##_block_format_diff_info *) item->_format_diff_info;

/********************************************************************/

#define MONITOR_STRUCT_FREQ_PTRS(ptr,ptr_prev)				\
  double elapsed, frequency;				                \
  elapsed = (double) ((ptr)->_time._sec - (ptr_prev)->_time._sec) +	\
    0.000000001 * ((int) (ptr)->_time._nsec - (int) (ptr_prev)->_time._nsec); \
  frequency = 1. / elapsed;

#define MONITOR_STRUCT_FREQ \
  MONITOR_STRUCT_FREQ_PTRS(ptr_block, ptr_prev)

/********************************************************************/

typedef struct lwrocmon_conn_info_t
{
  const char *type_str;
  const char *dir_str;
  const char *status_str;
  int active;

  char source_dotted[INET6_ADDRSTRLEN];
  char block_dotted[INET6_ADDRSTRLEN];

} lwrocmon_conn_info;

void lwrocmon_get_conn_info(lwroc_monitor_conn_source *conn_source,
			    lwroc_monitor_conn_block *conn_block,
			    lwrocmon_conn_info *conn_info);

/********************************************************************/

void lwrocmon_realloc_buffer(lwrocmon_buffer *buf,size_t size);

void lwrocmon_create_buffer(lwrocmon_buffer *buf,size_t size);

ssize_t lwrocmon_read_buffer(lwrocmon_buffer *buf,int fd);

ssize_t lwrocmon_write_buffer(lwrocmon_buffer *buf,int fd);

/********************************************************************/

extern FILE *_fid_messages;
extern int _printed_log_message;

void lwroc_print_message_line(time_t sec, uint32_t nsec,
			      const char *file, uint32_t line,
			      const char *level_str,
			      const char *host_port_thread,
			      const char *msg);

void lwroc_print_msg_greeting(int start);

void lwroc_print_msg_sources_info(lwrocmon_conn *conn,
				  int level_attach_detach,
				  const char *msg);

void lwroc_free_msg_source_info(lwrocmon_conn *conn);

int lwrocmon_handle_message_source(lwrocmon_conn *conn);

int lwrocmon_handle_message_item(lwrocmon_conn *conn,
				 lwroc_select_info *si);

int lwrocmon_print_message_item(lwrocmon_conn *conn);

int lwrocmon_handle_message_keepalive(lwrocmon_conn *conn,
				      lwroc_select_info *si);

void lwrocmon_print_message_status(struct timeval *now);

/********************************************************************/

void lwroc_free_mon_source_info(lwrocmon_conn *conn);

int lwrocmon_handle_monitor_source(lwrocmon_conn *conn);

int lwrocmon_handle_monitor_item(lwrocmon_conn *conn);

void lwrocmon_data_bump_good_prev(void);

/********************************************************************/

void lwrocmon_loop_rate_header(void);

void lwrocmon_loop_rate_data(int header_ago);

/********************************************************************/

void lwrocmon_detail_tree_init(void);

void lwrocmon_detail_source_update(void);

void lwrocmon_detail_block_update(void);

/********************************************************************/

void lwrocmon_tree_source_update(void);

void lwrocmon_tree_block_update(void);

/********************************************************************/

void lwrocmon_setup_udp_hint(void);

/********************************************************************/

void lwrocmon_awaken_sleeping(struct sockaddr_storage *addr,
			      lwroc_select_info *si);

void lwrocmon_add_mon_host(const char *cmd,
			   struct sockaddr_storage *serv_addr,
			   int added_why);

/********************************************************************/

typedef struct lwroc_multi_col_info_t
{
  int  _extra_cols;
  int *_col_breaks;
} lwroc_multi_col_info;

extern lwroc_multi_col_info _lwroc_multi_col;

/********************************************************************/

typedef struct lwroc_status_format_t
{
  uint32_t    status;
  const char *marker;
  const char *text;
  short       color;
} lwroc_status_format;

#define LWROC_LAST_ERROR_FORGET  1

lwroc_status_format *lwroc_get_status_format(uint32_t status);

/********************************************************************/

void lwrocmon_time_ago_str(char *str, uint64_t time_ago);

/********************************************************************/

#endif/*__LWROC_MON_H__*/

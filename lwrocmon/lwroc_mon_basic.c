/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_mon.h"

#include "lwroc_hostname_util.h"
#include "lwroc_message.h"
#include "lwroc_optimise.h"
#include "lwroc_macros.h"

#include "gen/lwroc_message_source_serializer.h"

#include "gen/lwroc_monitor_source_header_serializer.h"
#include "gen/lwroc_monitor_main_source_serializer.h"
#include "gen/lwroc_monitor_net_source_serializer.h"
#include "gen/lwroc_monitor_file_source_serializer.h"
#include "gen/lwroc_monitor_filewr_source_serializer.h"
#include "gen/lwroc_monitor_filter_source_serializer.h"
#include "gen/lwroc_monitor_serv_source_serializer.h"
#include "gen/lwroc_monitor_in_source_serializer.h"
#include "gen/lwroc_monitor_conn_source_serializer.h"
#include "gen/lwroc_monitor_analyse_source_serializer.h"
#include "gen/lwroc_monitor_analyse_block_serializer.h"
#include "gen/lwroc_monitor_dams_source_serializer.h"
#include "gen/lwroc_monitor_dams_block_serializer.h"

#include "gen/lwroc_monitor_block_header_serializer.h"
#include "gen/lwroc_monitor_main_block_serializer.h"
#include "gen/lwroc_monitor_net_block_serializer.h"
#include "gen/lwroc_monitor_file_block_serializer.h"
#include "gen/lwroc_monitor_filewr_block_serializer.h"
#include "gen/lwroc_monitor_filter_block_serializer.h"
#include "gen/lwroc_monitor_serv_block_serializer.h"
#include "gen/lwroc_monitor_in_block_serializer.h"
#include "gen/lwroc_monitor_conn_block_serializer.h"
#include "gen/lwroc_monitor_ana_ts_source_serializer.h"
#include "gen/lwroc_monitor_ana_ts_block_serializer.h"
#include "gen/lwroc_monitor_dam_source_serializer.h"
#include "gen/lwroc_monitor_dam_block_serializer.h"

#include "gen/lwroc_monitor_dt_trace_serializer.h"

#include "../dtc_arch/acc_def/has_curses.h"
#if HAS_CURSES
#define USE_CURSES 1
#endif
#include "../lu_common/colourtext.h"

#include <assert.h>
#include <arpa/inet.h>

#include "../dtc_arch/acc_def/myprizd.h"
#include "../dtc_arch/acc_def/myinttypes.h"
#include "../dtc_arch/acc_def/fall_through.h"

void lwroc_free_mon_source_info(lwrocmon_conn *conn)
{
  lwrocmon_src_info *src = conn->_mon_sources;

  conn->_mon_sources = NULL;
  conn->_mon_main_source = NULL;

  for ( ; src; )
    {
      lwrocmon_src_info *next = src->_next;

      free (src->_host_port_thread);
      free (src->_hostname);
      free (src->_thread);
      /*
	Allocated with the structure itself...
      free (src->_data);
      free (src->_data_prev);
      */
      if (src->_type_magic == LWROC_MONITOR_NET_SOURCE_SERPROTO_MAGIC1)
	{
	  uint32_t connb;
	  lwroc_monitor_net_source *net_source =
	    (lwroc_monitor_net_source *) src->_source_info;

	  for (connb = 0; connb < net_source->_conn_blocks; connb++)
	    {
	      lwroc_monitor_conn_source *conn_source =
		src->_conn_source_info + connb;
	      free (LWROC_UNCONST(char *, conn_source->_hostname));
	      free (LWROC_UNCONST(char *, conn_source->_label));
	      /*fprintf (stderr, "freed %p\n", &conn_source->_hostname);*/
	    }
	}

      free (src->_conn_source_info);
      free (src->_conn_data[0]); /* [1] allocated with [0] */
      free (src->_filewr_data[0]);
      free (src->_ana_ts_data[0]);
      free (src);

      src = next;
    }
  timerclear(&conn->_stale_expire);
}

void lwrocmon_get_conn_info(lwroc_monitor_conn_source *conn_source,
			    lwroc_monitor_conn_block *conn_block,
			    lwrocmon_conn_info *conn_info)
{
  struct sockaddr_storage look_addr;

  conn_info->type_str = "(?)";
  conn_info->dir_str = "??";
  conn_info->status_str = "??";
  conn_info->active = 0;

  lwroc_set_ipv4_addr(&look_addr,
		      conn_source->_ipv4_addr);
  lwroc_inet_ntop(&look_addr,
		  conn_info->source_dotted, sizeof(conn_info->source_dotted));

  lwroc_set_ipv4_addr(&look_addr,
		      conn_block->_ipv4_addr);
  lwroc_inet_ntop(&look_addr,
		  conn_info->block_dotted, sizeof(conn_info->block_dotted));

  switch (conn_source->_type)
    {
    case LWROC_REQUEST_MESSAGES:
      conn_info->type_str = "logs";
      break;
    case LWROC_REQUEST_MONITOR:
      conn_info->type_str = "mon";
      break;
    case LWROC_REQUEST_CONTROL:
      conn_info->type_str = "ctrl";
      break;
    case LWROC_REQUEST_DATA_TRANS:
      conn_info->type_str = "data";
      break;
    case LWROC_REQUEST_TRIVAMI_BUS:
      conn_info->type_str = "bus";
      break;
    case LWROC_REQUEST_TRIVAMI_EB:
      conn_info->type_str = "eb";
      break;
    case LWROC_REQUEST_REVERSE_LINK:
      conn_info->type_str = "revl";
      break;
    case LWROC_CONN_TYPE_TRANS:
      conn_info->type_str = "trans";
      break;
    case LWROC_CONN_TYPE_FNET:
      conn_info->type_str = "fnet";
      break;
    case LWROC_CONN_TYPE_STREAM:
      conn_info->type_str = "stream";
      break;
    case LWROC_CONN_TYPE_EBYE_PUSH:
      conn_info->type_str = "ebye-push";
      break;
    case LWROC_CONN_TYPE_EBYE_PULL:
      conn_info->type_str = "ebye-pull";
      break;
    case LWROC_CONN_TYPE_SYSTEM:
      conn_info->type_str = "system";
      break;
    case LWROC_CONN_TYPE_FILTER:
      conn_info->type_str = "filter";
      break;
    case LWROC_CONN_TYPE_FILE:
      conn_info->type_str = "file";
      break;
    }

  switch (conn_source->_direction)
    {
    case LWROC_CONN_DIR_OUTGOING:
      conn_info->dir_str = "<-";
      break;
    case LWROC_CONN_DIR_INCOMING:
      conn_info->dir_str = "->";
      break;
    }

  switch (conn_block->_status)
    {
    case LWROC_CONN_STATUS_NOT_CONNECTED:
      conn_info->status_str = "-";
      break;
    case LWROC_CONN_STATUS_CONNECTED:
      conn_info->status_str = "C:";
      conn_info->active = 1;
      break;
    }
}

int lwrocmon_handle_monitor_source(lwrocmon_conn *conn)
{
  lwroc_monitor_source_header header;
  lwroc_deserialize_error desererr;
  const char *wire = conn->_buf._buf;
  size_t left = conn->_buf._size;
  const char *end = wire + left;
  uint32_t block;
  lwrocmon_src_info **next_ptr;
  int i;

  /* Our old monitor source info is no longer useful. */
  lwroc_free_mon_source_info(conn);

  /* The items have to be in order as we unpack according to this list. */
  next_ptr = &conn->_mon_sources;

  wire =
    lwroc_monitor_source_header_deserialize_envelope(&header,
						     wire,
						     (size_t) (end - wire),
						     &desererr);

  if (wire == 0)
    {
      LWROC_ERROR_FMT("Monitor source header block malformed "
		      "(%s, 0x%x, 0x%x).\n",
		      desererr._msg, desererr._val1, desererr._val2);
      return 0;
    }

  for (block = 0; block < header._blocks; block++)
    {
      lwroc_message_source src_info;
      lwrocmon_src_info *mon_src_info = NULL;
      const uint32_t *wire_peek;
      uint32_t type_magic;
      const char *type_descr;
      size_t size_mon_src_info;
      size_t size_data;
      size_t size_source;
      size_t size_format_diff_info;
      size_t size_alloc;

      /* First comes the description of the source. */

      wire = lwroc_message_source_deserialize_inplace(&src_info,
						      wire,
						      (size_t) (end - wire),
						      &desererr);

      if (wire == NULL)
	{
	  LWROC_ERROR_FMT("Monitor source item malformed (%s, 0x%x, 0x%x).",
			  desererr._msg, desererr._val1, desererr._val2);
	  return 0;
	}

      /* We then need to peek into the buffer to find out what kind
       * of item comes next.  Identified by its magic.
       */

      if ((size_t) (end - wire) < sizeof (uint32_t) + sizeof (uint32_t))
	{
	  LWROC_ERROR_FMT("Monitor source item ended prematurely "
			  "(%" MYPRIzd ").",
			  (size_t) (end - wire));
	}

      wire_peek = (const uint32_t *) wire;
      type_magic = ntohl(wire_peek[1]);
      type_descr = NULL;

#define GET_MON_STRUCT_SIZES(struct_base_name)				\
      size_source = sizeof (struct_base_name##_source);			\
      size_data   = sizeof (struct_base_name##_block);			\
      size_format_diff_info = sizeof (struct_base_name##_block_format_diff_info);

      switch (type_magic)
	{
	case LWROC_MONITOR_NET_SOURCE_SERPROTO_MAGIC1:
	  GET_MON_STRUCT_SIZES(lwroc_monitor_net);
	  type_descr = "NET";
	  break;
	case LWROC_MONITOR_MAIN_SOURCE_SERPROTO_MAGIC1:
	  GET_MON_STRUCT_SIZES(lwroc_monitor_main);
	  type_descr = "MAIN";
	  break;
	case LWROC_MONITOR_FILE_SOURCE_SERPROTO_MAGIC1:
	  GET_MON_STRUCT_SIZES(lwroc_monitor_file);
	  type_descr = "FILE";
	  break;
	case LWROC_MONITOR_FILTER_SOURCE_SERPROTO_MAGIC1:
	  GET_MON_STRUCT_SIZES(lwroc_monitor_filter);
	  type_descr = "FILT";
	  break;
	case LWROC_MONITOR_SERV_SOURCE_SERPROTO_MAGIC1:
	  GET_MON_STRUCT_SIZES(lwroc_monitor_serv);
	  type_descr = "SERV";
	  break;
	case LWROC_MONITOR_IN_SOURCE_SERPROTO_MAGIC1:
	  GET_MON_STRUCT_SIZES(lwroc_monitor_in);
	  type_descr = "IN";
	  break;
	case LWROC_MONITOR_ANALYSE_SOURCE_SERPROTO_MAGIC1:
	  GET_MON_STRUCT_SIZES(lwroc_monitor_analyse);
	  type_descr = "ANLY";
	  break;
	case LWROC_MONITOR_DAMS_SOURCE_SERPROTO_MAGIC1:
	  GET_MON_STRUCT_SIZES(lwroc_monitor_dams);
	  type_descr = "DAMS";
	  break;
	default:
	  size_source = size_data = size_format_diff_info = 0;
	  break;
	}

      /* Make sure the structure parts are 64-bit aligned. */

      size_mon_src_info    = sizeof (lwrocmon_src_info);
      size_mon_src_info    = LWROC_ALIGN_SZ(size_mon_src_info,
					    sizeof (uint64_t));
      size_data            = LWROC_ALIGN_SZ(size_data,
					    sizeof (uint64_t));
      size_source          = LWROC_ALIGN_SZ(size_source,
					    sizeof (uint64_t));
      size_format_diff_info = LWROC_ALIGN_SZ(size_format_diff_info,
					    sizeof (uint64_t));

      /* Remember the info. */

      size_alloc = size_mon_src_info +
	size_source + LWROCMON_DATA_BLOCKS * size_data +
	size_format_diff_info;

      mon_src_info = (lwrocmon_src_info *) malloc (size_alloc);

      if (!mon_src_info)
	LWROC_FATAL("Memory allocation error.\n");

      mon_src_info->_source   = src_info._source;
      mon_src_info->_port     = src_info._port;
      mon_src_info->_hostname = strdup_chk(src_info._hostname);
      mon_src_info->_thread   = strdup_chk(src_info._thread);

      mon_src_info->_host_port_thread = (char *)
	malloc (strlen(src_info._hostname) + strlen(src_info._thread) +
		16); /* ":num/0" */

      if (!mon_src_info->_host_port_thread)
	LWROC_FATAL("Memory allocation error.\n");

      if (mon_src_info->_host_port_thread)
	{
	  if (src_info._port != LWROC_NET_DEFAULT_PORT)
	    sprintf (mon_src_info->_host_port_thread, "%s:%d/%s",
		     src_info._hostname, src_info._port, src_info._thread);
	  else
	    sprintf (mon_src_info->_host_port_thread, "%s/%s",
		     src_info._hostname, src_info._thread);
	}

      mon_src_info->_type_magic = type_magic;
      mon_src_info->_type_descr = type_descr;
      mon_src_info->_source_info =
	((char *) mon_src_info) + size_mon_src_info;
      mon_src_info->_data_size = size_data;

      if (size_data)
	{
	  mon_src_info->_data[0] =
	    ((char *) mon_src_info->_source_info) + size_source;
	  for (i = 1; i < LWROCMON_DATA_BLOCKS; i++)
	    mon_src_info->_data[i] =
	      ((char *) mon_src_info->_data[i-1]) + size_data;
	  mon_src_info->_format_diff_info =
	    ((char *) mon_src_info->_data[LWROCMON_DATA_BLOCKS-1]) + size_data;

	  assert (((char *) mon_src_info->_format_diff_info) +
		  size_format_diff_info ==
		  ((char *) mon_src_info) + size_alloc);

	  memset (mon_src_info->_data[0], 0, LWROCMON_DATA_BLOCKS * size_data);
	  memset (mon_src_info->_format_diff_info, 0, size_format_diff_info);
	}
      else
	{
	  for (i = 0; i < LWROCMON_DATA_BLOCKS; i++)
	    mon_src_info->_data[i] = NULL;
	  mon_src_info->_format_diff_info = NULL;
	}
      mon_src_info->_conn_source_info = NULL;
      for (i = 0; i < LWROCMON_DATA_BLOCKS; i++)
	{
	  mon_src_info->_conn_data[i] = NULL;
	  mon_src_info->_filewr_data[i] = NULL;
	  mon_src_info->_ana_ts_data[i] = NULL;
	}

      *next_ptr = mon_src_info;
      mon_src_info->_next = NULL;
      next_ptr = &mon_src_info->_next;

      /* printf ("SRC: %08x\n", type_magic); */

      switch (type_magic)
	{
	case LWROC_MONITOR_NET_SOURCE_SERPROTO_MAGIC1:
	  {
	    uint32_t connb;
	    lwroc_monitor_net_source *net_source =
	      (lwroc_monitor_net_source *) mon_src_info->_source_info;
	    wire = lwroc_monitor_net_source_deserialize(net_source,
							wire,
							(size_t) (end - wire),
							&desererr);
	    if (wire == NULL)
	      break;

	    mon_src_info->_conn_source_info = (lwroc_monitor_conn_source *)
	      malloc (net_source->_conn_blocks *
		      sizeof (lwroc_monitor_conn_source));
	    mon_src_info->_conn_data[0] = (lwroc_monitor_conn_block *)
	      malloc (LWROCMON_DATA_BLOCKS * net_source->_conn_blocks *
		      sizeof (lwroc_monitor_conn_block));
	    for (i = 1; i < LWROCMON_DATA_BLOCKS; i++)
	      mon_src_info->_conn_data[i] =
		mon_src_info->_conn_data[i-1] + net_source->_conn_blocks;

	    if (!mon_src_info->_conn_source_info ||
		!mon_src_info->_conn_data[0])
	      LWROC_FATAL("Memory allocation error.\n");

	    /* Make sure all string pointers are NULL, such that
	     * deserialize_strdup works.  And that our own freeing
	     * at the end also works in case the deserialisation
	     * aborts early.
	     */

	    for (connb = 0; connb < net_source->_conn_blocks; connb++)
	      {
		lwroc_monitor_conn_source *conn_source =
		  mon_src_info->_conn_source_info + connb;
		conn_source->_hostname = NULL;
		conn_source->_label = NULL;
		/*fprintf (stderr, "cleared %p\n", &conn_source->_hostname);*/
	      }

	    for (connb = 0; connb < net_source->_conn_blocks; connb++)
	      {
		lwroc_monitor_conn_source *conn_source =
		  mon_src_info->_conn_source_info + connb;
		wire =
		  lwroc_monitor_conn_source_deserialize_strdup(conn_source,
							       wire,
							       (size_t) (end -
									 wire),
							       &desererr);
		if (wire == NULL)
		  break;
	      }
	    break;
	  }
	case LWROC_MONITOR_MAIN_SOURCE_SERPROTO_MAGIC1:
	  {
	    lwroc_monitor_main_source *main_source =
	      (lwroc_monitor_main_source *) mon_src_info->_source_info;
	    wire = lwroc_monitor_main_source_deserialize(main_source,
							 wire,
							 (size_t) (end - wire),
							 &desererr);

	    /* To keep track of _mon_identX. */
	    conn->_mon_main_source = main_source;

	    break;
	  }
	case LWROC_MONITOR_FILE_SOURCE_SERPROTO_MAGIC1:
	  {
	    lwroc_monitor_file_source *file_source =
	      (lwroc_monitor_file_source *) mon_src_info->_source_info;
	    wire = lwroc_monitor_file_source_deserialize(file_source,
							 wire,
							 (size_t) (end - wire),
							 &desererr);

	    if (wire == NULL)
	      break;

	    mon_src_info->_filewr_data[0] = (lwroc_monitor_filewr_block *)
	      malloc (LWROCMON_DATA_BLOCKS * file_source->_filewr_blocks *
		      sizeof (lwroc_monitor_filewr_block));
	    for (i = 1; i < LWROCMON_DATA_BLOCKS; i++)
	      mon_src_info->_filewr_data[i] =
		mon_src_info->_filewr_data[i-1] + file_source->_filewr_blocks;

	    if (!mon_src_info->_filewr_data[0])
	      LWROC_FATAL("Memory allocation error.\n");

	    break;
	  }
	case LWROC_MONITOR_FILTER_SOURCE_SERPROTO_MAGIC1:
	  {
	    lwroc_monitor_filter_source *filter_source =
	      (lwroc_monitor_filter_source *) mon_src_info->_source_info;
	    wire = lwroc_monitor_filter_source_deserialize(filter_source,
							   wire,
							   (size_t) (end-wire),
							   &desererr);
	    break;
	  }
	case LWROC_MONITOR_SERV_SOURCE_SERPROTO_MAGIC1:
	  {
	    lwroc_monitor_serv_source *serv_source =
	      (lwroc_monitor_serv_source *) mon_src_info->_source_info;
	    wire = lwroc_monitor_serv_source_deserialize(serv_source,
							 wire,
							 (size_t) (end - wire),
							 &desererr);
	    break;
	  }
	case LWROC_MONITOR_IN_SOURCE_SERPROTO_MAGIC1:
	  {
	    lwroc_monitor_in_source *in_source =
	      (lwroc_monitor_in_source *) mon_src_info->_source_info;
	    wire = lwroc_monitor_in_source_deserialize(in_source,
						       wire,
						       (size_t) (end - wire),
						       &desererr);
	    break;
	  }
	case LWROC_MONITOR_ANALYSE_SOURCE_SERPROTO_MAGIC1:
	  {
	    lwroc_monitor_analyse_source *analyse_source =
	      (lwroc_monitor_analyse_source *) mon_src_info->_source_info;
	    wire =
	      lwroc_monitor_analyse_source_deserialize(analyse_source,
						       wire,
						       (size_t) (end - wire),
						       &desererr);

	    if (wire == NULL)
	      break;

	    mon_src_info->_ana_ts_data[0] = (lwroc_monitor_ana_ts_block *)
	      malloc (LWROCMON_DATA_BLOCKS * analyse_source->_ana_ts_blocks *
		      sizeof (lwroc_monitor_ana_ts_block));
	    for (i = 1; i < LWROCMON_DATA_BLOCKS; i++)
	      mon_src_info->_ana_ts_data[i] =
		mon_src_info->_ana_ts_data[i-1] +
		analyse_source->_ana_ts_blocks;

	    if (!mon_src_info->_ana_ts_data[0])
	      LWROC_FATAL("Memory allocation error.\n");

	    break;
	  }
	case LWROC_MONITOR_DAMS_SOURCE_SERPROTO_MAGIC1:
	  {
	    lwroc_monitor_dams_source *dams_source =
	      (lwroc_monitor_dams_source *) mon_src_info->_source_info;
	    wire =
	      lwroc_monitor_dams_source_deserialize(dams_source,
						    wire,
						    (size_t) (end - wire),
						    &desererr);

	    if (wire == NULL)
	      break;

	    mon_src_info->_dam_data[0] = (lwroc_monitor_dam_block *)
	      malloc (LWROCMON_DATA_BLOCKS * dams_source->_dam_blocks *
		      sizeof (lwroc_monitor_dam_block));
	    for (i = 1; i < LWROCMON_DATA_BLOCKS; i++)
	      mon_src_info->_dam_data[i] =
		mon_src_info->_dam_data[i-1] +
		dams_source->_dam_blocks;

	    if (!mon_src_info->_dam_data[0])
	      LWROC_FATAL("Memory allocation error.\n");

	    break;
	  }
	default:
	  {
	    uint32_t skip_size = ntohl(wire_peek[0]);

	    if ((size_t) (end - wire) < skip_size)
	      {
		LWROC_ERROR_FMT("Monitor source item info "
				"ended prematurely (%" MYPRIzd "), "
				"cannot skip.",
				(size_t) (end - wire));
	      }
	    wire += skip_size;
	    break;
	  }
	}

      if (wire == NULL)
	{
	  LWROC_ERROR_FMT("Monitor source item #%d info malformed "
			  "(%s, 0x%x, 0x%x).",
			  block,
			  desererr._msg, desererr._val1, desererr._val2);
	  return 0;
	}

    }

  if (wire != end)
    {
      LWROC_ERROR_FMT("Monitor source malformed "
		      "(%" MYPRIzd " bytes leftover).",
		      (size_t) (end - wire));
      return 0;
    }

  if (!conn->_mon_main_source)
    {
      LWROC_ERROR("Monitor source malformed, no main source info.");
      return 0;
    }

  /* TODO: initialise to -1, to have marker for none prev known... */
  conn->_good_data = 0;
  conn->_prev_data = 2;

  /* Force new header. */
  _lwrocmon_print_header_ago = -1;

  return 1;
}

const char *
lwrocmon_handle_monitor_item_normal(lwrocmon_conn *conn,
				    const char *wire, const char *end,
				    int *unpack_data)
{
  lwroc_deserialize_error desererr;

  uint32_t block;
  lwrocmon_src_info *item;

  *unpack_data = (conn->_good_data + 1) % LWROCMON_DATA_BLOCKS;
  if (*unpack_data == conn->_prev_data)
    *unpack_data = (*unpack_data + 1) % LWROCMON_DATA_BLOCKS;

  for (block = 0, item = conn->_mon_sources; item; item = item->_next, block++)
    {
      /* We cannot move the data to the prev buffers immediately, as
       * unpacking may fail (bug), or the header mismatches footer
       * (legal).
       */

      /* printf ("DEC: %08x\n", item->_type_magic); */

      switch (item->_type_magic)
	{
	case LWROC_MONITOR_NET_SOURCE_SERPROTO_MAGIC1:
	  {
	    uint32_t connb;
	    lwroc_monitor_net_source *net_source =
	      (lwroc_monitor_net_source *) item->_source_info;
	    lwroc_monitor_net_block *net_block =
	      (lwroc_monitor_net_block *) item->_data[*unpack_data];

	    wire = lwroc_monitor_net_block_deserialize(net_block,
						       wire,
						       (size_t) (end - wire),
						       &desererr);
	    if (wire == NULL)
	      break;
	    for (connb = 0; connb < net_source->_conn_blocks; connb++)
	      {
		lwroc_monitor_conn_block *conn_block =
		  item->_conn_data[*unpack_data] + connb;
		wire = lwroc_monitor_conn_block_deserialize(conn_block,
							    wire,
							    (size_t) (end -
								      wire),
							    &desererr);
		if (wire == NULL)
		  break;
	      }
	    break;
	  }
	case LWROC_MONITOR_MAIN_SOURCE_SERPROTO_MAGIC1:
	  {
	    lwroc_monitor_main_block *main_block =
	      (lwroc_monitor_main_block *) item->_data[*unpack_data];

	    wire = lwroc_monitor_main_block_deserialize(main_block,
							wire,
							(size_t) (end - wire),
							&desererr);
	    break;
	  }
	case LWROC_MONITOR_FILE_SOURCE_SERPROTO_MAGIC1:
	  {
	    uint32_t filewrb;
	    lwroc_monitor_file_source *file_source =
	      (lwroc_monitor_file_source *) item->_source_info;
	    lwroc_monitor_file_block *file_block =
	      (lwroc_monitor_file_block *) item->_data[*unpack_data];

	    wire = lwroc_monitor_file_block_deserialize(file_block,
							wire,
							(size_t) (end - wire),
							&desererr);

	    if (wire == NULL)
	      break;
	    for (filewrb = 0; filewrb < file_source->_filewr_blocks; filewrb++)
	      {
		lwroc_monitor_filewr_block *filewr_block =
		  item->_filewr_data[*unpack_data] + filewrb;
		wire = lwroc_monitor_filewr_block_deserialize(filewr_block,
							      wire,
							      (size_t) (end -
									wire),
							      &desererr);
		if (wire == NULL)
		  break;
	      }
	    break;
	  }
	case LWROC_MONITOR_FILTER_SOURCE_SERPROTO_MAGIC1:
	  {
	    lwroc_monitor_filter_block *filter_block =
	      (lwroc_monitor_filter_block *) item->_data[*unpack_data];

	    wire = lwroc_monitor_filter_block_deserialize(filter_block,
							  wire,
							  (size_t) (end - wire),
							  &desererr);
	    break;
	  }
	case LWROC_MONITOR_SERV_SOURCE_SERPROTO_MAGIC1:
	  {
	    lwroc_monitor_serv_block *serv_block =
	      (lwroc_monitor_serv_block *) item->_data[*unpack_data];

	    wire = lwroc_monitor_serv_block_deserialize(serv_block,
							wire,
							(size_t) (end - wire),
							&desererr);
	    break;
	  }
	case LWROC_MONITOR_IN_SOURCE_SERPROTO_MAGIC1:
	  {
	    lwroc_monitor_in_block *in_block =
	      (lwroc_monitor_in_block *) item->_data[*unpack_data];

	    wire = lwroc_monitor_in_block_deserialize(in_block,
						      wire,
						      (size_t) (end - wire),
						      &desererr);
	    break;
	  }
	case LWROC_MONITOR_ANALYSE_SOURCE_SERPROTO_MAGIC1:
	  {
	    uint32_t ana_tsb;
	    lwroc_monitor_analyse_source *analyse_source =
	      (lwroc_monitor_analyse_source *) item->_source_info;
	    lwroc_monitor_analyse_block *analyse_block =
	      (lwroc_monitor_analyse_block *) item->_data[*unpack_data];

	    wire =
	      lwroc_monitor_analyse_block_deserialize(analyse_block,
						      wire,
						      (size_t) (end - wire),
						      &desererr);

	    if (wire == NULL)
	      break;
	    for (ana_tsb = 0; ana_tsb < analyse_source->_ana_ts_blocks;
		 ana_tsb++)
	      {
		lwroc_monitor_ana_ts_block *ana_ts_block =
		  item->_ana_ts_data[*unpack_data] + ana_tsb;
		wire =
		  lwroc_monitor_ana_ts_block_deserialize(ana_ts_block,
							 wire,
							 (size_t) (end -
								   wire),
							 &desererr);
		if (wire == NULL)
		  break;
	      }
	    break;
	  }
	case LWROC_MONITOR_DAMS_SOURCE_SERPROTO_MAGIC1:
	  {
	    uint32_t damb;
	    lwroc_monitor_dams_source *dams_source =
	      (lwroc_monitor_dams_source *) item->_source_info;
	    lwroc_monitor_dams_block *dams_block =
	      (lwroc_monitor_dams_block *) item->_data[*unpack_data];

	    wire =
	      lwroc_monitor_dams_block_deserialize(dams_block,
						   wire,
						   (size_t) (end - wire),
						   &desererr);

	    if (wire == NULL)
	      break;
	    for (damb = 0; damb < dams_source->_dam_blocks;
		 damb++)
	      {
		lwroc_monitor_dam_block *dam_block =
		  item->_dam_data[*unpack_data] + damb;
		wire =
		  lwroc_monitor_dam_block_deserialize(dam_block,
						      wire,
						      (size_t) (end -
								wire),
						      &desererr);
		if (wire == NULL)
		  break;
	      }
	    break;
	  }
	default:
	  {
	    const uint32_t *wire_peek;
	    uint32_t skip_size;

	    if ((size_t) (end - wire) < sizeof (uint32_t) + sizeof (uint32_t))
	      {
		LWROC_ERROR_FMT("Monitor item ended prematurely "
				"(%" MYPRIzd ").",
				(size_t) (end - wire));
	      }

	    wire_peek = (const uint32_t *) wire;
	    skip_size = ntohl(wire_peek[0]);
	    if ((size_t) (end - wire) < skip_size)
	      {
		LWROC_ERROR_FMT("Monitor source item info "
				"ended prematurely (%" MYPRIzd "), "
				"cannot skip.",
				(size_t) (end - wire));
	      }
	    wire += skip_size;
	    break;
	  }
	}

      if (wire == NULL)
	{
	  LWROC_ERROR_FMT("Monitor block #%d malformed (%s, 0x%x, 0x%x).\n",
			  block,
			  desererr._msg, desererr._val1, desererr._val2);
	  return NULL;
	}
    }

  return wire;
}

void lwrocmon_handle_monitor_item_normal_good(lwrocmon_conn *conn,
					      int *unpack_data)
{
  /* conn->_prev_data = conn->_good_data; */
  conn->_good_data = *unpack_data;
}

void lwrocmon_handle_monitor_item_normal_print(lwrocmon_conn *conn)
{
  lwrocmon_src_info *item;

  if (!conn->_mon_sources)
    printf ("========================================"
	    "=======================================\n");

  for (item = conn->_mon_sources; item; item = item->_next)
    {
      const char *type_descr;

      type_descr = item->_type_descr ? item->_type_descr : "?";

      if (item == conn->_mon_sources)
	printf ("== [ %4s ] ============================"
		"=======================================\n",
		type_descr);
      else
	printf ("-- [ %4s ] ----------------------------"
		"---------------------------------------\n",
		type_descr);

      switch (item->_type_magic)
	{
	case LWROC_MONITOR_NET_SOURCE_SERPROTO_MAGIC1:
	  {
	    lwroc_monitor_net_source *ptr_source =
	      (lwroc_monitor_net_source *) item->_source_info;

	    MONITOR_STRUCT_PTRS(lwroc_monitor_net);

	    lwroc_monitor_net_block_print_diff(ptr_source,
					       ptr_block, ptr_prev,
					       ptr_info,
					       &_config._pd_config);


	    if (ptr_source->_conn_blocks)
	      {
		uint32_t connb;

		printf ("-- [ %4s ] ----------------------------"
			"---------------------------------------\n",
			"conn");

		for (connb = 0; connb < ptr_source->_conn_blocks; connb++)
		  {
		    lwroc_monitor_conn_source *conn_source =
		      item->_conn_source_info + connb;
		    lwroc_monitor_conn_block *conn_block =
		      item->_conn_data[conn->_good_data] + connb;

		    lwrocmon_conn_info conn_info;
		    char status_hollerith[5];

		    lwrocmon_get_conn_info(conn_source, conn_block,
					   &conn_info);

		    lwroc_uint32_to_hollerith(status_hollerith,
					      conn_block->_status);

		    printf ("%2s %s%-6s%s %-2s[%s%10s:%-5d%s/%10s:%-5d] %s %s\n",
			    conn_info.dir_str,
			    conn_info.active ? CT_OUT(BOLD) : CT_OUT(NORM),
			    conn_info.type_str,
			    CT_OUT(NORM),
			    conn_info.status_str,
			    conn_info.active ? CT_OUT(BOLD) : CT_OUT(NORM),
			    conn_info.block_dotted,
			    conn_block->_port,
			    CT_OUT(NORM),
			    conn_info.source_dotted,
			    conn_source->_port,
			    conn_source->_hostname ?
			    conn_source->_hostname : "",
			    conn_source->_label ?
			    conn_source->_label : "");
		    printf ("   %c%10" PRIu64 "/%10" PRIu64 ""
			    " s:%4s e:%10" PRIu64 " b:%10" PRIu64 "\n",
			    (conn_block->_buf_data_fill &
			     LWROC_CONN_DATA_FILL_FULL) ? 'F' : ' ',
			    (conn_block->_buf_data_fill &
			     ~LWROC_CONN_DATA_FILL_FULL),
			    conn_source->_buf_data_size,
			    status_hollerith,
			    conn_block->_events,
			    conn_block->_bytes);
		  }
	      }
	    break;
	  }
	case LWROC_MONITOR_MAIN_SOURCE_SERPROTO_MAGIC1:
	  {
	    lwroc_monitor_main_source *ptr_source =
	      (lwroc_monitor_main_source *) item->_source_info;
	    MONITOR_STRUCT_PTRS(lwroc_monitor_main);

	    lwroc_monitor_main_source_print_diff(ptr_source, NULL,
						 NULL, NULL);

	    lwroc_monitor_main_block_print_diff(ptr_block, ptr_prev,
						ptr_info,
						&_config._pd_config);
	    break;
	  }
	case LWROC_MONITOR_FILE_SOURCE_SERPROTO_MAGIC1:
	  {
	    lwroc_monitor_file_source *ptr_source =
	      (lwroc_monitor_file_source *) item->_source_info;

	    MONITOR_STRUCT_PTRS(lwroc_monitor_file);

	    lwroc_monitor_file_block_print_diff(ptr_block, ptr_prev,
						ptr_info,
						&_config._pd_config);

	    if (ptr_source->_filewr_blocks)
	      {
		uint32_t filewrb;

		printf ("-- [ %4s ] ----------------------------"
			"---------------------------------------\n",
			"file");

		for (filewrb = 0; filewrb < ptr_source->_filewr_blocks;
		     filewrb++)
		  {
		    lwroc_monitor_filewr_block *filewr_block =
		      item->_filewr_data[conn->_good_data] + filewrb;
		    const char *state = "-";

		    switch (filewr_block->_state)
		      {
		      case LWROC_FILEWR_STATE_CLOSED:
			state = "closed";
			break;
		      case LWROC_FILEWR_STATE_OPEN:
			state = "open";
			break;
		      case LWROC_FILEWR_STATE_JAMMED:
			state = "jammed";
			break;
		      }

		    /* TODO: this should be unified... */
		    printf ("%-6s %12" PRIu64 " %s\n",
			    state,
			    filewr_block->_bytes,
			    filewr_block->_filename);

		  }
	      }
	    break;
	  }
	case LWROC_MONITOR_FILTER_SOURCE_SERPROTO_MAGIC1:
	  {
	    MONITOR_STRUCT_PTRS(lwroc_monitor_filter);

	    lwroc_monitor_filter_block_print_diff(ptr_block, ptr_prev,
						  ptr_info,
						  &_config._pd_config);
	    break;
	  }
	case LWROC_MONITOR_SERV_SOURCE_SERPROTO_MAGIC1:
	  {
	    MONITOR_STRUCT_PTRS(lwroc_monitor_serv);

	    lwroc_monitor_serv_block_print_diff(ptr_block, ptr_prev,
						ptr_info,
						&_config._pd_config);
	    break;
	  }
	case LWROC_MONITOR_IN_SOURCE_SERPROTO_MAGIC1:
	  {
	    lwroc_monitor_in_source *ptr_source =
	      (lwroc_monitor_in_source *) item->_source_info;

	    MONITOR_STRUCT_PTRS(lwroc_monitor_in);

	    lwroc_monitor_in_block_print_diff(ptr_source,
					      ptr_block, ptr_prev,
					      ptr_info,
					      &_config._pd_config);
	    break;
	  }
	case LWROC_MONITOR_ANALYSE_SOURCE_SERPROTO_MAGIC1:
	  {
	    lwroc_monitor_analyse_source *ptr_source =
	      (lwroc_monitor_analyse_source *) item->_source_info;

	    MONITOR_STRUCT_PTRS(lwroc_monitor_analyse);

	    lwroc_monitor_analyse_block_print_diff(ptr_block, ptr_prev,
						   ptr_info,
						   &_config._pd_config);

	    if (ptr_source->_ana_ts_blocks)
	      {
		uint32_t ana_tsb;

		printf ("-- [ %4s ] ----------------------------"
			"---------------------------------------\n",
			"ants");

		for (ana_tsb = 0; ana_tsb < ptr_source->_ana_ts_blocks;
		     ana_tsb++)
		  {
		    lwroc_monitor_ana_ts_block *ana_ts_block =
		      item->_ana_ts_data[conn->_good_data] + ana_tsb;

		    if (ana_ts_block->_ts_ref_off_sig_k == (uint64_t) -2)
		      continue;

		    /* TODO: this should be unified... */
		    printf ("%4x %016" PRIx64 "  %10.1f +/- %2.1f\n",
			    ana_ts_block->_id,
			    ana_ts_block->_src_index_mask,
			    (double)
			    (int64_t) ana_ts_block->_ts_ref_offset_k  *
			    0.001,
			    (double) ana_ts_block->_ts_ref_off_sig_k *
			    0.001);

		  }
	      }
	    break;
	  }
	case LWROC_MONITOR_DAMS_SOURCE_SERPROTO_MAGIC1:
	  {
	    lwroc_monitor_dams_source *ptr_source =
	      (lwroc_monitor_dams_source *) item->_source_info;

	    MONITOR_STRUCT_PTRS(lwroc_monitor_dams);

	    lwroc_monitor_dams_block_print_diff(ptr_block, ptr_prev,
						ptr_info,
						&_config._pd_config);

	    if (ptr_source->_dam_blocks)
	      {
		uint32_t damb;

		printf ("-- [ %4s ] ----------------------------"
			"---------------------------------------\n",
			"dams");

		for (damb = 0; damb < ptr_source->_dam_blocks;
		     damb++)
		  {
		    lwroc_monitor_dam_block *dam_block =
		      item->_dam_data[conn->_good_data] + damb;

		    if (dam_block->_id == (uint32_t) -1)
		      continue;

		    printf ("%2d: %10" PRIu64 " %10" PRIu64 "\n",
			    dam_block->_id,
			    dam_block->_hits,
			    dam_block->_hits_missed);

		    /*

		    MONITOR_STRUCT_PTRS(lwroc_monitor_dams);

		    lwroc_monitor_dam_block_print_diff();

		    lwroc_monitor_dam_block_print_diff(ptr_block, ptr_prev,
						       ptr_info,
						       &_config._pd_config);
		    */
		  }
	      }
	    break;
	  }
	}
    }

  conn->_prev_data = conn->_good_data;
}

const char *
lwrocmon_handle_monitor_item_dt_trace(lwrocmon_conn *conn,
				      const char *wire, const char *end)
{
  lwroc_deserialize_error desererr;

  wire = lwroc_monitor_dt_trace_deserialize(&conn->_dt_trace,
					    wire,
					    (size_t) (end - wire),
					    &desererr);

  if (wire == NULL)
    {
      LWROC_ERROR_FMT("Monitor DT trace malformed (%s, 0x%x, 0x%x).\n",
		      desererr._msg, desererr._val1, desererr._val2);
      return NULL;
    }

  return wire;
}

FILE *_lwroc_dt_trace_fid = NULL;
/* Dumping is done with time offset, to (mostly) avoid wrapping.
 * Wrapping would still happen if the trace is longer than ~254 s.
 */
uint32_t _lwroc_dt_trace_offset_local = 0;
uint32_t _lwroc_dt_trace_offset_stamp = 0;

void lwrocmon_handle_monitor_item_dt_trace_dump(lwrocmon_conn *conn)
{
  uint32_t i;

  if (_lwroc_dt_trace_fid == NULL)
    {
      time_t mt;
      struct tm *mt_tm;
      char filename[256];

      mt = time(NULL);
      mt_tm = localtime(&mt);

      strftime(filename,sizeof(filename),
	       "dt_trace_%Y%m%d_%H%M%S.dt",mt_tm);

      _lwroc_dt_trace_fid = fopen(filename, "w");

      if (_lwroc_dt_trace_fid == NULL)
	{
	  LWROC_PERROR("fopen");
	  LWROC_FATAL_FMT("Failed to open deadtime trace dump file '%s' "
			  "for writing.", filename);
	}
    }

  /* printf ("length: %d\n", conn->_dt_trace._array_used); */

  fprintf (_lwroc_dt_trace_fid,
	   "System: %s%s\n",
	   conn->_hostname,
	   conn->_port_str);

  if (conn->_hint_master)
    fprintf (_lwroc_dt_trace_fid,
	     "Master: %s%s\n",
	     conn->_hint_master->_hostname,
	     conn->_hint_master->_port_str);

  if (conn->_dt_trace._ctime & 0x80000000)
    fprintf (_lwroc_dt_trace_fid,
	     "ctime: 0.%09d\n",
	     (conn->_dt_trace._ctime & 0xffff) * 100);

  for (i = 0; i+1 < conn->_dt_trace._array_used; )
    {
      uint32_t mark, value;
      uint32_t timescale;

      mark  = conn->_dt_trace._array[i++];
      value = conn->_dt_trace._array[i++];

      timescale = mark & LWROC_DT_TRACE_TIMESCALE_MASK;

      if (timescale == LWROC_DT_TRACE_TIMESCALE_EVENT)
	{
	  fprintf (_lwroc_dt_trace_fid,
		   "Event: %10d  Trig: %2d  Spill: %d\n",
		   value,
		   mark & 0xff,
		   (mark >> LWROC_DT_TRACE_SPILL_SHIFT) & 0x3);
	}
      else
	{
	  const char *t_str = "?";
	  const char *scale_str = "?";
	  uint32_t scale_mul = 1;
	  int fmt_uint64t = 0;

	  switch (timescale)
	    {
	    case LWROC_DT_TRACE_TIMESCALE_LOCAL_NS:
	      scale_str = "local";
	      break;
	    case LWROC_DT_TRACE_TIMESCALE_LOCAL_US:
	      scale_str = "local";
	      scale_mul = 1000;
	      break;
	    case LWROC_DT_TRACE_TIMESCALE_STAMP:
	      scale_str = "stamp";
	      fmt_uint64t = 1;
	      break;
	    }

	  switch (mark & LWROC_DT_TRACE_T_MASK)
	    {
	    case LWROC_DT_TRACE_T_TRIGGER:          t_str = "trigger";
	      break;
	    case LWROC_DT_TRACE_T_LAST_POLL:        t_str = "last_poll";
	      break;
	    case LWROC_DT_TRACE_T_AFTER_POLL:       t_str = "after_poll";
	      break;
	    case LWROC_DT_TRACE_T_AFTER_DT_RELEASE: t_str = "after_dt_rel";
	      break;
	    case LWROC_DT_TRACE_T_AFTER_POSTPROC:   t_str = "after_postproc";
	      break;
	    }

	  if (fmt_uint64t)
	    {
	      uint32_t hi;

	      if (!_lwroc_dt_trace_offset_stamp)
		{
		  _lwroc_dt_trace_offset_stamp =
		    0x80000000 | ((mark - 1) & 0xff);
		}

	      hi = (mark - _lwroc_dt_trace_offset_stamp) & 0xff;

	      fprintf (_lwroc_dt_trace_fid,
		       "@%s %" PRIu64 " %s\n",
		       scale_str,
		       (((uint64_t) hi) << 32) + value,
		       t_str);
	    }
	  else
	    {
	      uint32_t hi;

	      if (!_lwroc_dt_trace_offset_local)
		{
		  _lwroc_dt_trace_offset_local =
		    0x80000000 | ((mark - 1) & 0xff);
		}

	      hi = (mark - _lwroc_dt_trace_offset_local) & 0xff;

	      fprintf (_lwroc_dt_trace_fid,
		       "@%s %3d.%09d %s\n",
		       scale_str,
		       (int) hi,
		       (int) (value * scale_mul),
		       t_str);
	    }
	}
    }
  fflush(_lwroc_dt_trace_fid);
}

int lwrocmon_handle_monitor_item(lwrocmon_conn *conn)
{
  lwroc_monitor_block_header header, footer;
  lwroc_deserialize_error desererr;
  const char *wire = conn->_buf._buf;
  int unpack_data;
  size_t left = conn->_buf._size;
  const char *end = wire + left;

  /* First the header. */

  wire = lwroc_monitor_block_header_deserialize_envelope(&header,
							 wire,
							 (size_t) (end - wire),
							 &desererr);

  if (wire == NULL)
    {
      LWROC_ERROR_FMT("Monitor header block malformed (%s, 0x%x, 0x%x).\n",
		      desererr._msg, desererr._val1, desererr._val2);
      return 0;
    }

  /* Then through all items. */

  if (header._type == LWROC_MONITOR_BLOCK_TYPE_NORMAL)
    wire = lwrocmon_handle_monitor_item_normal(conn, wire, end,
					       &unpack_data);
  else if (header._type == LWROC_MONITOR_BLOCK_TYPE_DT_TRACE)
    wire = lwrocmon_handle_monitor_item_dt_trace(conn, wire, end);

  if (wire == NULL)
    return 0;

  /* And last the footer. */

  wire = lwroc_monitor_block_header_deserialize(&footer,
						wire,
						(size_t) (end - wire),
						&desererr);

  if (wire == NULL)
    {
      LWROC_ERROR_FMT("Monitor footer block malformed (%s, 0x%x, 0x%x).\n",
		      desererr._msg, desererr._val1, desererr._val2);
      return 0;
    }

  if (footer._sequence != header._sequence)
    {
      /* The sender updated the underlying transmit buffer while
       * sending to us.  This is perfectly legal.  Mainly means that
       * we have been slow receiving.  So mostly our own fault.
       * Some data is thus old, some is new.  Ignore.
       */
      return 1;
    }

  /* The data was good, it can be used! */

  if (header._type == LWROC_MONITOR_BLOCK_TYPE_NORMAL)
    lwrocmon_handle_monitor_item_normal_good(conn, &unpack_data);

  /* Sometimes, do something with it. */

  if (header._type == LWROC_MONITOR_BLOCK_TYPE_NORMAL)
    {
      if (_config._print_mon_buffers)
	lwrocmon_handle_monitor_item_normal_print(conn);
    }
  if (header._type == LWROC_MONITOR_BLOCK_TYPE_DT_TRACE)
    {
      if (_config._dump_dt_trace)
	lwrocmon_handle_monitor_item_dt_trace_dump(conn);
    }

  return 1;
}

void lwrocmon_data_bump_good_prev(void)
{
  pd_ll_item *iter;

  PD_LL_FOREACH(_lwrocmon_conns, iter)
    {
      lwrocmon_conn *conn =
	PD_LL_ITEM(iter, lwrocmon_conn, _conns);

      conn->_prev_data = conn->_good_data;
    }
}

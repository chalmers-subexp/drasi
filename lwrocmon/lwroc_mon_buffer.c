/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwrocmon_struct.h"
#include "lwroc_mon.h"

#include "lwroc_message.h"

#include <errno.h>

#include "../dtc_arch/acc_def/myinttypes.h"
#include "../dtc_arch/acc_def/myprizd.h"
#include "../dtc_arch/acc_def/has_msg_nosignal.h"

/********************************************************************/

void lwrocmon_realloc_buffer(lwrocmon_buffer *buf,size_t size)
{
  buf->_buf = realloc(buf->_buf,size);

  if (!buf->_buf)
    {
      LWROC_ERROR_FMT("Memory allocation error "
		      "(buf %" MYPRIzd " bytes).\n", size);
      exit(1);
    }

  /* printf ("realloc (%" MYPRIzd " -> %" MYPRIzd ")\n", buf->_alloc, size); */

  buf->_alloc  = size;
}

void lwrocmon_create_buffer(lwrocmon_buffer *buf,size_t size)
{
  buf->_buf = NULL;
  lwrocmon_realloc_buffer(buf, size);
}

void lwrocmon_destroy_buffer(lwrocmon_buffer *buf)
{
  free(buf->_buf);
  buf->_buf   = NULL;
  buf->_alloc = 0;
}

/********************************************************************/

/* Prerequisite: file descriptor ok for read by select, or we'll block
 * will only issue one read call, return -1 => failure, disconnect.
 */
ssize_t lwrocmon_read_buffer(lwrocmon_buffer *buf,int fd)
{
  size_t max_recv = buf->_size - buf->_offset;

  ssize_t n = read(fd,buf->_buf + buf->_offset,max_recv);

  /* printf ("R: %" MYPRIzd " %" MYPRIzd " %" MYPRIzd " %" MYPRIzd "\n", max_recv, n, buf->_size, buf->_offset); */

  if (n == 0)
    {
      /* Socket closed on other end, this we treat as an error. */
      return -1;
    }

  if (n < 0)
    {
      if (errno == EINTR ||
	  errno == EAGAIN)
	return 0; /* try again next time */

      perror("read");
      LWROC_WARNING("Error while reading data.\n");
      /* Treat it as some general failure, disconnect if that's an
       * option.
       */
      return -1;
    }

  buf->_offset += (size_t) n;

  return n;
}

/* Prerequisite: file descriptor ok for write by select, or we'll block
 * will only issue one write call, return -1 => failure, disconnect.
 */
ssize_t lwrocmon_write_buffer(lwrocmon_buffer *buf,int fd)
{
  size_t max_send = buf->_size - buf->_offset;

  ssize_t n = send(fd, buf->_buf + buf->_offset, max_send, MSG_NOSIGNAL);

  /* printf ("W: %" MYPRIzd " %" MYPRIzd " %" MYPRIzd " %" MYPRIzd "\n", max_send, n, buf->_size, buf->_offset); */

  if (n == 0)
    {
      LWROC_ERROR("write returned 0, cannot happen.\n");
      exit(1);
    }

  if (n < 0)
    {
      if (errno == EINTR ||
	  errno == EAGAIN)
	return 0; /* try again next time */

      if (errno == EPIPE) {
	LWROC_WARNING("Disconnected, pipe closed.\n");
      } else {
	LWROC_PERROR("write");
	LWROC_WARNING("Error while writing data.\n");
	/* Treat it as some general failure, disconnect if that's an
	 * option.
	 */
      }
      return -1;
    }

  buf->_offset += (size_t) n;

  return n;
}

/********************************************************************/

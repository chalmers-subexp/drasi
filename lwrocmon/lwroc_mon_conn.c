/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwrocmon_struct.h"
#include "lwroc_mon.h"

#include "lwroc_hostname_util.h"
#include "lwroc_message.h"
#include "lwroc_message_internal.h"
#include "lwroc_timeouts.h"
#include "lwroc_net_io_util.h"
#include "lwroc_net_badrequest.h"

#include "gen/lwroc_portmap_msg_serializer.h"
#include "gen/lwroc_request_msg_serializer.h"
#include "gen/lwroc_badrequest_serializer.h"

#include "gen/lwroc_message_source_serializer.h"
#include "gen/lwroc_message_item_serializer.h"
#include "gen/lwroc_message_keepalive_serializer.h"

#include "gen/lwroc_monitor_source_header_serializer.h"
#include "gen/lwroc_monitor_block_header_serializer.h"

#include <fcntl.h>
#include <errno.h>
#include <assert.h>
#include <string.h>

#include "../dtc_arch/acc_def/fall_through.h"

/********************************************************************/

int _lwrocmon_source_update = 1;
int _lwrocmon_block_update = 1;

/********************************************************************/

void lwrocmon_failed_setup_retry(lwrocmon_conn *conn,
				 struct timeval *now)
{
  conn->_state = LWROCMON_CONN_STATE_FAILED_SLEEP;

  lwroc_rate_limit_timeout(&conn->_retry_timeout,
			   LWROC_CONNECTION_MIN_RETRY_INTERVAL,
			   LWROC_CONNECTION_MAX_RETRY_INTERVAL,
			   now, &conn->_next_time);

  if (_config._num_print)
    {
      LWROC_FATAL("Reconnects not allowed in count-limited printing.\n");
      /* Why does the fatal above not exit?? */
      exit(1);
    }
}

/********************************************************************/

void lwrocmon_awaken_sleeping(struct sockaddr_storage *addr,
			      lwroc_select_info *si)
{
  pd_ll_item *iter;

  PD_LL_FOREACH(_lwrocmon_conns, iter)
    {
      lwrocmon_conn *conn =
	PD_LL_ITEM(iter, lwrocmon_conn, _conns);

      if (lwroc_compare_sockaddr(&conn->_serv_addr, addr))
	{
	  /* printf ("Found!\n"); */

	  if (conn->_state == LWROCMON_CONN_STATE_FAILED_SLEEP)
	    {
	      /* Try reconnect immediately. */
	      conn->_next_time = si->now;

	      /* printf ("Found sleeping!\n"); */
	    }
	}
    }
}

/********************************************************************/

int lwrocmon_create_socket(lwrocmon_conn *conn)
{
  conn->_fd = socket(PF_INET,SOCK_STREAM,IPPROTO_TCP);
  if (conn->_fd < 0)
    {
      perror("socket");
      LWROC_WARNING("Cannot create socket.\n");
      return 0;
    }

  /* Make the server socket non-blocking, such that we're not hit by
   * false selects (see Linux man page bug notes).
   */

  if (fcntl(conn->_fd,F_SETFL,fcntl(conn->_fd,F_GETFL) | O_NONBLOCK) == -1)
    {
      perror("fcntl");
      exit(1);
    }

  return 1;
}

/********************************************************************/

/* TODO: this is very close to lwroc_net_client_base_connect_socket(). */

int lwrocmon_connect_socket(lwrocmon_conn *conn,
			    struct timeval *now)
{
  struct sockaddr_storage *serv_addr;

  serv_addr = &conn->_serv_addr;
  if (conn->_state == LWROCMON_CONN_STATE_DATA_WAIT_CONNECTED)
    serv_addr = &conn->_data_addr;

  for ( ; ; )
    {
      int ret;

      ret = lwroc_net_io_connect(conn->_fd, serv_addr);

      if (ret == 0)
	{
	  conn->_state++; /* state after xxx_WAIT_CONNECTED */
	  /* Success. */
	  return 1;
	}
      else
	{
	  if (errno == EINTR)
	    continue; /* Try again. */

	  if (errno == EINPROGRESS)
	    return 0;

	  LWROC_PERROR("connect");
	  LWROC_WARNING("Failure connecting to server\n");

	  /* This socket failed, remove it. */
	  lwroc_safe_close(conn->_fd);
	  conn->_fd = -1;
	  /* Take some time, and then try again. */
	  lwrocmon_failed_setup_retry(conn, now);
	  return -1;
	}
    }
}

/********************************************************************/

void lwrocmon_prepare_request_msg(lwrocmon_conn *conn)
{
  lwroc_request_msg req_msg;

  memset(&req_msg, 0, sizeof (req_msg));

  if (_config._log_filename)
    req_msg._request = LWROC_REQUEST_MESSAGES;
  else
    req_msg._request = LWROC_REQUEST_MONITOR;

  /* Send along our random identifier, for any hints messages.
   * Use the _link_ack_idX fields.
   */

  req_msg._link_ack_id1 = _lwrocmon_hint_ident[0];
  req_msg._link_ack_id2 = _lwrocmon_hint_ident[1];

  conn->_buf._size = lwroc_request_msg_serialized_size();
  /* printf ("%" MYPRIzd " %" MYPRIzd "\n", conn->_buf._alloc, conn->_buf._size); */
  assert(conn->_buf._alloc >= conn->_buf._size);
  lwroc_request_msg_serialize(conn->_buf._buf, &req_msg);
}


/********************************************************************/

void lwrocmon_handle_badrequest(lwrocmon_conn *conn)
{
  lwroc_badrequest badreq;
  lwroc_deserialize_error desererr;
  const char *end;

  end = lwroc_badrequest_deserialize(&badreq,
				     conn->_buf._buf,
				     conn->_buf._size,
				     &desererr);

  if (end == NULL)
    {
      LWROC_ERROR_FMT("Bad request response malformed (%s, 0x%x, 0x%x).\n",
		      desererr._msg, desererr._val1, desererr._val2);
      return;
    }

  if (badreq._failure != conn->_last_failure)
    {
      lwroc_net_badrequest_report(NULL,
				  "Host response",
				  &badreq,
				  NULL /* TODO */);

      conn->_last_failure = badreq._failure;
    }
}

/********************************************************************/

void lwrocmon_conn_setup_select(lwroc_select_item *item,
				lwroc_select_info *si)
{
  lwrocmon_conn *conn =
    PD_LL_ITEM(item, lwrocmon_conn, _select_item);

#if STATE_MACHINE_DEBUG
  printf ("[%d]", conn->_state);
#endif

  if (timerisset(&conn->_stale_expire) &&
      lwroc_select_info_time_passed_else_setup(si, &conn->_stale_expire))
    {
      /* Delete monitor information of lost connection with delay. */
      lwroc_free_mon_source_info(conn);
      _lwrocmon_source_update = 1;
    }

 switch_again:
  assert(conn->_state != 0);
  switch (conn->_state)
    {
    case LWROCMON_CONN_STATE_FAILED_SLEEP:
      if (!lwroc_select_info_time_passed_else_setup(si, &conn->_next_time))
	break;
      /* Try connection again. */
      conn->_state = LWROCMON_CONN_STATE_PORTMAP_SETUP;
      FALL_THROUGH;
    case LWROCMON_CONN_STATE_PORTMAP_SETUP:
    case LWROCMON_CONN_STATE_DATA_SETUP:
      if (!lwrocmon_create_socket(conn))
	{

	}
      conn->_state++; /* xxx_WAIT_CONNECTED */

      lwrocmon_connect_socket (conn, &si->now);

      /* What to do depends on how far we got. */
      goto switch_again;

    case LWROCMON_CONN_STATE_PORTMAP_READ_PREPARE:
      conn->_buf._size = lwroc_portmap_msg_serialized_size();
      conn->_retry_timeout._progress = LWROCMON_CONN_PROGRESS_PMAP_CONNECTED;
      goto common_prepare_read;
    case LWROCMON_CONN_STATE_DATA_CHUNK_READ_PREPARE:
      /* We have gotten the first packet OK, so restart any error
       * reporting.  Also, restart quick reconnect attempts.
       */
      conn->_last_failure = 0;
      FALL_THROUGH;
    case LWROCMON_CONN_STATE_DATA_FIRST_READ_PREPARE:
      conn->_buf._size = sizeof (lwroc_msg_header_wire);
      goto common_prepare_read;
    common_prepare_read:
      /* Timeout if no (keepalive) message. */
      conn->_next_time = si->now;
      conn->_next_time.tv_sec += 6 * LWROC_CONNECTION_MSG_KEEPALIVE_INTERVAL;
      /* Reset the receive pointer. */
      conn->_buf._offset = 0;
      conn->_state++; /* xxx_READ */
      FALL_THROUGH;
    case LWROCMON_CONN_STATE_PORTMAP_READ:
    case LWROCMON_CONN_STATE_DATA_FIRST_READ:
    case LWROCMON_CONN_STATE_DATA_CHUNK_READ:
      lwroc_select_info_time_setup(si, &conn->_next_time);
      LWROC_READ_FD_SET(conn->_fd, si);
      break;

    case LWROCMON_CONN_STATE_DATA_ACK_WRITE_PREPARE:
      goto common_prepare_write;
    case LWROCMON_CONN_STATE_DATA_WRITE_PREPARE:
      lwrocmon_prepare_request_msg(conn);
      conn->_retry_timeout._progress = LWROCMON_CONN_PROGRESS_DATA_CONNECTED;
      goto common_prepare_write;
    common_prepare_write:
      /* Reset the send pointer. */
      conn->_buf._offset = 0;
      conn->_state++;
      FALL_THROUGH;
    case LWROCMON_CONN_STATE_PORTMAP_WAIT_CONNECTED:
    case LWROCMON_CONN_STATE_DATA_WAIT_CONNECTED:
    case LWROCMON_CONN_STATE_DATA_WRITE:
    case LWROCMON_CONN_STATE_DATA_ACK_WRITE:
      LWROC_WRITE_FD_SET(conn->_fd, si);
      break;

    case LWROCMON_CONN_STATE_DATA_DELAY_MSG_PRINT:
      lwroc_select_info_time_setup(si, &conn->_next_time);
      break;

    case LWROCMON_CONN_STATE_SHUTDOWN:
      /* Make sure it gets handled immediately, set timeout to 0 */
      lwroc_select_info_set_timeout_now(si);
      break;
    }

#if STATE_MACHINE_DEBUG
  printf (" -> %d\n", conn->_state);
#endif
}

int lwrocmon_conn_after_select(lwroc_select_item *item,
			       lwroc_select_info *si)
{
  lwrocmon_conn *conn =
    PD_LL_ITEM(item, lwrocmon_conn, _select_item);

#if STATE_MACHINE_DEBUG
  printf ("{%d}", conn->_state);
#endif

 switch_again:
  assert(conn->_state != 0);
  switch (conn->_state)
    {
    case LWROCMON_CONN_STATE_PORTMAP_READ:
    case LWROCMON_CONN_STATE_DATA_FIRST_READ:
    case LWROCMON_CONN_STATE_DATA_CHUNK_READ:
    {
      ssize_t n;

      if (timercmp(&si->now, &conn->_next_time, >))
	return 0; /* Timeout waiting for keepalive message. */

      if (!LWROC_READ_FD_ISSET(conn->_fd, si))
	break;

      n = lwrocmon_read_buffer(&conn->_buf,conn->_fd);

      if (n < 0)
	return 0; /* failure */

      if (conn->_buf._offset < conn->_buf._size)
	{
	  /* we've not reached end of buffer yet */
	  break;
	}
      conn->_state++;
      goto switch_again;
    }
    case LWROCMON_CONN_STATE_PORTMAP_READ_CHECK:
      assert(conn->_buf._offset == lwroc_portmap_msg_serialized_size());
    /* So we have read the portmap message. */
    {
      lwroc_portmap_msg pmap_msg;
      lwroc_deserialize_error desererr;
      const char *end;

      end = lwroc_portmap_msg_deserialize(&pmap_msg,
					  conn->_buf._buf, conn->_buf._offset,
					  &desererr);

      if (end == 0)
	{
	  LWROC_WARNING_FMT("Malformed portmap response (%s, 0x%x, 0x%x).\n",
			    desererr._msg, desererr._val1, desererr._val2);
	  /*
	  LWROC_WARNING_FMT("Bad magic (0x%08x) in portmap response.\n",
		  ntohl(pmap_msg->_header._magic));
	  LWROC_WARNING_FMT("Bad size (%d) in portmap response.\n",
		  ntohl(pmap_msg->_header._msg_size));
	  */
	  return 0;
	}

      /* Close the portmap connection. */
      lwroc_safe_close(conn->_fd);
      conn->_fd = -1;

      lwroc_copy_sockaddr_port(&conn->_data_addr,
			       &conn->_serv_addr,
			       pmap_msg._port);

      conn->_state = LWROCMON_CONN_STATE_DATA_SETUP;
      conn->_retry_timeout._progress = LWROCMON_CONN_PROGRESS_PMAP_DONE;
      break;
    }
    case LWROCMON_CONN_STATE_DATA_FIRST_READ_CHECK:
      /* Whatever happens, we kill old data within a second. */
      conn->_stale_expire = si->now;
      conn->_stale_expire.tv_sec += 1;
      FALL_THROUGH;
    case LWROCMON_CONN_STATE_DATA_CHUNK_READ_CHECK:
    {
      lwroc_msg_header_wire *header =
	(lwroc_msg_header_wire *) conn->_buf._buf;

      /* First, have we read the entire message? */
      uint32_t size;
      uint32_t magic;
      int ret;

      size = ntohl(header->_msg_size);

      if (size != conn->_buf._size)
	{
	  /* printf ("-- %d %d\n", size, (int) conn->_buf._size); */

	  if (size < conn->_buf._size) {
	    /* Can only happen first time. */
	    LWROC_WARNING_FMT("Bad size (%d) in first response.\n",
			      size);
	    return 0;
	  }
	  if (size > 0x10000) {
	    LWROC_WARNING_FMT("Cowardly refusing "
			      "large size (%d) of response.\n",
			      size);
	    return 0;
	  }
	  /* We need more(?) space. */
	  if (conn->_buf._alloc < size)
	    lwrocmon_realloc_buffer(&conn->_buf, size);
	  conn->_buf._size = size;
	  conn->_state--; /* go back to reading */
	  break;
	}

      /* printf (":: %d\n", size); */

      magic = ntohl(header->_magic);

      /* If nothing goes bad, next state is to read another chunk. */
      conn->_state = LWROCMON_CONN_STATE_DATA_CHUNK_READ_PREPARE;

#if STATE_MACHINE_DEBUG
      printf ("%x\n", magic);
#endif

      switch (magic)
	{
	case LWROC_BADREQUEST_SERPROTO_MAGIC1:
	  /* bad request */
	  lwrocmon_handle_badrequest(conn);
	  return 0; /* connection shall die */

	case LWROC_MESSAGE_SOURCE_SERPROTO_MAGIC1:
	  if (!_config._log_filename)
	    goto unexpected_magic;
	  ret = lwrocmon_handle_message_source(conn);
	  break;

	case LWROC_MESSAGE_ITEM_SERPROTO_MAGIC1:
	  if (!_config._log_filename)
	    goto unexpected_magic;
	  conn->_state = LWROCMON_CONN_STATE_DATA_DELAY_MSG_PRINT;
	  /* Might set state to ..._DATA_ACK_WRITE_PREPARE. */
	  ret = lwrocmon_handle_message_item(conn, si);
	  break;

	case LWROC_MESSAGE_KEEPALIVE_SERPROTO_MAGIC1:
	  if (!_config._log_filename)
	    goto unexpected_magic;
	  ret = lwrocmon_handle_message_keepalive(conn, si);
	  break;

	case LWROC_MONITOR_SOURCE_HEADER_SERPROTO_MAGIC1:
	  if (_config._log_filename)
	    goto unexpected_magic;
	  _lwrocmon_source_update = 1;
	  ret = lwrocmon_handle_monitor_source(conn);
	  break;

	case LWROC_MONITOR_BLOCK_HEADER_SERPROTO_MAGIC1:
	  if (_config._log_filename)
	    goto unexpected_magic;
	  _lwrocmon_block_update = 1;
	  ret = lwrocmon_handle_monitor_item(conn);
	  break;

	default:
	  LWROC_WARNING_FMT("Bad magic (0x%08x) in response.", magic);
	  return 0;

	unexpected_magic:
	  LWROC_WARNING_FMT("Unexpected magic (0x%08x) in response.", magic);
	  return 0;
	}

      if (ret)
	{
	  if (conn->_retry_timeout._progress <
	      LWROCMON_CONN_PROGRESS_DATA_FIRST_OK)
	    {
	      conn->_retry_timeout._progress =
		LWROCMON_CONN_PROGRESS_DATA_FIRST_OK;

	      if (!_config._show_detail &&
		  !_config._show_tree)
		LWROC_INFO_FMT("Connected with '%s%s'...\n",
			       conn->_hostname, conn->_port_str);
	    }
	  else
	    conn->_retry_timeout._progress =
	      LWROCMON_CONN_PROGRESS_DATA_CHUNK_OK;
	}
      return ret;
    }
    case LWROCMON_CONN_STATE_DATA_DELAY_MSG_PRINT:
      /* We try to get messages from different sources sorted in time
       * by delaying the printing of them if they are within a short
       * time span around actual time.  It is just a hack - no
       * guarantee, especially if there are many messages, or the
       * clocks are severely shifted.
       */

      if (timercmp(&(si->now), &(conn->_next_time), <))
	break;
      /*
      printf ("%d.%06d  %d.%.06d\n",
	      (int)si->now.tv_sec,
	      (int)si->now.tv_usec,
	      (int)conn->_next_time.tv_sec,
	      (int)conn->_next_time.tv_usec);
      */
      lwrocmon_print_message_item(conn);
      conn->_state = LWROCMON_CONN_STATE_DATA_CHUNK_READ_PREPARE;
      break;
    case LWROCMON_CONN_STATE_PORTMAP_WAIT_CONNECTED:
    case LWROCMON_CONN_STATE_DATA_WAIT_CONNECTED:
    {
      int so_error;
      socklen_t optlen;
      int ret;

      if (!LWROC_WRITE_FD_ISSET(conn->_fd, si))
	break;

      /* So the connect has finished, figure out if it was successful. */

      /* TODO: This could (almost) use
       * lwroc_net_client_base_connect_get_status().
       */

      optlen = sizeof(so_error);

      ret = getsockopt(conn->_fd,SOL_SOCKET,SO_ERROR,&so_error,&optlen);

      if (ret != 0)
	{
	  perror("getsockopt");
	  exit(1);
	}

      if (so_error != 0)
	{
	  if (!_config._show_detail &&
	      !_config._show_tree)
	    LWROC_WARNING_FMT("Delayed connect to '%s%s': %s\n",
			      conn->_hostname, conn->_port_str,
			      strerror(so_error));
	  return 0; /* connection failed */
	}

      conn->_state++; /* state after xxx_WAIT_CONNECTED */
      /* Next state shall either read or write. */

      /*conn->_mode = LWROCMON_CONN_STATE_SEND_ORDERS;*/
      /* must wait for allowance to write, break */
      break;
    }

    case LWROCMON_CONN_STATE_DATA_WRITE:
    case LWROCMON_CONN_STATE_DATA_ACK_WRITE:
    {
      ssize_t n;

      if (!LWROC_WRITE_FD_ISSET(conn->_fd, si))
	break;

      n = lwrocmon_write_buffer(&conn->_buf,conn->_fd);

      if (n < 0)
	return 0; /* failure */

      if (conn->_buf._offset < conn->_buf._size)
	{
	  /* we've not reached end of buffer yet */
	  break;
	}
      if (conn->_state == LWROCMON_CONN_STATE_DATA_ACK_WRITE)
	conn->_state = LWROCMON_CONN_STATE_DATA_DELAY_MSG_PRINT;
      else
	conn->_state++;
      break;
    }

    }

#if STATE_MACHINE_DEBUG
  printf (" -> %d [ok]\n", conn->_state);
#endif

  return 1;
}

/********************************************************************/

int lwrocmon_conn_after_fail(lwroc_select_item *item,
			     lwroc_select_info *si)
{
  lwrocmon_conn *conn =
    PD_LL_ITEM(item, lwrocmon_conn, _select_item);

  /* Connection closed, remove it. */

  if (conn->_fd != -1)
    {
      lwroc_safe_close(conn->_fd);
      conn->_fd = -1;
      lwroc_print_msg_sources_info(conn,
				   LWROC_MSGLVL_DETACH,
				   timercmp(&si->now, &conn->_next_time, >) ?
				   "Log detached (timeout)." :
				   "Log detached.");
      lwroc_free_msg_source_info(conn);
      if (_config._show_detail ||
	  _config._show_tree)
	{
	  /* We do not kill the data immediately, but mark it as stale. */
	  if (!timerisset(&conn->_stale_expire))
	    {
	      conn->_stale_expire = si->now;
	      conn->_stale_expire.tv_sec += _config._expire_time;

	      _lwrocmon_source_update = 1;
	    }
	}
      else
	lwroc_free_mon_source_info(conn);
      if (!_config._show_detail &&
	  !_config._show_tree &&
	  /* These states have already shown warning. */
	  conn->_state != LWROCMON_CONN_STATE_PORTMAP_WAIT_CONNECTED &&
	  conn->_state != LWROCMON_CONN_STATE_DATA_WAIT_CONNECTED)
	LWROC_WARNING_FMT("Connection with '%s%s' closed...\n",
			  conn->_hostname, conn->_port_str);
      /* Force new header. */
      _lwrocmon_print_header_ago = -1;

    }

  /* Dynamic connections (none yet - ever?) we might remove. */

  /* Try connection again... */

  lwrocmon_failed_setup_retry(conn, &si->now);

  return 1;
}

/********************************************************************/

lwroc_select_item_info lwrocmoc_conn_select_item_info =
{
  lwrocmon_conn_setup_select,
  lwrocmon_conn_after_select,
  lwrocmon_conn_after_fail,
  NULL,
  0,
};

/********************************************************************/

void lwrocmon_add_mon_host(const char *cmd,
			   struct sockaddr_storage *serv_addr,
			   int added_why)
{
  lwrocmon_conn *conn;
  uint16_t port;

  /* Create the connection structure. */

  conn = (lwrocmon_conn *) malloc (sizeof (lwrocmon_conn));

  if (!conn)
    LWROC_FATAL("Memory allocation failure (connection info).");

  memset (conn, 0, sizeof (*conn));

  /* When adding a host, we immediately look the hostname up.  But we
   * do not care about any port or if it responds.  Also portmapping
   * is done later.  (In part as it has to be retried after
   * communication failure.)
   */

  if (cmd)
    {
      char *colon;

      if (!lwroc_get_host_port(cmd, LWROC_NET_DEFAULT_PORT, &conn->_serv_addr))
	exit(1);

      strncpy(conn->_hostname, cmd, sizeof (conn->_hostname));
      conn->_hostname[sizeof (conn->_hostname) - 1] = 0;
      colon = strchr(conn->_hostname,':');
      if (colon)
	*colon = 0;
    }
  else
    {
      conn->_serv_addr = *serv_addr;

      if (_config._no_name_lookup)
	lwroc_inet_ntop(&conn->_serv_addr,
			conn->_hostname, sizeof (conn->_hostname));
      else
	lwroc_get_hostname(&conn->_serv_addr,
			   conn->_hostname, sizeof (conn->_hostname));
    }

  port = lwroc_get_port(&conn->_serv_addr);

  if (port == LWROC_NET_DEFAULT_PORT)
    conn->_port_str[0] = 0;
  else
    {
      if (port > LWROC_NET_DEFAULT_PORT && port <= LWROC_NET_DEFAULT_PORT+20)
	sprintf (conn->_port_str, ":+%d", port - LWROC_NET_DEFAULT_PORT);
      else
	sprintf (conn->_port_str, ":%d", port);
    }

  conn->_state = LWROCMON_CONN_STATE_PORTMAP_SETUP;
  conn->_fd = -1;

  {
    size_t sz_portmap_msg = lwroc_portmap_msg_serialized_size();
    size_t sz_request_msg = lwroc_request_msg_serialized_size();

    size_t sz = sz_portmap_msg;
    if (sz_request_msg > sz)
      sz = sz_request_msg;

    lwrocmon_create_buffer(&conn->_buf, sz);
  }

  conn->_select_item.item_info = &lwrocmoc_conn_select_item_info;

  conn->_added_why = added_why;

  conn->_msg_rate_ignore_quota = 100;

  /* Insert into the list. */

  PD_LL_ADD_BEFORE(&_lwrocmon_net_clients, &conn->_select_item._items);

  PD_LL_ADD_BEFORE(&_lwrocmon_conns, &conn->_conns);
}

/********************************************************************/

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "../dtc_arch/acc_def/has_curses.h"

#include "lwroc_mon.h"

#if HAS_CURSES
#define USE_CURSES 1

/* Must be early, or stdio may miss vs(n)printf. */
#include <stdarg.h>

#include "lwroc_hostname_util.h"

#include "gen/lwroc_monitor_main_source_serializer.h"
#include "gen/lwroc_monitor_net_source_serializer.h"
#include "gen/lwroc_monitor_file_source_serializer.h"
#include "gen/lwroc_monitor_in_source_serializer.h"

#include "gen/lwroc_monitor_main_block_serializer.h"
#include "gen/lwroc_monitor_net_block_serializer.h"
#include "gen/lwroc_monitor_file_block_serializer.h"
#include "gen/lwroc_monitor_in_block_serializer.h"

#include "../lu_common/colourtext.h"

#include <stdio.h>
#include <math.h>
#include <assert.h>

WINDOW *mw;
WINDOW *wtop;

lwroc_multi_col_info _lwroc_multi_col = { 0, NULL };

void lwroc_multicol_remap(int *y, int *x)
{
  int y_use, x_use;

  y_use = *y;
  x_use = *x;

  if (_lwroc_multi_col._col_breaks)
    {
      int extra_col;

      for (extra_col = 0; extra_col < _lwroc_multi_col._extra_cols;
	   extra_col++)
	{
	  if (*y >= _lwroc_multi_col._col_breaks[extra_col])
	    {
	      y_use = *y - _lwroc_multi_col._col_breaks[extra_col];
	      x_use = *x + (1 + extra_col) * 81;
	    }
	}
    }

  *y = y_use;
  *x = x_use;
}

void wma(WINDOW *win, int y, int x, int attrs, short color,
	 const char *fmt,...)
  __attribute__ ((__format__ (__printf__, 6, 7)));

void wma(WINDOW *win, int y, int x, int attrs, short color,
	 const char *fmt,...)
{
  va_list ap;
  char buf[1024];
  int n;

  va_start(ap, fmt);
#if HAS_VSNPRINTF
  n = vsnprintf(buf, size, fmt, ap);
#else
  /* Let's hope we do not overflow, see below... */
  n = vsnprintf(buf,sizeof(buf), fmt, ap);

  buf[sizeof(buf)-1] = 0;
#endif
  va_end(ap);

  assert ((size_t) n < sizeof (buf));

  lwroc_multicol_remap(&y, &x);

  wmove(win, y, x);
  wattrset(win, CURSES_ATTRSET_ATTR_CAST attrs);
  wcolor_set(win, color, NULL);
  waddstr(win, buf);
}

void wma_percent(WINDOW *win, int y, int x, int attrs, short color,
		 int width, double value)
{
  int prec = 1;

  if (width < 6)
    prec = 0;

  if (value >= 2)
    {
      if (value <= 20)
	prec = 2;
      else if (value >= 0.9 * pow(10,width-2))
	prec = 0;

      wma(win, y, x, attrs, color, "%*.*f", width, prec, value);
    }
  else
    {
      if (width < 5)
	prec = 0;

      wma(win, y, x, attrs, color, "%*.*f%%", width-1, prec, value * 100.);
    }
}

void wmc(WINDOW *win, int y, int x, int attrs, short color,
	 const chtype c)
{
  lwroc_multicol_remap(&y, &x);

  wmove(win, y, x);
  wattrset(win, CURSES_ATTRSET_ATTR_CAST attrs);
  wcolor_set(win, color, NULL);
  waddch(win, c);
}

typedef void (*void_void_func)(void);

void lwrocmon_detail_tree_init(void)
{
  mw = initscr();
  start_color();
  atexit((void_void_func) endwin);
  nodelay(mw, TRUE);
  nocbreak();
  noecho();
  nonl();
  curs_set(0);
  colourpair_prepare();

  wrefresh(mw);
}

void lwrocmon_detail_source_update(void)
{
  wclear(mw);
}

void lwrocmon_detail_block_update(void)
{
  pd_ll_item *iter;

  int line = 0;

  PD_LL_FOREACH(_lwrocmon_conns, iter)
    {
      lwrocmon_conn *conn =
	PD_LL_ITEM(iter, lwrocmon_conn, _conns);

      char dotted[INET6_ADDRSTRLEN];
      uint16_t port;
      int live;
      lwrocmon_src_info *item;

      live = (conn->_mon_sources && !timerisset(&conn->_stale_expire));

      lwroc_inet_ntop(&conn->_serv_addr, dotted, sizeof (dotted));
      port = lwroc_get_port(&conn->_serv_addr);

      wma(mw, line, 3, live ? A_BOLD : A_NORMAL, CTR_BLUE_BG_YELLOW,
	  "%15s:%d", dotted, port);

      line++;

      for (item = conn->_mon_sources; item; item = item->_next)
	{
	  switch (item->_type_magic)
	    {
	    case LWROC_MONITOR_NET_SOURCE_SERPROTO_MAGIC1:
	      {
		lwroc_monitor_net_source *ptr_source =
		  (lwroc_monitor_net_source *) item->_source_info;
		uint32_t connb;

		for (connb = 0; connb < ptr_source->_conn_blocks; connb++)
		  {
		    lwroc_monitor_conn_source *conn_source =
		      item->_conn_source_info + connb;
		    lwroc_monitor_conn_block *conn_block =
		      item->_conn_data[conn->_good_data] + connb;

		    lwrocmon_conn_info conn_info;

		    lwrocmon_get_conn_info(conn_source, conn_block,
					   &conn_info);
		    if (!live)
		      conn_info.active = 0;

		    wma(mw, line, 0, conn_info.active ? A_BOLD : A_DIM,
			CTR_BLUE, "%s", conn_info.dir_str);

		    wma(mw, line, 3, conn_info.active ? A_BOLD : A_DIM,
			CTR_BLUE, "%s", conn_info.type_str);

		    wma(mw, line, 10, conn_info.active ? A_BOLD : A_DIM,
			CTR_BLUE, "%s", conn_info.status_str);

		    wma(mw, line, 13, conn_info.active ? A_BOLD : A_DIM,
			CTR_BLUE,
			"%15s:%d", conn_info.block_dotted, conn_block->_port);
		    wma(mw, line, 34, conn_info.active ? A_BOLD : A_DIM,
			CTR_BLUE, "/%d", conn_source->_port);

		    wma(mw, line, 42, A_NORMAL,
			CTR_BLUE, "%s", conn_source->_hostname);

		    wattrset(mw, A_NORMAL);

		    wmove(mw,line,12);
		    waddstr(mw, "[");
		    wmove(mw,line,40);
		    waddstr(mw, "]");

		    line++;
		  }
	      }
	      break;
	    }
	}
    }

  wrefresh(mw);
}

#else/*!HAS_CURSES*/

void lwrocmon_detail_tree_init(void) { }

void lwrocmon_detail_source_update(void) { }

void lwrocmon_detail_block_update(void) { }

#endif/*!HAS_CURSES*/

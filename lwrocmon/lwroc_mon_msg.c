/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwrocmon_struct.h"
#include "lwroc_mon.h"

#include "lwroc_hostname_util.h"
#include "lwroc_message.h"

#include "gen/lwroc_message_source_serializer.h"
#include "gen/lwroc_message_item_serializer.h"
#include "gen/lwroc_message_keepalive_serializer.h"
#include "gen/lwroc_message_item_ack_serializer.h"

#include <string.h>
#include <assert.h>

/********************************************************************/

FILE *_fid_messages = NULL;

void lwroc_print_message_line(time_t sec, uint32_t nsec,
			      const char *file, uint32_t line,
			      const char *level_str,
			      const char *host_port_thread,
			      const char *msg)
{
  time_t mt;
  struct tm *mt_tm;
  char mt_date[64];
  char mt_tz[64];
  uint32_t mt_ns;

  mt = sec;
  mt_tm = localtime(&mt);
  strftime(mt_date,sizeof(mt_date),"%Y-%m-%d %H:%M:%S",mt_tm);
  strftime(mt_tz,sizeof(mt_tz),"%z %Z",mt_tm);
  mt_ns = nsec;

  /* On a preceding line, ended with carriage-return (no line-feed),
   * we print ancillary information, like file:line, local
   * time-zone offset and boldify-information.
   */

  fprintf (_fid_messages, "%s :%s:%d:\r",
	   mt_tz, file, line);

  /* If the port number is the default port, we do not print it. */

  /* Note: the hostname width cannot change, or lwroclog will figure out
   * the wrong indent for the message.
   */
  fprintf (_fid_messages, "%s.%06d %-2s %-15s %s\n",
	   mt_date, mt_ns / 1000,
	   level_str, host_port_thread,
	   msg);

  fflush(_fid_messages);

  _printed_log_message = 1;
}

void lwroc_print_msg_source_info(lwrocmsg_src_info *src_info,
				 const char *hostname,
				 int level_attach_detach,
				 const char *msg)
{
  struct timespec ts;

  const char *level_str =
    (level_attach_detach == LWROC_MSGLVL_ATTACH) ? "A" : "D";

  if (src_info && !src_info->_mainthread)
    return; /* We only print for the main thread. */

  clock_gettime(CLOCK_REALTIME, &ts);

  lwroc_print_message_line(ts.tv_sec, (uint32_t) ts.tv_nsec,
			   "-", 0,
			   level_str,
			   src_info ? src_info->_host_port_thread : hostname,
			   msg);

}

void lwroc_print_msg_greeting(int start)
{
  char hostname[256];

  lwroc_gethostname(hostname, sizeof (hostname));

  lwroc_print_msg_source_info(NULL, hostname,
			      start ? LWROC_MSGLVL_ATTACH :
			      LWROC_MSGLVL_DETACH,
			      start == 2 ? "Mark." :
			      start == 1 ? "Log writer started." :
			      "Log writer stopped.");
}

void lwroc_print_msg_sources_info(lwrocmon_conn *conn,
				  int level_attach_detach,
				  const char *msg)
{
  lwrocmsg_src_info *src = conn->_msg_sources;

  for ( ; src; src = src->_next)
    {
      lwroc_print_msg_source_info(src, NULL, level_attach_detach, msg);
    }
}

void lwroc_free_msg_source_info(lwrocmon_conn *conn)
{
  lwrocmsg_src_info *src = conn->_msg_sources;

  conn->_msg_sources = NULL;

  for ( ; src; )
    {
      lwrocmsg_src_info *next = src->_next;

      free (src->_host_port_thread);
      free (src->_hostname);
      free (src->_thread);
      free (src);

      src = next;
    }
}

int lwrocmon_handle_message_source(lwrocmon_conn *conn)
{
  lwroc_message_source src_info;
  lwrocmsg_src_info *conn_src_info = NULL;
  lwroc_deserialize_error desererr;
  const char *end;

  end = lwroc_message_source_deserialize_inplace(&src_info,
						 conn->_buf._buf,
						 conn->_buf._size,
						 &desererr);

  if (end == NULL)
    {
      LWROC_ERROR_FMT("Message source block malformed (%s, 0x%x, 0x%x).\n",
		      desererr._msg, desererr._val1, desererr._val2);
      return 0;
    }

  conn_src_info = (lwrocmsg_src_info *) malloc (sizeof (lwrocmsg_src_info));

  if (!conn_src_info)
    LWROC_FATAL("Memory allocation error.\n");

  conn_src_info->_source   = src_info._source;
  conn_src_info->_port     = src_info._port;
  conn_src_info->_hostname = strdup_chk(src_info._hostname);
  conn_src_info->_thread   = strdup_chk(src_info._thread);
  conn_src_info->_mainthread = src_info._mainthread;

  conn_src_info->_host_port_thread = (char *)
    malloc (strlen(src_info._hostname) + strlen(src_info._thread) +
	    16); /* ":num/0" */

  if (conn_src_info->_host_port_thread)
    {
      if (src_info._port != LWROC_NET_DEFAULT_PORT)
	sprintf (conn_src_info->_host_port_thread, "%s:%d/%s",
		 src_info._hostname, src_info._port, src_info._thread);
      else
	sprintf (conn_src_info->_host_port_thread, "%s/%s",
		 src_info._hostname, src_info._thread);
    }

  conn_src_info->_next = conn->_msg_sources;
  conn->_msg_sources = conn_src_info;

  lwroc_print_msg_source_info(conn_src_info, NULL,
			      LWROC_MSGLVL_ATTACH, "Log attached.");

  return 1;
}

/********************************************************************/

void lwrocmon_prepare_item_ack_msg(lwrocmon_conn *conn,
				   uint32_t source, uint32_t ack_ident)
{
  lwroc_message_item_ack ack_msg;

  memset(&ack_msg, 0, sizeof (ack_msg));

  ack_msg._source    = source;
  ack_msg._ack_ident = ack_ident;

  conn->_buf._size = lwroc_message_item_ack_serialized_size();
  /* printf ("%" MYPRIzd " %" MYPRIzd "\n", conn->_buf._alloc, conn->_buf._size); */
  assert(conn->_buf._alloc >= conn->_buf._size);
  lwroc_message_item_ack_serialize(conn->_buf._buf, &ack_msg);
}

/********************************************************************/

lwrocmsg_src_info *lwroc_find_source_info(lwrocmon_conn *conn, uint32_t source)
{
  lwrocmsg_src_info *src = conn->_msg_sources;

  for ( ; src; src = src->_next)
    {
      if (src->_source == source)
	return src;
    }
  return NULL;
}

int lwrocmon_handle_message_item(lwrocmon_conn *conn,
				 lwroc_select_info *si)
{
  lwroc_deserialize_error desererr;
  const char *end;

  /*{
    size_t i, n;
    uint32_t *p = (uint32_t *) conn->_buf._buf;
    n = conn->_buf._size / sizeof (uint32_t);
    printf ("---\n");
    for (i = 0; i < n; i++)
      printf ("%03zx: item: %08x\n", i*4, *(p++));
  }*/

  end = lwroc_message_item_deserialize_inplace(&conn->_msg,
					       conn->_buf._buf,
					       conn->_buf._size,
					       &desererr);

  if (end == 0)
    {
      LWROC_ERROR_FMT("Message item block malformed (%s, 0x%x, 0x%x).\n",
		      desererr._msg, desererr._val1, desererr._val2);
      return 0;
    }

  /* Printing is delayed by 100 ms relative to the original message
   * time, in order to try to get messages from different sources in
   * time order.
   */

  conn->_next_time.tv_sec = (time_t) conn->_msg._time._sec;
  conn->_next_time.tv_usec = (int) (conn->_msg._time._nsec / 1000);

  if (timercmp(&(si->now), &(conn->_next_time), <))
    {
      /* If the originally suggested time is in the future, it is
       * obviously wrong, so set it to the current time.
       */
      conn->_next_time = si->now;
    }
  else
    {
      conn->_next_time.tv_usec += 100000; /* 10 ms */

      if (conn->_next_time.tv_usec > 1000000)
	{
	  conn->_next_time.tv_usec -= 1000000;
	  conn->_next_time.tv_sec++;
	}
    }

  if (conn->_msg._ack_ident)
    {
      lwrocmon_prepare_item_ack_msg(conn,
				    conn->_msg._source,
				    conn->_msg._ack_ident);

      conn->_state = LWROCMON_CONN_STATE_DATA_ACK_WRITE_PREPARE;
    }

  return 1;
}

int lwrocmon_print_message_item(lwrocmon_conn *conn)
{
  char level_str[5];

  const char *host_port_thread = NULL;

  lwrocmsg_src_info *src_info;

  level_str[0] = (char)  conn->_msg._level;
  level_str[1] = (char) (conn->_msg._level >> 8);
  level_str[2] = (char) (conn->_msg._level >> 16);
  level_str[3] = (char) (conn->_msg._level >> 24);
  level_str[4] = 0;

  src_info =
    lwroc_find_source_info(conn, conn->_msg._source);

  if (src_info)
    {
      host_port_thread = src_info->_host_port_thread;
    }
  else
    LWROC_ERROR ("Message for unknown source.\n");

  if (conn->_msg._lost)
    {
      lwroc_print_message_line((time_t) conn->_msg._time._sec,
			       conn->_msg._time._nsec,
			       "-", 0,
			       "D", host_port_thread,
			       "Messages lost for this thread "
			       "(buffer full, avoided lock-up).");
      fprintf (stderr,"report loss!\n");
    }

  lwroc_print_message_line((time_t) conn->_msg._time._sec,
			   conn->_msg._time._nsec,
			   conn->_msg._file, conn->_msg._line,
			   level_str, host_port_thread,
			   conn->_msg._msg);

  return 1;
}

int lwrocmon_handle_message_keepalive(lwrocmon_conn *conn,
				      lwroc_select_info *si)
{
  lwroc_message_keepalive keepalive;
  lwroc_deserialize_error desererr;
  const char *end;

  /*lwrocmsg_src_info *src_info;*/

  (void) si;

  /*{
    size_t i, n;
    uint32_t *p = (uint32_t *) conn->_buf._buf;
    n = conn->_buf._size / sizeof (uint32_t);
    printf ("---\n");
    for (i = 0; i < n; i++)
      printf ("%03zx: item: %08x\n", i*4, *(p++));
  }*/

  end = lwroc_message_keepalive_deserialize(&keepalive,
					    conn->_buf._buf,
					    conn->_buf._size,
					    &desererr);

  if (end == 0)
    {
      LWROC_ERROR_FMT("Message keepalive block malformed (%s, 0x%x, 0x%x).\n",
		      desererr._msg, desererr._val1, desererr._val2);
      return 0;
    }

  /* if () */
  /*
  src_info =
    lwroc_find_source_info(conn, conn->_msg._source);

  if (src_info)
    {
      if (!src_info->_attach_logged)
	{
	  src_info->_attach_logged = 1;
	  lwroc_print_msg_source_info(src_info, NULL,
				      LWROC_MSGLVL_ATTACH, "Log attached.");
	}
    }
  else
    LWROC_ERROR ("Message keep-alive for unknown source.\n");
  */
  return 1;
}

/********************************************************************/

void lwrocmon_print_message_status(struct timeval *now)
{
  int n_conns = 0;
  int n_sleep = 0;
  time_t max_diff = 0;

  pd_ll_item *iter;

  PD_LL_FOREACH(_lwrocmon_conns, iter)
    {
      lwrocmon_conn *conn =
	PD_LL_ITEM(iter, lwrocmon_conn, _conns);

      n_conns++;

      if (conn->_state == LWROCMON_CONN_STATE_FAILED_SLEEP)
	{
	  struct timeval diff;

	  n_sleep++;

	  /* How long until next connection attempt? */

	  if (timercmp(now, &conn->_next_time, <))
	    {
	      timersub(&conn->_next_time, now, &diff);
	      if (diff.tv_usec)
		diff.tv_sec++;
	      if (diff.tv_sec > max_diff)
		max_diff = diff.tv_sec;
	    }
	}
    }

  printf ("Log conn: %d/%d.",
	  n_conns - n_sleep, n_conns);
  if (n_sleep)
    printf (" (Reconn. delay %d s.)",
	    (int) max_diff);
  printf ("   \r");
  fflush(stdout);
}

/********************************************************************/

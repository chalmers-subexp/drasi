/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_mon.h"

#include "lwroc_hostname_util.h"

#include "gen/lwroc_monitor_main_source_serializer.h"
#include "gen/lwroc_monitor_net_source_serializer.h"
#include "gen/lwroc_monitor_file_source_serializer.h"
#include "gen/lwroc_monitor_filter_source_serializer.h"
#include "gen/lwroc_monitor_serv_source_serializer.h"
#include "gen/lwroc_monitor_in_source_serializer.h"

#include "gen/lwroc_monitor_main_block_serializer.h"
#include "gen/lwroc_monitor_net_block_serializer.h"
#include "gen/lwroc_monitor_file_block_serializer.h"
#include "gen/lwroc_monitor_filter_block_serializer.h"
#include "gen/lwroc_monitor_serv_block_serializer.h"
#include "gen/lwroc_monitor_in_block_serializer.h"

#include "../dtc_arch/acc_def/has_tiocgwinsz.h"
#include "../dtc_arch/acc_def/has_curses.h"
#if HAS_CURSES
#define USE_CURSES 1
#endif
#include "../lu_common/colourtext.h"

#include <stdio.h>

void lwrocmon_loop_rate_header(void)
{
  lwrocmon_src_info *item;
  pd_ll_item *iter;

  PD_LL_FOREACH(_lwrocmon_conns, iter)
    {
      lwrocmon_conn *conn =
	PD_LL_ITEM(iter, lwrocmon_conn, _conns);

      conn->_rate_info.flags = 0 /* LWROCMON_RATE_PRINT_CPU_FRAC */;

      for (item = conn->_mon_sources; item; item = item->_next)
	{
	  switch (item->_type_magic)
	    {
	    case LWROC_MONITOR_NET_SOURCE_SERPROTO_MAGIC1:
	      conn->_rate_info.flags |= LWROCMON_RATE_PRINT_BUF;
	      conn->_rate_info.flags |= LWROCMON_RATE_PRINT_SENT;

	      {
		lwroc_monitor_net_source *ptr_source =
		  (lwroc_monitor_net_source *) item->_source_info;
		uint32_t connb;

		for (connb = 0; connb < ptr_source->_conn_blocks; connb++)
		  {
		    lwroc_monitor_conn_source *conn_source =
		      item->_conn_source_info + connb;

		    if (conn_source->_type == LWROC_CONN_TYPE_SYSTEM &&
			(conn_source->_kind == LWROC_CONN_KIND_TRIVA ||
			 conn_source->_kind == LWROC_CONN_KIND_TRIMI))
		      conn->_rate_info.flags |= LWROCMON_RATE_PRINT_STATUS;
		  }
	      }
	      break;
	    case LWROC_MONITOR_MAIN_SOURCE_SERPROTO_MAGIC1:
	      conn->_rate_info.flags |= LWROCMON_RATE_PRINT_EVENTS;
	      conn->_rate_info.flags |= LWROCMON_RATE_PRINT_COLL;
	      break;
	    case LWROC_MONITOR_FILTER_SOURCE_SERPROTO_MAGIC1:
	      conn->_rate_info.flags |= LWROCMON_RATE_PRINT_FILTER_EVENTS;
	      conn->_rate_info.flags |= LWROCMON_RATE_PRINT_FILTER_COLL;
	      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_CPU_FRAC)
		conn->_rate_info.flags |= LWROCMON_RATE_PRINT_CPU_FRAC_FILTER;
	      break;
	    case LWROC_MONITOR_FILE_SOURCE_SERPROTO_MAGIC1:
	      conn->_rate_info.flags |= LWROCMON_RATE_PRINT_WRITTEN;
	      conn->_rate_info.flags |= LWROCMON_RATE_PRINT_WR_SINCE;
	      conn->_rate_info.flags |= LWROCMON_RATE_PRINT_WR_SIZE;
	      conn->_rate_info.flags |= LWROCMON_RATE_PRINT_WR_NAME;
	      break;
	    }
	}
    }

  printf ("\n");
  PD_LL_FOREACH(_lwrocmon_conns, iter)
    {
      lwrocmon_conn *conn =
	PD_LL_ITEM(iter, lwrocmon_conn, _conns);
      size_t width = 0;

      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_STATUS)
	width += 8 + 1+7;
      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_EVENTS)
	{
	  if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_STATUS)
	    width++;
	  width += 9 + 1+6;
	}
      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_COLL)
	width += 1+7;
      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_FILTER_EVENTS)
	width += 9 + 1+6;
      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_FILTER_COLL)
	width += 1+7;
      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_BUF)
	width += 1+6;
      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_SENT)
	width += 1+7;
      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_WRITTEN)
	width += 1+7;
      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_WR_SINCE)
	width += 1+7;
      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_WR_SIZE)
	width += 1+7;
      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_WR_NAME)
	width += 1+10;
      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_CPU_FRAC)
	{
	  width += 1+6 + 1+6;
	  if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_CPU_FRAC_FILTER)
	    width += 1+6;
	  width += 1+6;
	}

      if (width)
	{
	  size_t hostname_len = strlen(conn->_hostname);
	  size_t port_no_len  = strlen(conn->_port_str);

	  size_t dots = width - 2 - port_no_len;
	  if (dots > hostname_len)
	    dots -= hostname_len;
	  else if (dots > 0)
	    {
	      hostname_len = dots;
	      dots = 0;
	    }

	  printf ("| %s%*.*s%*.*s%s%*s",
		  CT_OUT(BOLD),
		  (int) hostname_len, (int) hostname_len,conn->_hostname,
		  (int) port_no_len, (int) port_no_len, conn->_port_str,
		  CT_OUT(NORM),
		  (int) dots, " ");
	}
    }

  printf ("\n");
  PD_LL_FOREACH(_lwrocmon_conns, iter)
    {
      lwrocmon_conn *conn =
	PD_LL_ITEM(iter, lwrocmon_conn, _conns);

      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_STATUS)
	{
	  printf ("%8s","Status");
	  printf (" %s%7s%s",
		  CT_OUT(RED), "Error", CT_OUT(NORM));
	}
      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_EVENTS)
	{
	  if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_STATUS)
	    printf (" ");
	  printf ("%9s", "Events");
	  printf (" %s%6s%s",
		  CT_OUT(GREEN), "Incr", CT_OUT(NORM));
	}
      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_COLL)
	{
	  printf (" %s%7s%s",
		  CT_OUT(RED), "Coll", CT_OUT(NORM));
	}
      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_FILTER_EVENTS)
	{
	  printf (" %9s", "Events");
	  printf (" %s%6s%s",
		  CT_OUT(GREEN), "F-Incr", CT_OUT(NORM));
	}
      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_FILTER_COLL)
	{
	  printf (" %s%7s%s",
		  CT_OUT(RED), "F-Coll", CT_OUT(NORM));
	}
      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_BUF)
	{
	  printf (" %s%6s%s",
		  CT_OUT(MAGENTA), "Buf", CT_OUT(NORM));
	}
      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_SENT)
	{
	  printf (" %s%7s%s",
		  CT_OUT(BLUE), "Sent", CT_OUT(NORM));
	}
      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_WRITTEN)
	{
	  printf (" %s%7s%s",
		  CT_OUT(GREEN), "Written", CT_OUT(NORM));
	}
      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_WR_SINCE)
	{
	  printf (" %7s", "Since");
	}
      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_WR_SIZE)
	{
	  printf (" %s%7s%s",
		  CT_OUT(GREEN), "Size", CT_OUT(NORM));
	}
      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_WR_NAME)
	{
	  printf (" %-10s", "File");
	}
      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_CPU_FRAC)
	{
	  printf (" %s%6s%s",
		  CT_OUT(GREEN), "In", CT_OUT(NORM));
	  printf (" %s%6s%s",
		  CT_OUT(RED), "Main", CT_OUT(NORM));
	  if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_CPU_FRAC_FILTER)
	    printf (" %s%6s%s",
		    CT_OUT(RED), "Filt", CT_OUT(NORM));
	  printf (" %s%6s%s",
		  CT_OUT(BLUE), "Serv", CT_OUT(NORM));
	}
    }
  printf ("\n");

  PD_LL_FOREACH(_lwrocmon_conns, iter)
    {
      lwrocmon_conn *conn =
	PD_LL_ITEM(iter, lwrocmon_conn, _conns);

      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_STATUS)
	{
	  printf ("%8s","");
	  printf (" %7s","");
	}
      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_EVENTS)
	{
	  if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_STATUS)
	    printf (" ");
	  printf ("%9s","");
	  printf (" %6s","");
	}
      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_COLL)
	{
	  printf (" %7s","B/s");
	}
      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_FILTER_EVENTS)
	{
	  printf (" %9s","(filter)");
	  printf (" %6s","");
	}
      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_FILTER_COLL)
	{
	  printf (" %7s","B/s");
	}
      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_BUF)
	{
	  printf (" %6s","");
	}
      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_SENT)
	{
	  printf (" %7s","B/s");
	}
      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_WRITTEN)
	{
	  printf (" %7s","B/s");
	}
      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_WR_SINCE)
	{
	  printf (" %7s", "");
	}
      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_WR_SIZE)
	{
	  printf (" %7s", "B");
	}
      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_WR_NAME)
	{
	  printf (" %-10s", "");
	}
      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_CPU_FRAC)
	{
	  printf (" %6s","");
	  printf (" %6s","");
	  if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_CPU_FRAC_FILTER)
	    printf (" %6s","");
	  printf (" %6s","");
	}
   }
  printf ("\n");
}

void lwrocmon_time_ago_str(char *str, uint64_t time_ago)
{
  /* We display the time duration in hh:mm:ss format.
   * this is easier to understand than using e.g. seconds
   * and then wrapping to minutes etc.  Fractional
   * minutes or hours is just horrible.
   *
   *   mm:ss
   * h:mm:ss
   *  hh:mm:
   * Ddhh:mm uses 7 locations, switching to
   * DDDDdhh when passing over 10 days
   */

  uint64_t secs, mins, hours, days;

  if (((int64_t) time_ago) < 0)
    {
      if (((int64_t) time_ago) < -1)
	{
	  strcpy (str, "future");
	  return;
	}
      time_ago = 0;
    }

  secs = time_ago;
  mins = secs / 60; secs %= 60;
  hours = mins / 60; mins %= 60;
  days = hours / 24; hours %= 24;

  if (days >= 10)
    sprintf (str,"%dd%02d",
	     (int) days, (int) hours);
  else if (days >= 1)
    sprintf (str,"%dd%02d:%02d",
	     (int) days, (int) hours, (int) mins);
  else if (hours >= 10)
    sprintf (str,"%d:%02d:",
	     (int) hours, (int) mins);
  else if (hours >= 1)
    sprintf (str,"%d:%02d:%02d",
	     (int) hours, (int) mins, (int) secs);
  else
    sprintf (str,"%02d:%02d",
	     (int) mins, (int) secs);
}

void lwroc_mon_rate_item_conn(lwrocmon_conn *conn,
			      lwroc_monitor_conn_source *ptr_source,
			      lwroc_monitor_conn_block *ptr_block)
{


  if (ptr_source->_direction == LWROC_CONN_DIR_OUTGOING &&
      (ptr_source->_type == LWROC_REQUEST_DATA_TRANS ||
       ptr_source->_type == LWROC_CONN_TYPE_TRANS ||
       ptr_source->_type == LWROC_CONN_TYPE_EBYE_PUSH ||
       ptr_source->_type == LWROC_CONN_TYPE_EBYE_PULL ||
       ptr_source->_type == LWROC_CONN_TYPE_FNET ||
       ptr_source->_type == LWROC_CONN_TYPE_STREAM))
    {
      struct sockaddr_storage look_addr;
      char hostname[256];

      /* Data input connection. */

      lwroc_set_ipv4_addr(&look_addr,
			  ptr_source->_ipv4_addr);
      lwroc_set_port(&look_addr, ptr_source->_port);

      if (_config._no_name_lookup)
	lwroc_inet_ntop(&look_addr, hostname, sizeof (hostname));
      else
	lwroc_get_hostname(&look_addr, hostname, sizeof (hostname));

      if (_config._influxdb)
	{
	  printf ("\nINFLUXDB:"
		  /*     tags follow */
		  "host=%s,"
		  "in_host=%s,"
		  "buf=in"
		  " " /* fields follow */
		  "events=%" PRIu64 ","
		  "bytes=%" PRIu64 ","
		  "fill=%" PRIu64 ","
		  "size=%" PRIu64 ""
		  " " /* timestamp */
		  "%" PRIu64 "%09" PRIu32 ""
		  "\n",
		  conn->_hostname,
		  hostname,
		  ptr_block->_events,
		  ptr_block->_bytes,
		  ptr_block->_buf_data_fill < ptr_source->_buf_data_size ?
		  ptr_block->_buf_data_fill : ptr_source->_buf_data_size,
		  ptr_source->_buf_data_size,
		  ptr_block->_time._sec,
		  ptr_block->_time._nsec);
	}
    }
}

#define CPU_FRAC(dest, ptr_block, ptr_prev) do {			\
    uint64_t diff_cputime_usec =					\
      ptr_block->_cputime_usec - ptr_prev->_cputime_usec;		\
    if (diff_cputime_usec)						\
      dest = frequency * 0.000001 * (double) diff_cputime_usec;		\
  } while (0)

void lwrocmon_loop_rate_data(int header_ago)
{
  lwrocmon_src_info *item;
  pd_ll_item *iter;
  int used = 0;
  int cols = 80;

#if HAS_TIOCGWINSZ
  {
    struct winsize winsz;
    ioctl(0, TIOCGWINSZ, &winsz);

    cols = winsz.ws_col; /* winsz.ws_row */
  }
#endif

  PD_LL_FOREACH(_lwrocmon_conns, iter)
    {
      lwrocmon_conn *conn =
	PD_LL_ITEM(iter, lwrocmon_conn, _conns);

      /* Search for the information we want. */

      int has_event_incr = 0;
      int has_coll_rate = 0;
      int has_filter_event_incr = 0;
      int has_filter_coll_rate = 0;
      int has_sent_rate = 0;
      int has_file_wr_rate = 0;

      uint64_t events = 0;
      uint64_t event_incr = 0;

      uint64_t filter_events = 0;
      uint64_t filter_event_incr = 0;

      uint64_t buf_fill = 0;
      uint64_t buf_size = 0;

      double coll_rate = 0;
      double filter_coll_rate = 0;
      double sent_rate = 0;
      double file_wr_rate = 0;

      double main_cpu_frac = 0;
      double filter_cpu_frac = 0;
      double in_cpu_frac = 0;
      double serv_cpu_frac = 0;

      lwroc_status_format *status_format = NULL;
      lwroc_status_format *error_format = NULL;

      uint64_t error_ago = 0;
      int error_dim = 0;

      int filewr_ago_ok = 0;
      uint64_t filewr_ago = 0;
      uint32_t filewr_state = 0;
      uint64_t filewr_size = 0;
      const char *filewr_name = NULL;

      for (item = conn->_mon_sources; item; item = item->_next)
	{
	  switch (item->_type_magic)
	    {
	    case LWROC_MONITOR_NET_SOURCE_SERPROTO_MAGIC1:
	      {
		lwroc_monitor_net_source *ptr_source =
		  (lwroc_monitor_net_source *) item->_source_info;
		uint32_t connb;
		MONITOR_STRUCT_PTRS(lwroc_monitor_net);

		if (_config._influxdb)
		  {
		    printf ("\nINFLUXDB:"
			    /*     tags follow */
			    "host=%s,"
			    "buf=main"
			    " " /* fields follow */
			    "fill=%" PRIu64 ","
			    "size=%" PRIu64 ""
			    " " /* timestamp */
			    "%" PRIu64 "%09" PRIu32 ""
			    "\n",
			    conn->_hostname,
			    ptr_block->_buf_data_fill <
			    ptr_source->_buf_data_size ?
			    ptr_block->_buf_data_fill :
			    ptr_source->_buf_data_size,
			    ptr_source->_buf_data_size,
			    ptr_block->_time._sec,
			    ptr_block->_time._nsec);
		  }

		if (_config._influxdb)
		  {
		    printf ("\nINFLUXDB:"
			    /*     tags follow */
			    "host=%s,"
			    "out=network"
			    " " /* fields follow */
			    "bytes=%" PRIu64 ""
			    " " /* timestamp */
			    "%" PRIu64 "%09" PRIu32 ""
			    "\n",
			    conn->_hostname,
			    ptr_block->_sent_data,
			    ptr_block->_time._sec,
			    ptr_block->_time._nsec);
		  }

		buf_fill = ptr_block->_buf_data_fill;
		buf_size = ptr_source->_buf_data_size;

		if (ptr_prev->_time._sec)
		  {
		    MONITOR_STRUCT_FREQ;

		    has_sent_rate = 1;
		    sent_rate = frequency *
		      (double) (ptr_block->_sent_data - ptr_prev->_sent_data);
		  }

		(void) ptr_info;
		(void) ptr_prev;

		for (connb = 0; connb < ptr_source->_conn_blocks; connb++)
		  {
		    lwroc_monitor_conn_source *conn_source =
		      item->_conn_source_info + connb;
		    lwroc_monitor_conn_block *conn_block =
		      item->_conn_data[conn->_good_data] + connb;

		    lwroc_mon_rate_item_conn(conn,
					     conn_source,
					     conn_block);

		    if (conn_source->_type == LWROC_CONN_TYPE_SYSTEM)
		      {
			status_format =
			  lwroc_get_status_format(conn_block->_aux_status);
			error_format =
			  lwroc_get_status_format(conn_block->_aux_last_error);

			if (header_ago == 0)
			  error_dim = 1;
			else if (conn_block->_aux_time_error._sec &&
			    conn_block->_aux_time_error._sec +
			    2 * (uint64_t) _config._print_rate <
			    conn_block->_time._sec)
			  {
			    error_format = NULL;
			    error_ago =
			      conn_block->_time._sec -
			      conn_block->_aux_time_error._sec;
			  }
		      }
		  }
		break;
	      }

	    case LWROC_MONITOR_MAIN_SOURCE_SERPROTO_MAGIC1:
	      {
		MONITOR_STRUCT_PTRS(lwroc_monitor_main);

		if (_config._influxdb)
		  {
		    printf ("\nINFLUXDB:"
			    /*     tags follow */
			    "host=%s,"
			    "buf=main"
			    " " /* fields follow */
			    "events=%" PRIu64 ","
			    "bytes=%" PRIu64 ""
			    " " /* timestamp */
			    "%" PRIu64 "%09" PRIu32 ""
			    "\n",
			    conn->_hostname,
			    ptr_block->_events,
			    ptr_block->_bytes,
			    ptr_block->_time._sec,
			    ptr_block->_time._nsec);
		  }

		events = ptr_block->_events;

		if (ptr_prev->_time._sec)
		  {
		    uint64_t coll_bytes;
		    MONITOR_STRUCT_FREQ;

		    has_event_incr = 1;
		    event_incr = (ptr_block->_events - ptr_prev->_events);

		    has_coll_rate = 1;
		    coll_bytes = ptr_block->_bytes - ptr_prev->_bytes;
		    coll_rate = coll_bytes ?
		      frequency * (double) coll_bytes : 0;

		    if (event_incr)
		      _lwrocmon_event_incr_nonzero = 1;
		    /*
		    printf ("%20.10f  %" PRIu64 "  %" PRIu64 "\n",
			    frequency,
			    ptr_block->_cputime_usec,
			    ptr_prev->_cputime_usec);
		    */
		    CPU_FRAC(main_cpu_frac, ptr_block, ptr_prev);
		  }

		(void) ptr_info;
		(void) ptr_prev;
		break;
	      }

	    case LWROC_MONITOR_FILTER_SOURCE_SERPROTO_MAGIC1:
	      {
		MONITOR_STRUCT_PTRS(lwroc_monitor_filter);

		if (_config._influxdb)
		  {
		    printf ("\nINFLUXDB:"
			    /*     tags follow */
			    "host=%s,"
			    "buf=filter"
			    " " /* fields follow */
			    "events=%" PRIu64 ","
			    "bytes=%" PRIu64 ""
			    " " /* timestamp */
			    "%" PRIu64 "%09" PRIu32 ""
			    "\n",
			    conn->_hostname,
			    ptr_block->_events,
			    ptr_block->_bytes,
			    ptr_block->_time._sec,
			    ptr_block->_time._nsec);
		  }

		filter_events = ptr_block->_events;

		if (ptr_prev->_time._sec)
		  {
		    uint64_t filter_coll_bytes;
		    MONITOR_STRUCT_FREQ;

		    has_filter_event_incr = 1;
		    filter_event_incr =
		      (ptr_block->_events - ptr_prev->_events);

		    has_filter_coll_rate = 1;
		    filter_coll_bytes = ptr_block->_bytes - ptr_prev->_bytes;
		    filter_coll_rate = filter_coll_bytes ?
		      frequency * (double) filter_coll_bytes : 0;

		    /* We claim to produce data also if filter
		     * generates output data.
		     */
		    if (filter_event_incr)
		      _lwrocmon_event_incr_nonzero = 1;
		    /*
		    printf ("%20.10f  %" PRIu64 "  %" PRIu64 "\n",
			    frequency,
			    ptr_block->_cputime_usec,
			    ptr_prev->_cputime_usec);
		    */
		    CPU_FRAC(filter_cpu_frac, ptr_block, ptr_prev);
		  }

		(void) ptr_info;
		(void) ptr_prev;
		break;
	      }

	    case LWROC_MONITOR_FILE_SOURCE_SERPROTO_MAGIC1:
	      {
		lwroc_monitor_filewr_block *filewr_block_use = NULL;
		lwroc_monitor_file_source *ptr_source =
		  (lwroc_monitor_file_source *) item->_source_info;
		MONITOR_STRUCT_PTRS(lwroc_monitor_file);

		if (_config._influxdb)
		  {
		    printf ("\nINFLUXDB:"
			    /*     tags follow */
			    "host=%s,"
			    "out=file"
			    " " /* fields follow */
			    "events=%" PRIu64 ","
			    "bytes=%" PRIu64 ""
			    " " /* timestamp */
			    "%" PRIu64 "%09" PRIu32 ""
			    "\n",
			    conn->_hostname,
			    ptr_block->_events,
			    ptr_block->_bytes,
			    ptr_block->_time._sec,
			    ptr_block->_time._nsec);
		  }

		if (ptr_prev->_time._sec)
		  {
		    MONITOR_STRUCT_FREQ;

		    has_file_wr_rate = 1;
		    file_wr_rate = frequency *
		      (double) (ptr_block->_bytes - ptr_prev->_bytes);
		  }

		(void) ptr_info;
		(void) ptr_prev;

		/* We prefer an open file, otherwise take the recently
		 * closed one, which is the first.
		 */

		if (ptr_source->_filewr_blocks)
		  {
		    uint32_t filewrb;

		    filewr_block_use =
		      item->_filewr_data[conn->_good_data] + 0;

		    for (filewrb = 1; filewrb < ptr_source->_filewr_blocks;
			 filewrb++)
		      {
			lwroc_monitor_filewr_block *filewr_block =
			  item->_filewr_data[conn->_good_data] + filewrb;

			if (filewr_block->_state == LWROC_FILEWR_STATE_OPEN ||
			    filewr_block->_state == LWROC_FILEWR_STATE_JAMMED)
			  filewr_block_use = filewr_block;
		      }

		    filewr_state = filewr_block_use->_state;
		    if (filewr_block_use->_open_close_time._sec)
		      {
			filewr_ago   =
			  ptr_block->_time._sec -
			  filewr_block_use->_open_close_time._sec;
			filewr_ago_ok = 1;
		      }
		    filewr_size  = filewr_block_use->_bytes;
		    filewr_name  = filewr_block_use->_filename;
		  }

		break;
	      }

	    case LWROC_MONITOR_SERV_SOURCE_SERPROTO_MAGIC1:
	      {
		MONITOR_STRUCT_PTRS(lwroc_monitor_serv);

		if (ptr_prev->_time._sec)
		  {
		    MONITOR_STRUCT_FREQ;

		    CPU_FRAC(serv_cpu_frac, ptr_block, ptr_prev);
		  }

		(void) ptr_info;
		(void) ptr_prev;
		break;
	      }

	    case LWROC_MONITOR_IN_SOURCE_SERPROTO_MAGIC1:
	      {
		MONITOR_STRUCT_PTRS(lwroc_monitor_in);

		if (ptr_prev->_time._sec)
		  {
		    MONITOR_STRUCT_FREQ;

		    CPU_FRAC(in_cpu_frac, ptr_block, ptr_prev);
		  }

		(void) ptr_info;
		(void) ptr_prev;
		break;
	      }
	    }
	}

      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_STATUS)
	{
	  if (status_format)
	    printf ("%*s%s%*s%s",
		    8 - (int) strlen(status_format->text), "",
		    COLOURTEXT_GET(0,status_format->color),
		    (int) strlen(status_format->text),
		    status_format->text,
		    CT_OUT(NORM));
	  else
	    printf (" %8s", "?");
	  used += 9;

	  if (error_format)
	    {
	      short color = error_format->color;

	      if (error_dim)
		color = CTR_NORM;

	      printf (" %*s%s%*s%s",
		      7 - (int) strlen(error_format->text), "",
		      COLOURTEXT_GET(0,color),
		      (int) strlen(error_format->text),
		      error_format->text,
		      CT_OUT(NORM));
	      (void) color;
	    }
	  else if (error_ago)
	    {
	      char str[32];

	      lwrocmon_time_ago_str(str, error_ago);

	      printf (" %7s", str);
	    }
	  else
	    printf (" %7s", "?");
	  used += 8;
	}

      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_EVENTS)
	{
	  char str_val[64];

	  if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_STATUS)
	    {
	      printf (" ");
	      used += 1;
	    }

	  lwroc_format_prefix(str_val, sizeof (str_val),
			      (double) events, 9,
			      &_lwrocmon_rate_state._events_diff_info);

	  printf ("%s%9s%s",
		  CT_OUT(BOLD),
		  str_val,
		  CT_OUT(NORM));
	  used += 9;

	  if (has_event_incr)
	    lwroc_format_prefix(str_val, sizeof (str_val),
				(double) event_incr, -6,
				&_lwrocmon_rate_state._incr_diff_info);
	  else
	    strcpy(str_val, "-");

	  printf (" %s%6s%s",
		  CT_OUT(BOLD_GREEN),
		  str_val,
		  CT_OUT(NORM));
	  used += 7;
	}

      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_COLL)
	{
	  char str_val[64];

	  if (has_coll_rate)
	    lwroc_format_prefix(str_val, sizeof (str_val),
				coll_rate, 7,
				&_lwrocmon_rate_state._coll_diff_info);
	  else
	    strcpy(str_val, "-");

	  printf (" %s%7s%s",
		  CT_OUT(BOLD_RED),
		  str_val,
		  CT_OUT(NORM));
	  used += 8;
	}

      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_FILTER_EVENTS)
	{
	  char str_val[64];

	  lwroc_format_prefix(str_val, sizeof (str_val),
			      (double) filter_events, 9,
			      &_lwrocmon_rate_state._filter_events_diff_info);

	  printf (" %s%9s%s",
		  CT_OUT(BOLD),
		  str_val,
		  CT_OUT(NORM));
	  used += 9;

	  if (has_filter_event_incr)
	    lwroc_format_prefix(str_val, sizeof (str_val),
				(double) filter_event_incr, -6,
				&_lwrocmon_rate_state._filter_incr_diff_info);
	  else
	    strcpy(str_val, "-");

	  printf (" %s%6s%s",
		  CT_OUT(BOLD_GREEN),
		  str_val,
		  CT_OUT(NORM));
	  used += 7;
	}

      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_FILTER_COLL)
	{
	  char str_val[64];

	  if (has_filter_coll_rate)
	    lwroc_format_prefix(str_val, sizeof (str_val),
				filter_coll_rate, 7,
				&_lwrocmon_rate_state._filter_coll_diff_info);
	  else
	    strcpy(str_val, "-");

	  printf (" %s%7s%s",
		  CT_OUT(BOLD_RED),
		  str_val,
		  CT_OUT(NORM));
	  used += 8;
	}

      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_BUF)
	{
	  printf (" %s%5.1f%%%s",
		  CT_OUT(BOLD_MAGENTA),
		  100.0 * (double) buf_fill / (double) buf_size,
		  CT_OUT(NORM));
	  used += 6;
	}

      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_SENT)
	{
	  char str_val[64];

	  if (has_sent_rate)
	    lwroc_format_prefix(str_val, sizeof (str_val),
				sent_rate, 7,
				&_lwrocmon_rate_state._sent_diff_info);
	  else
	    strcpy(str_val, "-");

	  printf (" %s%7s%s",
		  CT_OUT(BOLD_BLUE),
		  str_val,
		  CT_OUT(NORM));
	  used += 8;
	}

      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_WRITTEN)
	{
	  char str_val[64];

	  if (has_file_wr_rate)
	    lwroc_format_prefix(str_val, sizeof (str_val),
				file_wr_rate, 7,
				&_lwrocmon_rate_state._file_wr_diff_info);
	  else
	    strcpy(str_val, "-");

	  printf (" %s%7s%s",
		  CT_OUT(BOLD_GREEN),
		  str_val,
		  CT_OUT(NORM));
	  used += 8;
	}

      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_WR_SINCE)
	{
	  if (filewr_state && filewr_ago_ok)
	    {
	      char str[32];

	      lwrocmon_time_ago_str(str, filewr_ago);

	      printf (" %s%7s%s",
		      filewr_state == LWROC_FILEWR_STATE_OPEN ?
		      CT_OUT(BOLD) : CT_OUT(RED),
		      str,
		      CT_OUT(NORM));
	    }
	  else
	    printf (" %7s", "");
	  used += 8;
	}
      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_WR_SIZE)
	{
	  char str_val[64];

	  switch (filewr_state)
	    {
	    case LWROC_FILEWR_STATE_CLOSED:
	      printf (" %s%7s%s",
		      CT_OUT(BOLD_RED),
		      "CL -",
		      CT_OUT(NORM));
	      break;
	    case LWROC_FILEWR_STATE_OPEN:
	      lwroc_format_prefix(str_val, sizeof (str_val),
				  (double) filewr_size, 7,
				  &_lwrocmon_rate_state._file_sz_diff_info);

	      printf (" %s%7s%s",
		      CT_OUT(BOLD_GREEN),
		      str_val,
		      CT_OUT(NORM));
	      break;
	    case LWROC_FILEWR_STATE_HOLD:
	      printf (" %s%7s%s",
		      CT_OUT(BOLD_RED),
		      "HOLD -",
		      CT_OUT(NORM));
	      break;
	    case LWROC_FILEWR_STATE_JAMMED:
	      printf (" %s%7s%s",
		      CT_OUT(BOLD_WHITE_BG_RED),
		      "JAMMED",
		      CT_OUT(NORM));
	      break;
	    default:
	      printf (" %s%7s%s",
		      CT_OUT(BOLD_RED),
		      "? -",
		      CT_OUT(NORM));
	      break;
	    }
	  used += 8;
	}
      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_WR_NAME)
	{
	  size_t remain = (size_t) (cols - used - 3);
	  const char *ptr = filewr_name;
	  const char *cutaway = "";

	  while (strlen(cutaway) + strlen(ptr) > remain)
	    {
	      const char *p;

	      p = strchr(ptr,'/');

	      if (!p)
		{
		  ssize_t len, use;

		  cutaway = "...";

		  len = (ssize_t) strlen(ptr);
		  use = (ssize_t) (remain - strlen(cutaway));

		  if (use > len)
		    use = len;
		  if (use < 0)
		    use = 0;

		  ptr = ptr + len - use;
		  break;
		}
	      ptr = p+1;
	      cutaway = "?/";
	    }

	  printf (" %s%s%s%s",
		  cutaway,
		  filewr_state == LWROC_FILEWR_STATE_OPEN ?
		  CT_OUT(BOLD) : CT_OUT(NORM),
		  /*filewr_name*/ ptr,
		  CT_OUT(NORM));
	}

      if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_CPU_FRAC)
	{
	  printf (" %s%5.1f%%%s",
		  CT_OUT(BOLD_GREEN),
		  100.0 * in_cpu_frac,
		  CT_OUT(NORM));

	  printf (" %s%5.1f%%%s",
		  CT_OUT(BOLD_RED),
		  100.0 * main_cpu_frac,
		  CT_OUT(NORM));

	  if (conn->_rate_info.flags & LWROCMON_RATE_PRINT_CPU_FRAC_FILTER)
	    printf (" %s%5.1f%%%s",
		    CT_OUT(BOLD_RED),
		    100.0 * filter_cpu_frac,
		    CT_OUT(NORM));

	  printf (" %s%5.1f%%%s",
		  CT_OUT(BOLD_BLUE),
		  100.0 * serv_cpu_frac,
		  CT_OUT(NORM));
	}
    }

  printf ("\n");

  if (_config._influxdb)
    printf ("\nINFLUXDBFLUSH:\n");

  /* Needed for influxdb, since stdout is not line buffered
   * when going to a pipe.
   */
  fflush(stdout);
}

/* influxdb items:

host=%,buf=main          bytes=%,events=%
host=%,buf=main          fill=%,size=%

host=%,in_host=%,buf=in  bytes=%,events=%,fill=%,size=%

host=%,out=network       bytes=%
host=%,out=file          bytes=%,events=%

*/

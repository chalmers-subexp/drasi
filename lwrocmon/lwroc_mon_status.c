/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "../dtc_arch/acc_def/has_curses.h"

#include "lwroc_mon.h"

#if HAS_CURSES
#define USE_CURSES 1
#endif

#include "../lu_common/colourtext.h"

lwroc_status_format _lwroc_status_format[] = {
  /* One-letter codes are in principle fine, we are making progress.
   * Two-letter codes means trouble.  Two-letter codes that start with
   * 'W' (or "Wt") means we are being held for some badness reason.
   * Waiting for deadtime, buffer space or readout is not really the
   * fault of the DAQ, something is clogging or taking long time.
   */
  { LWROC_TRIVA_STATUS_REINIT,           "R", "ReInit",   CTR_BLUE_BG_YELLOW },
  { LWROC_TRIVA_STATUS_WAIT_ABORT,      "WA", "WtAbort",  CTR_WHITE_BG_RED },
  { LWROC_TRIVA_STATUS_WAIT_TERM,       "WT", "WtTerm",   CTR_WHITE_BG_RED },
  { LWROC_TRIVA_STATUS_WAIT_CONN,       "WC", "WtConn",   CTR_WHITE_BG_RED },
  { LWROC_TRIVA_STATUS_WAIT_EB_CLR,     "WE", "WtEBClr",  CTR_WHITE_BG_RED },
  { LWROC_TRIVA_STATUS_WAIT_MASTER_SELF,"WM", "WtMaster", CTR_WHITE_BG_RED },
  { LWROC_TRIVA_STATUS_WAIT_SLAVE,      "WS", "WtSlave",  CTR_WHITE_BG_RED },
  { LWROC_TRIVA_STATUS_WAIT_IDENT,      "WI", "WtIdent",  CTR_WHITE_BG_RED },
  { LWROC_TRIVA_STATUS_WAIT_TEST,       "Wt", "WtTest",   CTR_WHITE_BG_RED },
  { LWROC_TRIVA_STATUS_WAIT_RUN,        "Wr", "WtRun",    CTR_WHITE_BG_RED },
  { LWROC_TRIVA_STATUS_TEST,             "T", "Test",     CTR_BLUE_BG_YELLOW },
  { LWROC_TRIVA_STATUS_RUN,              "r", "run",      CTR_YELLOW_BG_BLUE },
  { LWROC_TRIVA_STATUS_INSPILL,          "i", "inspill",  CTR_YELLOW_BG_BLUE },
  { LWROC_TRIVA_STATUS_STUCK_INSPILL,    "k", "st.inspl", CTR_RED_BG_YELLOW },
  { LWROC_TRIVA_STATUS_OFFSPILL,         "o", "offspill", CTR_YELLOW_BG_BLUE },
  { LWROC_TRIVA_STATUS_STOP,             "S", "stopped",  CTR_BLUE_BG_YELLOW },
  { LWROC_TRIVA_STATUS_WAIT_NET,        "WN", "WaitNet",  CTR_WHITE_BG_RED },
  { LWROC_TRIVA_STATUS_WAIT_DT,         "DT", "DeadTime", CTR_BLACK_BG_YELLOW },
  { LWROC_TRIVA_STATUS_WAIT_BUF,        "BF", "BufSpace", CTR_BLACK_BG_YELLOW },
  { LWROC_TRIVA_STATUS_WAIT_READ,       "RD", "Readout",  CTR_BLACK_BG_YELLOW },
  { LWROC_TRIVA_STATUS_NODIAGNOSE,      "D?", "NoDiagns", CTR_WHITE_BG_RED },
  { LWROC_TRIVA_STATUS_SLAVE,            "s",  "slave",   CTR_BLUE_BG_YELLOW },
  { LWROC_TRIVA_STATUS_SLAVE_WAIT_SELF, "Ws", "WtOSlave", CTR_WHITE_BG_RED },
  { LWROC_TRIVA_STATUS_SLAVE_WAIT_ABORT,"Wa", "slWtAbrt", CTR_WHITE_BG_RED }, /* unused */
  { LWROC_TRIVA_STATUS_HEADACHE,        "HA", "Headache", CTR_WHITE_BG_RED },
  /* Failure codes, one letter(?) */
  { LWROC_LAST_ERROR_FORGET,             "-", "-",        CTR_NONE },
  { LWROC_TRIVA_FAIL_NONE,               "-", "-",        CTR_NONE },
  { LWROC_TRIVA_FAIL_LOCAL,              "L", "Local",    CTR_WHITE_BG_RED },
  { LWROC_TRIVA_FAIL_NET,                "N", "Net",      CTR_WHITE_BG_RED },
  { LWROC_TRIVA_FAIL_EB_ID,              "I", "EB_ID",    CTR_WHITE_BG_RED },
  { LWROC_TRIVA_FAIL_TEST,               "T", "Test",     CTR_WHITE_BG_RED },
  { LWROC_TRIVA_FAIL_MISMATCH,           "M", "Mism",     CTR_WHITE_BG_RED },
  { LWROC_TRIVA_FAIL_EC_MISMATCH,        "E", "ECmism",   CTR_WHITE_BG_RED }, /* unused */
  { LWROC_TRIVA_FAIL_BAD_EC_SYNC,        "C", "ECdsync",  CTR_WHITE_BG_RED },
  { LWROC_TRIVA_FAIL_TRIG_UNEXPECT,      "U", "UnExpTrg", CTR_WHITE_BG_RED },
  { LWROC_TRIVA_FAIL_SEQ_ERROR,          "S", "SeqErr",   CTR_WHITE_BG_RED },
  { LWROC_TRIVA_FAIL_SEQ_UNEXPECT,       "Q", "SeqUnExp", CTR_WHITE_BG_RED },
  { LWROC_TRIVA_FAIL_SEQ_MALFORM,        "F", "SeqMalf",  CTR_WHITE_BG_RED },
  { LWROC_TRIVA_FAIL_EB_DATA_CONN,       "D", "DataCon",  CTR_WHITE_BG_RED },
  { LWROC_TRIVA_FAIL_STHR_BUG_FATAL,     "B", "BugFatal", CTR_WHITE_BG_RED },
  /* Bus status codes, one letter. */
  { LWROC_TRIVA_BUS_STATUS_NONE,         "-", "-",        CTR_NONE },
  { LWROC_TRIVA_BUS_STATUS_WAIT,         "W", "Waiting",  CTR_WHITE_BG_RED },
  { LWROC_TRIVA_BUS_STATUS_DEADTIME,     "D", "Deadtime", CTR_WHITE_BG_RED },
  { LWROC_TRIVA_BUS_STATUS_FAIL,         "F", "Failed",   CTR_WHITE_BG_RED },

  { 0,                                   "?", "?",        CTR_RED_BG_GREEN },
};

lwroc_status_format *lwroc_get_status_format(uint32_t status)
{
  lwroc_status_format *p = _lwroc_status_format;

  for  ( ; p->status; p++)
    {
      if (p->status == status)
	break;
    }

  return p;
}

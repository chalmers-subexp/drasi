/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "../dtc_arch/acc_def/has_curses.h"

#include "lwroc_mon.h"

#if HAS_CURSES
#define USE_CURSES 1

#include "lwroc_hostname_util.h"
#include "lwroc_message.h"
#include "lwroc_timestamp_marks.h"
#include "lwroc_net_legacy.h"
#include "lwroc_leap_seconds.h"

#include "gen/lwroc_monitor_main_source_serializer.h"
#include "gen/lwroc_monitor_net_source_serializer.h"
#include "gen/lwroc_monitor_file_source_serializer.h"
#include "gen/lwroc_monitor_filter_source_serializer.h"
#include "gen/lwroc_monitor_in_source_serializer.h"
#include "gen/lwroc_monitor_analyse_source_serializer.h"
#include "gen/lwroc_monitor_dams_source_serializer.h"

#include "gen/lwroc_monitor_main_block_serializer.h"
#include "gen/lwroc_monitor_net_block_serializer.h"
#include "gen/lwroc_monitor_file_block_serializer.h"
#include "gen/lwroc_monitor_filter_block_serializer.h"
#include "gen/lwroc_monitor_in_block_serializer.h"
#include "gen/lwroc_monitor_analyse_block_serializer.h"
#include "gen/lwroc_monitor_dams_block_serializer.h"

#include "../lu_common/colourtext.h"

#include <assert.h>
#include <limits.h>
#include <math.h>

#include "../dtc_arch/acc_def/fall_through.h"

/* #define DEBUG_TREE 1 */

int _num_source_update = 0;
int _num_block_update = 0;

/* First figure out which systems exist, and how they are connected.
 *
 * Then sort them to arrange them on the screen.
 *
 * Finally assign coordinates.
 */

struct lwroc_mon_tree_item_t;
typedef struct lwroc_mon_tree_item_t lwroc_mon_tree_item;

/* This item describes one (wished for) connection.
 */

#define LWROC_MTCD_IN    0
#define LWROC_MTCD_OUT   1
#define LWROC_MTCD_NUM   2
#define LWROC_MTCD_NONE -1

typedef struct lwroc_mon_tree_conn_t
{
  /* List of all conns.  (Used for freeing.) */
  pd_ll_item _conns;

  struct
  {
    /* This is the address that it (wants) to be connected to. */
    struct sockaddr_storage _addr;
    /* This is the actual item that we connect to. */
    lwroc_mon_tree_item *_tree_item;
    /* This is the index of our connection. */
    /*uint32_t _connb;*/

    /* The line where we are shown. */
    int _lineoff;

    lwroc_monitor_conn_source *_conn_source;
    lwroc_monitor_conn_block *_conn_block;
    lwroc_monitor_conn_block *_conn_block_prev;

    lwroc_format_diff_info _diff_info;

    /* If we are part of a list. */
    pd_ll_item _conns;
  } d[LWROC_MTCD_NUM];

  uint64_t _timesort_src_index_mask;

} lwroc_mon_tree_conn;

PD_LL_SENTINEL(_lwroc_mon_tree_conns);

typedef struct lwroc_mon_tree_item_line_t
{
  int y_top, y_bottom, x;
  short color;

} lwroc_mon_tree_item_line;

void lwroc_line_extents(lwroc_mon_tree_item_line *line, int y)
{
  if (y < line->y_top)
    line->y_top = y;
  if (y > line->y_bottom)
    line->y_bottom = y;
}

/* Time tracking kinds and common time scale. */

#define LWROC_TRACK_TIMESCALE_UNKNOWN  0
#define LWROC_TRACK_TIMESCALE_1G_TAI   1
#define LWROC_TRACK_TIMESCALE_1G_UTC   2
#define LWROC_TRACK_TIMESCALE_LOCAL    3

typedef struct lwroc_mon_tree_timescale_t
{
  int             _kind;

  uint64_t        _ref_time_stamp;
  lwroc_item_time _ref_time;
  double          _dydx;

} lwroc_mon_tree_timescale;

lwroc_mon_tree_timescale _lwroc_mon_global_timescale;

/* This item describes one line.  It can either a readout node or
 * a event builder / time sorter.
 */

#define LWROC_MON_TREE_SYS_KIND_MASTER     1
#define LWROC_MON_TREE_SYS_KIND_SLAVE      2
#define LWROC_MON_TREE_SYS_KIND_UNTRIG     3
#define LWROC_MON_TREE_SYS_KIND_MERGE      4
#define LWROC_MON_TREE_SYS_KIND_MERGE_EB   5
#define LWROC_MON_TREE_SYS_KIND_MERGE_TS   6
#define LWROC_MON_TREE_SYS_KIND_TRANS_SRC  7  /* A pull source; dest conn(). */
#define LWROC_MON_TREE_SYS_KIND_PUSH_SRC   8  /* A push source; src conn(). */
#define LWROC_MON_TREE_SYS_KIND_FILE_SRC   9
#define LWROC_MON_TREE_SYS_KIND_FNET_SRC   10
#define LWROC_MON_TREE_SYS_KIND_OMITTED    11

typedef struct lwroc_mon_tree_item_dupl_t
{
  /* List of all duplicate items. */
  pd_ll_item _items;

  lwrocmon_conn *_us;

  struct sockaddr_storage _addr;

} lwroc_mon_tree_item_dupl;

struct lwroc_mon_tree_item_t
{
  /* List of all items. */
  pd_ll_item _items;

  /* Graphics info. */
  int _line;
  int _indent;

  int _filter_lineoff;
  int _filerate_lineoff;
  int _filewr_lineoff;
  int _ana_ts_lineoff;
  int _dam_lineoff;

  lwroc_mon_tree_item_line _line_bus;
  lwroc_mon_tree_item_line _line_data;

  /* Which round were we placed? */
  int _placed_round;

  /* Who are we for? */
  lwrocmon_conn *_us;

  struct sockaddr_storage _addr;

  PD_LL_SENTINEL_ITEM(_duplicates);

  /* If we have a trigbus ctrl item. */
  lwroc_mon_tree_conn *_system;

  /* If we have a filter stage. */
  lwroc_mon_tree_conn *_filter;

  uint32_t _sys_kind;

  const char *_ext_hostname;
  const char *_ext_label;

  /* For label. */
  lwroc_monitor_main_source *_main_source;
  /* If we have a timestamp calibration. */
  lwroc_monitor_main_block  *_main_block;

  lwroc_monitor_filter_source *_filter_source;
  lwroc_monitor_filter_block  *_filter_block;

  lwroc_monitor_net_block  *_net_block;
  lwroc_monitor_net_block  *_net_block_prev;

  lwroc_monitor_file_block *_file_block;
  lwroc_monitor_file_block *_file_block_prev;

  uint32_t _num_filewr;
  lwroc_monitor_filewr_block *_filewr_block;
  lwroc_monitor_filewr_block *_filewr_block_prev;

  lwroc_format_diff_info _file_diff_info[2];

  /* Do we actually use these two data? */
  lwroc_monitor_analyse_block *_analyse_block;
  lwroc_monitor_analyse_block *_analyse_block_prev;

  uint32_t _num_ana_ts;
  lwroc_monitor_ana_ts_block *_ana_ts_block;
  lwroc_monitor_ana_ts_block *_ana_ts_block_prev;

  lwroc_monitor_dams_block *_dams_block;
  lwroc_monitor_dams_block *_dams_block_prev;

  uint32_t _num_dam;
  lwroc_monitor_dam_block *_dam_block;
  lwroc_monitor_dam_block *_dam_block_prev;

  /* If we are a slave, this is the master (multiple for fake connections): */
  PD_LL_SENTINEL_ITEM(_masters);

  /* If we are a master, these are the slaves. */
  PD_LL_SENTINEL_ITEM(_slaves);

  /* If we are an event builder, this is the master (mult for fake): */
  PD_LL_SENTINEL_ITEM(_eb_masters);

  /* If we are a master, these are the event builders. */
  PD_LL_SENTINEL_ITEM(_ebs);

  /* If we have an data output, this is the destination. */
  PD_LL_SENTINEL_ITEM(_data_dests);

  /* If we have data inputs. */
  PD_LL_SENTINEL_ITEM(_data_srcs);

  /* Message rate printing. */
  lwroc_format_diff_info _msg_rate_diff_info;
};

#define LWROC_MSG_RATE_IGNORE_QUOTA_MAX        20
#define LWROC_MSG_RATE_IGNORE_QUOTA_THRESHOLD   5  /* Show messages when
						    * below.
						    */

PD_LL_SENTINEL(_lwroc_mon_tree_items);

lwroc_mon_tree_item *lwroc_mon_tree_item_alloc(void)
{
  lwroc_mon_tree_item *tree_item;

  tree_item = malloc (sizeof (lwroc_mon_tree_item));

  if (!tree_item)
    LWROC_FATAL("Memory allocation error.\n");

  /*fprintf (stderr,"alloc tree_item: %p\n", tree_item);*/

  memset (tree_item, 0, sizeof (*tree_item));

  tree_item->_line_bus.x        = -1;
  tree_item->_line_bus.y_top    = INT_MAX;
  tree_item->_line_bus.y_bottom = INT_MIN;

  tree_item->_line_data.x        = -1;
  tree_item->_line_data.y_top    = INT_MAX;
  tree_item->_line_data.y_bottom = INT_MIN;

  PD_LL_INIT(&tree_item->_duplicates);
  PD_LL_INIT(&tree_item->_masters);
  PD_LL_INIT(&tree_item->_slaves);
  PD_LL_INIT(&tree_item->_eb_masters);
  PD_LL_INIT(&tree_item->_ebs);
  PD_LL_INIT(&tree_item->_data_dests);
  PD_LL_INIT(&tree_item->_data_srcs);

  PD_LL_ADD_BEFORE(&_lwroc_mon_tree_items, &tree_item->_items);

  /* tree_item->_us->_msg_rate_ignore_quota = LWROC_MSG_RATE_IGNORE_QUOTA_MAX;
   */

  return tree_item;
}

lwroc_mon_tree_item_dupl *
lwroc_mon_tree_item_dupl_alloc(lwroc_mon_tree_item *tree_item)
{
  lwroc_mon_tree_item_dupl *tree_item_dupl;

  tree_item_dupl = malloc (sizeof (lwroc_mon_tree_item_dupl));

  if (!tree_item_dupl)
    LWROC_FATAL("Memory allocation error.\n");

  memset (tree_item_dupl, 0, sizeof (*tree_item_dupl));

  PD_LL_ADD_BEFORE(&tree_item->_duplicates, &tree_item_dupl->_items);

  return tree_item_dupl;
}

int lwroc_compare_tree_item_sockaddr(lwroc_mon_tree_item *item,
				     struct sockaddr_storage *addr)
{
  pd_ll_item *iter;

  if (lwroc_compare_sockaddr(&item->_addr, addr))
    return 1;

  /* Also check the duplicates, they may have the sought address. */
  PD_LL_FOREACH(item->_duplicates, iter)
    {
      lwroc_mon_tree_item_dupl *dupl =
	PD_LL_ITEM(iter, lwroc_mon_tree_item_dupl, _items);

      if (lwroc_compare_sockaddr(&dupl->_addr, addr))
	return 1;
    }

  return 0;
}

lwroc_mon_tree_item *lwroc_find_mon_tree_item(struct sockaddr_storage *addr)
{
  pd_ll_item *iter;
  uint16_t port;

  port = lwroc_get_port(addr);

  /* Do not match if the port is 0. */
  /* A cheating way to not match for 0.0.0.0:0 addresses. */
  if (port == 0)
    return NULL;

  /* Go through the list of known items. */
  PD_LL_FOREACH(_lwroc_mon_tree_items, iter)
    {
      lwroc_mon_tree_item *item =
	PD_LL_ITEM(iter, lwroc_mon_tree_item, _items);

      if (lwroc_compare_tree_item_sockaddr(item, addr))
	return item;
    }
  return NULL;
}

#if DEBUG_TREE
void lwroc_dump_mon_tree_item(pd_ll_item *iter2, const char *kind, int d)
{
  lwroc_mon_tree_conn *conn =
    PD_LL_ITEM(iter2, lwroc_mon_tree_conn, d[d]._conns);

  fprintf (stderr,"%-10s {%p}   (:%d) [%p] (:%d) [%p]\n",
	   kind,
	   conn,
	   lwroc_get_port(&conn->d[LWROC_MTCD_IN]._addr),
	   conn->d[LWROC_MTCD_IN]._tree_item,
	   lwroc_get_port(&conn->d[LWROC_MTCD_OUT]._addr),
	   conn->d[LWROC_MTCD_OUT]._tree_item);
}

void lwroc_dump_mon_tree_items(void)
{
  pd_ll_item *iter;
  pd_ll_item *iter2;

  fprintf (stderr,"--------------------------------------\n");
  PD_LL_FOREACH(_lwroc_mon_tree_items, iter)
    {
      lwroc_mon_tree_item *item =
	PD_LL_ITEM(iter, lwroc_mon_tree_item, _items);

#if DEBUG_TREE
      fprintf (stderr,"...... ");
      if (item->_us)
	fprintf (stderr,":%d", lwroc_get_port(&item->_addr));
      fprintf (stderr," ...... %p", item);
      fprintf (stderr," (%s) k:%d eh:%s el:%s\n",
	       item->_us && item->_us->_hostname ? item->_us->_hostname : "-",
	       item->_sys_kind, item->_ext_hostname, item->_ext_label);
#endif

      PD_LL_FOREACH(item->_masters, iter2)
	lwroc_dump_mon_tree_item(iter2, "master", LWROC_MTCD_OUT);

      PD_LL_FOREACH(item->_slaves, iter2)
	lwroc_dump_mon_tree_item(iter2, "slave", LWROC_MTCD_IN);

      PD_LL_FOREACH(item->_eb_masters, iter2)
	lwroc_dump_mon_tree_item(iter2, "eb_master", LWROC_MTCD_OUT);

      PD_LL_FOREACH(item->_ebs, iter2)
	lwroc_dump_mon_tree_item(iter2, "eb", LWROC_MTCD_IN);

      PD_LL_FOREACH(item->_data_dests, iter2)
	lwroc_dump_mon_tree_item(iter2, "ddest", LWROC_MTCD_IN);

      PD_LL_FOREACH(item->_data_srcs, iter2)
	lwroc_dump_mon_tree_item(iter2, "dsrc", LWROC_MTCD_OUT);
    }
  fprintf (stderr,"--------------------------------------\n");
}
#endif

typedef struct lwroc_ts_label_info_t
{
  const char *_label;
  int         _ambiguity;
} lwroc_ts_label_info;

int lwroc_find_ts_id_label(lwroc_mon_tree_item *item,
			   uint32_t ts_id,
			   lwroc_ts_label_info *label);

int lwroc_find_ts_id_label_conn(lwroc_mon_tree_item *item,
				lwroc_mon_tree_conn *conn,
				uint32_t ts_id,
				lwroc_ts_label_info *label)
{
  lwroc_mon_tree_item *src_item = conn->d[LWROC_MTCD_IN]._tree_item;
  int hits = 0;
  const char *src_label = NULL;

  (void) item;

  /* fprintf (stderr, "lwroc_find_ts_id_label_conn : %04x\n", ts_id); */

  if (!src_item)
    return 0;

  if (src_item->_main_source)
    src_label = src_item->_main_source->_label;

  if (!src_label)
    src_label = src_item->_ext_label;

  if (src_label &&
      conn->d[LWROC_MTCD_OUT]._conn_block &&
      conn->d[LWROC_MTCD_OUT]._conn_block->_timestamp)
    {
      if (conn->_timesort_src_index_mask & (((uint64_t) 1) << ts_id))
	{
	  /* We found a match. */

	  uint64_t src_index_mask = conn->_timesort_src_index_mask;

	  label->_label = src_label;
	  label->_ambiguity = 0;

	  /* Count the number of IDs at this point. */
	  /* We'd prefer it to be just 1. */
	  while (src_index_mask)
	    {
	      label->_ambiguity += !!(src_index_mask & 1);
	      src_index_mask >>= 1;
	    }

	  hits = 1;
	}
    }

  {
    lwroc_ts_label_info parent_label;

    parent_label._label = NULL;
    parent_label._ambiguity = 0;

    /* Search recursively. */
    if (lwroc_find_ts_id_label(src_item, ts_id, &parent_label))
      {
	/* A parent label is better if we do not have one,
	 * or if its ambiguity level is the same or smaller.
	 * We also pick for the same, since presumably the parent
	 * is closer to the source and thus has a more precise label.
	 */
	if (label->_label == NULL ||
	    parent_label._ambiguity <= label->_ambiguity)
	  *label = parent_label;

	hits = 1;
      }
  }

  /* fprintf (stderr, "lwroc_find_ts_id_label_conn : hits %d\n", hits); */

  return hits;
}

int lwroc_find_ts_id_label(lwroc_mon_tree_item *item,
			   uint32_t ts_id,
			   lwroc_ts_label_info *label)
{
  pd_ll_item *iter;
  int hits = 0;

  /* fprintf (stderr, "lwroc_find_ts_id_label : %04x\n", ts_id); */

  /* We want to find a label associated with the ID.
   *
   * Search all sources, and if any of them has the ID identified use
   * the label.  A source may have several IDs (in case it is a
   * middle-layer time sorter).  If so, then try to search further
   * upstream, hopefully becoming more uniqie.
   */

  PD_LL_FOREACH(item->_data_srcs, iter)
    {
      lwroc_mon_tree_conn *conn =
	PD_LL_ITEM(iter, lwroc_mon_tree_conn, d[LWROC_MTCD_OUT]._conns);

      hits += lwroc_find_ts_id_label_conn(item, conn, ts_id, label);

      /*
      fprintf (stderr, "iter -> \"%s\" amb %d hits %d\n",
	       label->_label, label->_ambiguity, hits);
      */
    }

  if (hits > 1)
    {
      /* We found hits at two sources - ambiguous. */
      /* TODO: check if the labels are the same? */
      label->_label = "?";
    }

  return hits;
}

void lwroc_connect_mon_tree_item(lwroc_mon_tree_item *item,
				 pd_ll_item *iter2, const char *kind,
				 int d_this,
				 size_t offset_other)
{
  lwroc_mon_tree_conn *conn;
  lwroc_mon_tree_item *find_item;
  int d_find = !d_this;

  conn = PD_LL_ITEM(iter2, lwroc_mon_tree_conn, d[d_this]._conns);

  assert(conn->d[d_this]._tree_item == item);
  assert(conn->d[d_this]._tree_item);
  assert(!conn->d[d_find]._tree_item);

  find_item = lwroc_find_mon_tree_item(&conn->d[d_this]._addr);

  (void) kind;
#if DEBUG_TREE
  fprintf (stderr,"connect: %-7s (:%d) -> %p",
	   kind, lwroc_get_port(&conn->d[d_this]._addr),
	   find_item);
#endif

  if (find_item)
    {
      pd_ll_item *iter3;

      pd_ll_item *sentinel =
	(pd_ll_item *) (((char *) find_item) + offset_other);

      PD_LL_FOREACH(*sentinel, iter3)
	{
	  lwroc_mon_tree_conn *conn2 =
	    PD_LL_ITEM(iter3, lwroc_mon_tree_conn, d[d_find]._conns);

	  (void) conn2;

	  if (lwroc_compare_tree_item_sockaddr(item,
					       &conn2->d[d_find]._addr))
	    {
	      /* conn and conn2 are the same connection.
	       * We now want to merge them, i.e. free one of them.
	       * They are both parts of lists, so we also have to
	       * replace the removed one with the kept one in that
	       * list.
	       *
	       * Remove conn2, keep conn.
	       */
#if 1
	      assert(PD_LL_IS_EMPTY(&conn->d[d_find]._conns));
	      assert(!PD_LL_IS_EMPTY(&conn2->d[d_find]._conns));

	      PD_LL_REPLACE(&conn2->d[d_find]._conns, &conn->d[d_find]._conns);
	      /*PD_LL_REMOVE(&conn2->d[d_find]._conns);*/

	      assert(!conn->d[d_find]._tree_item);
	      assert(!conn->d[d_find]._conn_block);

	      assert(conn2->d[d_find]._tree_item);
	      assert(conn2->d[d_find]._conn_block);

	      conn->d[d_find]._addr        = conn2->d[d_find]._addr;
	      conn->d[d_find]._tree_item   = conn2->d[d_find]._tree_item;
	      conn->d[d_find]._conn_source = conn2->d[d_find]._conn_source;
	      conn->d[d_find]._conn_block  = conn2->d[d_find]._conn_block;
	      conn->d[d_find]._conn_block_prev =
		conn2->d[d_find]._conn_block_prev;
#endif
#if DEBUG_TREE
	      fprintf (stderr," *  { %p %p }",
		       conn->d[d_find]._conn_block,
		       conn2->d[d_find]._conn_block);
#endif
	      break;
	    }
	}
    }
#if DEBUG_TREE
  fprintf (stderr,"\n");
#endif
#if DEBUG_TREE
  lwroc_dump_mon_tree_items();
#endif
}

void lwroc_missing_mon_tree_item(lwroc_mon_tree_item *item,
				 pd_ll_item *iter2, const char *kind,
				 int d_this,
				 size_t offset_other,
				 int added_why)
{
  lwroc_mon_tree_conn *conn;
  lwroc_mon_tree_item *find_item;
  int d_find = !d_this;

  conn = PD_LL_ITEM(iter2, lwroc_mon_tree_conn, d[d_this]._conns);

#if DEBUG_TREE
  fprintf (stderr,"missing: %-7s\n",
	   kind);
#endif

  if (conn->d[d_find]._tree_item)
    return;

  assert(conn->d[d_this]._tree_item == item);
  assert(conn->d[d_this]._tree_item);
  assert(!conn->d[d_find]._tree_item);

  find_item = lwroc_find_mon_tree_item(&conn->d[d_this]._addr);

  if (!find_item)
    {
      uint32_t sys_kind = 0;
      const char *ext_hostname = NULL;
      const char *ext_label = NULL;

      /* Make sure we try to connect to the system. */

      if (conn->d[d_this]._conn_source &&
	  (conn->d[d_this]._conn_source->_type == LWROC_CONN_TYPE_TRANS ||
	   conn->d[d_this]._conn_source->_type == LWROC_CONN_TYPE_FNET ||
	   conn->d[d_this]._conn_source->_type == LWROC_CONN_TYPE_STREAM ||
	   conn->d[d_this]._conn_source->_type == LWROC_CONN_TYPE_EBYE_PUSH ||
	   conn->d[d_this]._conn_source->_type == LWROC_CONN_TYPE_EBYE_PULL ||
	   conn->d[d_this]._conn_source->_type == LWROC_CONN_TYPE_FILE))
	{
	  /* If it is a transport or stream server, or file,
	   * we do not expect to find it.
	   */
	  if (d_this == LWROC_MTCD_IN)
	    return; /* Do not make a dummy for destinations. */
	  if (conn->d[d_this]._conn_source->_type == LWROC_CONN_TYPE_FNET)
	    sys_kind = LWROC_MON_TREE_SYS_KIND_FNET_SRC;
	  else if (conn->d[d_this]._conn_source->_type == LWROC_CONN_TYPE_FILE)
	    sys_kind = LWROC_MON_TREE_SYS_KIND_FILE_SRC;
	  else if (conn->d[d_this]._conn_source->_type ==
		   LWROC_CONN_TYPE_EBYE_PUSH)
	    sys_kind = LWROC_MON_TREE_SYS_KIND_PUSH_SRC;
	  else
	    sys_kind = LWROC_MON_TREE_SYS_KIND_TRANS_SRC;
	  ext_hostname = conn->d[d_this]._conn_source->_hostname;
	  ext_label = conn->d[d_this]._conn_source->_label;
	}
      else
	{
	  if (((_config._no_down_up & LWROCMON_CONN_ADDED_SEARCH_UP) &&
	       added_why == LWROCMON_CONN_ADDED_SEARCH_UP) ||
	      ((_config._no_down_up & LWROCMON_CONN_ADDED_SEARCH_DOWN) &&
	       added_why == LWROCMON_CONN_ADDED_SEARCH_DOWN) ||
	      ((_config._no_down_up & LWROCMON_CONN_ADDED_SEARCH_DOWN_UP) &&
	       added_why == LWROCMON_CONN_ADDED_SEARCH_DOWN_UP))
	    {
	      /* We add the dummy system still. */
	      sys_kind = LWROC_MON_TREE_SYS_KIND_OMITTED;
	    }
	  else
	    {
	      lwrocmon_add_mon_host(NULL, &conn->d[d_this]._addr, added_why);

	      /* TODO: this system should be marked for removal if connection
	       * is lost.  When doing that, we then also have to mark such
	       * systems above (in the find_item) that they are still needed,
	       * so not so urgent with removal.
	       */
	    }
	}

      /* Create a dummy system. */

      find_item = lwroc_mon_tree_item_alloc();

      find_item->_addr = conn->d[d_this]._addr;

      /* fprintf (stderr, "AUTODISCOVERED!\n"); */

      /* TODO: This is kind of a hacky way to give the system kind... */
      if (sys_kind)
	find_item->_sys_kind = sys_kind;
      if (ext_hostname)
	find_item->_ext_hostname = ext_hostname;
      if (ext_label)
	find_item->_ext_label = ext_label;
    }

  /* Add the connection to the end of the relevant list. */
  {
    pd_ll_item *sentinel =
      (pd_ll_item *) (((char *) find_item) + offset_other);

    PD_LL_ADD_BEFORE(sentinel, &conn->d[d_find]._conns);

    conn->d[d_find]._tree_item = find_item;
  }

  if (find_item->_us)
    find_item->_us->_added_why |= added_why;

  (void) kind;
}

void lwroc_connect_mon_tree_items(void)
{
  pd_ll_item *iter;
  pd_ll_item *iter2;

#if DEBUG_TREE
  lwroc_dump_mon_tree_items();
#endif

  /* Go through all items, and all connections, and try to find the
   * corresponding connection.
   */
  PD_LL_FOREACH(_lwroc_mon_tree_items, iter)
    {
      lwroc_mon_tree_item *item =
	PD_LL_ITEM(iter, lwroc_mon_tree_item, _items);

#if DEBUG_TREE
      fprintf (stderr,"...... ");
      if (item->_us)
	fprintf (stderr,":%d", lwroc_get_port(&item->_addr));
      fprintf (stderr," ...... %p\n", item);
#endif

      PD_LL_FOREACH(item->_masters, iter2)
	lwroc_connect_mon_tree_item(item, iter2, "master", LWROC_MTCD_OUT,
				    offsetof(lwroc_mon_tree_item, _slaves));

      PD_LL_FOREACH(item->_eb_masters, iter2)
	lwroc_connect_mon_tree_item(item, iter2, "eb_master", LWROC_MTCD_OUT,
				    offsetof(lwroc_mon_tree_item, _ebs));

      PD_LL_FOREACH(item->_data_dests, iter2)
	lwroc_connect_mon_tree_item(item, iter2, "ddest", LWROC_MTCD_IN,
				    offsetof(lwroc_mon_tree_item, _data_srcs));
    }

#if DEBUG_TREE
  lwroc_dump_mon_tree_items();
#endif

  /* Then go through all items again, and find any still missing
   * connections.
   */
  PD_LL_FOREACH(_lwroc_mon_tree_items, iter)
    {
      lwroc_mon_tree_item *item =
	PD_LL_ITEM(iter, lwroc_mon_tree_item, _items);

      int added_up   = LWROCMON_CONN_ADDED_SEARCH_UP;
      int added_down = LWROCMON_CONN_ADDED_SEARCH_DOWN;

      if (item->_us &&
	  !(item->_us->_added_why & (LWROCMON_CONN_ADDED_PRIMARY |
				     LWROCMON_CONN_ADDED_SEARCH_UP)))
	added_up = LWROCMON_CONN_ADDED_SEARCH_DOWN_UP;

      if (item->_us &&
	  !(item->_us->_added_why & (LWROCMON_CONN_ADDED_PRIMARY |
				     LWROCMON_CONN_ADDED_SEARCH_DOWN)))
	added_down = LWROCMON_CONN_ADDED_SEARCH_UP_DOWN;

      PD_LL_FOREACH(item->_masters, iter2)
	lwroc_missing_mon_tree_item(item, iter2, "master", LWROC_MTCD_OUT,
				    offsetof(lwroc_mon_tree_item, _slaves),
				    added_up /* sibling? */);
      PD_LL_FOREACH(item->_slaves, iter2)
	lwroc_missing_mon_tree_item(item, iter2, "slave", LWROC_MTCD_IN,
				    offsetof(lwroc_mon_tree_item, _masters),
				    added_down /* sibling? */);

      PD_LL_FOREACH(item->_eb_masters, iter2)
	lwroc_missing_mon_tree_item(item, iter2, "eb_master", LWROC_MTCD_OUT,
				    offsetof(lwroc_mon_tree_item, _ebs),
				    added_up);
      PD_LL_FOREACH(item->_ebs, iter2)
	lwroc_missing_mon_tree_item(item, iter2, "ebs", LWROC_MTCD_IN,
				    offsetof(lwroc_mon_tree_item, _eb_masters),
				    added_down);

      PD_LL_FOREACH(item->_data_dests, iter2)
	lwroc_missing_mon_tree_item(item, iter2, "dests", LWROC_MTCD_IN,
				    offsetof(lwroc_mon_tree_item, _data_srcs),
				    added_down);
      PD_LL_FOREACH(item->_data_srcs, iter2)
	lwroc_missing_mon_tree_item(item, iter2, "srcs", LWROC_MTCD_OUT,
				    offsetof(lwroc_mon_tree_item, _data_dests),
				    added_up);
    }

#if DEBUG_TREE
  lwroc_dump_mon_tree_items();
#endif

#if DEBUG_TREE
  fprintf (stderr,"\n\n");
#endif
}

void lwroc_sys_kind_mon_tree_item(lwroc_mon_tree_item *item)
{
  if (item->_system)
    {
      lwroc_monitor_conn_source *conn_source =
	item->_system->d[LWROC_MTCD_OUT]._conn_source;

      if (conn_source->_kind == LWROC_CONN_KIND_TRIVA ||
	  conn_source->_kind == LWROC_CONN_KIND_TRIMI)
	{
	  if (conn_source->_kind_sub == LWROC_CONN_KIND_SUB_MASTER)
	    item->_sys_kind = LWROC_MON_TREE_SYS_KIND_MASTER;
	  if (conn_source->_kind_sub == LWROC_CONN_KIND_SUB_SLAVE)
	    item->_sys_kind = LWROC_MON_TREE_SYS_KIND_SLAVE;
	}
      if (conn_source->_kind == LWROC_CONN_KIND_UNTRIG)
	item->_sys_kind = LWROC_MON_TREE_SYS_KIND_UNTRIG;
      if (conn_source->_kind == LWROC_CONN_KIND_EB)
	item->_sys_kind = LWROC_MON_TREE_SYS_KIND_MERGE_EB;
      if (conn_source->_kind == LWROC_CONN_KIND_TS)
	item->_sys_kind = LWROC_MON_TREE_SYS_KIND_MERGE_TS;

      /*printf ("/%08x %08x/\n", conn_source->_kind, conn_source->_kind_sub);*/
    }
  if (!item->_sys_kind)
    {
      /* If we have no definite information about system kind, we take
       * a guess.
       */
      if (PD_LL_IS_EMPTY(&item->_data_srcs))
	{
	  if (PD_LL_IS_EMPTY(&item->_masters))
	    item->_sys_kind = LWROC_MON_TREE_SYS_KIND_MASTER;
	  else
	    item->_sys_kind = LWROC_MON_TREE_SYS_KIND_SLAVE;
	}
      else
	item->_sys_kind = LWROC_MON_TREE_SYS_KIND_MERGE;
    }
}

int lwroc_indent_extra_mon_tree_item(lwroc_mon_tree_item *item)
{
  if (item->_sys_kind == LWROC_MON_TREE_SYS_KIND_MASTER ||
      item->_sys_kind == LWROC_MON_TREE_SYS_KIND_UNTRIG ||
      item->_sys_kind == LWROC_MON_TREE_SYS_KIND_TRANS_SRC ||
      item->_sys_kind == LWROC_MON_TREE_SYS_KIND_PUSH_SRC ||
      item->_sys_kind == LWROC_MON_TREE_SYS_KIND_FILE_SRC ||
      item->_sys_kind == LWROC_MON_TREE_SYS_KIND_FNET_SRC)
    return 2;
  return 0;
}

void lwroc_indent_mon_tree_item(lwroc_mon_tree_item *item, int indent,
				int depth)
{
  pd_ll_item *iter;

  if (!item)
    return;

  /* Protect against recursion. */
  if (depth > 20)
    return;

  if (item->_indent < indent)
    item->_indent = indent;

  PD_LL_FOREACH(item->_slaves, iter)
    {
      lwroc_mon_tree_conn *conn =
	PD_LL_ITEM(iter, lwroc_mon_tree_conn, d[LWROC_MTCD_IN]._conns);

      lwroc_indent_mon_tree_item(conn->d[LWROC_MTCD_OUT]._tree_item,
				 item->_indent+2, depth+1);
    }

  PD_LL_FOREACH(item->_data_dests, iter)
    {
      lwroc_mon_tree_conn *conn =
	PD_LL_ITEM(iter, lwroc_mon_tree_conn, d[LWROC_MTCD_IN]._conns);
      int add = 2;

      /* This fixes up such that an EB or TS of a master which has no
       * slaves has the same indent as if there would have been some
       * slave in-between, which would have pushed the EB indent.
       */
      add += lwroc_indent_extra_mon_tree_item(item);

      lwroc_indent_mon_tree_item(conn->d[LWROC_MTCD_OUT]._tree_item,
				 item->_indent + add, depth+1);
    }
}

/* Basis for item location: item->_line
 *                          item->_filter_lineoff    (fo)
 *                          item->_filerate_lineoff  (fr)
 *                          item->_filewr_lineoff    (fw)
 *                          item->_ana_ts_lineoff    (ta (< 0))
 *
 *               Center-location
 *
 * ta:   TS-ANALYSIS.............................................
 * -1:                  ,------------- (connection line (if any))
 *  0:           NODENAME       rate/buffer
 * +1:           ts_offset      ts_track
 * +2:           (ts_offset raw)
 * +3:           (ts debug I)
 * +4:           (ts debug II)
 * +5:           (ts debug III)
 * fo:           FILTER
 * fr:           file-rate
 *
 *      Left-location (connections to masters)
 *
 *  0:   conn-to-master...
 *  n-1: conn-to-eb-master (i.e. from EB to master)
 *
 *                                          Right-location
 *
 *  0:    [conn->...._lineoff]              Dest-conn...
 *                                 WR_ID      (ts_offset)
 *  n-1:                                    Dest-conn
 *
 *  Cover full width
 *
 * fw: File status and name...
 *
 */

void lwroc_place_mon_tree_item(lwroc_mon_tree_item *item, int *line,
			       int *pending_space, int iround, int depth)
{
  pd_ll_item *iter;
  int num_master_lines = 0;
  int num_dest_lines2[2] = { 0, 0 };  /* 0: from main, 1: from filter */
  int num_center_lines = 1; /* timestamp track and file write global */
  int num_lines;
  int num_ana_ts_lines = 0;
  int num_dam_lines = 0;

  lwroc_mon_tree_conn *conn_data_src[64];
  int num_conn_data_src = 0;

  if (item->_line >= 0)
    return; /* Item already placed. */
  if (depth > 20)
    return; /* Protect against recursion. */
  item->_placed_round = iround;

  PD_LL_FOREACH(item->_eb_masters, iter)
    {
      lwroc_mon_tree_conn *conn =
	PD_LL_ITEM(iter, lwroc_mon_tree_conn, d[LWROC_MTCD_OUT]._conns);

      if (conn->d[LWROC_MTCD_IN]._tree_item)
	lwroc_place_mon_tree_item(conn->d[LWROC_MTCD_IN]._tree_item,
				  line, pending_space, iround, depth+1);
      num_master_lines++;
    }

  PD_LL_FOREACH(item->_masters, iter)
    {
      lwroc_mon_tree_conn *conn =
	PD_LL_ITEM(iter, lwroc_mon_tree_conn, d[LWROC_MTCD_OUT]._conns);

      if (conn->d[LWROC_MTCD_IN]._tree_item)
	lwroc_place_mon_tree_item(conn->d[LWROC_MTCD_IN]._tree_item,
				  line, pending_space, iround, depth+1);
      num_master_lines++;
    }

  PD_LL_FOREACH(item->_data_srcs, iter)
    {
      lwroc_mon_tree_conn *conn =
	PD_LL_ITEM(iter, lwroc_mon_tree_conn, d[LWROC_MTCD_OUT]._conns);

      conn_data_src[num_conn_data_src++] = conn;

      if (conn->d[LWROC_MTCD_IN]._tree_item)
	lwroc_place_mon_tree_item(conn->d[LWROC_MTCD_IN]._tree_item,
				  line, pending_space, iround, depth+1);

      *pending_space = 1;
    }

  if (*pending_space)
    {
      (*line)++;
      *pending_space = 0;
    }

  PD_LL_FOREACH(item->_data_dests, iter)
    {
      lwroc_mon_tree_conn *conn =
	PD_LL_ITEM(iter, lwroc_mon_tree_conn, d[LWROC_MTCD_IN]._conns);

      int iline2_ind = 0;  /* 1 if from filter */

      if (conn->d[LWROC_MTCD_IN]._conn_source &&
	  conn->d[LWROC_MTCD_IN]._conn_source->_kind ==
	  LWROC_CONN_KIND_SRCBUF_FILTER)
	iline2_ind = 1;

      conn->d[LWROC_MTCD_IN]._lineoff = num_dest_lines2[iline2_ind];
      /*
      fprintf (stderr, "._y = %d (%d)\n",
	       num_dest_lines2[iline2_ind], iline2_ind);
      */
      if ((_config._print_ts_track & LWROCMON_PRINT_TS_TRACK_TO_INPUT) &&
	  conn->d[LWROC_MTCD_OUT]._conn_block &&
	  conn->d[LWROC_MTCD_OUT]._conn_block->_timestamp)
	num_dest_lines2[iline2_ind]++;

      num_dest_lines2[iline2_ind]++;
    }

  if (item->_analyse_block)
    {
      uint32_t ana_tsb;

      for (ana_tsb = 0; ana_tsb < item->_num_ana_ts; ana_tsb++)
	{
	  lwroc_monitor_ana_ts_block *ana_ts_block =
	    item->_ana_ts_block + ana_tsb;

	  if (ana_ts_block->_ts_ref_off_sig_k == (uint64_t) -2)
	    continue;
	  if (_config._print_ts_analysis)
	    num_ana_ts_lines++;

	  if (ana_ts_block->_src_index_mask)
	    {
	      int idx = 0;
	      uint64_t src_index_mask = ana_ts_block->_src_index_mask;

	      while (src_index_mask)
		{
		  while (!(src_index_mask & 1))
		    {
		      idx++;
		      src_index_mask >>= 1;
		    }

		  if (idx < num_conn_data_src)
		    conn_data_src[idx]->_timesort_src_index_mask |=
		      ((uint64_t) 1) << ana_ts_block->_id;

		  idx++;
		  src_index_mask >>= 1;
		}
	    }
	}
    }

  if (item->_dams_block)
    {
      uint32_t damb;

      for (damb = 0; damb < item->_num_dam; damb++)
	{
	  lwroc_monitor_dam_block *dam_block =
	    item->_dam_block + damb;

	  if (dam_block->_id == (uint32_t) -1)
	    continue;
	  if (_config._print_dams)
	    num_dam_lines++;
	}
    }

  item->_line = (*line);

  if (item->_main_block)
    {
      int ts_lines = 0;

      if (_config._print_ts_track & LWROCMON_PRINT_TS_TRACK_RO_TRACK)
	{
	  if (item->_main_block->_ts_ref_stamp_freq_k)
	    ts_lines = 1;
	  if (item->_main_block->_last_timestamp)
	    {
	      ts_lines = 1;
	      if (_config._print_raw_ts)
		ts_lines = 2;
	    }
	}
      num_center_lines += ts_lines;
    }

  if (_config._print_debug_ts &&
      item->_main_block &&
      item->_main_block->_last_timestamp)
    num_center_lines = 7;

  if (item->_filter)
    {
      item->_filter_lineoff = num_center_lines;

      /* If the filter would start before the main pipe destinations,
       * move it down.
       */
      if (item->_filter_lineoff < num_dest_lines2[0])
	item->_filter_lineoff = num_dest_lines2[0];

      /* The filter name consumes one center line. */
      num_center_lines = item->_filter_lineoff + 1;
    }

  /* The file writer rate is placed below the center items. */
  if (item->_file_block)
    {
      item->_filerate_lineoff = num_center_lines;
      num_center_lines++;
    }

  /* We will use as many lines as the most of masters or destinations,
   * or plain center lines.
   */

  num_lines                                     = num_center_lines;
  if (num_master_lines   > num_lines) num_lines = num_master_lines;
  if (num_dest_lines2[0] > num_lines) num_lines = num_dest_lines2[0];

  if (item->_filter)
    {
      /* Does the filter destinations cause more lines to be needed. */
      if (item->_filter_lineoff + num_dest_lines2[1] > num_lines)
	num_lines = item->_filter_lineoff + num_dest_lines2[1];
    }

  /* The file write info is placed below all master/center/dest items. */
  if (item->_num_filewr)
    {
      item->_filewr_lineoff = num_lines;

      num_lines += (int) item->_num_filewr;
    }

  if (num_ana_ts_lines)
    {
      /* TS analysis is shown above main lines.  Subtract additional 1
       * for incoming connection space (in pending_space line).
       *
       * Since the incoming connection (typically) was giving some
       * whitespace (pending_space), we need to recover that too, so
       * added 1 total number of lines.
       */
      item->_ana_ts_lineoff = -(int) num_ana_ts_lines - 1;

      item->_line += (int) num_ana_ts_lines + 1;

      num_lines += (int) num_ana_ts_lines + 1;
    }

  if (num_dam_lines)
    {
      /* DAM statistics is shown at the end. */

      item->_dam_lineoff = num_lines;

      num_lines += (int) num_dam_lines + 1;
    }
  else
    item->_dam_lineoff = -1;

  (*line) += num_lines;

  lwroc_sys_kind_mon_tree_item(item);

  lwroc_indent_mon_tree_item(item, 0, 0);

  PD_LL_FOREACH(item->_slaves, iter)
    {
      lwroc_mon_tree_conn *conn =
	PD_LL_ITEM(iter, lwroc_mon_tree_conn, d[LWROC_MTCD_IN]._conns);

      if (conn->d[LWROC_MTCD_OUT]._tree_item)
	{
	  lwroc_place_mon_tree_item(conn->d[LWROC_MTCD_OUT]._tree_item,
				    line, pending_space, iround, depth+1);
	}
    }

  if (!PD_LL_IS_EMPTY(&item->_slaves))
    *pending_space = 1;

  /* Move filter destinations to start at filter line. */
  PD_LL_FOREACH(item->_data_dests, iter)
    {
      lwroc_mon_tree_conn *conn =
	PD_LL_ITEM(iter, lwroc_mon_tree_conn, d[LWROC_MTCD_IN]._conns);

      if (conn->d[LWROC_MTCD_IN]._conn_source &&
	  conn->d[LWROC_MTCD_IN]._conn_source->_kind ==
	  LWROC_CONN_KIND_SRCBUF_FILTER)
	conn->d[LWROC_MTCD_IN]._lineoff += item->_filter_lineoff;
      /*
      fprintf (stderr, "._y += (%d) => %d\n",
	       item->_filter_lineoff,
	       conn->d[LWROC_MTCD_IN]._lineoff);
      */
    }

  /* Update quota for showing of message rate.
   */

  if (item->_net_block &&
      item->_net_block_prev &&
      item->_us)
    {
      lwroc_monitor_net_block *ptr_block = item->_net_block;
      lwroc_monitor_net_block *ptr_prev = item->_net_block_prev;

      /* double a = item->_us->_msg_rate_ignore_quota; */
      double add_quota, sub_quota;
      MONITOR_STRUCT_FREQ;
      (void) frequency;

      add_quota = elapsed;

      if (add_quota < 0)
	add_quota = 0;

      /* We do not add much in case things jammed. */
      if (add_quota > 2)
	add_quota = 2;

      item->_us->_msg_rate_ignore_quota += add_quota;

      if (item->_us->_msg_rate_ignore_quota > LWROC_MSG_RATE_IGNORE_QUOTA_MAX)
	item->_us->_msg_rate_ignore_quota = LWROC_MSG_RATE_IGNORE_QUOTA_MAX;

      sub_quota = (double) (item->_net_block->_num_msg -
			    item->_net_block_prev->_num_msg);

      if (sub_quota < 0)
	sub_quota = 0;

      /* Do not subtract if previous item does not know what time it had,
       * i.e. for initial item.
       */
      if (item->_net_block_prev->_num_msg == 0 &&
	  ptr_prev->_time._sec == 0)
	sub_quota = 0;

      item->_us->_msg_rate_ignore_quota -= sub_quota;

      if (item->_us->_msg_rate_ignore_quota < 0)
	item->_us->_msg_rate_ignore_quota = 0;
      /*
      fprintf (stderr, "%s %.1f +%.1f -%.1f => %.1f\n",
	       item->_us ? item->_us->_hostname : "?",
	       a, add_quota, sub_quota,
	       item->_us->_msg_rate_ignore_quota);
      */
    }
}

void lwroc_place_mon_tree_items(void)
{
  pd_ll_item *iter;
  int iround;
  int line = 1;
  int pending_space = 0;

  /* Sorting the items is easiest from top-down, i.e. we begin by
   * finding all tree_items that have no further data outputs connected.
   * This will usually be the final time-sorter.
   *
   * Then for each such end-item we go through all eb_masters (would
   * be only one).  That one is visited first.
   * For a time-sorter, this will be nothing.
   * For an event builder, this will be the master system.
   *
   * We then go through all data sources, and place any that have not
   * already been placed.
   *
   * Then, we place ourselves.
   *
   * Then, we go through the slaves of a system, they are placed.
   *
   */

  PD_LL_FOREACH(_lwroc_mon_tree_items, iter)
    {
      lwroc_mon_tree_item *item =
	PD_LL_ITEM(iter, lwroc_mon_tree_item, _items);

      /* Mark all items as unvisited by setting line number to -1. */

      item->_line = -3;
      item->_placed_round = 0;

      /* If item has no data output, it should be handled in the outer
       * loop.
       *
       * But if item has a master, it likely is an unconnected slave.
       * To avoid that from being placed first, schedule it for a later round.
       */

      if (PD_LL_IS_EMPTY(&item->_data_dests))
	{
	  if (!PD_LL_IS_EMPTY(&item->_masters))
	    item->_line = -2;
	  else
	    item->_line = -1;
	}
    }

  for (iround = -1; iround >= -3; iround--)
    {
      PD_LL_FOREACH(_lwroc_mon_tree_items, iter)
	{
	  lwroc_mon_tree_item *item =
	    PD_LL_ITEM(iter, lwroc_mon_tree_item, _items);

	  if (item->_line == iround)
	    lwroc_place_mon_tree_item(item, &line, &pending_space, -iround, 0);
	}
    }
}

int lwroc_time_track_scale(lwroc_mon_tree_item *item)
{
  if (item->_main_block &&
      item->_main_block->_last_timestamp)
    {
      lwroc_monitor_main_block *mb = item->_main_block;

      double t_dydx = (double) mb->_ts_ref_stamp_freq_k * 1.e-3;
      double t_y0   = (double) (int64_t) mb->_ts_ref_stamp_y0_k * 1.e-3;
      double s_dydx = (double) mb->_ts_ref_stamp_sig_freq_k * 1.e-3;
      double s_y0   = (double) mb->_ts_ref_stamp_sig_y0_k * 1.e-3;

      (void) s_y0;

      /* First check if increment rate is in ns. */
      if (fabs(t_dydx - 1.e9) < 1.e9*200e-6 + 5 * s_dydx)
	{
	  int leap = lwroc_get_leap_seconds((time_t) mb->_ts_ref_time._sec);
	  int use_leap;

	  for (use_leap = 1; use_leap >= 0; use_leap--)
	    {
	      /* Then, what should the timestamp nominally be at the
	       * reference time?
	       */
	      uint64_t ref_time_stamp =
		(mb->_ts_ref_time._sec +
		 (uint64_t) (int64_t) (use_leap ? leap : 0)) * 1000000000 +
		mb->_ts_ref_time._nsec;
	      /* Calculate the difference of the actual fitted
	       * timestamp at the reference time.
	       *
	       * In total:
	       *
	       * stamp_diff = (mb->_ts_ref_stamp + t_y0) - ref_time_stamp
	       *
	       * But split, in order to do the large difference in
	       * int64_t, since double would suffer cancellation loss.
	       */
	      int64_t stamp_diff =
		(int64_t) mb->_ts_ref_stamp -
		(int64_t) ref_time_stamp;

	      double d_stamp_diff = (double) stamp_diff + t_y0;

	      /* Less than 1.5 s off.  Allowing extra 1.0 with leap. */
	      if (fabs(d_stamp_diff) < (1.5 + use_leap) * 1.e9)
		{
		  return use_leap ?
		    LWROC_TRACK_TIMESCALE_1G_TAI :
		    LWROC_TRACK_TIMESCALE_1G_UTC;
		}
	    }
	}

      if (t_dydx > 1e6)
	{
	  return LWROC_TRACK_TIMESCALE_LOCAL;
	}
    }

  return LWROC_TRACK_TIMESCALE_UNKNOWN;
}

void lwroc_combine_time_track_items_round(int iround,
					  lwroc_item_time *tnow)
{
  lwroc_mon_tree_timescale *global = &_lwroc_mon_global_timescale;
  pd_ll_item *iter;

  /* Go through all systems in this round.
   *
   * If any of them seem to match with a 1G_UTC (i.e. WR) scale,
   * then we select that as our common time.
   */

  lwroc_mon_tree_item *best_local_item = NULL;

  PD_LL_FOREACH(_lwroc_mon_tree_items, iter)
    {
      lwroc_mon_tree_item *item =
	PD_LL_ITEM(iter, lwroc_mon_tree_item, _items);

      if (item->_placed_round == iround)
	{
	  int scale = lwroc_time_track_scale(item);

	  if (scale == LWROC_TRACK_TIMESCALE_LOCAL)
	    {
	      /* For anyone that has a local timescale, keep the
	       * highest one.
	       */
	      if (!best_local_item ||
		  item->_line < best_local_item->_line)
		best_local_item = item;
	    }
	  else if (scale != LWROC_TRACK_TIMESCALE_UNKNOWN)
	    {
	      /* But any report of a 1G+TAI/UTC takes precedence. */
	      global->_kind = scale;
	      break;
	    }
	}
    }

  if (best_local_item)
    {
      /* We have found a local timescale. */

      global->_kind = LWROC_TRACK_TIMESCALE_LOCAL;

      /* Pick the reference values from the first hit / best item. */

      {
	lwroc_monitor_main_block *mb = best_local_item->_main_block;

	double t_dydx = (double) mb->_ts_ref_stamp_freq_k * 1.e-3;
	double t_y0   = (double) (int64_t) mb->_ts_ref_stamp_y0_k * 1.e-3;
	double s_dydx = (double) mb->_ts_ref_stamp_sig_freq_k * 1.e-3;
	double s_y0   = (double) mb->_ts_ref_stamp_sig_y0_k * 1.e-3;

	double d_time_diff;
	double d_time_stamp;

	(void) s_dydx;
	(void) s_y0;

	/* To combine timescales, they must be moved to the same
	 * reference time.  Use current time.
	 */

	global->_ref_time = *tnow;

	/* How much must we move the time? */

	d_time_diff =
	  (double) ((int64_t) tnow->_sec -
		    (int64_t) mb->_ts_ref_time._sec) +
	  1.e-9 * (double) ((int32_t) tnow->_nsec -
			    (int32_t) mb->_ts_ref_time._nsec);
	d_time_stamp = d_time_diff * t_dydx + t_y0;

	global->_ref_time_stamp = mb->_ts_ref_stamp;
	global->_ref_time_stamp += (uint64_t) d_time_stamp;

	global->_dydx = t_dydx;
      }
    }
}

void lwroc_data_inputs_timescale(lwroc_item_time *tnow,
				 int now_leap)
{
  lwroc_mon_tree_timescale *global = &_lwroc_mon_global_timescale;
  pd_ll_item *iter;
  pd_ll_item *iter2;

  /* No front-end has reported a timescale. */
  /* See if times reported by the timesorter inputs could
   * be compatible with a WR / TAI scale.
   */

  /* fprintf (stderr, "lwroc_data_inputs_timescale...\n"); */

  PD_LL_FOREACH(_lwroc_mon_tree_items, iter)
    {
      lwroc_mon_tree_item *item =
	PD_LL_ITEM(iter, lwroc_mon_tree_item, _items);

      /* fprintf (stderr, "a..."); */

      PD_LL_FOREACH(item->_data_dests, iter2)
	{
	  lwroc_mon_tree_conn *conn =
	    PD_LL_ITEM(iter2, lwroc_mon_tree_conn, d[LWROC_MTCD_IN]._conns);

	  /* fprintf (stderr, "b..."); */

	  if (conn->d[LWROC_MTCD_OUT]._conn_block &&
	      conn->d[LWROC_MTCD_OUT]._conn_block->_timestamp)
	    {
	      uint64_t stamp;

	      stamp = conn->d[LWROC_MTCD_OUT]._conn_block->_timestamp;
	      if (stamp >= LWROC_MERGE_TIMESTAMP_MARK_BEGIN)
		stamp = conn->d[LWROC_MTCD_OUT]._conn_block->_timestprv;

	      /* fprintf (stderr, "c... %" PRIu64 "\n", stamp); */

	      if (stamp < LWROC_MERGE_TIMESTAMP_MARK_BEGIN)
		{
		  uint64_t stamp_sec = stamp / 1000000000;
		  uint64_t now = tnow->_sec + (uint64_t) (int64_t) now_leap;

		  /* fprintf (stderr, "%" PRIu64 " %" PRIu64 "\n",sec,now); */

		  if (stamp_sec < now + 2 &&
		      stamp_sec > now - 120)
		    global->_kind = LWROC_TRACK_TIMESCALE_1G_TAI;
		}
	    }
	}
    }
}


void lwroc_combine_time_track_items(void)
{
  lwroc_mon_tree_timescale *global = &_lwroc_mon_global_timescale;
  lwroc_item_time tnow;
  int now_leap;
  int iround;

  {
    struct timeval now;

    gettimeofday(&now, NULL);

    tnow._sec  = (uint64_t) now.tv_sec;
    tnow._nsec = (uint32_t) now.tv_usec * 1000;

    now_leap = lwroc_get_leap_seconds(now.tv_sec);
  }

  global->_kind = LWROC_TRACK_TIMESCALE_UNKNOWN;

  /* First figure out some kind of common time-scale.
   *
   * Essentially by majority vote.  (With preference to systems that
   * seem to belong to (the?) data-flow).
   *
   * Then evaluate how each system is in relationship to that.
   */

  for (iround = 1; iround <= 3; iround++)
    {
      lwroc_combine_time_track_items_round(iround, &tnow);

      if (global->_kind != LWROC_TRACK_TIMESCALE_UNKNOWN)
	break;
    }

  if (global->_kind == LWROC_TRACK_TIMESCALE_UNKNOWN)
    lwroc_data_inputs_timescale(&tnow, now_leap);

  if (global->_kind == LWROC_TRACK_TIMESCALE_1G_TAI ||
      global->_kind == LWROC_TRACK_TIMESCALE_1G_UTC)
    {
      uint64_t ticks_per_s = 1000000000;
      int leap = 0;

      /* We set the global timescale to use the nominial values. */

      global->_ref_time = tnow;

      global->_dydx = (double) ticks_per_s;

      if (global->_kind == LWROC_TRACK_TIMESCALE_1G_TAI)
	leap = now_leap;

      global->_ref_time_stamp =
	( (uint64_t) global->_ref_time._sec +
	  (uint64_t) (int64_t) leap) * ticks_per_s +
	(((uint64_t) global->_ref_time._nsec) * ticks_per_s) / 1000000000;
    }

  if (global->_kind != LWROC_TRACK_TIMESCALE_UNKNOWN)
    {
      pd_ll_item *iter;

      /* Move everyone down, to give space for the global timestamp header. */

      PD_LL_FOREACH(_lwroc_mon_tree_items, iter)
	{
	  lwroc_mon_tree_item *item =
	    PD_LL_ITEM(iter, lwroc_mon_tree_item, _items);

	  if (item->_line >= 0)
	    item->_line++;
	}
    }
}

void lwroc_extents_mon_tree_c_slave(lwroc_mon_tree_item *tree_item,
				    lwroc_mon_tree_conn *tree_conn,
				    int y)
{
  lwroc_mon_tree_item *tree_master;

  (void) tree_item;

  /* Find the master. */

  tree_master = tree_conn->d[LWROC_MTCD_IN]._tree_item;

  tree_master->_line_bus.x = 1;

  lwroc_line_extents(&tree_master->_line_bus, y);
  lwroc_line_extents(&tree_master->_line_bus, tree_master->_line);
}

void lwroc_extents_mon_tree_c_data(lwroc_mon_tree_item *tree_item,
				   lwroc_mon_tree_conn *tree_conn,
				   int y, short linecolor)
{
  lwroc_mon_tree_item *tree_dest;
  int line_data_x;

  (void) tree_item;

  /* Find the master. */

  tree_dest = tree_conn->d[LWROC_MTCD_OUT]._tree_item;

  if (!tree_dest)
    return;

  y += tree_conn->d[LWROC_MTCD_IN]._lineoff;

  line_data_x = 71 + tree_item->_indent;
  /* Master with no slave should not have the data line cut in itself. */
  line_data_x += lwroc_indent_extra_mon_tree_item(tree_item);

  if (tree_dest->_line_data.x < line_data_x)
    tree_dest->_line_data.x = line_data_x;

  lwroc_line_extents(&tree_dest->_line_data, y);
  lwroc_line_extents(&tree_dest->_line_data, tree_dest->_line-1);

  if (!tree_dest->_line_data.color)
    tree_dest->_line_data.color = linecolor;
}

void lwroc_extents_mon_tree_item(lwroc_mon_tree_item *item)
{
  pd_ll_item *iter;
  short color;
  int iline;

  iline = 0;
  PD_LL_FOREACH(item->_masters, iter)
    {
      lwroc_mon_tree_conn *conn =
	PD_LL_ITEM(iter, lwroc_mon_tree_conn, d[LWROC_MTCD_OUT]._conns);

      lwroc_extents_mon_tree_c_slave(item, conn,
				     item->_line + iline);
      iline++;
    }

  PD_LL_FOREACH(item->_eb_masters, iter)
    {
      lwroc_mon_tree_conn *conn =
	PD_LL_ITEM(iter, lwroc_mon_tree_conn, d[LWROC_MTCD_OUT]._conns);

      lwroc_extents_mon_tree_c_slave(item, conn,
				     item->_line + iline);
      iline++;
    }

  color = 0;

  /* TODO: do these matter?, as colour is forced for EB / TS. */
  if (item->_sys_kind == LWROC_MON_TREE_SYS_KIND_MASTER ||
      item->_sys_kind == LWROC_MON_TREE_SYS_KIND_SLAVE)
    color = CTR_BLUE;
  if (item->_sys_kind == LWROC_MON_TREE_SYS_KIND_UNTRIG)
    color = CTR_GREEN;

  PD_LL_FOREACH(item->_data_dests, iter)
    {
      lwroc_mon_tree_conn *conn =
	PD_LL_ITEM(iter, lwroc_mon_tree_conn, d[LWROC_MTCD_IN]._conns);

      lwroc_extents_mon_tree_c_data(item, conn,
				    item->_line, color);
    }
}

void lwroc_extents_mon_tree_items(void)
{
  pd_ll_item *iter;

  PD_LL_FOREACH(_lwroc_mon_tree_items, iter)
    {
      lwroc_mon_tree_item *item =
	PD_LL_ITEM(iter, lwroc_mon_tree_item, _items);

      lwroc_extents_mon_tree_item(item);
    }

  /* Assign data line colours. */
  PD_LL_FOREACH(_lwroc_mon_tree_items, iter)
    {
      lwroc_mon_tree_item *item =
	PD_LL_ITEM(iter, lwroc_mon_tree_item, _items);

      if (item->_line_data.x != -1 /* &&
	  !item->_line_data.color */)
	{
	  if (item->_sys_kind == LWROC_MON_TREE_SYS_KIND_MERGE_EB)
	    item->_line_data.color = CTR_BLUE;
	  if (item->_sys_kind == LWROC_MON_TREE_SYS_KIND_MERGE_TS)
	    item->_line_data.color = CTR_CYAN;
	}

      if (!item->_line_data.color)
	item->_line_data.color = CTR_NONE;
    }
}

size_t lwroc_mon_text_scroll(size_t use, size_t len)
{
  int missing = (int) (len - use);
  int tcut = (int) (30 - missing);
  int toff;

  /* 0-5: fixed at 0, 25-35: fixed at max, 55-60: fixed at 0. */
  /* 5-25: moving forward, 35-55: moving backward. */

  if (missing <= 0)
    return 0;

  toff = _num_block_update % 60;

  if (toff > 30)
    toff = 60 - toff;
  toff -= 5;
  if (toff < 0)
    toff = 0;
  else if (toff > 20)
    toff = 20;

  tcut = 20 - missing;

  if (toff > tcut)
    return (size_t) (toff - tcut);
  return 0;
}

void wma(WINDOW *win, int y, int x, int attrs, short color,
	 const char *fmt,...)
  __attribute__ ((__format__ (__printf__, 6, 7)));

void wma_percent(WINDOW *win, int y, int x, int attrs, short color,
		 int width, double value);

void wmc(WINDOW *win, int y, int x, int attrs, short color,
	 const chtype c);

extern WINDOW* mw;

void lwroc_draw_extents_mon_tree_item(lwroc_mon_tree_item *item)
{
  int y;

  if (item->_line_bus.x != 0)
    {
      for (y = item->_line_bus.y_top; y <= item->_line_bus.y_bottom; y++)
	{
	  wmc(mw, y, item->_line_bus.x, A_BOLD, CTR_BLUE, ACS_VLINE);
	}
    }

  if (item->_line_data.x != 0)
    {
      for (y = item->_line_data.y_top; y <= item->_line_data.y_bottom; y++)
	{
	  wmc(mw, y, item->_line_data.x, A_BOLD,
	      item->_line_data.color, ACS_VLINE);
	}
    }
}

void lwroc_draw_extents_mon_tree_items(void)
{
  pd_ll_item *iter;

  PD_LL_FOREACH(_lwroc_mon_tree_items, iter)
    {
      lwroc_mon_tree_item *item =
	PD_LL_ITEM(iter, lwroc_mon_tree_item, _items);

      lwroc_draw_extents_mon_tree_item(item);
    }
}

/* Internal marker. */
#define STATUS_EXPIRED 0xffffedcb

void lwroc_draw_mon_tree_conn_part(int y, int x,
				   short conncolor, short nonconncolor,
				   lwroc_monitor_conn_block *conn_block,
				   lwroc_monitor_conn_source *conn_source,
				   int b)
{
  (void) conn_block;

  if (conn_block->_status == LWROC_CONN_STATUS_CONNECTED)
    {
      if (conn_source->_type == LWROC_CONN_TYPE_TRANS ||
	  conn_source->_type == LWROC_CONN_TYPE_FNET ||
	  conn_source->_type == LWROC_CONN_TYPE_STREAM ||
	  conn_source->_type == LWROC_CONN_TYPE_EBYE_PUSH ||
	  conn_source->_type == LWROC_CONN_TYPE_EBYE_PULL)
	wma(mw, y, x, A_NORMAL, CTR_BLACK_BG_MAGENTA, "-->");
      else if (conn_source->_type == LWROC_CONN_TYPE_FILE)
	wma(mw, y, x, A_NORMAL, CTR_YELLOW_BG_MAGENTA, "-->");
      else
	wma(mw, y, x, A_NORMAL, conncolor, b ? "<--" : "-->");
#if 0
      waddch (mw, ACS_URCORNER);
      waddch (mw, ACS_LRCORNER);
      waddch (mw, ACS_LTEE);
      waddch (mw, ACS_BTEE);
      waddch (mw, ACS_HLINE);
      waddch (mw, ACS_PLUS);
      waddch (mw, ACS_DIAMOND);
      waddch (mw, ACS_CKBOARD);
      waddch (mw, ACS_DEGREE);
      waddch (mw, ACS_PLMINUS);
      waddch (mw, ACS_BULLET);
      waddch (mw, ACS_LARROW);
      waddch (mw, ACS_UARROW);
      waddch (mw, ACS_RARROW);
      waddch (mw, ACS_DARROW); /* no good */
#endif
    }
  else
    {
      const char *status_str = "-?-";

      switch (conn_block->_status)
	{
	case LWROC_CONN_STATUS_NOT_CONNECTED:
	  if (conn_source->_type == LWROC_CONN_TYPE_TRANS ||
	      conn_source->_type == LWROC_CONN_TYPE_FNET ||
	      conn_source->_type == LWROC_CONN_TYPE_STREAM ||
	      conn_source->_type == LWROC_CONN_TYPE_EBYE_PUSH ||
	      conn_source->_type == LWROC_CONN_TYPE_EBYE_PULL)
	    {
	      wma(mw, y, x, A_NORMAL, nonconncolor, "---");
	      return;
	    }
	  status_str = "-X-";
	  break;
	case LWROC_CONN_STATUS_NOT_ATTEMPTED:
	  status_str = "-N-";
	  break;
	case STATUS_EXPIRED:
	  status_str = "-Z-";
	  break;
	}

      wma(mw, y, x, A_UNDERLINE, CTR_WHITE_BG_RED, "%s", status_str);
    }
}

int lwroc_mon_tree_conn_is_foreign(lwroc_monitor_conn_source *conn_source,
				   short *color)
{
  switch (conn_source->_type)
    {
    case LWROC_CONN_TYPE_TRANS:     *color = CTR_BLUE;  return 1;
    case LWROC_CONN_TYPE_FNET:      *color = CTR_GREEN; return 1;
    case LWROC_CONN_TYPE_STREAM:    *color = CTR_GREEN; return 1;
    case LWROC_CONN_TYPE_EBYE_PUSH: *color = CTR_GREEN; return 1;
    case LWROC_CONN_TYPE_EBYE_PULL: *color = CTR_BLUE;  return 1;
  }
  return 0;
}

void lwroc_draw_mon_tree_conn_ipv4(int y, int x,
				   lwroc_monitor_conn_block *conn_block,
				   lwroc_monitor_conn_source *conn_source)
{
  struct sockaddr_storage look_addr;
  char hostname[256];
  size_t offs;
  char type = '?';

  if (conn_block->_status == LWROC_CONN_STATUS_CONNECTED)
    {
      lwroc_set_ipv4_addr(&look_addr,
			  conn_block->_ipv4_addr);
      if (_config._no_name_lookup)
	lwroc_inet_ntop(&look_addr, hostname, sizeof (hostname));
      else
	lwroc_get_hostname(&look_addr, hostname, sizeof (hostname));
    }
  else
    {
      strcpy(hostname, "-");
    }

  switch (conn_source->_type)
    {
    case LWROC_CONN_TYPE_TRANS:     type = 'T'; break;
    case LWROC_CONN_TYPE_FNET:      type = 'F'; break;
    case LWROC_CONN_TYPE_STREAM:    type = 'S'; break;
    case LWROC_CONN_TYPE_EBYE_PUSH: type = 'e'; break;
    case LWROC_CONN_TYPE_EBYE_PULL: type = 'E'; break;
    case LWROC_CONN_TYPE_FILE:      type = '?'; break;
 }

  /* We have 9 characters space.  This since we use the same space of
   * a fill meter (6-1, the first is only used for 100%), plus the out
   * (<--) connection (3) plus one of the data connection line (which
   * a foreign connection never uses).
   */

  offs = lwroc_mon_text_scroll(8, strlen(hostname));

  wma(mw, y, x, A_NORMAL, CTR_NONE, "%c:", type);
  wma(mw, y, x+2,
      conn_block->_status == LWROC_CONN_STATUS_CONNECTED ? A_BOLD : A_NORMAL,
      CTR_NONE,
      "%8.8s", hostname + offs);
  /*wma(mw, y, x+16, A_NORMAL, CTR_NONE, "]");*/
}

void lwroc_draw_mon_tree_conn(lwroc_mon_tree_item *tree_item,
			      lwroc_mon_tree_conn *tree_conn,
			      int y, int x,
			      short conncolor, short nonconncolor)
{
  lwroc_monitor_conn_source *conn_source_a;
  lwroc_monitor_conn_source *conn_source_b;
  lwroc_monitor_conn_block *conn_block_a;
  lwroc_monitor_conn_block *conn_block_b;

  (void) tree_item;

  conn_block_a = tree_conn->d[LWROC_MTCD_IN ]._conn_block;
  conn_block_b = tree_conn->d[LWROC_MTCD_OUT]._conn_block;
  conn_source_a = tree_conn->d[LWROC_MTCD_IN ]._conn_source;
  conn_source_b = tree_conn->d[LWROC_MTCD_OUT]._conn_source;

  if (conn_block_a)
    lwroc_draw_mon_tree_conn_part(y, x,   conncolor, nonconncolor,
				  conn_block_a, conn_source_a, 0);
  if (conn_block_b)
    lwroc_draw_mon_tree_conn_part(y, x+3, conncolor, nonconncolor,
				  conn_block_b, conn_source_b, 1);
}

void lwroc_draw_mon_tree_c_slave(lwroc_mon_tree_item *tree_item,
				 lwroc_mon_tree_conn *tree_conn,
				 int y)
{
  lwroc_mon_tree_item *tree_master;

  (void) tree_item;

  /* Find the master. */

  tree_master = tree_conn->d[LWROC_MTCD_IN]._tree_item;

  wmc(mw, y, tree_master->_line_bus.x, A_BOLD, CTR_BLUE,
      y == tree_master->_line_bus.y_bottom ? ACS_LLCORNER :
      y == tree_master->_line_bus.y_top ? ACS_ULCORNER : ACS_LTEE);
  wmc(mw, y, tree_master->_line_bus.x+1, A_BOLD, CTR_BLUE, ACS_HLINE);
}

void lwroc_draw_mon_tree_c_data(lwroc_mon_tree_item *tree_item,
				lwroc_mon_tree_conn *tree_conn,
				int y)
{
  lwroc_mon_tree_item *tree_dest;

  (void) tree_item;

  /* Find the master. */

  tree_dest = tree_conn->d[LWROC_MTCD_OUT]._tree_item;

  if (!tree_dest)
    return;

  wmc(mw, y, tree_dest->_line_data.x, A_BOLD,
      tree_dest->_line_data.color,
      y == tree_dest->_line_data.y_bottom ? ACS_LRCORNER :
      y == tree_dest->_line_data.y_top ? ACS_URCORNER : ACS_RTEE);
  wmc(mw, y, tree_dest->_line_data.x-1, A_BOLD,
      tree_dest->_line_data.color, ACS_HLINE);
}

void lwroc_draw_mon_tree_fill(lwroc_mon_tree_conn *tree_conn,
			      int d, int width,
			      int y, int x, short fillcolor,
			      int disabled, int broken)
{
  uint64_t used = tree_conn->d[d]._conn_block->_buf_data_fill &
    ~LWROC_CONN_DATA_FILL_FULL;
  uint64_t size = tree_conn->d[d]._conn_source->_buf_data_size;
  double fill = ((double) used) / (double) size;
  const char *flag;
  int flagattrs;
  short flagcolor;
  size_t off = 0;

  if (fill > 1.)
    fill = 1.;

  if (_config._print_buf_size)
    {
      double d_size = (double) size;
      char str_val[64];

      lwroc_format_diff_info info;

      info._prefix_age = 0;
      info._prefix = 2;     /* We prefer M prefix, to avoid seeing k. */

      lwroc_format_prefix(str_val, sizeof (str_val),
			  d_size, width,
			  &info);

      wma(mw, y, x, A_BOLD, fillcolor, "%*s", width, str_val);
    }
  else
    {
      if (width == 6)
	wma(mw, y, x, A_BOLD, fillcolor, "%5.1f%%", 100. * fill);
      else
	wma(mw, y, x, A_BOLD, fillcolor, "%3.0f", 100. * fill);
    }

  if (tree_conn->d[d]._conn_block->_buf_data_fill &
      LWROC_CONN_DATA_FILL_FULL)
    { flag = " Full ";      flagattrs = A_BOLD; flagcolor = CTR_WHITE_BG_RED; }
  else if (disabled)
    { flag = " Disabled ";  flagattrs = A_BOLD; flagcolor = CTR_WHITE_BG_RED; }
  else if (broken)
    { flag = " Broken timestamp ";
      /* */                 flagattrs = A_BOLD; flagcolor = CTR_WHITE_BG_RED; }
  else
    { flag = " ";           flagattrs = A_NORMAL; flagcolor = CTR_NONE; }

  if (strlen(flag) > 1)
    off = ((size_t) _num_block_update) % (strlen(flag) - 1);

  wma(mw, y, x+width, flagattrs, flagcolor, "%.2s", flag + off);
}

void lwroc_draw_mon_tree_rate(int has_rate, double rate,
			      int y, int x,
			      int rateattrs, short ratecolor, int width,
			      lwroc_format_diff_info *info)
{
  char str_val[64];
  lwroc_format_diff_info dummy_info;

  if (!info)
    {
      dummy_info._prefix_age = 0;
      dummy_info._prefix = 0;

      info = &dummy_info;
    }

  if (has_rate)
    lwroc_format_prefix(str_val, sizeof (str_val),
			rate, width,
			info);
  else
    {
      strcpy(str_val, "-");

      if (ratecolor == CTR_WHITE_BG_RED)
	ratecolor = CTR_RED;
      if (ratecolor == CTR_YELLOW_BG_RED)
	ratecolor = CTR_RED;
    }

  wma(mw, y, x, rateattrs, ratecolor, "%*s", width, str_val);
}

void lwroc_draw_mon_tree_rate_event(lwroc_mon_tree_conn *tree_conn,
				    int d, int d_fill,
				    int eventsfield,
				    int y, int x,
				    int rateattrs, short ratecolor, int width)
{
  lwroc_monitor_conn_block *ptr_block = tree_conn->d[d]._conn_block;
  lwroc_monitor_conn_block *ptr_prev = tree_conn->d[d]._conn_block_prev;
  int has_rate = 0;
  double rate = -1.;

  if (tree_conn->d[d]._conn_source)
    {
      MONITOR_STRUCT_FREQ;

      if (eventsfield)
	{
	  has_rate = (ptr_block->_events > ptr_prev->_events);
	  rate =
	    (double) (ptr_block->_events - ptr_prev->_events);
	}
      else
	{
	  has_rate = (ptr_block->_bytes > ptr_prev->_bytes);
	  rate =
	    (double) (ptr_block->_bytes - ptr_prev->_bytes);
	}

      rate *= frequency;
    }

  lwroc_draw_mon_tree_rate(has_rate, rate,
			   y, x, rateattrs, ratecolor, width,
			   &tree_conn->d[d]._diff_info);

  if (!has_rate &&
      d_fill != LWROC_MTCD_NONE &&
      tree_conn->d[d_fill]._conn_block)
    {
      double fill =
	(double) (tree_conn->d[d_fill]._conn_block->_buf_data_fill &
		  ~LWROC_CONN_DATA_FILL_FULL) /
	(double) tree_conn->d[d_fill]._conn_source->_buf_data_size;

      if (fill > 1.)
	fill = 1.;

      if (fill > 0.25)
	wma(mw, y, x+width-6, A_NORMAL, CTR_NONE, "%3.0f%%", 100. * fill);
      if (tree_conn->d[d_fill]._conn_block->_buf_data_fill &
	  LWROC_CONN_DATA_FILL_FULL)
	wma(mw, y, x+width-6+4, A_BOLD, CTR_WHITE_BG_RED, "F");
     }
}

void lwroc_draw_mon_tree_file_rate(lwroc_mon_tree_item *item,
				   int eventsfield,
				   int y, int x,
				   int rateattrs, short ratecolor, int width)
{
  lwroc_monitor_file_block *ptr_block = item->_file_block;
  lwroc_monitor_file_block *ptr_prev = item->_file_block_prev;
  int has_rate = 0;
  double rate = -1.;

  MONITOR_STRUCT_FREQ;

  if (eventsfield)
    {
      has_rate = (ptr_block->_events > ptr_prev->_events);
      rate =
	(double) (ptr_block->_events - ptr_prev->_events);
    }
  else
    {
      has_rate = (ptr_block->_bytes > ptr_prev->_bytes);
      rate =
	(double) (ptr_block->_bytes - ptr_prev->_bytes);
    }

  rate *= frequency;

  lwroc_draw_mon_tree_rate(has_rate, rate,
			   y, x, rateattrs, ratecolor, width,
			   &item->_file_diff_info[eventsfield]);
}

void lwroc_draw_mon_tree_status(lwroc_mon_tree_conn *tree_conn,
				int d, int y, int x)
{
  lwroc_monitor_conn_block *ptr_block = tree_conn->d[d]._conn_block;
  lwroc_status_format *status_format;

  status_format =
    lwroc_get_status_format(ptr_block->_aux_status);

  wma(mw, y, x, A_BOLD, status_format->color, "%2s", status_format->marker);
}

void lwroc_draw_mon_tree_last_error(lwroc_mon_tree_conn *tree_conn,
				    int d, int y, int x)
{
  lwroc_monitor_conn_block *ptr_block = tree_conn->d[d]._conn_block;
  lwroc_status_format *status_format;
  uint32_t aux_last_error = ptr_block->_aux_last_error;

  if (ptr_block->_aux_time_error._sec + 10 <
      ptr_block->_time._sec)
    aux_last_error = LWROC_LAST_ERROR_FORGET;

  status_format =
    lwroc_get_status_format(aux_last_error);

  wma(mw, y, x, A_BOLD, status_format->color, "%s", status_format->marker);
}

void lwroc_draw_mon_tree_tbus_status(lwroc_mon_tree_conn *tree_conn,
				     int d, int y, int x)
{
  lwroc_monitor_conn_block *ptr_block = tree_conn->d[d]._conn_block;
  lwroc_status_format *status_format;

  if (!ptr_block)
    return;

  status_format =
    lwroc_get_status_format(ptr_block->_aux_status);

  wma(mw, y, x, A_BOLD, status_format->color, "%1s", status_format->marker);
}

void lwroc_draw_mon_tree_global_timestamp(void)
{
  lwroc_mon_tree_timescale *global = &_lwroc_mon_global_timescale;

  int y = 0, x = 15, x_tz = 5;

  if (global->_kind == LWROC_TRACK_TIMESCALE_1G_TAI)
    {
      wma(mw, y, x, A_NORMAL, CTR_GREEN,
	  "1G+TAI");
    }

  if (global->_kind == LWROC_TRACK_TIMESCALE_1G_UTC)
    {
      wma(mw, y, x, A_NORMAL, CTR_GREEN,
	  "1G+UTC");
      /*
      wma(mw, y, x, A_NORMAL, CTR_GREEN,
	  "1G+UTC %+.3fppm  %+.2f%ss  ",
	  (dydx - 1.e9) * 1.e-9 * 1.e6,
	  d_time_diff, time_prefix);
      */
    }

  if (_config._print_raw_ts == LWROCMON_PRINT_RAW_TS_HUMAN &&
      (global->_kind == LWROC_TRACK_TIMESCALE_1G_TAI ||
       global->_kind == LWROC_TRACK_TIMESCALE_1G_UTC))
    {
      time_t mt;
      struct tm *mt_tm;
      char mt_tz[64];

      mt = 0;
      mt_tm = localtime(&mt);
      strftime(mt_tz,sizeof(mt_tz),"%z %Z",mt_tm);

      wma(mw, y, x_tz, A_NORMAL, CTR_CYAN,
	  "%s", mt_tz);
    }

  if (global->_kind == LWROC_TRACK_TIMESCALE_LOCAL)
    {
      wma(mw, y, x, A_NORMAL, CTR_GREEN,
	  "%.3f MHz   ",
	  global->_dydx * 1.e-6);
    }
}

void lwroc_draw_mon_tree_timescale_track(lwroc_mon_tree_item *item,
					 int y, int x)
{
  lwroc_mon_tree_timescale *global = &_lwroc_mon_global_timescale;

  int matching = 0;

  /* Tell how well we are matching the globally chosen timescale. */

  {
    lwroc_monitor_main_block *mb = item->_main_block;

    double t_dydx = (double) mb->_ts_ref_stamp_freq_k * 1.e-3;
    double t_y0   = (double) (int64_t) mb->_ts_ref_stamp_y0_k * 1.e-3;
    double s_dydx = (double) mb->_ts_ref_stamp_sig_freq_k * 1.e-3;
    double s_y0   = (double) mb->_ts_ref_stamp_sig_y0_k * 1.e-3;

    double dydx_diff = t_dydx - global->_dydx;

    (void) t_y0;
    (void) s_y0;

    /* Allow 200 ppm + 5 * local sigma. */

    if (fabs(dydx_diff) < 1.e9*200e-6 + 5 * s_dydx)
      {
	matching = 1;
      }

    if (matching)
      {
	/* If the local device gives the same timestamp, how
	 * far off is the clock?
	 */

	double d_time_diff =
	  (double) ((int64_t) mb->_ts_ref_time._sec -
		    (int64_t) global->_ref_time._sec) +
	  1.e-9 * (double) ((int32_t) mb->_ts_ref_time._nsec -
			    (int32_t) global->_ref_time._nsec);
	int64_t stamp_diff =
	  (int64_t) mb->_ts_ref_stamp -
	  (int64_t) global->_ref_time_stamp;

	double d_stamp_diff  = ((double) stamp_diff) + t_y0;
	/* The above combines (to avoid cancellation loss):
	 *
	 * stamp_diff = (mb->_ts_ref_stamp + t_y0) - ref_time_stamp
	 */

	double d_cur_time_diff = d_time_diff - d_stamp_diff / t_dydx;

	double d_time_show_diff = d_cur_time_diff;
	const char *time_prefix = "";

	if (fabs(d_time_show_diff) < 200.e-9)
	  {
	    d_time_show_diff *= 1.e9;
	    time_prefix = "n";
	  }
	else if (fabs(d_time_show_diff) < 200.e-6)
	  {
	    d_time_show_diff *= 1.e6;
	    time_prefix = "u";
	  }
	else if (fabs(d_time_show_diff) < 200.e-3)
	  {
	    d_time_show_diff *= 1.e3;
	    time_prefix = "m";
	  }

	wma(mw, y, x, A_NORMAL, CTR_GREEN,
	    "%+.3fppm   %+.2f%ss  " /* " %.1f %1.f" */,
	    dydx_diff / global->_dydx * 1.e6,
	    d_time_show_diff, time_prefix /*, t_dydx, global->_dydx*/);
      }
    else
      {
	wma(mw, y, x, A_BOLD, CTR_WHITE_BG_RED,
	    "%.3f MHz   ",
	    t_dydx * 1.e-6);
      }
  }
}

void lwroc_draw_mon_tree_timestamp_offset(uint64_t stamp,
					  uint64_t stamp_prev,
					  int y, int x,
					  int y_raw, int x_raw,
					  int disabled,
					  int broken,
					  int mul)
{
  lwroc_mon_tree_timescale *global = &_lwroc_mon_global_timescale;

  /* double d_time_diff; */
  double d_time_stamp;
  uint64_t stamp_diff;
  double d_stamp_diff;
  double d_cur_time_diff;

  short color = CTR_CYAN;

  if (stamp >= LWROC_MERGE_TIMESTAMP_MARK_BEGIN &&
      stamp_prev && (stamp_prev < LWROC_MERGE_TIMESTAMP_MARK_BEGIN))
    {
      stamp = stamp_prev;
      color = (disabled || broken) ? CTR_WHITE_BG_RED : CTR_NONE; /* ??? */
    }

  if (stamp >= LWROC_MERGE_TIMESTAMP_MARK_BEGIN)
    {
      wma(mw, y, x, A_NORMAL, color,
	  "%7s-", "");
    }
  else
    {
  /*
  d_time_diff =
    (double) ((int64_t) global->_ref_time.tv_sec -
	      (int64_t) mb->_ts_ref_time._sec) +
    1.e-9 * (double) ((int32_t) global->_ref_time.tv_usec * 1000 -
		      (int32_t) mb->_ts_ref_time._nsec);
  d_time_stamp = d_time_diff * global->_dydx + global->_y0;
  */
      d_time_stamp = 0;

      /* To get correct sign for differences that are to large to be
       * represented with a signed integer, we explicitly handle
       * the two cases using unsigned differences, converted to double.
       */

      if (stamp > global->_ref_time_stamp)
	{
	  stamp_diff = stamp - global->_ref_time_stamp;
	  d_stamp_diff = (double) stamp_diff;
	}
      else
	{
	  stamp_diff = global->_ref_time_stamp - stamp;
	  d_stamp_diff = -((double) stamp_diff);
	}

      d_stamp_diff -= d_time_stamp;

      d_cur_time_diff = d_stamp_diff / global->_dydx;

      d_cur_time_diff *= mul;

      if (fabs(d_cur_time_diff) > 999*24*3600)
	wma(mw, y, x, A_NORMAL, color,  /* up to -999999d */
	    "%7.0fd", d_cur_time_diff / (3600. * 24.));
      else if (fabs(d_cur_time_diff) > 96*3600)
	{
	  int days  = (int) (d_cur_time_diff / (3600. * 24.));
	  int hours =
	    (int) ((fabs(d_cur_time_diff)-(abs(days)*3600*24))/3600.);
	  wma(mw, y, x, A_NORMAL, color,  /* up to -999d99h */
	      "%4dd%02dh", days,hours);
	}
      else if (fabs(d_cur_time_diff) > 300*60)
	{
	  int hours = (int) (d_cur_time_diff / 3600);
	  int mins =
	    (int) ((fabs(d_cur_time_diff)-(abs(hours)*3600))/60.);
	  wma(mw, y, x, A_NORMAL, color,  /* up to -999h99m, max 96 h */
	      "%4dh%02dm", hours, mins);
	}
      else if (fabs(d_cur_time_diff) > 300)
	{
	  int mins = (int) (d_cur_time_diff / 60);
	  int secs = (int) (fabs(d_cur_time_diff)-(abs(mins)*60));
	  wma(mw, y, x, A_NORMAL, color,  /* up to -999m99s, max 300 m */
	      "%4dm%02ds", mins, secs);
	}
      else if (fabs(d_cur_time_diff) > 99)
	wma(mw, y, x, A_NORMAL, color,  /* up to -999.99 s, max 300 s */
	    "%7.2fs", d_cur_time_diff);
      else
	wma(mw, y, x, A_NORMAL, color,  /* up to -99.999 s */
	    "%7.3fs", d_cur_time_diff);
    }

  if (_config._print_raw_ts)
    {
      if (stamp == LWROC_MERGE_TIMESTAMP_MISSING)
	wma(mw, y_raw, x_raw, A_NORMAL, color, "missing");
      else if (stamp == LWROC_MERGE_TIMESTAMP_DISABLED)
	wma(mw, y_raw, x_raw, A_NORMAL, color, "disabled");
      else if (stamp == LWROC_MERGE_TIMESTAMP_WAITDATA)
	wma(mw, y_raw, x_raw, A_NORMAL, color, "waitdata");
      else if (stamp == LWROC_MERGE_TIMESTAMP_BROKEN)
	wma(mw, y_raw, x_raw, A_NORMAL, color, "broken");
      else if (_config._print_raw_ts == LWROCMON_PRINT_RAW_TS_HEX)
	wma(mw, y_raw, x_raw, A_NORMAL, color,
	    "%04x:%04x:%04x:%04x",
	    (int) ((stamp >> 48) & 0xffff),
	    (int) ((stamp >> 32) & 0xffff),
	    (int) ((stamp >> 16) & 0xffff),
	    (int) ((stamp      ) & 0xffff));
      else if (_config._print_raw_ts == LWROCMON_PRINT_RAW_TS_HUMAN)
	{
	  if (global->_kind == LWROC_TRACK_TIMESCALE_1G_TAI ||
	      global->_kind == LWROC_TRACK_TIMESCALE_1G_UTC)
	    {
	      time_t mt;
	      struct tm *mt_tm;
	      char mt_date[64];
	      uint32_t mt_s, mt_ns;

	      mt_s  = (uint32_t) (stamp / 1000000000);
	      mt_ns = (uint32_t) (stamp % 1000000000);

	      if (global->_kind == LWROC_TRACK_TIMESCALE_1G_TAI)
		{
		  int leap;

		  /* This is not fantastically correct, since it wants
		   * the UTC time to figure out the actual leap
		   * seconds.  If in TAI, we are sending the time with
		   * the leap seconds actually added.
		   */
		  leap = lwroc_get_leap_seconds((time_t) mt_s);
		  /* So we call it twice.  Still could be bad around
		   * the flip point, but only for a second instead
		   * of as many seconds as leap seconds.
		   */
		  leap = lwroc_get_leap_seconds((time_t) (mt_s -
							  (uint32_t) leap));
		  /* Apply the correction. */
		  mt_s -= (uint32_t) leap;
		}

	      mt = (time_t) mt_s;
	      mt_tm = localtime(&mt);
	      strftime(mt_date,sizeof(mt_date),"%Y-%m-%d %H:%M:%S",mt_tm);

	      (void) mt_ns;
	      /* Do not print the ns part: it is confusing, since it does
	       * not match with the offsets calculated.  The offsets are
	       * calculated versus the local system clock.  But as this
	       * just prints the raw value, and the other values is not
	       * printed, it only gives half the story.
	       */
	      wma(mw, y_raw, x_raw, A_NORMAL, color,
		  "%s"/*".%06d"*/,
		  mt_date/*, mt_ns / 1000*/);
	    }
	  else
	    {
	      wma(mw, y_raw, x_raw, A_NORMAL, color, "?");
	    }
	}
      else
	wma(mw, y_raw, x_raw, A_NORMAL, color,
	    "%" PRIu64 "", stamp);
    }
}

void lwroc_draw_mon_tree_num_msg(lwroc_mon_tree_item *item,
				 int y, int x)
{
  lwroc_monitor_net_block *ptr_block = item->_net_block;
  lwroc_monitor_net_block *ptr_prev = item->_net_block_prev;
  int has_rate = 0;
  double rate = -1.;

  {
    MONITOR_STRUCT_FREQ;

    has_rate = (ptr_block->_num_msg > ptr_prev->_num_msg);
    rate =
      (double) (ptr_block->_num_msg -
		ptr_prev->_num_msg);

    rate *= frequency;
  }

  lwroc_draw_mon_tree_rate(has_rate, rate,
			   y, x, A_BOLD, CTR_WHITE_BG_RED, 5,
			   &item->_msg_rate_diff_info);
}

void lwroc_draw_mon_tree_meas_dt(lwroc_mon_tree_item *item,
				 int y, int x)
{
  double ctime_us, poll_us, dt_us, postproc_us;
  double events;

  if ((item->_net_block->_meas_dt_2events &
       item->_net_block_prev->_meas_dt_2events & 1) &&
      item->_net_block->_meas_dt_2events !=
      item->_net_block_prev->_meas_dt_2events)
    {
      events = (item->_net_block->     _meas_dt_2events -
		item->_net_block_prev->_meas_dt_2events) / 2;
      poll_us = (item->_net_block->     _meas_dt_us_poll -
		 item->_net_block_prev->_meas_dt_us_poll);
      postproc_us = (item->_net_block->     _meas_dt_us_postproc -
		     item->_net_block_prev->_meas_dt_us_postproc);
      dt_us = (item->_net_block->     _meas_dt_us -
	       item->_net_block_prev->_meas_dt_us);

      poll_us /= events;
      postproc_us /= events;
      dt_us /= events;

      ctime_us = (item->_net_block->_meas_dt_ctime & 0xffff) / 10;

      /* We include the ctime in the bold deadtime shown, and
       * half the poll time.
       */
      dt_us += ctime_us + poll_us * 0.5;
      /* Also, getting the total dt before readout return is
       * more useful than having to add the two components.
       */
      postproc_us += dt_us;

      wma(mw, y, x, A_REVERSE, CTR_NONE,
	  "%3.0f,%3.0f,",
	  ctime_us, poll_us);
      wma(mw, y, x+8, A_REVERSE | A_BOLD, CTR_NONE,
	  "%4.0f",
	  dt_us);
      wma(mw, y, x+12, A_REVERSE, CTR_NONE,
	  ";%4.0f",
	  postproc_us);
    }
  else
    {
      wma(mw, y, x, A_REVERSE, CTR_NONE,
	  "  -   -    -    -");
    }
}

void lwroc_draw_mon_tree_sys_name(lwroc_mon_tree_item *item,
				  size_t maxlen,
				  uint16_t default_port,
				  int y, int x,
				  int sysattrs, short syscolor,
				  uint32_t health)
{
  /* char dotted[INET6_ADDRSTRLEN]; */
  char hostname[256];
  char port_str[16];
  uint16_t port;
  const char *label = NULL;

  int failattrs = A_NORMAL;
  short failcolor = CTR_NONE;
  const char *failtext = "";

  size_t fail_len = 0;
  size_t hostname_len = 0;
  size_t port_len = 0;
  size_t label_len = 0;

  size_t hostname_offs = 0;
  size_t hostname_use = 0;
  size_t label_offs = 0;
  size_t label_use = 0;
  size_t fill_len = 0;

  switch (health)
    {
    case LWROC_NET_SYS_HEALTH_OK:
      break;
    case LWROC_NET_SYS_HEALTH_LOGWAIT:
      failattrs = ((int) A_REVERSE) | sysattrs;
      failcolor = CTR_NONE;
      failtext = "LogWait: ";
      break;
    case LWROC_NET_SYS_HEALTH_SIGNAL:
      failcolor = CTR_YELLOW_BG_MAGENTA;
      syscolor = CTR_WHITE_BG_RED;
      failtext = "SIGNAL: ";
      break;
    case LWROC_NET_SYS_HEALTH_BUG:
      failcolor = CTR_RED_BG_GREEN;
      syscolor = CTR_WHITE_BG_RED;
      failtext = "BUG: ";
      break;
    case LWROC_NET_SYS_HEALTH_FATAL:
      failcolor = CTR_WHITE_BG_MAGENTA;
      syscolor = CTR_WHITE_BG_RED;
      failtext = "FATAL: ";
      break;
    default:
      failtext = "??: ";
      FALL_THROUGH;
    case 0:
      sysattrs = A_REVERSE;
      syscolor = CTR_NONE;
      break;
    }

  fail_len = strlen(failtext);
  maxlen -= fail_len;

  if (item->_us)
    strncpy(hostname, item->_us->_hostname, sizeof (hostname));
  else if (item->_sys_kind == LWROC_MON_TREE_SYS_KIND_FILE_SRC)
    strncpy(hostname, item->_ext_hostname, sizeof (hostname));
  else
    {
      /* TODO: do this lookup only once. */
      if (_config._no_name_lookup)
	lwroc_inet_ntop(&item->_addr, hostname, sizeof (hostname));
      else
	lwroc_get_hostname(&item->_addr, hostname, sizeof (hostname));
    }
  hostname[sizeof (hostname) - 1] = 0;
  port = lwroc_get_port(&item->_addr);

  hostname_len = strlen(hostname);

  if (port != default_port &&
      item->_sys_kind != LWROC_MON_TREE_SYS_KIND_FILE_SRC)
    {
      if (port > default_port && port <= default_port+20)
	snprintf(port_str,sizeof(port_str), "+%d", port - default_port);
      else
	snprintf(port_str,sizeof(port_str), "%d", port);
      port_len = 1 + strlen(port_str);
    }
  if (item->_main_source)
    {
      label = item->_main_source->_label;
    }

  if (!label)
    label = item->_ext_label;

  if (label)
    {
      label_len = strlen(label);
    }

  if (0)
  {
    /* We focus on the system name.
     * Port will be printed, in full.
     * If there is space left over, the label will be printed.
     */

    hostname_use = hostname_len;

    if (hostname_use + port_len > maxlen)
      {
	hostname_use = maxlen - port_len;
      }
    if (hostname_use + port_len < maxlen)
      {
	label_use = maxlen - hostname_use - port_len - 1 /* # */;
      }
    if (label_use > label_len)
      label_use = label_len;
  }

  if (0)
  {
    /* We focus on the system name.
     * Port will be printed, in full.
     * If there is space left over, the label will be printed.
     */

    label_use = label_len;

    if (label_use + port_len > maxlen)
      {
	label_use = maxlen - port_len;
      }
    if (label_use + port_len < maxlen)
      {
	hostname_use = maxlen - label_use - port_len - 1 /* # */;
      }
    if (hostname_use > hostname_len)
      hostname_use = hostname_len;
  }

  {
    /* After subtracting the port length (which is usually not shown
     * anyhow), we share the space between the hostname and the label.
     */

    size_t remain = maxlen - port_len;
    size_t label_tot = label_len + (label_len ? 1 : 0);

    if (hostname_len + label_tot > remain)
      {
	hostname_use = remain / 2;
	label_use = remain - hostname_use;

	if (label_use > label_tot)
	  {
	    label_use = label_tot;
	    hostname_use = remain - label_use;
	  }
	if (hostname_use > hostname_len)
	  {
	    hostname_use = hostname_len;
	    label_use = remain - hostname_use;
	  }
      }
    else
      {
	hostname_use = hostname_len;
	label_use = label_tot;
      }

    if (label_use)
      label_use--;

    hostname_offs = lwroc_mon_text_scroll(hostname_use, hostname_len);
    label_offs    = lwroc_mon_text_scroll(label_use,    label_len);
  }

  fill_len =
    maxlen - (hostname_use + port_len +
	      label_use + (label_use ? 1 : 0) /* # */);
  if (((ssize_t) fill_len) < 0)
    fill_len = 0;

  if (fail_len)
    {
      wma(mw, y, x, failattrs, failcolor,
	  "%s", failtext);
    }
  wma(mw, y, x + (int) fail_len, sysattrs, syscolor,
      "%*.*s", (int) hostname_use, (int) hostname_use,
      hostname + hostname_offs);
  if (port_len)
    {
      wma(mw, y, x + (int) fail_len + (int) hostname_use,
	  sysattrs/*A_NORMAL*/, syscolor, ":");
      wma(mw, y, x + (int) fail_len + (int) hostname_use + 1,
	  sysattrs, syscolor,
	  "%s", port_str);
    }
  wma(mw, y, x + (int) fail_len + (int) hostname_use + (int) port_len,
      sysattrs, syscolor, "%*s", (int) fill_len, "");
  if (label_use)
    {
      wma(mw, y, x + (int) fail_len + (int) maxlen - (int) label_use - 1,
	  sysattrs, syscolor, "%c", fill_len > 0 ? ' ' : ' '); /* last '#' */
      wma(mw, y, x + (int) fail_len + (int) maxlen - (int) label_use,
	  sysattrs, syscolor,
	  "%*.*s", (int) label_use, (int) label_use,
	  label + label_offs);
    }

  /*
  {
    char search[16];

    if (item->_us)
      snprintf(search,sizeof(search), " %02x", item->_us->_added_why);
    else
      snprintf(search,sizeof(search), " ??");

    wma(mw, y, x + (int) maxlen - 3, A_NORMAL, CTR_NONE, search);
  }
  */
}

void lwroc_draw_mon_tree_item(lwroc_mon_tree_item *item)
{
  pd_ll_item *iter;

  int live;
  uint32_t health = 0;
  short nodecolor;
  int iline;
  int indent = item->_indent;
  uint16_t default_port;
  lwroc_mon_tree_conn *conn_ext_src = NULL;
  int show_dt = 0;
  int show_msg = 0;

  default_port = LWROC_NET_DEFAULT_PORT;

  live = (item->_us &&
	  item->_us->_mon_sources && !timerisset(&item->_us->_stale_expire));

  switch (item->_sys_kind)
    {
    case LWROC_MON_TREE_SYS_KIND_MASTER:
      nodecolor = CTR_YELLOW_BG_BLUE;
      indent += 2;
      break;
    case LWROC_MON_TREE_SYS_KIND_SLAVE:
      nodecolor = CTR_BLUE_BG_YELLOW;
      break;
    case LWROC_MON_TREE_SYS_KIND_UNTRIG:
      nodecolor = CTR_WHITE_BG_CYAN;
      indent += 2;
      break;
    case LWROC_MON_TREE_SYS_KIND_MERGE:
    case LWROC_MON_TREE_SYS_KIND_MERGE_EB:
    case LWROC_MON_TREE_SYS_KIND_MERGE_TS:
      nodecolor = CTR_WHITE_BG_MAGENTA;
      break;
    case LWROC_MON_TREE_SYS_KIND_TRANS_SRC:
    case LWROC_MON_TREE_SYS_KIND_PUSH_SRC:
    case LWROC_MON_TREE_SYS_KIND_FILE_SRC:
    case LWROC_MON_TREE_SYS_KIND_FNET_SRC:
      if (item->_sys_kind == LWROC_MON_TREE_SYS_KIND_FNET_SRC)
	nodecolor = CTR_CYAN_BG_BLUE;
      else if (item->_sys_kind == LWROC_MON_TREE_SYS_KIND_FILE_SRC)
	nodecolor = CTR_BLACK_BG_CYAN;
      else if (item->_sys_kind == LWROC_MON_TREE_SYS_KIND_PUSH_SRC)
	nodecolor = CTR_BLACK_BG_GREEN;
      else
	nodecolor = CTR_WHITE_BG_BLUE;
      default_port = LMD_TCP_PORT_TRANS;
      /* Fake a health for external sources.  If it is connected. */
      if (!PD_LL_IS_EMPTY(&item->_data_dests))
	{
	  lwroc_monitor_conn_block *conn_block_b;

	  conn_ext_src =
	    PD_LL_ITEM(PD_LL_FIRST(item->_data_dests),
		       lwroc_mon_tree_conn, d[LWROC_MTCD_IN]._conns);

	  conn_block_b = conn_ext_src->d[LWROC_MTCD_OUT]._conn_block;

	  if (conn_block_b &&
	      conn_block_b->_status == LWROC_CONN_STATUS_CONNECTED)
	    health = LWROC_NET_SYS_HEALTH_OK;
	  else
	    conn_ext_src = NULL;
	}
      break;
    case LWROC_MON_TREE_SYS_KIND_OMITTED:
      nodecolor = 0;
      break;
    default:
      assert(0);
      break;
    }

  if (live &&
      item->_net_block)
    health = item->_net_block->_sys_health;

  if (item->_net_block &&
      item->_net_block_prev &&
      item->_us &&
      item->_us->_msg_rate_ignore_quota <
      LWROC_MSG_RATE_IGNORE_QUOTA_THRESHOLD)
    show_msg = 1;

  if (item->_net_block &&
      item->_net_block_prev &&
      (item->_net_block->_meas_dt_2events & 1))
    {
      show_dt = 1;
      show_msg = 0;
    }

  if (item->_sys_kind == LWROC_MON_TREE_SYS_KIND_MASTER ||
      item->_sys_kind == LWROC_MON_TREE_SYS_KIND_UNTRIG)
    {
      if (item->_system)
	{
	  lwroc_draw_mon_tree_rate_event(item->_system,
					 LWROC_MTCD_OUT, LWROC_MTCD_NONE, 1,
					 item->_line, -2 + indent,
					 A_BOLD, nodecolor, 6);
	  lwroc_draw_mon_tree_status(item->_system, LWROC_MTCD_OUT,
				     item->_line, -2 + indent + 7);
	  lwroc_draw_mon_tree_last_error(item->_system, LWROC_MTCD_OUT,
					 item->_line, -2 + indent + 10);
	}
      else
	{
	  wma(mw, item->_line, -2 + indent,
	      A_NORMAL, CTR_YELLOW_BG_BLUE, "master");
	  wma(mw, item->_line, -2 + indent + 7,
	      A_NORMAL, CTR_NONE, " ");
	}
    }

  if (item->_sys_kind == LWROC_MON_TREE_SYS_KIND_MERGE ||
      item->_sys_kind == LWROC_MON_TREE_SYS_KIND_MERGE_EB ||
      item->_sys_kind == LWROC_MON_TREE_SYS_KIND_MERGE_TS)
    {
      if (PD_LL_IS_EMPTY(&item->_eb_masters) &&
	  item->_system)
	{
	  lwroc_draw_mon_tree_rate_event(item->_system,
					 LWROC_MTCD_OUT, LWROC_MTCD_NONE, 1,
					 item->_line, 3 + indent,
					 A_BOLD, nodecolor, 6);
	}
    }

  if (conn_ext_src)
    {
      lwroc_draw_mon_tree_rate_event(conn_ext_src,
				     LWROC_MTCD_OUT, LWROC_MTCD_NONE, 1,
				     item->_line, 3/*33*//*34*/+indent,
				     A_BOLD, nodecolor, 6);
    }

  lwroc_draw_mon_tree_sys_name(item,
			       show_msg ? 15 : (show_dt ? 13 : 21),
			       default_port,
			       item->_line, 11+indent,
			       live ? A_BOLD : A_NORMAL, nodecolor,
			       health);

  if (show_msg)
    lwroc_draw_mon_tree_num_msg(item,
				item->_line, 11+16+indent);

  if (show_dt)
    lwroc_draw_mon_tree_meas_dt(item,
				item->_line, 11+14+indent);

  /*
  wma(mw, item->_line, 11+indent, live ? A_BOLD : A_NORMAL, nodecolor,
      "%21.21s", sys_name);
  */
  if (item->_main_block)
    {
      if (_config._print_ts_track & LWROCMON_PRINT_TS_TRACK_RO_TRACK)
	{
	  if (item->_main_block->_last_timestamp)
	    {
	      /* We have a timestamp value for the main readout.
	       * Tell how it compares to the global timescale.
	       */
	      lwroc_draw_mon_tree_timestamp_offset(item->_main_block->
						   _last_timestamp, 0,
						   item->_line+1, 5+indent,
						   item->_line+2, 5+indent,
						   0, 0, 1);
	    }
	  if (item->_main_block->_ts_ref_stamp_freq_k)
	    {
	      lwroc_draw_mon_tree_timescale_track(item,
						  item->_line+1, 15+indent);
	    }
	}
    }

  if (_config._print_debug_ts &&
      item->_main_block &&
      item->_main_block->_last_timestamp)
    {
      /* TODO: should not be here, but for testing... */

      {
	lwroc_monitor_main_block *mb = item->_main_block;

	double t_dydx = (double) mb->_ts_ref_stamp_freq_k * 1.e-3;
	double t_y0   = (double) (int64_t) mb->_ts_ref_stamp_y0_k * 1.e-3;
	double s_dydx = (double) mb->_ts_ref_stamp_sig_freq_k * 1.e-3;
	double s_y0   = (double) mb->_ts_ref_stamp_sig_y0_k * 1.e-3;

#define TIMESCALE_UNKNOWN  0
#define TIMESCALE_1G_UTC   1

	int timescale = TIMESCALE_UNKNOWN;
	double d_stamp_diff = 0;
	double d_cur_time_diff = 0;

	(void) t_dydx;
	(void) t_y0;
	(void) s_dydx;
	(void) s_y0;

	if (fabs(t_dydx - 1.e9) < 1.e9*200e-6 + 5 * s_dydx)
	  {
	    uint64_t ref_time_stamp =
	      mb->_ts_ref_time._sec * 1000000000 +
	      mb->_ts_ref_time._nsec;
	    int64_t stamp_diff =
	      (int64_t) mb->_ts_ref_stamp -
	      (int64_t) ref_time_stamp;

	    d_stamp_diff = ((double) stamp_diff) + t_y0;
	    /* The above combines (to avoid cancellation loss):
	     *
	     * stamp_diff = (mb->_ts_ref_stamp + t_y0) - ref_time_stamp
	     */

	    wma(mw, item->_line+4, 11+indent, A_NORMAL, CTR_NONE,
		"%" PRIu64 "  %" PRId64 "  %.3f %.3f",
		ref_time_stamp,
		stamp_diff,
		t_y0,
		d_stamp_diff);

	    wma(mw, item->_line+5, 11+indent, A_NORMAL, CTR_NONE,
		"%" PRIu64 "",
		mb->_ts_ref_stamp);

	    if (fabs(d_stamp_diff) < 1.e9) /* less than 1 s off */
	      {
		timescale = TIMESCALE_1G_UTC;
	      }
	  }

	if (timescale == TIMESCALE_1G_UTC)
	  {
	    double d_time_diff = d_stamp_diff / 1.e9;
	    const char *time_prefix = "";

	    if (fabs(d_time_diff) < 200.e-9)
	      {
		d_time_diff *= 1.e9;
		time_prefix = "n";
	      }
	    else if (fabs(d_time_diff) < 200.e-6)
	      {
		d_time_diff *= 1.e6;
		time_prefix = "u";
	      }
	    else if (fabs(d_time_diff) < 200.e-3)
	      {
		d_time_diff *= 1.e3;
		time_prefix = "m";
	      }

	    wma(mw, item->_line+3, 37+indent, A_NORMAL, CTR_GREEN,
		"1G+UTC %+.3fppm  %+.2f%ss  ",
		(t_dydx - 1.e9) * 1.e-9 * 1.e6,
		d_time_diff, time_prefix);
	  }
	else
	  {
	    wma(mw, item->_line+3, 37+indent, A_NORMAL, CTR_GREEN,
		"%.3f MHz   ", t_dydx * 1.e-6);
	  }

	/* What would the present time correspond to in timestamp. */
	{
	  struct timeval now;
	  double d_time_diff;
	  double d_time_stamp;
	  int64_t stamp_diff;
	  double d_stamp_diffx;

	  gettimeofday(&now, NULL);

	  d_time_diff =
	    (double) ((int64_t) now.tv_sec -
		      (int64_t) mb->_ts_ref_time._sec) +
	    1.e-9 * (double) ((int32_t) now.tv_usec * 1000 -
			      (int32_t) mb->_ts_ref_time._nsec);
	  d_time_stamp = d_time_diff * t_dydx + t_y0;

	  stamp_diff =
	    (int64_t) item->_main_block->_last_timestamp -
	    (int64_t) mb->_ts_ref_stamp;
	  d_stamp_diffx = (double) stamp_diff;

	  d_stamp_diffx -= d_time_stamp;

	  d_cur_time_diff = d_stamp_diffx / t_dydx;

	  wma(mw, item->_line+6, 11+indent, A_NORMAL, CTR_NONE,
	      "%.6f %.6f %" PRIi64 " %.6f",
	      d_time_diff,
	      d_time_stamp,
	      stamp_diff,
	      d_stamp_diffx);

	}

	wma(mw, item->_line+3, 5+indent, A_NORMAL, CTR_CYAN,
	    "%" PRIu64 " %.3fs", item->_main_block->_last_timestamp,
	    d_cur_time_diff);
      }
    }

  if (item->_line_data.x != -1)
    {
      int x;

      wmc(mw, item->_line-1, 31+indent,
	  A_BOLD, item->_line_data.color, ACS_ULCORNER);
      wmc(mw, item->_line-1, 32+indent,
	  A_BOLD, item->_line_data.color, ACS_HLINE);
      for (x = 33+indent; x < item->_line_data.x-2; x++)
	wmc(mw, item->_line-1, x,
	    A_NORMAL, item->_line_data.color, ACS_HLINE);
      wmc(mw, item->_line-1, item->_line_data.x-2,
	  A_BOLD, item->_line_data.color, ACS_HLINE);
      wmc(mw, item->_line-1, item->_line_data.x-1,
	  A_BOLD, item->_line_data.color, ACS_HLINE);
      wmc(mw, item->_line-1, item->_line_data.x,
	  A_BOLD, item->_line_data.color, ACS_LRCORNER);
      /* TODO use lwroc_draw_mon_tree_c_data() or a version of it
       * above, such that we do a TEE and not ACS_LRCORNER when we are
       * not the last destination.)
       *
       * Also: calculate the indent (x pos) of the line from the worst
       * indented data source or destination, not just the destination.
       */
    }

  iline = 0;
  PD_LL_FOREACH(item->_masters, iter)
    {
      lwroc_mon_tree_conn *conn =
	PD_LL_ITEM(iter, lwroc_mon_tree_conn, d[LWROC_MTCD_OUT]._conns);

      lwroc_draw_mon_tree_tbus_status(conn, LWROC_MTCD_IN,
				      item->_line, 4);

      lwroc_draw_mon_tree_conn(item, conn,
			       item->_line + iline, 6, CTR_BLUE_BG_YELLOW, -1);

      lwroc_draw_mon_tree_c_slave(item, conn,
				  item->_line + iline);
      iline++;
    }

  PD_LL_FOREACH(item->_eb_masters, iter)
    {
      lwroc_mon_tree_conn *conn =
	PD_LL_ITEM(iter, lwroc_mon_tree_conn, d[LWROC_MTCD_OUT]._conns);

      lwroc_draw_mon_tree_tbus_status(conn, LWROC_MTCD_IN,
				      item->_line, 4);

      lwroc_draw_mon_tree_conn(item, conn,
			       item->_line + iline, 6, CTR_BLACK_BG_YELLOW,-1);

      lwroc_draw_mon_tree_c_slave(item, conn,
				  item->_line + iline);
      iline++;
    }

  if (item->_system)
    {
      short ratecolor = CTR_NONE;

      switch (item->_sys_kind)
	{
	case LWROC_MON_TREE_SYS_KIND_MASTER:
	case LWROC_MON_TREE_SYS_KIND_UNTRIG:
	  ratecolor = CTR_RED;
	  break;
	case LWROC_MON_TREE_SYS_KIND_MERGE_EB:
	  ratecolor = CTR_BLUE;
	  break;
	case LWROC_MON_TREE_SYS_KIND_MERGE_TS:
	  ratecolor = CTR_CYAN;
	  break;
	}

      lwroc_draw_mon_tree_rate_event(item->_system,
				     LWROC_MTCD_OUT, LWROC_MTCD_NONE, 0,
				     item->_line, 33+indent+show_dt*10,
				     A_BOLD, ratecolor, 8);
      lwroc_draw_mon_tree_fill(item->_system, LWROC_MTCD_OUT,
			       show_dt ? 3 : 6,
			       item->_line, 41+indent+show_dt*10,
			       CTR_MAGENTA, 0, 0);

      /* We cannot have a filter without a system, so can be here.
       * To use rate color.
       */
      if (item->_filter)
	{
	  const char *label = "(filter)";

	  if (item->_filter_source->_label[0])
	    label = item->_filter_source->_label;

	  wmc(mw, item->_line+item->_filter_lineoff, 11+indent,
	      A_BOLD, nodecolor, ACS_LLCORNER);
	  wmc(mw, item->_line+item->_filter_lineoff, 12+indent,
	      A_BOLD, nodecolor, ACS_HLINE);
	  wma(mw,
	      item->_line+item->_filter_lineoff,
	      13+indent,
	      live ? A_BOLD : A_NORMAL, nodecolor,
	      "%-*s", (int) (show_dt ? 11 : 19), label);

	  lwroc_draw_mon_tree_rate_event(item->_filter,
					 LWROC_MTCD_OUT, LWROC_MTCD_NONE, 1,
					 item->_line+item->_filter_lineoff,
					 3 + indent,
					 A_BOLD, nodecolor, 6);

	  lwroc_draw_mon_tree_rate_event(item->_filter,
					 LWROC_MTCD_OUT, LWROC_MTCD_NONE, 0,
					 item->_line+item->_filter_lineoff,
					 33+indent+show_dt*10,
					 A_BOLD, ratecolor, 8);
	  lwroc_draw_mon_tree_fill(item->_filter, LWROC_MTCD_OUT,
				   show_dt ? 3 : 6,
				   item->_line+item->_filter_lineoff,
				   41+indent+show_dt*10,
				   CTR_MAGENTA, 0, 0);
	}
    }

  PD_LL_FOREACH(item->_data_dests, iter)
    {
      lwroc_mon_tree_conn *conn =
	PD_LL_ITEM(iter, lwroc_mon_tree_conn, d[LWROC_MTCD_IN]._conns);
      short foreigncolor = CTR_NONE;
      int disabled = 0;
      int broken = 0;

      iline = conn->d[LWROC_MTCD_IN]._lineoff;

      if (!show_dt)
	lwroc_draw_mon_tree_conn(item, conn,
				 item->_line + iline, 48+indent,
				 CTR_WHITE_BG_MAGENTA, CTR_MAGENTA);

      lwroc_draw_mon_tree_c_data(item, conn,
				 item->_line + iline);

      if (conn->d[LWROC_MTCD_IN]._conn_source &&
	  conn->d[LWROC_MTCD_IN]._conn_source->_buf_data_size)
	{
	  /* This fill should not be reported along with this
	   * connection?  it is one fill factor for the entire item,
	   * and not one per connection.
	   */
	}

      if (conn->d[LWROC_MTCD_IN]._conn_source &&
	  lwroc_mon_tree_conn_is_foreign(conn->d[LWROC_MTCD_IN]._conn_source,
					 &foreigncolor))
	{
	  lwroc_draw_mon_tree_rate_event(conn,
					 LWROC_MTCD_IN, LWROC_MTCD_IN, 0,
					 item->_line + iline, 52+indent,
					 A_BOLD/* | A_UNDERLINE*/,
					 foreigncolor, 7);

	  lwroc_draw_mon_tree_conn_ipv4(item->_line + iline, 60+indent,
					conn->d[LWROC_MTCD_IN]._conn_block,
					conn->d[LWROC_MTCD_IN]._conn_source);
	}

      if (conn->d[LWROC_MTCD_OUT]._conn_block)
	{
	  if (conn->d[LWROC_MTCD_OUT]._conn_block->_timestamp ==
	      LWROC_MERGE_TIMESTAMP_DISABLED)
	    disabled = 1;
	  if (conn->d[LWROC_MTCD_OUT]._conn_block->_timestamp ==
	      LWROC_MERGE_TIMESTAMP_BROKEN)
	    broken = 1;
	}

      if (conn->d[LWROC_MTCD_OUT]._conn_source &&
	  conn->d[LWROC_MTCD_OUT]._conn_source->_buf_data_size)
	{
	  lwroc_draw_mon_tree_fill(conn, LWROC_MTCD_OUT,
				   6,
				   item->_line + iline, 63+indent,
				   CTR_MAGENTA,
				   disabled, broken);
	}

      if (conn->d[LWROC_MTCD_OUT]._conn_source)
	{
	  lwroc_draw_mon_tree_rate_event(conn,
					 LWROC_MTCD_OUT, LWROC_MTCD_IN, 0,
					 item->_line + iline, 55+indent,
					 A_BOLD, CTR_RED, 8);
	}

      if ((_config._print_ts_track & LWROCMON_PRINT_TS_TRACK_TO_INPUT) &&
	  conn->d[LWROC_MTCD_OUT]._conn_block &&
	  conn->d[LWROC_MTCD_OUT]._conn_block->_timestamp)
	{
	  int j = 0; int mul = 1;
	  /* This loop is for visual debugging of the various
	   * time formats (s, mins, hours...)
	   */
	  /*for (j = 0; j < 8; j++, mul *= 10)*/
	  lwroc_draw_mon_tree_timestamp_offset(conn->d[LWROC_MTCD_OUT].
					       _conn_block->_timestamp,
					       conn->d[LWROC_MTCD_OUT].
					       _conn_block->_timestprv,
					       item->_line + iline + 1,
					       61+indent-j*9,
					       item->_line + iline + 1,
					       35+indent,
					       disabled, broken,
					       mul);

	  if (conn->_timesort_src_index_mask)
	    {
	      /* TODO: collides with raw timestamp... (40+indent above) */

	      int src_index[64];
	      int num_src_index = 0;
	      uint64_t src_index_mask = conn->_timesort_src_index_mask;
	      int idx = 0;
	      int i;

	      while (src_index_mask)
		{
		  while (!(src_index_mask & 1))
		    {
		      idx++;
		      src_index_mask >>= 1;
		    }

		  src_index[num_src_index++] = idx;

		  idx++;
		  src_index_mask >>= 1;
		}

	      for (i = 0; i < num_src_index; i++)
		{
		  if (num_src_index <= 4)
		    wma(mw, item->_line + iline + 1,
			61+indent + (i - num_src_index) * 5,
			A_NORMAL, CTR_NONE,
			"%04x",
			src_index[i] << 8);
		  else if (num_src_index <= 10)
		    wma(mw, item->_line + iline + 1,
			61+indent + (i - num_src_index) * 2,
			A_NORMAL, CTR_NONE,
			"%x",
			src_index[i]);
		  else
		    wma(mw, item->_line + iline + 1,
			61+indent + (i - num_src_index) * 1,
			A_NORMAL, CTR_NONE,
			"%x",
			src_index[i]);
		}
	    }
	}
    }

  if (item->_file_block)
    {
      uint32_t filewrb;
      int any_open = 0;
      int fline = 0;

      for (filewrb = 0; filewrb < item->_num_filewr; filewrb++)
	{
	  lwroc_monitor_filewr_block *filewr_block =
	    item->_filewr_block + filewrb;

	  if (filewr_block->_state == LWROC_FILEWR_STATE_OPEN ||
	      filewr_block->_state == LWROC_FILEWR_STATE_JAMMED)
	    any_open = 1;
	}

      for (filewrb = 0; filewrb < item->_num_filewr; filewrb++)
	{
	  lwroc_monitor_filewr_block *filewr_block =
	    item->_filewr_block + filewrb;
	  int fileattrs = A_NORMAL;
	  short agocolor = CTR_NONE;
	  short filecolor = CTR_NONE;
	  short sizecolor = CTR_GREEN;
	  char str[32];
	  size_t offs;
	  int jammed = 0;
	  int hold = 0;

	  if (!filewr_block->_state)
	    continue;

	  switch (filewr_block->_state)
	    {
	    case LWROC_FILEWR_STATE_CLOSED:
	      agocolor = CTR_RED;
	      break;
	    case LWROC_FILEWR_STATE_OPEN:
	      fileattrs = A_BOLD;
	      sizecolor = CTR_WHITE_BG_GREEN;
	      break;
	    case LWROC_FILEWR_STATE_HOLD:
	      agocolor = CTR_RED;
	      hold = 1;
	      break;
	    case LWROC_FILEWR_STATE_JAMMED:
	      fileattrs = A_BOLD;
	      sizecolor = CTR_WHITE_BG_RED;
	      agocolor = CTR_WHITE_BG_RED;
	      jammed = 1;
	      break;
	    }

	  if (any_open)
	    {
	      if (jammed)
		{
		  wma(mw, item->_line + item->_filewr_lineoff + fline,
		      indent - 1,
		      A_BOLD, agocolor, "JM");
		}
	    }
	  else
	    {
	      if (hold)
		{
		  wma(mw, item->_line + item->_filewr_lineoff + fline,
		      indent - 1,
		      A_BOLD, agocolor, "HD");
		}
	      else
		{
		  wma(mw, item->_line + item->_filewr_lineoff + fline,
		      indent - 1,
		      A_BOLD, agocolor, "CL");
		}
	    }

	  if (filewr_block->_open_close_time._sec)
	    {
	      uint64_t filewr_ago   =
		item->_file_block->_time._sec -
		filewr_block->_open_close_time._sec;

	      lwrocmon_time_ago_str(str, filewr_ago);

	      wma(mw, item->_line + item->_filewr_lineoff + fline, indent + 2,
		  fileattrs, agocolor, "%7s", str);
	    }

	  {
	    /* Dummy.  We always grow.  And when we don't (new file),
	     * we do not want to remember old unit anyhow.
	     */
	    lwroc_format_diff_info diff_info;
	    memset(&diff_info, 0, sizeof (diff_info));

	    lwroc_format_prefix(str, sizeof (str),
				(double) filewr_block->_bytes, 8,
				&diff_info);
	    wma(mw, item->_line + item->_filewr_lineoff + fline, indent + 10,
		fileattrs, sizecolor, "%8s", str);
	  }

	  offs = lwroc_mon_text_scroll(52, strlen(filewr_block->_filename));

	  wma(mw, item->_line + item->_filewr_lineoff + fline, indent + 20,
	      fileattrs, filecolor,
	      "%-50.50s", filewr_block->_filename + offs);

	  fline++;
	}

      while (fline < (int) item->_num_filewr)
	{
	  wma(mw, item->_line + item->_filewr_lineoff + fline, indent - 1,
	      A_NORMAL, CTR_NONE, "%-60s", "");
	  wma(mw, item->_line + item->_filewr_lineoff + fline, indent + 20,
	      A_NORMAL, CTR_NONE, "-");
	  fline++;
	}

      if (any_open)
	{
	  lwroc_draw_mon_tree_file_rate(item, 1,
					item->_line + item->_filerate_lineoff,
					indent + 11,
					A_BOLD, nodecolor, 6);
	  lwroc_draw_mon_tree_file_rate(item, 0,
					item->_line + item->_filerate_lineoff,
					indent + 18,
					A_BOLD, CTR_GREEN, 8);
	}
      else
	{
	  wma(mw, item->_line + item->_filerate_lineoff, indent + 11,
	      A_NORMAL, CTR_RED, "%*s", 6, "-");
	  wma(mw, item->_line + item->_filerate_lineoff, indent + 18,
	      A_NORMAL, CTR_RED, "%*s", 8, "-");
	}

    }

  if (_config._print_ts_analysis &&
      item->_analyse_block)
    {
      uint32_t ana_tsb;
      int aline = item->_line + item->_ana_ts_lineoff;

      for (ana_tsb = 0; ana_tsb < item->_num_ana_ts; ana_tsb++)
	{
	  lwroc_monitor_ana_ts_block *ana_ts_block =
	    item->_ana_ts_block + ana_tsb;
	  lwroc_monitor_ana_ts_block *ana_ts_block_prev = NULL;
	  lwroc_ts_label_info label;
	  uint32_t ana_tsb_prev;

	  if (ana_ts_block->_ts_ref_off_sig_k == (uint64_t) -2)
	    continue;

	  /* Find the previous block for same id for differences. */
	  for (ana_tsb_prev = 0; ana_tsb_prev < item->_num_ana_ts;
	       ana_tsb_prev++)
	    {
	      lwroc_monitor_ana_ts_block *ana_ts_block_try =
		item->_ana_ts_block_prev + ana_tsb_prev;

	      if (ana_ts_block_try->_ts_ref_off_sig_k != (uint64_t) -2)
		{
		  if (ana_ts_block->_id == ana_ts_block_try->_id)
		    {
		      ana_ts_block_prev = ana_ts_block_try;
		      break;
		    }
		}
	    }

	  label._label = NULL;
	  label._ambiguity = 0;

	  /* fprintf (stderr, "---\nFind label: %04x\n", ana_ts_block->_id); */
	  lwroc_find_ts_id_label(item, ana_ts_block->_id, &label);
	  /* fprintf (stderr, "Found label: %04x -> \"%s\" (amb: %d)\n",
	     ana_ts_block->_id, label._label, label._ambiguity); */

	  if (label._label)
	    {
	      size_t offs;

	      offs = lwroc_mon_text_scroll(7, strlen(label._label));

	      wma(mw, aline, indent-4,
		  A_BOLD, CTR_NONE,
		  "%02x",
		  ana_ts_block->_id);

	      wma(mw, aline, indent-1,
		  A_BOLD, CTR_NONE,
		  "%7.7s", label._label + offs);
	    }
	  else
	    {
	      wma(mw, aline, indent-4,
		  A_BOLD, CTR_NONE,
		  "%04x",
		  ana_ts_block->_id << 8);
	    }

	  /*
	  wma(mw, aline, indent+41,
	      A_NORMAL, CTR_NONE,
	      "%016" PRIu64 "",
	      ana_ts_block->_src_index_mask);
	  */
	  if (ana_ts_block->_ts_ref_off_sig_k == (uint64_t) -1)
	    {
	      /* wchar_t sigma[]= { L'\u03c3', L'\0' }; */

	      if (_config._print_ts_analysis ==
		  LWROCMON_TS_ANALYSIS_SYNC_CHECK)
		{
		  wma(mw, aline, indent+7+1,
		      A_NORMAL, CTR_NONE, "ncv");
		  wma(mw, aline, indent+11+1,
		      A_NORMAL, CTR_NONE, "nrf");
		  wma(mw, aline, indent+15+2,
		      A_NORMAL, CTR_NONE, "good");
		  wma(mw, aline, indent+21+1,
		      A_NORMAL, CTR_NONE, "amb");
		  wma(mw, aline, indent+25+1,
		      A_NORMAL, CTR_NONE, "bad");
		  wma(mw, aline, indent+29+1,
		      A_NORMAL, CTR_NONE, "mis");
		  wma(mw, aline, indent+33+1,
		      A_NORMAL, CTR_NONE, "spu");
		  wma(mw, aline, indent+37+1,
		      A_NORMAL, CTR_NONE, "rel");
		  wma(mw, aline, indent+41+1,
		      A_NORMAL, CTR_NONE, "#o");
		}
	      else
		{
		  wma(mw, aline, indent+11,
		      A_NORMAL, CTR_NONE, "all-trig");

		  wmc(mw, aline, indent+25,
		      A_NORMAL, CTR_NONE, 103 | A_ALTCHARSET);
		  wma(mw, aline, indent+26,
		      A_NORMAL, CTR_NONE, "5o");
		  wmc(mw, aline, indent+31,
		      A_NORMAL, CTR_NONE, 103 | A_ALTCHARSET);
		  wma(mw, aline, indent+32,
		      A_NORMAL, CTR_NONE, "20o");
		  wma(mw, aline, indent+38,
		      A_NORMAL, CTR_NONE, "miss");
		}

	      wma(mw, aline, indent+49,
		  A_NORMAL, CTR_NONE, "sync-trig");

	      wma(mw, aline, indent+61,
		  A_NORMAL, CTR_NONE, ">20o");

	      /* waddwstr(mw, sigma); */

	      aline++;
	      continue;
	    }

	  if (_config._print_ts_analysis ==
	      LWROCMON_TS_ANALYSIS_SYNC_CHECK)
	    {
	      lwroc_monitor_ana_ts_block *ptr_block = ana_ts_block;
	      lwroc_monitor_ana_ts_block *ptr_prev  = ana_ts_block_prev;

	      int has_nocv_rate = 0;
	      double nocv_rate = -1.;
	      int has_noref_rate = 0;
	      double noref_rate = -1.;
	      int has_good_rate = 0;
	      double good_rate = -1.;
	      int has_mism_rate = 0;
	      double amb_rate = -1.;
	      int has_amb_rate = 0;
	      double mism_rate = -1.;
	      int has_miss_rate = 0;
	      double miss_rate = -1.;
	      int has_spur_rate = 0;
	      double spur_rate = -1.;

	      if (ptr_prev)
		{
		  MONITOR_STRUCT_FREQ;

		  has_nocv_rate = (ptr_block->_sync_check_no_checkval >
				   ptr_prev->_sync_check_no_checkval);

		  nocv_rate = (double) (ptr_block->_sync_check_no_checkval -
					ptr_prev->_sync_check_no_checkval);

		  has_noref_rate = (ptr_block->_sync_check_no_ref >
				   ptr_prev->_sync_check_no_ref);

		  noref_rate = (double) (ptr_block->_sync_check_no_ref -
					ptr_prev->_sync_check_no_ref);

		  has_good_rate = (ptr_block->_sync_check_good >
				   ptr_prev->_sync_check_good);

		  good_rate = (double) (ptr_block->_sync_check_good -
					ptr_prev->_sync_check_good);

		  has_amb_rate = (ptr_block->_sync_check_ambiguous >
				  ptr_prev->_sync_check_ambiguous);

		  amb_rate = (double) (ptr_block->_sync_check_ambiguous -
				       ptr_prev->_sync_check_ambiguous);

		  has_mism_rate = (ptr_block->_sync_check_mismatch >
				   ptr_prev->_sync_check_mismatch);

		  mism_rate = (double) (ptr_block->_sync_check_mismatch -
					ptr_prev->_sync_check_mismatch);

		  has_miss_rate = (ptr_block->_sync_check_missing >
				   ptr_prev->_sync_check_missing);

		  miss_rate = (double) (ptr_block->_sync_check_missing -
					ptr_prev->_sync_check_missing);

		  has_spur_rate = (ptr_block->_sync_check_spurious >
				   ptr_prev->_sync_check_spurious);

		  spur_rate = (double) (ptr_block->_sync_check_spurious -
					ptr_prev->_sync_check_spurious);

		  nocv_rate  *= frequency;
		  noref_rate *= frequency;
		  good_rate  *= frequency;
		  amb_rate   *= frequency;
		  mism_rate  *= frequency;
		  miss_rate  *= frequency;
		  spur_rate  *= frequency;
		}

	      lwroc_draw_mon_tree_rate(has_nocv_rate, nocv_rate,
				       aline, indent+7,
				       A_BOLD, CTR_RED, 4,
				       NULL);
	      lwroc_draw_mon_tree_rate(has_noref_rate, noref_rate,
				       aline, indent+11,
				       A_BOLD, CTR_RED, 4,
				       NULL);
	      lwroc_draw_mon_tree_rate(has_good_rate, good_rate,
				       aline, indent+15,
				       A_BOLD, CTR_GREEN, 6,
				       NULL);
	      lwroc_draw_mon_tree_rate(has_amb_rate, amb_rate,
				       aline, indent+21,
				       A_BOLD, CTR_YELLOW_BG_RED, 4,
				       NULL);
	      lwroc_draw_mon_tree_rate(has_mism_rate, mism_rate,
				       aline, indent+25,
				       A_BOLD, CTR_WHITE_BG_RED, 4,
				       NULL);
	      lwroc_draw_mon_tree_rate(has_miss_rate, miss_rate,
				       aline, indent+29,
				       A_BOLD, CTR_MAGENTA, 4,
				       NULL);
	      lwroc_draw_mon_tree_rate(has_spur_rate, spur_rate,
				       aline, indent+33,
				       A_BOLD, CTR_BLUE, 4,
				       NULL);

	      if (ptr_block->_sync_check_sigmas_k > 9900 /* > 9.9 * 1000 */)
		wma(mw, aline, indent+41,
		    A_BOLD, CTR_NONE,
		    "%3.0f", ptr_block->_sync_check_sigmas_k * 0.001);
	      else if (ptr_block->_sync_check_sigmas_k > 0)
		wma(mw, aline, indent+41,
		    A_BOLD, CTR_NONE,
		    "%3.1f", ptr_block->_sync_check_sigmas_k * 0.001);
	      else
		wma(mw, aline, indent+41,
		    A_BOLD, CTR_NONE,
		    "%3.1s","-");
	    }
	  else
	    {
	      if (ana_ts_block->_ts_ref_off_sig_k == (uint64_t) -3)
		{
		  wma(mw, aline, indent+7,
		      A_NORMAL, CTR_GREEN,
		      "%6.1s","-");

		  wma(mw, aline, indent+16,
		      A_NORMAL, CTR_GREEN,
		      "%-4.1s","-");

		  wma(mw, aline, indent+22,
		      A_NORMAL, CTR_GREEN,
		      "%6.1s","-");
		  wma(mw, aline, indent+29,
		      A_NORMAL, CTR_NONE,
		      "%6.1s","-");
		  wma(mw, aline, indent+36,
		      A_NORMAL, CTR_NONE,
		      "%6.1s","-");
		}
	      else
		{
		  double offset = (double) (int64_t)
		    ana_ts_block->_ts_ref_offset_k * 0.001;
		  double stddev = (double) (int64_t)
		    ana_ts_block->_ts_ref_off_sig_k * 0.001;

		  if (fabs(offset) < 999999)
		    wma(mw, aline, indent+7,
			A_BOLD, CTR_GREEN,
			"%6.0f", offset);
		  else
		    wma(mw, aline, indent+7,
			A_BOLD, CTR_GREEN,
			"%5.0fk", offset * 0.001);

		  wmc(mw, aline, indent+14,
		      A_NORMAL, CTR_GREEN, 103 | A_ALTCHARSET);

		  wma(mw, aline, indent+16,
		      A_BOLD, CTR_GREEN,
		      "%-4.0f", stddev);

		  wma_percent(mw, aline, indent+22,
			      A_BOLD, CTR_GREEN, 6,
			      (double) (int64_t)
			      ana_ts_block->_within5sigma_k * 0.001);
		  wma_percent(mw, aline, indent+29,
			      A_BOLD, CTR_NONE, 6,
			      (double) (int64_t)
			      ana_ts_block->_within20sigma_k * 0.001);
		  wma_percent(mw, aline, indent+36,
			      A_BOLD, CTR_NONE, 6,
			      (double) (int64_t)
			      ana_ts_block->_outside_k * 0.001);
		  /*
		    wma(mw, item->_line + item->_ana_ts_lineoff + aline,
			indent+11,
			A_BOLD, CTR_GREEN,
			"%-4.1f",
			(double) (int64_t)
			ana_ts_block->_ts_ref_off_sig_k * 0.0001);
		  */
		}
	    }

	  if (ana_ts_block->_ts_sync_ref_off_sig_k == (uint64_t) -3)
	    {
	      wma(mw, aline, indent+44,
		  A_NORMAL, CTR_GREEN,
		  "%8.1s","-");

	      wma(mw, aline, indent+55,
		  A_NORMAL, CTR_GREEN,
		  "%-4.1s","-");

	      wma(mw, aline, indent+61,
		  A_NORMAL, CTR_GREEN,
		  "%4.1s","-");
	    }
	  else
	    {
	      wma(mw, aline, indent+44,
		  A_BOLD, CTR_GREEN,
		  "%8.0f",
		  (double) (int64_t)
		  ana_ts_block->_ts_sync_ref_offset_k * 0.001);

	      wmc(mw, aline, indent+53,
		  A_NORMAL, CTR_GREEN, 103 | A_ALTCHARSET);

	      wma(mw, aline, indent+55,
		  A_BOLD, CTR_GREEN,
		  "%-4.0f",
		  (double) (int64_t)
		  ana_ts_block->_ts_sync_ref_off_sig_k * 0.001);

	      wma_percent(mw, aline, indent+61,
			  A_BOLD, CTR_GREEN, 4,
			  (double) (int64_t)
			  ana_ts_block->_sync_outside20sigma_k * 0.001);
	    }
	  aline++;
	}
    }

  if (_config._print_dams &&
      item->_dams_block)
    {
      uint32_t damb;
      int aline = item->_line + item->_dam_lineoff;

      MONITOR_STRUCT_FREQ_PTRS(item->_dams_block, item->_dams_block_prev);

      if (item->_dam_lineoff != -1)
	{
	  wma(mw, aline, indent+3,
	      A_NORMAL, CTR_NONE, "ID");
	  wma(mw, aline, indent+6+1,
	      A_NORMAL, CTR_NONE, "evnts");
	  wma(mw, aline, indent+12+1,
	      A_NORMAL, CTR_NONE, "trig");
	  wma(mw, aline, indent+17+2,
	      A_NORMAL, CTR_NONE, "hits");
	  wma(mw, aline, indent+23+1,
	      A_NORMAL, CTR_NONE, "ovfl");
	  wma(mw, aline, indent+28+3,
	      A_NORMAL, CTR_NONE, "pu");
	  wma(mw, aline, indent+33+2,
	      A_NORMAL, CTR_NONE, "bad");
	  wma(mw, aline, indent+38+1,
	      A_NORMAL, CTR_NONE, "miss");
	  wma(mw, aline, indent+43+1,
	      A_NORMAL, CTR_NONE, "filt");
	  wma(mw, aline, indent+48+1,
	      A_NORMAL, CTR_NONE, "drop");

	  aline++;
	}

      for (damb = 0; damb < item->_num_dam; damb++)
	{
	  lwroc_monitor_dam_block *dam_block =
	    item->_dam_block + damb;
	  lwroc_monitor_dam_block *dam_block_prev =
	    item->_dam_block_prev ?
	    item->_dam_block_prev + damb : NULL;
	  /* lwroc_ts_label_info label; */

	  if (dam_block->_id == (uint32_t) -1)
	    continue;

	  /* Find the previous block for same id for differences. */

	  {
	    lwroc_monitor_dam_block *ptr_block = dam_block;
	    lwroc_monitor_dam_block *ptr_prev  = dam_block_prev;

	    int has_events_rate = 0;
	    double events_rate = -1.;
	    int has_trig_sig_rate = 0;
	    double trig_sig_rate = -1.;
	    int has_hits_rate = 0;
	    double hits_rate = -1.;
	    int has_hits_ovfl_rate = 0;
	    double hits_ovfl_rate = -1.;
	    int has_hits_pileup_rate = 0;
	    double hits_pileup_rate = -1.;
	    int has_hits_bad_rate = 0;
	    double hits_bad_rate = -1.;
	    int has_hits_missed_rate = 0;
	    double hits_missed_rate = -1.;
	    int has_hits_filt_rate = 0;
	    double hits_filt_rate = -1.;
	    int has_hits_drop_rate = 0;
	    double hits_drop_rate = -1.;

	    if (ptr_prev)
	      {
		has_events_rate = (ptr_block->_events >
				   ptr_prev->_events);

		events_rate = (double) (ptr_block->_events -
					ptr_prev->_events);

		has_trig_sig_rate = (ptr_block->_trig_sig >
				     ptr_prev->_trig_sig);

		trig_sig_rate = (double) (ptr_block->_trig_sig -
					  ptr_prev->_trig_sig);

		has_hits_rate = (ptr_block->_hits >
				 ptr_prev->_hits);

		hits_rate = (double) (ptr_block->_hits -
				      ptr_prev->_hits);

		has_hits_ovfl_rate = (ptr_block->_hits_ovfl >
				      ptr_prev->_hits_ovfl);

		hits_ovfl_rate = (double) (ptr_block->_hits_ovfl -
					   ptr_prev->_hits_ovfl);

		has_hits_pileup_rate = (ptr_block->_hits_pileup >
					ptr_prev->_hits_pileup);

		hits_pileup_rate = (double) (ptr_block->_hits_pileup -
					     ptr_prev->_hits_pileup);

		has_hits_bad_rate = (ptr_block->_hits_bad >
				     ptr_prev->_hits_bad);

		hits_bad_rate = (double) (ptr_block->_hits_bad -
					  ptr_prev->_hits_bad);

		has_hits_missed_rate = (ptr_block->_hits_missed >
					ptr_prev->_hits_missed);

		hits_missed_rate = (double) (ptr_block->_hits_missed -
					     ptr_prev->_hits_missed);

		has_hits_filt_rate = (ptr_block->_hits_filt >
				      ptr_prev->_hits_filt);

		hits_filt_rate = (double) (ptr_block->_hits_filt -
					   ptr_prev->_hits_filt);

		has_hits_drop_rate = (ptr_block->_hits_drop >
				      ptr_prev->_hits_drop);

		hits_drop_rate = (double) (ptr_block->_hits_drop -
					   ptr_prev->_hits_drop);

		events_rate *= frequency;
		trig_sig_rate *= frequency;
		hits_rate *= frequency;
		hits_ovfl_rate *= frequency;
		hits_pileup_rate *= frequency;
		hits_bad_rate *= frequency;
		hits_missed_rate *= frequency;
		hits_filt_rate *= frequency;
		hits_drop_rate *= frequency;
	      }

	    wma(mw, aline, indent+3,
		A_BOLD, CTR_NONE,
		"%d", dam_block->_id);

	    lwroc_draw_mon_tree_rate(has_events_rate, events_rate,
				     aline, indent+6,
				     A_BOLD, CTR_GREEN, 6,
				     NULL);
	    lwroc_draw_mon_tree_rate(has_trig_sig_rate, trig_sig_rate,
				     aline, indent+12,
				     A_BOLD, CTR_GREEN, 5,
				     NULL);
	    lwroc_draw_mon_tree_rate(has_hits_rate, hits_rate,
				     aline, indent+17,
				     A_BOLD, CTR_GREEN, 6,
				     NULL);
	    lwroc_draw_mon_tree_rate(has_hits_ovfl_rate, hits_ovfl_rate,
				     aline, indent+23,
				     A_BOLD, CTR_RED, 5,
				     NULL);
	    lwroc_draw_mon_tree_rate(has_hits_pileup_rate, hits_pileup_rate,
				     aline, indent+28,
				     A_BOLD, CTR_RED, 5,
				     NULL);
	    lwroc_draw_mon_tree_rate(has_hits_bad_rate, hits_bad_rate,
				     aline, indent+33,
				     A_BOLD, CTR_WHITE_BG_RED, 5,
				     NULL);
	    lwroc_draw_mon_tree_rate(has_hits_missed_rate, hits_missed_rate,
				     aline, indent+38,
				     A_BOLD, CTR_WHITE_BG_RED, 5,
				     NULL);
	    lwroc_draw_mon_tree_rate(has_hits_filt_rate, hits_filt_rate,
				     aline, indent+43,
				     A_BOLD, CTR_GREEN, 5,
				     NULL);
	    lwroc_draw_mon_tree_rate(has_hits_drop_rate, hits_drop_rate,
				     aline, indent+48,
				     A_BOLD, CTR_WHITE_BG_RED, 5,
				     NULL);
	  }
	  aline++;
	}
    }
}

void lwroc_draw_mon_tree_shortcuts(void)
{
  int y = 0, x = 35;

  wma(mw, y, x,   A_REVERSE, CTR_NONE, "s");
  wma(mw, y, x+2, A_NORMAL, CTR_NONE,
      "%-6s",
      _config._print_ts_analysis ?
      (_config._print_ts_analysis == LWROCMON_TS_ANALYSIS_SYNC_CHECK ?
       "ts-chk" : "ts-ana") :
      "-");

  wma(mw, y, x+9,  A_REVERSE, CTR_NONE, "t");
  wma(mw, y, x+11, A_NORMAL, CTR_NONE,
      "%-6s",
      _config._print_ts_track == LWROCMON_PRINT_TS_TRACK_RO_TRACK ? "ts-trk" :
      _config._print_ts_track == LWROCMON_PRINT_TS_TRACK_TO_INPUT ? "ts-inp" :
      _config._print_ts_track == LWROCMON_PRINT_TS_TRACK_BOTH ? "ts-t-i" :
      "-");

  wma(mw, y, x+18, A_REVERSE, CTR_NONE, "r");
  wma(mw, y, x+20, A_NORMAL, CTR_NONE,
      "%-7s",
      _config._print_raw_ts ?
      (_config._print_raw_ts == LWROCMON_PRINT_RAW_TS_DECIMAL ?
       "raw-dec" :
       (_config._print_raw_ts == LWROCMON_PRINT_RAW_TS_HEX ?
	"raw-hex" : "raw-hum")) :
      "-");

  wma(mw, y, x+28, A_REVERSE, CTR_NONE, "b");
  wma(mw, y, x+30, A_NORMAL, CTR_NONE,
      "%-3s",
      _config._print_buf_size ? "-" : "buf");

  wma(mw, y, x+34, A_REVERSE, CTR_NONE, "d");
  wma(mw, y, x+36, A_NORMAL, CTR_NONE,
      "%-3s",
      _config._print_dams ? "dam" : "-");
}

void lwroc_draw_mon_tree_items(void)
{
  pd_ll_item *iter;

  lwroc_extents_mon_tree_items();

  lwroc_draw_mon_tree_global_timestamp();

  lwroc_draw_mon_tree_shortcuts();

  lwroc_draw_extents_mon_tree_items();

  /* fprintf (stderr,"--\n"); */

  PD_LL_FOREACH(_lwroc_mon_tree_items, iter)
    {
      lwroc_mon_tree_item *item =
	PD_LL_ITEM(iter, lwroc_mon_tree_item, _items);

      lwroc_draw_mon_tree_item(item);
      /*
      fprintf (stderr, "%d,%d %s:%d\n",
	       item->_indent, item->_line,
	       item->_us ? item->_us->_hostname : "-",
	       lwroc_get_port(&item->_addr));
      */
    }
}

void lwroc_update_conn_hints(void)
{
  pd_ll_item *iter;
  pd_ll_item *iter2;

  /* Update hints in the (long-lived) connection structures.
   *
   * So far: who is the master of each slave.
   */

  /* First: clear all (possibly out-of-date) information). */

  PD_LL_FOREACH(_lwrocmon_conns, iter)
    {
      lwrocmon_conn *conn =
	PD_LL_ITEM(iter, lwrocmon_conn, _conns);

      conn->_hint_master = NULL;

      /* printf ("clean...\n"); */
    }

  /* Then loop over all items we know. */
  PD_LL_FOREACH(_lwroc_mon_tree_items, iter)
    {
      lwroc_mon_tree_item *item =
	PD_LL_ITEM(iter, lwroc_mon_tree_item, _items);

      /* printf ("item...\n"); */

      PD_LL_FOREACH(item->_masters, iter2)
	{
	  lwroc_mon_tree_conn *conn =
	    PD_LL_ITEM(iter2, lwroc_mon_tree_conn, d[LWROC_MTCD_OUT]._conns);

	  lwroc_monitor_conn_block *conn_block_a;
	  lwroc_monitor_conn_block *conn_block_b;

	  conn_block_a = conn->d[LWROC_MTCD_IN ]._conn_block;
	  conn_block_b = conn->d[LWROC_MTCD_OUT]._conn_block;

	  /*
	  printf ("slave-master... [%p,%p] (%08x,%08x)\n",
		  conn_block_a,
		  conn_block_b,
		  conn_block_a ? conn_block_a->_status : 0,
		  conn_block_b ? conn_block_b->_status : 0);
	  */

	  /* We only use the connection as hint if the status is
	   * connected from both sides.
	   */
	  if (conn_block_a &&
	      conn_block_b &&
	      conn_block_a->_status == LWROC_CONN_STATUS_CONNECTED &&
	      conn_block_b->_status == LWROC_CONN_STATUS_CONNECTED)
	    {
	      lwroc_mon_tree_item *tree_master;

	      tree_master = conn->d[LWROC_MTCD_IN]._tree_item;

	      /*
	      printf ("master... %s%s [%p]\n",
		      item->_us->_hostname,
		      item->_us->_port_str,
		      tree_master);
	      */

	      if (tree_master)
		item->_us->_hint_master = tree_master->_us;
	    }
	}
    }
}




void lwroc_mon_tree_item_conn(lwroc_mon_tree_item *tree_item,
			      lwroc_monitor_conn_source *conn_source,
			      lwroc_monitor_conn_block *conn_block,
			      lwroc_monitor_conn_block *conn_block_prev)
{
  lwroc_mon_tree_conn *tree_conn;

  struct sockaddr_storage want_addr;

  int d =
    (conn_source->_direction == LWROC_CONN_DIR_INCOMING ?
     LWROC_MTCD_IN : LWROC_MTCD_OUT);

  (void) conn_block;

  /* Where does this connection want to be connected? */

  lwroc_set_ipv4_addr(&want_addr,
		      conn_source->_ipv4_addr);
  lwroc_set_port(&want_addr, conn_source->_port);

  tree_conn = malloc (sizeof (lwroc_mon_tree_conn));

  if (!tree_conn)
    LWROC_FATAL("Memory allocation error.\n");

  /*fprintf (stderr,"alloc tree_conn: %p\n", tree_conn);*/

  memset (tree_conn, 0, sizeof (*tree_conn));

  tree_conn->d[d]._addr = want_addr;

  tree_conn->d[d]._tree_item       = tree_item;
  tree_conn->d[d]._conn_source     = conn_source;
  tree_conn->d[d]._conn_block      = conn_block;
  tree_conn->d[d]._conn_block_prev = conn_block_prev;

  tree_conn->_timesort_src_index_mask = 0;

  PD_LL_INIT(&tree_conn->d[LWROC_MTCD_IN]._conns);
  PD_LL_INIT(&tree_conn->d[LWROC_MTCD_OUT]._conns);

  PD_LL_ADD_BEFORE(&_lwroc_mon_tree_conns, &tree_conn->_conns);

  switch (conn_source->_type)
    {
    case LWROC_REQUEST_DATA_TRANS:
    case LWROC_CONN_TYPE_TRANS:
    case LWROC_CONN_TYPE_FNET:
    case LWROC_CONN_TYPE_STREAM:
    case LWROC_CONN_TYPE_EBYE_PUSH:
    case LWROC_CONN_TYPE_EBYE_PULL:
    case LWROC_CONN_TYPE_FILE:
      if (conn_source->_direction == LWROC_CONN_DIR_OUTGOING)
	PD_LL_ADD_BEFORE(&tree_item->_data_srcs, &tree_conn->d[d]._conns);
      else
	PD_LL_ADD_BEFORE(&tree_item->_data_dests, &tree_conn->d[d]._conns);
      return;

    case LWROC_REQUEST_TRIVAMI_BUS:
      if (conn_source->_direction == LWROC_CONN_DIR_OUTGOING)
	PD_LL_ADD_BEFORE(&tree_item->_masters, &tree_conn->d[d]._conns);
      else
	PD_LL_ADD_BEFORE(&tree_item->_slaves, &tree_conn->d[d]._conns);
      return;

    case LWROC_REQUEST_TRIVAMI_EB:
      if (conn_source->_direction == LWROC_CONN_DIR_OUTGOING)
	PD_LL_ADD_BEFORE(&tree_item->_eb_masters, &tree_conn->d[d]._conns);
      else
	PD_LL_ADD_BEFORE(&tree_item->_ebs, &tree_conn->d[d]._conns);
      return;

    case LWROC_CONN_TYPE_SYSTEM:
      tree_item->_system = tree_conn;
      return;

    case LWROC_CONN_TYPE_FILTER:
      tree_item->_filter = tree_conn;
      return;
    }

  PD_LL_REMOVE(&tree_conn->_conns);

  free (tree_conn);
}

lwroc_mon_tree_item *lwroc_mon_tree_item_duplicate(lwrocmon_conn *conn)
{
  lwroc_monitor_main_source *conn_main_source;
  lwroc_monitor_main_source *item_main_source;
  pd_ll_item *iter;

  conn_main_source = conn->_mon_main_source;

  PD_LL_FOREACH(_lwroc_mon_tree_items, iter)
    {
      lwroc_mon_tree_item *item =
	PD_LL_ITEM(iter, lwroc_mon_tree_item, _items);

      item_main_source = item->_us->_mon_main_source;

      if (item_main_source && conn_main_source &&
	  item_main_source->_mon_ident1 == conn_main_source->_mon_ident1 &&
	  item_main_source->_mon_ident2 == conn_main_source->_mon_ident2 &&
	  item_main_source->_mon_ident3 == conn_main_source->_mon_ident3 &&
	  item_main_source->_mon_ident4 == conn_main_source->_mon_ident4)
	{
	  lwroc_mon_tree_item_dupl *dupl =
	    lwroc_mon_tree_item_dupl_alloc(item);

	  dupl->_us = conn;
	  dupl->_addr = conn->_serv_addr;

	  return item;
	}
    }

  return NULL;
}

void lwrocmon_getch(void)
{
  /* Get whatever user input may be pending. */

  for ( ; ; )
    {
      int key = wgetch(mw);

      if (key == ERR)
	break;

      if (key == 'b')
	_config._print_buf_size = !_config._print_buf_size;
      if (key == 'd')
	_config._print_dams = !_config._print_dams;
      if (key == 's')
	_config._print_ts_analysis =
	  (_config._print_ts_analysis + 1) % LWROCMON_TS_ANALYSIS_NUM;
      if (key == 't')
	_config._print_ts_track =
	  (_config._print_ts_track + 1) % LWROCMON_PRINT_TS_TRACK_NUM;
      if (key == 'r')
	{
	  _config._print_raw_ts =
	    (_config._print_raw_ts + 1) % LWROCMON_PRINT_RAW_TS_NUM;
	  if (_config._print_raw_ts)
	    _config._print_ts_track = LWROCMON_PRINT_TS_TRACK_BOTH;
	}
    }
}

void lwrocmon_tree_source_update(void)
{
  /* TODO:
   * Since we use werase() now, we can probably get rid of this completely?
   */
  wclear(mw);

  _num_source_update++;
}

void lwroc_multi_column(void)
{
  int ysize, xsize;
  int extra_cols;
  int i;

  getmaxyx(mw, ysize, xsize);

  /* printf ("%d x %d\n", ysize, xsize); */

  extra_cols = (xsize - 1) / 81;

  _lwroc_multi_col._extra_cols = extra_cols;
  _lwroc_multi_col._col_breaks =
    realloc(_lwroc_multi_col._col_breaks,
	    ((unsigned int) _lwroc_multi_col._extra_cols) * sizeof (int));

  for (i = 0; i < _lwroc_multi_col._extra_cols; i++)
    _lwroc_multi_col._col_breaks[i] = ysize * (i + 1);
}

void lwrocmon_tree_block_update(void)
{
  pd_ll_item *iter;
  pd_ll_item *tmp_iter;
  pd_ll_item *iter2;
  pd_ll_item *tmp_iter2;

  lwrocmon_getch();

  /* Free all items and connections.  The local representation, which
   * has pointers to the actual holders of the information.  The
   * actual holders are not freed.
   */

  PD_LL_FOREACH_TMP(_lwroc_mon_tree_items, iter, tmp_iter)
    {
      lwroc_mon_tree_item *item =
	PD_LL_ITEM(iter, lwroc_mon_tree_item, _items);

      PD_LL_FOREACH_TMP(item->_duplicates, iter2, tmp_iter2)
	{
	  lwroc_mon_tree_item_dupl *dupl =
	    PD_LL_ITEM(iter2, lwroc_mon_tree_item_dupl, _items);

	  free(dupl);
	}
      free(item);
    }

  PD_LL_FOREACH_TMP(_lwroc_mon_tree_conns, iter, tmp_iter)
    {
      lwroc_mon_tree_conn *conn =
	PD_LL_ITEM(iter, lwroc_mon_tree_conn, _conns);
      free(conn);
    }

  /* All items were freed. */
  PD_LL_INIT(&_lwroc_mon_tree_items);
  PD_LL_INIT(&_lwroc_mon_tree_conns);

  PD_LL_FOREACH(_lwrocmon_conns, iter)
    {
      lwrocmon_conn *conn =
	PD_LL_ITEM(iter, lwrocmon_conn, _conns);
      lwrocmon_src_info *item;

      lwroc_mon_tree_item *tree_item;

      /* Check that this is not a duplicate. */
      if (lwroc_mon_tree_item_duplicate(conn))
	continue;

      tree_item = lwroc_mon_tree_item_alloc();

      tree_item->_us = conn;
      tree_item->_addr = conn->_serv_addr;

      for (item = conn->_mon_sources; item; item = item->_next)
	{
	  switch (item->_type_magic)
	    {
	    case LWROC_MONITOR_MAIN_SOURCE_SERPROTO_MAGIC1:
	      {
		lwroc_monitor_main_source *main_source =
		  (lwroc_monitor_main_source *) item->_source_info;
		lwroc_monitor_main_block *main_block =
		  (lwroc_monitor_main_block *) item->_data[conn->_good_data];
		/*
		printf ("main mon... %" PRIu64 "\n",
			main_block->_last_timestamp);
		*/
		tree_item->_main_source = main_source;
		tree_item->_main_block = main_block;
		break;
	      }
	    case LWROC_MONITOR_FILTER_SOURCE_SERPROTO_MAGIC1:
	      {
		lwroc_monitor_filter_source *filter_source =
		  (lwroc_monitor_filter_source *) item->_source_info;
		lwroc_monitor_filter_block *filter_block =
		  (lwroc_monitor_filter_block *) item->_data[conn->_good_data];
		/*
		printf ("filter mon... %" PRIu64 "\n",
			filter_block->_last_timestamp);
		*/
		tree_item->_filter_source = filter_source;
		tree_item->_filter_block = filter_block;
		break;
	      }
	    case LWROC_MONITOR_NET_SOURCE_SERPROTO_MAGIC1:
	      {
		lwroc_monitor_net_source *ptr_source =
		  (lwroc_monitor_net_source *) item->_source_info;
		lwroc_monitor_net_block *net_block =
		  (lwroc_monitor_net_block *) item->_data[conn->_good_data];
		lwroc_monitor_net_block *net_block_prev =
		  (lwroc_monitor_net_block *) item->_data[conn->_prev_data];
		uint32_t connb;

		tree_item->_net_block = net_block;
		tree_item->_net_block_prev = net_block_prev;

		for (connb = 0; connb < ptr_source->_conn_blocks; connb++)
		  {
		    lwroc_monitor_conn_source *conn_source =
		      item->_conn_source_info + connb;
		    lwroc_monitor_conn_block *conn_block =
		      item->_conn_data[conn->_good_data] + connb;
		    lwroc_monitor_conn_block *conn_block_prev =
		      item->_conn_data[conn->_prev_data] + connb;

		    /* TODO: this can move out, or elsewhere.  Is a hack. */
		    /* Need not be set every time... */
		    if (timerisset(&conn->_stale_expire))
		      conn_block->_status = STATUS_EXPIRED;

		    lwroc_mon_tree_item_conn(tree_item,
					     conn_source, conn_block,
					     conn_block_prev);
		  }
		break;
	      }
	    case LWROC_MONITOR_FILE_SOURCE_SERPROTO_MAGIC1:
	      {
		lwroc_monitor_file_source *ptr_source =
		  (lwroc_monitor_file_source *) item->_source_info;
		lwroc_monitor_file_block *file_block =
		  (lwroc_monitor_file_block *) item->_data[conn->_good_data];
		lwroc_monitor_file_block *file_block_prev =
		  (lwroc_monitor_file_block *) item->_data[conn->_prev_data];

		tree_item->_file_block      = file_block;
		tree_item->_file_block_prev = file_block_prev;

		if (ptr_source->_filewr_blocks)
		  {
		    tree_item->_num_filewr = ptr_source->_filewr_blocks;
		    tree_item->_filewr_block =
		      item->_filewr_data[conn->_good_data];
		    tree_item->_filewr_block_prev =
		      item->_filewr_data[conn->_prev_data];
		  }
		break;
	      }
	    case LWROC_MONITOR_ANALYSE_SOURCE_SERPROTO_MAGIC1:
	      {
		lwroc_monitor_analyse_source *ptr_source =
		  (lwroc_monitor_analyse_source *) item->_source_info;
		lwroc_monitor_analyse_block *analyse_block =
		  (lwroc_monitor_analyse_block *) item->_data[conn->_good_data];
		lwroc_monitor_analyse_block *analyse_block_prev =
		  (lwroc_monitor_analyse_block *) item->_data[conn->_prev_data];

		tree_item->_analyse_block      = analyse_block;
		tree_item->_analyse_block_prev = analyse_block_prev;

		if (ptr_source->_ana_ts_blocks)
		  {
		    tree_item->_num_ana_ts = ptr_source->_ana_ts_blocks;
		    tree_item->_ana_ts_block =
		      item->_ana_ts_data[conn->_good_data];
		    tree_item->_ana_ts_block_prev =
		      item->_ana_ts_data[conn->_prev_data];
		  }
		break;
	      }
	    case LWROC_MONITOR_DAMS_SOURCE_SERPROTO_MAGIC1:
	      {
		lwroc_monitor_dams_source *ptr_source =
		  (lwroc_monitor_dams_source *) item->_source_info;
		lwroc_monitor_dams_block *dams_block =
		  (lwroc_monitor_dams_block *) item->_data[conn->_good_data];
		lwroc_monitor_dams_block *dams_block_prev =
		  (lwroc_monitor_dams_block *) item->_data[conn->_prev_data];

		tree_item->_dams_block      = dams_block;
		tree_item->_dams_block_prev = dams_block_prev;

		if (ptr_source->_dam_blocks)
		  {
		    tree_item->_num_dam = ptr_source->_dam_blocks;
		    tree_item->_dam_block =
		      item->_dam_data[conn->_good_data];
		    tree_item->_dam_block_prev =
		      item->_dam_data[conn->_prev_data];
		  }
		break;
	      }
	    }
	}
    }

  lwroc_connect_mon_tree_items();

  lwroc_place_mon_tree_items();

  lwroc_combine_time_track_items();

  lwroc_multi_column();

  werase(mw);

  lwroc_draw_mon_tree_items();

  lwroc_update_conn_hints();

  _num_block_update++;

  {
    char spinner[] = "|/-\\";
    char jumper[] = "_.-^'|";

    wma(mw, 0, 77, A_NORMAL, CTR_MAGENTA, "%c%c",
	spinner[_num_block_update % (int) strlen(spinner)],
	jumper[_num_source_update % (int) strlen(jumper)]);
  }

  wrefresh(mw);
}

#else/*!HAS_CURSES*/

void lwrocmon_tree_source_update(void) { }

void lwrocmon_tree_block_update(void) { }

#endif/*!HAS_CURSES*/

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwrocmon_struct.h"
#include "lwroc_mon.h"

#include "lwroc_hostname_util.h"
#include "lwroc_message.h"
#include "lwroc_net_io_util.h"

#include <errno.h>
#include <assert.h>
#include <string.h>

#include "../dtc_arch/acc_def/fall_through.h"

/********************************************************************/

void lwrocmon_udp_hint_setup_select(lwroc_select_item *item,
				    lwroc_select_info *si)
{
  lwrocmon_udp_hint *udp_hint =
    PD_LL_ITEM(item, lwrocmon_udp_hint, _select_item);

  LWROC_READ_FD_SET(udp_hint->_fd, si);
}

int lwrocmon_udp_hint_after_select(lwroc_select_item *item,
				   lwroc_select_info *si)
{
  lwrocmon_udp_hint *udp_hint =
    PD_LL_ITEM(item, lwrocmon_udp_hint, _select_item);

  if (LWROC_READ_FD_ISSET(udp_hint->_fd, si))
    {
      /* See what hint someone wants to send us. */

      uint32_t packet[4];
      ssize_t n;
      struct sockaddr_storage src_addr;
      socklen_t addrlen;
      int i;

      addrlen = sizeof (src_addr);

      n = recvfrom(udp_hint->_fd, &packet, sizeof (packet),
		   0,
		   (struct sockaddr *) &src_addr,
		   &addrlen);

      if (n == 4 * sizeof (uint32_t))
	{
	  /* Only deal with packets of the expected size. */

	  for (i = 0; i < 4; i++)
	    packet[i] = ntohl(packet[i]);

	  if (packet[0] == _lwrocmon_hint_ident[0] &&
	      packet[1] == _lwrocmon_hint_ident[1])
	    {
	      char dotted[INET6_ADDRSTRLEN];
	      uint16_t port;

	      /* Find connections at the given address and port and
	       * reactivate them for immediate reconnection attempt if
	       * they are sleeping.
	       */

	      /* We compare to the address where the packet came from,
	       * but with the port that it sent us, not which it used to
	       * send the message.
	       */

	      port = (uint16_t) packet[2];

	      lwroc_set_port(&src_addr, port);

	      if (!_config._show_detail &&
		  !_config._show_tree)
		{
		  lwroc_inet_ntop(&src_addr, dotted, sizeof (dotted));
		  LWROC_INFO_FMT("Awaken hint from %s:%d.",
				 dotted, port);
		}

	      lwrocmon_awaken_sleeping(&src_addr, si);
	    }
	}
    }

  return 1;
}

/********************************************************************/

lwroc_select_item_info lwrocmoc_udp_hint_select_item_info =
{
  lwrocmon_udp_hint_setup_select,
  lwrocmon_udp_hint_after_select,
  NULL,
  NULL,
  0,
};

/********************************************************************/

uint32_t _lwrocmon_hint_ident[2];

/********************************************************************/

void lwrocmon_setup_udp_hint(void)
{
  lwrocmon_udp_hint *udp_hint;
  struct timeval now;

  udp_hint = (lwrocmon_udp_hint *) malloc (sizeof (lwrocmon_udp_hint));

  if (!udp_hint)
    LWROC_FATAL("Memory allocation failure (udp_hint info).");

  memset (udp_hint, 0, sizeof (*udp_hint));

  udp_hint->_fd =
    lwroc_net_io_socket_bind(0, 0 /* udp */, "udp reconnect hint", 0);

  udp_hint->_port = lwroc_net_io_socket_get_port(udp_hint->_fd,
						 "udp reconnect hint");

  udp_hint->_select_item.item_info = &lwrocmoc_udp_hint_select_item_info;

  /* Insert into the list. */

  PD_LL_ADD_BEFORE(&_lwrocmon_net_clients, &udp_hint->_select_item._items);

  /* Create some identifier for the hints, so we can see if they are
   * for us.
   *
   * Also note that we give the port in the lower part of the second
   * word.  That is necessary, since whoever sends us a hint must have
   * remembered that port, and not to the port where we connected to
   * it from.
   */

  gettimeofday(&now, NULL);

  _lwrocmon_hint_ident[0] = (uint32_t) now.tv_sec;
  _lwrocmon_hint_ident[1] =
    ((uint32_t) udp_hint->_port) |
    (((((uint32_t) now.tv_usec) << 2) ^
      ((uint32_t) getpid())) &
     0xffff0000);
}

/********************************************************************/

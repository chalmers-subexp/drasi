/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "../dtc_arch/acc_def/myinttypes.h"

#include "lwroc_message.h"
#include "lwroc_message_internal.h"

#include "../dtc_arch/acc_def/myprizd.h"
#include "../dtc_arch/acc_def/has_curses.h"
#if HAS_CURSES
#define USE_CURSES 1
#endif
#include "../lu_common/colourtext.h"

#include "lwroc_parse_util.c"
#include "lwroc_ctrl_client.c"
#include "lwroc_ctrl_token.c"
#include "lwroc_net_proto.h"
#include "gen/lwroc_file_request_serializer.h"
#include "gen/lwroc_acqr_request_serializer.h"
#include "gen/lwroc_merge_request_serializer.h"

void lwrocctrl_usage(char *cmdname)
{
  printf ("LWROC control tool\n");
  printf ("\n");
  printf ("Usage: %s HOST[:PORT] <options>\n", cmdname);
  printf ("\n");
  printf ("  --gdb                    Run me in GDB (must be the first "
	  "argument).\n");
  printf ("  --file-open[=[auto=SIZE,][time=Ns,][events=N,]NAME]  Open a new file.\n");
  printf ("  --file-new[=[auto=SIZE,][time=Ns,][events=N,]NAME]  Continue/open a new file.\n");
  printf ("  --file-newrun[=[auto=SIZE,][time=Ns,][events=N,]NAME]  Open a new run.\n");
  printf ("  --file-close             Close current file.\n");
  printf ("  --file-hold              Hold data for file collection.\n");
  printf ("  --file-unjam             Close file with write failure (sets hold).\n");
  printf ("  --file-status            Show current file status.\n");
  printf ("  --acq-start              Start acq (trigger 14).\n");
  printf ("  --acq-stop               Stop acq (trigger 15), and/or hold.\n");
  printf ("  --acq-status             Report acq status.\n");
  printf ("  --acq-meas-dt            Request systems to measure deadtime "
	  "(for 15 s).\n");
  printf ("  --merge-try-reconnect    Request merger to reconnect disabled sources.\n");
  printf ("  --unlimit-log[=Ns]       No logging rate limit (default 15 s).\n");
  printf ("  --debug[=NUM|0xMASK]     Enable / disable (0) debug messages.\n");
  printf ("  --help                   Show this message.\n");
  printf ("  HOST[:[+]PORT]           Drasi instance(s) to control.");
  printf ("\n");
}

void lwroc_ctrl_handle_request_error(struct lwroc_ctrl_client *lcc,
				     int ret,
				     char *err_msg,
				     const char *task,
				     int performer_error_ok)
{
  if (ret != 0)
    {
      LWROC_WARNING_FMT("Failed to perform "
			"%s operation (%s):\n",
			task,
			ret == -1 ? "communication" : "performer");

      if (err_msg[0] != 0)
	LWROC_ERROR_FMT("%s\n",
			err_msg);
      else
	{
	  const char *descr = lwroc_ctrl_last_error(lcc);

	  if (descr)
	    LWROC_ERROR_FMT("%s\n", descr);
	  else
	    LWROC_PERROR("lwroc_ctrl_request");
	}
      if (ret == -1 ||
	  !performer_error_ok)
	exit(1);
    }
}

int main(int argc, char *argv[])
{
  const char *hostname = NULL;
  struct lwroc_ctrl_client *lcc;
  uint32_t debug_mask = 0;
  int set_debug = 0;
  uint32_t unlimit_log = 0;
  uint32_t file_op = 0;
  uint32_t acq_op = 0;
  uint32_t merge_op = 0;
  const char *file_opt = NULL;
  lwroc_file_request file_req;
  char *file_opt_filename = NULL;
  int i;
  int ret;
  char err_msg[LWROC_CTRL_ERR_MSG_SIZE];

  _lwroc_message_local = 1;

  colourtext_init();

  for (i = 1; i < argc; i++)
    {
      char *request = argv[i];
      char *post;

      if (LWROC_MATCH_C_ARG("--help")) {
	lwrocctrl_usage(argv[0]);
	exit(0);
      } else if (LWROC_MATCH_C_ARG("--debug")) {
	set_debug = 1; debug_mask = 1;
      } else if (LWROC_MATCH_C_PREFIX("--debug=",post)) {
	int bit = (int) lwroc_parse_hex_limit(post, "debug bit", 0, 32);
	if (!bit)
	  debug_mask = 0;
	else
	  debug_mask = ((uint32_t) 1) << (bit - 1);
	set_debug = 1;
      } else if (LWROC_MATCH_C_ARG("--file-close")) {
	file_op = LWROC_FILE_OP_CLOSE;
      } else if (LWROC_MATCH_C_ARG("--file-hold")) {
	file_op = LWROC_FILE_OP_HOLD;
      } else if (LWROC_MATCH_C_ARG("--file-unjam")) {
	file_op = LWROC_FILE_OP_UNJAM;
      } else if (LWROC_MATCH_C_ARG("--file-open")) {
	file_op = LWROC_FILE_OP_OPEN;
      } else if (LWROC_MATCH_C_ARG("--file-new")) {
	file_op = LWROC_FILE_OP_NEWFILE;
      } else if (LWROC_MATCH_C_ARG("--file-newrun")) {
	file_op = LWROC_FILE_OP_NEWRUN;
      } else if (LWROC_MATCH_C_PREFIX("--file-open=",post)) {
	file_op = LWROC_FILE_OP_OPEN; file_opt = post;
      } else if (LWROC_MATCH_C_PREFIX("--file-new=",post)) {
	file_op = LWROC_FILE_OP_NEWFILE; file_opt = post;
      } else if (LWROC_MATCH_C_PREFIX("--file-newrun=",post)) {
	file_op = LWROC_FILE_OP_NEWRUN; file_opt = post;
      } else if (LWROC_MATCH_C_ARG("--file-status")) {
	file_op = LWROC_FILE_OP_STATUS;
      } else if (LWROC_MATCH_C_ARG("--acq-start")) {
	acq_op = LWROC_ACQR_OP_START;
      } else if (LWROC_MATCH_C_ARG("--acq-stop")) {
	acq_op = LWROC_ACQR_OP_STOP;
      } else if (LWROC_MATCH_C_ARG("--acq-status")) {
	acq_op = LWROC_ACQR_OP_STATUS;
      } else if (LWROC_MATCH_C_ARG("--acq-meas-dt")) {
	acq_op = LWROC_ACQR_OP_MEAS_DT;
      } else if (LWROC_MATCH_C_ARG("--merge-try-reconnect")) {
	merge_op = LWROC_MERGE_OP_TRY_RECONNECT_DIS_SRC;
      } else if (LWROC_MATCH_C_ARG("--unlimit-log")) {
	unlimit_log = 15;
      } else if (LWROC_MATCH_C_PREFIX("--unlimit-log=",post)) {
	unlimit_log = (uint32_t) lwroc_parse_time(post,"unlimit log");
      } else if (LWROC_MATCH_C_ARG/*NODOC*/("--merge-reconnect-disabled")) {
	LWROC_BADCFG("The option --merge-reconnect-disabled has changed "
		     "name to --merge-try-reconnect.");
      } else {
	/*CMDLINEARG:HOST*/
	if (hostname) {
	  LWROC_BADCFG_FMT("Hostname already given, or bad option '%s'.\n",
			   argv[i]);
	}
	/* Filename. */
	hostname = argv[i];
      }
    }

  if (file_op)
    {
      const char *request;
      lwroc_parse_split_list parse_info;

      file_req._op = file_op;
      file_req._size = 0;
      file_req._time = 0;
      file_req._events = 0;

      lwroc_parse_split_list_setup(&parse_info, file_opt, ',');

      while ((request = lwroc_parse_split_list_next(&parse_info)) != NULL)
	{
	  const char *post;

	  /* TODO: remove the NODOC, by fixing the doc check script. */
	  if (LWROC_MATCH_C_PREFIX/*NODOC*/("auto=",post))
	    {
	      file_req._size = lwroc_parse_hex(post, "file auto-size");
	    }
	  else if (LWROC_MATCH_C_PREFIX/*NODOC*/("time=",post))
	    {
	      file_req._time =
		(uint32_t) lwroc_parse_time(post,"file auto-time");
	    }
	  else if (LWROC_MATCH_C_PREFIX/*NODOC*/("events=",post))
	    {
	      file_req._events = lwroc_parse_hex(post,"file auto-events");
	    }
	  else
	    {
	      if (file_opt_filename)
		LWROC_BADCFG("Filename already given.\n");
	      file_opt_filename =
		strdup_chk(request); /* Request will be released. */
	    }
	}

      /* Avoid users creating files called 'help'. */
      if (file_opt_filename && strcmp(file_opt_filename,"help") == 0)
	{
	  lwrocctrl_usage(argv[0]);
	  exit(0);
	}

      file_req._filename = (file_opt_filename ? file_opt_filename : "");
    }

  if (!hostname)
    LWROC_BADCFG("No hostname given - nothing to control.\n");

  lcc = lwroc_ctrl_connect(hostname);

  if (!lcc)
    {
      LWROC_PERROR("lwroc_ctrl_connect");
      LWROC_FATAL("Failed to connect to host.\n");
    }

  LWROC_INFO_FMT("Connected to: %s\n", hostname);

  if (set_debug)
    {
      uint32_t ret_debug_mask;

      ret = lwroc_ctrl_request(lcc,
			       0, LWROC_CONTROL_OP_DEBUG_MASK,
			       debug_mask, &ret_debug_mask,
			       err_msg, sizeof (err_msg),
			       NULL, 0, NULL, 0, NULL);

      lwroc_ctrl_handle_request_error(lcc, ret, err_msg, "debug control", 1);

      printf ("Current debug mask: %08x.\n", ret_debug_mask);
    }

  if (unlimit_log)
    {
      uint32_t ret_unlimit_log;

      ret = lwroc_ctrl_request(lcc,
			       0, LWROC_CONTROL_OP_UNLIMIT_LOG,
			       unlimit_log, &ret_unlimit_log,
			       err_msg, sizeof (err_msg),
			       NULL, 0, NULL, 0, NULL);

      lwroc_ctrl_handle_request_error(lcc, ret, err_msg, "unlimit log", 0);

      printf ("Unlimited log quota: %d s.\n", ret_unlimit_log);
    }

  if (file_op)
    {
      uint32_t ret_file_val;

      lwroc_file_request_sersize sersize;
      char wire_request[512], wire_response[512];
      size_t sz_request, sz_response = 0;
      lwroc_deserialize_error desererr;
      const char *end;

      sz_request = lwroc_file_request_serialized_size(&file_req, &sersize);

      if (sz_request > sizeof (wire_request))
	LWROC_FATAL("Too large file control request message.\n");

      lwroc_file_request_serialize(wire_request, &file_req, &sersize);

      free(file_opt_filename);

      ret = lwroc_ctrl_request(lcc,
			       0, LWROC_CONTROL_OP_FILE,
			       0, &ret_file_val,
			       err_msg, sizeof (err_msg),
			       wire_request, sz_request,
			       wire_response, sizeof (wire_response),
			       &sz_response);

      lwroc_ctrl_handle_request_error(lcc, ret, err_msg, "file control",
				      1 /* On performer error, we still
					 * have a response to give. */ );

      /* printf ("ret: %d %" MYPRIzd "\n", ret, sz_response); */

      end =
	lwroc_file_request_deserialize_inplace(&file_req,
					       wire_response, sz_response,
					       &desererr);

      if (end == NULL)
	LWROC_FATAL_FMT("Bad response, malformed (%s, 0x%x, 0x%x).\n",
			desererr._msg, desererr._val1, desererr._val2);

      if (ret == 0)
	{
	  const char *status_str = "?";

	  switch (file_req._op)
	    {
	    case LWROC_FILE_OP_OPEN:   status_str = "open";   break;
	    case LWROC_FILE_OP_CLOSE:  status_str = "close";  break;
	    case LWROC_FILE_OP_HOLD:   status_str = "hold";   break;
	    case LWROC_FILE_OP_JAMMED: status_str = "jammed"; break;
	    }

	  printf ("Current file state: %d\n", ret_file_val);
	  printf ("Current file status: %s\n", status_str);
	  printf ("File: %s\n", file_req._filename);
	  printf ("Written: %" PRIu64 "\n", file_req._size);
	  printf ("Events: %" PRIu64 "\n", file_req._events);
	}
    }

  if (acq_op)
    {
      uint32_t ret_acqr_val;

      lwroc_acqr_request acqr_req;
      char wire_request[32], wire_response[32];
      size_t sz_request, sz_response = 0;
      lwroc_deserialize_error desererr;
      const char *end;

      memset(&acqr_req, 0, sizeof (acqr_req));
      acqr_req._op = acq_op;

      sz_request = lwroc_merge_request_serialized_size();

      if (sz_request > sizeof (wire_request))
	LWROC_FATAL("Too large acq control request message.\n");

      lwroc_acqr_request_serialize(wire_request, &acqr_req);

      ret = lwroc_ctrl_request(lcc,
			       0, LWROC_CONTROL_OP_ACQ_READOUT,
			       0, &ret_acqr_val,
			       err_msg, sizeof (err_msg),
			       wire_request, sz_request,
			       wire_response, sizeof (wire_response),
			       &sz_response);

      lwroc_ctrl_handle_request_error(lcc, ret, err_msg, "acq control", 0);

      /* printf ("ret: %d %" MYPRIzd "\n", ret, sz_response); */

      end =
	lwroc_acqr_request_deserialize(&acqr_req,
				       wire_response, sz_response,
				       &desererr);

      if (end == NULL)
	LWROC_FATAL_FMT("Bad response, malformed (%s, 0x%x, 0x%x).\n",
			desererr._msg, desererr._val1, desererr._val2);

      if (ret == 0)
	{
	  printf ("Current acq status: %d.\n", ret_acqr_val);
	  printf ("Events: %" PRIu64 "\n", acqr_req._events);
	}
    }

  if (merge_op)
    {
      uint32_t ret_merge_val;

      lwroc_merge_request merge_req;
      char wire_request[32], wire_response[32];
      size_t sz_request, sz_response = 0;
      lwroc_deserialize_error desererr;
      const char *end;

      memset(&merge_req, 0, sizeof (merge_req));
      merge_req._op = merge_op;

      sz_request = lwroc_merge_request_serialized_size();

      if (sz_request > sizeof (wire_request))
	LWROC_FATAL("Too large merge control request message.\n");

      lwroc_merge_request_serialize(wire_request, &merge_req);

      ret = lwroc_ctrl_request(lcc,
			       0, LWROC_CONTROL_OP_MERGE,
			       0, &ret_merge_val,
			       err_msg, sizeof (err_msg),
			       wire_request, sz_request,
			       wire_response, sizeof (wire_response),
			       &sz_response);

      lwroc_ctrl_handle_request_error(lcc, ret, err_msg, "merge control", 0);

      /* printf ("ret: %d %" MYPRIzd "\n", ret, sz_response); */

      end =
	lwroc_merge_request_deserialize(&merge_req,
				       wire_response, sz_response,
				       &desererr);

      if (end == NULL)
	LWROC_FATAL_FMT("Bad response, malformed (%s, 0x%x, 0x%x).\n",
			desererr._msg, desererr._val1, desererr._val2);

      if (ret == 0)
	{
	  /* ret_merge_val not used for anything (yet). */
	  printf ("Current merge status: %d/%d sources.\n",
		  merge_req._num_active_sources,
		  merge_req._num_sources);
	  /* printf ("Events: %" PRIu64 "\n", merge_req._events); */
	}
    }

  if (lwroc_ctrl_close(lcc) != 0)
    {
      LWROC_PERROR("lwroc_ctrl_close");
      LWROC_FATAL("Failure during socket close.\n");
    }

  return 0;
}

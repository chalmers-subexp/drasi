/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>
#include <errno.h>
#include <float.h>
#include "../dtc_arch/acc_def/myinttypes.h"

#include "lwroc_message.h"
#include "lwroc_message_internal.h"

#include "../dtc_arch/acc_def/myprizd.h"
#include "../dtc_arch/acc_def/has_curses.h"
#if HAS_CURSES
#define USE_CURSES 1
#endif
#include "../lu_common/colourtext.h"

#include "lwroc_parse_util.c"
#include "lwroc_net_proto.h"
#include "lwroc_array_heap.h"
#include "lwroc_track_timestamp_extra.h"

/* #include "../lwroc/git-describe-include.h" */

#define LWROC_DTT_SCALE_STAMP             1
#define LWROC_DTT_SCALE_LOCAL             2

#define LWROC_DTT_KIND_TRIGGER            1
#define LWROC_DTT_KIND_START_CTIME        2 /* Generated internally. */
#define LWROC_DTT_KIND_LAST_POLL          3
#define LWROC_DTT_KIND_AFTER_POLL         4
#define LWROC_DTT_KIND_AFTER_DT_RELEASE   5
#define LWROC_DTT_KIND_AFTER_POSTPROC     6
#define LWROC_DTT_KIND_RECORDING_GAP      7

typedef struct lwroc_dtt_mark_t
{
  double _t;
  char   _scale;
  char   _kind;

} lwroc_dtt_mark;

lwroc_dtt_mark *_lwroc_dtt_marks = 0;
size_t          _lwroc_dtt_nmarks = 0;
size_t          _lwroc_dtt_alloc_marks = 0;

typedef struct lwroc_dtt_event_t
{
  uint32_t _eventno;
  uint8_t  _trig;
  uint8_t  _spill;

  size_t   _mark_off;
  size_t   _nmarks;

} lwroc_dtt_event;

lwroc_dtt_event *_lwroc_dtt_events = 0;
size_t           _lwroc_dtt_nevents = 0;
size_t           _lwroc_dtt_alloc_events = 0;

#define LWROC_DTT_SYSTEM_FLAGS_HAS_CTIME  0x0001

typedef struct lwroc_dtt_system_t
{
  pd_ll_item _items;

  const char *_name;
  const char *_master_name;

  struct lwroc_dtt_system_t *_master;

  size_t     *_events;
  size_t      _nevents;
  size_t      _alloc_events;

  int         _index;
  const char *_shortname;
  const char *_vcdid_dt;
  const char *_vcdid_cpu;
  const char *_vcdid_trig;
  const char *_vcdid_event_no;

  /* Used during fill. */
  int         _flags;
  double      _ctime;
  double      _prev_max_t;

  /* Time correction vs. master. */
  double _dydx;
  double _y0;

  /* Used during dump. */
  double _next_time;
  size_t _next_event;
  size_t _next_mark;

} lwroc_dtt_system;

PD_LL_SENTINEL(_lwroc_dtt_systems);

char _lwroc_dtt_next_id = 0;

char *lwroc_dtt_new_vcdid(void)
{
  char id[16];

  /* Not optimum encoding, but works. */
  snprintf(id,sizeof(id), "a%x", _lwroc_dtt_next_id++);

  return strdup_chk(id);
}

lwroc_dtt_system *lwroc_dtt_find_system(const char *name)
{
  lwroc_dtt_system *syst;
  pd_ll_item *iter;

  PD_LL_FOREACH(_lwroc_dtt_systems, iter)
    {
      syst =
	PD_LL_ITEM(iter, lwroc_dtt_system, _items);

      if (strcmp(syst->_name, name) == 0)
	return syst;
    }

  return NULL;
}

lwroc_dtt_system *lwroc_dtt_find_or_create_system(const char *name)
{
  lwroc_dtt_system *syst;

  syst = lwroc_dtt_find_system(name);

  if (syst)
    return syst;

  LWROC_INFO_FMT("System: %s\n", name);

  syst = malloc (sizeof (lwroc_dtt_system));

  if (!syst)
    LWROC_FATAL("Memory allocation failure (system).");

  memset (syst, 0, sizeof(*syst));

  syst->_name = strdup_chk(name);
  syst->_ctime = 0.;
  syst->_prev_max_t = DBL_MIN;

  PD_LL_ADD_BEFORE(&_lwroc_dtt_systems, &syst->_items);

  return syst;
}

void lwroc_dtt_set_master(lwroc_dtt_system *syst, const char *master)
{
  if (syst->_master)
    {
      if (strcmp(syst->_master_name, master) != 0)
	LWROC_FATAL_FMT("System '%s' already has master '%s' set, "
			"new attempt: '%s'.",
			syst->_name,
			syst->_master_name,
			master);
    }
  else
    syst->_master_name = strdup_chk(master);
}

void lwroc_dtt_set_ctime(lwroc_dtt_system *syst, double val_ctime)
{
  syst->_ctime = val_ctime;

  if (!(syst->_flags & LWROC_DTT_SYSTEM_FLAGS_HAS_CTIME))
    {
      /* Apply the ctime to all previous events. */

      size_t iev = 0;
      lwroc_dtt_event *ev;

      size_t i;
      lwroc_dtt_mark *mark;

      double prev_max_t = DBL_MIN;

      for (iev = 0 ; iev < syst->_nevents; iev++)
	{
	  ev =
	    &_lwroc_dtt_events[syst->_events[iev]];

	  mark = &_lwroc_dtt_marks[ev->_mark_off];

	  for (i = 0; i < ev->_nmarks; i++, mark++)
	    {
	      if (mark->_kind == LWROC_DTT_KIND_START_CTIME)
		{
		  double start_ctime = mark->_t - syst->_ctime;

		  if (start_ctime < prev_max_t)
		    start_ctime = prev_max_t;

		  mark->_t = start_ctime;
		}

	      if (mark->_scale == LWROC_DTT_SCALE_LOCAL)
		prev_max_t = mark->_t;
	    }
	}
    }

  syst->_flags |= LWROC_DTT_SYSTEM_FLAGS_HAS_CTIME;
}

lwroc_dtt_event *lwroc_dtt_add_event(lwroc_dtt_system *syst)
{
  lwroc_dtt_event *event;
  size_t event_off;

  if (syst->_nevents >= syst->_alloc_events)
    {
      syst->_alloc_events *= 2;
      if (syst->_alloc_events < 1024)
	syst->_alloc_events = 1024;

      syst->_events =
	(size_t *) realloc(syst->_events,
			   syst->_alloc_events * sizeof (size_t));

      if (!syst->_events)
	LWROC_FATAL("Memory allocation failure (events list).");
    }

  if (_lwroc_dtt_nevents >= _lwroc_dtt_alloc_events)
    {
      _lwroc_dtt_alloc_events *= 2;
      if (_lwroc_dtt_alloc_events < 1024)
	_lwroc_dtt_alloc_events = 1024;

      _lwroc_dtt_events =
	(lwroc_dtt_event *) realloc(_lwroc_dtt_events,
				    _lwroc_dtt_alloc_events *
				    sizeof (lwroc_dtt_event));

      if (!_lwroc_dtt_events)
	LWROC_FATAL("Memory allocation failure (events).");
    }

  event_off = _lwroc_dtt_nevents++;

  event = &_lwroc_dtt_events[event_off];

  syst->_events[syst->_nevents++] = event_off;

  memset(event, 0, sizeof (*event));

  event->_mark_off = (size_t) -1;

  return event;
}

lwroc_dtt_mark *lwroc_dtt_add_mark(lwroc_dtt_event *event)
{
  lwroc_dtt_mark *mark;
  size_t mark_off;

  if (_lwroc_dtt_nmarks >= _lwroc_dtt_alloc_marks)
    {
      _lwroc_dtt_alloc_marks *= 2;
      if (_lwroc_dtt_alloc_marks < 1024)
	_lwroc_dtt_alloc_marks = 1024;

      _lwroc_dtt_marks =
	(lwroc_dtt_mark *) realloc(_lwroc_dtt_marks,
				    _lwroc_dtt_alloc_marks *
				    sizeof (lwroc_dtt_mark));

      if (!_lwroc_dtt_marks)
	LWROC_FATAL("Memory allocation failure (marks).");
    }

  mark_off = _lwroc_dtt_nmarks++;

  mark = &_lwroc_dtt_marks[mark_off];

  if (event->_mark_off == (size_t) -1)
    event->_mark_off = mark_off;

  event->_nmarks++;

  memset(mark, 0, sizeof (*mark));

  return mark;
}

int lwroc_dtt_system_find_next_time(lwroc_dtt_system *syst)
{
  lwroc_dtt_event *event;
  lwroc_dtt_mark  *mark;

  /*
  fprintf (stdout, "Find %d: %zd %zd (%zd)\n",
	   syst->_index,
	   syst->_next_event, syst->_next_mark,
	   syst->_nevents);
  */

  for ( ; ; )
    {
      event = &_lwroc_dtt_events[syst->_events[syst->_next_event]];

      /* fprintf (stdout, "Event: (marks = %zd)\n", event->_nmarks); */

    next_mark:
      syst->_next_mark++; /* Easily adds from -1. */

      if (syst->_next_mark < event->_nmarks)
	{
	  mark = &_lwroc_dtt_marks[event->_mark_off + syst->_next_mark];

	  if (mark->_scale == LWROC_DTT_SCALE_LOCAL)
	    {
	      syst->_next_time = mark->_t;

	      /*
		fprintf (stdout, "Next %d: %15.9f  %zd %zd (%zd)\n",
			 syst->_index,
			 syst->_next_time,
			 syst->_next_event, syst->_next_mark,
			 syst->_nevents);
	      */
	      return 1;
	    }

	  goto next_mark;
	}

      syst->_next_event++;
      syst->_next_mark = (size_t) -1;

      if (syst->_next_event >= syst->_nevents)
	return 0;
    }
}

int lwroc_dtt_system_compare_less(lwroc_dtt_system *syst_a,
				  lwroc_dtt_system *syst_b,
				  void *extra_ptr)
{
  (void) extra_ptr;
  return (syst_a->_next_time < syst_b->_next_time);
}

void lwrocdt_usage(char *cmdname)
{
  printf ("LWROC deadtime trace parser\n");
  printf ("\n");
  printf ("Usage: %s <options>\n", cmdname);
  printf ("\n");
}

void lwroc_dtt_read_input(FILE *fid)
{
  char line[1024];
  lwroc_dtt_system *syst = NULL;
  lwroc_dtt_event *event = NULL;

  for ( ; ; )
    {
      char *request = line;
      char *post;
      size_t len;

      if (fgets(line, sizeof (line), fid) == NULL)
	{
	  if (feof (fid))
	    break;
	  LWROC_FATAL("Error while reading input.\n");
	}

      len = strlen(line);
      while (len > 0 &&
	     isspace(line[len-1]))
	line[--len] = 0;

      if (LWROC_MATCH_C_PREFIX/*NODOC*/("System:",post))
	{
	  while (isspace(*post))
	    post++;

	  syst = lwroc_dtt_find_or_create_system(post);

	  event = NULL;
	}
      else if (!syst)
	{
	  /* All remaining items needs a system to act on. */

	  LWROC_FATAL_FMT("Unhandled input '%s' "
			  "without system specified first.", line);
	}
      else if (LWROC_MATCH_C_PREFIX/*NODOC*/("Master:",post))
	{
	  while (isspace(*post))
	    post++;

	  lwroc_dtt_set_master(syst, post);
	}
      else if (LWROC_MATCH_C_PREFIX/*NODOC*/("ctime:",post))
	{
	  double val_ctime;
	  int n;

	  n = sscanf(request,
		     "ctime: %lf\n",
		     &val_ctime);

	  if (n != 1)
	    LWROC_FATAL_FMT("Unhandled ctime '%s'.", line);

	  lwroc_dtt_set_ctime(syst, val_ctime);
	}
      else if (LWROC_MATCH_C_PREFIX/*NODOC*/("Event:",post))
	{
	  unsigned int eventno;
	  unsigned int trig;
	  unsigned int spill;
	  int n;

	  n = sscanf(request,
		     "Event: %u Trig: %u Spill: %u\n",
		     &eventno, &trig, &spill);

	  if (n != 3)
	    LWROC_FATAL_FMT("Unhandled event '%s'.", line);

	  event = lwroc_dtt_add_event(syst);

	  event->_eventno = eventno;
	  event->_trig    = (uint8_t) trig;
	  event->_spill   = (uint8_t) spill;

	  /* Did we have a previous event? */
	  if (syst->_nevents >= 2)
	    {
	      size_t prev_iev = syst->_nevents-2;
	      lwroc_dtt_event *prev_ev;

	      prev_ev =
		&_lwroc_dtt_events[syst->_events[prev_iev]];

	      if (prev_ev->_eventno != eventno-1)
		{
		  /* There was a gap since previous report. */

		  lwroc_dtt_mark *mark;

		  mark = lwroc_dtt_add_mark(event);

		  mark->_t     = syst->_prev_max_t;
		  mark->_scale = LWROC_DTT_SCALE_LOCAL;
		  mark->_kind  = LWROC_DTT_KIND_RECORDING_GAP;
		}
	    }

	  /*
	  printf ("add_event: %d %d\n",
		  event->_eventno, event->_trig);
	  */
	}
      else if (request[0] == '@')
	{
	  double value;
	  char *end;
	  char kind = 0;
	  char scale = 0;

	  request++;
	  post = NULL;

	  if (LWROC_MATCH_C_PREFIX/*NODOC*/("stamp",post))
	    {
	      scale = LWROC_DTT_SCALE_STAMP;
	      goto parse_time;
	    }
	  else if (LWROC_MATCH_C_PREFIX/*NODOC*/("local",post))
	    {
	      scale = LWROC_DTT_SCALE_LOCAL;
	      goto parse_time;
	    }
	  else
	    {
	      LWROC_FATAL_FMT("Unhandled input time '%s'.", line);
	    }

	parse_time:
	  errno = 0;
	  value = strtod(post, &end);
	  if (errno)
	    LWROC_FATAL_FMT("Failed to parse time in '%s'.", line);

	  /* Use ispace() instead of isblank(), latter sometimes missing. */

	  request = end;
	  while (isspace(*request))
	    request++;

	  if (LWROC_MATCH_C_ARG/*NODOC*/("trigger"))
	    kind = LWROC_DTT_KIND_TRIGGER;
	  else if (LWROC_MATCH_C_ARG/*NODOC*/("last_poll"))
	    kind = LWROC_DTT_KIND_LAST_POLL;
	  else if (LWROC_MATCH_C_ARG/*NODOC*/("after_poll"))
	    kind = LWROC_DTT_KIND_AFTER_POLL;
	  else if (LWROC_MATCH_C_ARG/*NODOC*/("after_dt_rel"))
	    kind = LWROC_DTT_KIND_AFTER_DT_RELEASE;
	  else if (LWROC_MATCH_C_ARG/*NODOC*/("after_postproc"))
	    kind = LWROC_DTT_KIND_AFTER_POSTPROC;
	  else
	    {
	      LWROC_FATAL_FMT("Unhandled time kind '%s'.", line);
	    }

	  if (scale == LWROC_DTT_SCALE_LOCAL)
	    {
	      if (value < syst->_prev_max_t)
		LWROC_FATAL_FMT("Time less than previous '%s'.", line);
	    }

	  /* For the time being not handled!
	   * Needs to come after event line.
	   */
	  if (kind != LWROC_DTT_KIND_LAST_POLL)
	    {
	      lwroc_dtt_mark *mark;

	      if (!event)
		LWROC_FATAL_FMT("Unhandled time '%s' without event.",
				line);

	      if (kind  == LWROC_DTT_KIND_AFTER_POLL &&
		  scale == LWROC_DTT_SCALE_LOCAL)
		{
		  double start_ctime;

		  mark = lwroc_dtt_add_mark(event);

		  start_ctime = value - syst->_ctime;

		  /* TODO: this is not correct.  OTOH, when
		   * this happens, then we do not really know
		   * how early the deadtime was.  Since obviously
		   * the CPU was busy with something else (e.g.
		   * the previous event).
		   */
		  if (start_ctime < syst->_prev_max_t)
		    start_ctime = syst->_prev_max_t;

		  mark->_t = start_ctime;
		  mark->_scale = scale;
		  mark->_kind  = LWROC_DTT_KIND_START_CTIME;
		}

	      mark = lwroc_dtt_add_mark(event);

	      mark->_t = value;
	      mark->_scale = scale;
	      mark->_kind  = kind;

	      /*
	      printf ("add_mark: %d %d %.9f\n",
		      mark->_scale, mark->_kind, mark->_t);
	      */

	      if (scale == LWROC_DTT_SCALE_LOCAL)
		syst->_prev_max_t = value;
	    }
	}
      else
	{
	  LWROC_FATAL_FMT("Unhandled input '%s'.", line);
	}
    }

  LWROC_INFO_FMT("Done reading input "
		 "(%" MYPRIzd " events, %" MYPRIzd " marks).\n",
		 _lwroc_dtt_nevents,
		 _lwroc_dtt_nmarks);
}

lwroc_dtt_mark *lwroc_dtt_find_event_mark(lwroc_dtt_event *ev,
					  char scale, char kind)
{
  lwroc_dtt_mark *mark = &_lwroc_dtt_marks[ev->_mark_off];
  size_t i;

  for (i = 0; i < ev->_nmarks; i++, mark++)
    {
      if ((!scale || mark->_scale == scale) &&
	  (!kind  || mark->_kind  == kind ))
	return mark;
    }
  return NULL;
}

void lwroc_dtt_align_slave_timescale(lwroc_dtt_system *syst)
{
  lwroc_dtt_system *master = syst->_master;
  size_t iev_m = 0;
  size_t iev_s = 0;

  lwroc_linear_fit_sol sol;
  lwroc_linear_fit_data *lf_data;
  double *tmp_2array;

  /* At most, we have as many common events as the most number of
   * events in any of the sources.
   */

  size_t nalloc;
  size_t n;
  size_t num_used_fit;
  size_t i;

  nalloc = master->_nevents;
  if (syst->_nevents > nalloc)
    nalloc = syst->_nevents;

  lf_data = (lwroc_linear_fit_data *)
    malloc(nalloc * sizeof (lwroc_linear_fit_data));
  tmp_2array = (double *)
    malloc(2 * nalloc * sizeof (double));

  if (!lf_data ||
      !tmp_2array)
    LWROC_FATAL("Memory allocation failure (alignment).");

  /* fprintf (stderr, "align %" MYPRIzd " %" MYPRIzd "\n",
	      master->_nevents, syst->_nevents); */

  /* We loop through all events of both systems, and use those
   * which are common.
   */

  n = 0;

  for ( ; iev_m < master->_nevents &&
	  iev_s < syst->_nevents; )
    {
      lwroc_dtt_event *ev_m =
	&_lwroc_dtt_events[master->_events[iev_m]];
      lwroc_dtt_event *ev_s =
	&_lwroc_dtt_events[syst->_events[iev_s]];

      lwroc_dtt_mark *mark_m;
      lwroc_dtt_mark *mark_s;

      if (ev_m->_eventno < ev_s->_eventno)
	{
	  /* fprintf (stderr, "%d m\n", ev_m->_eventno); */
	  iev_m++;
	  continue;
	}
      if (ev_s->_eventno < ev_m->_eventno)
	{
	  /* fprintf (stderr, "s %d\n", ev_s->_eventno); */
	  iev_s++;
	  continue;
	}

      /* Found a common event. */

      /* fprintf (stderr, "    %d\n", ev_m->_eventno); */

      /* Find the after_poll marks. */

      mark_m = lwroc_dtt_find_event_mark(ev_m,
					 LWROC_DTT_SCALE_LOCAL,
					 LWROC_DTT_KIND_START_CTIME);
      mark_s = lwroc_dtt_find_event_mark(ev_s,
					 LWROC_DTT_SCALE_LOCAL,
					 LWROC_DTT_KIND_START_CTIME);

      if (mark_m && mark_s)
	{
	  double x = mark_s->_t;
	  double y = mark_m->_t - mark_s->_t;

	  lf_data[n]._x = x;
	  lf_data[n]._y = y;
	  lf_data[n]._flags = LWROC_TIMESTAMP_FIT_FIT1;
	  n++;
	}

      iev_m++;
      iev_s++;
    }

  lwroc_linear_fit_filter_fit(&sol, lf_data,
			      n,
			      tmp_2array,
			      LWROC_TIMESTAMP_FIT_FIT1,
			      &num_used_fit);

  syst->_dydx = sol.dydx;
  syst->_y0   = sol.y0;

  (void) i;

  LWROC_INFO_FMT("Align: %s - %s : %6.1f us  %6.3f us/s",
		 syst->_name,
		 syst->_master_name,
		 syst->_y0   * 1.e6,
		 syst->_dydx * 1.e6);

  /*
  fprintf (stderr,
	   "alignment: y0=%15.9f (%.9f) dydx=%15.9f (%.9f) "
	   "%" MYPRIzd "/%" MYPRIzd "\n",
	   sol.y0,   sqrt(sol.var_y0),
	   sol.dydx, sqrt(sol.var_dydx),
	   num_used_fit, n);

  for (i = 0; i < n; i++)
    {
      double x = lf_data[i]._x;
      double y = lf_data[i]._y;

      double y_fit = sol.y0 + sol.dydx * x;

      if (lf_data[i]._flags & LWROC_TIMESTAMP_FIT_FIT2)
	{
	  fprintf (stderr, "%8.3f %8.3f %8.3f\n",
		   x,
		   1.e6 *  y,
		   1.e6 * (y - y_fit));
	}
      else
	{
	  fprintf (stderr, "%8.3f                      %8.3f %8.3f\n",
		   x,
		   1.e6 *  y,
		   1.e6 * (y - y_fit));
	}
    }
  */
}

void lwroc_dtt_align_timestamp_timescale(lwroc_dtt_system *syst,
					 lwroc_linear_fit_sol *total)
{
  size_t iev = 0;

  lwroc_linear_fit_sol sol;
  lwroc_linear_fit_data *lf_data;
  double *tmp_2array;

  /* At most, we have as many samples as events.
   */

  size_t nalloc;
  size_t n;
  size_t num_used_fit;
  size_t i;

  nalloc = syst->_nevents;

  lf_data = (lwroc_linear_fit_data *)
    malloc(nalloc * sizeof (lwroc_linear_fit_data));
  tmp_2array = (double *)
    malloc(2 * nalloc * sizeof (double));

  if (!lf_data ||
      !tmp_2array)
    LWROC_FATAL("Memory allocation failure (alignment).");

  /* We loop through all events of both systems, looking for any with
   * both timestamp and time.
   */

  n = 0;

  for ( ; iev < syst->_nevents; )
    {
      lwroc_dtt_event *ev =
	&_lwroc_dtt_events[syst->_events[iev]];

      lwroc_dtt_mark *mark_trig;
      lwroc_dtt_mark *mark_sctime;

      /* Find the trigger and after_poll marks. */

      mark_trig = lwroc_dtt_find_event_mark(ev,
					    LWROC_DTT_SCALE_STAMP,
					    LWROC_DTT_KIND_TRIGGER);
      mark_sctime = lwroc_dtt_find_event_mark(ev,
					      LWROC_DTT_SCALE_LOCAL,
					      LWROC_DTT_KIND_START_CTIME);

      if (mark_trig && mark_sctime)
	{
	  double x = mark_sctime->_t;
	  double y = mark_trig->_t;

	  lf_data[n]._x = x;
	  lf_data[n]._y = y;
	  lf_data[n]._flags = LWROC_TIMESTAMP_FIT_FIT1;
	  n++;
	}

      iev++;
    }

  if (n < 10)
    return;

  lwroc_linear_fit_filter_fit(&sol, lf_data,
			      n,
			      tmp_2array,
			      LWROC_TIMESTAMP_FIT_FIT1,
			      &num_used_fit);

  syst->_dydx = sol.dydx;
  syst->_y0   = sol.y0;

  /* Weight with inverse of variance. */
  sol.var_dydx = 1. / sol.var_dydx;
  sol.var_y0   = 1. / sol.var_y0;

  total->dydx     += sol.var_dydx * sol.dydx;
  total->y0       += sol.var_y0   * sol.y0;

  total->var_dydx += sol.var_dydx;
  total->var_y0   += sol.var_y0;

  (void) i;

  LWROC_INFO_FMT("Align stamp: %s : %6.1f ticks  %6.3f tick/s",
		 syst->_name,
		 syst->_y0,
		 syst->_dydx);

  /*
  fprintf (stderr,
	   "alignment: y0=%15.9f (%.9f) dydx=%15.9f (%.9f) "
	   "%" MYPRIzd "/%" MYPRIzd "\n",
	   sol.y0,   sqrt(sol.var_y0),
	   sol.dydx, sqrt(sol.var_dydx),
	   num_used_fit, n);

  for (i = 0; i < n; i++)
    {
      double x = lf_data[i]._x;
      double y = lf_data[i]._y;

      double y_fit = sol.y0 + sol.dydx * x;

      if (lf_data[i]._flags & LWROC_TIMESTAMP_FIT_FIT2)
	{
	  fprintf (stderr, "%8.3f %8.3f %8.3f\n",
		   x,
		   1.e6 *  y,
		   1.e6 * (y - y_fit));
	}
      else
	{
	  fprintf (stderr, "%8.3f                      %8.3f %8.3f\n",
		   x,
		   1.e6 *  y,
		   1.e6 * (y - y_fit));
	}
    }
  */
}

void lwroc_dtt_apply_correction(lwroc_dtt_system *syst)
{
  size_t iev;
  double tprev = 0;

  for (iev = 0; iev < syst->_nevents; iev++)
    {
      lwroc_dtt_event *ev =
	&_lwroc_dtt_events[syst->_events[iev]];
      lwroc_dtt_mark *mark =
	&_lwroc_dtt_marks[ev->_mark_off];
      size_t i;

      for (i = 0; i < ev->_nmarks; i++, mark++)
	{
	  double t_in, t_out;

	  if (mark->_scale == LWROC_DTT_SCALE_LOCAL)
	    {
	      t_in = mark->_t;

	      /* Hack to ensure that we do not get 0-length deadtimes. */
	      if (t_in <= tprev)
		t_in += 0.5e-6;

	      tprev = t_in;

	      t_out = t_in + syst->_y0 + syst->_dydx * t_in;

	      mark->_t = t_out;
	    }
	}
    }
}

void lwroc_dtt_align_timescales(void)
{
  lwroc_dtt_system *syst;
  pd_ll_item *iter;

  lwroc_linear_fit_sol stamp_total;

  memset(&stamp_total, 0, sizeof (stamp_total));

  /* For each system that has timestamps, we first determine
   * a global timescale of the timestamps.
   */

  PD_LL_FOREACH(_lwroc_dtt_systems, iter)
    {
      syst =
	PD_LL_ITEM(iter, lwroc_dtt_system, _items);

      lwroc_dtt_align_timestamp_timescale(syst, &stamp_total);
    }

  if (stamp_total.var_y0 != 0.0)
    {
      double s_dydx;

      stamp_total.y0   /= stamp_total.var_y0;
      stamp_total.dydx /= stamp_total.var_dydx;

      s_dydx = sqrt(1 / stamp_total.var_dydx);

      if (fabs(stamp_total.dydx - 1.e9) < 1.e9*200e-6 + 5 * s_dydx)
	{
	  LWROC_INFO_FMT("Timestamp rate 1G + %.1f (+/- %.1f)\n",
			 stamp_total.dydx - 1.e9, s_dydx);

	  stamp_total.dydx = 1.e9;
	}

      LWROC_INFO_FMT("Global stamp: %6.1f ticks  %6.3f tick/s",
		     stamp_total.y0,
		     stamp_total.dydx);

      /* The local timescale of each system (having fitted) is:
       *
       * stamp = y0 + t * dydx
       *
       * The global (average) scale is:
       *
       * stamp = g_y0 + t * g_dydx
       *
       * We now want to recalculate each local timescale such that
       * they adhere to the global time.  I.e.
       *
       * y0 + t_old * dydx = stamp = g_y0 + t_new * g_dydx
       *
       * t_new * g_dydx = y0 - g_y0 + t_old * dydx
       *
       * t_new = (y0 - g_y0 + t_old * dydx) / g_dydx
       */

      PD_LL_FOREACH(_lwroc_dtt_systems, iter)
	{
	  double v_y0;
	  double v_dydx;

	  syst =
	    PD_LL_ITEM(iter, lwroc_dtt_system, _items);

	  v_y0   = syst->_y0;
	  v_dydx = syst->_dydx;

	  if (v_dydx != 0.0)
	    {
	      syst->_y0   = (v_y0 - stamp_total.y0) / stamp_total.dydx;
	      syst->_dydx = syst->_dydx / stamp_total.dydx - 1.;

	      fprintf (stderr, "corr %.9f %.9f\n",
		       syst->_y0, syst->_dydx);

	      lwroc_dtt_apply_correction(syst);
	    }
	}

      /* Just check that all systems now give the same fit. */

      memset(&stamp_total, 0, sizeof (stamp_total));

      PD_LL_FOREACH(_lwroc_dtt_systems, iter)
	{
	  syst =
	    PD_LL_ITEM(iter, lwroc_dtt_system, _items);

	  lwroc_dtt_align_timestamp_timescale(syst, &stamp_total);
	}
    }


  /* For each system that has a master, we can align the scales
   * using the event numbers.
   */

  PD_LL_FOREACH(_lwroc_dtt_systems, iter)
    {
      syst =
	PD_LL_ITEM(iter, lwroc_dtt_system, _items);

      if (syst->_master_name)
	{
	  syst->_master = lwroc_dtt_find_system(syst->_master_name);

	  /*
	  fprintf (stderr,
		   "slave: %s  master: %s\n",
		   syst->_name,
		   syst->_master_name);
	  */

	  if (syst->_master)
	    {
	      lwroc_dtt_align_slave_timescale(syst);

	      lwroc_dtt_apply_correction(syst);

	      lwroc_dtt_align_slave_timescale(syst);
	    }
	}
    }
}

char *lwroc_fmt_binary(char *str, uint64_t value, int n)
{
  int i;

  for (i = 0; i < n; i++)
    str[n-i-1] = (char) ('0' + ((value >> i) & 1));
  str[n] = 0;

  return str;
}

char *lwroc_fmt_binary_fixed(char *str, char fix, int n)
{
  int i;

  for (i = 0; i < n; i++)
    str[n-i-1] = fix;
  str[n] = 0;

  return str;
}

void lwroc_dtt_dump_data(void)
{
  lwroc_dtt_system *syst;
  pd_ll_item *iter;

  size_t iev = 0;
  lwroc_dtt_event *ev;

  size_t i;
  lwroc_dtt_mark *mark;

  PD_LL_FOREACH(_lwroc_dtt_systems, iter)
    {
      syst =
	PD_LL_ITEM(iter, lwroc_dtt_system, _items);

      printf ("System: %s  (events: %" MYPRIzd ")\n",
	      syst->_name,
	      syst->_nevents);

      for (iev = 0 ; iev < syst->_nevents; iev++)
	{
	  ev =
	    &_lwroc_dtt_events[syst->_events[iev]];

	  printf ("Event: %" MYPRIzd " [%" MYPRIzd "] "
		  "%" PRIu32 " %d  (marks: %" MYPRIzd ")\n",
		  iev, syst->_events[iev],
		  ev->_eventno, ev->_trig,
		  ev->_nmarks);

	  mark = &_lwroc_dtt_marks[ev->_mark_off];

	  for (i = 0; i < ev->_nmarks; i++, mark++)
	    {
	      printf ("Mark: %" MYPRIzd " [%" MYPRIzd "] %d %d  %.9f\n",
		      i,
		      ev->_mark_off + i,
		      mark->_scale, mark->_kind, mark->_t);
	    }
	}
    }
}

void lwroc_dtt_write_vcd(FILE *fid)
{
  lwroc_dtt_system *syst;
  pd_ll_item *iter;

  size_t nsystems;
  size_t systems_active;
  lwroc_dtt_system **systems;

  double cur_time;

  char bin[128];

  fprintf (fid, "$date\n");
  fprintf (fid, "  Jun 20, 2021\n");
  fprintf (fid, "$date\n");
  fprintf (fid, "$version\n");
  fprintf (fid, "  lwrocdt - drasi/LWROC deadtime trace analyser (%s).\n",
	   "version"/*GIT_DESCRIBE_STRING*/);
  fprintf (fid, "$end\n");
  fprintf (fid, "$comment\n");
  fprintf (fid, "  Deadtime trace.\n");
  fprintf (fid, "$end\n");
  fprintf (fid, "$timescale 1ns $end\n");
  fprintf (fid, "$scope module setup $end\n");

  /*
  fprintf (fid, "$var wire 1 a sig $end\n");
  fprintf (fid, "$var wire BITWIDTH ID NAME $end\n");
  */

  nsystems = 0;

  PD_LL_FOREACH(_lwroc_dtt_systems, iter)
    {
      char *dot;
      char *colon;

      syst =
	PD_LL_ITEM(iter, lwroc_dtt_system, _items);

      syst->_shortname = strdup_chk(syst->_name);

      dot = strchr(syst->_shortname, '.');
      if (dot)
	*dot = 0;

      colon = strchr(syst->_shortname, ':');
      if (colon)
	*colon = '_';

      syst->_vcdid_dt = lwroc_dtt_new_vcdid();
      syst->_vcdid_cpu = lwroc_dtt_new_vcdid();

      printf ("$var wire 1 %s %s_DT $end\n",
	      syst->_vcdid_dt,
	      syst->_shortname);
      printf ("$var wire 1 %s %s_cpu $end\n",
	      syst->_vcdid_cpu,
	      syst->_shortname);

      if (!syst->_master) /* We are master. */
	{
	  syst->_vcdid_event_no = lwroc_dtt_new_vcdid();
	  syst->_vcdid_trig     = lwroc_dtt_new_vcdid();

	  printf ("$var wire 32 %s %s_event $end\n",
		  syst->_vcdid_event_no,
		  syst->_shortname);
	  printf ("$var wire 4 %s %s_trig $end\n",
		  syst->_vcdid_trig,
		  syst->_shortname);
	}

      nsystems++;

      syst->_index = (int) nsystems;
    }

  fprintf (fid, "$upscope $end\n");
  fprintf (fid, "$enddefinitions $end\n");
  fprintf (fid, "$dumpvars\n");
  /* Initial values. */
  /* "value""ID" */

  systems = (lwroc_dtt_system **)
    malloc (nsystems * sizeof (lwroc_dtt_system *));

  if (!systems)
    LWROC_FATAL("Memory allocation failure (systems dump list).");

  systems_active = 0;

  PD_LL_FOREACH(_lwroc_dtt_systems, iter)
    {
      syst =
	PD_LL_ITEM(iter, lwroc_dtt_system, _items);

      printf ("x%s\n", syst->_vcdid_dt);
      printf ("x%s\n", syst->_vcdid_cpu);

      if (!syst->_master) /* We are master. */
	{
	  fprintf (fid, "b%s %s\n",
		   lwroc_fmt_binary_fixed(bin, 'x', 32),
		   syst->_vcdid_event_no);
	  fprintf (fid, "b%s %s\n",
		   lwroc_fmt_binary_fixed(bin, 'x', 4),
		   syst->_vcdid_trig);
	}

      syst->_next_event = 0;
      syst->_next_mark = (size_t) -1;

      if (lwroc_dtt_system_find_next_time(syst))
	{
	  LWROC_HEAP_INSERT(lwroc_dtt_system *,
			    systems,
			    systems_active,
			    lwroc_dtt_system_compare_less,
			    syst, NULL);
	}
    }

  fprintf (fid, "$end\n");

  cur_time = DBL_MIN;

  while (systems_active)
    {
      lwroc_dtt_event *event;
      lwroc_dtt_mark  *mark;

      /*
      size_t i;
      printf("LIST: %" MYPRIzd "\n", systems_active);
      for (i = 0; i < systems_active; i++)
	{
	  printf ("LIST%" MYPRIzd ": %p : %.9f\n",
		  i, systems[i], systems[i]->_next_time);
	}
      fflush(stdout);
      */

      /* The array is a heap, with the first item as the next to handle. */
      syst = systems[0];

      if (syst->_next_time < cur_time)
	{
	  LWROC_FATAL_FMT("Backwards time during .vcd dump (%.9f < %.9f).",
			  syst->_next_time, cur_time);
	}
      else if (syst->_next_time > cur_time)
	{
	  cur_time = syst->_next_time;

	  /* The time of the event is the next time we found. */
	  fprintf (fid, "#%.0f\n",
		   cur_time * 1.e9);
	}

      event = &_lwroc_dtt_events[syst->_events[syst->_next_event]];
      mark = &_lwroc_dtt_marks[event->_mark_off + syst->_next_mark];

      switch (mark->_kind)
	{
	case LWROC_DTT_KIND_START_CTIME:
	  fprintf (fid, "1%s\n", syst->_vcdid_dt);
	  if (!syst->_master)
	    {
	      fprintf (fid, "b%s %s\n",
		       lwroc_fmt_binary(bin, event->_eventno, 32),
		       syst->_vcdid_event_no);
	      fprintf (fid, "b%s %s\n",
		       lwroc_fmt_binary(bin, event->_trig, 4),
		       syst->_vcdid_trig);
	    }
	  break;
	case LWROC_DTT_KIND_AFTER_POLL:
	  fprintf (fid, "1%s\n", syst->_vcdid_cpu);
	  break;
	case LWROC_DTT_KIND_AFTER_DT_RELEASE:
	  fprintf (fid, "0%s\n", syst->_vcdid_dt);
	  break;
	case LWROC_DTT_KIND_AFTER_POSTPROC:
	  fprintf (fid, "0%s\n", syst->_vcdid_dt);
	  fprintf (fid, "0%s\n", syst->_vcdid_cpu);
	  /* We do *not* set the eventno / trig to 0
	   * again after the event.
	   * Reason: it does not display well.
	   */
	  break;
	case LWROC_DTT_KIND_RECORDING_GAP:
	  fprintf (fid, "x%s\n", syst->_vcdid_dt);
	  fprintf (fid, "x%s\n", syst->_vcdid_cpu);
	  if (!syst->_master) /* We are master. */
	    {
	      fprintf (fid, "b%s %s\n",
		       lwroc_fmt_binary_fixed(bin, 'x', 32),
		       syst->_vcdid_event_no);
	      fprintf (fid, "b%s %s\n",
		       lwroc_fmt_binary_fixed(bin, 'x', 4),
		       syst->_vcdid_trig);
	    }
	  /* We do *not* set the eventno / trig to 0
	   * again after the event.
	   * Reason: it does not display well.
	   */
	  break;

	}

      if (!lwroc_dtt_system_find_next_time(syst))
	{
	  /* No further items for this system, get rid of it
	   * by using the last one (which will be resorted).
	   */
	  systems[0] = systems[--systems_active];
	}

      LWROC_HEAP_MOVE_DOWN(lwroc_dtt_system *,
			   systems,
			   systems_active,
			   lwroc_dtt_system_compare_less,
			   0, NULL);
    }
}

int main(int argc, char *argv[])
{
  int i;

  _lwroc_message_local = 1;

  colourtext_init();

  for (i = 1; i < argc; i++)
    {
      char *request = argv[i];

      if (LWROC_MATCH_C_ARG("--help")) {
	lwrocdt_usage(argv[0]);
	exit(0);
      } else {
	LWROC_BADCFG_FMT("Unrecognized option: '%s'", argv[i]);
      }
    }

  lwroc_dtt_read_input(stdin);

  /* lwroc_dtt_dump_data(); */

  lwroc_dtt_align_timescales();

  /* lwroc_dtt_dump_data(); */

  lwroc_dtt_write_vcd(stdout);

  return 0;
}

/********************************************************************/

/* Force-include, to avoid version from library.
 */

#define LWROC_TRACK_TIMESTAMP_LOG_FMT       LWROC_LOG_FMT
#define LWROC_TRACK_TIMESTAMP_CCLOG_FMT     LWROC_CCLOG_FMT
#define LWROC_TRACK_TIMESTAMP_INFO_FMT      LWROC_INFO_FMT
#define LWROC_TRACK_TIMESTAMP_ERROR_FMT     LWROC_ERROR_FMT
#define LWROC_TRACK_TIMESTAMP_WARNING_FMT   LWROC_WARNING_FMT

#define LWROC_TRACK_TIMESTAMP_LOG_NEW_FIT   1

#include "../dtc_arch/acc_def/mynan.h"
#include "lwroc_track_timestamp.c"

/********************************************************************/

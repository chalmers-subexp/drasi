/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwroc_parse_util.h"
#include "lwroc_hostname_util.h"

#include "lwroc_message.h"
#include "lwroc_message_internal.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include "../dtc_arch/acc_def/has_curses.h"
#if HAS_CURSES
#define USE_CURSES 1
#endif
#include "../lu_common/colourtext.h"


/* All text leading up to a '\n' is considered one line.
 * Since we have encoded some preliminary information in text
 * followed by one (or more) '\r', that is also part of the line.
 */

/* The format of the main line is:
 *
 * YYYY-MM-DD hh:mm:ss.ss LV node[:port]/THREAD     Message with whatever\n
 *
 * All division is done by spaces.  I.e., a space in the node name or
 * the thread name would break this parsing.  The time is in local
 * time.  The first ancillary line is:
 *
 * +/-xxxx tzon :file:line:[auxiliary]\r
 *
 * +/-xxxx is e.g. +0200 for times that are 2 hours ahead of UTC, or
 * -0600 for times that are six hours behind.  file:line gives the
 * source location of the original message.  This can then be followed
 * by one or more space-or-\r-separated auxiliary information.  For
 * the time being just 'Bo,l' giving the offset and length of a
 * suggested boldface mark of the message.  The auxiliary information
 * shall come in order.  Any global information shall come before
 * location-specific information.
 */

char _lwroclog_hostname[256];
char *_lwroclog_hostname_end = NULL;

#define LWRLOG_LEVEL_SIGNAL      (1 << (LWROC_MSGLVL_SIGNAL ))
#define LWRLOG_LEVEL_BUG         (1 << (LWROC_MSGLVL_BUG    ))
#define LWRLOG_LEVEL_FATAL       (1 << (LWROC_MSGLVL_FATAL  ))
#define LWRLOG_LEVEL_BADCFG      (1 << (LWROC_MSGLVL_BADCFG ))
#define LWRLOG_LEVEL_ERROR       (1 << (LWROC_MSGLVL_ERROR  ))
#define LWRLOG_LEVEL_ATTACH      (1 << (LWROC_MSGLVL_ATTACH ))
#define LWRLOG_LEVEL_DETACH      (1 << (LWROC_MSGLVL_DETACH ))
#define LWRLOG_LEVEL_WARNING     (1 << (LWROC_MSGLVL_WARNING))
#define LWRLOG_LEVEL_ACTION      (1 << (LWROC_MSGLVL_ACTION ))
#define LWRLOG_LEVEL_INFO        (1 << (LWROC_MSGLVL_INFO   ))
#define LWRLOG_LEVEL_LOG         (1 << (LWROC_MSGLVL_LOG    ))
#define LWRLOG_LEVEL_DEBUG       (1 << (LWROC_MSGLVL_DEBUG  ))
#define LWRLOG_LEVEL_SPAM        (1 << (LWROC_MSGLVL_SPAM   ))

#define LWRLOG_FIELD_DATE        0x00000001
#define LWRLOG_FIELD_TIME        0x00000002
#define LWRLOG_FIELD_TIMESFRAC   0x00000004
#define LWRLOG_FIELD_TOFFZONE    0x00000008
#define LWRLOG_FIELD_FILELINE    0x00000010
#define LWRLOG_FIELD_LEVEL       0x00000020
#define LWRLOG_FIELD_HOSTTHREAD  0x00000040
#define LWRLOG_FIELD_DOMAIN      0x00000080
#define LWRLOG_FIELD_MESSAGE     0x00000100
#define LWRLOG_FIELD_ALL         0x000001ff

typedef struct lwrlog_config_t
{
  int  _show_level;
  int  _show_field;

} lwrlog_config;

lwrlog_config _config;

typedef struct lwrlog_line_item_t
{
  const char *_s;
  const char *_e;
} lwrlog_line_item;

#define LWRLOG_LINE_ITEM_SET_NULL(x)		\
  do { (x)._s = (x)._e = NULL; } while (0)
#define LWRLOG_LINE_ITEM_EAT_TRAIL_SPACE(x)				\
  do { while ((x)._e > (x)._s && (x)._e[-1] == ' ') (x)._e--; } while (0)

char _lwrlog_prevdate[10] = { 0,0,0,0,0,0,0,0,0,0 };
int  _lwrlog_lines_since_header = 0;

void lwrlog_parse_bold_mark(lwrlog_line_item *aux,
			    int *bold_start, int *bold_len)
{
  /* Eat space and cr. */

  while (aux->_s < aux->_e &&
	 (*aux->_s == ' ' || *aux->_s == '\r'))
    aux->_s++;

  /* Do we have a bold-mark? */

  if (aux->_s < aux->_e &&
      *aux->_s == 'B')
    {
      char *end;
      aux->_s++; /* Move past the B, will not match here again. */

      *bold_start = (int) strtol(aux->_s, &end, 10);
      if (end < aux->_e && *end == ',')
	{
	  *bold_len = (int) strtol(end+1, &end, 10);
	  if (end <= aux->_e &&
	      (*end == ' ' || *end == '\r'))
	    {
	      aux->_s = end+1;
	      return; /* Found. */
	    }
	}
    }
  *bold_start = -1; /* None found. */
}

int add_printed(ssize_t len, int *printed)
{
  *printed += (int) len;
  return (int) len;
}

#define PRLEN(len) (add_printed((len),&printed))
#define PRLENADD(len) do { printed += (len); }  while (0)

void lwrlog_handle_line(const char *line, const char *end)
{
  lwrlog_line_item junkaux, toffzone, file, fileline, aux;
  lwrlog_line_item mdate, mtime, mtimesfrac, level;
  lwrlog_line_item host, hostport, thread;
  lwrlog_line_item message;

  const char *p;
  const char *cr;
  const char *lcr;
  const char *space;
  const char *hptend = NULL;

  int id_level = 0;
  int date_change = 0;
  int printed = 0;

  LWRLOG_LINE_ITEM_SET_NULL(junkaux);
  LWRLOG_LINE_ITEM_SET_NULL(toffzone);
  LWRLOG_LINE_ITEM_SET_NULL(file);
  LWRLOG_LINE_ITEM_SET_NULL(fileline);
  LWRLOG_LINE_ITEM_SET_NULL(aux);
  LWRLOG_LINE_ITEM_SET_NULL(mdate);
  LWRLOG_LINE_ITEM_SET_NULL(mtime);
  LWRLOG_LINE_ITEM_SET_NULL(mtimesfrac);
  LWRLOG_LINE_ITEM_SET_NULL(level);
  LWRLOG_LINE_ITEM_SET_NULL(host);
  LWRLOG_LINE_ITEM_SET_NULL(hostport);
  LWRLOG_LINE_ITEM_SET_NULL(thread);
  LWRLOG_LINE_ITEM_SET_NULL(message);

  /* When we come here, end is either at the (presumed) very end or
   * pointing to the newline.  In either case, we just assume it to be
   * the end.
   */

  /* We first search for the '\r'. */

  cr = (char *) memchr (line, '\r', (size_t) (end - line));

  if (cr)
    {
      const char *colon, *cur;

      /* Also find the last '\r', terminating the aux info, */

      lcr = (char *) memchr (line, '\r', (size_t) (end - line));

      /* The time-zone info is terminated by a colon. */

      colon = (char *) memchr(line, ':', (size_t) (cr - line));

      if (colon)
	{
	  toffzone._s = line;
	  toffzone._e = colon;

	  LWRLOG_LINE_ITEM_EAT_TRAIL_SPACE(toffzone);

	  cur = colon + 1;

	  /* File-line has one colon inside, and is terminated by
	   * another one.
	   */

	  colon = (char *) memchr(cur, ':', (size_t) (cr - cur));

	  if (colon)
	    {
	      file._s = fileline._s = cur;
	      file._e = fileline._e = colon;

	      cur = colon + 1;

	      colon = (char *) memchr(cur, ':', (size_t) (cr - cur));

	      if (colon)
		{
		  fileline._e = colon;

		  cur = colon + 1;
		}
	    }

	  aux._s = cur;
	  aux._e = lcr;
	}
      else
	{
	  junkaux._s = line;
	  junkaux._e = lcr;
	}
      p = lcr + 1;
    }
  else
    p = line;

  /* Now ready to dissect the main line. */

  space = (char *) memchr (p, ' ', (size_t) (end - p));

  if (space)
    {
      mdate._s = p;
      mdate._e = space;

      p = space + 1;

      while (p < end && *p == ' ')
	p++;

      space = (char *) memchr (p, ' ', (size_t) (end - p));

      if (space)
	{
	  char *dot;

	  mtime._s = p;
	  mtime._e = space;

	  dot = (char *) memchr (p, '.', (size_t) (space - p));

	  if (dot)
	    {
	      mtime._e = dot;
	      mtimesfrac._s = dot;
	      mtimesfrac._e = space;
	    }

	  p = space + 1;

	  while (p < end && *p == ' ')
	    p++;

	  space = (char *) memchr (p, ' ', (size_t) (end - p));

	  if (space)
	    {
	      level._s = p;
	      level._e = space;

	      p = space + 1;

	      while (p < end && *p == ' ')
		p++;

	      space = (char *) memchr (p, ' ', (size_t) (end - p));

	      if (space)
		{
		  char *slash;
		  char *colon;

		  host._s = p;
		  host._e = space;

		  hptend = space;

		  slash =
		    (char *) memchr (p, '/', (size_t) (space - p));

		  if (slash)
		    {
		      host._e = slash;
		      thread._s = slash+1;
		      thread._e = space;
		    }

		  colon =
		    (char *) memchr (p, ':', (size_t) (host._e - p));

		  if (colon)
		    {
		      hostport._s = colon;
		      hostport._e = host._e;
		      host._e = colon;
		    }

		  p = space + 1;

		  while (p < end && *p == ' ')
		    p++;
		}
	    }
	}
    }

  message._s = p;
  message._e = end;

  /* Recover the indent (leading spaces) of the message.
   * Only happens if we have a parsed host-port-thread end.
   *
   * This depends on the log-writer padding the host-port-thread
   * to 15 characters with spaces.  Mishaps are harmless though,
   * as we only will give a wrong indentation.
   */
  if (hptend)
    {
      ssize_t hptlen = hptend - host._s;

      if (hptlen < 15)
	hptend = host._s + 15;

      hptend++;

      if (hptend < message._s)
	message._s = hptend;
    }

  /* Find out the level. */

  if (level._s)
    switch (*level._s)
      {
      case 'S': id_level = LWROC_MSGLVL_SIGNAL;  break;
      case 'B': id_level = LWROC_MSGLVL_BUG;     break;
      case 'F': id_level = LWROC_MSGLVL_FATAL;   break;
      case 'C': id_level = LWROC_MSGLVL_BADCFG;  break;
      case 'E': id_level = LWROC_MSGLVL_ERROR;   break;
      case 'A': id_level = LWROC_MSGLVL_ATTACH;  break;
      case 'D': id_level = LWROC_MSGLVL_DETACH;  break;
      case 'W': id_level = LWROC_MSGLVL_WARNING; break;
      case 'a': id_level = LWROC_MSGLVL_ACTION;  break;
      case 'i': id_level = LWROC_MSGLVL_INFO;    break;
      case 'l': id_level = LWROC_MSGLVL_LOG;     break;
      case 'd': id_level = LWROC_MSGLVL_DEBUG;   break;
      case 's': id_level = LWROC_MSGLVL_SPAM;    break;
      default: break;
    }

  /* Do any selections. */

  if (id_level &&
      !(_config._show_level & (1 << id_level)))
    return; /* Level identified, but not wanted. */

  /* We print a header-line if the date has changed. */

  if (mdate._e && mdate._e - mdate._s == 10)
    {
      if (memcmp(mdate._s, _lwrlog_prevdate, 10) != 0)
	{
	  memcpy(_lwrlog_prevdate, mdate._s, 10);
	  date_change = 1;
	}
    }
  else
    date_change = 1; /* badly formatted */

  if (!(_config._show_field & LWRLOG_FIELD_DATE))
    {
      if (date_change ||
	  _lwrlog_lines_since_header >= 20)
	{
	  printf ("%s%.*s%s\n",
		  CT_OUT(BOLD),
		  PRLEN(mdate._e - mdate._s),
		  mdate._s ? mdate._s : "",
		  CT_OUT(NORM));
	  _lwrlog_lines_since_header = 0;
	}
      _lwrlog_lines_since_header++;
    }

  if (_config._show_field & LWRLOG_FIELD_DATE)
    printf ("%.*s ",
	    PRLEN(mdate._e - mdate._s),
	    mdate._s ? mdate._s : "");
  PRLENADD(1);

  if (_config._show_field & (LWRLOG_FIELD_TIME |
			     LWRLOG_FIELD_TIMESFRAC))
    {
      printf ("%.*s%.*s ",
	      (_config._show_field & LWRLOG_FIELD_TIME) ?
	      PRLEN(mtime._e - mtime._s) : 0,
	      mtime._s ? mtime._s : "",
	      (_config._show_field & LWRLOG_FIELD_TIMESFRAC) ?
	      PRLEN(mtimesfrac._e - mtimesfrac._s) : 0,
	      mtimesfrac._s ? mtimesfrac._s : "");
      PRLENADD(1);
    }

  if (_config._show_field & LWRLOG_FIELD_TOFFZONE)
    printf ("%.*s ",
	    PRLEN(toffzone._e - toffzone._s),
	    toffzone._s ? toffzone._s : "");
  PRLENADD(1);

  if (_config._show_field & LWRLOG_FIELD_HOSTTHREAD)
    {
      if (!(_config._show_field & LWRLOG_FIELD_DOMAIN))
	{
	  /* First try to cut down common parts of the hostname.
	   * (Common with the hostname of the machine running this
	   * program.
	   */

	  const char *cmp_end = _lwroclog_hostname_end;
	  const char *chk_end = host._e;

	  while (cmp_end != _lwroclog_hostname &&
		 chk_end != host._s)
	    {
	      cmp_end--;
	      chk_end--;

	      if (*cmp_end != *chk_end)
		break;
	      /* Whenever we find a dot, this is a common level to leave out. */
	      if (*chk_end == '.')
		host._e = chk_end+1;
	    }
	}
      {
	int hostlen = (int) (host._e - host._s);
	int hostportlen = (int) (hostport._e - hostport._s);
	int threadlen = (int) (thread._e - thread._s);
	int pad = 15 - (hostlen + hostportlen);
	if (pad < threadlen) pad = threadlen;

	printf ("%s%.*s%.*s%s/%-*.*s ",
		CT_OUT(UL),
		PRLEN(hostlen), host._s ? host._s : "",
		PRLEN(hostportlen), hostport._s ? hostport._s : "",
		CT_OUT(NORM),
		PRLEN(pad),
		threadlen, thread._s ? thread._s : "");
      }
      PRLENADD(1);
    }

  if (_config._show_field & LWRLOG_FIELD_FILELINE)
    {
      int filelinelen = (int) (fileline._e - fileline._s);
      int pad = 25;
      if (pad < filelinelen) pad = filelinelen;
      printf ("%-*.*s ",
	      PRLEN(pad),
	      filelinelen, fileline._s ? fileline._s : "");
    }
  PRLENADD(1);

  if (_config._show_field & LWRLOG_FIELD_LEVEL)
    {
      int levellen = (int) (level._e - level._s);
      int pad = 2;
      if (pad < levellen) pad = levellen;
      printf ("%-*.*s ",
	      PRLEN(pad),
	      levellen, level._s ? level._s : "");
    }
  PRLENADD(1);

 {
    const char *ctext;
    const char *ctextback;

    lwroc_message_colourstr(id_level, 0, &ctext, &ctextback);

    printf ("%s", ctext);

    if ((_config._show_field & LWRLOG_FIELD_MESSAGE) &&
	message._s)
      {
	/* Go through the message together with any bold-face
	 * markings.
	 */

	int msg_len = (int) (message._e - message._s);
	int msg_off = 0;

	for ( ; ; )
	  {
	    int bold_start = -1;
	    int bold_len = 0;

	    /* printf ("B? '%.*s' -> ", (int) (aux._e - aux._s), aux._s); */

	    lwrlog_parse_bold_mark(&aux, &bold_start, &bold_len);

	    /* printf ("B: %d %d\n", bold_start, bold_len); */

	    if (bold_start < msg_off || /* handles -1 */
		bold_start >= msg_len)
	      {
		/* Dump remainder of message. */
		printf ("%.*s", msg_len - msg_off, message._s + msg_off);
		break;
	      }
	    if (bold_start + bold_len > msg_len)
	      bold_len = msg_len - bold_start;
	    /* Dump up to the bold start, bold mark. */
	    printf ("%.*s%s%.*s%s%s",
		    bold_start - msg_off, message._s + msg_off,
		    CT_OUT(BOLD),
		    bold_len, message._s + bold_start,
		    CT_OUT(NORM),ctext);

	    msg_off = bold_start + bold_len;
	  }
      }

    printf ("%s\n", ctextback);
  }
}







void lwroclog_usage(char *cmdname)
{
  printf ("LWROC log selection and colouriser tool\n");
  printf ("\n");
  printf ("Usage: %s <options>\n", cmdname);
  printf ("\n");
  printf ("  --gdb                    Run me in GDB (must be the first "
	  "argument).\n");
  printf ("  --[info|log|debug|spam]  Minimum severity of messages to show.\n");
  printf ("  --us                     Show fractional time (us).\n");
  printf ("  --file                   Show source location of log message.\n");
  printf ("  --full                   Show all log fields.\n");
  printf ("  --colour=yes|no          Force colour and markup on or off.\n");
  printf ("  --help                   Show this message.\n");
  printf ("\n");
}

int main(int argc, char *argv[])
{
  /* const char *filename = NULL; */
  int i;

  _lwroc_message_local = 1;

  colourtext_init();

  memset(&_config, 0, sizeof (_config));

  _config._show_level =
    LWRLOG_LEVEL_SIGNAL |
    LWRLOG_LEVEL_BUG | LWRLOG_LEVEL_FATAL | LWRLOG_LEVEL_ERROR |
    LWRLOG_LEVEL_ATTACH | LWRLOG_LEVEL_DETACH |
    LWRLOG_LEVEL_WARNING | LWRLOG_LEVEL_ACTION;

  _config._show_field =
    LWRLOG_FIELD_TIME | LWRLOG_FIELD_HOSTTHREAD | LWRLOG_FIELD_MESSAGE;

  lwroc_gethostname(_lwroclog_hostname, sizeof (_lwroclog_hostname));
  _lwroclog_hostname_end = _lwroclog_hostname + strlen(_lwroclog_hostname);

  for (i = 1; i < argc; i++)
    {
      char *request = argv[i];
      char *post;

      if (LWROC_MATCH_C_ARG("--help")) {
	lwroclog_usage(argv[0]);
	exit(0);
      } else if (LWROC_MATCH_C_PREFIX("--colour=",post)) {
	int force = 0;

	if (strcmp(post,"yes") == 0) {
#if HAS_CURSES
	  force = 1;
#else
	  LWROC_WARNING("No colour support since ncurses not compiled in.\n");
#endif
	} else if (strcmp(post,"no") == 0)
	  force = -1;
	else if (strcmp(post,"auto") != 0)
	  LWROC_BADCFG_FMT("Bad option '%s' for --colour=", post);

	colourtext_setforce(force);
      } else if (LWROC_MATCH_C_ARG("--info")) {
	_config._show_level |=
	  LWRLOG_LEVEL_INFO;
      } else if (LWROC_MATCH_C_ARG("--log")) {
	_config._show_level |=
	  LWRLOG_LEVEL_INFO | LWRLOG_LEVEL_LOG;
      } else if (LWROC_MATCH_C_ARG("--debug")) {
	_config._show_level |=
	  LWRLOG_LEVEL_INFO | LWRLOG_LEVEL_LOG | LWRLOG_LEVEL_DEBUG;
      } else if (LWROC_MATCH_C_ARG("--spam")) {
	_config._show_level |=
	  LWRLOG_LEVEL_INFO | LWRLOG_LEVEL_LOG | LWRLOG_LEVEL_DEBUG |
	  LWRLOG_LEVEL_SPAM;
      } else if (LWROC_MATCH_C_ARG("--file")) {
	_config._show_field |= LWRLOG_FIELD_FILELINE;
      } else if (LWROC_MATCH_C_ARG("--full")) {
	_config._show_field = LWRLOG_FIELD_ALL;
      } else if (LWROC_MATCH_C_ARG("--us")) {
	_config._show_field |= LWRLOG_FIELD_TIMESFRAC;
      } else {
	/* Filename. */
	/* filename = argv[i]; */
	LWROC_FATAL_FMT("Unhandled argument: %s", argv[i]);
      }
    }

  /* (void) filename; */

  {
    char line[1024];

    while (fgets(line, sizeof (line), stdin))
      {
	char *e = line + strlen(line);

	if (e > line && *(e-1) == '\n')
	  *(--e) = 0;

	lwrlog_handle_line(line, e);
      }
  }

  return 0;
}

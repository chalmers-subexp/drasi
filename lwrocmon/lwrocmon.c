/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include "lwrocmon_struct.h"
#include "lwroc_mon.h"

#include "lwroc_message.h"
#include "lwroc_message_internal.h"
#include "lwroc_parse_util.h"
#include "lwroc_leap_seconds.h"

#include "../dtc_arch/acc_def/has_curses.h"
#if HAS_CURSES
#define USE_CURSES 1
#endif
#include "../lu_common/colourtext.h"
/*#include <locale.h>*/

#include <stdio.h>
#include <signal.h>

#define STATE_MACHINE_DEBUG 0

/* We have a few 'major' modes of operation.
 *
 * When only one host is given, we work in rate mode, i.e. about once
 * per second, we print the status updates from the host in question.
 *
 * With several hostnames given, we show the tree-style
 * ncurses-display of all hosts given, with their current status.
 * (If space enough, doing rate at the bottom...)
 *
 * When given --log, we collect log-messages from all given hosts into
 * a log-file.  This does *not* display the log messages, see lwrocmark
 * for that.
 *
 * With --ctrl, we may send control messages.  This mode quits after
 * communication is over.
 */

lwrocmon_config _config;

#define LWROCMON_NEW_HEADER_AFTER_DATA_LINES  19

struct timeval _lwrocmon_next_time_print;
int _lwrocmon_print_header_ago = -1;
int _lwrocmon_num_print;
int _lwrocmon_event_incr_nonzero = 0;

lwrocmon_rate_state _lwrocmon_rate_state;

/********************************************************************/

volatile int _lwrocmon_shutdown = 0;

PD_LL_SENTINEL(_lwrocmon_net_clients);

PD_LL_SENTINEL(_lwrocmon_conns);

/********************************************************************/

void lwrocmon_loop_setup_service(void *arg,
				 lwroc_select_info *si)
{
  (void) arg;

  if (timerisset(&_lwrocmon_next_time_print))
    lwroc_select_info_time_setup(si, &_lwrocmon_next_time_print);
}

/********************************************************************/

void lwrocmon_loop_before_service(void *arg,
				 lwroc_select_info *si)
{
  (void) arg;
  (void) si;

  /* TODO: In order to print messages in the correct time order,
   * we need to do two things:
   *
   * - whenever multiple sources have messages that has passed the
   *   time for their printing (i.e. a certain delay), we need to
   *   select the earliest message for printing.
   *
   * - since we may not read another message in just one cycle for
   *   whomever we just ate a message, we must have some buffer to
   *   make it more likely that it does not end up after other
   *   messages just because this program was too slow.
   *
   * Note: these efforts do not guarantee time order.  For that we
   * would have to do time-sorting, i.e. always check that other
   * sources have no later messages.  But that does not scale - for
   * every message form some client, all other clients get a query.
   * Also it may hold delivery up.  We opt for the usually-corrent
   * solution.
   *
   * A buffer is anyhow required in order to deal with 'danger'
   * messages, i.e. those that shall have their level bumped up to
   * fatal in case the connection is lost before 'undanger' was
   * reported.  (Used around potentially crash/lock-up-prone acts,
   * like first hardware access.
   */

  (void) si;
}

/********************************************************************/

int _printed_log_message = 0;
struct timeval _printed_log_message_at;

void lwrocmon_loop_after_service(void *arg,
				 lwroc_select_info *si)
{
  (void) arg;

  LFENCE;
  if (_lwroc_message_printed &&
      _config._log_filename)
    {
      lwrocmon_print_message_status(&si->now);
      _lwroc_message_printed = 0; /* Reset. */
      SFENCE;
    }

  if (timerisset(&_lwrocmon_next_time_print) &&
      !(timercmp(&si->now, &_lwrocmon_next_time_print, <)))
    {
      /* We have passed the time. */

      if (_config._print_rate) {
	if (_lwrocmon_print_header_ago == -1 ||
	    _lwrocmon_print_header_ago >= LWROCMON_NEW_HEADER_AFTER_DATA_LINES)
	  {
	    lwrocmon_loop_rate_header();

	    _lwrocmon_print_header_ago = 0;
	  }
	lwrocmon_loop_rate_data(_lwrocmon_print_header_ago);
	_lwrocmon_print_header_ago++;
      }

      if (_config._log_filename)
	lwrocmon_print_message_status(&si->now);

      if (_config._show_detail)
	{
	  if (_lwrocmon_source_update)
	    lwrocmon_detail_source_update();
	  if (_lwrocmon_block_update)
	    lwrocmon_detail_block_update();
	}
      if (_config._show_tree)
	{
	  if (_lwrocmon_source_update)
	    lwrocmon_tree_source_update();
	  if (_lwrocmon_block_update)
	    lwrocmon_tree_block_update();
	}
      _lwrocmon_source_update = 0;
      _lwrocmon_block_update = 0;

      /* Schedule the print to happen a bit after the top of the
       * second.  Last scheduled production of a monitor message is 20
       * ms efter the top.  We also have passed network, so set it for
       * 100 ms after the top.
       */

      if (_config._print_rate ||
	  _config._log_filename)
	{
	  _lwrocmon_next_time_print = si->now;
	  _lwrocmon_next_time_print.tv_usec = 100000;
	  if (timercmp(&_lwrocmon_next_time_print,&si->now,<)) {
	    _lwrocmon_next_time_print.tv_sec +=
	      (_config._print_rate ? _config._print_rate : 1);
	  }
	}
      else if (_config._show_detail ||
	       _config._show_tree)
	{
	  _lwrocmon_next_time_print = si->now;
	  if (_lwrocmon_next_time_print.tv_usec < 100000)
	    {
	      _lwrocmon_next_time_print.tv_usec = 100000;
	    }
	  else if (_lwrocmon_next_time_print.tv_usec >= 600000)
	    {
	      _lwrocmon_next_time_print.tv_sec++;
	      _lwrocmon_next_time_print.tv_usec = 100000;
	    }
	  else
	    {
	      _lwrocmon_next_time_print.tv_usec = 600000;
	    }
	}

      if (_config._num_print)
	{
	  _lwrocmon_num_print--;

	  if (!_lwrocmon_num_print)
	    {
	      if (_config._nz_event_incr &&
		  !_lwrocmon_event_incr_nonzero)
		exit(2);
	      exit(0);
	    }
	}

      lwrocmon_data_bump_good_prev();
    }

  if (_fid_messages)
    {
      if (_printed_log_message)
	{
	  _printed_log_message_at = si->now;
	  _printed_log_message = 0;
	}
      else
	{
	  struct timeval mark_at;

	  mark_at = _printed_log_message_at;
	  mark_at.tv_sec += 3600;

	  if (timercmp(&si->now, &_printed_log_message_at, <))
	    _printed_log_message_at = si->now;
	  else if (timercmp(&si->now, &mark_at, >))
	    {
	      lwroc_print_msg_greeting(2);
	      /* Will set _printed_log_message, thus next round here
	       * resets timer.
	       */
	    }
	}
    }
}

/********************************************************************/

void lwrocmon_loop(void)
{
  lwroc_select_loop(&_lwrocmon_net_clients, &_lwrocmon_shutdown, NULL,
		    lwrocmon_loop_setup_service, NULL,
		    NULL, NULL,
		    lwrocmon_loop_after_service, NULL,
		    1);
}

/********************************************************************/

void lwrocmon_init_conn_list(void)
{
}

/********************************************************************/

volatile int _lwrocmon_quit_count = 0;

void lwrocmon_sighandler(int sig)
{
  switch(sig)
    {
    case SIGINT:
      if (_lwrocmon_quit_count < 2)
	_lwrocmon_shutdown = 1;
      return;
    case SIGTERM:
      if (_lwrocmon_quit_count < 2)
	_lwrocmon_shutdown = 1;
      return;
    }
  exit(0);
}

/********************************************************************/

void lwrocmon_usage(char *cmdname)
{
  printf ("LWROC monitor tool\n");
  printf ("\n");
  printf ("Usage: %s <options> HOST[:PORT]...\n", cmdname);
  printf ("\n");
  printf ("  --gdb                    Run me in GDB (must be the first "
	  "argument).\n");
  printf ("  --rate[=N]               Show rate for given systems "
	  "(every N s).\n");
  printf ("  --tree                   Show ncurses tree view of systems.\n");
  printf ("  --no-up                  Do not search up in tree.\n");
  printf ("  --no-down                Do not search down in tree.\n");
  printf ("  --no-down-up             Do not search up in tree after destinations.\n");
  printf ("  --no-ts-track        (t) Do not show timestamp tracking vs. CPU clock/NTP.\n");
  printf ("  --no-ts-analysis     (s) Do not show timestamp analysis.\n");
  printf ("  --no-dams            (d) Do not show DAM (data acq. mod.) statistics.\n");
  printf ("  --no-lookup              Do not do hostname lookup.\n");
  printf ("  --raw-ts[=hex|dec]   (r) Show raw timestamp values.\n");
  printf ("  --buf-size           (b) Show buffer sizes instead of fill ratios.\n");
  printf ("  --detail                 Show ncurses detailed system status.\n");
  printf ("  --monbuf                 Dump raw monitor buffer data.\n");
  printf ("  --count=N                Quit after showing N reports "
	  "(only --rate).\n");
  printf ("  --influxdb               Lines for InfluxDB line protocol "
	  "(requires --rate).\n");
  printf ("  --expire-time=N          Expire stale data (lost connection) "
	  "after N s.\n");
  printf ("  --nz-rate-success        Exit status 0 if event increase seen "
	  "(only --rate).\n");
  printf ("  --dt-trace               Store deadtime trace files "
	  "(requires --tree).\n");
  printf ("  --log[=FILENAME]         Record log messages from systems.\n");
  printf ("  --help                   Show this message.\n");
  printf ("  HOST[:[+]PORT]           Drasi instance(s) to monitor.");
  printf ("\n");
}

/********************************************************************/

int main(int argc,char *argv[])
{
  int i;

  _lwroc_message_local = 1;

  /* There is no reason to connect to localhost by default.
  {
    struct sockaddr_in addr;
    addr.sin_addr.s_addr = 0x0100007f;
    addr.sin_family = AF_INET;
    addr.sin_port = LWROC_NET_DEFAULT_PORT;
    lwrocmon_add_mon_host(NULL,(struct sockaddr_storage *) &addr,
			  LWROCMON_CONN_ADDED_PRIMARY);
  }
  */
  /* setlocale(LC_ALL, ""); */
  colourtext_init();

  if (argc == 1)
    {
      lwrocmon_usage(argv[0]);
      exit(1);
    }

  memset(&_config, 0, sizeof (_config));

  _config._pd_config._flags = LWROC_PRINT_DIFF_CONFIG_USE_PREFIX;
  _config._expire_time = 5;
  _config._print_ts_analysis = LWROCMON_TS_ANALYSIS_SYNC_CHECK;
  _config._print_ts_track = LWROCMON_PRINT_TS_TRACK_BOTH;
  _config._print_dams = 1;

  memset(&_lwrocmon_rate_state, 0, sizeof (_lwrocmon_rate_state));

  lwrocmon_init_conn_list();

  for (i = 1; i < argc; i++)
    {
      char *request = argv[i];
      char *post;

      if (LWROC_MATCH_C_ARG("--help")) {
	lwrocmon_usage(argv[0]);
	exit(0);
      } else if (LWROC_MATCH_C_ARG("--log")) {
	_config._log_filename = "lwlog.l";
      } else if (LWROC_MATCH_C_PREFIX("--log=",post)) {
	_config._log_filename = post;
      } else if (LWROC_MATCH_C_ARG("--detail")) {
#if HAS_CURSES
	_config._show_detail = 1;
#else
	LWROC_BADCFG("Curses support not compiled in, "
		     "--detail not available.");
#endif
      } else if (LWROC_MATCH_C_ARG("--tree")) {
#if HAS_CURSES
	_config._show_tree = 1;
#else
	LWROC_BADCFG("Curses support not compiled in, "
		     "--tree not available.");
#endif
      } else if (LWROC_MATCH_C_ARG("--no-lookup")) {
	_config._no_name_lookup = 1;
      } else if (LWROC_MATCH_C_ARG("--no-up")) {
	_config._no_down_up |= LWROCMON_CONN_ADDED_SEARCH_UP;
      } else if (LWROC_MATCH_C_ARG("--no-down")) {
	_config._no_down_up |= LWROCMON_CONN_ADDED_SEARCH_DOWN;
      } else if (LWROC_MATCH_C_ARG("--no-down-up")) {
	_config._no_down_up |= LWROCMON_CONN_ADDED_SEARCH_DOWN_UP;
      } else if (LWROC_MATCH_C_ARG("--no-ts-track")) {
	_config._print_ts_track = 0;
      } else if (LWROC_MATCH_C_ARG("--no-ts-analysis")) {
	_config._print_ts_analysis = 0;
      } else if (LWROC_MATCH_C_ARG("--no-dams")) {
	_config._print_dams = 0;
      } else if (LWROC_MATCH_C_ARG("--raw-ts")) {
	_config._print_raw_ts = LWROCMON_PRINT_RAW_TS_DECIMAL;
      } else if (LWROC_MATCH_C_PREFIX("--raw-ts=",post)) {
	if      (strcmp(post,"dec") == 0)
	  _config._print_raw_ts = LWROCMON_PRINT_RAW_TS_DECIMAL;
	else if (strcmp(post,"hex") == 0)
	  _config._print_raw_ts = LWROCMON_PRINT_RAW_TS_HEX;
	else
	  LWROC_BADCFG("Bad option for --raw-ts.");
      } else if (LWROC_MATCH_C_ARG("--buf-size")) {
	_config._print_buf_size = 1;
      } else if (LWROC_MATCH_C_ARG/*NODOC*/("--debug-ts")) {
	/* This option is for debugging lwrocmon itself, so not documented. */
	_config._print_debug_ts = 1;
      } else if (LWROC_MATCH_C_ARG("--monbuf")) {
	_config._print_mon_buffers = 1;
      } else if (LWROC_MATCH_C_ARG("--rate")) {
	_config._print_rate = 1;
      } else if (LWROC_MATCH_C_ARG("--influxdb")) {
	_config._influxdb = 1;
      } else if (LWROC_MATCH_C_PREFIX("--rate=",post)) {
	_config._print_rate = lwroc_parse_time(post, "rate");
      } else if (LWROC_MATCH_C_PREFIX("--count=",post)) {
	_config._num_print = (int) lwroc_parse_hex_u32(post, "update count");
      } else if (LWROC_MATCH_C_PREFIX("--expire-time=",post)) {
	_config._expire_time = lwroc_parse_time(post, "expire time");
      } else if (LWROC_MATCH_C_ARG("--nz-rate-success")) {
	_config._nz_event_incr = 1;
      } else if (LWROC_MATCH_C_ARG("--dt-trace")) {
	_config._dump_dt_trace = 1;
      } else {
	/*CMDLINEARG:HOST*/
	/* Hostname. */
	lwrocmon_add_mon_host(argv[i], NULL,
			      LWROCMON_CONN_ADDED_PRIMARY);
      }
    }

  if (PD_LL_IS_EMPTY(&_lwrocmon_conns))
    LWROC_BADCFG("No drasi instance HOST[:[+]PORT] given.");

  {
    /* We create the leap-seconds filename from the executable
     * path.  Then not depending on the startup script.
     */

#define LEAP_SECONDS_LIST "../download/leap-seconds.list"

    char *leapfile;
    char *slash;

    leapfile = (char *) malloc (strlen(argv[0])+strlen(LEAP_SECONDS_LIST)+1);

    strcpy(leapfile, argv[0]);
    slash = strrchr(leapfile, '/');

    if (slash)
      slash++;
    else
      slash = leapfile;

    strcpy(slash, LEAP_SECONDS_LIST);

    lwroc_read_leap_seconds(leapfile);

    free(leapfile);
  }

  if (_config._log_filename)
    {
      _fid_messages = fopen(_config._log_filename,"a");

      if (_fid_messages == NULL)
	{
	  LWROC_ERROR_FMT("Failed to open output log file '%s'.",
			  _config._log_filename);
	  exit(1);
	}

      lwroc_print_msg_greeting(1);
    }

  if (!!_config._show_detail +
      !!_config._show_tree +
      !!_config._print_rate > 1)
    LWROC_BADCFG("--tree, --rate, and --detail "
		 "are mutually exclusive");

  if (_config._influxdb &&
      !_config._print_rate)
    LWROC_BADCFG("--influxdb requires --rate");

  if (_config._dump_dt_trace &&
      !_config._show_tree)
    LWROC_BADCFG("--dt-trace requires --tree");

  if (!_config._show_detail &&
      !_config._show_tree &&
      !_config._print_rate &&
      !_config._print_mon_buffers)
    _config._print_mon_buffers = 1; /* Perhaps rather rate as default? */

  if (_config._print_rate ||
      _config._show_detail ||
      _config._show_tree ||
      _config._log_filename)
    {
      /* Schedule an update, soon!
       * But give us a chance to establish the connections first.
       */
      gettimeofday(&_lwrocmon_next_time_print, NULL);
      _lwrocmon_next_time_print.tv_sec += 1;
      _lwrocmon_next_time_print.tv_usec = 100000;
    }

  /* TODO: where available, use sigaction with SA_RESTART ? */

  signal(SIGINT, lwrocmon_sighandler);
  signal(SIGTERM,lwrocmon_sighandler);

  if (_config._show_detail ||
      _config._show_tree)
    {
      lwrocmon_detail_tree_init();
    }

  _lwrocmon_num_print = _config._num_print;

  lwrocmon_setup_udp_hint();

  lwrocmon_loop();

  if (_fid_messages)
    lwroc_print_msg_greeting(0);

  if (_fid_messages)
    fclose (_fid_messages);
  _fid_messages = NULL;

  return 0;
}

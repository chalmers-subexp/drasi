/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#ifndef __LWROCMON_STRUCT_H__
#define __LWROCMON_STRUCT_H__

#include "lwroc_net_proto.h"
#include "lwroc_serializer_util.h"
#include "lwroc_select_util.h"
#include "lwroc_timeout_util.h"

#include "../dtc_arch/acc_def/socket_types.h"

/********************************************************************/

#define LWROCMON_PRINT_RAW_TS_DECIMAL  1
#define LWROCMON_PRINT_RAW_TS_HEX      2
#define LWROCMON_PRINT_RAW_TS_HUMAN    3
#define LWROCMON_PRINT_RAW_TS_NUM      4

#define LWROCMON_PRINT_TS_TRACK_RO_TRACK  1  /* Readout TS tracking. */
#define LWROCMON_PRINT_TS_TRACK_TO_INPUT  2  /* Time-sorter input.   */
#define LWROCMON_PRINT_TS_TRACK_BOTH      3  /* Both.                */
#define LWROCMON_PRINT_TS_TRACK_NUM       4

#define LWROCMON_TS_ANALYSIS_SYNC_CHECK  1
#define LWROCMON_TS_ANALYSIS_MATCH_ALL   2
#define LWROCMON_TS_ANALYSIS_NUM         3

/********************************************************************/

typedef struct lwrocmon_config_t
{
  int         _log;
  const char *_log_filename;

  int         _show_detail;
  int         _show_tree;

  int         _print_ts_analysis;
  int         _print_ts_track;
  int         _print_raw_ts;
  int         _print_debug_ts;
  int         _print_mon_buffers;
  int         _print_rate;
  int         _print_buf_size;
  int         _print_dams;
  int         _num_print;
  int         _nz_event_incr;
  int         _expire_time;
  int         _no_name_lookup;
  int         _no_down_up;

  int         _influxdb;
  int         _dump_dt_trace;

  lwroc_print_diff_config _pd_config;

} lwrocmon_config;

extern lwrocmon_config _config;

/********************************************************************/

typedef struct lwrocmon_buffer_t
{
  size_t  _size;   /* total size to read / write*/
  size_t  _offset; /* current offset */
  size_t  _alloc;  /* allocated size */

  char *_buf;

} lwrocmon_buffer;

/********************************************************************/

#define LWROCMON_RATE_PRINT_STATUS           0x0001
#define LWROCMON_RATE_PRINT_EVENTS           0x0002
#define LWROCMON_RATE_PRINT_COLL             0x0004
#define LWROCMON_RATE_PRINT_FILTER_EVENTS    0x0008
#define LWROCMON_RATE_PRINT_FILTER_COLL      0x0010
#define LWROCMON_RATE_PRINT_BUF              0x0020
#define LWROCMON_RATE_PRINT_SENT             0x0040
#define LWROCMON_RATE_PRINT_WRITTEN          0x0080
#define LWROCMON_RATE_PRINT_WR_SINCE         0x0100
#define LWROCMON_RATE_PRINT_WR_SIZE          0x0200
#define LWROCMON_RATE_PRINT_WR_NAME          0x0400
#define LWROCMON_RATE_PRINT_CPU_FRAC         0x0800
#define LWROCMON_RATE_PRINT_CPU_FRAC_FILTER  0x1000

typedef struct lwrocmon_rate_info_t
{
  int flags;

} lwrocmon_rate_info;

/********************************************************************/

#define LWROCMON_CONN_STATE_PORTMAP_SETUP             1
#define LWROCMON_CONN_STATE_PORTMAP_WAIT_CONNECTED    2
#define LWROCMON_CONN_STATE_PORTMAP_READ_PREPARE      3
#define LWROCMON_CONN_STATE_PORTMAP_READ              4
#define LWROCMON_CONN_STATE_PORTMAP_READ_CHECK        5

#define LWROCMON_CONN_STATE_DATA_SETUP                6
#define LWROCMON_CONN_STATE_DATA_WAIT_CONNECTED       7
#define LWROCMON_CONN_STATE_DATA_WRITE_PREPARE        8
#define LWROCMON_CONN_STATE_DATA_WRITE                9
#define LWROCMON_CONN_STATE_DATA_FIRST_READ_PREPARE  10
#define LWROCMON_CONN_STATE_DATA_FIRST_READ          11
#define LWROCMON_CONN_STATE_DATA_FIRST_READ_CHECK    12

#define LWROCMON_CONN_STATE_DATA_CHUNK_READ_PREPARE  13
#define LWROCMON_CONN_STATE_DATA_CHUNK_READ          14
#define LWROCMON_CONN_STATE_DATA_CHUNK_READ_CHECK    15

#define LWROCMON_CONN_STATE_DATA_ACK_WRITE_PREPARE   16
#define LWROCMON_CONN_STATE_DATA_ACK_WRITE           17

#define LWROCMON_CONN_STATE_DATA_DELAY_MSG_PRINT     18

#define LWROCMON_CONN_STATE_FAILED_SLEEP             19
#define LWROCMON_CONN_STATE_SHUTDOWN                 20

/********************************************************************/

#define LWROCMON_CONN_PROGRESS_PMAP_CONNECTED         1
#define LWROCMON_CONN_PROGRESS_PMAP_DONE              2
#define LWROCMON_CONN_PROGRESS_DATA_CONNECTED         3
#define LWROCMON_CONN_PROGRESS_DATA_FIRST_OK          4
#define LWROCMON_CONN_PROGRESS_DATA_CHUNK_OK          5

/********************************************************************/

#define LWROCMON_CONN_ADDED_PRIMARY         0x0001
#define LWROCMON_CONN_ADDED_SEARCH_UP       0x0002 /* direct up */
#define LWROCMON_CONN_ADDED_SEARCH_UP_DOWN  0x0004 /* not direct, first up */
#define LWROCMON_CONN_ADDED_SEARCH_DOWN     0x0008 /* direct down */
#define LWROCMON_CONN_ADDED_SEARCH_DOWN_UP  0x0010 /* not direct, first down */

/********************************************************************/

struct lwrocmsg_src_info_t;
struct lwrocmon_src_info_t;

typedef struct lwrocmon_conn_t
{
  int _state;

  int _fd;

  lwrocmon_buffer _buf;

  struct timeval _next_time;

  uint32_t _last_failure;

  lwroc_timeout_state _retry_timeout;

  struct sockaddr_storage _serv_addr;
  struct sockaddr_storage _data_addr;

  char _hostname[256];
  char _port_str[16];

  lwroc_select_item _select_item;

  pd_ll_item _conns;

  struct lwrocmsg_src_info_t *_msg_sources;

  struct lwrocmon_src_info_t *_mon_sources;

  lwroc_monitor_main_source *_mon_main_source;

  lwroc_monitor_dt_trace _dt_trace;

  lwroc_message_item _msg;

  int      _good_data;
  int      _prev_data;

  struct timeval _stale_expire;

  int      _added_why;

  lwrocmon_rate_info _rate_info;

  /* TODO: this should be in lwroc_mon_tree_item, but since that
   * is reallocated each display round, it has no memory...
   */
  double _msg_rate_ignore_quota;

  /* Who is the master of a slave (updated from by tree view). */
  struct lwrocmon_conn_t *_hint_master;

} lwrocmon_conn;

extern PD_LL_SENTINEL_ITEM(_lwrocmon_net_clients);

extern PD_LL_SENTINEL_ITEM(_lwrocmon_conns);

typedef struct lwrocmon_udp_hint_t
{
  lwroc_select_item _select_item;

  int _fd;

  uint16_t _port;

} lwrocmon_udp_hint;

/********************************************************************/

typedef struct lwrocmsg_src_info_t
{
  struct lwrocmsg_src_info_t *_next;

  uint32_t _source;
  char    *_host_port_thread;
  char    *_hostname;
  char    *_thread;
  uint16_t _port;
  uint16_t _mainthread;

} lwrocmsg_src_info;

/********************************************************************/

/* We use 3 data blocks to handle failed unpacking (legal), and
 * holding current and one sets of previous data.
 */
#define LWROCMON_DATA_BLOCKS  3

typedef struct lwrocmon_src_info_t
{
  struct lwrocmon_src_info_t *_next;

  uint32_t _source;
  char    *_host_port_thread;
  char    *_hostname;
  char    *_thread;
  uint16_t _port;

  uint32_t _type_magic;
  const char *_type_descr;
  void    *_source_info;
  size_t   _data_size;
  void    *_data[LWROCMON_DATA_BLOCKS];
  void    *_format_diff_info;

  lwroc_monitor_conn_source *_conn_source_info;
  lwroc_monitor_conn_block  *_conn_data[LWROCMON_DATA_BLOCKS];

  lwroc_monitor_filewr_block *_filewr_data[LWROCMON_DATA_BLOCKS];

  lwroc_monitor_ana_ts_block *_ana_ts_data[LWROCMON_DATA_BLOCKS];

  lwroc_monitor_dam_block    *_dam_data[LWROCMON_DATA_BLOCKS];

} lwrocmon_src_info;

/********************************************************************/

extern uint32_t _lwrocmon_hint_ident[2];

/********************************************************************/

#endif/*__LWROCMON_STRUCT_H__*/

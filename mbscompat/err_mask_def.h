#ifndef __ERR_MASK_DEF_H__
#define __ERR_MASK_DEF_H__

#define MASK__PRTTERM   0x80000000
#define MASK__PRTSLOG   0x20000000
#define MASK__PRTT      (MASK__PRTTERM | MASK__PRTSLOG)

#endif/*__ERR_MASK_DEF_H__*/

#ifndef __ERROR_MAC_H__
#define __ERROR_MAC_H__

#include "f_ut_error.h"

/* We do the macro with a long version where file and line number is kept.
 */

#define F_ERROR(errnum, retstr, str, mask) \
  f_ut_error_fileline(__FILE__, __LINE__, c_modnam, errnum, retstr, str, mask);

#endif/*__ERROR_MAC_H__*/

#ifndef __F_UT_ERROR_H__
#define __F_UT_ERROR_H__

/* Note, steering commands are not supported. */

void f_ut_error(const char *mod,
		int32_t errnum,
		char *retstr,
		const char *str,
		uint32_t mask);

/* For internal use with macro, also keep file and line number. */
void f_ut_error_fileline(const char *file, int line,
			 const char *mod,
			 int32_t errnum,
			 char *retstr,
			 const char *str,
			 uint32_t mask);

#endif/*__F_UT_ERROR_H__*/

#ifndef __F_UT_PRINTM_H__
#define __F_UT_PRINTM_H__

/* Since the original includes this, we also do that. */
#include "typedefs.h"

/* By using a function, we do not get the file and line number :-( */
/* But we declare it here (before the macro), for testing the
 * implementation as a function that we also provide.  It is provided
 * for object files that expect to find a function.
 */

void printm (const char *format, ...)
  __attribute__ ((__format__ (__printf__, 1, 2)));

/* Let's try to map printm to the same message level as LWROC_INFO. */

#include "../lwroc/lwroc_message.h"

#ifdef VARIADIC_MACROS_OLD

#define printm(__VA_ARGS__...)						\
  do { lwroc_message_internal(LWROC_MSGLVL_INFO, NULL,			\
			      __FILE__,__LINE__,__VA_ARGS__); } while (0)

#endif
#ifdef VARIADIC_MACROS_c11

#define printm(...)							\
  do { lwroc_message_internal(LWROC_MSGLVL_INFO, NULL,			\
			      __FILE__,__LINE__,__VA_ARGS__); } while (0)

#endif

#endif/*__F_UT_PRINTM_H__*/

#ifndef __S_VESHE_H__
#define __S_VESHE_H__

#include "../dtc_arch/acc_def/byteorder_include.h"

#include "typedefs.h"

#if BYTE_ORDER == BIG_ENDIAN
typedef struct
{
  INTS4 l_dlen;
  INTS2 i_subtype;
  INTS2 i_type;
  CHARS h_control;
  CHARS h_subcrate;
  INTS2 i_procid;
} s_veshe;
#endif

#if BYTE_ORDER == LITTLE_ENDIAN
typedef struct
{
  INTS4 l_dlen;
  INTS2 i_type;
  INTS2 i_subtype;
  INTS2 i_procid;
  CHARS h_subcrate;
  CHARS h_control;
} s_veshe;
#endif

#endif/*__S_VESHE_H__*/

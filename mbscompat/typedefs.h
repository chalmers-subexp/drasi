#ifndef __TYPEDEFS_H__
#define __TYPEDEFS_H__

#include "../dtc_arch/acc_def/mystdint.h"

typedef       int64_t  INTS8;
typedef      uint64_t  INTU8;
typedef       int32_t  INTS4;
typedef      uint32_t  INTU4;
typedef       int16_t  INTS2;
typedef      uint16_t  INTU2;
typedef        int8_t  INTS1;
typedef       uint8_t  INTU1;

typedef         float  REAL4;
typedef        double  REAL8;

typedef   signed char  CHARS;
typedef unsigned char  CHARU;
typedef          char  CHARX;

#endif/*__TYPEDEFS_H__*/

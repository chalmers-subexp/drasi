#!/usr/bin/perl

# This file is part of DRASI - a data acquisition data pump.
#
# Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301  USA

use strict;
use warnings;

my $meastime = $ARGV[0];
my $timesort = $ARGV[1];
my $srcs = $ARGV[2];
my $ebsize = $ARGV[3];
my $file_cpumhz = $ARGV[4];
my $file_rate = $ARGV[5];

sub readcpuMHz($)
{
    my $filename = shift;

    my @cpuMHz;
    my $cpui = 0;
    my $cpun = 0;

    open(my $fh, '<', $filename)
	or die "Could not open file '$filename' $!";

    while (my $line = <$fh>) {
	if ($line =~ /UPDATE/) {
	    $cpui = 0;
	    $cpun++;
	} elsif ($line =~ /cpu MHz\s*:\s*([0-9\.]*)/) {
	    my $MHz = $1;
	    $cpuMHz[$cpui] += $MHz;
	    #print "$cpui $MHz\n";
	    $cpui++;
	} else {
	    die "Bad cpumhz line: $line";
	}
    }

    close $fh;

    foreach my $cpuMHz (@cpuMHz)
    {
	$cpuMHz /= $cpun;
    }

    return @cpuMHz;
}

sub readrates($)
{
    my $filename = shift;

    my @rates;

    open(my $fh, '<', $filename)
	or die "Could not open file '$filename' $!";

    while (my $line = <$fh>) {
	if ($line =~ /\s*(\d+)\s+(\d+|\-)\s*(\d+\.?\d*|\-)([kM])?\s+(\d+\.?\d*)\%\s+(\d+.?\d+)([kM])?\s+(\d+\.?\d*)\%\s+(\d+\.?\d*)\%\s+(\d+\.?\d*)\%/) {
	    my $events = $2;
	    my $data = $3;
	    my $incpu = $8;
	    my $maincpu = $9;
	    my $servcpu = $10;
	    if ($4 eq "k") { $data *= 1000; }
	    if ($4 eq "M") { $data *= 1000000; }
	    $events /= (1.0 * $meastime);
	    @rates = ($events, $data, $incpu, $maincpu, $servcpu);
	}
    }

    close $fh;

    return @rates;
}

my @cpuMHz = readcpuMHz($file_cpumhz);

#foreach my $cpuMHz (@cpuMHz)
#{
#    print "$cpuMHz\n";
#}

my @rates = readrates($file_rate);

# print join(' ',@rates)."\n";

print sprintf ("%2d %2d %7d %7d %7d  ".
	       "%5.1f %5.1f %5.1f\n",
	       $timesort,$srcs,$ebsize,
	       $rates[0], $rates[1] / 1000000,
	       $rates[2],
	       $rates[3],
	       $rates[4]);

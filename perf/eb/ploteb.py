#!/usr/bin/python

# This file is part of DRASI - a data acquisition data pump.
#
# Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301  USA

import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt('eb_res.txt');

cts = 0
csrcs = 1
cevsz = 2
cevrt = 3
cdtrt = 4
cinpct = 5
cmnpct = 6
csvpct = 7

f, axarr = plt.subplots(2, sharex=True)
#axarr[0].plot(x, y)
#axarr[0].set_title('Sharing X axis')
#axarr[1].scatter(x, y)

for ts in [0, 1]:
    for srcs in [10, 15, 20, 25, 3, 4, 5, 2, 1, ]:

	s1data = data[np.logical_and(data[:,csrcs] == srcs,
				     data[:,cts] == ts)]

	# print srcs

	col = 'r';
	if (srcs > 10):
	    col = 'g';
	elif (srcs > 2):
	    col = 'b';
	elif (srcs > 1):
	    col = 'm';

	p, = axarr[ts].semilogx(s1data[:,cevsz],s1data[:,cdtrt],col);

	if (srcs > 9):
	    p10 = p;
	elif (srcs > 2):
	    p3 = p;
	elif (srcs > 1):
	    p2 = p;
	else:
	    p1 = p;

	#axarr[ts].semilogx(s1data[:,cevsz],s1data[:,cdtrt]*100./s1data[:,cinpct],'r--');
	#axarr[ts].semilogx(s1data[:,cevsz],s1data[:,cdtrt]*100./s1data[:,cmnpct],'g--');
	#axarr[ts].semilogx(s1data[:,cevsz],s1data[:,cdtrt]*100./s1data[:,csvpct],'b--');

plt.sca(axarr[0])
legend1 = plt.legend([p1, p2, p3, p10],
		     ['1 src', '2 srcs', '3,4,5 srcs', '10,15,20,25 srcs'],
		     loc='upper left', fontsize=10);

axarr[0].set_title('Event builder');
axarr[1].set_title('Time sorter');
axarr[1].set_xlabel('Event size [B]');
axarr[0].set_ylabel('Data rate [MB/s]');
axarr[1].set_ylabel('Data rate [MB/s]');

axarr[0].plot([18, 24], [500, 180], 'r');
axarr[0].text(15,600,'6.4 Mev/s /\n13 Mfrag/s',ha='left',va='bottom',rotation=30);

axarr[1].plot([32, 42], [500, 290], 'k');
axarr[1].text(25,600,'6 Mev/s',ha='left',va='bottom',rotation=30);

axarr[1].set_xlim([10, 100000]);

plt.sca(axarr[1])
plt.xticks([10,   100,   1000, 10000, 100000],
	   ['10', '100', '1k', '10k', '100k'])

#fig = plt.figure()

for ts in [0, 1]:
    print "Timesort: %d" % (ts)

    tsdata = data[np.where(data[:,cts] == ts)]

    datahigh = tsdata[np.where(tsdata[:,cevsz] >= 5000)];

    A = np.vstack([datahigh[:,cdtrt]]).T
    b = np.vstack([datahigh[:,csvpct]*3600./100]).T

    x = np.linalg.lstsq(A,b)[0]

    clk_per_byte = x[0]

    print clk_per_byte

    datalow = tsdata[np.where(tsdata[:,cevsz] <= 500)];

    A = np.vstack([datalow[:,cevrt]]).T
    b = np.vstack([datalow[:,csvpct]*3600./100,
		   datalow[:,cdtrt] * clk_per_byte,
		   datalow[:,csvpct]*3600./100 -
		   datalow[:,cdtrt] * clk_per_byte]).T

    # print A
    # print b

    x = np.linalg.lstsq(A,b)[0]

    clk_per_ev = x[0] * 1000000

    print clk_per_ev

    ########################################################################

    s1data = tsdata[np.where(tsdata[:,cevsz] < 200)]
    s1data = tsdata

    A = np.vstack([s1data[:,cevrt],
		   s1data[:,cdtrt]]).T
    b = np.vstack([s1data[:,csvpct]*3600./100]).T

    #print A
    #print b

    x = np.linalg.lstsq(A,b)[0]
    res = np.linalg.lstsq(A,b)[1]

    #print np.matmul(A,x)-b
    #print res

    print "SERV: %.2fclk/ev + %.2fclk/B" % (x[0]*1000000, x[1])

    ########################################################################

    s1data = tsdata[np.where(tsdata[:,cevsz] < 2000)]
    s1data = tsdata

    A = np.vstack([s1data[:,csrcs]*s1data[:,cevrt],
		   s1data[:,cdtrt]+
		   ((s1data[:,cevrt]*(s1data[:,csrcs]-1))*
		    0.000016*(1-s1data[:,cts]))]).T
    b = np.vstack([s1data[:,cinpct]*3600./100]).T

    #print A
    #print b

    x = np.linalg.lstsq(A,b)[0]
    res = np.linalg.lstsq(A,b)[1]

    #print np.matmul(A,x)-b
    #print res

    print "IN:   %.2fclk/frag + %.2fclk/B" % (x[0]*1000000, x[1])

    ########################################################################

    s1data = tsdata[np.where(tsdata[:,cevsz] < 2000)]
    #s1data = tsdata

    A = np.vstack([s1data[:,csrcs]*s1data[:,cevrt],
		   s1data[:,cevrt],
		   s1data[:,cdtrt]]).T
    b = np.vstack([s1data[:,cmnpct]*3600./100]).T

    #print A
    #print b

    x = np.linalg.lstsq(A,b)[0]
    res = np.linalg.lstsq(A,b)[1]

    #print np.matmul(A,x)-b
    #print res

    print "MRG:  %.2fclk/frag + %.2fclk/ev + %.2fclk/B" % \
	(x[0]*1000000, x[1]*1000000, x[2])

########################################################################

plt.savefig('drasi_eb.png', dpi=200)

plt.show();

#!/bin/bash

# This file is part of DRASI - a data acquisition data pump.
#
# Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301  USA

# Note: to be able to disentangle clk cycles per event and data,
# frequency scaling must be disabled on the merger CPU.

# MERGER=192.168.1.3
# READOUT=192.168.1.1

# MERGERSRV=192.168.1.3
# TRANSCLI=192.168.1.2

MERGER=192.168.40.4
READOUT=192.168.40.17

MERGERSRV=192.168.40.4
TRANSCLI=192.168.40.17

#STARTTIME=10
#MEASTIME=100
STARTTIME=3
MEASTIME=30

function cleanup
{
    echo "Killing..."

    ssh $READOUT killall -9 testdaq
    killall -9 lwrocmerge
    ssh $TRANSCLI killall -9 empty

    echo "Kill done!"
}


function starttest
{
    # taskset -c 0-3

    echo "Starting..."

    TIMESORT=$1
    SRCS=$2
    EVSIZE=$3

    DRASI_SRCS=

    MAXRATE=$(((135+EVSIZE)/SRCS))
    if [ $MAXRATE -gt $((1000/SRCS)) ] ; then MAXRATE=$((1000/SRCS)); fi

    for srci in `seq 1 $SRCS`
    do
	WRARGS=
	if [ $TIMESORT -eq 1 ]
	then
	    WRARGS="--wr-stamp=$srci --stamp-incr=1$srci"
	fi

	# READOUT
	# taskset -c $((srci-1)),$((srci+6-1)) \
	ssh $READOUT \
	    taskset -c $(((srci-1)%10)),$(((srci+6-1)%10)) \
	    $PWD/bin/testdaq --port=$((60000+srci)) \
	    --buf=size=100M --log-no-start-wait \
	    $WRARGS \
	    --max-ev-size=110000 --event-size=$EVSIZE --rate=$MAXRATE \
	    --server=drasi,dest=$MERGER:24000 &

	DRASI_SRCS="$DRASI_SRCS --drasi=$READOUT:$((60000+srci))"

	# ssh $READOUT \
	#     "taskset -c $((srci-1)),$((srci+6-1)) \
	#     $HOME/ucesb/file_input/empty_file \
	#     --lmd --event-size=$EVSIZE --subevent-size=$EVSIZE | \
	#     $HOME/ucesb/empty/empty \
	#     --file=- --server=trans:$((24000+srci)),hold 2> /dev/null" &

	# DRASI_SRCS="$DRASI_SRCS --trans=$READOUT:$((24000+srci))"
    done

    # sleep 10

    MODE=event
    if [ $TIMESORT -eq 1 ]
    then
	MODE=wr
    fi

    # MERGER
    $PWD/bin/lwrocmerge --port=24000 \
			--buf=size=1000M --log-no-start-wait \
			$DRASI_SRCS \
			--merge-mode=$MODE --merge-no-validate \
			--max-ev-size=10000000 \
			--server=trans &

    sleep 3
    # TRANSCLI
    ssh $TRANSCLI $HOME/ucesb/empty/empty --trans=$MERGERSRV \
	--event-sizes 2> /dev/null &

    echo "Started!"
}

function measuretest
{
    echo "Measuring..."

    TIMESORT=$1
    SRCS=$2
    EVSIZE=$3

    FILE_CPUMHZ=`tempfile`
    FILE_RATE=`tempfile`

    for i in `seq 1 $((2*MEASTIME+1))` ;
    do echo UPDATE ; cat /proc/cpuinfo | grep MHz ; sleep .5 ; \
	done > $FILE_CPUMHZ &

    bin/lwrocmon localhost:24000 --rate=$MEASTIME --count=2 > $FILE_RATE

    sleep 2

    scripts/analyseeb.pl $MEASTIME $TIMESORT $SRCS $EVSIZE \
			 $FILE_CPUMHZ $FILE_RATE >> \
			 eb_res.txt

    rm $FILE_CPUMHZ
    rm $FILE_RATE
    rm $FILE_TOP

    echo "Measured"
}

for SRCS in 2 3 4 5 # 10 15 20 25 # 50 100 200 500 # 1 2 3 4 5
do
    for TIMESORT in 0 1
    do
	SMALLSIZE="28 52"
	if [ $TIMESORT -eq 1 ]
	then
	    SMALLSIZE=48
	fi
	for EVSIZE in $SMALLSIZE 80 \
				 100 150 200 300 500 800 \
				 1000 1500 2000 3000 5000 8000 \
				 #10000 15000 20000 30000 50000 80000 \
				 #100000 # 200000 500000 1000000
	do
	    cleanup
	    sleep 3
	    starttest $TIMESORT $SRCS $EVSIZE
	    sleep $STARTTIME
	    measuretest $TIMESORT $SRCS $EVSIZE
	done
    done
done

cleanup

#!/usr/bin/python

# This file is part of DRASI - a data acquisition data pump.
#
# Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301  USA

import numpy as np
import matplotlib.pyplot as plt

execfile("perf2.py")

def evdata(x):
    evsz = x[:,[0]];
    rate = 1.e6 / x[:,[1]];
    sz = evsz * rate;
    payload = evsz - 28;
    perword = ((payload - 100) / 900) * (0.006 - 0.2) + 0.2;
    perword[payload < 100] = 0.2;
    perword[payload > 1000] = 0.006;
    addreadout = evsz * perword;
    ratecorr = 1.e6 / (x[:,[1]] + addreadout);
    szcorr = evsz * ratecorr;
    return np.hstack((evsz,rate,sz,szcorr));

drasi_        = evdata(drasi);
drasi_1w_     = evdata(drasi_1w);
drasi_x80_    = evdata(drasi_x80);
drasi_x80_1w_ = evdata(drasi_x80_1w);

drasi_irq_x80_1w_ = evdata(drasi_irq_x80_1w);

drasi_x80_1w_3x20us_ = evdata(drasi_x80_1w_3x20us);

drasi_irq_x80_1w_3x20us_ = evdata(drasi_irq_x80_1w_3x20us);

mbs_nfrag50_    = evdata(mbs_nfrag50);
mbs_nfrag200_   = evdata(mbs_nfrag200);
mbs_nfrag800_   = evdata(mbs_nfrag800);
mbs_nfrag3200_  = evdata(mbs_nfrag3200);
mbs_nfrag12800_ = evdata(mbs_nfrag12800);

mbs_irq_nf50_    = evdata(mbs_irq_nfrag50);
mbs_irq_nf200_   = evdata(mbs_irq_nfrag200);
mbs_irq_nf800_   = evdata(mbs_irq_nfrag800);
mbs_irq_nf3200_  = evdata(mbs_irq_nfrag3200);

mbs_x80_nfrag50_    = evdata(mbs_x80_nfrag50);
mbs_x80_nfrag200_   = evdata(mbs_x80_nfrag200);
mbs_x80_nfrag800_   = evdata(mbs_x80_nfrag800);
mbs_x80_nfrag3200_  = evdata(mbs_x80_nfrag3200);
mbs_x80_nfrag12800_ = evdata(mbs_x80_nfrag12800);

mbs_x80_irq_nf50_    = evdata(mbs_x80_irq_nfrag50);
mbs_x80_irq_nf200_   = evdata(mbs_x80_irq_nfrag200);
mbs_x80_irq_nf800_   = evdata(mbs_x80_irq_nfrag800);
mbs_x80_irq_nf3200_  = evdata(mbs_x80_irq_nfrag3200);

mbs_x80_3x20us_nfrag50_    = evdata(mbs_x80_3x20us_nfrag50);
mbs_x80_3x20us_nfrag200_   = evdata(mbs_x80_3x20us_nfrag200);
mbs_x80_3x20us_nfrag800_   = evdata(mbs_x80_3x20us_nfrag800);
mbs_x80_3x20us_nfrag3200_  = evdata(mbs_x80_3x20us_nfrag3200);
#mbs_x80_3x20us_nfrag12800_ = evdata(mbs_x80_3x20us_nfrag12800);

mbs_x80_irq_3x20us_nf50_    = evdata(mbs_x80_irq_3x20us_nfrag50);
mbs_x80_irq_3x20us_nf200_   = evdata(mbs_x80_irq_3x20us_nfrag200);
mbs_x80_irq_3x20us_nf800_   = evdata(mbs_x80_irq_3x20us_nfrag800);
mbs_x80_irq_3x20us_nf3200_  = evdata(mbs_x80_irq_3x20us_nfrag3200);

mbs_2eb_nfrag50_    = evdata(mbs_2eb_nfrag50);
mbs_2eb_nfrag200_   = evdata(mbs_2eb_nfrag200);
mbs_2eb_nfrag800_   = evdata(mbs_2eb_nfrag800);
mbs_2eb_nfrag3200_  = evdata(mbs_2eb_nfrag3200);
mbs_2eb_nfrag12800_ = evdata(mbs_2eb_nfrag12800);

mbs_irq_2eb_nf50_    = evdata(mbs_irq_2eb_nfrag50);
mbs_irq_2eb_nf200_   = evdata(mbs_irq_2eb_nfrag200);
mbs_irq_2eb_nf800_   = evdata(mbs_irq_2eb_nfrag800);
mbs_irq_2eb_nf3200_  = evdata(mbs_irq_2eb_nfrag3200);

def padmaximum(x1,x2):
    l1 = len(x1);
    l2 = len(x2);
    y1 = x1;
    y2 = x2;
    if (l2 > l1):
	y1 = np.pad(x1,(0,l2-l1),'constant');
    if (l1 > l2):
	y2 = np.pad(x2,(0,l1-l2),'constant');
    # print l1,l2,len(y1),len(y2)
    return np.maximum(y1,y2);



def plotlines(col, drawall):

    mbs_max =                     mbs_nfrag50_[:,col];
    mbs_max = padmaximum(mbs_max, mbs_nfrag200_[:,col]);
    mbs_max = padmaximum(mbs_max, mbs_nfrag800_[:,col]);
    mbs_max = padmaximum(mbs_max, mbs_nfrag3200_[:,col]);
    mbs_max = padmaximum(mbs_max, mbs_nfrag12800_[:,col]);

    mbs_irq_max =                         mbs_irq_nf50_[:,col];
    mbs_irq_max = padmaximum(mbs_irq_max, mbs_irq_nf200_[:,col]);
    mbs_irq_max = padmaximum(mbs_irq_max, mbs_irq_nf800_[:,col]);
    mbs_irq_max = padmaximum(mbs_irq_max, mbs_irq_nf3200_[:,col]);

    mbs_x80_max =                         mbs_x80_nfrag50_[:,col];
    mbs_x80_max = padmaximum(mbs_x80_max, mbs_x80_nfrag200_[:,col]);
    mbs_x80_max = padmaximum(mbs_x80_max, mbs_x80_nfrag800_[:,col]);
    mbs_x80_max = padmaximum(mbs_x80_max, mbs_x80_nfrag3200_[:,col]);
    mbs_x80_max = padmaximum(mbs_x80_max, mbs_x80_nfrag12800_[:,col]);

    mbs_x80_irq_max =                             mbs_x80_irq_nf50_[:,col];
    mbs_x80_irq_max = padmaximum(mbs_x80_irq_max, mbs_x80_irq_nf200_[:,col]);
    mbs_x80_irq_max = padmaximum(mbs_x80_irq_max, mbs_x80_irq_nf800_[:,col]);
    mbs_x80_irq_max = padmaximum(mbs_x80_irq_max, mbs_x80_irq_nf3200_[:,col]);

    mbs_x80_3x20us_max =                                mbs_x80_3x20us_nfrag50_[:,col];
    mbs_x80_3x20us_max = padmaximum(mbs_x80_3x20us_max, mbs_x80_3x20us_nfrag200_[:,col]);
    mbs_x80_3x20us_max = padmaximum(mbs_x80_3x20us_max, mbs_x80_3x20us_nfrag800_[:,col]);
    mbs_x80_3x20us_max = padmaximum(mbs_x80_3x20us_max, mbs_x80_3x20us_nfrag3200_[:,col]);
    #mbs_x80_3x20us_max = padmaximum(mbs_x80_3x20us_max, mbs_x80_3x20us_nfrag12800_[:,col]);

    mbs_x80_irq_3x20us_max =                                    mbs_x80_irq_3x20us_nf50_[:,col];
    mbs_x80_irq_3x20us_max = padmaximum(mbs_x80_irq_3x20us_max, mbs_x80_irq_3x20us_nf200_[:,col]);
    mbs_x80_irq_3x20us_max = padmaximum(mbs_x80_irq_3x20us_max, mbs_x80_irq_3x20us_nf800_[:,col]);
    mbs_x80_irq_3x20us_max = padmaximum(mbs_x80_irq_3x20us_max, mbs_x80_irq_3x20us_nf3200_[:,col]);

    mbs_2eb_max =                         mbs_2eb_nfrag50_[:,col];
    mbs_2eb_max = padmaximum(mbs_2eb_max, mbs_2eb_nfrag200_[:,col]);
    mbs_2eb_max = padmaximum(mbs_2eb_max, mbs_2eb_nfrag800_[:,col]);
    mbs_2eb_max = padmaximum(mbs_2eb_max, mbs_2eb_nfrag3200_[:,col]);
    mbs_2eb_max = padmaximum(mbs_2eb_max, mbs_2eb_nfrag12800_[:,col]);

    mbs_irq_2eb_max =                             mbs_irq_2eb_nf50_[:,col];
    mbs_irq_2eb_max = padmaximum(mbs_irq_2eb_max, mbs_irq_2eb_nf200_[:,col]);
    mbs_irq_2eb_max = padmaximum(mbs_irq_2eb_max, mbs_irq_2eb_nf800_[:,col]);
    mbs_irq_2eb_max = padmaximum(mbs_irq_2eb_max, mbs_irq_2eb_nf3200_[:,col]);

    if (drawall == 1):
	p71, = plt.semilogx(drasi_[:,0],
			    drasi_[:,col]/1.e6,    'g-',
			    linewidth=2);
	p72, = plt.semilogx(drasi_1w_[:,0],
			    drasi_1w_[:,col]/1.e6, 'g--',
			    linewidth=2);

    if (drawall == 0 or drawall == 1):
	p01, = plt.semilogx(drasi_x80_[:,0],
			    drasi_x80_[:,col]/1.e6,    'b-',
			    linewidth=2);
	p02, = plt.semilogx(drasi_x80_1w_[:,0],
			    drasi_x80_1w_[:,col]/1.e6, 'b--',
			    linewidth=2);

	p92, = plt.semilogx(drasi_irq_x80_1w_[:,0],
			    drasi_irq_x80_1w_[:,col]/1.e6, 'c--',
			    linewidth=2);

    if (drawall == 1):
	p11, = plt.semilogx(mbs_nfrag50_[:,0],
			    mbs_nfrag50_[:,col]/1.e6,    'r:x');
	p12, = plt.semilogx(mbs_nfrag200_[:,0],
			    mbs_nfrag200_[:,col]/1.e6,   'r:+');
	p13, = plt.semilogx(mbs_nfrag800_[:,0],
			    mbs_nfrag800_[:,col]/1.e6,   'r:1');
	p14, = plt.semilogx(mbs_nfrag3200_[:,0],
			    mbs_nfrag3200_[:,col]/1.e6,  'r:3');
	p15, = plt.semilogx(mbs_nfrag12800_[:,0],
			    mbs_nfrag12800_[:,col]/1.e6, 'r:|');
	p19, = plt.semilogx(mbs_nfrag50_[:,0],
			    mbs_max/1.e6, 'r--', linewidth=2);

	p21, = plt.semilogx(mbs_irq_nf50_[:,0],
			    mbs_irq_nf50_[:,col]/1.e6,    'm:x');
	p22, = plt.semilogx(mbs_irq_nf200_[:,0],
			    mbs_irq_nf200_[:,col]/1.e6,   'm:+');
	p23, = plt.semilogx(mbs_irq_nf800_[:,0],
			    mbs_irq_nf800_[:,col]/1.e6,   'm:1');
	p24, = plt.semilogx(mbs_irq_nf3200_[:,0],
			    mbs_irq_nf3200_[:,col]/1.e6,  'm:3');
	p29, = plt.semilogx(mbs_irq_nf50_[:,0],
			    mbs_irq_max/1.e6,  'm--', linewidth=2);

    if (drawall == 0 or drawall == 1):
	p51, = plt.semilogx(mbs_x80_nfrag50_[:,0],
			    mbs_x80_nfrag50_[:,col]/1.e6,    'r:x');
	p52, = plt.semilogx(mbs_x80_nfrag200_[:,0],
			    mbs_x80_nfrag200_[:,col]/1.e6,   'r:+');
	p53, = plt.semilogx(mbs_x80_nfrag800_[:,0],
			    mbs_x80_nfrag800_[:,col]/1.e6,   'r:1');
	p54, = plt.semilogx(mbs_x80_nfrag3200_[:,0],
			    mbs_x80_nfrag3200_[:,col]/1.e6,  'r:3');
	p55, = plt.semilogx(mbs_x80_nfrag12800_[:,0],
			    mbs_x80_nfrag12800_[:,col]/1.e6, 'r:|');
	p59, = plt.semilogx(mbs_x80_nfrag50_[:,0],
			    mbs_x80_max/1.e6, 'r-', linewidth=2);

	p61, = plt.semilogx(mbs_x80_irq_nf50_[:,0],
			    mbs_x80_irq_nf50_[:,col]/1.e6,    'm:x');
	p62, = plt.semilogx(mbs_x80_irq_nf200_[:,0],
			    mbs_x80_irq_nf200_[:,col]/1.e6,   'm:+');
	p63, = plt.semilogx(mbs_x80_irq_nf800_[:,0],
			    mbs_x80_irq_nf800_[:,col]/1.e6,   'm:1');
	p64, = plt.semilogx(mbs_x80_irq_nf3200_[:,0],
			    mbs_x80_irq_nf3200_[:,col]/1.e6,  'm:3');
	p69, = plt.semilogx(mbs_x80_irq_nf50_[:,0],
			    mbs_x80_irq_max/1.e6,  'm-', linewidth=2);

    if (drawall == 2):
	p51, = plt.semilogx(mbs_x80_3x20us_nfrag50_[:,0],
			    mbs_x80_3x20us_nfrag50_[:,col]/1.e6,    'r:x');
	p52, = plt.semilogx(mbs_x80_3x20us_nfrag200_[:,0],
			    mbs_x80_3x20us_nfrag200_[:,col]/1.e6,   'r:+');
	p53, = plt.semilogx(mbs_x80_3x20us_nfrag800_[:,0],
			    mbs_x80_3x20us_nfrag800_[:,col]/1.e6,   'r:1');
	p54, = plt.semilogx(mbs_x80_3x20us_nfrag3200_[:,0],
			    mbs_x80_3x20us_nfrag3200_[:,col]/1.e6,  'r:3');
	#p55, = plt.semilogx(mbs_x80_3x20us_nfrag12800_[:,0],
	#                    mbs_x80_3x20us_nfrag12800_[:,col]/1.e6, 'r:|');
	p59, = plt.semilogx(mbs_x80_3x20us_nfrag50_[:,0],
			    mbs_x80_3x20us_max/1.e6, 'r-', linewidth=2);

	p61, = plt.semilogx(mbs_x80_irq_3x20us_nf50_[:,0],
			    mbs_x80_irq_3x20us_nf50_[:,col]/1.e6,    'm:x');
	p62, = plt.semilogx(mbs_x80_irq_3x20us_nf200_[:,0],
			    mbs_x80_irq_3x20us_nf200_[:,col]/1.e6,   'm:+');
	p63, = plt.semilogx(mbs_x80_irq_3x20us_nf800_[:,0],
			    mbs_x80_irq_3x20us_nf800_[:,col]/1.e6,   'm:1');
	p64, = plt.semilogx(mbs_x80_irq_3x20us_nf3200_[:,0],
			    mbs_x80_irq_3x20us_nf3200_[:,col]/1.e6,  'm:3');
	p69, = plt.semilogx(mbs_x80_irq_3x20us_nf50_[:,0],
			    mbs_x80_irq_3x20us_max/1.e6,  'm-', linewidth=2);

    if (drawall == 1):
	p31, = plt.semilogx(mbs_2eb_nfrag50_[:,0],
			    mbs_2eb_nfrag50_[:,col]/1.e6,    'c:x');
	p32, = plt.semilogx(mbs_2eb_nfrag200_[:,0],
			    mbs_2eb_nfrag200_[:,col]/1.e6,   'c:+');
	p33, = plt.semilogx(mbs_2eb_nfrag800_[:,0],
			    mbs_2eb_nfrag800_[:,col]/1.e6,   'c:1');
	p34, = plt.semilogx(mbs_2eb_nfrag3200_[:,0],
			    mbs_2eb_nfrag3200_[:,col]/1.e6,  'c:3');
	p35, = plt.semilogx(mbs_2eb_nfrag12800_[:,0],
			    mbs_2eb_nfrag12800_[:,col]/1.e6, 'c:|');
	p39, = plt.semilogx(mbs_2eb_nfrag50_[:,0],
			    mbs_2eb_max/1.e6, 'c-', linewidth=2);

	p41, = plt.semilogx(mbs_irq_2eb_nf50_[:,0],
			    mbs_irq_2eb_nf50_[:,col]/1.e6,    'y:x');
	p42, = plt.semilogx(mbs_irq_2eb_nf200_[:,0],
			    mbs_irq_2eb_nf200_[:,col]/1.e6,   'y:+');
	p43, = plt.semilogx(mbs_irq_2eb_nf800_[:,0],
			    mbs_irq_2eb_nf800_[:,col]/1.e6,   'y:1');
	p44, = plt.semilogx(mbs_irq_2eb_nf3200_[:,0],
			    mbs_irq_2eb_nf3200_[:,col]/1.e6,  'y:3');
	p49, = plt.semilogx(mbs_irq_2eb_nf50_[:,0],
			    mbs_irq_2eb_max/1.e6,  'y-', linewidth=2);

    if (drawall == 2):
	#p01, = plt.semilogx(drasi_x80_3x20us_[:,0],
	#                    drasi_x80_3x20us_[:,col]/1.e6,    'b-',
	#                    linewidth=2);
	p02, = plt.semilogx(drasi_x80_1w_3x20us_[:,0],
			    drasi_x80_1w_3x20us_[:,col]/1.e6, 'b--',
			    linewidth=2);
	p92, = plt.semilogx(drasi_irq_x80_1w_3x20us_[:,0],
			    drasi_irq_x80_1w_3x20us_[:,col]/1.e6, 'c--',
			    linewidth=2);

    if (drawall == 1):
	legend1 = plt.legend([p01, p02, p92,
			      p71, p72,
			      p19, p29,
			      p59, p69,
			      p39, p49],
			     ['drasi',
			      'drasi 1 word DT release',
			      'drasi irq, 1 word DT rel.',
			      'drasi (x86l-3)',
			      'drasi 1 w DT rel (x86l-3)',
			      'MBS polled (x86l-3)',
			      'MBS irq (x86l-3)',
			      'MBS polled (x86l-80)',
			      'MBS irq (x86l-80)',
			      'MBS polled 2EB',
			      'MBS irq 2EB' ],
			     loc='lower right');

	legend2 = plt.legend([p11, p12, p13, p14, p15],
			     ['N_FRAG=50',
			      'N_FRAG=200',
			      'N_FRAG=800',
			      'N_FRAG=3200',
			      'N_FRAG=12800',],
			     loc='upper left');

	plt.gca().add_artist(legend1);
    elif (drawall == 0):
	legend1 = plt.legend([p01, p02, p92,
			      p59, p69],
			     ['drasi',
			      'drasi 1 word DT release',
			      'drasi irq, 1 word DT rel.',
			      'MBS polled',
			      'MBS irq'],
			     loc='lower right');

	legend2 = plt.legend([p51, p52, p53, p54, p55],
			     ['N_FRAG=50',
			      'N_FRAG=200',
			      'N_FRAG=800',
			      'N_FRAG=3200',
			      'N_FRAG=12800',],
			     loc='center right');

	plt.gca().add_artist(legend1);
    elif (drawall == 2):
	legend1 = plt.legend([p02, p92,
			      p59, p69],
			     ['drasi 1 word DT release',
			      'drasi irq, 1 word DT rel.',
			      'MBS polled',
			      'MBS irq'],
			     loc='lower right');

	legend2 = plt.legend([p51, p52, p53, p54],
			     ['N_FRAG=50',
			      'N_FRAG=200',
			      'N_FRAG=800',
			      'N_FRAG=3200',],
			     loc='center right');

	plt.gca().add_artist(legend1);


    plt.xlabel('Event size [B]');
    plt.ylabel('Data rate [MB/s]');

    #ax = plt.gca()
    #ax.xaxis.set_major_formatter(plt.ScalarFormatter())

    plt.xticks([10,   100,   1000, 10000, 100000],
	       ['10', '100', '1k', '10k', '100k'])

#####################################################################

if (False):
    fig = plt.figure()

    plotlines(2, 1)

    plt.title('0-time data readout, trig: 100 ns');

#####################################################################

if (False):
    fig = plt.figure()

    plotlines(1, 2)

    plt.title('0-time data readout, trig: 0-20 us, avg 6.6 us');

#####################################################################

if (True):
    fig = plt.figure()

    plotlines(2, 0)

    plt.arrow(20, 1, 0, 12, head_width=3, head_length=2, fc='k', ec='k')
    plt.plot([18,22],[28/3.0, 28/3.0],'k');
    plt.text(17,28/3.0,'3',ha='right',va='center');
    plt.plot([18,22],[28/4.0, 28/4.0],'k');
    plt.text(17,28/4.0,'4',ha='right',va='center');
    plt.plot([18,22],[28/9.0, 28/9.0],'k');
    plt.text(17,28/9.0,'9',ha='right',va='center');
    plt.text(18,16,u'Event overhead (\u03bcs)',ha='left',va='bottom',rotation=70);
    plt.xlim([10,1.e6])

    plt.title('0-time data readout, trig period: 100 ns');

    plt.savefig('drasi_vs_mbs_dt0.png', dpi=200)

#####################################################################

if (False):
    fig = plt.figure()

    plotlines(3, 0)

    plt.title('0-time data readout + fake, trig: 100 ns');

#####################################################################

if (True):
    fig = plt.figure()

    plotlines(2, 2)

    plt.title('0-time data readout, trig period: 0-20 us, avg 6.6 us');

    plt.savefig('drasi_vs_mbs_dt6us.png', dpi=200)

#####################################################################

plt.show();

'''
bin/f_user_speed --log-no-start-wait --trimi=master \
  --eb=x86l-80 --server=drasi,dest=x86l-80 --subev=subcrate=1,control=0 \
  --buf=size=0x04000000 --max-ev-size=950000

bin/f_user_speed ... trimi=master,multi-word-trig-clear

bin/lwrocmerge --eb-master=r4l-11 --drasi=r4l-11 --server=trans \
  --merge-mode=event --log-no-start-wait --buf=size=0x10000000 \
  --max-ev-size=950000



bin_ppc-linux_4.2.2/tridi_ctrl --addr=2 \
  "DEADTIME_IN(1)=TRIMI_TDT" \
  "TRIG_PENDING[1]=PULSER(1)" "period(1)=100ns"

bin_ppc-linux_4.2.2/tridi_ctrl --addr=2 \
  "DEADTIME_IN(1)=TRIMI_TDT" \
  "TRIG_PENDING[1]=PULSER(1)" "period(1)=20000ns" \
  "TRIG_PENDING[2]=PULSER(2)" "period(2)=19990ns" \
  "TRIG_PENDING[3]=PULSER(3)" "period(3)=20030ns"

htj/trloii/trloctrl/fw_f248ff45_tridi

bin_ppc-linux_4.2.2/tridi_ctrl --addr=2 --print-config
===============================================================================
TRIG_PENDING[1]          = PULSER(1)
DEADTIME_IN(1)           = TRIMI_TDT
-------------------------------------------------------------------------------
period(1)                = 100 ns
===============================================================================

../../trimictrl/bin_ppc-linux_4.2.2/trimictrl --print-config

===============================================================================
serial_in                = CTRL_IN(1)
encoded_in               = TRLO_ENCODED_TRIG
-------------------------------------------------------------------------------
link_period              = 5
fast_dt                  = 10
===============================================================================




'''

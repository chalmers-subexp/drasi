#!/usr/bin/perl

use strict;
use warnings;

my $lines = 0;
my $usage_funcs = 0;

my $usage_func = ();
my $prev_usage = ();

my $prevargprefix = ();

my %usageopts = ();
my %usageprefixes = ();
my %usageargs = ();

my %usagehashelp = ();
my $verify_usage_call = ();

my %subusage = ();
my %issubusage = ();

my $anyissue = 0;

sub splitmulti($);

while (my $line = <>)
{
    chomp $line;

    # print "$line\n";

    # Is it a start of a ..._usage() function?
    if ($line =~ /void\s+([_a-zA-z][_a-zA-z0-9]*_usage)\s*\(.*\)(;)?/)
    {
	my $func = $1;
	my $semicolon = $2;

	if (!$semicolon)
	{
	    print "$line\n";

	    if ($usage_func) {
		die "Found $func, but still in $usage_func.";
	    }

	    if ($usageopts{$func})
	    {
		print STDERR "*** Found $func, again!\n";
	    }
	    else
	    {
		$usageopts{$func} = [];
		$usageprefixes{$func} = [];
		$usageargs{$func} = [];
	    }

	    $usage_func = $func;
	    $usage_funcs++;
	}
    }
    # Is it the end of a ..._usage() function?
    if ($line =~ /^}/)
    {
	if ($usage_func)
	{
	    $prev_usage = $usage_func;
	    $usage_func = ();
	}
    }
    if ($usage_func)
    {
	# Inside ..._usage() function - is it an option description?
	if ($line =~ /printf\s\(\"(\s{2,15})([^\s\"]+)(\s|\\n)/)
	{
	    my $spaces = $1;
	    my $opt = $2;

	    my $uopts = $usageopts{$usage_func};

	    if ($opt =~ /^(--)?help$/)
	    {
		# The arg help is eaten, and handled specially.
		# So we must eat it here too,
		# to not warn about missing calls.
	    }
	    elsif ($opt =~ /^--gdb$/)
	    {
		# This opt is handled by the external script,
		# so should have no C handling code.
	    }
	    else
	    {
		if ($spaces =~ /    /)
		{
		    my $prevopt = pop @$uopts;
		    $opt = $prevopt.$opt;
		}
		my @opts = splitmulti($opt);
		push @$uopts, @opts;
	    }

	    # print "opt: >$opt<<$spaces>\n";
	}
    }
    else
    {
	# Not inside ..._usage() function - look for option handlers.

	# Prefix option, i.e. with argument, typically 'xxx='.
	if ($line =~ /LWROC_MATCH_C_PREFIX(.*)?\(\"(.*)\"/)
	{
	    my $nodoc = $1;
	    my $prefix = $2;

	    # print "prefix: $prefix ($prev_usage)\n";

	    if (!($nodoc =~ /\/\*\s*NODOC\s*\*\//))
	    {
		my $uprefixes = $usageprefixes{$prev_usage};
		push @$uprefixes, $prefix;

		$prevargprefix = $prefix;
	    }
	}
	# Non-prefix option, i.e. without argument, typically 'xxx'.
	if ($line =~ /LWROC_MATCH_C_ARG(.*)?\(\"(.*)\"/)
	{
	    my $nodoc = $1;
	    my $arg = $2;

	    # print "arg: $arg\n";

	    if (!($nodoc =~ /\/\*\s*NODOC\s*\*\//))
	    {
		if ($arg =~ /^(--)?help$/)
		{
		    $usagehashelp{$prev_usage} = 1;
		    $verify_usage_call = $prev_usage;
		}
		else
		{
		    my $uargs = $usageargs{$prev_usage};
		    push @$uargs, $arg;

		    $prevargprefix = $arg;
		}
	    }
	}
	# Directive that help handler is elsewhere.
	if ($line =~ /\/\*\s*NOHELPCALL\s*\*\//)
	{
	    $usagehashelp{$prev_usage} = 2;
	}
	# Directive for 'bare' option, i.e. argument only without name. */
	if ($line =~ /\/\*\s*CMDLINEARG:(.*)\s*\*\//)
	{
	    my $arg = $1;

	    my $uargs = $usageargs{$prev_usage};
	    push @$uargs, $arg;
	}
	# Directive telling which ..._usage() function contain the
	# descriptions of the sub-argument options of this handled
	# option.
	if ($line =~ /\/\*\s*SUBUSAGE:\s*(.*)\s*\*\//)
	{
	    my $subusage = $1;

	    $subusage{$prev_usage."#".$prevargprefix} = $subusage;

	    $issubusage{$subusage} = 1;
	}
    }
    $lines++;
}

sub splitmulti($)
{
    my $opt = shift;

    my $preeq = $opt;
    my $posteq = "";
    if ($preeq =~ s/=(.*)/=/)
    {
	$posteq = $1;
    }
    # $preeq =~ s/\|.*//;

    if ($preeq =~ /([^=]*)\[([^=]*)\](.*)/)
    {
	my $pre = $1;
	my $mid = $2;
	my $post = $3;

	if ($mid =~ /\|/)
	{
	    # print ">> $opt >>>> $pre <<>> $mid <<>> $post <<>> $posteq <<\n";

	    my @opts = ();

	    foreach my $m (split /\|/,$mid)
	    {
		push @opts, splitmulti($pre.$m.$post.$posteq);
	    }
	    return @opts;
	}
    }

    return ($opt);
}

# Every option which is documented should also exist

sub multimatch($$);

sub multimatch($$)
{
    my $opt = shift;
    my $match = shift;

    my $preeq = $opt;
    $preeq =~ s/=.*/=/;
    # $preeq =~ s/\|.*//;

    if ($preeq =~ /([^=]*)\[([^=]*)\](.*)/)
    {
	my $pre = $1;
	my $mid = $2;
	my $post = $3;

	if ($mid =~ /\|/)
	{
	    # print ">> $opt >>>> $pre <<>> $mid <<>> $post <<  $match\n";

	    foreach my $m (split /\|/,$mid)
	    {
		if (multimatch($pre.$m.$post, $match)) {
		    return 1;
		}
	    }
	}
    }

    # printf ("MATCH ???  $opt  $match\n");

    if ($opt eq $match)
    {
	# printf ("MATCH:::  $opt  $match\n");
	return 1;
    }
    return 0;
}

sub assignsubusage($$$)
{
    my $usage = shift;
    my $opt = shift;
    my $arg = shift;

    if (!$usage) { return; }

    my $subusage = $subusage{$usage."#".$arg};

    if ($subusage)
    {
	if ($subusage{$usage."#".$opt} &&
	    $subusage{$usage."#".$opt} ne $subusage) {
	    die "subusage already set for $usage#$opt";
	}

	$subusage{$usage."#".$opt} = $subusage;
	$issubusage{$subusage} = 2;
    }
}

sub match_arg_opt($$$)
{
    my $usage = shift;
    my $opt = shift;
    my $arg = shift;

    # print "MATCH_ARG_OPT  $opt  $arg\n";

    if (multimatch($opt, $arg))
    {
	assignsubusage($usage, $opt, $arg);
	return 1;
    }

    my $cleanopt = $opt;

    $cleanopt =~ s/\[=.*//;
    $cleanopt =~ s/\[:.*//;

    if (multimatch($cleanopt, $arg))
    {
	assignsubusage($usage, $opt, $arg);
	return 1;
    }

    if ($opt =~ /\[(.*)\](.*)/)
    {
	my $opt1 = $1;
	my $opt2 = $2;

	if (multimatch($opt2, $arg) ||
	    multimatch($opt1.$opt2, $arg))
	{
	    assignsubusage($usage, $opt, $arg);
	    return 1;
	}
    }

    return 0;
}

sub clean_opt($)
{
    my $opt = shift;

    my $cleanopt = $opt;

    $cleanopt =~ s/=.*/=/;
    $cleanopt =~ s/@.*/@/;
    $cleanopt =~ s/\[=.*/=/;
    $cleanopt =~ s/\[:.*/:/;

    return $cleanopt;
}

sub match_opt_prefix($$$)
{
    my $usage = shift;
    my $opt = shift;
    my $prefix = shift;

    my $cleanopt = clean_opt($opt);

    # print "$cleanopt\n";

    if (multimatch($cleanopt, $prefix))
    {
	assignsubusage($usage, $opt, $prefix);
	return 1;
    }
    return 0;
}

sub check_duplicates($$)
{
    my $kind = shift;
    my $listref = shift;

    my @list = @$listref;

    for (my $i = 0; $i <= $#list; $i++)
    {
	for (my $j = 0; $j < $i; $j++)
	{
	    if ($list[$i] eq $list[$j])
	    {
		print STDERR "*** duplicate  $kind  $list[$i]\n";
		return 1;
	    }
	}
    }
    return 0;
}


sub check_usage($$)
{
    my $usage = shift;
    my $doprint = shift;

    my $hadissue = 0;

    if ($doprint) { print "\nUsage: $usage\n"; }

    my $uopts = $usageopts{$usage};
    my $uprefixes = $usageprefixes{$usage};
    my $uargs = $usageargs{$usage};

    $hadissue |= check_duplicates("OPT",$uopts);
    $hadissue |= check_duplicates("ARG",$uargs);
    $hadissue |= check_duplicates("PREFIX",$uprefixes);

    if ($doprint)
    {
	print "OPT: ";
	for my $opt (@$uopts)
	{
	    print "$opt  ";
	}
	print "\n";
	print "ARGS: ";
	for my $arg (@$uargs)
	{
	    print "$arg  ";
	}
	print "\n";
	print "PREFIXES: ";
	for my $prefix (@$uprefixes)
	{
	    print "$prefix  ";
	}
	print "\n\n";
    }

    if (!$usagehashelp{$usage})
    {
	if ($doprint) {
	    print STDERR "*** $usage  help arg/call not found.\n";
	}
	$hadissue = 1;
    }

    printf ("CHECK_CODE\n");

    for my $opt (@$uopts)
    {
	printf ("OPT_LOOP  $opt\n");

	for my $arg (@$uargs)
	{
	    if (match_arg_opt($usage, $opt, $arg)) {
		goto found_opt;
	    }
	}
	for my $prefix (@$uprefixes)
	{
	    if (match_opt_prefix($usage, $opt, $prefix)) {
		goto found_opt;
	    }
	}

	if ($doprint) {
	    my $cleanopt = clean_opt($opt);
	    print STDERR "*** $usage  OPT '$opt' -> '$cleanopt' ".
		"no code found.\n";
	}
	$hadissue = 1;

      found_opt:
	;
    }

    printf ("CHECK_ARG\n");

    for my $arg (@$uargs)
    {
	printf ("ARG_LOOP  $arg\n");

	for my $opt (@$uopts)
	{
	    if (match_arg_opt((), $opt, $arg)) {
		goto found_arg;
	    }
	}

	if ($doprint) {
	    print STDERR "*** $usage  ARG '$arg' no doc found.\n";
	}
	$hadissue = 1;

      found_arg:
	;
    }

    printf ("CHECK_PREFIX\n");

    for my $prefix (@$uprefixes)
    {
	printf ("PREFIX_LOOP  $prefix\n");

	for my $opt (@$uopts)
	{
	    if (match_opt_prefix((), $opt, $prefix)) {
		goto found_arg;
	    }
	}

	if ($doprint) {
	    print STDERR "*** $usage  PREFIX '$prefix' no doc found.\n";
	}
	$hadissue = 1;

      found_arg:
	;
    }

    printf ("...\n");

    $anyissue |= $hadissue;

    return $hadissue;
}

foreach my $usage (sort keys %usageopts)
{
    if (check_usage($usage,0))
    {
	check_usage($usage,1);
    }
}

foreach my $subusage (sort keys %issubusage)
{
    if ($issubusage{$subusage} < 2)
    {
	die "subusage $subusage never mapped to opt.";
    }
}

sub printsubusage($$);

sub printsubusage($$)
{
    my $usage = shift;
    my $indent = shift;

    print "$indent$usage\n";

    my $uopts = $usageopts{$usage};

    for my $opt (@$uopts)
    {
	print "$indent  $opt\n";

	my $subusage = $subusage{$usage."#".$opt};

	if ($subusage)
	{
	    printsubusage($subusage,$indent."    ");
	}
    }
}

foreach my $usage (sort keys %usageopts)
{
    if (!$issubusage{$usage})
    {
	printsubusage($usage,"");
    }
}


print "Checked $lines lines, $usage_funcs usage().\n";

if ($anyissue) {
    die "Issues found!";
}

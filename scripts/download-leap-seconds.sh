#!/bin/sh

LEAPFILE=$1

# Note: using http:// does not work, gives a redirect to https:// .
URL=https://hpiers.obspm.fr/iers/bul/bulc/ntp/leap-seconds.list

PERHAPS1=/usr/share/zoneinfo/leap-seconds.list

# Using the perl script seems safer.
NOW=`perl ../scripts/getdate.pl || date +%s`
NOW=`echo $NOW | sed 's/.$//'` # cut last digit, some expr only do 32-bit evals
UNIX_EPOCH=`echo 2208988800 | sed 's/.$//'`
NOW1900=`expr $NOW + $UNIX_EPOCH`

# echo $NOW $UNIX_EPOCH $NOW1900

set -e

ERROREXIT=1

if [ "x$IGNORELEAPSECONDFILE" = "x1" ]
then
    ERROREXIT=0
fi

if [ -f $LEAPFILE ]
then
    # using [[:space:]] instead of \s to avoid sed error in FreeBSD.
    EXPIRE=`grep "\#@" $LEAPFILE | sed -e 's/#@[[:space:]]*//' -e 's/[[:space:]]*$//' -e 's/.$//'`

    # echo Expire $EXPIRE $NOW $NOW1900 $UNIX_EPOCH

    if [ $NOW1900 -lt $EXPIRE ]
    then
	ACCEPTSTAMPOLD=30 # Normally do not reattempt download if 30 days
	# When things seem ok, this can be silenced...
	# Note!!! 'expr' returns 1 of result is 0, so doing '|| true'
	DAYSLEFT=`expr \( $EXPIRE - $NOW1900 \) / 8640 || true` # 86400, last digit cut
	if [ $DAYSLEFT -lt 10 ]
	then
	    echo "   OK    $LEAPFILE ($DAYSLEFT days left)"
	    ACCEPTSTAMPOLD=5
	fi

	# Download success is not mandatory
	DOWNLOADFAILOK=1
	# If we do not have a .stamp file (telling when the last
	# download happened), or that was more than 30 days ago, we
	# reattempt download.
	if [ -f $LEAPFILE.stamp ]
	then
	    STAMP_EPOCH=`perl ../scripts/getdate.pl $LEAPFILE.stamp || date -r +$LEAPFILE.stamp %s`
	    STAMP_EPOCH=`echo $STAMP_EPOCH | sed 's/.$//'` # cut last digit
	    DAYSOLD=`expr \( $NOW - $STAMP_EPOCH \) / 8640 || true`
	    if [ $DAYSOLD -lt $ACCEPTSTAMPOLD ]
	    then
		exit 0;
	    else
		echo "Old $LEAPFILE.stamp (download $DAYSOLD days ago)"
	    fi
	fi
    fi
fi

echo "DOWNLOAD $URL"

# Certificates did not work well with the CI environment

curl -s -S -f --insecure $URL > $LEAPFILE || \
    wget --no-check-certificate -nv $URL -O - > $LEAPFILE || \
    rm $LEAPFILE

if [ ! -f $LEAPFILE ]
then
    echo "Failed to download $LEAPFILE from $URL"

    if [ -f $PERHAPS1 ]
    then
	echo "Copy $LEAPFILE from $PERHAPS1"
	cp $PERHAPS1 $LEAPFILE
    fi
fi

if [ ! -f $LEAPFILE ]
then
    if [ -z ${DOWNLOADFAILOK+x} ]
    then
	echo "Try on other platform, or manually, or use IGNORELEAPSECONDFILE=1"
	exit $ERROREXIT
    else
	echo "Download attempt before expiry - failure ok."
	exit 0
    fi
fi

# Download itself was a success
touch $LEAPFILE.stamp

EXPIRE=`grep "\#@" $LEAPFILE | sed -e 's/#@[[:space:]]*//' -e 's/[[:space:]]*$//' -e 's/.$//'`

if [ $NOW1900 -gt $EXPIRE ]
then
    echo "Freshly downloaded leap-seconds file already expired ($NOW1900 > $EXPIRE): $LEAPFILE from $URL"
    echo "To ignore, use IGNORELEAPSECONDFILE=1"
    exit $ERROREXIT
fi

# echo $EXPIRE $NOW1900
DAYSLEFT=`expr \( $EXPIRE - $NOW1900 \) / 8640` # 86400, but last digit cut
echo "         $LEAPFILE ($DAYSLEFT days left)"

#!/bin/sh

# This file is part of DRASI - a data acquisition data pump.
#
# Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301  USA

ME=`basename "$0"`
DIRNAME=`dirname "$0"`

# This will not work well if we compile with something else...
CC=cc

CC_MACHINE=`$CC -dumpmachine`
CC_VERSION=`$CC -dumpversion`
ARCH_SUFFIX=${CC_MACHINE}_${CC_VERSION}

GDB=

if [ "x$1" = "x--gdb" ]
then
    GDB="gdb --args"
    shift
elif [ "x$1" = "x--lldb" ]
then
    GDB="lldb --"
    shift
elif [ "x$1" = "x--valgrind" ]
then
    GDB="valgrind"
    shift
fi

case $ME in
    lwrocmon|lwroclog|lwrocctrl|lwrocdt|lwrocmerge)
	PROGDIR=lwrocmon
	;;
    f_user_example|f_user_regress|f_user_speed|f_user_sync|f_user_cmvlc_mdpp|\
    filter_example|filter_trbts2lmdwr|filter_lmd2ebye)
	# Are for testing only, but anyhow useful.
	# Special name:
	PROGDIR=f_user_example
	exec $GDB $DIRNAME/../$PROGDIR/${ARCH_SUFFIX}-$ME "$@"
	;;
    testdaq)
	PROGDIR=testdaq
	;;
    nulldaq)
	PROGDIR=nulldaq
	;;
    trigbussim|port_help)
	PROGDIR=trigbussim
	;;
    test_track_timestamp)
	# Is for special testing only.
	PROGDIR=lwroc
	;;
    *)
	echo "Unknown DRASI executable: $ME"
	exit 1
	;;
esac

exec $GDB $DIRNAME/../$PROGDIR/bin_${ARCH_SUFFIX}/$ME "$@"

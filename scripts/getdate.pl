#!/usr/bin/perl

use strict;
use warnings;

# This script since some platforms have a 'date' command that does not do +%s
# And then 'perl -e command' is also broken...

my $timestamp = time();

if ($#ARGV >= 0)
{
    # This does the job of 'date -r'
    $timestamp = (stat($ARGV[0]))[9];
}

print "$timestamp\n";

# We actually use this script as first option, since some date
# implementations just give garbage when not knowing a format option.

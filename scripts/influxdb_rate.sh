#!/bin/bash

SERVER=""
DB=""
USER=""
MEAS="drasi_rate"
DRASIDIR=""

HOSTS=()

PASSWORDFILE=$HOME/.drasi/influxdbusers

USAGE="
Usage: influxdb_rate.sh [options] HOSTS

  --server=SERVER  InfluxDB server.
  --user=USER      Username.
  --db=DB          Database name.
  --meas=MEAS[,TAG]  Measurement and tag_set prefix.
  --drasi-dir=DIR  drasi/ directory.

Note: USER:PASSWORD to be stored in $PASSWORDFILE
"

set -e

for i in "$@"
do
    case $i in
	--help|-h)
	    echo "${USAGE}"
	    exit 0
	    ;;
	--server=*)
	    SERVER="${i#*=}"
	    ;;
	--db=*)
	    DB="${i#*=}"
	    ;;
	--user=*)
	    USER="${i#*=}"
	    ;;
	--meas=*)
	    MEAS="${i#*=}"
	    ;;
	--drasi-dir=*)
	    DRASIDIR="${i#*=}"
	    ;;
	*)
	    HOSTS+=($i)
	    ;;
    esac
done

if [ ! $SERVER ]
then
    echo "--server not given"
    exit 1
fi

if [ ! $DB ]
then
    echo "--db not given"
    exit 1
fi

if [ ! $USER ]
then
    echo "--user not given"
    exit 1
fi

USERPASSWD=`egrep "^$USER:" $PASSWORDFILE` ||
    (echo "Did not find $USER: in $PASSWORDFILE." ; exit 1 )
APITOKEN=`echo $USERPASSWD | sed -e s/.*://`

LINES=""

$DRASIDIR/bin/lwrocmon ${HOSTS[*]} --rate --influxdb | \
    grep "INFLUXDB" --line-buffered | sed -u -e "s/INFLUXDB://" | \
while read i
do
    # echo "line: " $i

    if [ "$i" = "INFLUXDBFLUSH:" ]
    then
	echo "  $SERVER/write?db=$DB  $LINES"
	curl -u $USERPASSWD -XPOST "$SERVER/write?db=$DB" --data-binary "$LINES"
	LINES=""
    else
	LINE="$MEAS,$i"
	LINES=$LINES$'\n'$LINE
    fi

    # curl -u $USER -XPOST "$SERVER/write?db=$DB" --data-binary "$MEAS,$i"
done

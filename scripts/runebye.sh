#!/bin/bash

# This file is part of DRASI - a data acquisition data pump.
#
# Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301  USA

SESSION="runebye"
TIMELIMIT=1h
ATTACH=1
MODE="mixed"

# 'seq 0 -x' not globally defined, myseq ensures:
#  myseq 0 0 -> 0
#  myseq 0 -x -> nothing
function myseq
{
    i=$1
    while [ $i -le $2 ]; do echo $((i++)); done
}

USAGE="
Usage: runebye.sh [options]

  --kill          kill running session
  --session=NAME  name of session (default '$SESSION')
  --no-attach     do not attach to session after creation
  --mode=NAME     test mode (mixed, fanout, midas-fanout, fanin)
  --help          prints this message
"

set -e

for i in "$@"
do
    case $i in
	--session=*)
	    SESSION="${i#*=}"
	    ;;
	--kill)
	    echo "Killing session '$SESSION'"
	    if tmux has-session -t $SESSION
	    then
		tmux kill-window -t $SESSION
	    else
		echo "No session to kill."
		exit 1
	    fi
	    exit
	    ;;
	--no-attach)
	    ATTACH=0
	    ;;
	--timeout=*)
	    TIMELIMIT="${i#*=}"
	    ;;
	--mode=mixed|--mode=fanout|--mode=midas-fanout|--mode=fanin)
	    MODE="${i#*=}"
	    ;;
	--help|-h)
	    echo "${USAGE}"
	    exit 0
	    ;;
	*)
	    echo "Unknown option: $i"
	    exit 1
	    ;;
    esac
done

if tmux has-session -t $SESSION
then
    if [ $ATTACH != 0 ]
    then
	echo "Found -- attaching to '$SESSION'..."
	tmux -2 attach-session -t $SESSION
	exit
    else
	echo "Found -- session '$SESSION' already existing."
	exit 1
    fi
fi

######################################################################

NUM_INSTANCES=10

######################################################################

LOGFILE=runebye.log

if ! touch $LOGFILE
then
    LOGFILE=/tmp/runebye.log
    if ! touch $LOGFILE
    then
	echo "Failed to touch logfile: $LOGFILE"
	exit 1
    fi
fi

######################################################################

# The commands to execute

declare -A CMD_PRE
declare -A CMD_INST
declare -A CMD_MON
declare -A CMD_RATEMON

######################################################################

MIDASDIR="$HOME/sw/MIDASNewGen_Core_140423"

EBYE_LOOPFILE=sorted6.ebye

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$MIDASDIR/DataPackage/DataXferLib/V4_TCP/Linux64/:$MIDASDIR/DataPackage/DataSpyLib/Linux64/

######################################################################

FILEWR="--file-writer"

if [ "$MODE" = "mixed" ]
then
    CMD_INST["0"]="bin/lwrocmerge \
      --buf=size=100M \
      --port=36000 \
      --log-no-start-wait \
      --merge-mode=ebye-unsorted \
      --file=loop,${EBYE_LOOPFILE} \
      --server=ebye-push-relay=localhost:21305,nohold"

    CMD_INST["1"]="bin/lwrocmerge \
      --buf=size=100M \
      --port=36100 \
      --log-no-start-wait \
      --merge-mode=ebye-unsorted \
      --ebye-push-sink=port=21305 \
      --server=xfer-push-relay=localhost:22305,nohold \
      --server=xfer-push-relay=localhost:10310,nohold"

    CMD_INST["2"]="bin/lwrocmerge \
      --buf=size=100M \
      --port=36200 \
      --log-no-start-wait \
      --merge-mode=xfer \
      --xfer-push-sink=port=22305"

    CMD_INST["3"]="$MIDASDIR/DataPackage/DataSink/Linux64/DataSink -p 10310"

    CMD_INST["4"]="$MIDASDIR/DataPackage/DataRelay/Linux64/DataRelay -n localhost -i 5 -p 24305"

    CMD_INST["5"]="bin/lwrocmerge \
      --buf=size=100M \
      --port=36400 \
      --log-no-start-wait \
      --merge-mode=xfer \
      --xfer-push-sink=port=24305 \
      $FILEWR"

    ###

    CMD_MON["0"]="bin/lwrocmon \
      localhost:36000 \
      --tree"

    CMD_MON["1"]="bin/lwrocmon \
      localhost:36100 \
      --tree"

    CMD_MON["2"]="bin/lwrocmon \
      localhost:36200 \
      --tree"

    CMD_MON["5"]="bin/lwrocmon \
      localhost:36400 \
      --tree"
fi

######################################################################

if [ "$MODE" = "fanout" ] || \
   [ "$MODE" = "midas-fanout" ] || \
   [ "$MODE" = "fanin" ]
then
    # Must wait for the merger to start, else nc will quit immediately.
    CMD_PRE["0"]="sleep 2 ; "
    CMD_INST["0"]="../egmwsort/bin/ebye_gen | \
      pv -L 100M | nc localhost 21305"

    if [ "$MODE" = "fanin" ]
    then
	CMD_PRE["1"]="sleep 2 ; "
	CMD_INST["1"]="../egmwsort/bin/ebye_gen --synth-ch=1 | \
	  pv -L 100M | nc localhost 21306"

	CMD_PRE["2"]="sleep 2 ; "
	CMD_INST["2"]="../egmwsort/bin/ebye_gen --synth-ch=2 | \
	   pv -L 100M | nc localhost 21307"
    fi

    if [ "$MODE" = "fanout" ] || \
       [ "$MODE" = "fanin" ]
    then
	CMD_INST["3"]="bin/lwrocmerge \
	  --buf=size=100M \
	  --port=36000 \
	  --log-no-start-wait \
	  --merge-mode=xfer \
	  --xfer-push-sink=port=21305 \
	  --server=xfer-push-relay=localhost:22305 \
	  --server=xfer-push-relay=localhost:23305,nohold \
	  --server=xfer-push-relay=localhost:24305,nohold \
	  --server=xfer-push-relay=localhost:25305 \
	  $FILEWR"

	if [ "$MODE" = "fanin" ]
	then
	    CMD_INST["3"]="${CMD_INST["3"]} \
	      --xfer-push-sink=port=21306 \
	      --xfer-push-sink=port=21307"
	fi

	CMD_MON["3"]="bin/lwrocmon \
	  localhost:36000 \
	  --tree"
    fi

    if [ "$MODE" = "midas-fanout" ]
    then
	CMD_INST["3"]="../midas_caen_tspy/fanout \
	  -p 21305 \
	  -o localhost:22305 \
	  -o localhost:23305 \
	  -o localhost:24305"
    fi

    CMD_INST["4"]="bin/lwrocmerge \
      --buf=size=100M \
      --port=36100 \
      --log-no-start-wait \
      --merge-mode=xfer \
      --xfer-push-sink=port=25305 \
      "

    CMD_MON["4"]="bin/lwrocmon \
      localhost:36100 \
      --tree"

    if [ "$MODE" = "fanout" ]
    then
	CMD_INST["4"]="${CMD_INST["4"]} \
	  --server=drasi,dest=localhost:36200"

	CMD_INST["5"]="bin/lwrocmerge \
	  --buf=size=100M \
	  --port=36200 \
	  --log-no-start-wait \
	  --merge-mode=xfer \
	  --drasi=localhost:36100 \
	  --server=drasi,dest=localhost:36300 \
	  "

	CMD_INST["6"]="bin/lwrocmerge \
	  --buf=size=100M \
	  --port=36300 \
	  --log-no-start-wait \
	  --merge-mode=ebye \
	  --drasi=localhost:36200 \
	  "
    fi

    CMD_INST["7"]="nc -l 22305 | \
      ../egmwsort/bin/egmwsort - --synthetic-check"

    CMD_INST["8"]="nc -l 23305 | \
      ../egmwsort/bin/egmwsort - --synthetic-check"

    CMD_INST["9"]="nc -l 24305 | \
      ../egmwsort/bin/egmwsort - --synthetic-check"
fi

######################################################################

TIMEOUT="timeout --signal=KILL $TIMELIMIT "

######################################################################

# Pane numbers

PANE_MONMASTER=$((NUM_INSTANCES))

######################################################################

# Create session and set panes up

echo "Create new '$SESSION'..."
cols=$(tput cols)
lines=$(tput lines)
tmux -2 new-session -d -x $cols -y $lines -s $SESSION

# Make first of three columns
tmux split-window -h -l $((2*cols/3))

# In first column, make instance rows
for i in `myseq 0 $((NUM_INSTANCES-2))`
do
    tmux select-pane -t $i
    #tmux display -p '#{pane_width}x#{pane_height}'
    #echo $i: -v -l $(((NUM_INSTANCES-i-1)*lines/(NUM_INSTANCES)))
    tmux split-window -v -l $(((NUM_INSTANCES-i-1)*lines/(NUM_INSTANCES)))
done

# Make second and third columns
tmux select-pane -t $PANE_MONMASTER
tmux split-window -h -l $((cols/3))

# In second column, make instance rows
for i in `myseq 0 $((NUM_INSTANCES-2))`
do
    tmux select-pane -t $((NUM_INSTANCES+i))
    #echo $i: -v -l $(((NUM_INSTANCES-i-1)*lines/(NUM_INSTANCES)))
    tmux split-window -v -l $(((NUM_INSTANCES-i-1)*lines/(NUM_INSTANCES)))
done

######################################################################

######################################################################

# Done setting panes up, start the programs.

# tmux select-pane -t $PANE_WRITELOG
# tmux send-keys "$TIMEOUT $CMD_WRITELOG" C-m

# tmux select-pane -t $PANE_SHOWLOG
# tmux send-keys "$TIMEOUT $CMD_SHOWLOG" C-m

for i in `myseq 0 $((NUM_INSTANCES-1))`
do
    tmux select-pane -t $i
    tmux send-keys "RUNSIMCMD=\"${CMD_PRE["$i"]}$TIMEOUT ${CMD_INST["$i"]}\"" C-m
    tmux send-keys "echo \$RUNSIMCMD" C-m
    tmux send-keys "eval \${RUNSIMCMD}" C-m

    tmux select-pane -t $((NUM_INSTANCES+i))
    tmux send-keys "$TIMEOUT ${CMD_MON["$i"]}" C-m
done

if [ $ATTACH != 0 ]
then
    tmux -2 attach-session -t $SESSION
fi

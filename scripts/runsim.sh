#!/bin/bash

# This file is part of DRASI - a data acquisition data pump.
#
# Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301  USA

SESSION="runsim"
NUM_SLAVES_LIST=0
NOSINGLEEB=0
NOSINGLETRIVA=0
EBFILTER=0
TSFILTER=0
TIMESORT=0
NOTIMESORT2=0
DEFAULTTIMEOUT=0
NTPSERVER=""
TRANSSERV=0
STREAMSERV=0
FIRSTTRANSSERV=0
FIRSTSTREAMSERV=0
TIMELIMIT=1h
F_USER_EXAMPLE="f_user_example"
VALGRIND=""
SEGFAULT=0
SPAMLOG=0
FLOODLOG=0
TRIGRATE=100000
PERIODIC=""
TSANALYSESYNCTRIGGER=""
TRIGBUSSIMUSR1=""
TRIGBUSSIMUSR2=""
ATTACH=1
EVENT_SIZE=0
LOGNORATELIMIT=""
LOGACKWAIT=""

# 'seq 0 -x' not globally defined, myseq ensures:
#  myseq 0 0 -> 0
#  myseq 0 -x -> nothing
function myseq
{
    i=$1
    while [ $i -le $2 ]; do echo $((i++)); done
}

USAGE="
Usage: runsim.sh [options]

  --kill          kill running session
  --slaves=N[,M]  start with N readout slaves (or N,M or N,M,O or ...)
  --no-eb         do not add EB for single masters
  --no-triva      do not use trigger bus for single masters
  --eb-filter     filter stage in event builder
  --ts-filter     timestamp filter stage in first readout (w/ transport server)
  --timesort      add a time-sorter stage
  --no-timesort2  do not add the second (verification) time-sorter stage
  --def-timeout   Use default timeouts (spill stuck / ts disable [none]).
  --ntp=HOST      use direct NTP for adjusted time tracking
  --trans=N|hold  transport server from last stage with N clients
  --stream=N|hold stream server from last stage with N clients
  --firsttrans    transport server from first stage with N clients
  --firststream   stream server from first stage with N clients
  --level=LVL     show log messages at level (info, log, debug)
  --segfault=N    Cause segfault at event N in master.
  --spam-log      Generate log messages every second in master.
  --flood-log     Generate many log messages every event in master.
  --trigrate=N    Trigger rate.
  --periodic=N    Periodic trigger N.
  --sigusr1-wrong-trig  trigbussim SIGUSR2 causes a wrong trigger read.
  --sigusr2-spill-flip  trigbussim SIGUSR2 causes a spill trigger.
  --sigusr2-spill-rand  trigbussim SIGUSR2 causes a random spill trigger.
  --event-size=N  Event size (master nodes).
  --log-ack-wait  Wait for log client ack of each message.
  --log-no-rate-limit  Do not rate-limit log messages.
  --timeout=LIM   time limit (for timeout(1)) of each process (default 1h)
  --valgrind      run all processes under valgrind
  --regress       use f_user_regress instead of f_user_example
  --session=NAME  name of session (default '$SESSION')
  --no-attach     do not attach to session after creation
  --help          prints this message
"

set -e

for i in "$@"
do
    case $i in
	--session=*)
	    SESSION="${i#*=}"
	    ;;
	--kill)
	    echo "Killing session '$SESSION'"
	    if tmux has-session -t $SESSION
	    then
		tmux kill-window -t $SESSION
	    else
		echo "No session to kill."
		exit 1
	    fi
	    exit
	    ;;
	--no-attach)
	    ATTACH=0
	    ;;
	--slaves=*)
	    NUM_SLAVES_LIST="${i#*=}"
	    NUM_SLAVES_LIST=`echo $NUM_SLAVES_LIST | sed -e "s/,/ /g"`
	    ;;
	--timesort)
	    TIMESORT=1
	    ;;
	--eb-filter)
	    EBFILTER=1
	    ;;
	--ts-filter)
	    TSFILTER=1
	    ;;
	--no-timesort2)
	    NOTIMESORT2=1
	    ;;
	--def-timeout)
	    DEFAULTTIMEOUT=1
	    ;;
	--ntp=*)
	    NTPSERVER="--ntp=${i#*=}"
	    ;;
	--no-eb)
	    NOSINGLEEB=1
	    ;;
	--no-triva)
	    NOSINGLETRIVA=1
	    ;;
	--trans=hold)
	    TRANSSERV="hold"
	    ;;
	--trans=*)
	    TRANSSERV="nohold,clients=${i#*=}"
	    ;;
	--trans)
	    TRANSSERV="nohold"
	    ;;
	--stream=hold)
	    STREAMSERV="hold"
	    ;;
	--stream=*)
	    STREAMSERV="nohold,clients=${i#*=}"
	    ;;
	--stream)
	    STREAMSERV="nohold"
	    ;;
	--firsttrans=hold)
	    FIRSTTRANSSERV="hold"
	    ;;
	--firsttrans=*)
	    FIRSTTRANSSERV="nohold,clients=${i#*=}"
	    ;;
	--firsttrans)
	    FIRSTTRANSSERV="nohold"
	    ;;
	--firststream=hold)
	    FIRSTSTREAMSERV="hold"
	    ;;
	--firststream=*)
	    FIRSTSTREAMSERV="nohold,clients=${i#*=}"
	    ;;
	--firststream)
	    FIRSTSTREAMSERV="nohold"
	    ;;
	--level=*)
	    SHOWLOGLEVEL="--${i#*=}"
	    ;;
	--segfault=*)
	    SEGFAULT="${i#*=}"
	    ;;
	--spam-log)
	    SPAMLOG=1
	    ;;
	--flood-log)
	    FLOODLOG=1
	    ;;
	--trigrate=*)
	    TRIGRATE="${i#*=}"
	    ;;
	--periodic=*)
	    PERIODIC=$i
	    TSANALYSESYNCTRIGGER="--merge-ts-analyse-sync-trig=${i#*=}"
	    ;;
	--sigusr1-wrong-trig)
	    TRIGBUSSIMUSR1=--sigusr1-wrong-trig
	    ;;
	--sigusr2-spill-flip)
	    TRIGBUSSIMUSR2=--sigusr2-spill-flip
	    ;;
	--sigusr2-spill-rand)
	    TRIGBUSSIMUSR2=--sigusr2-spill-rand
	    ;;
	--event-size=*)
	    EVENT_SIZE="${i#*=}"
	    ;;
	--log-no-rate-limit)
	    LOGNORATELIMIT="--log-no-rate-limit"
	    ;;
	--log-ack-wait)
	    LOGACKWAIT="--log-ack-wait"
	    ;;
	--timeout=*)
	    TIMELIMIT="${i#*=}"
	    ;;
	--valgrind)
	    VALGRIND="--valgrind"
	    ;;
	--regress)
	    F_USER_EXAMPLE="f_user_regress"
	    ;;
	--help|-h)
	    echo "${USAGE}"
	    exit 0
	    ;;
	*)
	    echo "Unknown option: $i"
	    exit 1
	    ;;
    esac
done

if tmux has-session -t $SESSION
then
    if [ $ATTACH != 0 ]
    then
	echo "Found -- attaching to '$SESSION'..."
	tmux -2 attach-session -t $SESSION
	exit
    else
	echo "Found -- session '$SESSION' already existing."
	exit 1
    fi
fi

######################################################################

declare -A NUM_TRIVAS
declare -A HAS_EB

TIMESORT2=$TIMESORT
if [ $NOTIMESORT2 = 1 ]
then
    TIMESORT2=0
fi

NUM_INSTANCES=0

busno=0
for NUM_SLAVES in $NUM_SLAVES_LIST
do
    HAS_EB["$busno"]=$((!NOSINGLEEB))

    h=${HAS_EB["$busno"]}

    echo HASEB: ${HAS_EB["$busno"]} $h

    if [ $NUM_SLAVES != 0 ]; then HAS_EB["$busno"]=1; fi

    echo HASEB: ${HAS_EB["$busno"]}

    NUM_INSTANCES=$((NUM_INSTANCES + 1+NUM_SLAVES+HAS_EB["$busno"]))
    NUM_TRIVAS["$busno"]=$((NUM_SLAVES+1))
    busno=$((busno+1))
done

NUM_BUSES=$busno

NUM_RATEMON=$((NUM_BUSES + TIMESORT + TIMESORT2))

NUM_INSTANCES=$((NUM_INSTANCES + TIMESORT + TIMESORT2))

######################################################################

LOGFILE=runsim.log

if ! touch $LOGFILE
then
    LOGFILE=/tmp/runsim.log
    if ! touch $LOGFILE
    then
	echo "Failed to touch logfile: $LOGFILE"
	exit 1
    fi
fi

######################################################################

# The commands to execute

declare -A CMD_INST
declare -A CMD_MON
declare -A CMD_RATEMON

# Second stage (verifying) timesorter is not user for stream/transport
IND_FINAL=$((NUM_INSTANCES-1-TIMESORT2))

# If we have a timesorter, it is last
IND_TS=$IND_FINAL
IND_TS2=$((IND_TS+1))

NEXT_BUS_FIRST_IND=0

PORT_TS=23999
PORT_TS2=23998

CMD_WRITELOG="bin/lwrocmon $VALGRIND --log=$LOGFILE"

CMD_SHOWLOG="tail -f $LOGFILE | bin/lwroclog $VALGRIND $SHOWLOGLEVEL"

FILEWR="--file-writer"

if [ $TIMESORT = 1 ]
then
    CMD_INST["$IND_TS"]="bin/lwrocmerge $VALGRIND \
      $LOGNORATELIMIT $LOGACKWAIT \
      --port=$PORT_TS \
      --label=TS \
      --merge-mode=wr $FILEWR $TSANALYSESYNCTRIGGER \
      --merge-ts-analyse-ref=1 \
      --buf=size=50M,nohint"
    CMD_WRITELOG="$CMD_WRITELOG localhost:$PORT_TS"

    CMD_MON["$IND_TS"]="bin/lwrocmon $VALGRIND localhost:$PORT_TS"

    CMD_RATEMON["$NUM_BUSES"]="bin/lwrocmon $VALGRIND localhost:$PORT_TS \
      --rate"

    FILEWR=""
fi

if [ $TIMESORT2 = 1 ]
then
    CMD_INST["$IND_TS2"]="bin/lwrocmerge $VALGRIND \
      $LOGNORATELIMIT $LOGACKWAIT \
      --port=$PORT_TS2 \
      --label=TS2 \
      --merge-mode=wr $TSANALYSESYNCTRIGGER \
      --buf=size=50M,nohint"
    CMD_WRITELOG="$CMD_WRITELOG localhost:$PORT_TS2"

    CMD_MON["$IND_TS2"]="bin/lwrocmon $VALGRIND localhost:$PORT_TS2"

    CMD_RATEMON["$((NUM_BUSES+1))"]="bin/lwrocmon $VALGRIND \
      localhost:$PORT_TS2 --rate"

    CMD_INST["$IND_TS"]="${CMD_INST["$IND_TS"]} \
      --server=drasi,dest=localhost:$PORT_TS2"

    TSDISABLE="ts-disable=45s,"
    if [ $DEFAULTTIMEOUT = 1 ]
    then
	TSDISABLE=""
    fi

    CMD_INST["$IND_TS2"]="${CMD_INST["$IND_TS2"]} \
      --drasi=${TSDISABLE}localhost:$PORT_TS"
fi

declare -A CMD_TRIGBUSSIM

TS_MAX_EV_SIZE=0

busno=0
for NUM_SLAVES in $NUM_SLAVES_LIST
do
    IND_MASTER=$((NEXT_BUS_FIRST_IND))
    IND_SLAVE=$((IND_MASTER+1))
    IND_EB=$((IND_SLAVE+NUM_SLAVES))

    NEXT_BUS_FIRST_IND=$((IND_EB+${HAS_EB["$busno"]}))

    PORT_MASTER=$((24000+busno*100))
    PORT_EB=$((24099+busno*100))
    PORT_TBSIM=$((24097+busno*100))

    IND_DATA=$IND_EB
    PORT_DATA=$PORT_EB
    if [ ${HAS_EB["$busno"]} != 1 ]
    then
	IND_DATA=$IND_MASTER
	PORT_DATA=$PORT_MASTER
    fi

    echo IND_MASTER $IND_MASTER  IND_DATA $IND_DATA   PORT_DATA $PORT_DATA

    CMD_TRIGBUSSIM["$busno"]="bin/trigbussim $VALGRIND \
      --port=$PORT_TBSIM --rate=$TRIGRATE \
      $TRIGBUSSIMUSR1 $TRIGBUSSIMUSR2 $PERIODIC \
      --modules=${NUM_TRIVAS["$busno"]} "

    F_USER_EXAMPLE_MASTER="${F_USER_EXAMPLE}"
    SUBEV_OPT="--subev=crate=0,t=1234,st=0,proc=0,ctrl=0"

    if [ $TIMESORT = 1 ] || [ $SEGFAULT != 0 ] || [ $EVENT_SIZE != 0 ] || \
	   [ $NOSINGLETRIVA != 0 ] || [ $SPAMLOG != 0 ] || [ $FLOODLOG != 0 ]
    then
	F_USER_EXAMPLE_MASTER="testdaq"
	SUBEV_OPT="--wr-stamp=$((busno+1))"
	if [ $SEGFAULT != 0 ]
	then
	    SUBEV_OPT="${SUBEV_OPT} --segfault=${SEGFAULT}"
	fi
	if [ $SPAMLOG != 0 ]
	then
	    SUBEV_OPT="${SUBEV_OPT} --spam-log"
	fi
	if [ $FLOODLOG != 0 ]
	then
	    SUBEV_OPT="${SUBEV_OPT} --flood-log"
	fi
	if [ $EVENT_SIZE != 0 ]
	then
	    SUBEV_OPT="${SUBEV_OPT} --event-size=${EVENT_SIZE} \
	      --max-ev-size=${EVENT_SIZE}"
	fi
    fi

    CMD_INST["$IND_MASTER"]="bin/${F_USER_EXAMPLE_MASTER} $VALGRIND \
      $LOGNORATELIMIT $LOGACKWAIT \
      --port=$PORT_MASTER \
      --label=MA$((busno)) \
      --buf=size=50M,nohint \
       ${SUBEV_OPT} $NTPSERVER"

    if [ $NUM_SLAVES != 0 ] || [ $NOSINGLETRIVA = 0 ]
    then
	CMD_INST["$IND_MASTER"]="${CMD_INST["$IND_MASTER"]} \
	  --triva=master,spill1213,sim=1@localhost:${PORT_TBSIM}"
    fi

    if [ $TSFILTER = 1 ]
    then
	CMD_INST["$IND_MASTER"]="${CMD_INST["$IND_MASTER"]} \
	  --server=ts-filter,trans --ts-filter-mode=wr"
	# Only use in one system (trigger bus), due to fixed transport
	# server port.
	TSFILTER=0
    fi

    if [ $DEFAULTTIMEOUT != 1 ]
    then
	CMD_INST["$IND_MASTER"]="${CMD_INST["$IND_MASTER"]} \
	  --inspill-stuck-timeout=5s --max-ev-interval=10s"
    fi

    if [ ${HAS_EB["$busno"]} = 1 ]
    then
	EVENT_BUILDER="lwrocmerge"
	if [ $EBFILTER = 1 ]
	then
	    EVENT_BUILDER="filter_example"
	fi

	CMD_INST["$IND_MASTER"]="${CMD_INST["$IND_MASTER"]} \
	  --server=drasi,dest=localhost:$PORT_EB --eb=localhost:$PORT_EB"
	CMD_INST["$IND_EB"]="bin/${EVENT_BUILDER} $VALGRIND \
	  $LOGNORATELIMIT $LOGACKWAIT \
	  --port=$PORT_EB \
	  --label=EB$((busno)) \
	  --eb-master=localhost:$PORT_MASTER \
	  --drasi=localhost:$PORT_MASTER \
	  --merge-mode=event $FILEWR \
	  --buf=size=50M,nohint"
    fi

    CMD_WRITELOG="$CMD_WRITELOG \
      localhost:$PORT_MASTER localhost:$PORT_EB"

    CMD_MON["$IND_MASTER"]="bin/lwrocmon $VALGRIND localhost:$PORT_MASTER"

    if [ ${HAS_EB["$busno"]} = 1 ]
    then
	CMD_MON["$IND_EB"]="bin/lwrocmon $VALGRIND localhost:$PORT_EB"
    fi

    CMD_RATEMON["$busno"]="bin/lwrocmon $VALGRIND localhost:$PORT_DATA --rate"

    for i in `myseq 1 $NUM_SLAVES`
    do
	CMD_INST["$IND_MASTER"]="${CMD_INST["$IND_MASTER"]} \
	  --slave=localhost:$((PORT_MASTER+i))"
	if [ ${HAS_EB["$busno"]} = 1 ]
	then
	    CMD_INST["$IND_EB"]="${CMD_INST["$IND_EB"]} \
	      --drasi=localhost:$((PORT_MASTER+i))"
	fi
	CMD_WRITELOG="$CMD_WRITELOG localhost:$((PORT_MASTER+i))"

	CMD_INST["$((IND_MASTER+i))"]="bin/${F_USER_EXAMPLE} $VALGRIND \
	  $LOGNORATELIMIT $LOGACKWAIT \
	  --port=$((PORT_MASTER+i)) \
	  --label=SL$((busno))_$((i+1)) \
	  --triva=slave,sim=$((i+1))@localhost:${PORT_TBSIM} \
	  --buf=size=5M,nohint \
	  --server=spilldelay,drasi,dest=localhost:$PORT_EB \
	  --master=localhost:$PORT_MASTER \
	  --subev=crate=0,t=1234,st=$i,proc=0,ctrl=0"
	CMD_MON["$((IND_MASTER+i))"]="bin/lwrocmon $VALGRIND \
	  localhost:$((PORT_MASTER+i))"
	# echo ${CMD_SLAVE["$i"]}

	if [ $DEFAULTTIMEOUT != 1 ]
	then
	    CMD_INST["$((IND_MASTER+i))"]="${CMD_INST["$((IND_MASTER+i))"]} \
	      --inspill-stuck-timeout=5s --max-ev-interval=10s"
	fi
    done

    # Just add the master event size
    MAX_EV_SIZE=$(($EVENT_SIZE + 4096 * (1 + $NUM_SLAVES)))

    echo "EB Max event size $MAX_EV_SIZE"

    if [ ${HAS_EB["$busno"]} = 1 ]
    then
	CMD_INST["$IND_EB"]="${CMD_INST["$IND_EB"]} \
	  --max-ev-size=$MAX_EV_SIZE"

	if [ $DEFAULTTIMEOUT != 1 ]
	then
	    CMD_INST["$IND_EB"]="${CMD_INST["$IND_EB"]} \
	      --max-ev-interval=20s"
	fi
    fi

    if [ $busno = 0 ]
    then
	if [ $FIRSTTRANSSERV != 0 ]
	then
	    CMD_INST["$IND_DATA"]="${CMD_INST["$IND_DATA"]} \
	      --server=trans,$FIRSTTRANSSERV"
	fi

	if [ $FIRSTSTREAMSERV != 0 ]
	then
	    CMD_INST["$IND_DATA"]="${CMD_INST["$IND_DATA"]} \
	      --server=stream,$FIRSTSTREAMSERV"
	fi
    fi

    if [ $TIMESORT = 1 ]
    then
	CMD_INST["$IND_DATA"]="${CMD_INST["$IND_DATA"]} \
	  --server=drasi,dest=localhost:$PORT_TS"

	TSDISABLE="ts-disable=30s,"
	if [ $DEFAULTTIMEOUT = 1 ]
	then
	    TSDISABLE=""
	fi

	CMD_INST["$IND_TS"]="${CMD_INST["$IND_TS"]} \
	  --drasi=${TSDISABLE}localhost:$PORT_DATA"

	TS_MAX_EV_SIZE=$((MAX_EV_SIZE > TS_MAX_EV_SIZE ? \
				      MAX_EV_SIZE : TS_MAX_EV_SIZE))
    fi

    busno=$((busno+1))
done

if [ $TIMESORT = 1 ]
then
    CMD_INST["$IND_TS"]="${CMD_INST["$IND_TS"]} \
	  --max-ev-size=$TS_MAX_EV_SIZE"

    if [ $DEFAULTTIMEOUT != 1 ]
    then
	CMD_INST["$IND_TS"]="${CMD_INST["$IND_TS"]} \
	  --max-ev-interval=35s"
    fi
fi

if [ $TIMESORT2 = 1 ]
then
    CMD_INST["$IND_TS2"]="${CMD_INST["$IND_TS2"]} \
	  --max-ev-size=$TS_MAX_EV_SIZE"
fi


if [ $TRANSSERV != 0 ]
then
    CMD_INST["$IND_FINAL"]="${CMD_INST["$IND_FINAL"]} \
      --server=trans,$TRANSSERV"
fi

if [ $STREAMSERV != 0 ]
then
    CMD_INST["$IND_FINAL"]="${CMD_INST["$IND_FINAL"]} \
      --server=stream,$STREAMSERV"
fi

######################################################################

TIMEOUT="timeout --signal=KILL $TIMELIMIT "

######################################################################

# Pane numbers

PANE_MONMASTER=$((NUM_INSTANCES))

PANE_TRIGBUSSIM=$((2*NUM_INSTANCES))
PANE_RATEMON=$((PANE_TRIGBUSSIM+NUM_BUSES))
PANE_WRITELOG=$((PANE_RATEMON+NUM_RATEMON))
PANE_SHOWLOG=$((PANE_WRITELOG+1))

######################################################################

# Create session and set panes up

echo "Create new '$SESSION'..."
cols=$(tput cols)
lines=$(tput lines)
tmux -2 new-session -d -x $cols -y $lines -s $SESSION

# Make first of three columns
tmux split-window -h -l $((2*cols/3))

# In first column, make instance rows
for i in `myseq 0 $((NUM_INSTANCES-2))`
do
    tmux select-pane -t $i
    #tmux display -p '#{pane_width}x#{pane_height}'
    #echo $i: -v -l $(((NUM_INSTANCES-i-1)*lines/(NUM_INSTANCES)))
    tmux split-window -v -l $(((NUM_INSTANCES-i-1)*lines/(NUM_INSTANCES)))
done

# Make second and third columns
tmux select-pane -t $PANE_MONMASTER
tmux split-window -h -l $((cols/3))

# In second column, make instance rows
for i in `myseq 0 $((NUM_INSTANCES-2))`
do
    tmux select-pane -t $((NUM_INSTANCES+i))
    #echo $i: -v -l $(((NUM_INSTANCES-i-1)*lines/(NUM_INSTANCES)))
    tmux split-window -v -l $(((NUM_INSTANCES-i-1)*lines/(NUM_INSTANCES)))
done

# In third column, first make two rows, last row is showing log (50 % height)
upperhalflines=$((lines/2))
tmux select-pane -t $PANE_TRIGBUSSIM
tmux split-window -v -l $((lines-upperhalflines))

# The divide upper pane into three panes: 1: trigbus, 2: ratemin: 3: write log
writeloglines=$((upperhalflines/4))                 # 25 % of half height
trigbuslines=$(((upperhalflines-writeloglines)/2))  # half of remainder
ratemonlines=$((upperhalflines-trigbuslines-writeloglines)) # the other half
# echo trigbus-ratemon-writelog: $trigbuslines $ratemonlines $writeloglines
tmux select-pane -t $PANE_TRIGBUSSIM
tmux split-window -v -l $((ratemonlines+writeloglines))
#echo -t $((PANE_TRIGBUSSIM+1))
tmux select-pane -t $((PANE_TRIGBUSSIM+1))
tmux split-window -v -l $writeloglines

# Then split the trigbussim pane into subpanes
for i in `myseq 0 $((NUM_BUSES-2))`
do
    tmux select-pane -t $((PANE_TRIGBUSSIM+i))
    #echo $i: -v -l $(((NUM_BUSES-i-1)*lines/(NUM_BUSES)))
    tmux split-window -v -l $(((NUM_BUSES-i-1)*trigbuslines/(NUM_BUSES)))
done

# Then split the ratemon pane into subpanes
for i in `myseq 0 $((NUM_RATEMON-2))`
do
    tmux select-pane -t $((PANE_RATEMON+i))
    #echo $i: -v -l $(((NUM_RATEMON-i-1)*lines/(NUM_RATEMON-i)))
    tmux split-window -v -l $(((NUM_RATEMON-i-1)*ratemonlines/(NUM_RATEMON)))
done

######################################################################

function check_listening
{
    attempts=0

    while [ $attempts -lt 3 ]
    do
	nc -z 127.0.0.1 $1 && return

	attempts=$((attempts+1))
	sleep 1
    done
    echo "*** NO ONE LISTENING on port $1 *** (after 3 seconds of waiting)"
    exit 1
}

######################################################################

# Done setting panes up, start the programs.

for i in `myseq 0 $((NUM_BUSES-1))`
do
    tmux select-pane -t $((PANE_TRIGBUSSIM+i))
    tmux send-keys "$TIMEOUT ${CMD_TRIGBUSSIM["$i"]}" C-m
done

sleep .1

for i in `myseq 0 $((NUM_BUSES-1))`
do
    PORT_TBSIM=$((24097+i*100))
    check_listening $PORT_TBSIM
done

for i in `myseq 0 $((NUM_RATEMON-1))`
do
    tmux select-pane -t $((PANE_RATEMON+i))
    tmux send-keys "$TIMEOUT ${CMD_RATEMON["$i"]}" C-m
done

tmux select-pane -t $PANE_WRITELOG
tmux send-keys "$TIMEOUT $CMD_WRITELOG" C-m

tmux select-pane -t $PANE_SHOWLOG
tmux send-keys "$TIMEOUT $CMD_SHOWLOG" C-m

for i in `myseq 0 $((NUM_INSTANCES-1))`
do
    tmux select-pane -t $i
    tmux send-keys "RUNSIMCMD=\"$TIMEOUT ${CMD_INST["$i"]}\"" C-m
    tmux send-keys "echo \$RUNSIMCMD" C-m
    tmux send-keys "eval \${RUNSIMCMD}" C-m

    tmux select-pane -t $((NUM_INSTANCES+i))
    tmux send-keys "$TIMEOUT ${CMD_MON["$i"]}" C-m
done

if [ $ATTACH != 0 ]
then
    tmux -2 attach-session -t $SESSION
fi

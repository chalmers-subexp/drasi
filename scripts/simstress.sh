#!/bin/bash

# This file is part of DRASI - a data acquisition data pump.
#
# Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301  USA

SESSION="runsim"
NUM_SLAVES=0
NUM_STRESS=0

USAGE="
Usage: simstress.sh [options]

  --slaves=N      start with N readout slaves
  --num-stress=N  number of times to cause havoc
  --session=NAME name of session (default '$SESSION')
  --help          prints this message
"

set -e

for i in "$@"
do
    case $i in
	--session=*)
	    SESSION="${i#*=}"
	    ;;
	--slaves=*)
	    NUM_SLAVES="${i#*=}"
	    ;;
	--num-stress=*)
	    NUM_STRESS="${i#*=}"
	    ;;
	--help|-h)
	    echo "${USAGE}"
	    exit 0
	    ;;
	*)
	    echo "Unknown option: $i"
	    exit 1
	    ;;
    esac
done

echo "Stress session '$SESSION'..."

function check_running
{
    # Before trying something nasty, make sure things are working.

    # Check by calling the rate monitor, and seeing that we indeed
    # have some rate.

    attempts=0

    while [ $attempts -lt 3 ]
    do
	if bin/lwrocmon localhost:24099 \
			--rate --count=3 --nz-rate-success > \
			/dev/null 2> /dev/null
	then
	    echo "Running!"
	    return;
	fi

	attempts=$((attempts+1))
	sleep 1
    done
    echo "*** SIMULATOR NOT RUNNING ***"
    echo $OUTPUT
    exit 1
}

function check_dead
{
    attempts=0

    sleep .1

    while [ $attempts -lt 3 ]
    do
	CHECK_DEAD=`cat check.dead`

	if [ x$EVIL_WHEN = x$CHECK_DEAD ]
	then
	    echo "Terminated! $attempts"
	    return;
	fi

	attempts=$((attempts+1))
	sleep 1
    done
    echo "*** PROCESS NOT TERMINATED *** (check.dead not updated)"
    exit 1
}

done_tortures=0

while [ 1 ]
do
    # Make sure the readout is running before doing something nasty
    check_running

    if [ $NUM_STRESS -gt 0 ] && [ $done_tortures -ge $NUM_STRESS ]
    then
	echo "Exercised stress $NUM_STRESS times, done."
	exit
    fi

    # We have a few ways to crash things:
    # SIGUSR1 to trigbussim - cause an event mismatch
    # C-c (abort a program), then restart it
    # C-z (stop a program), then let it continue

    EVIL_WHEN=`date "+%H:%M:%S.%3N"`
    EVIL_HOW=$((1 + RANDOM % 3))
    EVIL_WHO=$((1 + RANDOM % (2+NUM_SLAVES)))

    echo -n "$EVIL_WHEN: Be EVIL!  (how: $EVIL_HOW, who: $EVIL_WHO)  "

    if [ $EVIL_HOW -eq 1 ]
    then
	echo "Simulate mismatch!"
	killall -USR1 trigbussim
    fi
    if [ $EVIL_HOW -eq 2 ]
    then
	echo "Ctrl-C !"

	tmux send-keys -t $((EVIL_WHO-1)) C-c
	sleep .1
	# And two more just in case it does not exit cleanly
	# Not exiting cleanly is now a fault, so do not give it that chance...
	# tmux send-keys -t $((EVIL_WHO-1)) C-c
	# tmux send-keys -t $((EVIL_WHO-1)) C-c
	sleep 1

	# Check that is has exited.  By us being able to update a file.
	tmux send-keys -t $((EVIL_WHO-1)) "echo $EVIL_WHEN > check.dead" Enter
	check_dead

	tmux send-keys -t $((EVIL_WHO-1)) "# $EVIL_WHEN" Enter

	# Then try to get it up again

	tmux send-keys -t $((EVIL_WHO-1)) "echo \$RUNSIMCMD" Enter
	tmux send-keys -t $((EVIL_WHO-1)) "\$RUNSIMCMD" Enter

    fi
    if [ $EVIL_HOW -eq 3 ]
    then
	echo "stop-start cycle"

	bin/lwrocctrl localhost:24000 --acq-stop

	sleep 1

	bin/lwrocctrl localhost:24000 --acq-start
    fi
    if [ $EVIL_HOW -eq 4 ]
    then
	EVIL_SLEEP=$((1 + RANDOM % 10))

	echo "Ctrl-Z ! sleep $EVIL_SLEEP"

	tmux send-keys -t $((EVIL_WHO-1)) C-z
	sleep $EVIL_SLEEP

	tmux send-keys -t $((EVIL_WHO-1)) "# $EVIL_WHEN" Enter

	# Then try to get it up again

	tmux send-keys -t $((EVIL_WHO-1)) "fg" Enter
    fi

    done_tortures=$((done_tortures+1))
done

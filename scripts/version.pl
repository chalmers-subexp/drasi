#!/usr/bin/perl

# This file is part of DRASI - a data acquisition data pump.
#
# Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301  USA

use strict;
use warnings;

# Figure out the current version number of drasi.

# Currently, we are on a trajectory towards 1.0 according to
# 0.xx.yyy, where xx is a fractional number describing how
# many items in the todo-file have been handled.  (If items
# are added, the number will move backwards).  yyy is the
# count of 'TODO'/'FIXME' present in the code.  These generally
# mark smaller issues.

# Count minor issues.

`find . | egrep "akefile|\\.(h|c|sh|pl|mk)\$" | \
    grep -v "/gen" | \
    xargs egrep -i "TODO|FIXME" | egrep -v "TODO|FIXME"`;

my $YYY=`find . | egrep "akefile|\\.(h|c|sh|pl|mk)\$" | \
    grep -v "/gen" | grep -v "version.pl" | \
    xargs egrep -i "TODO|FIXME" | wc -l`;

chomp $YYY;

# Count TODO.TXT entries

my $filename = 'TODO.TXT';
open(my $fh, '<', $filename)
  or die "Could not open file '$filename' $!";

my $hadline = 0;
my @items = ( 0, 0 );
my $done = 0;
my $aredone = 0;

while (my $row = <$fh>) {
    chomp $row;
    if ($row =~ /^\s*$/) {
	$items[$aredone] += $hadline;
	$hadline = 0;
    }
    elsif ($row =~ /^=*$/) {
	$hadline = 0;
    }
    elsif ($row =~ /^Done$/) {
	$aredone = 1;
    }
    else {
	$hadline = 1;
    }
}

$items[$aredone] += $hadline;

close($fh);

my $XX="0";

$items[0] += $items[1];

# drasi has been used in production experiments for 5+ years
# (first note is Oct 2016), so we bump to be version 1.x :)

$XX = sprintf("%.2f", 1 + $items[1] / $items[0]);

# Show result

print "Version: $XX.$YYY   ($items[1]/$items[0])\n";

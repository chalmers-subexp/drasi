/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

/*****************************************************************************/

#include "lwroc_message.h"
#include "lwroc_mon_block.h"
#include "lwroc_net_conn_monitor.h"
#include "lwroc_readout.h"
#include "lwroc_parse_util.h"
#include "lwroc_data_pipe.h"
#include "lwroc_leap_seconds.h"

#include "lmd/lwroc_lmd_event.h"
#include "lmd/lwroc_lmd_titris_stamp.h"
#include "lmd/lwroc_lmd_white_rabbit_stamp.h"
#include "lmd/lwroc_lmd_ev_sev.h"
#include "lmd/lwroc_lmd_util.h"

#include <stdarg.h>
#include <stdio.h>

#include "../dtc_arch/acc_def/time_include.h"
#include "../dtc_arch/acc_def/myinttypes.h"

void init(void);
void read_event(uint64_t cycle, uint16_t trig);
void readout_loop(int *start_no_stop);

void cmdline_usage(void);
int parse_cmdline_arg(const char *request);

extern struct lwroc_readout_functions _lwroc_readout_functions;

/*****************************************************************************/

typedef struct test_config_t
{
  int  _titris_stamp;
  int  _wr_stamp;

  uint32_t _bad_stamp;
  uint32_t _bad_stamp_stretch;

  uint32_t _stamp_incr;

  int64_t  _stamp_offset;

  int  _sticky_fraction;

  int  _crate;

  uint32_t _event_size;

  uint64_t _allow_rate;

  int  _spam_log_msg;
  int  _flood_log_msg;

  uint64_t _segfault;

} test_config;

test_config _test_config;

/*****************************************************************************/

void lwroc_readout_pre_parse_functions(void)
{
  memset(&_test_config, 0, sizeof (_test_config));

  _test_config._segfault = (uint64_t) -1;

  /* _test_config._bad_stamp = 20000; */

  _lwroc_readout_functions.init = init;
  _lwroc_readout_functions.read_event = read_event;
  _lwroc_readout_functions.untriggered_loop = readout_loop;
  _lwroc_readout_functions.cmdline_fcns.usage = cmdline_usage;
  _lwroc_readout_functions.cmdline_fcns.parse_arg = parse_cmdline_arg;
  _lwroc_readout_functions.fmt = &_lwroc_lmd_format_functions;
}

/*****************************************************************************/

extern volatile int _lwroc_quit;

extern lwroc_thread_block *_main_thread_block;

extern lwroc_pipe_buffer_control *_lwroc_main_data;

extern lwroc_monitor_main_block  _lwroc_mon_main;
extern lwroc_mon_block          *_lwroc_mon_main_handle;
extern lwroc_net_conn_monitor   *_lwroc_mon_main_system_handle;

void cmdline_usage(void)
{
  printf ("  --titris-stamp=ID        Produce TITRIS-style time stamps.\n");
  printf ("  --wr-stamp=ID            Produce White rabbit-style time stamps.\n");
  printf ("  --bad-stamp=N            Produce bad-marked time-stamp every N events.\n");
  printf ("  --bad-stamp-stretch=N    Produce bad-marked time-stamp stretch N seconds.\n");
  printf ("  --stamp-incr=N           Increment timestamp by N ticks each event.\n");
  printf ("  --stamp-offset=N         Add offset N to timestamp.\n");
  printf ("  --sticky-fraction=N      Produce sticky events every N normal events.\n");
  printf ("  --crate=N                Mark subevents as crate N.\n");
  printf ("  --event-size=N           Pad events to size N (incl. headers).\n");
  printf ("  --rate=N                 Allowed data rate N B/s.\n");
  printf ("  --spam-log               Generate log messages every second.\n");
  printf ("  --flood-log              Generate many log messages every event.\n");
  printf ("  --segfault=N             Cause segfault at event N.\n");
  printf ("\n");
}

int parse_cmdline_arg(const char *request)
{
  const char *post;

  /*NOHELPCALL*/ /* We are a option subset, --help handled elsewhere. */

  if (LWROC_MATCH_C_PREFIX("--titris-stamp=",post)) {
    _test_config._titris_stamp =
      (int) lwroc_parse_hex_u32(post, "titris ID");
  }
  else if (LWROC_MATCH_C_PREFIX("--wr-stamp=",post)) {
    _test_config._wr_stamp =
      (int) lwroc_parse_hex_u32(post, "WRID");
  }
  else if (LWROC_MATCH_C_PREFIX("--bad-stamp=",post)) {
    _test_config._bad_stamp =
      lwroc_parse_hex_u32(post, "bad stamp");
  }
  else if (LWROC_MATCH_C_PREFIX("--bad-stamp-stretch=",post)) {
    _test_config._bad_stamp_stretch =
      lwroc_parse_hex_u32(post, "bad stamp stretch");
  }
  else if (LWROC_MATCH_C_PREFIX("--stamp-incr=",post)) {
    _test_config._stamp_incr =
      lwroc_parse_hex_u32(post, "stamp incr.");
  }
  else if (LWROC_MATCH_C_PREFIX("--stamp-offset=",post)) {
    _test_config._stamp_offset =
      (int64_t) lwroc_parse_hex(post, "stamp offset");
  }
  else if (LWROC_MATCH_C_PREFIX("--sticky-fraction=",post)) {
    _test_config._sticky_fraction =
      (int) lwroc_parse_hex_u32(post, "sticky fraction");
  }
  else if (LWROC_MATCH_C_PREFIX("--crate=",post)) {
    _test_config._crate =
      (int) lwroc_parse_hex_limit(post, "crate", 0, 0xff);
  }
  else if (LWROC_MATCH_C_PREFIX("--event-size=",post)) {
    _test_config._event_size =
      lwroc_parse_hex_u32(post, "event size");
  }
  else if (LWROC_MATCH_C_PREFIX("--rate=",post)) {
    _test_config._allow_rate =
      (uint64_t) lwroc_parse_hex_u32(post, "rate");
  }
  else if (LWROC_MATCH_C_ARG("--spam-log")) {
    _test_config._spam_log_msg = 1;
  }
  else if (LWROC_MATCH_C_ARG("--flood-log")) {
    _test_config._flood_log_msg = 1;
  }
  else if (LWROC_MATCH_C_PREFIX("--segfault=",post)) {
    _test_config._segfault =
      lwroc_parse_hex(post, "segfault");
  }
  else
    return 0;

  return 1;
}

/*****************************************************************************/

void lwroc_readout_setup_functions(void)
{
  if (_test_config._stamp_incr &&
      (!_test_config._titris_stamp &&
       !_test_config._wr_stamp))
    LWROC_BADCFG("--stamp-incr make no sense without "
		 "--titris-stamp/--wr-stamp.");
}

/*****************************************************************************/

typedef struct testdaq_state_t
{
  lmd_stream_handle *lmd_stream;

  uint64_t total; /* data written so far */

  uint32_t sticky_active;
  uint32_t sticky_mark;
  uint32_t sticky_payload_count;

} testdaq_state;

testdaq_state es = { NULL, 0, 0, 0, 0 };

uint64_t _spam_cycle_last = 1;
time_t _spam_last = 0;

void init(void)
{
  es.lmd_stream = lwroc_get_lmd_stream("READOUT_PIPE");

  lwroc_init_timestamp_track();
}

uint64_t _continous_stamp = 0;

void create_timestamp(uint32_t **dest, uint64_t cycle)
{
  struct timespec tnow;
  uint64_t stamp;
  int bad_mark = 0;

  if (!_test_config._titris_stamp &&
      !_test_config._wr_stamp)
    return;

  if (_test_config._stamp_incr)
    {
      _continous_stamp += _test_config._stamp_incr;
      stamp = _continous_stamp;
    }
  else
    {
      int leap;

      clock_gettime(CLOCK_REALTIME, &tnow);

      leap = lwroc_get_leap_seconds(tnow.tv_sec);

      /* Lets write the time-stamp in units of 1 ns - WR scale...
       * This is arbitrary.
       */

      stamp = (1000000000 * ((uint64_t) tnow.tv_sec +
			     (uint64_t) (int64_t) leap) +
	       (uint64_t) (tnow.tv_nsec / 1));

      /*
       * For testing of sync triggers.
       *
       * Trigbussim can generate periodic triggers, and then place them
       * at 0.123456789 after the top-of-the-second.  Since the timestamp
       * is gotten from the CPU clock, it will jitter.  Simply clamp to
       * the 0.123000xxx if the timestamp is 0.123xxxxxx.  This has the
       * unfortunate side-effect of creating some out-of-order
       * timestamps.
       */
      /*
      if ((stamp / 1000000) % 1000 == 123)
	stamp = (stamp / 1000000000) * 1000000000 + 123000000 + (stamp % 1000);
      */
    }

  stamp += (uint64_t) _test_config._stamp_offset;

#if 0
  if ((stamp & 0x000ffffff) == 0)
    {
      stamp -= 0x1000000;
      fprintf (stderr,
	       "Inject BAD: %016" PRIx16 "  \n", stamp);
    }
#endif

  if (_test_config._bad_stamp &&
      ((cycle - 1) % _test_config._bad_stamp) == 0)
    {
      bad_mark = 1;
    }

  if (_test_config._bad_stamp_stretch &&
      (((uint64_t) tnow.tv_sec) % (2 * _test_config._bad_stamp_stretch) <
       _test_config._bad_stamp_stretch))
    {
      bad_mark = 1;
    }

  lwroc_report_event_timestamp(stamp, 0);

  if (_test_config._titris_stamp)
    {
      uint32_t *titris_stamp = *dest;

      titris_stamp[0] =
	((((uint32_t) _test_config._titris_stamp) & 0xff) << 8) |
	(bad_mark ? TITRIS_STAMP_EBID_ERROR : 0);
      titris_stamp[1] = TITRIS_STAMP_L16_ID |
	(uint32_t) ( stamp        & 0xffff);
      titris_stamp[2] = TITRIS_STAMP_M16_ID |
	(uint32_t) ((stamp >> 16) & 0xffff);
      titris_stamp[3] = TITRIS_STAMP_H16_ID |
	(uint32_t) ((stamp >> 32) & 0xffff);

      *dest += 4;
    }
  if (_test_config._wr_stamp)
    {
      uint32_t *wr_stamp = *dest;

      wr_stamp[0] =
	((((uint32_t) _test_config._wr_stamp) & 0xff) << 8) |
	(bad_mark ? WHITE_RABBIT_STAMP_EBID_ERROR : 0);
      wr_stamp[1] = WHITE_RABBIT_STAMP_LL16_ID |
	(uint32_t) ( stamp        & 0xffff);
      wr_stamp[2] = WHITE_RABBIT_STAMP_LH16_ID |
	(uint32_t) ((stamp >> 16) & 0xffff);
      wr_stamp[3] = WHITE_RABBIT_STAMP_HL16_ID |
	(uint32_t) ((stamp >> 32) & 0xffff);
      wr_stamp[4] = WHITE_RABBIT_STAMP_HH16_ID |
	(uint32_t) ((stamp >> 48) & 0xffff);

      *dest += 5;
    }
}

int *_null_pointer = NULL;

void read_event(uint64_t cycle, uint16_t trig)
{
  uint32_t stamp_words = 0;
  uint32_t junk = 0;
  /* uint32_t junk = 50000 + (cycle % 70000); */
  size_t event_size;
  char *buf;
  uint32_t *junk_ptr;
  /* uint32_t i; */
  size_t size;

  lmd_event_10_1_host *event;
  lmd_subevent_10_1_host *sev;
  struct lwroc_lmd_subevent_info info;

  info.type = 0x0cae;
  info.subtype = 0x0cae;
  info.control = 0;
  info.subcrate = (uint8_t) _test_config._crate;
  info.procid = 0;

  if (_test_config._titris_stamp)
    stamp_words = 4;
  else if (_test_config._wr_stamp)
    stamp_words = 5;

  event_size =
    sizeof (lmd_event_10_1_host) +
    sizeof (lmd_subevent_10_1_host) +
    stamp_words * sizeof (uint32_t);

  if (_test_config._event_size)
    {
      if (event_size < _test_config._event_size)
	event_size = _test_config._event_size;
    }
  else
    {
      junk = (uint32_t) (cycle % 17);

      event_size += junk * sizeof (uint32_t);
    }

  if (_test_config._sticky_fraction)
    event_size += 6 * sizeof (uint32_t);

  lwroc_reserve_event_buffer(es.lmd_stream, (uint32_t) cycle,
			     event_size, 0, 1000);

  lwroc_new_event(es.lmd_stream, &event, trig);
  buf = lwroc_new_subevent(es.lmd_stream,
			   LWROC_LMD_SEV_NORMAL, &sev, &info);

  junk_ptr = (uint32_t *)buf;

  create_timestamp(&junk_ptr, cycle);

  size = (size_t) (((char *) junk_ptr) - ((char *) event));

  if (size + 6 * sizeof (uint32_t) <= event_size)
    {
      *(junk_ptr++) = 0; /* separator */
      *(junk_ptr++) = 0; /* seed */
      *(junk_ptr++) = 0; /* separator */
      *(junk_ptr++) = 7; /* separator */
      *(junk_ptr++) = es.sticky_active;
      *(junk_ptr++) = es.sticky_mark;

      /*
	  printf("Event %d: active %08x\n",
	  (int) cycle, es.sticky_active);
	  fflush(stdout);
      */
    }

#if 0
  memset(junk_ptr, 0, junk * sizeof (uint32_t));
#endif

  size = (size_t) (((char *) junk_ptr) - ((char *) event));

  if (size < event_size)
    {
      size_t add_words =
	(event_size - size) / sizeof (uint32_t);
      uint32_t *end_ptr = junk_ptr + add_words;
      uint64_t *junk_ptr64;
      uint64_t x1, x2, x3, x4;

      /* TODO: Version with 32-bit LCG for 32-bit platform.
       * 64-bit multiplication not fast on 32-bit.
       */

      /* Make sure we are aligned. */
      if (junk_ptr < end_ptr &&
	  !LWROC_IS_ALIGNED(junk_ptr, sizeof (uint64_t)))
	*(junk_ptr++) = 0x12345678;

      junk_ptr64 = (uint64_t *) junk_ptr;

      /* Use four LCG generators in parallel to fill the data.
       * Parallel to avoid CPU-bound dependency chain.
       */
      x1 =   cycle;
      x2 =   cycle ^ trig;
      x3 =  -cycle;
      x4 =  -cycle ^ trig;

      /* Since end_ptr may be misaligned, test 5 words while we write 4. */
      while (junk_ptr64 + 4 <= (uint64_t *) end_ptr)
	{
	  x1 = (6364136223846793005ll * x1 + 1442695040888963407ll);
	  x2 = (6364136223846793005ll * x2 + 1442695040888963407ll);
	  x3 = (6364136223846793005ll * x3 + 1442695040888963407ll);
	  x4 = (6364136223846793005ll * x4 + 1442695040888963407ll);

	  *(junk_ptr64++) = x1;
	  *(junk_ptr64++) = x2;
	  *(junk_ptr64++) = x3;
	  *(junk_ptr64++) = x4;
	}

      while (junk_ptr64 + 1 <= (uint64_t *) end_ptr)
	{
	  x1 = (6364136223846793005ll * x1 + 1442695040888963407ll);
	  *(junk_ptr64++) = (uint32_t) x1;
	}

      x1 = (6364136223846793005ll * x1 + 1442695040888963407ll);

      junk_ptr = (uint32_t *) junk_ptr64;

      if (junk_ptr < end_ptr)
	*(junk_ptr++) = (uint32_t) x1;

#if 0
      junk_ptr += add_words;
#endif
    }

  /*
    _end_written = junk_ptr + junk;
  */
  /*
    for (i = 0; i < junk; i++)
      *(junk_ptr++) = i;
  */
  lwroc_finalise_subevent(es.lmd_stream, LWROC_LMD_SEV_NORMAL, junk_ptr);

  if (_test_config._sticky_fraction &&
      (int) ((cycle >> 12) ^ cycle) % _test_config._sticky_fraction == 0)
    {
#if 0
      buf = lwroc_new_subevent(lmd_stream,
			       LWROC_LMD_SEV_STICKY_AFTER, &sev, &info);

      junk_ptr = (uint32_t *)buf;

      *(junk_ptr++) = cycle;

      lwroc_finalise_subevent(lmd_stream,
			      LWROC_LMD_SEV_STICKY_AFTER, junk_ptr);
#endif

      uint32_t sticky_after  = cycle & 0xff;
      uint32_t sticky_update = (cycle >> 8) & 0xff;
      uint32_t sticky_new    =  sticky_after  & ~es.sticky_active;
      uint32_t sticky_kill   = ~sticky_after  &  es.sticky_active;
      uint32_t sticky_renew  =  sticky_update &  es.sticky_active;
      int isev;
      /*
      char list[64];
      list[0] = 0;
      */

      if (_test_config._titris_stamp ||
	  _test_config._wr_stamp)
	{
	  uint32_t *p;

	  info.type = 10;
	  info.subtype = 1;
	  info.control = 0;
	  info.subcrate = (uint8_t) _test_config._crate;
	  info.procid = 0;

	  p = (uint32_t *) lwroc_new_subevent(es.lmd_stream,
					      LWROC_LMD_SEV_STICKY_AFTER,
					      &sev, &info);

	  create_timestamp(&p, cycle);

	  lwroc_finalise_subevent(es.lmd_stream,
				  LWROC_LMD_SEV_STICKY_AFTER, (char *) p);
	}

      for (isev = 0; isev < 8; isev++)
	{
	  char *sev_start;
	  uint32_t *p;
	  char *sevp_end;

	  if (!((sticky_kill |
		 sticky_new | sticky_renew) & (uint32_t) (1 << isev)))
	    continue;

	  info.type = 0x789a;
	  info.subtype = 0xbad0;
	  info.control = (uint8_t) isev;
	  info.subcrate = (uint8_t) _test_config._crate;
	  info.procid = 0;

	  buf = lwroc_new_subevent(es.lmd_stream,
				   LWROC_LMD_SEV_STICKY_AFTER,
				   &sev, &info);

	  if (sticky_kill & (uint32_t) (1 << isev))
	    {
	      /*
	      sprintf (list+strlen(list)," !%d",isev);
	      */
	      sev->_header.l_dlen = (uint32_t) -1; /* revoke */
	      lwroc_finalise_subevent(es.lmd_stream,
				      LWROC_LMD_SEV_STICKY_AFTER, buf);
	      continue;
	    }

	  sev_start = (char*) buf;

	  p = (uint32_t *) sev_start;

	  es.sticky_payload_count++;
	  *(p++) = es.sticky_payload_count;

	  es.sticky_mark = (es.sticky_mark & ~(uint32_t) (1 << isev)) |
	    ((es.sticky_payload_count & 1) << isev);

	  sevp_end = (char *) p;
	  /*
	  sprintf (list+strlen(list)," %d",isev);
	  */
	  lwroc_finalise_subevent(es.lmd_stream,
				  LWROC_LMD_SEV_STICKY_AFTER, sevp_end);
	}
      es.sticky_active = sticky_after;
      /*
      if (list[0])
	{
	  printf("Sticky after event %d: '%s' -> active %08x\n",
		 (int) cycle, list, es.sticky_active);
	  fflush(stdout);
	}
      */
    }

  lwroc_finalise_event_buffer(es.lmd_stream);
  /*
    fprintf(dump_fid, "Wrote event %p -- %p\n",
	    buf, buf + event_size);
    fflush(dump_fid);
  */

  if (cycle == _test_config._segfault)
    {
      *_null_pointer = 0;
    }

  es.total +=
    (uint64_t) ((size_t) (junk_ptr - (uint32_t *) event) *
		sizeof(uint32_t));

  if (_test_config._spam_log_msg &&
      (cycle & 15) == 0)
    {
      time_t now;

      now = time(NULL);

      if (now != _spam_last)
	{
	  LWROC_ACTION_FMT("Arrrrp... %" PRIu64 " [%" PRIu64 " /s] %.*s",
			   cycle,
			   cycle - _spam_cycle_last,
			   (int) (cycle % 10) + (int) ((cycle / 100) % 5),
			   "..::..::..::...................");

	  LWROC_DEBUG_FMT("Debug: %" PRIu64 "", cycle);
	  LWROC_SPAM_FMT("Spam: %" PRIu64 "", cycle);

	  _spam_last = now;
	  _spam_cycle_last = cycle;
	}
    }

  if (_test_config._flood_log_msg)
    {
      int j, k;

      for (j = rand() % 100; j > 0; j--)
	{
	  k = rand() % 20;

	  LWROC_LOG_FMT("Flood... %.*s",
			k, "01234567890123456789");

	  for (k = rand() % 5; k > 0; k--)
	    {
	      lwroc_message_rate_ctrl(LWROC_MESSAGE_RATE_CTRL_LIMIT);
	      LWROC_LOG("Rate-limited message.");
	    }

	  if (rand() % 2)
	    LWROC_LOG("Alone.");
	}
    }
}

void readout_loop(int *start_no_stop)
{
  unsigned int cycle;
  /*int i;*/
  uint64_t allowance = 0; /* data allowed until next check */
  struct timespec chk_last;

  *start_no_stop = 0;

  clock_gettime(CLOCK_REALTIME, &chk_last);
  /*
  FILE *dump_fid = fopen("dump.txt","w");
  */

  /* Not really a TRIVA status, but a status... */
  _lwroc_mon_main_system_handle->_block._aux_status = LWROC_TRIVA_STATUS_RUN;

  for (cycle = 1; !_lwroc_main_thread->_terminate; cycle++)
    {
      /* LWROC_INFO("Another message hits the fan!"); */

      /* If there is a rate limit, we allow data to be produced in
       * chunks of 0.01 of the limit per second.  Every time the limit
       * is hit, we check the time, and make any additional allowance
       * that the elapsed time suggests.  If the allowance to be added
       * is less than 0.01 of the limit per second, sleep for 0.01 s.
       * If the allowance to be added is more, then do not sleep.
       */

      if (_test_config._allow_rate &&
	  es.total > allowance)
	{
	  struct timespec chk_now;
	  uint64_t additional;
	  double elapsed;

	  /* How much time has passed since last check? */

	  clock_gettime(CLOCK_REALTIME, &chk_now);

	  elapsed =
	    (double) (chk_now.tv_sec - chk_last.tv_sec) +
	    1.e-9 * (double) (chk_now.tv_nsec - chk_last.tv_nsec);

	  additional =
	    (uint64_t) ((double) _test_config._allow_rate * elapsed);

	  allowance += additional;
	  /*
	  printf ("%.3f  %10" PRIu64 "  %10" PRIu64 "\n",
		  elapsed, additional, allowance - es.total);
	  */
	  if (allowance - es.total > _test_config._allow_rate)
	    allowance = es.total + _test_config._allow_rate;

	  if (allowance - es.total < _test_config._allow_rate / 50)
	    usleep(10000); /* 0.01 s */

	  chk_last = chk_now;
	}

      /* TODO: arrange a junk rate meter in the log viewer. */
      /*
      for (i = 0; i < 15; i++)
	LWROC_DEBUG_FMT("Junk %d", i);
      */

      read_event(cycle, 1);

      LWROC_MON_CHECK_COPY_BLOCK(_lwroc_mon_main_handle,&_lwroc_mon_main, 0);
      LWROC_MON_CHECK_COPY_CONN_MON_BLOCK(_lwroc_mon_main_system_handle, 0);
    }

  /* Not really a TRIVA status, but a status... */
  _lwroc_mon_main_system_handle->_block._aux_status = LWROC_TRIVA_STATUS_STOP;

  fprintf (stderr, "end-of-readout-loop\n");
}

/**/

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include <stdarg.h> /* Sometimes needed for vfprintf. */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "../lwroc/lwroc_message.h"

uint32_t _lwroc_debug_mask_enabled = 0;

int _message_internal_no_sleep = 0;

void lwroc_perror_internal(const lwroc_format_message_context *context,
			   const char *file, int line,
			   const char *s)
{
  (void) context;

  fprintf(stderr,"%s:%d: %s\n",file,line,s);
}

void lwroc_message_internal(int level,
			    const lwroc_format_message_context *context,
			    const char *file, int line,
			    const char *fmt,...)
{
  const char level_str[LWROC_MSGLVL_MAX+1] =
    { LWROC_MSGLVL_MARKS };

  va_list ap;

  (void) context;

  fprintf(stderr,"%s:%d: %d(%c): ",file,line,level,level_str[level]);
  va_start(ap, fmt);
  vfprintf(stderr, fmt, ap);
  va_end(ap);
  fprintf(stderr,"   \n"); /* spaces to clear periodic status message */

  if (level == LWROC_MSGLVL_SIGNAL ||
      level == LWROC_MSGLVL_BUG ||
      level == LWROC_MSGLVL_FATAL)
    {
      fprintf (stderr, "SIGNAL, BUG, or FATAL reported.  ");
      if (_message_internal_no_sleep == 1)
	{
	  fprintf (stderr, "Exiting.\n");
	  exit(1);
	}
      fprintf (stderr,
	       "Sleeping INDEFINITELY (to allow debugger attachment).\n");
      while ( 1 )
	sleep (100);
    }
  if (level == LWROC_MSGLVL_BADCFG)
    exit(1);
}

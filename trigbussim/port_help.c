/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include "../dtc_arch/acc_def/mystdint.h"
#include "../dtc_arch/acc_def/myinttypes.h"

#include "../lwroc/lwroc_message.h"
#include "../lwroc/lwroc_parse_util.h"
#include "../lwroc/lwroc_net_io_util.h"
#include "../lwroc/lwroc_hostname_util.h"
#include "../lwroc/lwroc_select_util.h"

void port_help_usage(char *cmdname)
{
  printf ("PORT_HELP - TCP port utility\n");
  printf ("\n");
  printf ("Usage: %s <options>\n", cmdname);
  printf ("\n");
  printf ("  --bound=HOST:PORT        Check if some server is listening on HOST:PORT.\n");
  printf ("  --random-port            Bind (and then close) to a random port, and print.\n");
  printf ("\n");
}

extern int _message_internal_no_sleep;

int main(int argc, char *argv[])
{
  int testport_fd;
  uint16_t test_port;
  int i;

  _message_internal_no_sleep = 1;

  for (i = 1; i < argc; i++)
    {
      char *request = argv[i];
      char *post;

      if (LWROC_MATCH_C_ARG("--help")) {
	port_help_usage(argv[0]);
	exit(0);
      } else if (LWROC_MATCH_C_PREFIX("--bound=",post)) {

	struct sockaddr_storage serv_addr;
	int fd;
	int ret;

	if (!lwroc_get_host_port(post, -1,
				 &serv_addr))
	  LWROC_BADCFG("Bad HOST:PORT specification.");

	fd = socket(PF_INET, SOCK_STREAM, 0);

	if (fd == -1)
	  {
	    LWROC_PERROR("socket");
	    LWROC_FATAL("Failed to create socket for bound test.");
	  }

	ret = lwroc_net_io_connect(fd, &serv_addr);

	if (ret == -1)
	  {
	    LWROC_PERROR("connect");
	    LWROC_FATAL_FMT("Failed to connect to %s.",post);
	  }

	/* Success. */

	lwroc_safe_close(fd);

      } else if (LWROC_MATCH_C_ARG("--random-port")) {

	testport_fd = lwroc_net_io_socket_bind(-1, 1, "randomport", 0);
	test_port = lwroc_net_io_socket_get_port(testport_fd, "randomport");
	lwroc_safe_close(testport_fd);

	printf ("%" PRIu16 "\n", test_port);
      } else {
	LWROC_BADCFG_FMT("Unrecognized option: '%s'", argv[i]);
      }
    }

  return 0;
}

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

/* Even simpler program to test the trigbussim code. */

#include <stdio.h>
#include <sys/types.h>
#include "../dtc_arch/acc_def/netinet_in_include.h"
#include "../dtc_arch/acc_def/socket_include.h"
#include <stdlib.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stddef.h>
#include <netinet/tcp.h>
#include <sched.h>
#include "../dtc_arch/acc_def/time_include.h"

#include "../lwroc/triva_registers.h"
#include "trigbussim.h"

#include "../dtc_arch/acc_def/myprizd.h"

#define PERROR_EXIT(s) do { perror(s); exit(1); } while (0)

#ifdef __GNUC__
# if __GNUC__ < 3
#  define OLD_VARIADIC 1 /* 2.95 do not do iso99 variadic macros */
# endif
#endif

#ifdef OLD_VARIADIC

#define INFO_FMT(x,__VA_ARGS__...) \
  do { fprintf(stdout, x "\n", __VA_ARGS__); } while (0)
#define ERROR_FMT_EXIT(x,__VA_ARGS__...) \
  do { fprintf(stderr, x "\n", __VA_ARGS__); exit(1); } while (0)

#else /* !OLD_VARIADIC */

#define INFO_FMT(x,...) \
  do { fprintf(stdout, x "\n", __VA_ARGS__); } while (0)
#define ERROR_FMT_EXIT(x,...)					\
  do { fprintf(stderr, x "\n", __VA_ARGS__); exit(1); } while (0)

#endif

int do_connect(uint16_t port)
{
  int fd;
  struct hostent *hostaddr;
  struct sockaddr_in servaddr;
  int ret;
  int flag;

  fd = socket(PF_INET, SOCK_STREAM, 0);

  if (fd == -1)
    PERROR_EXIT("socket");

  /* Disable Nagle's algorithm - otherwise we are very slow! */
  flag = 1;
  setsockopt(fd, IPPROTO_TCP, TCP_NODELAY,
	     (char *) &flag, sizeof(int));

  memset(&servaddr, 0, sizeof(servaddr));

  servaddr.sin_family = AF_INET;
  servaddr.sin_port = htons(port);

  hostaddr = gethostbyname("localhost");

  memcpy(&servaddr.sin_addr, hostaddr->h_addr,
	 (size_t) hostaddr->h_length);

  ret = connect(fd, (struct sockaddr *)&servaddr, sizeof(servaddr));

  if (ret == -1)
    ERROR_FMT_EXIT("Cannot connect to port %d.", port);
  else
    INFO_FMT("Connected to port %d.", port);

  return fd;
}

void triva_write(int fd, uint32_t off, uint32_t v)
{
  uint32_t msg[2];
  ssize_t n;

  msg[0] = htonl(0x80000000 | off);
  msg[1] = htonl(v);

  n = write(fd, &msg, sizeof (msg));
  if (n == -1)
    PERROR_EXIT("write(tw)");
  if (n != sizeof (msg))
    ERROR_FMT_EXIT("Did short (%" MYPRIszd ") write.", n);

  /* printf ("triva wrote %08x @ %08x\n", v, off); */
}

uint32_t triva_read(int fd, uint32_t off)
{
  uint32_t msg[1];
  ssize_t n;
  uint32_t v;

  msg[0] = htonl(0x40000000 | off);

  n = write(fd, &msg, sizeof (msg));
  if (n == -1)
    PERROR_EXIT("write(tr)");
  if (n != sizeof (msg))
    ERROR_FMT_EXIT("Did short (%" MYPRIszd ") write.", n);

  n = read(fd, &msg, sizeof (msg));
  if (n == -1)
    PERROR_EXIT("read(tr)");
  if (n != sizeof (msg))
    ERROR_FMT_EXIT("Did short (%" MYPRIszd ") write.", n);

  v = ntohl(msg[0]);

  /* printf ("triva read  %08x @ %08x\n", v, off); */

  return v;
}

#define LWROC_TRIVA_WRITE(reg, value) do {				\
    triva_write(fd, offsetof(lwroc_triva_reg_layout,reg), value);	\
  } while (0)
#define LWROC_TRIVA_READ(reg)					\
  triva_read(fd, offsetof(lwroc_triva_reg_layout,reg))


int main(int argc, char *argv[])
{
  int fd;
  trigsimbus_init_msg msg;
  ssize_t n;
  uint64_t triggers = 0;
  uint64_t triggers_last = 0;
  struct timeval last_t;

  int slave = 0;
  uint16_t port;

  if (argc > 1 && strcmp(argv[1],"--slave") == 0)
    slave = 1;

  memset(&msg, 0, sizeof (msg));

  fd = do_connect(TRIGBUSSIM_PORT);
  n = read(fd, &msg, sizeof (msg));
  if (n == -1)
    PERROR_EXIT("read(pmap)");
  if (n != sizeof (msg))
    ERROR_FMT_EXIT("Got short (%" MYPRIszd ") portmap response.", n);
  close(fd);

  if (ntohl(msg.magic) != TRIGBUSSIM_MAGIC)
    ERROR_FMT_EXIT("Bad magic (0x%08x) from portmapper.", ntohl(msg.magic));

  port = (uint16_t) ntohl(msg.port_mod);

  fd = do_connect(port);
  msg.magic = htonl((uint32_t) ~TRIGBUSSIM_MAGIC);
  msg.port_mod = htonl(slave ? 2 : 1);
  n = write(fd, &msg, sizeof (msg));
  if (n == -1)
    PERROR_EXIT("write(conn)");
  if (n != sizeof (msg))
    ERROR_FMT_EXIT("Did short (%" MYPRIszd ") write.", n);

  printf ("Before setup!\n");

  /* Ok, so we are connected.  Let's torture it! */

  LWROC_TRIVA_WRITE(control, TRIVA_CONTROL_BUS_ENABLE);
  if (!slave)
    LWROC_TRIVA_WRITE(control, TRIVA_CONTROL_MASTER);
  else
    LWROC_TRIVA_WRITE(control, TRIVA_CONTROL_SLAVE);

  LWROC_TRIVA_WRITE(control, TRIVA_CONTROL_HALT);
  LWROC_TRIVA_WRITE(control, TRIVA_CONTROL_RESET);

  /* */

  LWROC_TRIVA_WRITE(control, TRIVA_CONTROL_HALT);
  LWROC_TRIVA_WRITE(control, TRIVA_CONTROL_RESET);

  if (!slave)
    {
      LWROC_TRIVA_WRITE(status, 14); /*info->_mt = 14;*/
      LWROC_TRIVA_WRITE(control, TRIVA_CONTROL_GO);
    }
  else
    {
      LWROC_TRIVA_WRITE(control, TRIVA_CONTROL_GO);
    }

  printf ("Setup done!\n");

  /* */
  gettimeofday(&last_t,NULL);

  for ( ; ; )
    {
      uint32_t status;

      status = LWROC_TRIVA_READ(status);

      if (status & TRIVA_STATUS_EON)
	{
	  uint16_t got_trig = (uint16_t) (status & TRIVA_STATUS_TRIG_MASK);

	  /* printf ("Got trig: %d\n", got_trig); */

	  /* Release the deadtime. */

	  LWROC_TRIVA_WRITE(status, TRIVA_STATUS_EV_IRQ_CLEAR |
				    TRIVA_STATUS_IRQ_CLEAR);
	  LWROC_TRIVA_WRITE(status, TRIVA_STATUS_FC_PULSE);
	  LWROC_TRIVA_WRITE(status, TRIVA_STATUS_DT_CLEAR);

	  triggers++;

	  if (!(triggers & 0xff))
	    {
	      struct timeval now;
	      gettimeofday(&now,NULL);
	      if (now.tv_sec != last_t.tv_sec)
		{
		  double elapsed = (double) (now.tv_sec - last_t.tv_sec) +
		    1.e-6 * (double) (now.tv_usec - last_t.tv_usec);

		  double rate = (double) (triggers - triggers_last) / elapsed;

		  printf ("%" PRIu64"  %.0f Hz\n", triggers, rate);

		  triggers_last = triggers;
		  last_t = now;
		}
	    }

	  if (!slave)
	    {
	      /* fire another one */
	      LWROC_TRIVA_WRITE(status,
				(uint32_t) (got_trig & 7) + 1);
	    }
	}
      else
	{
	  /* printf ("No trigger...\n"); */
	  /* sleep(1); */
	  sched_yield();
	}
    }


  close(fd);
  printf ("Done!\n");

  return 0;
}

/* This file is part of DRASI - a data acquisition data pump.
 *
 * Copyright (C) 2017  Haakan T. Johansson  <f96hajo@chalmers.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

#include <stdio.h>
#include <time.h>
#include <string.h>
#include "../dtc_arch/acc_def/select_include.h"
#include <unistd.h>
#include <errno.h>
#include <stddef.h>
#include "../dtc_arch/acc_def/netinet_in_include.h"
#include <stdlib.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <math.h>
#include <signal.h>

#include "../lwroc/lwroc_message.h"
#include "../lwroc/lwroc_parse_util.h"
#include "../lwroc/triva_registers.h"
#include "../lwroc/lwroc_array_heap.h"
#include "../lwroc/lwroc_net_io_util.h"
#include "../lwroc/pd_linked_list.h"

#include "trigbussim.h"

#include "../dtc_arch/acc_def/myprizd.h"

typedef struct trigbussim_config_t
{
  uint16_t port;
  int modules;
  int debug;
  double rate;
  uint32_t periodic_trig;
  int sigusr1_wrong_trig;
  int sigusr2_spill_flip;
  struct
  {
    int mod;
    int n;
    int hash;
  } mismatch;
} trigbussim_config;

trigbussim_config _config;

extern int _message_internal_no_sleep;

/* Simulation of one or more TRIVA/MI buses.
 *
 * Instead of mapping hardware memory for register access, the
 * simulated DAQ program connects to a socket, and performs register
 * writes and reads using small writes and reads.
 *
 * We are also timing-based such that we can simulate some sort of
 * delay between a software issued trigger and it actually being
 * generated.  Or delay by the trigger bus cable.
 */

/* The easiest way to implement seems to roughly follow the
 * implementation of triva_mimic in the TRLO II.
 *
 * We have a state machine.  State transitions happen either due to
 * external signals, or due to timeouts.  When timeout-based, we
 * operate using the machine real clock, and progress step by step to
 * catch up with the machine clock.  This cannot lead to excessive
 * amounts of work to do, since we will always end up in some readout
 * state, that requires external input (= deadtime release) to
 * proceed.  (Unless we simulate lots of fast clear signals...)
 *
 * Since we use the same 'local signals' as the VHDL code, things
 * looks rather complicated, and could be written differently in C.
 * But since this is for testing and not so much performance, and
 * reading (see below) is fast, it should be just fine.
 *
 * Reading a register never modifies the state, so we can just
 * keep the register states, and serve them for any read.  I.e., a
 * read does not have to invoke the update-state-functions.
 *
 * Writes to registers do have effects (depending on where we are in
 * the state machine), so are treated as action registers.  We however
 * know (as opposed to the FPGA code (where it does not matter)), that
 * only one register can be written at the same time.
 */

#define TIMESPEC_CLEAR(a)     ((a)->tv_sec = (a)->tv_nsec = 0)
#define TIMESPEC_ISSET(a)     ((a)->tv_sec  || (a)->tv_nsec)
#define TIMESPEC_CMP(a,b,op) (((a)->tv_sec  == (b)->tv_sec ) ?	\
			      ((a)->tv_nsec op (b)->tv_nsec) :	\
			      ((a)->tv_sec  op (b)->tv_sec ))
#define TIMESPEC_ADD(a,b,r)  do {			\
    (r)->tv_sec  = (a)->tv_sec + (b)->tv_sec;		\
    (r)->tv_nsec = (a)->tv_nsec + (b)->tv_nsec;		\
    if ((r)->tv_nsec >= 1000000000) {			\
      (r)->tv_sec++;					\
      (r)->tv_nsec -= 1000000000;			\
    }							\
  } while (0)
#define TIMESPEC_SUB(a,b,r)  do {		\
    (r)->tv_sec  = (a)->tv_sec - (b)->tv_sec;	\
    (r)->tv_nsec = (a)->tv_nsec - (b)->tv_nsec;	\
    if ((r)->tv_nsec < 0) {			\
      (r)->tv_sec--;				\
      (r)->tv_nsec += 1000000000; 		\
    }						\
  } while (0)

#define TRIVA_CONN_STATE_PORTMAP_WRITE       1
#define TRIVA_CONN_STATE_PORTMAP_WAIT_CLOSE  2
#define TRIVA_CONN_STATE_ACCEPT_READ         3

typedef struct conn_state_t
{
  pd_ll_item conns;

  int fd;

  struct timespec t_next;

  struct
  {
    int      state;
    size_t   offset;

    trigsimbus_init_msg msg;
  } rw;
} conn_state;

PD_LL_SENTINEL(_connections);
uint16_t _access_port = (uint16_t) -1;

#define TRIVA_STATE_IDLE                1
#define TRIVA_STATE_GTIME               2
#define TRIVA_STATE_GTIME_SLAVE         3
#define TRIVA_STATE_FCACCEPT            4
#define TRIVA_STATE_FCDONE              5
#define TRIVA_STATE_LCTIME              6
#define TRIVA_STATE_START_OF_READOUT    7
#define TRIVA_STATE_READOUT             8

#define TRIVA_RW_STATE_NONE             0
#define TRIVA_RW_STATE_READ_ADDR        1
#define TRIVA_RW_STATE_READ_DATA        2
#define TRIVA_RW_STATE_WRITE_DATA       3

typedef struct mod_internal_state_t
{
  unsigned char pending_trig;

  unsigned int  user_dt : 1;
  unsigned int  interrupt_pending : 1;
  unsigned int  use_bus_ec : 1;

} mod_internal_state;

typedef struct module_t
{
  /* */

  struct
  {
    int      state;
    size_t   offset;

    uint32_t addr;
    uint32_t data;
  } rw;

  /* */

  int state;

  int fd;
  int modno;

  struct timespec t_timeout;
  struct timespec t_redo;

  struct timespec t_gtime;
  struct timespec t_fcatime;
  struct timespec t_ctime;

  struct
  {
    struct timespec t_recv;

    int      req_trig;
    uint32_t send_trig, send_ec;
  } bus[2];

  union
  {
    uint32_t               raw[256];
    lwroc_triva_reg_layout n; /* n = named */
  } r;

  mod_internal_state s;

  uint64_t *ptr_bus_deadtime;
  uint64_t  mask_bus_deadtime;

  uint32_t  num_dt_release;
  uint32_t  prev_num_dt_release;

  uint32_t  num_rw[2]; /* 0 = read, 1 = write */
  uint32_t  prev_num_rw[2];

  uint32_t  cnt_for_mismatch;

  uint32_t  last_print_status;
  uint32_t  last_print_control;

} module;

typedef struct trigbus_t
{
  module *modules;

} trigbus;

uint64_t _bus_deadtime = 0;
int _catchup_rescan = 0;

volatile int _sig_do_mismatch = 0;
volatile int _sig_do_wrong_trig = 0;
volatile int _sig_do_flip_spill = 0;

volatile int _ignore_deadtime_mod = -1;

int _spill_on = 0;

module  **_modules;
size_t  _n_modules;

module  **_modules_heap;
size_t  _n_modules_heap;

#define p_go                 (mod->r.n.control & TRIVA_CONTROL_GO)
#define p_slave            (!(mod->r.n.control & TRIVA_CONTROL_MASTER))
#define p_interrupt_enabled  (mod->r.n.control & TRIVA_CONTROL_IRQ_ENABLE)
#define p_bus_dt_enabled     (mod->r.n.control & TRIVA_CONTROL_BUS_ENABLE)

#define ec                   (mod->r.n.status  & TRIVA_STATUS_EC_MASK)
#define next_ec              ((mod->r.n.status + \
			       (1 << TRIVA_STATUS_EC_SHIFT)) &	\
			      TRIVA_STATUS_EC_MASK)

typedef struct mod_local_action_t
{
  unsigned int tdt               : 1;

  unsigned int clear_cur_trig    : 1;
  unsigned int take_pending_trig : 1;
  unsigned int take_front_in_mt  : 1;
  unsigned int take_bus_trig     : 1;

  /* unsigned int run_ticker     : 1; */
  unsigned int fire_readout      : 1;
  unsigned int fire_bus          : 1;

  unsigned int reset             : 1;

} mod_local_action;

#define get_current_trig     (mod->r.n.status & TRIVA_STATUS_TRIG_MASK)

#define set_current_trig(trig) do {				\
    mod->r.n.status &= (uint32_t) ~TRIVA_STATUS_TRIG_MASK;	\
    mod->r.n.status |= (uint32_t) trig;				\
  } while (0)

#define or_current_trig(trig) do {		\
    mod->r.n.status |= trig;			\
  } while (0)

#define set_ec(ec_shifted) do {					\
    mod->r.n.status &= (uint32_t) ~TRIVA_STATUS_EC_MASK;	\
    mod->r.n.status |= (ec_shifted);				\
  } while (0)

void triva_module_init(module *mod, int modno)
{
  memset(mod, 0, sizeof (module));

  mod->fd = -1;
  mod->modno = modno;

  /* printf ("%p %d\n",mod,mod->modno); */

  mod->state = TRIVA_STATE_IDLE;

  TIMESPEC_CLEAR(&mod->t_timeout);
  TIMESPEC_CLEAR(&mod->t_redo);
  TIMESPEC_CLEAR(&mod->t_gtime);
  TIMESPEC_CLEAR(&mod->t_fcatime);
  TIMESPEC_CLEAR(&mod->t_ctime);

  mod->r.n.status  = 0x1f20;
  mod->r.n.control =
    TRIVA_CONTROL_MASTER;

  mod->ptr_bus_deadtime  = &_bus_deadtime;
  mod->mask_bus_deadtime = ((uint64_t) 1) << modno;
}

void init_modules(size_t n)
{
  module *mods;
  size_t i;

  mods = (module *) malloc(n * sizeof (module));

  if (!mods)
    LWROC_FATAL("Failed to allocate modules.");

  _modules      = (module **) malloc(n * sizeof (module *));
  _modules_heap = (module **) malloc(n * sizeof (module *));

  if (!_modules ||
      !_modules_heap)
    LWROC_FATAL("Failed to allocate modules pointer list.");

  for (i = 0; i < n; i++)
    {
      _modules[i] = &(mods[i]);
      triva_module_init(_modules[i], (int) i);
    }

  _n_modules = n;
  _n_modules_heap = 0;
}

void trigbus_queue_trig_reset(module *send_mod, struct timespec *now,
			      int req_trig /* else reset*/,
			      uint32_t send_trig,uint32_t send_ec)
{
  size_t i;

  /* printf ("queue bus %d %d %d\n", req_trig, send_trig, send_ec); */

  /* Queue up that the trigger (or reset) will arrive on the bus
   * shortly for all connected modules.
   */

  /* For the time being, all modules on the same bus. */

  (void) send_mod;

  for (i = 0; i < _n_modules; i++)
    {
      module *mod = _modules[i];
      int j;

      /* We do not queue up bus messages. */

      for (j = 0; j < 2; j++)
	if (!TIMESPEC_ISSET(&mod->bus[j].t_recv))
	  break;

      if (j == 2)
	{
	  printf ("Internal limitation: bus message eaten for module #%d: "
		  "(r=%d, t=%d, ec=%d).\n",
		  mod->modno+1, req_trig, send_trig, send_ec);
	  continue;
	}

      {
	const struct timespec add = { 0 , 5*1000 }; /* 5 us */
	TIMESPEC_ADD(now, &add, &mod->bus[j].t_recv);
      }

      if (j == 0)
	{
	  if (!TIMESPEC_ISSET(&mod->t_redo) ||
	      TIMESPEC_CMP(&mod->bus[0].t_recv,&mod->t_redo,<))
	    mod->t_redo = mod->bus[0].t_recv;
	}

      mod->bus[j].req_trig  = req_trig;
      mod->bus[j].send_trig = send_trig;
      mod->bus[j].send_ec   = send_ec;
    }
}

void triva_module_update(module *mod, struct timespec *now,
			 uint32_t wr, size_t wr_off,
			 uint32_t front_in_mt)
{
  /*
    if ()
    front_in_mt
  */

  /* uint32_t front_in_mt = 0; */
  uint32_t bus_got_trig = 0;
  uint32_t bus_got_reset = 0;
  /* uint32_t bus_req_reset = 0; */
  uint32_t bus_recv_trig = 0;
  uint32_t bus_recv_evcnt = 0;
  uint32_t bus_recv_softtrig = 0;
  uint32_t bus_wait_send_dt = 0;
  uint32_t bus_deadtime = 0;
  uint32_t bus_got_bad_trig = 0;

  int      prev_state   = mod->state;
  uint32_t prev_status  = mod->r.n.status;
  uint32_t prev_control = mod->r.n.control;
  mod_internal_state prev_s = mod->s;
  int      do_print = 0;

  if (TIMESPEC_ISSET(&mod->bus[0].t_recv) &&
      !TIMESPEC_CMP(&mod->bus[0].t_recv,now,>))
    {
      if (mod->bus[0].req_trig)
	{
	  bus_got_trig = 1;
	  bus_recv_trig  = mod->bus[0].send_trig;
	  bus_recv_evcnt = mod->bus[0].send_ec;
	}
      else
	{
	  bus_got_reset = 1;
	}

      /*
      printf ("bus recv %d %d %d / %d\n",
	      bus_got_trig, bus_recv_trig, bus_recv_evcnt,
	      bus_got_reset);
      */

      mod->bus[0] = mod->bus[1];

      TIMESPEC_CLEAR(&mod->bus[1].t_recv);
    }

  bus_deadtime = *mod->ptr_bus_deadtime ? 1 : 0;

  for ( ; ; )
    {
      mod_local_action action;
      int state_next;

      memset(&action, 0, sizeof (action));

      action.tdt = 1;

      state_next = mod->state;

      switch (mod->state)
	{
	case TRIVA_STATE_IDLE:
	  if (!p_go)
	    action.clear_cur_trig = 1;
	  else
	    {
	      action.tdt = 0;

	      if (!p_slave) /* master */
		{
		  if (mod->s.pending_trig) /* pending_trig_or == 1 */
		    {
		      action.take_pending_trig = 1;
		      action.tdt = 1;
		      state_next = TRIVA_STATE_FCDONE;
		    }
		  else if (front_in_mt) /* front_in_mt_or == 1 */
		    {
		      action.take_front_in_mt = 1;
		      state_next = TRIVA_STATE_GTIME;

		      TIMESPEC_ADD(now,&mod->t_gtime,&mod->t_timeout);
		    }
		}
	      else /* slave */
		{
		  if (bus_got_trig)
		    {
		      action.take_bus_trig = 1;
		      state_next = TRIVA_STATE_GTIME_SLAVE;
		    }
		}
	    }
	  break;
	case TRIVA_STATE_GTIME:
	  action.tdt = 0;
	  action.take_front_in_mt = 1;

	  if (!TIMESPEC_CMP(&mod->t_timeout,now,>)) /* gtime */
	    {
	      state_next = TRIVA_STATE_FCACCEPT;

	      TIMESPEC_ADD(now,&mod->t_fcatime,&mod->t_timeout);
	    }
	  break;
	case TRIVA_STATE_GTIME_SLAVE:
	  action.tdt = 0;
	  state_next = TRIVA_STATE_FCACCEPT;

	  TIMESPEC_ADD(now,&mod->t_fcatime,&mod->t_timeout);
	  break;
	case TRIVA_STATE_FCACCEPT:
	  if (!TIMESPEC_CMP(&mod->t_timeout,now,>)) /* fcatime */
	    {
	      state_next = TRIVA_STATE_FCDONE;
	    }
	  break;
	case TRIVA_STATE_FCDONE:
	  action.fire_bus = 1;
	  state_next = TRIVA_STATE_LCTIME;

	  TIMESPEC_ADD(now,&mod->t_ctime,&mod->t_timeout);
	  break;
	case TRIVA_STATE_LCTIME:
	  if (!TIMESPEC_CMP(&mod->t_timeout,now,>)) /* ctime */
	    {
	      state_next = TRIVA_STATE_START_OF_READOUT;
	    }
	  break;
	case TRIVA_STATE_START_OF_READOUT:
	  action.fire_readout = 1;
	  state_next = TRIVA_STATE_READOUT;
	  break;
	case TRIVA_STATE_READOUT:
	  if ((mod->s.user_dt == 0) &&
	      (p_slave || (bus_wait_send_dt == 0)) &&
	      (p_slave || !p_bus_dt_enabled || (bus_deadtime == 0)))
	    {
	      action.clear_cur_trig = 1;
	      state_next = TRIVA_STATE_IDLE;
	      mod->num_dt_release++;
	    }
	  break;
	}

      if (0)
      printf ("action:[%d %d,%d,%d,%d %d,%d, %d]\n",
	      action.tdt,
	      action.clear_cur_trig,
	      action.take_pending_trig,
	      action.take_front_in_mt,
	      action.take_bus_trig,
	      action.fire_readout,
	      action.fire_bus,
	      action.reset);

      /* state update is at the end */

      if (action.take_pending_trig)
	{
	  set_current_trig(mod->s.pending_trig);
	  mod->s.pending_trig = 0;
	}

      if (action.take_front_in_mt)
	or_current_trig(front_in_mt);

      if (action.take_bus_trig)
	{
	  if (_sig_do_wrong_trig == mod->modno+1)
	    {
	      printf ("SIGUSR1 request wrong trig read, module #%d.\n  ",
		      mod->modno+1);
	      if (bus_recv_trig < 8)
		bus_recv_trig++;
	      else
		bus_recv_trig--;
	      _sig_do_wrong_trig = 0;
	    }
	  set_current_trig(bus_recv_trig);

	  if ((bus_recv_evcnt != ((next_ec >> 8) & 0x0f)) &&
	      (bus_recv_softtrig == 0))
	    mod->r.n.status |= TRIVA_STATUS_MISMATCH; /* mismatch = 1; */

	  mod->s.use_bus_ec = !!bus_recv_softtrig; /* FIXME - remove !! */

	  if (bus_got_bad_trig == 1)
	    mod->r.n.status |= TRIVA_STATUS_MISMATCH; /* mismatch = 1; */
	}

      /* if (run_ticker) */
	; /* Replaced with timing logic. */

      if (action.fire_bus)
	{
	  uint32_t ec_shifted;

	  if (mod->s.use_bus_ec == 1)
	    {
	      ec_shifted =
		((0 << 4) | bus_recv_evcnt) << TRIVA_STATUS_EC_SHIFT;
	    }
	  else
	    {
	      ec_shifted = next_ec;
	    }
	  set_ec(ec_shifted);

	  if (!p_slave)
	    {
	      trigbus_queue_trig_reset(mod, now, 1,
				       get_current_trig,
				       ((ec >> TRIVA_STATUS_EC_SHIFT) & 0x0f));
	    }
	}

      if (action.fire_readout)
	{
	  /*
	  printf ("#%d %d %d %d #%d   \n",
		  _config.mismatch.mod,
		  _config.mismatch.n,
		  _config.mismatch.hash,
		  mod->cnt_for_mismatch,
		  mod->modno+1);
	  */

	  mod->cnt_for_mismatch++;

	  if (_config.mismatch.n &&
	      (!_config.mismatch.mod ||
	       _config.mismatch.mod == mod->modno+1))
	    {
	      uint32_t n =
		mod->cnt_for_mismatch % (uint32_t) _config.mismatch.n;

	      if (n == (uint32_t) _config.mismatch.hash)
		{
		  printf ("Simulating mismatch, module #%d.\n  ",
			  mod->modno+1);
		  mod->r.n.status |= TRIVA_STATUS_MISMATCH;
		}
	    }
	  if (_sig_do_mismatch == mod->modno+1)
	    {
	      printf ("SIGUSR1 request mismatch, module #%d.\n  ",
		      mod->modno+1);
	      mod->r.n.status |= TRIVA_STATUS_MISMATCH;
	      _sig_do_mismatch = 0;
	    }

	  if (p_interrupt_enabled)
	    mod->s.interrupt_pending = 1;

	  mod->r.n.status |= TRIVA_STATUS_EON; /* event_ready = 1 */
	  mod->s.user_dt = 1;
	}

      if (wr_off == offsetof(lwroc_triva_reg_layout, status)) /* write */
	{
	  if (wr & TRIVA_STATUS_TRIG_MASK) /* sr_wr_mt_or */
	    {
	      mod->s.pending_trig = wr & TRIVA_STATUS_TRIG_MASK;
	    }

	  if (wr & TRIVA_STATUS_DT_CLEAR) /* sr_wr_dt_clr */
	    {
	      mod->s.user_dt = 0;
	      mod->s.interrupt_pending = 0;
	    }

	  if (wr & TRIVA_STATUS_IRQ_CLEAR) /* sr_wr_irq_clr = 1 */
	    {
	      mod->s.interrupt_pending = 0;
	    }

	  if (wr & TRIVA_STATUS_EV_IRQ_CLEAR) /* sr_wr_e_plus_i = 1 */
	    {
	      mod->r.n.status &=
		(uint32_t) ~TRIVA_STATUS_EON; /* event_ready = 0 */
	      mod->s.interrupt_pending = 0;
	    }
	}

      if (action.clear_cur_trig)
	{
	  set_current_trig(0);
	  mod->r.n.status &=
	    (uint32_t) ~TRIVA_STATUS_EON; /* event_ready = 0 */
	  mod->s.interrupt_pending = 0;
	  mod->s.use_bus_ec = 0;
	}

      if (wr_off == offsetof(lwroc_triva_reg_layout, control)) /* write */
	{
	  if (wr & TRIVA_CONTROL_IRQ_ENABLE) /* cr_wr_int_ena == 1 */
	    mod->r.n.control   |=
	      (uint32_t) TRIVA_CONTROL_IRQ_ENABLE;    /* interrupt_ena = 1 */

	  if (wr & TRIVA_CONTROL_IRQ_DISABLE) /* cr_wr_int_dis == 1 */
	    {
	      mod->r.n.control &=
		(uint32_t) ~TRIVA_CONTROL_IRQ_ENABLE; /* interrupt_ena = 0 */
	      mod->s.interrupt_pending = 0;
	    }

	  if (wr & TRIVA_CONTROL_RESET) /* cr_wr_res == 1 */
	    action.reset = 1;
	}

      /*printf ("%d %d %d\n", action.reset, (p_slave), (bus_got_reset == 1));*/

      if (action.reset /* cr_wr_res == 1 */ ||
	  ((p_slave) && (bus_got_reset == 1)))
	{
	  mod->s.pending_trig = 0;
	  set_current_trig(0);

	  mod->r.n.status &=
	    (uint32_t) ~TRIVA_STATUS_MISMATCH; /* mismatch = 0 */
	  mod->r.n.status |=
	    (uint32_t)  TRIVA_STATUS_EC_MASK;  /* ec = 0x1f */
	  mod->r.n.status &=
	    (uint32_t) ~TRIVA_STATUS_EON;      /* event_ready = 0 */

	  mod->s.interrupt_pending = 0;

	  if (!p_slave)
	    {
	      /* bus_req_reset = 1; */
	      trigbus_queue_trig_reset(mod, now, 0, 0, 0);
	    }

	  if (p_slave)
	    {
	      mod->r.n.control |= TRIVA_CONTROL_GO; /* go = 1 */
	    }

	  state_next = TRIVA_STATE_IDLE;
	}
      else
	{
	  /* bus_req_reset = 0; */
	}

      if (wr_off == offsetof(lwroc_triva_reg_layout, control)) /* write */
	{
	  if (wr & TRIVA_CONTROL_GO)                /* cr_wr_go == 1 */
	    mod->r.n.control |=
	      (uint32_t)  TRIVA_CONTROL_GO;         /* go = 1 */
	  else if (wr & TRIVA_CONTROL_HALT)         /* cr_wr_hlt == 1 */
	    mod->r.n.control &=
	      (uint32_t) ~TRIVA_CONTROL_GO;         /* go = 1 */

	  if (wr & TRIVA_CONTROL_MASTER)            /* cr_wr_mtm_ena == 1 */
	    mod->r.n.control |=
	      (uint32_t)  TRIVA_CONTROL_MASTER;     /* slave = 0 */
	  else if (wr & TRIVA_CONTROL_SLAVE)        /* cr_wr_mtm_dis == 1 */
	    mod->r.n.control &=
	      (uint32_t) ~TRIVA_CONTROL_MASTER;     /* slave = 1 */

	  if (wr & TRIVA_CONTROL_BUS_ENABLE)        /* cr_wr_bus_ena == 1 */
	    mod->r.n.control |=
	      (uint32_t)  TRIVA_CONTROL_BUS_ENABLE; /* bus_dt_ena = 1*/
	  else if (wr & TRIVA_CONTROL_BUS_DISABLE)  /* cr_wr_bus_dis == 1 */
	    mod->r.n.control &=
	      (uint32_t) ~TRIVA_CONTROL_BUS_ENABLE; /* bus_dt_ena = 0*/
	}

      if (wr_off == offsetof(lwroc_triva_reg_layout, ctime)) /* write */
	{
	  mod->r.n.ctime = wr & 0xffff;
	  mod->t_ctime.tv_nsec = (long) (100 * (0x10000 - mod->r.n.ctime));
	}
      if (wr_off == offsetof(lwroc_triva_reg_layout, fcatime)) /* write */
	{
	  mod->r.n.fcatime = wr & 0xffff;
	  mod->t_fcatime.tv_nsec = (long) (100 * (0x10000 - mod->r.n.fcatime));
	}

      /* tdt_latch = tdt; */

      if (state_next != mod->state)
	{
	  /* Update the state. */
	  mod->state = state_next;

	  /* If we are not in a timeout state, remove the timeout.
	   * Fixes resets happening etc...
	   */
	  if (mod->state != TRIVA_STATE_GTIME &&
	      mod->state != TRIVA_STATE_FCACCEPT &&
	      mod->state != TRIVA_STATE_LCTIME)
	    TIMESPEC_CLEAR(&mod->t_timeout);

	  /* Instead of getting back immediately from the outside,
	   * just make another turn in the state machine locally.
	   */
	  /*
	  continue;
	  */
	}
      mod->r.n.status &=
	(uint32_t) ~TRIVA_STATUS_DT_CLEAR;
      mod->r.n.status |=
	(mod->s.user_dt | action.tdt) ? TRIVA_STATUS_DT_CLEAR : 0;

      break;
    }

  if (p_slave)
    {
      /* We make life easy and say that deadtime distribution has 0 latency,
       * i.e. we need not do delay of the deadtime updates.
       */

      uint64_t prev_bus_deadtime = *mod->ptr_bus_deadtime;

      *mod->ptr_bus_deadtime &= ~mod->mask_bus_deadtime;
      *mod->ptr_bus_deadtime |=
	((mod->r.n.status & TRIVA_STATUS_DT_CLEAR) &&
	 _ignore_deadtime_mod != mod->modno) ? mod->mask_bus_deadtime : 0;

      if (!*mod->ptr_bus_deadtime && prev_bus_deadtime)
	{
	  /* Global deadtime on bus released.  All (masters) (one) needs
	   * to rerun their state machine, at the current time.
	   */
	  size_t i;
	  for (i = 0; i < _n_modules; i++)
	    {
	      module *chk_mod = _modules[i];

	      if (!(chk_mod->r.n.control & TRIVA_CONTROL_MASTER))
		continue;

	      /* It is ok to modify the t_redo, as we only move it earlier.
	       * any further update to the module will forget it.  But then
	       * we have already achieved our goal: rerun of the module.
	       */
	      if (!TIMESPEC_ISSET(&chk_mod->t_redo) &&
		  TIMESPEC_CMP(&chk_mod->t_redo,now,<))
		{
		  chk_mod->t_redo = *now;
		  /* If we are in the catchup loop, we need to resort the
		   * items.
		   */
		  _catchup_rescan = 1;
		}
	    }
	}
    }

  /* If the state or any of the status or control registers
   * changed, then schedule an update to happen in 10 ns.
   *
   * If not, the next update is at the next timeout value.
   */

  if (prev_state   != mod->state ||
      prev_status  != mod->r.n.status ||
      prev_control != mod->r.n.control ||
      prev_s.pending_trig != mod->s.pending_trig ||
      prev_s.user_dt != mod->s.user_dt)
    {
      const struct timespec add = { 0 , 10 };
      TIMESPEC_ADD(now, &add, &mod->t_redo);

      if (TIMESPEC_ISSET(&mod->t_timeout) &&
	  TIMESPEC_CMP(&mod->t_timeout,&mod->t_redo,<))
	mod->t_redo = mod->t_timeout;
    }
  else
    mod->t_redo = mod->t_timeout;

  if (TIMESPEC_ISSET(&mod->bus[0].t_recv))
    {
      if (!TIMESPEC_ISSET(&mod->t_redo) ||
	  TIMESPEC_CMP(&mod->bus[0].t_recv,&mod->t_redo,<))
	mod->t_redo = mod->bus[0].t_recv;
    }

  if (_config.debug)
    {
      if (wr_off != (size_t) -1 ||
	  mod->r.n.status  != mod->last_print_status ||
	  mod->r.n.control != mod->last_print_control)
	do_print = 1;
    }

  if (do_print)
  {
    time_t mt;
    struct tm *mt_tm;
    char mt_date[64];
    /*char mt_tz[64];*/

    mt = (time_t) now->tv_sec;
    mt_tm = localtime(&mt);
    strftime(mt_date,sizeof(mt_date),/*"%Y-%m-%d "*/"%H:%M:%S",mt_tm);
    /*strftime(mt_tz,sizeof(mt_tz),"%z %Z",mt_tm);*/
    printf ("#%2d: [%s.%09ld]  ", mod->modno+1, mt_date, now->tv_nsec);

    if (wr_off != (size_t) -1)
      printf ("wr:%08x@%02" MYPRIzx "  ", wr, wr_off);
    else
      printf ("                ");

    printf ("%04x %04x  [s:%d p:%02d i:%d &:%d] ",
	    mod->r.n.status,
	    mod->r.n.control,
	    mod->state,
	    mod->s.pending_trig,
	    mod->s.interrupt_pending,
	    mod->s.use_bus_ec);

    if (0)
      {
    if (TIMESPEC_ISSET(&mod->t_timeout))
      {
	uint64_t dt = (uint64_t) (mod->t_timeout.tv_nsec - now->tv_nsec) +
	  1000000000 * (uint64_t) (mod->t_timeout.tv_sec - now->tv_sec);

	printf ("[to:%7" PRIu64 "]  ", dt);
      }
    else
      printf ("              ");

    if (TIMESPEC_ISSET(&mod->bus[0].t_recv))
      {
	uint64_t dt = (uint64_t) (mod->bus[0].t_recv.tv_nsec - now->tv_nsec) +
	  1000000000 * (uint64_t) (mod->bus[0].t_recv.tv_sec - now->tv_sec);

	printf ("[br:%7" PRIu64 "]  ", dt);
      }
    else
      printf ("              ");

    if (TIMESPEC_ISSET(&mod->t_redo))
      {
	uint64_t dt = (uint64_t) (mod->t_redo.tv_nsec - now->tv_nsec) +
	  1000000000 * (uint64_t) (mod->t_redo.tv_sec - now->tv_sec);

	printf ("[rd:%7" PRIu64 "]  ", dt);
      }
    else
      printf ("              ");

    printf (" bDT:%d", bus_deadtime);
      }
    printf ("\n");

    mod->last_print_status  = mod->r.n.status;
    mod->last_print_control = mod->r.n.control;
  }
}



/* In order to handle the time-based actions, we keep a list of
 * modules with the next time they need treatment.  This creates an
 * issue when we due to register access interfere with some module in
 * the middle of the list.  But in that case, one just needs to move
 * it up or down in the array.  Hmmm, which requires one to find the
 * module.
 */

int module_t_redo_compare_less(module *mod_a, module *mod_b,
				  void *extra_ptr)
{
  (void) extra_ptr;
  return TIMESPEC_CMP(&mod_a->t_redo, &mod_b->t_redo, <);
}

void catchup_modules(struct timespec *now, struct timespec *t_next)
{
  size_t i;

  /* Before handling any register reads or writes, we need to process
   * any pending time events.
   */

 rescan_modules:
  /* As we are getting here, we will rebuild the scan list. */
  _catchup_rescan = 0;

  _n_modules_heap = 0;
  for (i = 0; i < _n_modules; i++)
    {
      module *mod = _modules[i];

      if (TIMESPEC_ISSET(&mod->t_redo))
	{
	  if (!TIMESPEC_CMP(&mod->t_redo,now,>))
	    {
	      LWROC_HEAP_INSERT(module *, _modules_heap, _n_modules_heap,
				module_t_redo_compare_less, mod, NULL);
	    }
	  else if (!TIMESPEC_ISSET(t_next) ||
		   TIMESPEC_CMP(&mod->t_redo,t_next,<))
	    {
	      *t_next = mod->t_redo;
	    }
	}
    }

  if (_n_modules_heap)
    for ( ; ; )
      {
	module *mod = _modules_heap[0];

	struct timespec t = mod->t_redo;

	triva_module_update(mod, &t, 0, (size_t) -1, 0);

	if (_catchup_rescan)
	  goto rescan_modules;

	if (TIMESPEC_ISSET(&mod->t_redo))
	  {
	    if (!TIMESPEC_CMP(&mod->t_redo,now,>))
	      goto again; /* Process this one again. */
	    else
	      {
		/* Later timeout... */

		if (!TIMESPEC_ISSET(t_next) ||
		    TIMESPEC_CMP(&mod->t_redo,t_next,<))
		  {
		    *t_next = mod->t_redo;
		  }
	      }
	  }

	/* Done with this item, replace by last item. */

	_n_modules_heap--;

	if (!_n_modules_heap)
	  break;

	_modules_heap[0] = _modules_heap[_n_modules_heap];

      again:
	LWROC_HEAP_MOVE_DOWN(module *, _modules_heap, _n_modules_heap,
			     module_t_redo_compare_less, 0, NULL);
      }
}

void module_before_select(module *mod, int *nfds,
			  fd_set *readfds, fd_set *writefds,
			  struct timespec *t_next)
{
  /* printf ("mod_before_select %d %d\n",mod->modno,mod->rw.state); */

  switch (mod->rw.state)
    {
    case TRIVA_RW_STATE_READ_ADDR:
    case TRIVA_RW_STATE_READ_DATA:
      FD_SET(mod->fd, readfds);
      if (mod->fd > *nfds)
	*nfds = mod->fd;
      break;
    case TRIVA_RW_STATE_WRITE_DATA:
      FD_SET(mod->fd, writefds);
      if (mod->fd > *nfds)
	*nfds = mod->fd;
      break;
    }

  if (TIMESPEC_ISSET(&mod->t_redo))
    {
      if (!TIMESPEC_ISSET(t_next) ||
	  TIMESPEC_CMP(&mod->t_redo,t_next,<))
	{
	  *t_next = mod->t_redo;
	}
    }
}

int module_after_select(module *mod,
			fd_set *readfds, fd_set * writefds,
			struct timespec *now)
{
  size_t nr;
  ssize_t n;

  /* printf ("mod_after_select %d %d\n",mod->modno,mod->rw.state); */

  switch (mod->rw.state)
    {
    case TRIVA_RW_STATE_READ_ADDR:
    case TRIVA_RW_STATE_READ_DATA:
      if (!FD_ISSET(mod->fd, readfds))
	break;

      nr = sizeof (uint32_t) - mod->rw.offset;
      n = read(mod->fd,
	       mod->rw.state == TRIVA_RW_STATE_READ_ADDR ?
	       &mod->rw.addr : &mod->rw.data,
	       nr);

      if (n == -1)
	{
	  if (errno == EAGAIN ||
	      errno == EINTR)
	    break;
	  LWROC_PERROR("read");
	  LWROC_ERROR("Unexpected failure reading from socket.");
	  return 0;
	}
      if (n == 0)
	return 0;
      mod->rw.offset += (size_t) n;
      if (mod->rw.offset == sizeof (uint32_t))
	{
	  if (mod->rw.state == TRIVA_RW_STATE_READ_ADDR)
	    {
	      mod->rw.addr = ntohl(mod->rw.addr);

	      /* printf ("got addr: %08x\n", mod->rw.addr); */

	      if (mod->rw.addr & 0x80000000)
		{
		  mod->rw.state = TRIVA_RW_STATE_READ_DATA;
		  mod->num_rw[1]++;
		}
	      else if (mod->rw.addr & 0x40000000)
		{
		  mod->rw.data =
		    mod->r.raw[((mod->rw.addr & 0x00ffffff) >> 2) & 0xff];

		  mod->rw.data = htonl(mod->rw.data);

		  mod->rw.state = TRIVA_RW_STATE_WRITE_DATA;

		  mod->num_rw[0]++;
		}
	      else
		{
		  LWROC_ERROR_FMT("Bad address read (%08x), no R/W mark.",
				  mod->rw.addr);
		  return 0;
		}

	      mod->rw.offset = 0;
	    }
	  else
	    {
	      mod->rw.data = ntohl(mod->rw.data);

	      /* printf ("got data: %08x\n", mod->rw.data); */

	      /* This module has a write access, so bang on it
	       * directly.
	       */

	      triva_module_update(mod, now,
				  mod->rw.data, mod->rw.addr & 0x00ffffff,
				  0);

	      mod->rw.state = TRIVA_RW_STATE_READ_ADDR;
	      mod->rw.offset = 0;
	    }
	}
      break;
    case TRIVA_RW_STATE_WRITE_DATA:
      if (!FD_ISSET(mod->fd, writefds))
	break;

      nr = sizeof (uint32_t) - mod->rw.offset;
      n = write(mod->fd, &mod->rw.data, nr);

      if (n == -1)
	{
	  if (errno == EAGAIN ||
	      errno == EINTR)
	    break;
	  LWROC_PERROR("write");
	  LWROC_ERROR("Unexpected failure writing to socket.");
	  return 0;
	}
      if (n == 0)
	{
	  LWROC_ERROR("Bad write - 0 returned - closing!");
	  return 0;
	}
      mod->rw.offset += (size_t) n;
      if (mod->rw.offset == sizeof (uint32_t))
	{
	  mod->rw.state = TRIVA_RW_STATE_READ_ADDR;
	  mod->rw.offset = 0;
	}
      break;
    }

  return 1;
}

void conn_before_select(conn_state *conn, int *nfds,
			fd_set *readfds, fd_set *writefds,
			struct timespec *t_next)
{
  /* printf ("conn_before_select %d\n",conn->rw.state); */

  switch (conn->rw.state)
    {
    case TRIVA_CONN_STATE_PORTMAP_WAIT_CLOSE:
    case TRIVA_CONN_STATE_ACCEPT_READ:
      FD_SET(conn->fd, readfds);
      if (conn->fd > *nfds)
	*nfds = conn->fd;
      break;
    case TRIVA_CONN_STATE_PORTMAP_WRITE:
      FD_SET(conn->fd, writefds);
      if (conn->fd > *nfds)
	*nfds = conn->fd;
      break;
    }

  if (TIMESPEC_ISSET(&conn->t_next))
    {
      if (!TIMESPEC_ISSET(t_next) ||
	  TIMESPEC_CMP(&conn->t_next,t_next,<))
	{
	  *t_next = conn->t_next;
	}
    }
}

int conn_after_select(conn_state *conn,
		      fd_set *readfds, fd_set *writefds,
		      struct timespec *now)
{
  size_t nr;
  ssize_t n;

  /* printf ("conn_after_select %d\n",conn->rw.state); */

  if (TIMESPEC_CMP(&conn->t_next,now,<))
    {
      LWROC_ERROR("Timeout for setup connection.");
      /* Timeout, tear connection down. */
      return 0;
    }

  switch (conn->rw.state)
    {
    case TRIVA_CONN_STATE_ACCEPT_READ:
      if (!FD_ISSET(conn->fd, readfds))
	break;

      nr = sizeof (conn->rw.msg) - conn->rw.offset;
      n = read(conn->fd, &conn->rw.msg, nr);

      if (n == -1)
	{
	  if (errno == EAGAIN ||
	      errno == EINTR)
	    break;
	  LWROC_PERROR("read");
	  LWROC_ERROR("Unexpected failure reading from socket.");
	  return 0;
	}
      if (n == 0)
	{
	  LWROC_ERROR("Socket closed!");
	  return 0;
	}
      conn->rw.offset += (size_t) n;
      if (conn->rw.offset == sizeof (conn->rw.msg))
	{
	  uint32_t magic = ntohl(conn->rw.msg.magic);
	  uint32_t modno = ntohl(conn->rw.msg.port_mod);
	  module *mod;

	  if (magic != (uint32_t) ~TRIGBUSSIM_MAGIC)
	    {
	      LWROC_ERROR_FMT("Access connection with wrong magic "
			      "(%" PRIu32 " != expect %" PRIu32 ").",
			      magic, ~TRIGBUSSIM_MAGIC);
	      return 0;
	    }

	  /* Ok, so is the requested module free? */
	  if (!modno ||
	      modno > _n_modules)
	    {
	      LWROC_ERROR_FMT("Request for module %d, "
			      "which does not exist [1,%" MYPRIzd "].",
			      modno, _n_modules);
	      return 0;
	    }

	  /* hehe - let's be one-based! */
	  mod = _modules[modno-1];

	  if (mod->fd != -1)
	    {
	      /* We do not support multiple operators. */
	      LWROC_ERROR_FMT("Request for module %d, "
			      "which already has controller.",
			      modno);
	      return 0;
	    }

	  LWROC_INFO_FMT("Controlling module #%d.", modno);

	  /* Connect the module. */
	  mod->fd = conn->fd;
	  mod->rw.state = TRIVA_RW_STATE_READ_ADDR;
	  mod->rw.offset = 0;

	  /*
	   * We actually do not need to disable Nagle's algorithm
	   * on this end of the connection, since we *always* perform
	   * a read call (for the next address) after each write.
	  int flag = 1;
	  setsockopt(mod->fd, IPPROTO_TCP, TCP_NODELAY,
		     (char *) &flag, sizeof(int));
	  */

	  /* Do not close the descriptor. */
	  conn->fd = -1;

	  /* Get rid of the conn structure. */
	  return 0;
	}
      break;
    case TRIVA_CONN_STATE_PORTMAP_WRITE:
      if (!FD_ISSET(conn->fd, writefds))
	break;

      nr = sizeof (conn->rw.msg) - conn->rw.offset;
      n = write(conn->fd, &conn->rw.msg, nr);

      if (n == -1)
	{
	  if (errno == EAGAIN ||
	      errno == EINTR)
	    break;
	  LWROC_PERROR("write");
	  LWROC_ERROR("Unexpected failure writing to socket.");
	  return 0;
	}
      if (n == 0)
	{
	  LWROC_ERROR("Bad write - 0 returned - closing!");
	  return 0;
	}
      conn->rw.offset += (size_t) n;
      /* printf ("wrote %" MYPRIzd "\n", n); */
      if (conn->rw.offset == sizeof (conn->rw.msg))
	{
	  /* We are done writing, wait for other end to tear down
	   * connection.  Since otherwise we get an unconnectable port
	   * for a while. */
	  conn->rw.state = TRIVA_CONN_STATE_PORTMAP_WAIT_CLOSE;
	}
      break;
    case TRIVA_CONN_STATE_PORTMAP_WAIT_CLOSE:
      if (!FD_ISSET(conn->fd, readfds))
	break;
      return 0; /* Ready for reading - closed. */
    }
  return 1;
}

void setup_accept(int server_fd, int *nfds, fd_set *readfds)
{
  FD_SET(server_fd, readfds);
  if (server_fd > *nfds)
    *nfds = server_fd;
}

void accept_connection(int server_fd, const char *descr,
		       int portmap,
		       struct timespec *now)
{
  conn_state *conn;
  int client_fd =
    lwroc_net_io_accept(server_fd, descr, NULL);

  if (client_fd == -1)
    return;

  conn = (conn_state *) malloc (sizeof (conn_state));

  if (!conn)
    LWROC_FATAL_FMT("Failure allocating memory for %s state.", descr);

  conn->fd = client_fd;

  if (portmap)
    {
      conn->rw.state = TRIVA_CONN_STATE_PORTMAP_WRITE;
      conn->rw.offset = 0;

      conn->rw.msg.magic = htonl(TRIGBUSSIM_MAGIC);
      conn->rw.msg.port_mod = htonl(_access_port);
    }
  else
    {
      conn->rw.state = TRIVA_CONN_STATE_ACCEPT_READ;
      conn->rw.offset = 0;
    }

  conn->t_next = *now;
  conn->t_next.tv_sec += 2; /* timeout in 2 seconds */

  PD_LL_ADD_BEFORE(&_connections, &conn->conns);

  LWROC_INFO_FMT("Accepted %s.", descr);
}

int signal_pipe_fd[2] = { -1, -1 };

void sighandler(int sig)
{
  char dummy = 0;
  ssize_t n;

  switch (sig)
    {
    case SIGUSR1:
      if (_config.sigusr1_wrong_trig)
	{
	  /* The wrong trigger read cannot be master module. */
	  _sig_do_wrong_trig =
	    2 + (int) ((size_t) time(NULL) % (_n_modules - 1));
	  /* printf ("Setup for wrong trig, module #%d...\n",
		     _sig_do_wrong_trig); */
	}
      else
	{
	  _sig_do_mismatch = 1 + (int) ((size_t) time(NULL) % _n_modules);
	  /* printf ("Setup for mismatch, module #%d...\n",
		     _sig_do_mismatch); */
	}
      break;
    case SIGUSR2:
      if (_config.sigusr2_spill_flip)
	{
	  _sig_do_flip_spill = 1;
	}
      else
	{
	  if (_ignore_deadtime_mod != -1)
	    {
	      printf ("SIGUSR2 stops ignoring deadtime, module #%d.  \n",
		      _ignore_deadtime_mod+1);
	      _ignore_deadtime_mod = -1;
	    }
	  else
	    {
	      if (_config.modules <= 1)
		printf ("Cannot ignore deadtime, no slaves.  \n");
	      _ignore_deadtime_mod = 1 + (rand() % (_config.modules-1));
	      printf ("SIGUSR2 ignores deadtime, module #%d.  \n",
		      _ignore_deadtime_mod+1);
	    }
	}
      break;
    }
  /* Other thread? then we need a memory fence. */

  n = write (signal_pipe_fd[1], &dummy, sizeof (char));
  (void) n;
}

void trigbussim_usage(char *cmdname)
{
  printf ("TRIGBUSSIM - trigger bus simulator\n");
  printf ("\n");
  printf ("Usage: %s <options>\n", cmdname);
  printf ("\n");
  printf ("  --gdb                    Run me in GDB (must be the first "
	  "argument).\n");
  printf ("  --port=PORT              Network TCP/IP port to bind to.\n");
  printf ("  --debug                  Print write accesses and state changes.\n");
  printf ("  --modules=N              Number of modules.\n");
  printf ("  --rate=N                 Rate of triggers [Hz].\n");
  printf ("  --periodic=N             Periodic trigger N (1 Hz, @x.123 s).\n");
  printf ("  --mismatch=mod#,N,hash   Simulate mismatch failures ~every N events.\n");
  printf ("  --sigusr1-wrong-trig     SIGUSR1 causes a wrong trigger read.\n");
  printf ("  --sigusr2-spill-flip     SIGUSR2 causes a spill trigger.\n");
  printf ("  --sigusr2-spill-rand     SIGUSR2 causes a random spill trigger.\n");
  printf ("  --insomniac              Disable debugger sleep.\n");
  printf ("  --help                   Show this message.\n");
  printf ("\n");
}

int main(int argc, char *argv[])
{
  int portmap_fd;
  int access_fd;

  int i;
  size_t j;

  fd_set readfds;
  fd_set writefds;

  struct timespec now;
  struct timespec t_next;

  struct timespec t_next_trig;
  struct timespec next_trig_add = { 1, 0 }; /* dummy values */

  struct timespec t_next_trig_periodic;

  time_t t_last_status = 0;

  memset (&_config, 0, sizeof (_config));
  _config.rate = -1;
  _config.port = TRIGBUSSIM_PORT;

  for (i = 1; i < argc; i++)
    {
      char *request = argv[i];
      char *post;

      if (LWROC_MATCH_C_ARG("--help")) {
	trigbussim_usage(argv[0]);
	exit(0);
      } else if (LWROC_MATCH_C_ARG("--debug")) {
	_config.debug = 1;
      } else if (LWROC_MATCH_C_ARG("--insomniac")) {
	_message_internal_no_sleep = 1;
      } else if (LWROC_MATCH_C_PREFIX("--port=",post)) {
	_config.port = lwroc_parse_hex_u16(post, "port number");
      } else if (LWROC_MATCH_C_PREFIX("--modules=",post)) {
	_config.modules = lwroc_parse_hex_u16(post, "modules");
      } else if (LWROC_MATCH_C_PREFIX("--rate=",post)) {
	_config.rate = atof(post);
      } else if (LWROC_MATCH_C_PREFIX("--periodic=",post)) {
	_config.periodic_trig =
	  (uint32_t) lwroc_parse_hex_limit(post, "periodic trig", 1, 15);
      } else if (LWROC_MATCH_C_PREFIX("--mismatch=",post)) {
	sscanf(post,"%d,%d,%d",
	       &_config.mismatch.mod,
	       &_config.mismatch.n,
	       &_config.mismatch.hash);
      } else if (LWROC_MATCH_C_ARG("--sigusr1-wrong-trig")) {
	_config.sigusr1_wrong_trig = 1;
      } else if (LWROC_MATCH_C_ARG("--sigusr2-spill-flip")) {
	_config.sigusr2_spill_flip = 1;
      } else if (LWROC_MATCH_C_ARG("--sigusr2-spill-rand")) {
	_config.sigusr2_spill_flip = 2;
      } else {
	LWROC_BADCFG_FMT("Unrecognized option: '%s'", argv[i]);
      }
    }

  if (_config.modules < 1)
    LWROC_BADCFG("Must have at least 1 module.");

  if (_config.modules < 2 &&
      _config.sigusr1_wrong_trig)
    LWROC_BADCFG("Must have at least 2 modules with --sigusr1-wrong-trig.");

  init_modules((size_t) _config.modules);

  portmap_fd = lwroc_net_io_socket_bind(_config.port, 1, "portmap", 0);
  access_fd  = lwroc_net_io_socket_bind(-1, 1, "access", 0);
  _access_port = lwroc_net_io_socket_get_port(access_fd, "access");

  if (pipe(signal_pipe_fd) != 0)
    {
      LWROC_PERROR("pipe");
      LWROC_FATAL("Failed to create signal pipe.");
    }

  /* TODO: where available, use sigaction with SA_RESTART ? */

  signal(SIGUSR1, sighandler);
  signal(SIGUSR2, sighandler);

  clock_gettime(CLOCK_REALTIME, &now);

  if (_config.rate < 0)
    _config.rate = 10;

  if (_config.rate != 0.0)
    {
      double period = 1.0 / _config.rate;
      int seconds = (int) (floor(period) + 0.5);
      int nanoseconds = (int) (1000000000 * (period - seconds));
      next_trig_add.tv_sec  = seconds;
      next_trig_add.tv_nsec = nanoseconds;

      t_next_trig = now;
    }
  else
    TIMESPEC_CLEAR(&t_next_trig);

  if (_config.periodic_trig)
    {
      t_next_trig_periodic.tv_sec  = now.tv_sec + 1;
      t_next_trig_periodic.tv_nsec = 123456789;
    }
  else
    TIMESPEC_CLEAR(&t_next_trig_periodic);

  for ( ; ; )
    {
      int nfds = -1;
      pd_ll_item *iter, *tmp_iter;
      struct timespec timeout;
      struct timeval tv_timeout, *ptimeout;

      TIMESPEC_CLEAR(&t_next);
      FD_ZERO(&readfds);
      FD_ZERO(&writefds);

      FD_SET(signal_pipe_fd[0], &readfds);

      for (j = 0; j < _n_modules; j++)
	{
	  module *mod = _modules[j];

	  /* printf ("mbs %d %d\n",mod->modno,mod->fd); */
	  /* fflush(stdout); */

	  if (mod->fd == -1)
	    continue;

	  module_before_select(mod, &nfds, &readfds, &writefds, &t_next);
	}

      PD_LL_FOREACH(_connections, iter)
	{
	  conn_state *conn =
	    PD_LL_ITEM(iter, conn_state, conns);

	  conn_before_select(conn, &nfds, &readfds, &writefds, &t_next);
	}

      setup_accept(portmap_fd, &nfds, &readfds);
      setup_accept(access_fd, &nfds, &readfds);

      ptimeout = NULL;

      if (TIMESPEC_ISSET(&t_next_trig))
	if (!TIMESPEC_ISSET(&t_next) ||
	    TIMESPEC_CMP(&t_next_trig,&t_next,<))
	  t_next = t_next_trig;

      if (TIMESPEC_ISSET(&t_next))
	{
	  TIMESPEC_SUB(&t_next, &now, &timeout);
	  if (timeout.tv_sec < 0)
	    TIMESPEC_CLEAR(&timeout);
	  tv_timeout.tv_sec = timeout.tv_sec;
	  tv_timeout.tv_usec = (int) (timeout.tv_nsec / 1000 + 1);
	  ptimeout = &tv_timeout;
	}

      /* Previously used pselect, to have ns precision timeout.  Not
       * available everywhere.  Changing to us precision timeouts does
       * not seem to change behaviour very much.  Note that only the
       * timeout has less resolution.  The times when things are
       * supposed to happen are still calculated with ns precision.
       */
      select(nfds + 1,  &readfds, &writefds, NULL, ptimeout);

      if (_sig_do_flip_spill)
	{
	  module *mod = _modules[0];

	  _sig_do_flip_spill = 0;
	  if (_config.sigusr2_spill_flip == 2)
	    _spill_on = rand() % 2;
	  else
	    _spill_on = !_spill_on;
	  /* TODO: Cheating by injecting the trigger 12/13 as
	   * pending trigger.  (I.e. as far as TRIVA is concerned,
	   * as if it came via the VME access.  Reason is that
	   * trigger injection below is subject to deadtime...
	   */
	  mod->s.pending_trig = _spill_on ? 12 : 13;
	  printf ("Inject trigger %d, module #%d.\n",
		  mod->s.pending_trig, mod->modno+1);
	}

      clock_gettime(CLOCK_REALTIME, &now);

      if (TIMESPEC_ISSET(&t_next_trig) &&
	  TIMESPEC_CMP(&t_next_trig,&now,<))
	{
	  module *mod;
	  uint32_t trig;

	  /* First run the modules up to the time of the trigger.
	   * t_next is a dummy, here, will be updated on the catchup
	   * that is done after the trigger.
	   */

	  catchup_modules(&t_next_trig, &t_next);

	  /* Try to fire the trigger in the master module. */

	  mod = _modules[0];

	  trig = (uint32_t) (t_next_trig.tv_sec % 11) + 1;

	  if (trig == _config.periodic_trig)
	    trig = ((trig) % 11) + 1;

	  if (TIMESPEC_ISSET(&t_next_trig_periodic) &&
	      TIMESPEC_CMP(&t_next_trig_periodic,&t_next_trig,==))
	    {
	      /* Try to fire the periodic trigger. */
	      trig = _config.periodic_trig;

	      printf ("Periodic %d.\n", _config.periodic_trig);

	      /* Next time to try the periodic trigger. */
	      t_next_trig_periodic.tv_sec  = now.tv_sec;
	      t_next_trig_periodic.tv_nsec = 123456789;
	      if (TIMESPEC_CMP(&t_next_trig_periodic,&now,<))
		t_next_trig_periodic.tv_sec++;
	    }

	  triva_module_update(mod, &t_next_trig,
			      0, (size_t) -1,
			      trig);

	  /* printf ("trig: %d\n",(uint32_t) (t_next_trig.tv_sec % 15) + 1); */

	  TIMESPEC_ADD(&now, &next_trig_add, &t_next_trig);

	  if (TIMESPEC_ISSET(&t_next_trig_periodic) &&
	      TIMESPEC_CMP(&t_next_trig_periodic,&t_next_trig,<))
	    t_next_trig = t_next_trig_periodic;
	}

      catchup_modules(&now, &t_next);

      for (j = 0; j < _n_modules; j++)
	{
	  module *mod = _modules[j];

	  if (mod->fd == -1)
	    continue;

	  if (!module_after_select(mod, &readfds, &writefds, &now))
	    {
	      /* I/O error - disconnect/close. */

	      LWROC_INFO_FMT("Control for module #%d detached.", mod->modno+1);
	      close(mod->fd);
	      mod->fd = -1;
	      mod->rw.state = TRIVA_RW_STATE_NONE;
	    }
	}

      PD_LL_FOREACH_TMP(_connections, iter, tmp_iter)
	{
	  conn_state *conn =
	    PD_LL_ITEM(iter, conn_state, conns);

	  if (!conn_after_select(conn, &readfds, &writefds, &now))
	    {
	      if (conn->fd != -1)
		{
		  close(conn->fd);
		  conn->fd = -1;
		}
	      PD_LL_REMOVE(iter);
	      free(conn);
	    }
	}

      if (FD_ISSET(portmap_fd, &readfds))
	accept_connection(portmap_fd, "portmap connection", 1, &now);
      if (FD_ISSET(access_fd, &readfds))
	accept_connection(access_fd, "access connection", 0, &now);

      if (FD_ISSET(signal_pipe_fd[0], &readfds))
	{
	  char dummy;
	  ssize_t n;
	  n = read(signal_pipe_fd[0], &dummy, sizeof (char));
	  (void) n;
	}

      if (!_config.debug &&
	  now.tv_sec != t_last_status)
	{
	  t_last_status = now.tv_sec;

	  for (j = 0; j < _n_modules && j < 5; j++)
	    {
	      module *mod = _modules[j];

	      printf ("#%d:%04x:%04x"
		      /*"[r:%d,w:%d]"*/
		      " ",
		      mod->modno+1, mod->r.n.status, mod->r.n.control
		      /*,
			mod->num_rw[0] - mod->prev_num_rw[0],
			mod->num_rw[1] - mod->prev_num_rw[1]*/);
	      mod->prev_num_rw[0] = mod->num_rw[0];
	      mod->prev_num_rw[1] = mod->num_rw[1];
	    }
	  if (_n_modules > 5)
	    printf ("... ");
	  {
	    module *mod = _modules[0];
	    printf ("%d   ",mod->num_dt_release - mod->prev_num_dt_release);
	    mod->prev_num_dt_release = mod->num_dt_release;
	  }
	  printf ("\r");
	  fflush(stdout);
	}
    }
}

